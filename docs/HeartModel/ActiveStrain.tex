\Paragraph{The active strain decomposition}
    
The conclusions above all rely on the assumption that soft tissue can
be modeled as elastic. In reality, cardiac cells have viscoelastic
behaviour. While rapid and repeated stretching causes a stiffing
effect, the tissue loosens when exposed to prolonged elongation. Some
of these effects are permanent, meaning that the pullback into
Lagrange configuration would actually lead to a different,
intermediate configuration. To account for this plasticity, we use the
\textit{active strain} model described
in~\cite{ambrosi2011electromechanical,activestrainmodels}. Separating the deformation gradient
$\fF$ into an elastic, passive part $\fF_\text{e}$ and an active part
$\fF_\text{a}$, we write
    \begin{equation}
      \fF = \fF_\text{e}\fF_\text{a}
    \end{equation}
The tensor $\fF_\text{a}$ accounts for the active deformation induced by the cells.

For each cell, we denote by $\gamma_{\vec f},\gamma_{\vec s},\gamma_{\vec n}
\colon  [0,T]\times \Omega\longrightarrow\R$
their deformation into the respective direction $\ff ,\fs ,\fn$. Setting
    \begin{equation}
      \fF_\text{a} =
      \vec I + \gamma_{\vec f} \ffz\otimes\ffz +\gamma_{\vec s}\fsz\otimes\fsz + \gamma_{\vec n}\fnz\otimes\fnz \,,
    \end{equation}
we can make the following assumptions:
\begin{itemize}
	\item Cardiac cells are transverse isotropic along $\ff$, i.e. $\gamma_{\vec s} = \gamma_{\vec n}$.
	\item The cells consist mostly of water and are therefore isochoric, i.e.,
        their volume is constant, so that $J_\text{a} := \det (\fF_\text{a} )=1$.
\end{itemize}
From these two conditions follows directly that
    \[
    \det (\fF_\text{a} ) = (\gamma_{\vec f}+1)(\gamma_{\vec s}+1)^2=1
    \]
and therefore $\displaystyle \gamma_{\vec s} =\frac1{\sqrt{\gamma_{\vec f}+1}}-1$.
Hence we only need to compute $\gamma_{\vec f}$. We then set a new stress response
    \[
    \vec P = \D_{\vec F} W(\fF\fF_\text{a}^{-1}) \,,
    \]
which allows a pullback into the actual Lagrange configuration.
An in-depth explanation of the evolution of $\gamma_{\vec f}$ is given in Sect.~\ref{sec:force}.

Within this active strain model, we need to recompute the invariants
as they now depend on $\gamma_{\vec f}$, i.e.
    \[
    \iota_1^\text{e} = (1+\gamma_{\vec f})\iota_1
    - \left((\gamma_{\vec f}+\gamma_{\vec f}\frac{\gamma_{\vec f}+2}{(1+\gamma_{\vec f})^2}\right) \iota_{4,\vec f }
    \]
and    
     \[
     \iota_{4,\vec f }^\text{e} = (1+\gamma_{\vec f})^{-2}\iota_{4,\vec f } \,,\qquad
     \iota_{4,\vec s }^\text{e}
     = (1+\gamma_{\vec f})\iota_{4,\vec s } ,\qquad \iota_{8,\vec f \vec s }^\text{e}
     = (1+\gamma_{\vec f})^{-\frac{1}{2}}\iota_{8,\vec f \vec s }
     \,.
     \]
