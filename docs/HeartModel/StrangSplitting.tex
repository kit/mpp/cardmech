\section{A splitting method for the monodomain equation}
\label{sec:splitting}

The idea of the splitting methods for the coupled model \eqref{eq:mono1} and \eqref{eq:mono2}
is a decoupling into a parabolic problem without reaction term
    \begin{eqnarray*}
        \beta  C_\text{m}
        \partial_t V_\text{m}
          &=&
         \nabla \cdot\vec \sigma
         \nabla V_\text{m}
     \end{eqnarray*}
and independent ODEs for $x\in \Omega$ without the diffusion term
        \begin{eqnarray*}
          C_\text{m}
        \partial_t V_\text{m}
          &=&
          -I_\text{ion}(V_\text{m},w) + I_\text{ext}
            \,,
            \\
            \partial_t w
          &=&
          G_w(w,V_\text{m})
          \,.
     \end{eqnarray*}
Let $\vec V^h\subset \mathrm H^1(\Omega)$ be a conforming finite element space
corresponding to a tetrahedral mesh $\Omega^h_\text{M}= \bigcup_{K\in \mathcal K_h} K$, and let
$\mathcal N^h\subset \overline \Omega_\text{M}$ be associated nodal points. Here, we use
linear elements in the tetrahedron $K$ with linear 
nodal basis functions $\phi_x$, where $x\in \mathcal N_h$
are the corner points.

Both, the electric potential $V_\text{m}$ and the gating variables  
are approximated in the finite element space $\vec V^h$ and thus represented
by the nodal values.

Let $N\in \mathbb N$ be the number of time steps, and set $t_n = n{\vartriangle} t$
with time steps size ${\vartriangle} t = T/N$. Furthermore, let $M\in \mathbb N$
be the number of substeps for the approximation of the cell models.
Then, starting with initial values $V_\text{m}^{h,0} = V_\text{m}^0$ and $w^{h,0} = w_0$,
the symmetric Strang splitting with substepping is defined for $n=1,2,...,N$ as follows: 
\begin{enumerate}
\item Independently for every nodal point $x\in \mathcal N^h$, compute
in $(t_{n-1},t_{n-1/2})$ an approximation of the ODE
        \begin{eqnarray*}
          C_\text{m}
          \partial_t V_\text{m}^{h}
          &=&
          -I_\text{ion}(V_\text{m}^{h},w^{h}) + I_\text{ext}
          \,,
          \\
          \partial_t w^{h}
          &=&
          G_w(w^{h},V_\text{m}^{h})
        \end{eqnarray*}
with initial values $V_\text{m}^{h}(t_{n-1}) = V_\text{m}^{h,n-1}$
and $w^{h}(t_{n-1}) = w^{h,n-1}$ using $M$ time steps of step size
$\displaystyle\frac{{\vartriangle} t}{2M}$; then, set
$V_\text{m}^{h,n-1/2}= V_\text{m}^{h}(t_{n-1/2})$ and $w^{h,n-1/2}= w^{h}(t_{n-1/2})$.
\item
For given $V_\text{m}^{h,n-1/2}\in \vec V^h$, compute $\widehat V_\text{m}^{h,n-1/2}\in \vec V^h$
with the implicit Euler method,
i.e., solve 
    \begin{eqnarray*}
      \qquad 
      \int_{\Omega_\text{M}}
        \Big(
        \widehat V_\text{m}^{h,n-1/2} \phi^h
        +
        \frac{{\vartriangle} t}{\beta  C_\text{m} }
        \, \nabla \widehat V_\text{m}^{h,n-1/2} \cdot \vec \sigma\nabla\phi^h     
        \Big)\,\mathrm dx = 
      \int_{\Omega_\text{M}}
         V_\text{m}^{h,n-1/2} \phi^h
        \,\mathrm dx \,,\qquad \phi^h\in \vec V^h
        \,.
     \end{eqnarray*}
\item Repeat the first step in $(t_{n-1/2},t_{n})$
starting with $(\widehat V_\text{m}^{h,n-1/2},w^{h,n-1/2})$; 
this gives $(V_\text{m}^{h,n},w^{h,n})$.
\end{enumerate}

\Paragraph{Time integration for the ODE system}
To solve the ODE   system
 
       \begin{eqnarray*}
          C_\text{m}
          \partial_t V_\text{m}^{h}
          &=&
          -I_\text{ion}(V_\text{m}^{h},w^{h}) + I_\text{ext}
          \,,
          \\
          \partial_t w^{h}
          &=&
          G_w(w^{h},V_\text{m}^{h})
        \end{eqnarray*}
with initial values $V_\text{m}^{h}(t_{n-1}) = V_\text{m}^{h,n-1}$
and $w^{h}(t_{n-1}) = w^{h,n-1}$ independently in every nodal point $x\in \mathcal N^h$ in $(t_{n-1},t_{n})$ we tested different time integration methods. The scheme for the second equation stays always the same and is described in the sections where the different cell models are explained. For the first equation we implemented the simple explicit Euler method and the two step Adams Bashforth method. For the explicit Euler method we have 
\begin{eqnarray*}
V_\text{m}^{h,n} = V_\text{m}^{h,n-1} - \frac{{\vartriangle} t}{C_\text{m}}(I_\text{ion}(V_\text{m}^{h,n-1},w^{h,n-1})-I_\text{ext}).
\end{eqnarray*}
For the twostep Adams Bashforth metod we compute the first two solutions with the explicit Euler method as given in this section and for every further timestep we use the following:
\begin{eqnarray*}
V_\text{m}^{h,n} = V_\text{m}^{h,n-1} - \frac{{\vartriangle} t}{2C_\text{m}}\Big(3(I_\text{ion}(V_\text{m}^{h,n-1},w^{h,n-1})-I_\text{ext}) - (I_\text{ion}(V_\text{m}^{h,n-2},w^{h,n-2})-I_\text{ext})\Big).
\end{eqnarray*}


Numerical results on the mesh in Fig.~\ref{fig:mesh} are presented in Fig.~\ref{fig:depolarization}.


\begin{figure}[H] 
\begin{minipage}[t]{0.4\textwidth}
\centering
  		\includegraphics[width=0.7\textwidth]{images/GeoLevel00}
  		\subcaption{Tetrahedral mesh on level $\ell= 0$.}
  		\label{geoLevel0}
\end{minipage}
\hfill
\begin{minipage}[t]{0.55\textwidth}
\centering
\includegraphics[width=\textwidth]{images/GeoLevel3Zoomed}
  		\subcaption{Tetrahedral mesh on on level $\ell= 3$ (zoomed).}
  		\label{geoLevel1}
\end{minipage}
\caption{Reference geometry approximating ventricles and atria on level $\ell=0$ and with three uniform refinement steps.}
\label{fig:mesh}
\end{figure}

\begin{figure}[H]%[11]{r}{\textwidth}
  \begin{center}
    \noindent\begin{tabular*}{\linewidth}{@{\extracolsep{\fill}}ccccccc@{}}
     \includegraphics[width=.15\textwidth]{images/v0025}
    &\hspace*{-3.5mm}\includegraphics[width=.15\textwidth]{images/v0075}
    &\hspace*{-3.5mm}\includegraphics[width=.15\textwidth]{images/v0200}
    &\hspace*{-3.5mm}\includegraphics[width=.15\textwidth]{images/v0250}
    &\hspace*{-3.5mm}\includegraphics[width=.15\textwidth]{images/v0325}
    &\hspace*{-3.5mm}\includegraphics[width=.15\textwidth]{images/v0700}
    &\hspace*{-3.5mm}\includegraphics[width=.06\textwidth]{images/scale}
      \\
      \small $t=25$ ms &
      \small $t=100$ ms &
      \small $t=200$ ms &
      \small $t=250$ ms &
      \small $t=325$ ms &
      \small $t=700$ ms &
      \small mV
      \\[-3mm]
    \end{tabular*}
  \end{center}
  \caption{Evolution of a depolarization wave on the ventricles and atria:
    In this model, the wave is initiated via external stimuli starting at the sinus node
    located in the wall of the right atrium of the heart
    and then propagates by electrophysiologic mechanisms. The electric potential is computed with
    conforming finite elements, which are coupled with an abstract interface
    to the ODE system describing the electrophysiology of the cell membranes
    at every nodal point. The time stepping is realized by a splitting scheme.
    The system is computed in parallel and requires approximately 8.5 million tetrahedra to faithfully represent a physiologically accurate propagation of the cardiac depolarization wave.}
  \label{fig:depolarization}
\end{figure}
