\renewcommand{\D}{\mathrm{D}}
\renewcommand{\div}{\operatorname{div}}
\newcommand{\tr}{\operatorname{trace}}
\newcommand{\dev}{\operatorname{dev}}
\newcommand{\cof}{\operatorname{Cof}}
\newcommand{\R}{\mathbb{R}} % reelle
%\newcommand{\ofxt}{(t,\vec{x})}
%\newcommand{\ofx}{(\vec{x})}
\newcommand{\ofxt}{(t,x)}
\newcommand{\ofx}{(x)}
\providecommand{\deform}[1]{#1^{\vec \varphi}}

\newcommand{\pT}{\deform{\vec T}}
\newcommand{\fPiolaF}{\hat{\vec T}(\fF )}
\newcommand{\fPiolaC}{\tilde{\vec T}(\fC )}
\newcommand{\fPiolaE}{\wedge{\vec T}(\fE )}
\newcommand{\sPiolaF}{\hat{\vec \Sigma}(\fF )}
\newcommand{\sPiolaC}{\tilde{\vec \Sigma}(\fC )}
\newcommand{\sPiolaE}{\wedge{\vec \Sigma}(\fE )}

\newcommand{\fphi}{\vec \varphi}
\newcommand{\fF}{\vec F}
\newcommand{\fC}{\vec C}
\newcommand{\fE}{\vec E}
\newcommand{\fT}{\vec T}
\newcommand{\fu}{\vec u}
\newcommand{\fx}{\vec x}


\newcommand{\ff}{\vec f}
\newcommand{\fs}{\vec s}
\newcommand{\fn}{\vec n}
\newcommand{\ffz}{\vec f}
\newcommand{\fsz}{\vec s}
\newcommand{\fnz}{\vec n}

\section{A model for cardiac electromechanics}\label{sec:mechanics}

We consider the human heart to be a bounded reference domain $\Omega$, which is
deformed into the current configuration
by a \textit{deformation} $\vec \varphi \colon [0,T]\times\Omega
\longrightarrow\R^3$ during one heartbeat ($T\approx
1s$). We set $\Omega_t = \vec\varphi(t,\Omega)$.

The deformation $\fphi$ is computed from the \textit{displacement}
$\vec u \colon [0,T]\times\Omega\longrightarrow \R^3$ by 
    \[
    \fphi\ofxt = \vec x + \vec u\ofxt \,,
    \]
the elastic response depends on the \textit{deformation gradient}
    \[
    \fF \ofxt :=
    \D\fphi\ofxt = \left(\frac{\partial}{\partial x_i}\varphi_j\ofxt\right)_{i,j=1,\dots ,3}
    \,.
    \]
From the separation of $\fphi$, we get $\fF = \vec I + \D\fu$, and we set $J=\det (\fF )$. 

We use the balance equation in the \textit{Lagrange configuration}
    \begin{subequations}
      \label{eq:motionlagr}
    \begin{align}
      \varrho_0 \partial^2_t\fu&=\div \vec P
    \end{align}
with reference density $\varrho_0$ and the stress $\vec P$ depending
on $\vec F$ and an active strain or active stress model. This is
complemented by boundary conditions on $\partial\Omega =
\Gamma_\text{D} \cup \Gamma_\text{P} \cup \Gamma_\text{LV}\cup
\Gamma_\text{RV}\cup\Gamma_\text{LA}\cup \Gamma_\text{RA}$ with
Dirichtlet boundary $\Gamma_\text{D}$, pericardium $\Gamma_\text{P}
=\partial\Omega_\text{T} \cap \partial\Omega_\text{V}$, and boundaries
$\Gamma_\text{LV} = \partial\Omega_\text{V}\cap
\partial\Omega_\text{LV}$, $\Gamma_\text{RV} =
\partial\Omega_\text{V}\cap \partial\Omega_\text{RV}$,
$\Gamma_\text{LA} = \partial\Omega_\text{A}\cap
\partial\Omega_\text{LA}$, $\Gamma_\text{LA} =
\partial\Omega_\text{A}\cap \partial\Omega_\text{LA}$ of the left and
right ventrical and atria chambers
$\Omega_\text{LV},\Omega_\text{RV},\Omega_\text{LA},\Omega_\text{RA}$,
see Fig.~\ref{fig:contraction}.

We use the boundary conditions
    \begin{align}
      \vec \varphi &= \vec 0\,,\qquad \text{ on } \Gamma_\text{D}\,,
      \\
      [\vec \varphi] &= \vec 0\,,\qquad \text{ on } \Gamma_\text{P}\,,
      \\
      \vec P \vec n &= - p_\text{LV}\vec n\,,\qquad \text{ on } \Gamma_\text{LV}\,,
      \\
      \vec P \vec n &= - p_\text{RV}\vec n\,,\qquad \text{ on } \Gamma_\text{RV}\,,
      \\
      \vec P \vec n &= - p_\text{LA}\vec n\,,\qquad \text{ on } \Gamma_\text{LA}\,,
      \\
      \vec P \vec n &= - p_\text{RA}\vec n\,,\qquad \text{ on } \Gamma_\text{RA}\,.
    \end{align}
    \end{subequations}

\begin{figure} 
\centering
\includegraphics[width=\textwidth]{images/contraction}
\caption{Reference geometry approximating ventricles and atria on level $\ell=0$ and with three uniform refinement steps.}
\label{fig:contraction}
\end{figure}
    
\Paragraph{Hyperelasticity}

We assume cardiovascular tissue to be hyperelastic, i.e., there exists
a stored energy function $W= W(\fF )$ which defines the stress response
(in case of no active forces) by
     \[
     \vec P = \D W(\fF )\,.
     \]
Since the material is frame-indifferent, $\tilde W$ exists such that
$W(\vec F) = \tilde{W}(\fC)$, where $\fC = \fF^\top \fF$ is left
Cauchy Green strain tensor.  Alternatively, the energy functional is
given in the form $W(\vec F) = \check{W}(\fE)$ for $\fE = \frac12(\fC
- \vec I)$, or $W(\vec F) = \mathring{W}(\vec \iota_{\fC})$ depending on invariants
$\vec \iota_{\fC}$ of the strain tensor.

\Paragraph{Anisotropy}

In isotropic materials, the hyperelastic energy only depends on the \textit{invariants} of $\fC$ 
     \[
     \iota_1(\fC) = \tr (\fC)\,,\qquad
     \iota_2(\fC)=\tr \left( \operatorname{Cof}(\fC )\right)\,, \qquad
     \iota_3(\fC )=\det (\fC ) = J^2\,.
     \]
The cardiac tissue is stronlgy anisotropic.
Depending on the fiber direction and the normal direction in the reference domain
     \begin{align*}
       \vec f &\colon \Omega \longrightarrow S^2\,,\\
       \vec n &\colon \Omega \longrightarrow S^2
     \end{align*}
with $\vec n \cdot \vec f = 0$ we define $\vec s = \vec f \times \vec n$.
This defines the \textit{anisotropic invariants}
     \begin{eqnarray*}
       \iota_{4,\vec f}(\fC ) &=& \vec f\cdot\fC\vec f \,,\\
       \iota_{4,\vec n}(\fC ) &=& \vec n\cdot\fC\vec n \,,\\
       \iota_{4,\vec s}(\fC ) &=& \vec s\cdot\fC\vec s \,,\\
%       \iota_{5,\vec f}(\fC ) &=& \vec f\cdot\fC^2\vec f \,,\\
%       \iota_{5,\vec n}(\fC ) &=& \vec n\cdot\fC^2\vec n \,,\\
%       \iota_{5,\vec s}(\fC ) &=& \vec s\cdot\fC^2\vec s \,,\\
       \iota_{8,\ff\fs}(\fC) &=& \big|\vec f\cdot\fC\vec s\big| \,.
     \end{eqnarray*}

\Paragraph{Constitutive models}
     
With the representation above, we now specify the hyperelastic energy
For cardiac tissue, several models are proposed to imitate its
viscoelastic behaviour. Commonly used models are:
    \begin{enumerate}
    \item \textbf{Neo-Hooke model.} One of the simplest non-linear material models is given by
      \begin{align*}
	\tilde{W}(\fC ) =& \frac{\mu}{2}(\tr (\fC )-3)
        + \Gamma_{\lambda}(J)
        \,,\qquad
        \Gamma_{\lambda}(J) = \frac{\lambda}2 (J-1)^2
      \end{align*}
      with parameters $\mu$ and $\lambda$.
      Though it is easy to implement, it does not capture the anisotropy of cardiac tissue.
    \item \textbf{Guccione model.} 
    An early contribution to anisotropic material models was given in~\cite{guccionemat}, defining the \textit{Guccione material}
      \begin{align*}
	\check{W}(\fE ) =& \frac{C_0}{2}\exp \left[ Q_{\ff ,\fs}(\fE )-1 \right]
        + \Gamma_{\lambda}(J)
      \end{align*}
      with parameters $C_0$, $\mu$ and $\lambda$ and $Q_{\ff ,\fs}(\fE )$ defining the functional
      \[ Q_{\ff ,\fs}(\fE ) := b_1\overline{E}_{11}^2 + b_2(\overline{E}_{22}^2+\overline{E}_{33}^2+\overline{E}_{23}^2+\overline{E}_{32}^2)+b_3(\overline{E}_{12}^2+\overline{E}_{21}^2+\overline{E}_{13}^2+\overline{E}_{31}^2)\, ,\]
      where 
      \[  \bar{E} = \ff\cdot \vec E \ff +  \fs\cdot \vec E \fs+ \fn\cdot \vec E \fn\]
      % \[  \bar{E} = \vec Q(\ff ,\fs )^\top\vec E \vec Q(\ff ,\fs )
      % \, ,\qquad \vec Q(\ff ,\fs ) = \ff \otimes\ff + \fs\otimes\fs + \fn\otimes\fn \]
      is the Green strain tensor oriented along the fibre direction.
      The parameters $b_1,b_2,b_3$ are determined by experiments.
    \item \textbf{Holzapfel-Ogden model.} We
      use the general energy proposed by~\cite{modelpassivemyo}
      defining a \textit{Holzapfel-Ogden material}:
      \begin{align*}
	\mathring{W}(\vec \iota_{\fC})
        &= \frac{a}{2b}\left[\exp\left(b(\iota_1(\fC )-3)\right)-1\right]
        +\frac{a_{\ff}}{2b_{\ff}}\left[\exp\left(b_{\ff}(\iota_{4,\vec f }(\fC )-1)^2\right) -1\right]
        \\
	&\qquad
        +\frac{a_{\fs}}{2b_{\fs}}\left[\exp\left(b_{\fs}(\iota_{4,\vec s }(\fC )-1)^2\right) -1\right]
	+ \frac{a_{\ff\fs}}{2b_{\ff\fs}}\left[ \exp\left(b_{\ff\fs}\iota_{8,\ff\fs}(\fC )^2\right)-1\right]
        + \Gamma_{\lambda}(J)
      \end{align*}
      with parameters
      $a,b,a_{\ff},b_{\ff},a_{\fs},b_{\fs},a_{\ff\fs},b_{\ff\fs}$
      determined by physical experiments.
    \end{enumerate}  

\input{ActiveStrain}

\input{ActiveStress}

\Paragraph{The Newmark method}

Now that we know how to properly calculate $\vec P (\D\vec{\varphi} ,
w)$, a proper time-integration strategy is needed to solve the dynamic
system~\eqref{eq:motionlagr}. We implement a \textit{Newmark
  $\beta$-scheme}~\cite{NonlinFE_SaS}, where at each time step $n$ we
solve for $\fu (t_n, \vec x ) = \vec u^n$, approximating
$\partial^2_t\fu \approx \vec a^n$ with
\begin{subequations}
	\begin{align}
	  \vec u^n &= \fu^{n-1} + {\vartriangle} t\vec v^{n-1}
          + ({\vartriangle} t)^2\left( \frac{1-2\beta_\text{N}}{2}\vec a^{n-1}
          + \beta_\text{N} \vec a^n\right) ,\label{newmarkappu}\\	
	  \vec v^n &= \vec v^{n-1} + {\vartriangle} t\left( (1-\gamma_\text{N})\vec a^{n-1}
          + \gamma_\text{N} \vec a^n\right) \label{newmarkappv}
	\end{align}
\end{subequations}
and $\vec a^0 =\vec 0$. The approximated dynamic system is the given by
     \begin{equation}\label{eq:newmarksystem}
       \varrho_0\vec a^n - \div \vec P\big(\vec I+ \mathrm D\vec u^n\big) =0 \,.
     \end{equation}
Solving for $\vec a^n$ in \eqref{newmarkappu} and replacing $\vec a^n$ and
$\vec v^n$ in \eqref{eq:newmarksystem} gives us
    \begin{equation}
      \varrho_0\left[\frac{1}{\beta_\text{N}{\vartriangle}t}
        \left( \frac{1}{{\vartriangle}t}\left(\vec u^n-\vec u^{n-1}\right)-\vec{v}^{n-1}\right)
        -\frac{1-2\beta_\text{N}}{2\beta_\text{N}}\vec{a}^{n-1}\right]
      - \div \vec P\big(\vec I + \mathrm D\vec u^n\big)
      = \vec 0
      \,.
    \end{equation}
The standard Newmark parameters are $\beta_\text{N}= 0.25$ and $\gamma_\text{N}= 0.5$.
As these values dictate the damping of the system, further combinations are being tested in our experiments.

\Paragraph{The ventricle volume}

The reference domains for each chamber $C$ are given by
$\Omega_\text{LV}$, $\Omega_\text{RV}$, $\Omega_\text{LA}$,
$\Omega_\text{RA}$.  The volume of the deformed chambers are then
denoted by $|\Omega_\text{LV}^{\vec \varphi}|,|\Omega_\text{RV}^{\vec \varphi}|,
|\Omega_\text{LA}^{\vec \varphi}|,|\Omega_\text{RA}^{\vec \varphi}|$, and would be computed by
    \begin{equation*}
      |\Omega_C^{\vec \varphi}|
      = \int_{\Omega_C^{\vec \varphi}} 1\mathrm{d}\vec x \, ,\qquad  C \in\{\text{LV, RV, LA, RA}\}
      \,.
    \end{equation*}
On the discrete geometry however, the chamber volumes are evaluated
by the sum of volumes of all tetrahedrons constructed by a surface
triangle $K\subset\partial\Omega_C$ and the center point $\vec
x_C$. By denoting the vectors of the vertices of $K$ by $\vec a_K$,
$\vec b_K$, $\vec c_K$, we obtain
    \begin{equation*}
      |\Omega_C^{\vec \varphi}|
      \approx  \sum_{K\subset\partial\Omega_C}
      \frac{1}{6} (\vec a_K - \vec x_C ) \cdot \big( (\vec b_K - \vec a_K )
      \times ( \vec c_K - \vec a_K) \big)
      \,.
    \end{equation*}



