\section{The monodomain equation}
\label{sec:monodomain}

In the first step, we only consider the monodomain model in the
reference domain without deformation, which describes the
electrophysiology independently in the ventricles and atria.

Therefore, let $\Omega_\text{M}=\Omega_\text{V}$ or
$\Omega_\text{M}=\Omega_\text{A}$ be the domain and let
$(I_\text{ion},G_w)$ the corresponding cell model
in the ventricles and atria, respectively.
The cell model is described by the
transmembrane current density $I_\text{ion}$ and the evolution of ion concentrations and
gating variable determined by~$G_w$. The ten Tusscher cell
model for the ventricles is described in Sect.~\ref{sec:TenTusscher},
and the Courtemanche cell model in Sect.~\ref{sec:Courtemanche}.

In this section we only consider the electrophysiology on the fixed reference geometry,
i.e., we have $ \vec \varphi = \operatorname{id}$ and $\gamma_{\vec f} = 0$.

\Paragraph{The monodomain data}

Depending on the fiber direction in the reference domain
     \begin{eqnarray*}
       \vec f \colon \Omega_\text{M} \longrightarrow S^2\,,
     \end{eqnarray*}
the conductivity tensor is given by
    \begin{eqnarray*}
      \vec \sigma(\vec f) = \sigma_\text{l} \,\vec f \otimes \vec f +
      \sigma_\text{t} \big(\vec I - \vec f \otimes \vec f\big)
      \in \mathbb R^{3\times 3}_\text{sym}
     \end{eqnarray*}
with conductivity parameters $\sigma_\text{l}\geq 0$ in longitudinal and
$\sigma_\text{t} \geq 0$ in transversal direction.
%We have $y^\top\sigma(x)y \geq \sigma_0 y^\top y$ with $\sigma_0 >0$, i.e.,
%the conductivity tensor is uniformly positive definite.

In our model we use a prescribed pacing mechanism,
the depolarization waves are initiated by an external stimulus
$I_\text{ext}\in \mathrm L_2\big((0,T);\mathrm L_2(\Omega)\big)$ modeling the activation
of the electric potential at the Purkinje Muscle Junctions in the ventricles and the sinus node in the atria. Here we define
    \begin{eqnarray}
      \label{eq:I_ext}
      I_\text{ext}(t,x) =
      \begin{cases}
        A_j & t\in (t_j,t_j+\tau_j)\,,\ x\in \mathcal N^h_\text{ext}\\
        0   & \text{else}
      \end{cases}
    \end{eqnarray}
at a subset of the finite element nodal points
$\mathcal N^h_\text{ext}\subset \mathcal N^h \subset \bar\Omega_\text{M}$, 
depending on the starting time $t_j\geq 0$, duration $\tau_j>0$, and amplitude $A_j>0$.
The values at the nodal points $z\in \mathcal N^h$ are interpolated by the
nodal basis functions $\phi_z$, which defines $I_\text{ext}\in \mathrm L_2(0,T;\Omega_\text{M})$
by $ I_\text{ext}(t,x) = \sum_{z\in \mathcal N_h}  I_\text{ext}(t,z) \phi_z(x)$.
Nevertheless, in the approximation only the values at the nodal points
are used in the numerical scheme, cf.~%Sect.~\ref{sec:splitting}.
Fig.~\ref{fig:Purkinje}.\\
A second possibility to define the external stimulus is the use of a sigmoid function instead of the step function defined above. Therefor  we use the values  $A_j$, $t_j$, $\tau_j$ and additionaly a scaling factor for the steepness of the sigmoid function denoted by $s_j$. 
Then we  define
\begin{eqnarray}
      \label{eq:I_ext_smoothed}
      I_\text{ext}(t,x) =
      \frac{A_j}{2}\Big(  \big( 1+\tanh(s_j(t-t_j)) \big) - \big(1+\tanh(s_j(t-(t_j+\tau_j)))  \big)\Big)
    \end{eqnarray}
for all nodal points $x \in \mathcal N^h $. For all nodal points $x \notin \mathcal N^h_\text{ext}$ we set 
$A_j =0$ such that $I_\text{ext}(t,x)=0 ~\forall t $.


\begin{figure}[thb] 
  \includegraphics[width=0.4\textwidth]{images/PurkinjeTree}
  \caption{Illustration of the Purkinje fibers and the stimulus points
    at the end of each branch are shown with their respective
    activation time.  The external stimulus points of the sinus node
    at $t = 0$\,s is shown in green.}
  \label{fig:Purkinje}
\end{figure}

\Paragraph{The monodomain evolution}    

We consider the coupled diffusion-reaction equation in $(0,T)\times \Omega_\text{M}$
for the electric potential $V_\text{m}$ and the vector of gating variables $w$
    \begin{subequations}
    \begin{eqnarray}
      \label{eq:mono1}
      \beta  C_\text{m}
        \partial_t V_\text{m}
          &=&
         \nabla \cdot\vec \sigma
         \nabla V_\text{m}
         -
          \beta
          I_\text{ion}(V_\text{m},w) + I_\text{ext}
            \,,
            \\
            \label{eq:mono2}
            \partial_t w
          &=&
          G_w(w,V_\text{m},\gamma_{\vec f})
     \end{eqnarray}
subject to the initial values at $t=0$ in $\Omega_\text{M}$
     \begin{eqnarray}
        V_\text{m}(0)
        &=&         V_\text{m}^0\,,
        \\
        w(0) 
          &=&
          w^0\,,
     \end{eqnarray}
and homogeneous Neumann boundary conditions on $(0,T) \times \partial\Omega$
     \begin{eqnarray}
       \vec n \cdot \vec \sigma \nabla V_\text{m}
       &=&  0
       \,.
     \end{eqnarray}
    \end{subequations}
