\section{A fully coupled model in cardiac electrophysiolgy}

A mathematical model for depolarization waves and the contraction of the heart
comprises a time-dependent nonlinear PDE system for the electric potential
and the mechanical deformation, coupled with ODEs for ion
concentrations and gating variables, several ODEs for the blood pressure in the cardiac
chambers, and a surrogate model for the interaction with the
circulatory system. Our fully-coupled model describes the following mechanisms:
\begin{itemize}
\item
To initiate a heartbeat, an electric impulse is initiated in the sinus node cells.
From there, the depolarization wave is captured by and propagates through the cardiac tissue.
\item
Intracellular and extracellular electrical potentials,
which influence the opening and closing of transmembrane ion channels and
govern the concentration of ions in the cells.
\item
The increased calcium concentration activates the contraction of myofibrils
in the cells, which results in a contraction of the heart muscle.
\item
The contraction of the heart muscle increases the pressure in the chambers,
which results in the blood flow.
\end{itemize}
For a qualitatively correct and sufficiently accurate computational model,
we numerically realize the following coupled electromechanical system, determined by
\begin{itemize}
\item
a reference domain $\Omega\subset \mathbb R^3$ consisting of
subdomains $\Omega_\text{V}$ for the ventricles, $\Omega_\text{A}$ for the atria,
and $\Omega_\text{T}$ for surrounding tissue;
\item
the electric potential $V_\text{m}\in \mathrm C^1([0,T],\mathrm L_2(\Omega)\cap
\mathrm C^0\big([0,T],\mathrm H^1(\Omega_\text{V}\cup \Omega_\text{A})\big)$;
\item
a vector of ion concentrations and gating variables $w\in
\mathrm C^1\big([0,T],\mathrm L_q(\Omega_\text{V}\cup \Omega_\text{A};\mathbb R^N)\big)$;
\item
the deformation vector
$\vec \varphi\in \mathrm C^2\big([0,T],\mathrm L_2(\Omega; \mathbb R^3)\big)
\cap
\mathrm C^0\big([0,T],\mathrm W^{1,p}(\Omega;\mathbb R^3)\big)$;
\item
the stretch along the fibre and a vector of state variables describing the 
force developement $(\gamma_{\vec f},g)\in
\mathrm C^1\big([0,T],\mathrm L_q(\Omega_\text{V}\cup \Omega_\text{A};\mathbb R^{1+K})\big)$;
\item
the blood pressure in the cardiac chambers $p\in
\mathrm C^1([0,T],\mathbb R^4)$; 
\item
a vector of variables $z\in
\mathrm C^1([0,T],\mathbb R^M)$ modeling
the circulatory system
\end{itemize}
and solving the coupled PDE -- ODE system
    \begin{subequations}
    \begin{align}
      \label{eq:mono}
        \beta  C_\text{m}
        \partial_t^{\vec \varphi} V_\text{m}
          &=
         \nabla^{\vec \varphi}\cdot \vec \sigma(\vec f^{\vec \varphi})
         \nabla^{\vec \varphi}V_\text{m}
         -
          \beta
          I_\text{ion}(V_\text{m},w,\gamma_{\vec f}) + I_\text{ext}
            \,,
            \\
                  \label{eq:cell}
            \partial_t^{\vec \varphi} w
          &=
            G_w(w,V_\text{m},\gamma_{\vec f})
            \,,
            \\
            \rho\partial_t^2 \vec \varphi
          &=
            \div \vec P\big(\mathrm D\vec \varphi,\vec f^{\vec \varphi},\gamma_{\vec f}\big)
            \,,
            \\
            \partial_t^{\vec \varphi} \gamma_{\vec f}
            &= G_{\gamma_{\vec f}}(\gamma_{\vec f},\mathrm D\vec \varphi,\vec f^{\vec \varphi} ,g,w)
            \,,
            \\
            \partial_t^{\vec \varphi}  g
            &=
            G_g(g,\gamma_{\vec f},\mathrm D\vec\varphi,w)
            \,,
            \\
            \label{eq:circ1}
            \partial_t p
          &=
            G_p(p,z)
            \,,
            \\
            \label{eq:circ2}
            \partial_t z
          &=
            G_z(z,p)
            \,,
            \\
          0 &= G_\text{C}(z,|\Omega_\text{VL}^{\vec \varphi}|,|\Omega_\text{VR}^{\vec \varphi}|,|\Omega_\text{AL}^{\vec \varphi}|,|\Omega_\text{AR}^{\vec \varphi}|)  
      \end{align}
    \end{subequations}
with total derivative in time
$\partial_t^{\vec \varphi} w = \partial_t w
\!+\! \nabla^{\vec \varphi}\! \cdot\! (w \partial_t \vec \varphi)$ and the covariant
gradient $\nabla^{\vec \varphi}$, and where
\begin{itemize}
\item 
  the equations \eqref{eq:mono} and \eqref{eq:cell} couples the parabolic equation of the
  potential with the ODE system describing the cell model
  which determined by $(I_\text{ion},G_w)$, where the 
  ion density $(I_\text{ion}$ and the gating mechanism $G_w$ are specified in the next sections
  for the different models;  
\item
  the depolarization wave is initiated by an external impulse
  $I_\text{ext}$ located at predefined nodes or natural cellular
  pacemakers with $(I_\text{ion},G_w)$ not exhibiting a stable resting
  state;
\item
  the electric potential is determined by the monodomain equation and
  the electric conductivity $\vec \sigma(\vec f^{\vec \varphi})$
  depending on the fiber direction $\vec f^{\vec \varphi}$ in the deformed configuration;
\item
  the deformation is activated by a strain $\vec F_\text{a}(\vec
  f^{\vec \varphi},\gamma_{\vec f})$ or a stress $\vec P_\text{a}(\vec
  f^{\vec \varphi},\gamma_{\vec f})$ along the fiber direction $\vec
  f^{\vec \varphi}$, i.e., $\vec P = \mathrm D W\big(\mathrm D\vec
  \varphi\vec F_\text{a}(\vec f^{\vec \varphi},\gamma_{\vec f})^{-1}
  \big)$ or $\vec P = \mathrm D W\big(\mathrm D\vec \varphi) + \vec
  P_\text{a}(\vec f^{\vec \varphi},g)$.
  %The local volume change is determined by 
  %$J = \det \mathrm D\vec \varphi)$.
  The tension development is modeled by a system of
  ODEs determined by $(G_{\gamma_{\vec f}},G_g)$ depending on
  the deformation gradient $\mathrm D\vec \varphi$ and
  the vector of ion concentrations and gating variables~$w$;
\item
  the interaction with the circulatory system
  is determined by the evolution \eqref{eq:circ1} of the pressure $p$
  described by $G_p$ depending on the parameters $z$ of the 
  circulatory system subject to the constraint \eqref{eq:circ2} that
  the volume in the chambers
  $|\Omega_\text{VL}^{\vec \varphi}|$, $|\Omega_\text{VR}^{\vec \varphi}|$,
  $|\Omega_\text{AL}^{\vec \varphi}|$,
  $|\Omega_\text{AR}^{\vec \varphi}|$
  evalutated from the deformation and the 
  circulatory system coincide;  
\item
  in this model where the blood flow is not evaluated explicitly,
  we assume that the pressure is spatially constant in every chamber so the 
  the circulatory system
  couples to the deformation by $\vec P \vec n = p \vec n$~on~the inner boundary;
\item
  we assume $\vec n\cdot
  [\vec \varphi] = 0$ along the pericardium.
\end{itemize}
The equations are complemented by initial values and boundary conditions.

The basic mathematical aspects of this electromechanical system are
well understood as recently reviewed
\cite{cherry2017introduction,franzone2014mathematical,quarteroni2017integrated}.
First results on the analysis of stability and existence of the
solution for the linearized electromechanical system are given in
\cite{andreianov2015solvability}. The extension to nonlinear
elastic models is more involved; a nonlinear analysis of a very
specific model for cardiac electromechanical activity is considered
in~\cite{pathmanathan2013existence}.  A detailed numerical
investigation of bioelectrical effects of mechano-electrical feedback
is included in~\cite{colli2017effects}. More specific
considerations on fast parallel solvers for the solution of the
discretized coupled system based on domain decomposition and multigrid
techniques are considered
in~\cite{augustin2016anatomically,franzone2015parallel,pavarino2017scalable,santiago2018fully}.
