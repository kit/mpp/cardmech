\section{The circulartory system}
\label{sec:circulartory}

The heart interacts with the human vascular system, see 
\cite{quarteroni2019mathematical} for the  
mathematical modelling of the human cardiovascular system, 
\cite{barbarotta2018transmurally} for a specific activation model for ventricular contraction,
\cite{fedele2017patient} for an aortic valve model, and \cite{tagliabue2017complex}
  for an idealized blood flow model in the ventricle.
  
Here we use a lumped parameter model of the
closed-loop human vascular system 
to include more realistic hemodynamic boundary conditions in our model
of cardiac mechanics, 
This model is driven by the volume changes of the four chambers
$|\Omega_\text{VL}^{\vec \varphi}|,|\Omega_\text{VR}^{\vec \varphi}|,
|\Omega_\text{AL}^{\vec \varphi}|,|\Omega_\text{AR}^{\vec \varphi}|$, where
the deformation $\vec\varphi$ depends on the 
pressure values of the right/left ventricular and right/left atrial chambers
    \[
    p = \big(p_\text{VR}, p_\text{VL}, p_\text{VR}, p_\text{VL}\big)
    \,.
    \]
The circulatory system is determines
    \[
    z = \big(v_\text{SysVen}, v_\text{SysArt}, v_\text{PulVen}, v_\text{PulArt},
    v_\text{VR}, v_\text{VL}, v_\text{VR}, v_\text{VL} \big)
    \]
describing the systemic venous, systemic arterial, pulmonary venous, pulmonary
arterial, right/left ventricular, and right/left atrial volumes,
and depending on the parameters 
$R_\text{SysArtValve}$,
$R_\text{SysArt}$,
$C_\text{SysArt}$,
$v_\text{SysArtUnstr}$,
$R_\text{SysPer}$,
$R_\text{SysVen}$,
$C_\text{SysVen}$,
$v_\text{SysVenUnstr}$,
$R_\text{RavValve}$,
$R_\text{PulArtValve}$,
$R_\text{PulArt}$,
$C_\text{PulArt}$,
$v_\text{PulArtUnstr}$,
$R_\text{PulPer}$,
$R_\text{PulVen}$,
$C_\text{PulVen}$,
$v_\text{PulVenUnstr}$, and
$R_\text{LavValve}$.

\begin{figure}[H] 
  \includegraphics[width=100mm]{images/CircWholeHeart}
  \caption{Schematic of the circulatory system model with the pressure values $p$,
    resistances $R$, and compliances $C$.}
  \label{fig:CircModel}
\end{figure}

In this closed loop system, the evolution of $z$ is determined by
\begin{align*}
\partial_t v_\text{VL} &= 
\max \left\lbrace\frac{p_\text{AL}- p_\text{VL}}{R_\text{LavValve}},0 \right\rbrace - \max \left\lbrace \frac{p_\text{VL} - p_\text{CSysArt}}{R_\text{SysArtValve} + R_\text{SysArt}},0 \right\rbrace \,,
\\
\partial_t v_\text{SysVen} &= 
\frac{p_\text{CSysArt} - p_\text{SysVen}}{R_\text{SysPer}} - \frac{p_\text{SysVen} - p_\text{VR}}{R_\text{SysVen}} \,,
\\
\partial_t v_\text{VR} &= 
\max \left\lbrace\frac{p_\text{VR}- p_\text{VR}}{R_\text{RavValve}},0 \right\rbrace - \max \left\lbrace \frac{p_\text{VR} - p_\text{CPulArt}}{R_\text{PulArtValve} + R_\text{PulArt}},0 \right\rbrace \,,
\\
\partial_t v_\text{PulVen} &= 
\frac{p_\text{CPulArt} - p_\text{PulVen}}{R_\text{PulPer}} - \frac{p_\text{PulVen} - p_\text{VL}}{R_\text{PulVen}} \,,
\\
\partial_t v_\text{SysArt} &= 
\max \left\lbrace \frac{p_\text{VL} - p_\text{CSysArt}}{R_\text{SysArtValve} + R_		\text{SysArt}},0 \right\rbrace - \frac{p_\text{CSysArt} - p_\text{SysVen}}{R_\text{SysPer}} \,,
\\
\partial_t v_\text{VR} &= 
\frac{p_\text{SysVen} - p_\text{VR}}{R_\text{SysVen}} - \max \left\lbrace\frac{p_\text{VR}- p_\text{VR}}{R_\text{RavValve}},0 \right\rbrace \,,
\\
\partial_t v_\text{PulArt} &= 
\max \left\lbrace \frac{p_\text{VR} - p_\text{CPulArt}}{R_\text{PulArtValve} + R_\text{PulArt}},0 \right\rbrace - \frac{p_\text{CPulArt} - p_\text{PulVen}}{R_\text{PulPer}} \,,
\\
\partial_t v_\text{AL} &= 
\frac{p_\text{PulVen} - p_\text{VL}}{R_\text{PulVen}} - \max \left\lbrace\frac{p_\text{AL}- p_\text{VL}}{R_\text{LavValve}},0 \right\rbrace 
\end{align*}
subject to the relations for the pressure values
    \[
     \big(p_\text{SysVen}, p_\text{SysArt}, p_\text{PulVen}, p_\text{PulArt} \big)
    \]
for the systemic venous, systemic arterial, pulmonary venous,
and pulmonary arterial pressures given by
\begin{align*}
p_\text{CSysArt} &= 
\frac{v_\text{SysArt}}{C_\text{SysArt}} \,, \qquad 
p_\text{SysVen} = 
\frac{v_\text{SysVen}}{C_\text{SysVen}} \,,
\\
p_\text{SysArt} &=
p_\text{VL} - R_\text{SysArtValve} \cdot \max \left\lbrace \frac{p_\text{VL} - p_\text{CSysArt}}{R_\text{SysArtValve} + R_\text{SysArt}},0 \right\rbrace \,,
\\
p_\text{CPulArt} &= 
\frac{v_\text{PulArt}}{C_\text{PulArt}} \,, \qquad
p_\text{PulVen} =
\frac{v_\text{PulVen}}{C_\text{PulVen}} \,,
\\
p_\text{PulArt} &=
p_\text{VR} - R_\text{PulArtValve} \cdot \max \left\lbrace \frac{p_\text{VR} - p_\text{CPulArt}}{R_\text{PulArtValve} + R_\text{PulArt}},0 \right\rbrace 
\end{align*}
and subject to the constraints
\begin{align}
  \label{eq:v}
  v_\text{VL} = |\Omega_\text{VL}^{\vec \varphi}|\,,\qquad
  v_\text{VR} = |\Omega_\text{VR}^{\vec \varphi}|\,,\qquad
  v_\text{AL} = |\Omega_\text{AL}^{\vec \varphi}|\,,\qquad
  v_\text{AR} = |\Omega_\text{AR}^{\vec \varphi}|
   \,,
\end{align}
where the deformation $\vec \varphi$ depends on 
$p = \big(p_\text{VR}, p_\text{VL}, p_\text{VR}, p_\text{VL}\big)$.

%% The coupling between this lumped parameter model and the finite element model is realized by minimizing the residual
%% \begin{equation*}
%% \vec r (\vec p ) = \vec v_\text{FEM} ( \vec p ) - \vec v_\text{CIRC} ( \vec p ) ~.
%% \end{equation*}
%% using the recurrence formula
%% \begin{equation*}
%% \vec p_{i+1} = \vec p_i - \vec J_i^{-1} \vec r (\vec p_i )
%% \end{equation*}
%% where $\vec J = \partial_{\vec p } \vec r (\vec p )$ is the Jacobian matrix, $\vec p$ is a vector containing the chamber pressures $p_\text{VR}, p_\text{VL}, p_\text{VR}, p_\text{AL}$, and $\vec v_\text{FEM}$ and $\vec v_\text{CIRC}$ are vectors containing the chamber volumes $v_\text{VR}$, $v_\text{VL}$, $v_\text{VR}$, $v_\text{AL}$ for the finite element model (FEM) and the circulatory system (CIRC) model respectiely.

\Paragraph{The incremental realization}
Withing one time step of the mechanical problem, we first find an approximated pressure $p$ through the closed-loop model. 
We either use a multistep method using previous pressure values, or simply add a fixed amount in each chamber during the first initialization steps.

Next, we update the mechanical and circulatory models and compare the resulting chamber volumes if the residuals $r_i=|\Omega_i^{\vec \varphi}|-v_i$ for $i\in\{\text{VL, VR, AL, AR}\}$ are below a threshold $\varepsilon_p$.
If they are, the pressure $p$ is accepted and one moves to the next time step. 
Otherwise, we update $p$ by a quasi Newton method:
\begin{equation}
	p^n = p^{n-1} - \vec C_n^{-1}r^n \, ,
\end{equation}
where $\vec C_n$ is the \textit{compliance matrix} determined by
\begin{equation}
	\vec C_n^{-1} = \vec C_{n-1}^{-1} + \left(\vartriangle p_n-\vec C_{n-1}^{-1}\vartriangle r_n\right) \frac{\vartriangle p_n^\top\vec C_{n-1}^{-1}}{\vartriangle p_n^\top\vec C_{n-1}^{-1}\vartriangle r_n}
\end{equation} 
with $\vartriangle p_n = p_n-p_{n-1}$, $\vartriangle r_n = r_n-r_{n-1}$.
