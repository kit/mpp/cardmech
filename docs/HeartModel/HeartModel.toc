\contentsline {section}{\tocsection {}{1}{Introduction}}{1}
\contentsline {paragraph}{\tocparagraph {}{}{\em Remark}}{1}
\contentsline {section}{\tocsection {}{2}{A fully coupled model in cardiac electrophysiolgy}}{3}
\contentsline {section}{\tocsection {}{3}{The monodomain equation}}{4}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The monodomain data}}{4}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The monodomain evolution}}{5}
\contentsline {section}{\tocsection {}{4}{A splitting method for the monodomain equation}}{5}
\contentsline {paragraph}{\tocparagraph {}{}{\bf Time integration for the ODE system}}{6}
\contentsline {section}{\tocsection {}{5}{The ten Tusscher cell model}}{7}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The evolution of ion concentrations}}{9}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The gating mechanism}}{10}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The gating evolution in the ten Tusscher model}}{10}
\contentsline {paragraph}{\tocparagraph {}{}{\bf Computing $\alpha (V_\text {m})$ and $\beta (V_\text {m})$}}{12}
\contentsline {section}{\tocsection {}{6}{The Courtemanche cell model}}{12}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The evolution of ion concentration}}{13}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The gating evolution in the Courtemanche model}}{14}
\contentsline {section}{\tocsection {}{7}{A model for cardiac electromechanics}}{16}
\contentsline {paragraph}{\tocparagraph {}{}{\bf Hyperelasticity}}{17}
\contentsline {paragraph}{\tocparagraph {}{}{\bf Anisotropy}}{17}
\contentsline {paragraph}{\tocparagraph {}{}{\bf Constitutive models}}{18}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The active strain decomposition}}{19}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The active stress decomposition}}{19}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The Newmark method}}{19}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The ventricle volume}}{20}
\contentsline {section}{\tocsection {}{8}{The cell-contraction model}}{20}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The Stretch Model}}{20}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The Land Tension Cell Model}}{21}
\contentsline {section}{\tocsection {}{9}{The circulartory system}}{22}
\contentsline {paragraph}{\tocparagraph {}{}{\bf The incremental realization}}{24}
\contentsline {section}{\tocsection {Appendix}{A}{Monodomain parameters}}{24}
\contentsline {section}{\tocsection {Appendix}{B}{Hyperelastic parameters}}{28}
\contentsline {section}{\tocsection {Appendix}{C}{Circulatory System Parameters}}{29}
\contentsline {section}{\tocsection {Appendix}{}{Acknowledgments}}{29}
\contentsline {section}{\tocsection {Appendix}{}{References}}{29}
