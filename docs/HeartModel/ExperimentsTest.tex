\section{Experiments for Benchmark}
\label{sec:experimentsBM}

We evaluate the accuracy and the efficiency of the splitting approach
presented in Sect.~\ref{sec:splitting} for realistic data and on the
reference geometry given from the benchmark of Nieder.  For our tests we choose a tetrahedral mesh with tree different spacial discretizations $dx = 0.5$mm,$ 0.2$mm,$ 0.1$mm.
The electric potential is activated by an external stimulus
$I_\text{ext}\in \mathrm L_2(0,T;\Omega)$ defined by \eqref{eq:I_ext}.
The positions of the stimulation points $\mathcal N^h_\text{ext}$ in
the cuboid  with space discretization $dx=0.5$mm are illustrated in Fig.~\ref{fig:Cuboid Excitation 0.5}. The duration and the amplitude are set to 
\begin{eqnarray*}
\tau_j &= 0.002\text{s}\\
A_j &= 0.8202 \text{V}
\end{eqnarray*}
\begin{figure}[H] 
\begin{minipage}[t]{0.8\textwidth}
\centering
\includegraphics[width=\textwidth]{images/gsmh-05Excitation3}
\end{minipage}
\begin{minipage}[t]{0.3\textwidth}
\centering
  		\includegraphics[width=0.7\textwidth]{images/gsmh-05Excitation1}
\end{minipage}
\hfill
\begin{minipage}[t]{0.6\textwidth}
\centering
\includegraphics[width=\textwidth]{images/gsmh-05Excitation2}
\end{minipage}
\caption{Reference geometry with excitation for space discretization $dx=0.5$mm.}
\label{fig:Cuboid Excitation 0.5}
\end{figure}

Paragraph{Numerical evaluation of the convergence properties}

We test the convergence of the splitting scheme defined in
Sect.~\ref{sec:splitting} with respect to the mesh level $\ell=0.5,0.2,0.1$,
the time step size ${\vartriangle} t = 0.1\cdot 2^{-j}$ ms, and the
number of substeps $M= 2^m$ defining the step size ${\vartriangle}
t/(2M)$ of time integration of the cell models. The corresponding
approximation of the electric potential is denoted by
$V_\text{m}^{\ell,j,m}$.

The convergence for different discretization parameters is evaluated by the 
the evolution of the electric potential
    $$
    V_\text{m,ref}^{\ell,j,m} =
    \big(V_{\text{m,ref},1}^{\ell,j,m},V_{\text{m,ref},2}^{\ell,j,m},\ldots)\big)
    \in \mathrm L_2(0,T;\mathbb R^K)
    $$
at reference points $\vec x_{\text{ref},k} \in  \bar \Omega_\text{V}$, i.e., 
$V_{\text{m,ref},k}^{\ell,j,m}(t) = V_\text{m}^{\ell,N,M}(t,\vec x_{\text{ref},k})$
for $t\in (0,T)$ and $k=1,...,K$. In our test we use $T=0.4$ s and $K=2$
evaluation points
$x_{\text{ref},1} = (0.0, 0.0, 0.0)$ and 
$x_{\text{ref},2} = (10.12,3.25 ,1.18087 )$.\\
We measure  distances in the $\mathrm L_2$-norm. As we have the solutions only on discrete timesteps $t_0=0, t_i = i*{\vartriangle dt}, t_N = T$ for $i=1, \ldots,N-1$ we define  for two solutions $V_1:=V_{\text{m,ref},1}^{\ell_1,j_1,m_1}$ and $V_2:=V_{\text{m,ref},1}^{\ell_2,j_2,m_2}$
\begin{equation*}
\big\| V_1 - V_2 \big\|_{0,(0,T)} = 1000\sqrt{{\vartriangle dt}\Big(-0.5\big(\big(V_1(t_0) -V_2(t_0)\big)^2+\big((V_1(t_N) -V_2(t_N))^2\big)\big)+ \sum_{i=1}^{N-1} \big(V_1(t_i) -V_2(t_i)\big)^2\Big)}.
\end{equation*}
We multiply by $1000$ to have the error in mV instead of Volt. If $j_1$ and $j_2$ are different, we use for ${\vartriangle dt}$ the smaller value and interpolate the values of the other solution linear between given values. 
 %In the following paragraph we explain the idea of approximating the error with extrapolation.


In the following table \ref{tab: convergence substepping 0.5} we study the influence of the substepping for the cell models. In the colums the timestep size ${\vartriangle dt}$ is fixed. As the timestep size for the cell model is the same going in the diagonal bottom left to top right  the distance of the solutions should stay the same. 
\begin{table}[H]
\centering
\caption{Convergence test for the substepping with dx=0.5mm  and ${\vartriangle} t = 0.1*2^{-j}$ ms.}
\begin{tabular}{c|c|c|c|c} 
     & $j= 0$  & $j= 1$  & $j=2$   & $j=3$\\
\hline
$\big\|V_\text{m,ref,1}^{0.5,j,1}-V_\text{m,ref,1}^{0.5,j,0}\big\|_{0,(0,T)}$ &   3.0718 &   1.5668 &   0.7784 &   0.4122\\ 
$\big\|V_\text{m,ref,1}^{0.5,j,2}-V_\text{m,ref,1}^{0.5,j,1}\big\|_{0,(0,T)}$ &   1.4741 &   0.7932 &   0.4150 &   0.1876\\ 
$\big\|V_\text{m,ref,1}^{0.5,j,3}-V_\text{m,ref,1}^{0.5,j,2}\big\|_{0,(0,T)}$ &   0.7748 &   0.4190 &   0.1896 &   0.0909\\ 
$\big\|V_\text{m,ref,1}^{0.5,j,4}-V_\text{m,ref,1}^{0.5,j,3}\big\|_{0,(0,T)}$ &   0.4150 &   0.1857 &   0.0908 &   0.0482\\ 
\end{tabular} 
 
\bigskip 
 
\begin{tabular}{c|c|c|c|c} 
     & $j= 0$  & $j= 1$  & $j=2$   & $j=3$\\
\hline
$\big\|V_\text{m,ref,9}^{0.5,j,1}-V_\text{m,ref,9}^{0.5,j,0}\big\|_{0,(0,T)}$ &   6.9095 &   3.8541 &   2.1112 &   1.1221\\ 
$\big\|V_\text{m,ref,9}^{0.5,j,2}-V_\text{m,ref,9}^{0.5,j,1}\big\|_{0,(0,T)}$ &   3.9289 &   2.1456 &   1.1192 &   0.5930\\ 
$\big\|V_\text{m,ref,9}^{0.5,j,3}-V_\text{m,ref,9}^{0.5,j,2}\big\|_{0,(0,T)}$ &   2.2044 &   1.1577 &   0.5795 &   0.2848\\ 
$\big\|V_\text{m,ref,9}^{0.5,j,4}-V_\text{m,ref,9}^{0.5,j,3}\big\|_{0,(0,T)}$ &   1.2516 &   0.5808 &   0.2985 &   0.1579\\ 
\end{tabular} 
\label{tab: convergence substepping 0.5}
\end{table}
\begin{table}[H]
\centering
\caption{Convergence test for the timestepping with dx=0.5mm  and ${\vartriangle} t = 0.1*2^{-j}$ ms.}
\begin{tabular}{c|c|c|c|c|c} 
     & $m= 0$  & $m= 1$  & $m=2$   & $m=3$ & $m=4$\\
\hline
$\big\|V_\text{m,ref,1}^{0.5, 1, m }-V_\text{m,ref,1}^{0.5, 0, m }\big\|_{0,(0,T)}$ &   2.8799 &   1.4756 &   0.8833 &   0.6034 &   0.4679\\ 
$\big\|V_\text{m,ref,1}^{0.5, 2, m }-V_\text{m,ref,1}^{0.5, 1, m }\big\|_{0,(0,T)}$ &   1.5704 &   0.7955 &   0.4286 &   0.2171 &   0.1441\\ 
$\big\|V_\text{m,ref,1}^{0.5, 3, m }-V_\text{m,ref,1}^{0.5, 2, m }\big\|_{0,(0,T)}$ &   0.7824 &   0.4176 &   0.1926 &   0.0964 &   0.0570\\ 
\end{tabular} 
 
\bigskip 
 
\begin{tabular}{c|c|c|c|c|c} 
     & $m= 0$  & $m= 1$  & $m=2$   & $m=3$ & $m=4$\\
\hline
$\big\|V_\text{m,ref,9}^{0.5, 1, m }-V_\text{m,ref,9}^{0.5, 0, m }\big\|_{0,(0,T)}$ &   9.2772 &   6.3279 &   4.6316 &   3.6233 &   2.9824\\ 
$\big\|V_\text{m,ref,9}^{0.5, 2, m }-V_\text{m,ref,9}^{0.5, 1, m }\big\|_{0,(0,T)}$ &   4.4865 &   2.7609 &   1.7444 &   1.1725 &   0.8957\\ 
$\big\|V_\text{m,ref,9}^{0.5, 3, m }-V_\text{m,ref,9}^{0.5, 2, m }\big\|_{0,(0,T)}$ &   2.2532 &   1.2663 &   0.7417 &   0.4482 &   0.3087\\ 
\end{tabular}
\label{tab: convergence timestepping 0.5} 
\end{table}



\begin{table}[H]
\centering
\caption{Convergence test for the substepping with dx=0.2mm  and ${\vartriangle} t = 0.1*2^{-j}$ ms.}
\begin{tabular}{c|c|c|c|c} 
     & $j= 0$  & $j= 1$  & $j=2$   & $j=3$\\
\hline
$\big\|V_\text{m,ref,1}^{0.2,j,1}-V_\text{m,ref,1}^{0.2,j,0}\big\|_{0,(0,T)}$ &   3.0237 &   1.5456 &   0.7746 &   0.4116\\ 
$\big\|V_\text{m,ref,1}^{0.2,j,2}-V_\text{m,ref,1}^{0.2,j,1}\big\|_{0,(0,T)}$ &   1.4296 &   0.7824 &   0.4111 &   0.1849\\ 
$\big\|V_\text{m,ref,1}^{0.2,j,3}-V_\text{m,ref,1}^{0.2,j,2}\big\|_{0,(0,T)}$ &   0.7876 &   0.4123 &   0.1850 &   0.0941\\ 
$\big\|V_\text{m,ref,1}^{0.2,j,4}-V_\text{m,ref,1}^{0.2,j,3}\big\|_{0,(0,T)}$ &   0.4194 &   0.1826 &   0.0916 &   0.0478\\ 
\end{tabular} 
 
\bigskip 
 
\begin{tabular}{c|c|c|c|c} 
     & $j= 0$  & $j= 1$  & $j=2$   & $j=3$\\
\hline
$\big\|V_\text{m,ref,9}^{0.2,j,1}-V_\text{m,ref,9}^{0.2,j,0}\big\|_{0,(0,T)}$ &  16.3121 &   9.2366 &   5.0141 &   2.5912\\ 
$\big\|V_\text{m,ref,9}^{0.2,j,2}-V_\text{m,ref,9}^{0.2,j,1}\big\|_{0,(0,T)}$ &   9.7641 &   5.0887 &   2.6064 &   1.3392\\ 
$\big\|V_\text{m,ref,9}^{0.2,j,3}-V_\text{m,ref,9}^{0.2,j,2}\big\|_{0,(0,T)}$ &   5.3123 &   2.6397 &   1.3351 &   0.6778\\ 
$\big\|V_\text{m,ref,9}^{0.2,j,4}-V_\text{m,ref,9}^{0.2,j,3}\big\|_{0,(0,T)}$ &   2.7703 &   1.3368 &   0.6785 &   0.3359\\ 
\end{tabular} 
\end{table}
\begin{table}[H]
\centering
\caption{Convergence test for the timestepping with dx=0.2mm  and ${\vartriangle} t = 0.1*2^{-j}$ ms.}
\begin{tabular}{c|c|c|c|c|c} 
     & $m= 0$  & $m= 1$  & $m=2$   & $m=3$ & $m=4$\\
\hline
$\big\|V_\text{m,ref,1}^{0.2, 1, m }-V_\text{m,ref,1}^{0.2, 0, m }\big\|_{0,(0,T)}$ &   2.8918 &   1.4665 &   0.8866 &   0.6049 &   0.4852\\ 
$\big\|V_\text{m,ref,1}^{0.2, 2, m }-V_\text{m,ref,1}^{0.2, 1, m }\big\|_{0,(0,T)}$ &   1.5498 &   0.7870 &   0.4243 &   0.2139 &   0.1430\\ 
$\big\|V_\text{m,ref,1}^{0.2, 3, m }-V_\text{m,ref,1}^{0.2, 2, m }\big\|_{0,(0,T)}$ &   0.7765 &   0.4143 &   0.1900 &   0.1013 &   0.0597\\ 
\end{tabular} 
 
\bigskip 
 
\begin{tabular}{c|c|c|c|c|c} 
     & $m= 0$  & $m= 1$  & $m=2$   & $m=3$ & $m=4$\\
\hline
$\big\|V_\text{m,ref,9}^{0.2, 1, m }-V_\text{m,ref,9}^{0.2, 0, m }\big\|_{0,(0,T)}$ &  20.1122 &  13.4191 &   9.1328 &   6.6839 &   5.3231\\ 
$\big\|V_\text{m,ref,9}^{0.2, 2, m }-V_\text{m,ref,9}^{0.2, 1, m }\big\|_{0,(0,T)}$ &  10.1792 &   6.0319 &   3.5867 &   2.2986 &   1.6480\\ 
$\big\|V_\text{m,ref,9}^{0.2, 3, m }-V_\text{m,ref,9}^{0.2, 2, m }\big\|_{0,(0,T)}$ &   5.2455 &   2.8307 &   1.5670 &   0.9114 &   0.5699\\ 
\end{tabular} 
\end{table}

\begin{table}[H]
  \centering
  \caption{Duration of the simulation with different space discretization and ${\vartriangle} t = 0.1 \cdot 2^{-j}$ ms, $T=0.7$ s, no substepping, smoothed exitation and on 128 procs(hours:minutes:seconds).}
\label{tab:convergence coarse}
\begin{tabular}{c|c|c|c|c} 
  
      & $j= 0$  & $j= 1$  & $j=2$   & $j=3$      \\
\hline
${\vartriangle} x=0.5 \text{mm}$ &   00:17:44   on 64 procs   &     00:31:44  on 64 procs  & 01:07:28 on 64 procs &     02:09:35   on 64 procs  \\
${\vartriangle} x=0.2 \text{mm}$ &   1:21:43      & 2:31:57  & 5:21:11 & 11:06:46 \\
${\vartriangle} x=0.1 \text{mm}$ & 8:11:24 & 16:17:32 & 51:36:58  &  70:39:15 
\end{tabular}
\end{table}
