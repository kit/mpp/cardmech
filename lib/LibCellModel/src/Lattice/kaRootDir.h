/*! \file kaRootDir.h
   \brief Class kaRootDir for handling of default root directories

   \author fs IBT - Universität Karlsruhe (TH)
 */

#ifndef KAROOTDIR_H
#define KAROOTDIR_H

#include <kaMachineOS.h>

//! Class for handling of default root directories
/*!
   The default is hardcoded, but can be overwritten by setting the system variable "kaRootDir"

   \author cw,fs,idb, IBT - Universität Karlsruhe (TH)
 */

class kaRootDir {
  std::string root;
  std::string data;
  std::string bin;

 public:
  kaRootDir(const char *name, const char *rootDir = std::string("../lib/LibCellModel").c_str()) {
    char *p = (char*) rootDir;

    if (!p)
#ifdef osWin32
      p = "J:\\kaRoot";
#else // ifdef osWin32
      p = (char*)"/Volumes/bordeaux/IBT";
# ifdef osAIX
    p = (char*)"/bgwhome4/mr198/IBT";
# endif // ifdef osAIX
# if (defined(osLinux) && !defined(i386))
    p = (char*)"/gpfs/S_fs1/mr198/IBT";
# endif // if (defined(osLinux) && !defined(i386))
#endif // ifdef osWin32

#ifdef osWin32
    root = p + std::string("\\") + name;
    data = p + std::string("\\data\\") + name;
    bin  = p + std::string("\\bin\\") + name;
#else // ifdef osWin32
    root = p + std::string("/") + name;
    data = p + std::string("/data/") + name;

# ifdef osIRIX
    bin = p + std::string("/bin/sgiN32mips4/") + name;
# endif // ifdef osIRIX
# ifdef osLinux
#  ifdef PpcArchitecture
    bin = p + std::string("/bin/linuxPPC/") + name;
#  else // ifdef PpcArchitecture
    bin = p + std::string("/bin/linuxELF/") + name;
#  endif // ifdef PpcArchitecture

# endif // ifdef osLinux
# ifdef osMac
    bin = p + std::string("/bin/macosx/") + name;
# endif // ifdef osMac

#endif // ifdef osWin32
  }

  const char* GetRoot() { return root.c_str(); }

  const char* GetData() { return data.c_str(); }

  const char* GetBin() { return bin.c_str(); }
}; // class kaRootDir

#endif // ifndef KAROOTDIR_H
