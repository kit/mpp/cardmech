/*
**      Name
**              xCMessage.h
**
**      Usage
**              xCMessage.cpp
**
**      Remark
**
**
**
**      History
**              10.03.00 gs
**
**
**  created at IBT - Universität Karlsruhe
*/

#ifndef XCMESSAGE_H
#define XCMESSAGE_H

#include "kaLattice.h"
#include "kaMask.h"
#ifndef NO_BB
# include "BlackBoard.h"
#endif // ifndef NO_BB
#include "DomainConditions.h"

// #include <dirent.h>
#include <string>
#include <vector>

#ifdef X_MOTIF
# include <Xm/Scale.h>
# include <Xm/Frame.h>
# include <Xm/Label.h>
# include <Xm/ToggleB.h>
#endif // ifdef X_MOTIF

using namespace std;

#include "xFDMessage.h"

#include <CellModelLayer.h>

typedef ML_CalcType CalcType;


//! This is a class to assign a cell/force model to a material
class CMS {
 public:
  CMS() {
    matname   = "undefined";
    pep       = NULL;
    pfp       = NULL;
    prepem    = NULL;
    prepfm    = NULL;
    emt       = EMT_Dummy;
    fmt       = FMT_Dummy;
    VmInit    = 0.0;
    ForceInit = 0.0;
  }

  ~CMS() {
    clear();
  }

  void clear() {
    matname = "undefined";
    emd     = "";
    fmd     = "";

    emt = EMT_Dummy;
    delete pep;
    pep = NULL;
    delete prepem;
    prepem = NULL;

    fmt = FMT_Dummy;
    delete pfp;
    pfp = NULL;
    delete prepfm;
    prepfm = NULL;

    VmInit    = 0.0;
    ForceInit = 0.0;
  }

  string matname; //!< The name of the material
  string emd;     //!< The cell model corresponding to material
  string fmd;     //!< The force model corresponding to material
  vbElphyParameters<CalcType> *pep;  //! the used elphy model parameter, will be initialized before usage
  vbForceParameters<CalcType> *pfp;  //! the used force model parameter, will be initialized before usage
  vbElphyModel<CalcType> *prepem;  //! precalculation elphy model
  vbForceModel<CalcType> *prepfm;  //! precalculation force model
  ElphyModelType emt; //! elphymodeltype (from Cellmodelbasis.h: enum CellModelType)
  ForceModelType fmt; //! forcemodeltype (from Cellmodelbasis.h: enum CellModelType)
  CalcType VmInit;    //! Vm after precalculation of the used elphy models
  CalcType ForceInit; //! Force after precalculation of the used force models
}; // class CMS

/*!
 * This class allows the control of xCELLent from the commandline and other
 * tools via message queues. The funktions are sending different messages to
 * the BlackBoardSerever via MQIn. xCELLent reads this messages and react.
 * Also the funktions supported by xFDMessage are used.
 */
class xCMessage : public virtual xFDMessage {
 public:
  //! constructor with the arguments BB-Name, MQIn-Name, MQOut-Name and idArg
#ifndef NO_BB
  xCMessage(string, string, string, int);

  virtual ~xCMessage() {}

  //! function to send message commands including double values
  void SendSetDouble(string, double);

  //!< function to send setting a cell/force model to a class
  void SendSetCellModel(string);

  inline void SendLoadCellModelFile(string name)  /*!send: load cell model file*/
  { SendMQIn("LoadCMF ", name.c_str()); }

  inline void SendLoadHeterogenLatticesFile(string name)  /*!send: load heterogeneous lattices file*/
  { SendMQIn("LoadHLF ", name.c_str()); }

  inline void SendSetPCAFile(string name)  /*!send: set file with precalcall data*/
  { SendMQIn("SetPCAFile ", name.c_str()); }

  inline void SendLoadProjectFile(string name) /*!send: load project file*/
  { SendMQIn("LoadPF ", name.c_str()); }

  inline void SendSetMaterialIntra(string name)  /*!send: load material description for intracellular domain*/
  { SendMQIn("SetMatIntra ", name.c_str()); }

  inline void SendResultPrefix(string name) /*!send: set resultprefix*/
  { SendMQIn("Resultprefix ", name.c_str()); }

  inline void SendCalcAll()                 /*!send: calculate all timesteps*/
  { SendMQIn("CalcAll"); }

  inline void SendCalcNext()                /*!send: calculate next timestep*/
  { SendMQIn("CalcNext"); }

  inline void SendReset()                   /*!send: reset calculation*/
  { SendMQIn("Reset"); }

  inline void SendSave()                    /*!send: save results*/
  { SendMQIn("Save"); }

  inline void SendSetCalcLength(double clen)  /*!send: calculation length*/
  { SendSetDouble("SetCalcLength ", clen); }

  inline void SendSetDTCell(double dtc) /*!send: delta t cell model*/
  { SendSetDouble("SetDTCell ", dtc); }

  inline void SendSetDTNext(double dtn) /*!send: delta t to next stop*/
  { SendSetDouble("SetDTNext ", dtn); }

  inline void SendSetDTSave(double dts) /*!send: delta t saving*/
  { SendSetDouble("SetDTSave ", dts); }

# ifdef X_MOTIF

  //! callback to calculate all timesteps
  static void callbackCalcAll(Widget, XtPointer, XtPointer);

  //! callback to calculate next timestep
  static void callbackCalcNext(Widget, XtPointer, XtPointer);

  //! callback to reset calculation
  static void callbackReset(Widget, XtPointer, XtPointer);

  //!< callback to save actual result
  static void callbackSave(Widget, XtPointer, XtPointer);
# endif // ifdef X_MOTIF
#else // ifndef NO_BB
  xCMessage(string BBName, string MQInName, string MQOutName, int idArg) : xFDMessage(
      (char*)BBName.c_str(), (char*)MQInName.c_str(), (char*)MQOutName.c_str(), idArg) {}

#endif // ifndef NO_BB
}; // class xCMessage

//! class to handle with errors
#define xCELLentError kaBaseException

// replacing lines below due to "..." argument handling!
/*class xCELLentError : public kaBaseException {
   public:
   xCELLentError(const char* format, ...) : kaBaseException(format) {};
   };*/

#endif // ifndef XCMESSAGE_H
