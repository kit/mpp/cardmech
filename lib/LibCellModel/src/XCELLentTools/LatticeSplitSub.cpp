/*
 *  LatticeSplitSub.cpp
 *  LatticeSplit
 *
 *  Created by Daniel Weiss on 26.11.04.
 *  Copyright 2004 IBT KA. All rights reserved.
 *
 *  updated in August 2007
 */

#include "LatticeSplitSub.h"

string getSplitConfigName(SplitDirection d) {
  if (d == xDirection)
    return "x";
  else if (d == yDirection)
    return "y";
  else
    return "z";
}

LatticeSplit::LatticeSplit(void) { init = NULL; }

bool LatticeSplit::setLattices(const char *vNewValue) {
  if (scanDirectory(vNewValue))
    return testLatticeAccessibility();

  return false;
}

bool LatticeSplit::setProjectFile(const char *xcltFileName) {
  try {
    initXCLTOptions initO(true);
    initO.verbose        = true;
    init                 = new initXCLT(xcltFileName, &initO, false);
    init->removeTmpFiles = true;

    string mat   = init->getMaterialFile();
    string phi   = mat.substr(0, mat.rfind(".lat"))+".phi.lat";
    string theta = mat.substr(0, mat.rfind(".lat"))+".theta.lat";

    myFiles.addEntry(mat);
    myFiles.addEntry(phi);
    myFiles.addEntry(theta);

    dwArray<HLFInfo> *HLF = init->getHLFInfo();
    for (int x = HLF->lBound(); x < HLF->uBound(); x++) {
      // cerr<<"adding "<<(*HLF)[x].file.c_str()<<" ...\n";
      myFiles.addEntry((*HLF)[x].file);
    }

    if (init->getPCAFile().length()) {
      myFiles.addEntry(init->getCMPFile(true));
      myFiles.addEntry(init->getVmFile(true));
    }
    return testLatticeAccessibility();
  } catch (kaBaseException& e) {
    cerr << " Error: " << e << endl;
    exit(-1);
  }
} // LatticeSplit::setProjectFile

LatticeSplit::~LatticeSplit() {
  delete init;
}

bool LatticeSplit::addLattice2Split(string latName) {
  myFiles.addEntry(latName);
  return myFiles[myFiles.uBound()-myFiles.lBound()-1] == latName;
}

bool LatticeSplit::scanDirectory(const char *lName) {
  // delete[] varName in parent function necessary
  bool phiFound = false, thetaFound = false, latFound = false;
  string mylName = lName;

  myFiles.addEntry(mylName);
  if (init == NULL)
    return true;

  string myPre = mylName.erase(mylName.rfind(".lat")+1);
  string myDir = mylName.erase(mylName.rfind("/")+1);
  myPre = myPre.substr(myPre.rfind("/")+1);
  errno = 0;
  DIR *d = opendir(myDir.c_str());

  if (errno == EACCES)
    throw kaBaseException("Permission denied.\n");
  if (errno == EMFILE)
    throw kaBaseException("Too many file descriptors in use by process.\n");
  if (errno == ENFILE)
    throw kaBaseException("Too many files are currently open in the system.\n");
  if (errno == ENOENT)
    throw kaBaseException("Directory does not exist, or name is an empty string.\n");
  if (errno == ENOMEM)
    throw kaBaseException("Insufficient memory to complete the operation.\n");
  if (errno == ENOTDIR)
    throw kaBaseException("%s is not a directory.\n", myDir.c_str());
  if (errno == 0) {
    struct dirent *de;
    while ((de = readdir(d))) {
      string file = de->d_name;
      if ((file.substr(0, myPre.length()) == myPre) &&
          ((file.substr(file.rfind(".")) == ".lat") || (file.substr(file.rfind(".")) == ".flat"))) {
        if (file != myFiles[0]) {
          myFiles.addEntry(myDir+file);
          cerr<<"adding "<<(myDir+file).c_str()<<endl;
        }
        if (file == myPre+"phi.lat")
          phiFound = true;
        if (file == myPre+"theta.lat")
          thetaFound = true;
        if (file == myPre+"lat")
          latFound = true;
      }
    }
    closedir(d);
  } else {
    throw kaBaseException("unknown error!\n");
  }
  if (!latFound)
    throw kaBaseException("essential file %s%slat is missing!\n", myDir.c_str(), myPre.c_str());
  if (!phiFound)
    throw kaBaseException("essential file %s%sphi.lat is missing!\n", myDir.c_str(), myPre.c_str());
  if (!thetaFound)
    throw kaBaseException("essential file %s%sphi.lat is missing!\n", myDir.c_str(), myPre.c_str());
  return true;
} // LatticeSplit::scanDirectory

template<class X>

int getSplitRange(kaLattice<X> *matLat, SplitDirection d, int config) {
  if (config == 0) {
    if (d == xDirection)
      return matLat->xLattice;
    else if (d == yDirection)
      return matLat->yLattice;
    else
      return matLat->zLattice;
  } else if (config == 1) {
    if (d == xDirection)
      return matLat->yLattice;
    else if (d == yDirection)
      return matLat->zLattice;
    else
      return matLat->xLattice;
  } else if (config == 2) {
    if (d == xDirection)
      return matLat->zLattice;
    else if (d == yDirection)
      return matLat->xLattice;
    else
      return matLat->yLattice;
  } else {
    cerr<<"unknown config\n";
    return -1;
  }
} // getSplitRange

template<class X>

int getIndex(kaLattice<X> *matLat, SplitDirection d, int MainSplitCnt, int NextSplitCnt1, int NextSplitCnt2) {
  if (d == xDirection)
    return NextSplitCnt2*matLat->xyLattice+NextSplitCnt1*matLat->xLattice+MainSplitCnt;
  else if (d == yDirection)
    return NextSplitCnt1*matLat->xyLattice+MainSplitCnt*matLat->xLattice+NextSplitCnt2;
  else if (d == zDirection)
    return MainSplitCnt*matLat->xyLattice+NextSplitCnt2*matLat->xLattice+NextSplitCnt1;
  else
    return -1;
}

template<class  X>

void splitMe(kaLatticeCreate scls, kaLatticeCreate tcls, SplitDirection mySD, int lBorder, int uBorder,
             int targetVoxelsCount) {
  kaLattice<X> Lattice(scls);

  if (mySD == xDirection) {
    tcls.xLattice = uBorder-lBorder+1;
    tcls.yLattice = Lattice.yLattice;
    tcls.zLattice = Lattice.zLattice;
  } else if (mySD == yDirection) {
    tcls.xLattice = Lattice.xLattice;
    tcls.yLattice = uBorder-lBorder+1;
    tcls.zLattice = Lattice.zLattice;
  } else if (mySD == zDirection) {
    tcls.xLattice = Lattice.xLattice;
    tcls.yLattice = Lattice.yLattice;
    tcls.zLattice = uBorder-lBorder+1;
  } else {
    cerr<<"unknown direction\n";
  }
  tcls.compression  = ctGZIP;
  tcls.writeVersion = ioNow;
  nskaGlobal::FileCheck FC;
  FC.Check(tcls.Name);
  if (FC.Exist() and (remove(tcls.Name) == -1))
    throw kaBaseException("Lattice %s already exists and could not be deleted", tcls.Name);

  kaLattice<X> tLattice(tcls);
  tLattice.m            = Lattice.m;
  tLattice.m.a(mySD, 3) = tLattice.m.a(mySD, 3)+tLattice.m.a(mySD, mySD)*lBorder;

  int vCount = 0;
  for (int mainSplitDir = lBorder; mainSplitDir <= uBorder; mainSplitDir++)
    for (int nextSplitDir1 = 0; nextSplitDir1 < getSplitRange(&Lattice, mySD, 1); nextSplitDir1++)
      for (int nextSplitDir2 = 0; nextSplitDir2 < getSplitRange(&Lattice, mySD, 2); nextSplitDir2++) {
        if (Lattice.lat[getIndex(&Lattice, mySD, mainSplitDir, nextSplitDir1, nextSplitDir2)] != 0)
          vCount++;
        tLattice.lat[getIndex(&tLattice, mySD, mainSplitDir-lBorder, nextSplitDir1,
                              nextSplitDir2)] =
          Lattice.lat[getIndex(&Lattice, mySD, mainSplitDir, nextSplitDir1, nextSplitDir2)];
      }
  printf("\ttissue voxels: %i = %.2f %% of %i (delta: %i voxels)\n", vCount, (float)vCount/targetVoxelsCount*100,
         targetVoxelsCount, vCount-targetVoxelsCount);
  tLattice.Save();
} // splitMe

bool LatticeSplit::testLatticeAccessibility() {
  kaLatticeSharedHeader *psh;

  LatticeOK = false;
  try {
    for (int x = 0; x <= myFiles.uBound(); x++) {
      kaLatticeCreate cls;
      strcpy(cls.Name, myFiles[x].c_str());
      printf("testing lattice accessibility for %s ...\n", myFiles[x].c_str());
      try {
        psh = new kaLatticeSharedHeader(cls);
      } catch (kaBaseException e) {
        throw kaBaseException("Lattice %s is not accessible ...\n", myFiles[x].c_str());
      }
      if (x == 0) {
        strcpy(LatticeName, myFiles[0].c_str());
        LatticeOK = TRUE;
      }
    }
  } catch (kaBaseException e) {
    cerr << " Error: " << e << endl;
    strcpy(LatticeName, "no valid lattice defined ... \nError: ");
    strcat(LatticeName, e.getText());
    LatticeOK = FALSE;
  }
  return LatticeOK;
} // LatticeSplit::testLatticeAccessibility

void LatticeSplit::SplitLattices(int parts, int uOL, int lOL, SplitDirection SD) {
  if (LatticeOK) {
    mySD = SD;
    typedef u_int8_t X;
    printf("reading tissue classes from %s ...\n", myFiles[0].c_str());
    kaLatticeCreate cls;
    strcpy(cls.Name, myFiles[0].c_str());
    kaLattice<X> matLat(cls);
    int tissueVoxels = 0;
    for (int index = 0; index < matLat.xyzLattice; index++)
      if (matLat.lat[index] != 0)
        tissueVoxels++;
    printf("%i of %i voxels (%.2f%%) are active tissue ... generate %i parts with approx %i voxels\n", tissueVoxels,
           matLat.xyzLattice, (float)100*tissueVoxels/matLat.xyzLattice, parts, tissueVoxels/parts);
    printf("LatticeDimension: %i x %i x %i Voxels\t-\tsplitting %s layer\n", matLat.xLattice, matLat.yLattice,
           matLat.zLattice, getSplitConfigName(mySD).c_str());
    int cPart         = 1;
    int lastBorder    = -1;
    int cVoxelsCount  = 0;
    int VoxelsInParts = 0;
    int splitFrom     = 0;
    int splitTo       = 0;
    for (int mainSplitDir = 0; mainSplitDir < getSplitRange(&matLat, mySD, 0); mainSplitDir++) {
      for (int nextSplitDir1 = 0; nextSplitDir1 < getSplitRange(&matLat, mySD, 1); nextSplitDir1++)
        for (int nextSplitDir2 = 0; nextSplitDir2 < getSplitRange(&matLat, mySD, 2); nextSplitDir2++) {
          if (matLat.lat[getIndex(&matLat, mySD, mainSplitDir, nextSplitDir1, nextSplitDir2)] != 0)
            cVoxelsCount++;
          if (cVoxelsCount >= cPart*tissueVoxels/parts) {
            printf(
              "part %i (%i ... %s ... %i -> %i ... %s_new ... %i) contains %i active voxels (%.2f %% of all = %.2f %% of %i)\n", cPart, lastBorder+1,
              getSplitConfigName(mySD).c_str(), mainSplitDir, (cPart == 1 ? 0 : lOL), getSplitConfigName(mySD).c_str(),
              (cPart == 1 ? mainSplitDir : mainSplitDir-lastBorder-1+lOL), cVoxelsCount-VoxelsInParts,
              (float)(cVoxelsCount-VoxelsInParts)/tissueVoxels*100,
              (float)(cVoxelsCount-VoxelsInParts)/(tissueVoxels/parts)*100, tissueVoxels/parts);
            if (cPart == 1)
              splitFrom = 0;
            else
              splitFrom = lastBorder+1-lOL;
            if (cPart == parts)
              splitTo = mainSplitDir;
            else
              splitTo = mainSplitDir+uOL;
            for (int x = 0; x <= myFiles.uBound(); x++)
              saveLatticePart2Disc(myFiles[x].c_str(), cPart, splitFrom, splitTo, tissueVoxels/parts);
            if (init) {
              if (init->getPCAFile().length()) {
                cerr<<"\tcreating backup for part #"<<cPart<<endl;
                kaLatticeCreate cls;

                strcpy(cls.Name, getTargetName(init->getMaterialFile(), cPart).c_str());
                cerr<<"\t\tMaterial:\t"<<cls.Name<<endl;
                kaLattice<u_int8_t> *mat = new kaLattice<u_int8_t>(cls);

                strcpy(cls.Name, getTargetName(init->getCMPFile(false), cPart).c_str());
                cerr<<"\t\tCellModels:\t"<<cls.Name<<endl;
                kaLattice<pvbEM> *cmp = new kaLattice<pvbEM>(cls);

                strcpy(cls.Name, getTargetName(init->getVmFile(false), cPart).c_str());
                cerr<<"\t\tVm:\t\t\t"<<cls.Name<<endl;
                kaLattice<double> *Vm = new kaLattice<double>(cls);

                init->writeMyBackup(mat, cmp, Vm, getTargetName(init->getPCAFile(), cPart));

                delete Vm;
                delete cmp;
                delete mat;

                deleteIfExists(getTargetName(init->getCMPFile(false), cPart));
                deleteIfExists(getTargetName(init->getVmFile(false), cPart));
              }
              if (init->getHLFFile().length()) {
                string tName = getTargetName(init->getHLFFile(), cPart);
                nskaGlobal::FileCheck FC;
                FC.Check(tName.c_str());
                if (!FC.Exist() || forceWriting) {
                  cerr<<"\twriting *.hlf file for part #"<<cPart<<" to "<<tName.c_str()<<endl;
                  ofstream out(tName.c_str());
                  if (!out.is_open())
                    throw kaBaseException("Can't open file %s\n", tName.c_str());
                  dwArray<HLFInfo> *HLF = init->getHLFInfo();
                  for (int x = HLF->lBound(); x < HLF->uBound(); x++) {
                    // cerr<<"adding "<<(*HLF)[x].file.c_str()<<" ...\n";
                    string w = getTargetName((*HLF)[x].file, cPart)+" "+(*HLF)[x].key+"\n";
                    out.write(w.c_str(), w.length());
                  }
                  out.close();
                } else {
                  cerr<<"WARNING: "<<
                    getTargetName(init->getHLFFile(),
                                  cPart).c_str()<<
                  " exists and will not be overwritten! You can change this behavior with -f.\n";
                }
              }
            }
            cPart++;
            lastBorder    = mainSplitDir;
            VoxelsInParts = cVoxelsCount;
          }
        }
    }
    if (cVoxelsCount != tissueVoxels)
      throw kaBaseException("cVoxelsCount (%i) != tissueVoxels (%i)", cVoxelsCount, tissueVoxels);
  } else {
    throw kaBaseException("no valid Lattice defined!");
  }
} // LatticeSplit::SplitLattices

string LatticeSplit::getTargetName(string srcName, int PartNr) {
  char Nr[3];

  if (PartNr-1 < 10)
    sprintf(Nr, "00%i", PartNr-1);
  else if ((PartNr-1 > 9) && (PartNr-1 < 100) )
    sprintf(Nr, "0%i", PartNr-1);
  else if ((PartNr-1 > 99) && (PartNr-1 < 1000) )
    sprintf(Nr, "%i", PartNr-1);
  else
    throw kaBaseException("to many Parts!\n");

  if (outDir.length())
    srcName = outDir+srcName.substr(srcName.rfind("/")+1);
  string myPre = srcName.substr(0, srcName.rfind("."));
  return myPre+"_"+Nr+srcName.substr(myPre.length());
}

void LatticeSplit::saveLatticePart2Disc(string lName, int PartNr, int lBorder, int uBorder, int targetVoxelsCount) {
  string tName = getTargetName(lName, PartNr);
  kaLatticeCreate scls;
  kaLatticeCreate tcls;

  strcpy(scls.Name, lName.c_str());
  strcpy(tcls.Name, tName.c_str());

  kaLatticeSharedHeader *psh = new kaLatticeSharedHeader(scls);

  printf("\tsplitting Lattice %s from %s=%i to %i in %s\n", lName.c_str(), getSplitConfigName(
           mySD).c_str(), lBorder, uBorder, tName.c_str());
  if (psh->lhDtype() == dtUchar) {
    splitMe<u_int8_t>(scls, tcls, mySD, lBorder, uBorder, targetVoxelsCount);
  } else if (psh->lhDtype() == dtFloat) {
    splitMe<float>(scls, tcls, mySD, lBorder, uBorder, targetVoxelsCount);
  } else if (psh->lhDtype() == dtDouble) {
    splitMe<double>(scls, tcls, mySD, lBorder, uBorder, targetVoxelsCount);
  } else if (psh->lhDtype() == dtUnknown) {
    splitMe<pvbEM>(scls, tcls, mySD, lBorder, uBorder, targetVoxelsCount);
  } else {
    throw kaBaseException("unknown dataType %i in saveLatticePart2Disc()!", psh->lhDtype());
  }
}
