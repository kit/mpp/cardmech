/*
 *  timeStep.h
 *  LatticeAPCreate
 *
 *  Created by dw.local on 12.11.07.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef TIMESTEP
#define TIMESTEP

string timeStep(double t, bool asNumber = false, char format[4] = (char*)"%.6f", int length = 10) {
  // cerr<<"calc timestep with "<<t<<endl;
  char buf[1024];

  sprintf(buf, format, t);
  string ret = buf;

  // cerr<<"buf="<<buf<<endl;
  for (int x = ret.length(); x <= length; x++) {
    ret = "0"+ret;

    // cerr<<"ret="<<ret.c_str()<<endl;
  }
  if (asNumber)
    return ret;
  else
    return "_"+ret+"_";

  // new: %.4f und to <8;
}

string timeStepFWD(double t, bool asNumber = false, char format[4] = (char*)"%.6f", int length = 10) {
  // cerr<<"calc timestep with "<<t<<endl;
  char buf[1024];

  sprintf(buf, format, t);
  string ret = buf;

  // cerr<<"buf="<<buf<<endl;
  for (int x = ret.length(); x <= length; x++) {
    ret = "0"+ret;

    // cerr<<"ret="<<ret.c_str()<<endl;
  }
  if (asNumber)
    return ret;
  else
    return ret;

  // new: %.4f und to <8;
}

string timeStepCA(double t, bool asNumber = false, char format[4] = (char*)"%.1f", int length = 5) {
  // cerr<<"calc timestep with "<<t<<endl;
  char buf[1024];

  sprintf(buf, format, t);
  string ret = buf;

  // cerr<<"buf="<<buf<<endl;
  for (int x = ret.length(); x <= length; x++) {
    ret = "0"+ret;

    // cerr<<"ret="<<ret.c_str()<<endl;
  }
  if (asNumber)
    return ret;
  else
    return "t"+ret;

  // new: %.4f und to <8;
}

float timeStepAsFloat(float t) {
  return atof(timeStep(t, true).c_str());
}

int getTimeStep(float *startAddr, float value, bool up = true) {
  for (int step = 0; step < SIZE; step++, startAddr++) {
    if (*startAddr == value)
      return step;
  }
  return 0;
}

inline double getTime(float *APStart, float *timeStart, float threshold, bool up = true, int minStep = 0) {
  float lowervalue   = *APStart;
  float *highervalue = APStart;
  float  timelow     = *timeStart;
  float *timehigh    = timeStart;

  // if (minStep > 0)
  //    cerr<<"minStep="<<minStep<<endl;

  if (up)
    if (lowervalue > threshold)
      return timelow;

  for (int step = 0; step < SIZE; step++, highervalue++, timehigh++) {
    if (up && (*highervalue > threshold) && (lowervalue <= threshold) && (step > minStep) )
      break;
    if (!up && (*highervalue < threshold) && (lowervalue >= threshold) && (step > minStep) )
      break;
    timelow    = *timehigh;
    lowervalue = *highervalue;
  }
  return timelow+(threshold-lowervalue)/(*highervalue-lowervalue)*(*timehigh-timelow);
}

#endif // ifndef TIMESTEP
