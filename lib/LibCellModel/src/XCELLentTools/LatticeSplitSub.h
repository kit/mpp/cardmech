/*
 *  LatticeSplitSub.h
 *  LatticeSplit
 *
 *  Created by Daniel Weiss on 26.11.04.
 *  Copyright 2004 IBT KA. All rights reserved.
 *
 *  updated in August 2007
 */

#include <kaLattice.h>
#include <kaMask.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <dwArray.h>
#include <initXCLT.h>
#include <initXCLTOptions.h>

enum SplitDirection {
  xDirection = 0,
  yDirection = 1,
  zDirection = 2
};

class LatticeSplit {
 private:
  char LatticeName[255];
  void saveLatticePart2Disc(string lName, int PartNr, int lBorder, int uBorder, int targetVoxelsCount);
  bool testLatticeAccessibility(void);
  SplitDirection mySD;     // SplitDirection (x,y,z)
  dwArray<string> myFiles; // lattice names
  int lOL, uOL;            // lower and upper overlay
  initXCLT *init;
  string getTargetName(string, int);

 public:
  bool forceWriting;
  bool LatticeOK;
  string outDir;
  bool setLattices(const char*);
  bool setProjectFile(const char*);
  void SplitLattices(int Parts, int uOL, int lOL, SplitDirection SD);
  bool scanDirectory(const char *lName);
  bool addLattice2Split(string);
  LatticeSplit(void);
  ~LatticeSplit(void);
};
