/*
 *  HLFInfo.cpp
 *  InitXCLT
 *
 *  Created by dw on 31.07.07.
 *  Copyright 2007 IBT Uni Karlsruhe. All rights reserved.
 *
 */

#include "HLFInfo.h"

HLFInfo::HLFInfo() {
  // cerr<<"initializing HLFInfo ...\n";
  file                             = "not set";
  key                              = "not set";
  allowedVarianceForInitializeXCLT = -1;
}
