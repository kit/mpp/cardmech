/*
 *  LatticeResultsIntegrate.cpp
 *  LatticeResultsIntegrate
 *
 *  Created by Martin W. Krueger on 21.10.2011
 *  Copyright 2011 IBT KIT. All rights reserved.
 *
 *
 */

#include <APDefinition.h>
#include <timeStep.h>
#include <kaParser.h>

int main(int argc, char **argv) {
  string suf    = "Vm.flat";
  string outPre = "/tmp/LatticeResultsIntegrate";
  float  tbegin = 0.0;
  float  tend   = (float)1000/1000;
  float  tstep  = .001;
  int SIZE      = 1000;

  char BUFFER[128];

  kaVersion v(1, 0, 0, argc, argv);

  v.addOption("pre", "prefix-string)", OT_required, "",
              "prefix of the files to process, the filenames will be generated from <PREFIX><TIMESTEP><SUFFIX>");
  v.addOption("suf", "suffix-string)", OT_optional, suf, "suffix of the files to process");
  v.addOption("o", "string", OT_optional, outPre, "output prefix");
  sprintf(BUFFER, "%.4f", tbegin);
  v.addOption("tf", "float", OT_optional, BUFFER, "(temporal) begin of the files to be processed, in sec");
  sprintf(BUFFER, "%.4f", tend);
  v.addOption("tt", "float", OT_optional, BUFFER, "(temporal) end of the files to be processed, in sec");
  sprintf(BUFFER, "%.4f", tstep);
  v.addOption("ts", "float", OT_optional, BUFFER, "(temporal) step of the files to be processed, in sec");

  v.addOption("verbose", "", OT_optional, "", "enable verbose output");
  if (argc < 4) {
    v.printHelp();
    return 0;
  } else {
    try {
      nskaGlobal::FileCheck FC;
      kaParser CL(argc, argv);

      string pre     = "/tmp/";
      bool   verbose = false;
      char   format[5];
      strcpy(format, "%.6f");
      char length = 10;

      if (CL.IsOption("-pre"))
        pre = CL.GetOption("-pre", 1);
      if (CL.IsOption("-suf"))
        suf = CL.GetOption("-suf", 1);
      if (CL.IsOption("-o"))
        outPre = CL.GetOption("-o", 1);
      if (CL.IsOption("-tf"))
        tbegin = atof(CL.GetOption("-tf", 1));
      if (CL.IsOption("-tt"))
        tend = atof(CL.GetOption("-tt", 1));
      if (CL.IsOption("-ts"))
        tstep = atof(CL.GetOption("-ts", 1));
      if (CL.IsOption("-verbose"))
        verbose = true;


      string file = pre+timeStep(tbegin, true, format, length)+suf;
      if (verbose)
        printf("checking initial file %s ...\n", file.c_str());
      FC.Check(file.c_str());
      if (FC.Exist()) {
        kaLatticeCreate cls;
        strcpy(cls.Name, file.c_str());

        kaLattice<double> init(cls);
        int xyz = init.xyzLattice;

        strcpy(cls.Name, (outPre+".value.integrated").c_str());
        cls.xLattice    = 1;
        cls.yLattice    = 1;
        cls.zLattice    = 1;
        cls.compression = ctGZIP;

        FC.Check(cls.Name);
        if (FC.Exist()) {
          remove(cls.Name);
        }

        kaLattice<APData> Time(cls);

        kaLattice<double> *pIntegratedValues;

        strcpy(cls.Name, (outPre+".integratedValues.dlat").c_str());

        cls.xLattice    = init.xLattice;
        cls.yLattice    = init.yLattice;
        cls.zLattice    = init.zLattice;
        cls.compression = ctGZIP;

        FC.Check(cls.Name);
        if (FC.Exist()) {
          remove(cls.Name);
        }

        pIntegratedValues = new kaLattice<double>(cls);
        for (int index = 0; index < xyz; index++) {
          pIntegratedValues->lat[index] = 0.0;
        }

        if (verbose)
          cerr<<"processing from "<<tbegin*1000<<" ms to "<<tend*1000<<" ms with steps of "<<tstep*1000<<" ms ...\n";
        float t = tbegin;

        for (int x = 0; x < SIZE; x++) {
          file = pre+timeStep(t, true, format, length)+suf;
          string zeitschritt = timeStep(t, true, format, length);
          FC.Check(file.c_str());
          if (FC.Exist()) {
            if (verbose)
              cerr<<"processing "<<file.c_str()<< " ... " << endl;
            strcpy(cls.Name, file.c_str());
            kaLattice<double> use(cls);
            for (int index = 0; index < xyz; index++) {
              pIntegratedValues->lat[index] += use.lat[index]*tstep;
            }
          } else {
            throw kaBaseException("file %s does not exist!\n", file.c_str());
          }
          t += tstep;
          t  = t*1000;
          t  = floor(t+(tstep/2));
          t  = t/1000;
          if (t > tend+1E-8)
            x = SIZE;
        }

        cerr<<"saving "<<(outPre+".integratedValues.dlat").c_str()<<" ... ";
        pIntegratedValues->Save();
        cerr<<"done!\n";
      } else {
        throw kaBaseException("can't find file %s!", file.c_str());
      }
    } catch (kaBaseException e) {
      cerr<<"Error: "<<e<<endl;
    }
  }
} // main
