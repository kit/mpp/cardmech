/*
 *  LatticeAPExtract.cpp
 *  LatticeAPExtract
 *
 *  Created by Daniel Weiss on 12.11.07.
 *  Copyright 2007 IBT Uni Karlsruhe. All rights reserved.
 *
 */

#include <APDefinition.h>
#include <timeStep.h>
#include <kaParser.h>

#define ACTIVATION
#define APD

int main(int argc, char **argv) {
  string inPre  = "/tmp/LatticeAPCreate";
  string outPre = "/tmp/LatticeAPExtract";

#ifdef ACTIVATION
  double activationAt = -0.15;
#endif // ifdef ACTIVATION
#ifdef APD
  double percentage = .9;
#endif // ifdef APD
  char BUFFER[128];

  kaVersion v(1, 1, 0, argc, argv);
  v.addOption("i", "string", OT_required,
              inPre.c_str(), "input prefix of the files to process, should be similar to -o in LatticeAPCreate");
  v.addOption("o", "string", OT_optional, outPre.c_str(), "output prefix");
#ifdef ACTIVATION
  sprintf(BUFFER, "%.4f", activationAt);
  v.addOption("activationAt", "", OT_optional, BUFFER, "threshold voltage defining the activation of the tissue, in V");
#endif // ifdef ACTIVATION
#ifdef APD
  sprintf(BUFFER, "%.0f", percentage*100);
  v.addOption("apd", "percentage", OT_optional, BUFFER, "threshold defining the end of the APD");
#endif // ifdef APD
  sprintf(BUFFER, "%i", SIZE);
  v.addOption("numValidEntries", "int value", OT_optional, BUFFER,
              "if you used -tf and -tt option in LatticeAPCreate and the resulting number of valid, processed files was smaller than the default value you NEED to set this number here. Otherwise, the tool is not working properly!");
  v.addOption("verbose", "", OT_optional, "", "enable verbose output");
  v.addOption("getInfo", "", OT_optional, "", "return the fixed SIZE value");

  if (argc < 2) {
    v.printHelp();
    return 0;
  } else {
    try {
      nskaGlobal::FileCheck FC;
      kaParser CL(argc, argv);
      bool verbose   = false;
      int  VALIDSIZE = SIZE;
      if (CL.IsOption("-i"))
        inPre = CL.GetOption("-i", 1);
      if (CL.IsOption("-o"))
        outPre = CL.GetOption("-o", 1);
      if (CL.IsOption("-verbose"))
        verbose = true;
      if (CL.IsOption("-getInfo")) {
        cout << SIZE << endl;
        exit(0);
      }
      if (CL.IsOption("-numValidEntries")) {
        VALIDSIZE = atoi(CL.GetOption("-numValidEntries", 1));
        if (VALIDSIZE > SIZE)
          throw kaBaseException("numValidEntries cannot be larger than SIZE!");
        else
          cerr<<VALIDSIZE<<endl;
      }

#ifdef ACTIVATION
      if (CL.IsOption("-activationAt"))
        activationAt = atof(CL.GetOption("-activationAt", 1));
#endif // ifdef ACTIVATION
      cerr << "Threshold for activation determination is " << activationAt << endl;
#ifdef APD
      if (CL.IsOption("-apd"))
        percentage = atof(CL.GetOption("-apd", 1))/100.0f;
#endif // ifdef APD
      if (verbose)
        cerr << "Setting environment variable kaNoShm for this process ...";
      if (0 == setenv("kaNoShm", "", 1) ) {
        if (verbose)
          cout << "OK!" << endl;
      } else {
        throw kaBaseException("unable to setenv kaNoShm");
      }

      kaLatticeCreate cls;
      strcpy(cls.Name, (inPre+".APLat").c_str());
      FC.Check(cls.Name);
      if (FC.Exist()) {
        if (verbose)
          cerr<<"loading "<<cls.Name<<" ...\n";
        kaLattice<APDefinition> APLat(cls);
        if (verbose)
          cerr<<"done!\n";
        strcpy(cls.Name, (inPre+".time.APLat").c_str());
        FC.Check(cls.Name);
        if (FC.Exist()) {
          if (verbose)
            cerr<<"loading "<<cls.Name<<" ...\n";
          kaLattice<APData> Time(cls);
#ifdef ACTIVATION
          strcpy(cls.Name, (outPre+".activationTime.flat").c_str());
          cls.xLattice    = APLat.xLattice;
          cls.yLattice    = APLat.yLattice;
          cls.zLattice    = APLat.zLattice;
          cls.compression = ctGZIP;
          FC.Check(cls.Name);
          if (FC.Exist())
            remove(cls.Name);
          if (verbose)
            cerr<<"loading "<<cls.Name<<" ...\n";
          kaLattice<float> ActivationTime(cls);

          strcpy(cls.Name, (outPre+".activation.lat").c_str());
          cls.xLattice    = APLat.xLattice;
          cls.yLattice    = APLat.yLattice;
          cls.zLattice    = APLat.zLattice;
          cls.compression = ctGZIP;
          FC.Check(cls.Name);
          if (FC.Exist())
            remove(cls.Name);
          if (verbose)
            cerr<<"loading "<<cls.Name<<" ...\n";
          kaLattice<uint8_t> Activation(cls);
#endif // ifdef ACTIVATION
#ifdef APD
          sprintf(BUFFER, ".apd%.0f.flat", percentage*100.0f);
          strcpy(cls.Name, (outPre+BUFFER).c_str());
          cls.xLattice    = APLat.xLattice;
          cls.yLattice    = APLat.yLattice;
          cls.zLattice    = APLat.zLattice;
          cls.compression = ctGZIP;
          FC.Check(cls.Name);
          if (FC.Exist())
            remove(cls.Name);
          if (verbose)
            cerr<<"loading "<<cls.Name<<" ...\n";
          kaLattice<float> APDLat(cls);
#endif // ifdef APD
          int xyz = APLat.xyzLattice;
          if (verbose)
            cerr<<"running the calculation in "<<xyz<<" voxels ...\n";
          int percentDone = 0;
          for (int index = 0; index < xyz; index++) {            // for each voxel
            if (verbose) {
              if (double(index*100/xyz) > percentDone) {
                cerr<<int(index*100/xyz)<<" percent done ...\r"; // percent of voxels
                percentDone = int(index*100/xyz);
              }
            }
#ifdef ACTIVATION
            Activation.lat[index] = 0;
#endif // ifdef ACTIVATION
#ifdef APD
            float minValue = HUGE;
            float maxValue = -HUGE;
#endif // ifdef APD
            float *cValue = &(APLat.lat[index].value.item[0]);
            for (int step = 1; step < VALIDSIZE; step++, cValue++) { // for each timestep
#ifdef ACTIVATION
              if ((APLat.lat[index].value.item[step-1] < activationAt) &&
                  (APLat.lat[index].value.item[step] >= activationAt) && (Activation.lat[index] == 0)) {
                Activation.lat[index]     = 1;
                ActivationTime.lat[index] = Time.lat[0].item[step];
              }
#endif // ifdef ACTIVATION
#ifdef APD
              minValue = *cValue < minValue ? *cValue : minValue;
              maxValue = *cValue > maxValue ? *cValue : maxValue;
#endif // ifdef APD
            }
#ifdef APD
            double starttime = 0;
            double endtime   = 0;
            APDLat.lat[index] = 0;            // initial APD value, index = voxelindex
# ifdef ACTIVATION
            if (Activation.lat[index] == 1) { // calculate APD only for activated voxels
# endif // ifdef ACTIVATION
            int it_cnt = 0;                   // count the iterations
                                              // why do we iterate at all?
            do {
              starttime = getTime(&(APLat.lat[index].value.item[0]), &(Time.lat[0].item[0]),
                                  maxValue-percentage*(maxValue-minValue), true, int(endtime*1000));
              endtime = getTime(&(APLat.lat[index].value.item[0]), &(Time.lat[0].item[0]),
                                maxValue-percentage*(maxValue-minValue), false, int(starttime*1000));
              APDLat.lat[index] = endtime-starttime;
              it_cnt++;
            } while (APDLat.lat[index] < .07 && it_cnt <= 100); // do not iterate more than 100 times, stop when APD is
                                                                // above 70ms
# ifdef ACTIVATION
          } else
            APDLat.lat[index] = 0;
# endif // ifdef ACTIVATION
#endif // ifdef APD
          }
#ifdef ACTIVATION
          cerr<<"saving "<<(outPre+".activation.lat").c_str()<<" ...";
          Activation.Save();
          cerr<<"done!\n";
          cerr<<"saving "<<(outPre+".activationTime.flat").c_str()<<" ...";
          ActivationTime.Save();
          cerr<<"done!\n";
#endif // ifdef ACTIVATION
#ifdef APD
          cerr<<"saving "<<(outPre+BUFFER).c_str()<<" ...";
          APDLat.Save();
          cerr<<"done!\n";
#endif // ifdef APD
        } else {
          throw kaBaseException("time not found!\n");
        }
      } else {
        throw kaBaseException("file %s not found!\n", cls.Name);
      }
    } catch (kaBaseException e) {
      cerr<<"Error: "<<e<<endl;
    }
  }
} // main
