/*
 *  APDefinition.h
 *  LatticeAPCreate, LatticeAPCreate
 *
 *  Created by Daniel Weiss on 12.11.07.
 *  Copyright 2007 IBT Uni Karlsruhe. All rights reserved.
 *
 */
#ifndef APDEFINITION
#define APDEFINITION

#include <kaLattice.h>
#include <kaVersion.h>

#undef NEWMETHOD
const int SIZE = 2500;

class APData {
 public:
  float item[SIZE];
  inline bool operator==(const APData& a) { return false; }

  inline bool operator!=(const APData& a) { return true; }
};

class APDefinition {
 public:
  APData value;
  int x;

  inline bool operator==(const APDefinition& a) { return x == a.x; }

  inline bool operator!=(const APDefinition& a) { return x != a.x; }
};

#ifdef NEWMETHOD
class TimeValue {
 public:
  double time;
  double value;
};

class APD90Data {
 public:
  TimeValue min;
  TimeValue max;
};
#endif // ifdef NEWMETHOD

#endif // ifndef APDEFINITION
