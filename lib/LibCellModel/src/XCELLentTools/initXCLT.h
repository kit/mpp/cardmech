/*
 *  initXCLT.h
 *  InitXCLT
 *
 *  Created by dw on 30.07.07.
 *  Copyright 2007 IBT Uni Karlsruhe. All rights reserved.
 *
 */
#ifndef INITXCLT
#define INITXCLT

typedef double FDTyp;
typedef  FDTyp CTyp;

#include <dwArray.h>
#include <HLFInfo.h>
#include <xCMessage.h>
#include <initXCLTOptions.h>
#include <iostream>
#include <fstream>

/*! \fn void deleteIfExists( string file )
 *  \brief Deletes a file if the file exists.
 *  \param file The filename with path to be deleted.
 */
static void deleteIfExists(string file) {
  ifstream myFile(file.c_str() );

  if (myFile)
    remove(file.c_str() );
}

typedef vbElphyModel<CalcType>*pvbEM;

class initXCLT {
 private:
  string LatName;
  string HLFFile;
  string PCAFile;
  dwArray<HLFInfo> HLF;
  kaLattice<pvbEM> *cmpl; //!< cell model pointer lattice
  kaLattice<u_int8_t> *mat;
  kaLattice<float> *phase;
  kaLattice<ML_CalcType> *Vm;
  kaLattice<int> *doneAtStage;

  CMS cellmodels[256];  //!< One cell model struct for each tissue class

  int cntMaterial;
  initXCLTOptions *opt;
  void readHLFFile(void);
  void setCMF(string);
  void initLattices(bool);
  void PreCalcAModel(pvbEM*, ML_CalcType*, int);
  void PreCalcAModelPhase(pvbEM*, ML_CalcType*, ML_CalcType, ML_CalcType);
  int FindSimilarCells(pvbEM*, ML_CalcType*, IndexType, int);
  int* sort(int[], int);
  bool IsInRange(ML_CalcType, ML_CalcType);
  bool checkAndSetIfStdElphy(IndexType, ElphyModelType);

 public:
  initXCLT(const char*, initXCLTOptions*, bool);
  ~initXCLT();
  void PreCalc();
  void writeBackup(void);
  void writeMyBackup(kaLattice<u_int8_t>*, kaLattice<pvbEM>*, kaLattice<ML_CalcType>*, string);
  bool   loadBackup(const char*);
  string getMaterialFile(void);
  string getVmFile(bool);
  string getCMPFile(bool);
  string getPCAFile(void);
  string getHLFFile(void);
  bool removeTmpFiles;

  dwArray<HLFInfo>* getHLFInfo();
}; // class initXCLT

#endif // ifndef INITXCLT
