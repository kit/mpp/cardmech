/*
 *  HLFInfo.h
 *  InitXCLT
 *
 *  Created by dw on 31.07.07.
 *  Copyright 2007 IBT Uni Karlsruhe. All rights reserved.
 *
 */

#ifndef HLFINFO
#define HLFINFO

#include <kaLattice.h>

class HLFInfo {
 private:
 public:
  string file;
  string key;
  float allowedVarianceForInitializeXCLT;
  kaLattice<float> *lat;
  HLFInfo();
};
#endif // ifndef HLFINFO
