/*
 *  initXCLTOptions.h
 *  InitXCLT
 *
 *  Created by dw on 02.08.07.
 *  Copyright 2007 IBT Uni Karlsruhe. All rights reserved.
 *
 */

#ifndef INITXCLTOPTIONS
#define INITXCLTOPTIONS

#include <CellModelLayer.h>
#include <kaMask.h>
#include <kaLattice.h>

class initXCLTOptions {
 private:
 public:
  ML_CalcType tinc;
  ML_CalcType text;
  ML_CalcType textlen;
  ML_CalcType amplitude;
  ML_CalcType allowedVariance; // in percent!

  int numStims;
  bool verbose;
  bool ssi;
  bool usePCAFile;
  bool oldStyle;
  bool usePhase;
  kaMask *msk;
  kaLattice<u_int8_t> *mskLattice;
  string output;

  initXCLTOptions(ML_CalcType, ML_CalcType, ML_CalcType, ML_CalcType, int, ML_CalcType);
  initXCLTOptions();
  initXCLTOptions(bool);
  void show();
};
#endif // ifndef INITXCLTOPTIONS
