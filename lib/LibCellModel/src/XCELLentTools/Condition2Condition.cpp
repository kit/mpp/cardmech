/*
 *  Condition2Condition.cpp
 *  Condition2Condition
 *
 *  Created by Daniel Weiss on 27.01.06.
 *  Copyright 2006 IBT Universitt Karlsruhe. All rights reserved.
 *
 */

#include <Condition2ConditionSub.h>
#include <kaVersion.h>
/* Changelog of Condition2Condition
 *
 * 30.10.2007 - changes by dw
 *
 * - added availability to accept similar entries only once (similar = same x,y,z and t_offset). Either the minimal or
 *maximal amplitude is kept (default: min, optional -max).
 *   This method can be disabled by -all to process and print all conditions (what the following programs to with
 *similar entries depends on them ...)
 *
 */

/* ToDo
 *
 * 22.10.2007, dw: clean Shm (Shm keeps allocated although the delete functions are called in ~Cond2Cond)
 * 31.10.2007, dw: dwArray should use a memcpy in the addEntry function!
 *
 */

int main(int argc, char *argv[]) {
  kaVersion v(1, 1, 0, argc, argv);

  v.addOption("", "conditionFile", OT_required, "", "Condition file that should be processed");
  v.addOption("", "SourceLattice", OT_required, "", "name of the source lattice on which the Conditions are based");
  v.addOption("", "TargetLattice", OT_required, "",
              "name of the target lattice to which the Conditions should be converted");
  v.addOption("setAmp", "<value>", OT_optional, "", "change the stimulation amplitude to a specific value");
  v.addOption("fAmp", "<value>", OT_optional, "", "scale the stimulation amplitude with this factor");
  v.addOption("setToff", "<value>", OT_optional, "", "set the time offset to a specific value");
  v.addOption("setText", "<value>", OT_optional, "", "set the stimulation frequency to a specific value");
  v.addOption("all", "", OT_optional, "",
              "processes all conditions and does not process values with equal x,y,z,t_off only once");
  v.addOption("max", "", OT_optional, "",
              "uses the maximal amplitude for values with equal x,y,z,t_off; default: minimal amplitude [only used if -all is not set!]");
  v.addOption("petsc", "", OT_optional, "", "converts Lattice coordinate based conditions into PETSc index based");
  v.addOption("only_if_in_tissue", "", OT_optional, "",
              "adds Conditions only in voxels that have a tissue class !=0 in the TargetLattice");
  v.addOption("verbose", "", OT_optional, "false", "provide additional output");


  if (argc < 4) {
    cerr<<
    "This tool can convert Conditions that are stored in local coordinates from one anatomy (source) to another (target). Furthermore it can convert the x,y,z coordinates to a PETSc compatible index."
        <<endl;
    v.printHelp();
    exit(-1);
  } else {
    try {
      Cond2Cond c(argc, argv);
      c.Convert(argv[2], argv[3]);
      return 0;
    } catch (kaBaseException& e) {
      cerr << argv[0] << " Error: " << e << endl;
      exit(-1);
    }
  }
} // main
