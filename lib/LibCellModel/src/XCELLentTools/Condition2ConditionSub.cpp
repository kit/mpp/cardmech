/*
 *  Condition2ConditionSub.cpp
 *  Condition2Condition
 *
 *  Created by Daniel Weiss on 27.01.06.
 *
 *      Last modified by Mathias Wilhelms on 21.12.09: changed condition type to new naming convention (Ie, Ii, If, Ve,
 *Vm, Vf)
 *
 *  Copyright 2000-2009 IBT Universitt Karlsruhe. All rights reserved.
 *
 */

#include "Condition2ConditionSub.h"

Cond2Cond::Cond2Cond(int argc, char *argv[]) {
  in = new ifstream(argv[1]);
  if (!in->is_open())
    throw kaBaseException("can't open %s!", argv[1]);
  sLat              = NULL;
  dLat              = NULL;
  setToff           = false;
  setTlen           = false;
  setText           = false;
  setAmp            = false;
  verbose           = false;
  fAmp              = 1;
  ampLeft           = AL_min;
  cntInArray        = 0;
  addMsgShown       = false;
  processAll        = false;
  setPETSc          = false;
  only_if_in_tissue = false;
  for (int i = 4; i < argc; i++) {
    if (strcmp(argv[i], "-verbose") == 0) {
      verbose = true;
    } else if ((strcmp(argv[i], "-setAmp") == 0) && (i < argc-1) ) {
      setAmp = true;
      Amp    = atof(argv[++i]);
    } else if ((strcmp(argv[i], "-setToff") == 0) && (i < argc-1) ) {
      setToff = true;
      Toff    = atof(argv[++i]);
    } else if ((strcmp(argv[i], "-setTlen") == 0) && (i < argc-1) ) {
      setTlen = true;
      Tlen    = atof(argv[++i]);
    } else if ((strcmp(argv[i], "-setText") == 0) && (i < argc-1) ) {
      setText = true;
      Text    = atof(argv[++i]);
    } else if ((strcmp(argv[i], "-fAmp") == 0) && (i < argc-1) ) {
      fAmp = atof(argv[++i]);
    } else if (strcmp(argv[i], "-max") == 0) {
      ampLeft = AL_max;
    } else if (strcmp(argv[i], "-all") == 0) {
      processAll = true;
    } else if (strcmp(argv[i], "-petsc") == 0) {
      setPETSc = true;
    } else if (strcmp(argv[i], "-only_if_in_tissue") == 0) {
      only_if_in_tissue = true;
    } else {
      throw kaBaseException("unknown or incomplete option %s", argv[i]);
    }
  }
}

Cond2Cond::~Cond2Cond() {
  if (in)
    delete in;
  if (sLat)
    delete sLat;
  if (dLat)
    delete dLat;
}

void Cond2Cond::Convert(char *srcLat, char *destLat) {
  if (verbose)
    cerr<<"converting from "<<srcLat<<" to "<< destLat<<endl;
  int cnt = 0;
  char buffer[1024];
  do {
    in->getline(buffer, sizeof(buffer));
    if (strlen(buffer))
      cnt++;
  } while (!in->eof());
  if (verbose)
    cerr<<"processing "<<cnt<<" lines ...\n";
  myConds.alloc(0, cnt);
  in->clear();
  in->seekg(0, fstream::beg);

  sLat = LoadLattice<uint8_t>(srcLat, verbose);
  dLat = LoadLattice<uint8_t>(destLat, verbose);
  if (sLat && dLat) {
    char buffer[1024];
    if (verbose)
      cerr<<"start the processing of "<<myConds.uBound()+1<<" lines ...\n";
    do {
      in->getline(buffer, sizeof(buffer));
      if (strlen(buffer))
        convertCondition(buffer);
    } while (!in->eof());
    if (verbose)
      cerr<<"done!\ngenerating the output ...\n";
  }
  for (int x = myConds.lBound(); x < myConds.uBound(); x++) {
    bool show = (myConds[x].CT[0] == 'V') || (myConds[x].amp > 0);
    if (show) {
      myConds[x].Print(setPETSc);
      if (verbose)
        dLat->lat[int(myConds[x].P.z*dLat->xyLattice+myConds[x].P.y*dLat->xLattice+
                      myConds[x].P.x)] = (int)myConds[x].amp;
    }
  }
} // Cond2Cond::Convert

void Cond2Cond::convertCondition(char *buffer) {
  if (verbose)
    cerr<<"converting "<<buffer<<" ...\n";
  SingleCondition<FDTyp, BaseCondition> h;
  h.ScanLine(buffer, 1);
  double amp = Amp;
  if (!setAmp)
    amp =
      (h.cflag == CT_I || h.cflag == CT_II || h.cflag == CT_IF) ? h.I : ((h.cflag == CT_U) || (h.cflag == CT_UI) ||
                                                                         (h.cflag == CT_UF)) ? h.U : 0;
  if (setToff)
    h.toff = Toff;
  if (setTlen)
    h.sl = Tlen;
  if (setText)
    h.cl = Text;
  if (fAmp != 1)
    amp = fAmp*amp;
  char CT[2];
  CT[0] =
    (h.cflag == CT_I || h.cflag == CT_II || h.cflag == CT_IF) ? 'I' : (h.cflag == CT_U || h.cflag == CT_UI ||
                                                                       h.cflag == CT_UF) ? 'V' : '?';
  CT[1] =
    (h.cflag == CT_I ||
     h.cflag == CT_U) ? 'e' : (h.cflag == CT_II) ? 'i' : (h.cflag == CT_UI) ? 'm' : (h.cflag == CT_IF ||
                                                                                     h.cflag == CT_UF) ? 'f' : '?';
  for (int z = h.cz; z < h.dz; z++)
    for (int y = h.cy; y < h.dy; y++)
      for (int x = h.cx; x < h.dx; x++) {
        bool firstentry = ((x == 0) && (y == 0) && (z == 0) ? true : false);
        double lx       = x;
        double ly       = y;
        double lz       = z;
        if (ConvertToLattice(lx, ly, lz, sLat, dLat)) {
          // cout<<lx<<" "<<ly<<" "<<lz<<" ";   //probleme, wenn lx,ly,lz ber printf("%i",(int)lx) ausgegeben werden
          // sollen, so wurde aus (4/1/8) eine (4/0/8),
          // daher dieser komische Weg!
          lx = int(lx+0.5);  // round to integer values, otherwise the test, if a condition already exists, does not
                             // work properly
          ly = int(ly+0.5);
          lz = int(lz+0.5);
          int PETScIndex = 0;
          if (setPETSc)
            PETScIndex = lz*dLat->xyLattice+ly*dLat->xLattice+lx;
          Condition C(lx, ly, lz, amp, h, CT, PETScIndex);
          if (processAll) {
            if (only_if_in_tissue) {                 // a condition is written only if there is tissue (!=0) in the
                                                     // target lattice where the condition should be applied
              if (int(dLat->val(lx, ly, lz)) != 0) { // checks if the target lattice is 0 at the point where the
                                                     // condition should be written
                C.Print(setPETSc);
              }
            } else {
              C.Print(setPETSc);
            }
            if (verbose) {
              dLat->lat[int(myConds[x].P.z*dLat->xyLattice+myConds[x].P.y*dLat->xLattice+
                            myConds[x].P.x)] = (int)myConds[x].amp;
            }
          } else {
            bool found = false;
            for (int xi = 0; xi <= cntInArray; xi++) {
              found = ((C.P == myConds[xi].P) && (C.h.toff == myConds[xi].h.toff) && !firstentry);

              if (found) {
                if (verbose) {
                  // C.Print(setPETSc);
                  // printf("=");
                  // myConds[xi].Print(setPETSc);
                }
                if (((ampLeft == AL_min) && (C.amp < myConds[xi].amp)) ||
                    ((ampLeft == AL_max) && (C.amp > myConds[xi].amp)) ) {
                  if (verbose) {
                    if (ampLeft == AL_min)
                      cerr<<"amp "<<C.amp <<"<"<<myConds[xi].amp<<" ...\n";
                    else
                      cerr<<"amp "<<C.amp <<">"<<myConds[xi].amp<<" ...\n";
                  }
                  myConds[xi].amp = C.amp;
                }
                xi = myConds.uBound()+1;
              }
            }
            if (!found) {
              //    myConds.addEntry(C);
              myConds[cntInArray] = C;

              // cerr<<cntInArray<<"="<<myConds[cntInArray].CT[0]<<myConds[cntInArray].CT[1]<<endl;
              cntInArray++;
              if (cntInArray > myConds.uBound()) {
                if (!addMsgShown) {
                  cerr<<
                  "Needed to switch to the mode where the addEntry method of dwArray is used. If this is to slow for your purpose, change the way to calculate cnt in the Convert() method of Cond2Cond class ...\n";
                  addMsgShown = true;
                }
                myConds.addEntry(C);
              }
            }
          }
        } else if (verbose) {
          cerr<<"\tpoint "<<lx<<"/"<<ly<<"/"<<lz<<" is out of my lattice \n";
        }
      }
} // Cond2Cond::convertCondition

Condition::Condition(int Px, int Py, int Pz, double Amp, SingleCondition<FDTyp, BaseCondition> H, char ct[2],
                     int petscind) {
  P.x        = Px;
  P.y        = Py;
  P.z        = Pz;
  amp        = Amp;
  h          = H;
  CT[0]      = ct[0];
  CT[1]      = ct[1];
  petscindex = petscind;
}

void Condition::Print(bool setPETSc) {
  if (setPETSc)
    printf("%i ", petscindex);
  else
    printf("%i %i %i ", P.x, P.y, P.z);
  printf("%.5f %.6f %.6f %.6f %c%c\n", amp, h.cl, h.sl, h.toff, CT[0], CT[1]);
}
