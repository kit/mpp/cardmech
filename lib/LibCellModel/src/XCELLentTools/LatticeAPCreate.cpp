/*
 *  LatticeAPCreate.cpp
 *  LatticeAPCreate
 *
 *  Created by Daniel Weiss on 12.11.07.
 *  Copyright 2007-2012 IBT Uni Karlsruhe / Karlsruhe Institute of Technology (KIT). All rights reserved.
 *
 * 14.07.2011 mwk: Bugfix of -activationAt option
 * 21.10.2011 mwk: Bugfix of -suf option
 * 31.01.2012 mwk: Repol Option
 * 20.03.2012 mwk: CA option
 */

#include <APDefinition.h>
#include <timeStep.h>
#include <kaParser.h>

int main(int argc, char **argv) {
  string suf            = "Vm.flat";
  string outPre         = "/tmp/LatticeAPCreate";
  float  tbegin         = 0.0;
  float  tend           = (float)SIZE/1000;
  float  tstep          = .001;
  float  activationAt   = -0.05;
  bool   skipActivation = false;
  bool   skipAPLat      = true;
  bool   repol          = false;
  bool   CA             = false;

  char BUFFER[128];

  kaVersion v(1, 0, 5, argc, argv);

  v.addOption("pre", "prefix-string)", OT_required, "",
              "prefix of the files to process, the filenames will be generated from <PREFIX>_<TIMESTEP>_<SUFFIX>");
  v.addOption("suf", "suffix-string)", OT_optional, suf, "suffix of the files to process");
  sprintf(BUFFER, "%.4f", tbegin);
  v.addOption("tf", "float", OT_optional, BUFFER, "(temporal) begin of the files to be processed, in sec");
  sprintf(BUFFER, "%.4f", tend);
  v.addOption("tt", "float", OT_optional, BUFFER, "(temporal) end of the files to be processed, in sec");
  sprintf(BUFFER, "%.4f", tstep);
  v.addOption("ts", "float", OT_optional, BUFFER, "(temporal) step of the files to be processed, in sec");
  sprintf(BUFFER, "%.4f", activationAt);
  v.addOption("activationAt", "", OT_optional, BUFFER, "threshold voltage defining the activation of the tissue, in V");
  v.addOption("skipActivation", "", OT_optional, "", "skip the generation of the activation lattice");
  v.addOption("repolarization", "", OT_optional, "", "generate lattice containing times of start of repolarization");
  v.addOption("APLat", "", OT_optional, "",
              "generate the APLat lattice\n\t\t\t(carefully check the sizes of your lattices and the Variable SIZE in APDefinition.h, maybe it is necessary to split your lattices before)");
  v.addOption("o", "string", OT_optional, outPre, "output prefix");
  v.addOption("CA", "", OT_optional, BUFFER, "Use cellular automaton style file names <pre>t0000.0<suf>");
  v.addOption("verbose", "", OT_optional, "", "enable verbose output");
  v.addOption("old", "", OT_optional, "",
              "use format *_0000.000_* instead of *_0000.000000_* (for old xCELLent results)");
  v.addOption("getInfo", "", OT_optional, "", "return the fixed SIZE value");
  if ((argc > 1) && (!strcmp(argv[1], "-getInfo"))) {
    cout << SIZE << endl;
    exit(0);
  }
  if (argc < 3) {
    v.printHelp();
    return 0;
  } else {
    try {
      nskaGlobal::FileCheck FC;
      kaParser CL(argc, argv);

      string pre     = "/tmp/";
      bool   verbose = false;
      char   format[5];
      strcpy(format, "%.6f");
      char length = 10;

      if (CL.IsOption("-pre"))
        pre = CL.GetOption("-pre", 1);
      if (CL.IsOption("-suf"))
        suf = CL.GetOption("-suf", 1);
      if (CL.IsOption("-o"))
        outPre = CL.GetOption("-o", 1);
      if (CL.IsOption("-tf"))
        tbegin = atof(CL.GetOption("-tf", 1));
      if (CL.IsOption("-tt"))
        tend = atof(CL.GetOption("-tt", 1));
      if (CL.IsOption("-ts"))
        tstep = atof(CL.GetOption("-ts", 1));
      if (CL.IsOption("-activationAt"))
        activationAt = atof(CL.GetOption("-activationAt", 1));
      if (CL.IsOption("-verbose"))
        verbose = true;
      if (CL.IsOption("-skipActivation"))
        skipActivation = true;
      if (CL.IsOption("-APLat"))
        skipAPLat = false;
      if (CL.IsOption("-repolarization"))
        repol = true;
      if (CL.IsOption("-old")) {
        strcpy(format, "%.3f");
        length = 7;
      }
      if (CL.IsOption("-CA")) {
        strcpy(format, "%4.1f");
        length = 5;
        CA     = true;
      }
      if (CL.IsOption("-getInfo")) {
        cout << SIZE << endl;
        exit(0);
      }

      // swotch start and end for repolarization
      if (repol) {
        float ttemp;
        ttemp  = tbegin;
        tbegin = tend;
        tend   = ttemp;
      }

      // if (verbose)
      cerr << "Threshold for activation determination is " << activationAt << endl;
      if (verbose)
        cerr << "Setting environment variable kaNoShm for this process ...";
      if (0 == setenv("kaNoShm", "", 1) ) {
        if (verbose)
          cout << "OK!" << endl;
      } else {
        throw kaBaseException("unable to setenv kaNoShm");
      }

      string file;
      if (CA)
        file = pre+timeStepCA(tbegin, false, format, length)+suf;
      else
        file = pre+timeStep(tbegin, false, format, length)+suf;

      if (verbose)
        printf("checking initial file %s ...\n", file.c_str());
      FC.Check(file.c_str());
      if (FC.Exist()) {
        kaLatticeCreate cls;
        strcpy(cls.Name, file.c_str());

        kaLattice<float> init(cls);
        int xyz = init.xyzLattice;

        strcpy(cls.Name, (outPre+".time.APLat").c_str());
        cls.xLattice    = 1;
        cls.yLattice    = 1;
        cls.zLattice    = 1;
        cls.compression = ctGZIP;

        FC.Check(cls.Name);
        if (FC.Exist()) {
          remove(cls.Name);
        }

        kaLattice<APData> Time(cls);
        kaLattice<APDefinition> *pAPLat;
        kaLattice<float> *pActivation;
        kaLattice<float> *pInsteadOfAPLat;

        if (skipAPLat)
          strcpy(cls.Name, (outPre+".APLatSmallTemp.flat").c_str());
        else
          strcpy(cls.Name, (outPre+".APLat").c_str());

        cls.xLattice    = init.xLattice;
        cls.yLattice    = init.yLattice;
        cls.zLattice    = init.zLattice;
        cls.compression = ctGZIP;

        FC.Check(cls.Name);
        if (FC.Exist()) {
          remove(cls.Name);
        }
        if (skipAPLat)
          pInsteadOfAPLat = new kaLattice<float>(cls);
        else
          pAPLat = new kaLattice<APDefinition>(cls);

        if (!skipActivation) {
          strcpy(cls.Name, (outPre+".Activation.flat").c_str());
          cls.xLattice    = init.xLattice;
          cls.yLattice    = init.yLattice;
          cls.zLattice    = init.zLattice;
          cls.compression = ctGZIP;

          FC.Check(cls.Name);
          if (FC.Exist()) {
            remove(cls.Name);
          }
          pActivation = new kaLattice<float>(cls);
        }

        if (verbose)
          cerr<<"processing from "<<tbegin*1000<<" ms to "<<tend*1000<<" ms with steps of "<<tstep*1000<<" ms ...\n";
        float t = tbegin;
        for (int x = 0; x < SIZE; x++)
          Time.lat[0].item[x] = -1;
        for (int x = 0; x < SIZE; x++) {
          // file=pre+timeStep((double)x/1000+toff)+suf;
          // printf("t=%.4f ms, x=%i, SIZE=%i\n",t,x,SIZE);
          // if(!repol) {
          string zeitschritt;
          if (CA) {
            file        = pre+timeStepCA(t, false, format, length)+suf;
            zeitschritt = timeStepCA(t, false, format, length);
          } else {
            file        = pre+timeStep(t, false, format, length)+suf;
            zeitschritt = timeStep(t, false, format, length);
          }

          FC.Check(file.c_str());

          // }
          // else {
          // file=pre+timeStep(t,false,format,length)+suf;
          // string zeitschritt=timeStep(t,false,format,length);
          // FC.Check(file.c_str());
          // }
          if (FC.Exist()) {
            if (verbose)
              cerr<<"processing "<<file.c_str()<< " ... ";
            strcpy(cls.Name, file.c_str());
            kaLattice<float> use(cls);
            for (int index = 0; index < xyz; index++) {
              if (!skipActivation) {
                if (x == 0) {                                    // first file
                  if (skipAPLat)
                    pInsteadOfAPLat->lat[index] = activationAt;  // [mwk]
                  else
                    pAPLat->lat[index].value.item[0] = activationAt;
                } else {
                  if (skipAPLat) {
                    if ((pInsteadOfAPLat->lat[index] < activationAt) && (use.lat[index] >= activationAt)) {
                      if (!CA)
                        pActivation->lat[index] = t*1000;  // [mwk] change for ms
                      else
                        pActivation->lat[index] = t;       // [mwk]
                    }
                  } else {
                    if ((pAPLat->lat[index].value.item[x-1] < activationAt) && (use.lat[index] >= activationAt)) {
                      if (!CA) {
                        pActivation->lat[index] = t*1000;  // [mwk] change for ms
                      } else {
                        pActivation->lat[index] = t;  // [mwk]
                      }
                    }
                  }
                }
              }
              if (skipAPLat)
                pInsteadOfAPLat->lat[index] = use.lat[index];
              else
                pAPLat->lat[index].value.item[x] = use.lat[index];
            }

            if (CA)
              Time.lat[0].item[x] = atof(timeStepCA(t, true, format, length).c_str());
            else
              Time.lat[0].item[x] = atof(timeStep(t, true, format, length).c_str());


            if (verbose)
              cerr<<"writing time "<<Time.lat[0].item[x]<<" ...";
            if (verbose)
              cerr<<"done!\n";
          } else {
            throw kaBaseException("file %s does not exist!\n", file.c_str());
          }
          if (!repol) {
            t += tstep;
            t  = t*1000;
            t  = floor(t+(tstep/2));
            t  = t/1000;
            if (t > tend+1E-8)
              x = SIZE;
          } else {
            t -= tstep;
            t  = t*1000;
            t  = floor(t+(tstep/2));
            t  = t/1000;
            if (t <= 0)
              x = SIZE;
          }
        }

        if (skipAPLat) {
          strcpy(cls.Name, (outPre+".APLatSmallTemp.flat").c_str());
          FC.Check(cls.Name);
          if (FC.Exist()) {
            if (verbose)
              cerr<<"removing "<<cls.Name<<" ... \n";
            remove(cls.Name);
          }
          strcpy(cls.Name, (outPre+".time.APLat").c_str());
          FC.Check(cls.Name);
          if (FC.Exist()) {
            if (verbose)
              cerr<<"removing "<<cls.Name<<" ... \n";
            remove(cls.Name);
          }
        } else {
          cerr<<"saving "<<(outPre+".APLat").c_str()<<" ... ";
          pAPLat->Save();
          cerr<<"done!\n";
          cerr<<"saving "<<(outPre+".time.APLat").c_str()<<" ... ";
          Time.Save();
          cerr<<"done!\n";
        }

        if (!skipActivation) {
          cerr<<"saving "<<(outPre+".Activation.flat").c_str()<<" ... ";
          pActivation->Save();
          cerr<<"done!\n";
        }
      } else {
        throw kaBaseException("can't find file %s!", file.c_str());
      }
    } catch (kaBaseException e) {
      cerr<<"Error: "<<e<<endl;
    }
  }
} // main
