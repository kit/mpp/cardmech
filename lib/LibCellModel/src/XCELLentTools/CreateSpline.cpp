/*
 *  CreateSpline.cpp
 *  CreateSpline
 *
 *  Created by dw.local on 18.04.06.
 *  Copyright 2006 IBT Uni Karlsruhe. All rights reserved.
 *
 */

#include "CreateSpline.h"

/* (C) Copr. 1986-92 Numerical Recipes Software ... */

// from /pinot/local/NumericalRecipesInC

int main(int argc, char *argv[]) {
  kaVersion v(1, 0, 0, argc, argv);

  v.addOption("", "CHAR* ConditionFile");
  v.addOption("yp1", "<FLOAT yp1Value>", OT_optional, "1E30",
              "defines the spline properties, defaultValue sets the boundary condition for a natural spline");
  v.addOption("ypn", "<FLOAT ypnValue>", OT_optional, "1E30",
              "defines the spline properties, defaultValue sets the boundary condition for a natural spline");
  v.addOption("toutinc", "<DOUBLE value>", OT_optional, "1", "incrementation of the output");
  v.addOption("getValue", "<DOUBLE targetValue>", OT_optional, "",
              "if set, only the y-Value for x=targetValue is printed");
  v.addOption("verbose", "", OT_optional, "", "enable verbose mode");

  if (argc < 2) {
    v.printHelp();
  } else {
    double *pXValue;
    double *pYValue;
    double *pY2Value;
    bool verbose = false;
    int  numeric = 6;
    try {
      char *condFile    = argv[1];
      float yp1         = 1E30;
      float ypn         = 1E30;
      double toutinc    = 1;
      double getValueAt = 0;

      for (int ac = 1; ac < argc; ac++) {
        if (!strcasecmp("-yp1", argv[ac]) && (ac+1 < argc) )
          yp1 = atof(argv[++ac]);
        else if (!strcasecmp("-ypn", argv[ac]) && (ac+1 < argc) )
          ypn = atof(argv[++ac]);
        else if (!strcasecmp("-verbose", argv[ac]))
          verbose = true;
        else if (!strcasecmp("-toutinc", argv[ac]) && (ac+1 < argc) )
          toutinc = atof(argv[++ac]);
        else if (!strcasecmp("-getValue", argv[ac]) && (ac+1 < argc) )
          getValueAt = atof(argv[++ac]);
        else if (!strcasecmp("-nums", argv[ac]) && (ac+1 < argc) )
          numeric = atoi(argv[++ac]);
      }
      sprintf(option, "NumEntries");
      FindOptionValue(condFile, option, retValue, true);
      int NumEntries = atoi(retValue);

      pXValue  = new double[NumEntries];
      pYValue  = new double[NumEntries];
      pY2Value = new double[NumEntries];

      NumEntries++;
      pXValue[0] = 0;
      pYValue[0] = 0;

      for (int i = 0; i < NumEntries-1; i++) {
        sprintf(option, "x_%i", i);
        FindOptionValue(condFile, option, retValue, true);
        float xValue = atof(retValue);
        sprintf(option, "y_%i", i);
        FindOptionValue(condFile, option, retValue, true);
        float yValue = atof(retValue);
        pXValue[i+1] = xValue;
        pYValue[i+1] = yValue;
      }

      spline(pXValue, pYValue, NumEntries, yp1, ypn, pY2Value);
      for (int i = 1; i < NumEntries; i++) {
        if (verbose)
          printf("class=%i\tvalue=%4.5f\ty2=%4.5f\n", (int)pXValue[i], pYValue[i], pY2Value[i]);
      }
      double ret;
      if (getValueAt) {
        splint(pXValue, pYValue, pY2Value, NumEntries, getValueAt, &ret);
        if (numeric == 1)
          printf("%.1f", ret);
        else if (numeric == 2)
          printf("%.2f", ret);
        else if (numeric == 3)
          printf("%.3f", ret);
        else if (numeric == 4)
          printf("%.4f", ret);
        else if (numeric == 5)
          printf("%.5f", ret);
        else if (numeric == 6)
          printf("%.6f", ret);
        else if (numeric == 7)
          printf("%.7f", ret);
        else if (numeric == 8)
          printf("%.8f", ret);
        else if (numeric == 9)
          printf("%.9f", ret);
        else if (numeric == 10)
          printf("%.10f", ret);
        else if (numeric == 11)
          printf("%.11f", ret);
        else if (numeric == 12)
          printf("%.12f", ret);
        else if (numeric == 13)
          printf("%.13f", ret);
        else if (numeric == 14)
          printf("%.14f", ret);
        else if (numeric == 15)
          printf("%.15f", ret);
        else if (numeric == 16)
          printf("%.16f", ret);
        else
          throw kaBaseException("\n\n\n\n\n\nunknown format!\n\n\n\n\n");
      } else {
        bool foundFlag;
        if (toutinc) {
          for (double i = pXValue[1]; i <= pXValue[NumEntries-1]+1E-5; i += toutinc) {
            splint(pXValue, pYValue, pY2Value, NumEntries, i, &ret);
            printf("%4.5f\t%4.5f", i, ret);

            // writing spline
            foundFlag = false;
            for (int j = 1; j < NumEntries; j++)
              if (abs(pXValue[j]-i) < 1E-5) {
                printf("\t%4.5f\n", pYValue[j]);
                foundFlag = true;
              }
            if (!foundFlag)
              printf("\n");
          }
        } else {
          for (int i = (int)pXValue[0]; i <= pXValue[NumEntries-1]; i++) {
            splint(pXValue, pYValue, pY2Value, NumEntries, i, &ret);
            printf("%i\t%4.5f", i, ret);

            // writing spline
            foundFlag = false;
            for (int j = 1; j < NumEntries; j++)
              if (pXValue[j] == i) {
                printf("\t%4.5f\n", pYValue[j]);
                foundFlag = true;
              }
            if (!foundFlag)
              printf("\n");
          }
        }
      }
      delete[] pYValue;
      delete[] pXValue;
    } catch (kaBaseException& e) {
      delete[] pYValue;
      delete[] pXValue;
      cerr<<"Error: "<<e<<endl;
    }
  }
} // main
