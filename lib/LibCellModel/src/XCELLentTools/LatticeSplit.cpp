/*
 *  LatticeSplit.cpp
 *  LatticeSplit
 *
 *  Created by dw.local on 26.01.06.
 *  Copyright 2006 IBT Universitaet Karlsruhe. All rights reserved.
 *
 */

/* Changelog of LatticeSplit.cpp
 *
 * 19.10.2007 - changes by mi096
 *
 * - added setting of environment variable kaNoShm at program start
 * - added deleting of temporary files if existent at program start
 * - added output of program version
 *
 * 19.10.2007 - changes by dw
 *
 * - works also when no *.hlf file is given in the *.xclt file
 *
 * 22.10.2007 - changes by dw
 *
 * - temporary file names include the process id and were removed from /tmp after processing
 */

#include <LatticeSplitSub.h>

const string programVersion = "1.3 / 22.10.2007";

PrintParameterModus PrintParameterMode = PrintParameterModeOff;

int main(int argc, char *argv[]) {
  cerr << argv[0] << " Version " << programVersion << endl;
  if (argc < 4) {
    cerr << argv[0] <<
    " \n\t\t<int numSplits>\n\t\t<char splitDirection (x|y|z)>\n\t\t<-pf *.xclt> || <-matLat *.lat>\n\t\t[-lOL <lower overlap - default: 1>]\n\t\t[-uOL <upper overlap - default: 1>]\n\t\t[-f :enables forced writing]\n\t\t[-o <output directory> (default: /tmp/)]"
         << endl;
  } else {
    try {
      int lOL = 1, uOL = 1;
      bool forceWriting = false;
      string pf, matLat, outDir;
      outDir = "/tmp/";
      for (int i = 3; i < argc; i++)
        if ((strcmp(argv[i], "-lOL") == 0) && (i < argc-1) ) {
          lOL = atoi(argv[++i]);
        } else if ((strcmp(argv[i], "-uOL") == 0) && (i < argc-1) ) {
          uOL = atoi(argv[++i]);
        } else if ((strcmp(argv[i], "-pf") == 0) && (i < argc-1) ) {
          pf = argv[++i];
        } else if ((strcmp(argv[i], "-matLat") == 0) && (i < argc-1) ) {
          matLat = argv[++i];
        } else if (strcmp(argv[i], "-f") == 0) {
          forceWriting = true;
        } else if (strcmp(argv[i], "-o") == 0) {
          outDir = argv[++i];
          if (outDir.substr(outDir.length()-1) != "/")
            outDir += "/";
        } else {
          throw kaBaseException("Unknown option %s", argv[i]);
        }

      SplitDirection SD;
      if (*argv[2] == 'x')
        SD = xDirection;
      else if (*argv[2] == 'y')
        SD = yDirection;
      else if (*argv[2] == 'z')
        SD = zDirection;
      else
        throw kaBaseException("unknown splitdirection!\n");

      cerr << "Setting environment variable kaNoShm for this process ...";
      if (0 == setenv("kaNoShm", "", 1) )
        cout << "OK!" << endl;
      else
        cout << "Failed!" << endl;

      LatticeSplit LS;
      LS.forceWriting = true;
      LS.outDir       = outDir;

      if (pf.length()) {
        if (LS.setProjectFile(pf.c_str()))
          LS.SplitLattices(atoi(argv[1]), uOL, lOL, SD);
      } else if (matLat.length()) {
        if (LS.setLattices(matLat.c_str()))
          LS.SplitLattices(atoi(argv[1]), uOL, lOL, SD);
      }
      return 0;
    } catch (kaBaseException& e) {
      cerr << argv[0] << " Error: " << e << endl;
      exit(-1);
    }
  }
} // main
