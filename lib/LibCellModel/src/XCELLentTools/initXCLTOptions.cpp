/*
 *  initXCLTOptions.cpp
 *  InitXCLT
 *
 *  Created by dw on 02.08.07.
 *  Copyright 2007 IBT Uni Karlsruhe. All rights reserved.
 *
 */

#include "initXCLTOptions.h"

initXCLTOptions::initXCLTOptions(ML_CalcType _tinc, ML_CalcType _text, ML_CalcType _textlen, ML_CalcType _amplitude,
                                 int _numStims, ML_CalcType _allowedVariance) {
  tinc            = _tinc;
  text            = _text;
  textlen         = _textlen;
  amplitude       = _amplitude;
  numStims        = _numStims;
  verbose         = true;
  ssi             = false;
  usePhase        = false;
  allowedVariance = _allowedVariance;
  oldStyle        = false;
  usePCAFile      = false;
  msk             = NULL;
  mskLattice      = NULL;
  output          = "";
}

initXCLTOptions::initXCLTOptions() {}

initXCLTOptions::initXCLTOptions(bool _usePCAFile) {
  usePCAFile = _usePCAFile;
  verbose    = false;
}

void initXCLTOptions::show() {
  cerr<<"tinc="<<tinc<<endl;
  cerr<<"text="<<text<<endl;
  cerr<<"textlen="<<textlen<<endl;
  cerr<<"amplitude="<<amplitude<<endl;
  cerr<<"numStims="<<numStims<<endl;
  cerr<<"allowedVariance="<<allowedVariance<<endl;
}
