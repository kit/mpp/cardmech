/*
**      Name
**              DomainConditions.h
**
**      Usage
**              DomainConditions.cpp
**
**      Remark
**
**
**
**      History
**              08.02.99 -fs creation
**              21.09.01 -fs specialization
**              28.04.04 -fs tridomain
**
**  created at IBT - Universität Karlsruhe
*/

#ifndef DOMAINCONDITIONS_H
#define DOMAINCONDITIONS_H

enum ConditionType { CT_I = 1, CT_U = 2, CT_II = 4, CT_UI = 8, CT_IF = 16, CT_UF = 32, FLAG_ISOTROP_HOM = 64 };

#include "Conditions.h"

//! Base class for conditions in the bidomain case

template<class ValTyp>
class BaseConditionBidomain {
 public:
  ValTyp cl; //!< cyclelength of stimulus
  ValTyp toff;  //!< temporal offset of stimulus
  ValTyp sl; //!< stimulus length
  ValTyp U   //!< potential
  , I;       //!< current

  //! function to get the values of I? or U?
  ValTyp Get(ConditionType ct) {
    switch (ct) {
      case CT_I:
      case CT_II:
      case CT_IF:
        return I;

      case CT_U:
      case CT_UI:
      case CT_UF:
        return U;

      default:
        throw ConditionError("Condition's type is unknown");
    }
  }
}; // class BaseConditionBidomain


template<class ValTyp, class BaseCondition>
class SingleCondition : public BaseCondition {
 public:
  IndexType cx, cy, cz;
  IndexType dx, dy, dz;
  ConditionType cflag;
  void ScanLine(char *buf, IndexType v);
};

typedef BaseConditionBidomain<FDTyp>                    BaseCondition;
typedef Conditions<FDTyp, BaseCondition, ConditionType> FDConditions;

/*!
 * the function is here overwritten, because BaseCondition is larger than in
 * the normal case. the function sets the coordinates, the amplitude, length,
 * cyclelength, temporal offset and the kind of impulse
 */
template<class FDTyp, class BaseCondition>
void SingleCondition<FDTyp, BaseCondition >::ScanLine(char *buf, IndexType v) {
  double C, C1, C2, C3;
  char   c1, c2;
  char   xstr[256], ystr[256], zstr[256];

  int rc = sscanf(buf, "%s %s %s %lf %lf %lf %lf %c%c",
                  xstr, ystr, zstr, &C, &C1, &C2, &C3, &c1, &c2);

#if KADEBUG
  fprintf(stderr, "ScanLine '%s' '%s' '%s' '%lf' '%lf' '%lf' '%lf' '%c %c'\n",
          xstr, ystr, zstr, C, C1, C2, C3, c1, c2);
#endif // if KADEBUG

  if (rc != 9)
    throw ConditionError("Condition line includes not enough words");

  rc = sscanf(xstr, "%d-%d", &cx, &dx);
  if ((rc != 1) && (rc != 2))
    throw ConditionError("Syntax error in condition line");
  cx = (IndexType)(cx/v+.5);
  if (rc == 1)
    dx = cx+1;
  else
    dx = (IndexType)(dx/v+.5);

  rc = sscanf(ystr, "%d-%d", &cy, &dy);
  if ((rc != 1) && (rc != 2))
    throw ConditionError("Syntax error in condition line");
  cy = (IndexType)(cy/v+.5);
  if (rc == 1)
    dy = cy+1;
  else
    dy = (IndexType)(dy/v+.5);

  rc = sscanf(zstr, "%d-%d", &cz, &dz);
  if ((rc != 1) && (rc != 2))
    throw ConditionError("Syntax error in condition line");
  cz = (IndexType)(cz/v+.5);
  if (rc == 1)
    dz = cz+1;
  else
    dz = (IndexType)(dz/v+.5);

  c1 = toupper(c1);
  c2 = toupper(c2);
  if (c1 == 'I') {
    if (c2 == 'E') {
      cflag = CT_I; this->U = -C; this->I = C;
    } else if (c2 == 'I')      {
      cflag = CT_II; this->U = -C; this->I = C;
    } else if (c2 == 'F')      {
      cflag = CT_IF; this->U = -C; this->I = C;
    } else { throw ConditionError("Condition's type is unknown"); }
  } else if (c1 == 'U')     {
    if (c2 == 'E') {
      cflag = CT_U; this->U = C; this->I = 0;
    } else if (c2 == 'I')      {
      cflag = CT_UI; this->U = C; this->I = 0;
    } else if (c2 == 'F')      {
      cflag = CT_UF; this->U = C; this->I = 0;
    } else { throw ConditionError("Condition's type is unknown"); }
  } else { throw ConditionError("Condition's type is unknown"); }

  this->cl   = C1;
  this->sl   = C2;
  this->toff = C3;
} // >::ScanLine

#endif // ifndef DOMAINCONDITIONS_H
