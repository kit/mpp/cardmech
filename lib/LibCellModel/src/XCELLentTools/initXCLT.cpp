/*
 *  initXCLT.cpp
 *  InitXCLT
 *
 *  Created by dw on 30.07.07.
 *  Copyright 2007 IBT Uni Karlsruhe. All rights reserved.
 *
 */

/* Changelog of initXCLT.cpp
 *
 * 19.10.2007 - changes by mi096
 *
 * - added correct error handling in constructor initXCLT by catching kaBaseException
 * - function getVmFile(bool) now saves the temporary file if function parameter is true
 *
 * 21.02.2008 - changes by dw
 *
 * - availability to load and save acCELLerateBackupVersion1.0
 *
 * 03.03.2008 - changes by dw
 *
 * - msk option
 *
 * 20.08.2009 - changey by cr116
 *
 * - added support for .aclt files
 * - usePCA bug (segmentation fault) fixed
 *
 * 09.03.2012 - mwk, gs
 * - force model not written to backup. Private version for Jan Richter to simulate atria hetero EP with force for
 *mechanics simulations
 *
 */
#define M_PI 3.1415

#include "initXCLT.h"
#include <vtkSmartPointer.h>
#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkInformation.h>
#include <vtkObjectBase.h>
#include <vtkPoints.h>

#include "VTKIOHelper.h"

#include <iostream>
#include <fstream>

#ifdef _OPENMP
   # include <omp.h>
#else // ifdef _OPENMP
   # define omp_get_thread_num() 0
#endif // ifdef _OPENMP

using namespace std;

string initXCLT::getVmFile(bool Save) {
  if (Save)
    Vm->Save();
  char buf[1024];
  sprintf(buf, "/tmp/initXCLT_Vm.%ld.dlat", (long)getpid());
  return buf;
}

string initXCLT::getCMPFile(bool Save) {
  if (Save)
    cmpl->Save();
  char buf[1024];
  sprintf(buf, "/tmp/initXCLT_CMP.%ld.dlat", (long)getpid());
  return buf;
}

string initXCLT::getMaterialFile(void) {
  return LatName;
}

string initXCLT::getPCAFile(void) {
  return PCAFile;
}

string initXCLT::getHLFFile(void) {
  return HLFFile;
}

dwArray<HLFInfo>* initXCLT::getHLFInfo(void) {
  return &HLF;
}

initXCLT::~initXCLT() {
  delete cmpl;
  delete mat;

  if (removeTmpFiles) {
    delete Vm;
    delete doneAtStage;
    deleteIfExists(getVmFile(false));
    deleteIfExists(getCMPFile(false));
    char buf[1024];
    sprintf(buf, "/tmp/initXCLT_doneAtStage.%ld.ilat", (long)getpid());
    deleteIfExists(buf);
  } else {
    deleteIfExists(getCMPFile(false));
    char buf[1024];
    Vm->Save();
    doneAtStage->Save();
    delete Vm;
    delete doneAtStage;
    sprintf(buf, "/tmp/initXCLT_doneAtStage.%ld.ilat", (long)getpid());
    cerr<<"\t\ttemporary file were kept in "<<getVmFile(false)<<", "<<buf<<endl;
  }
}

bool initXCLT::loadBackup(const char *backupFile) {
  if (opt->verbose)
    printf("loading backup from %s, this may take a while ...\n", backupFile);
  bool ret = false;
  FILE *bu = fopen(backupFile, "rb");
  if (bu) {
    char tempstr[256];
    fscanf(bu, "%s\n", tempstr);
    if (strcasecmp("xCELLentBackupVersion1.2", tempstr) == 0) {
      printf("\t\tstyle: xCELLentBackupVersion1.2\n");
      double timetemp;
      int xtemp, ytemp, ztemp;
      const IndexType MaxX = mat->xLattice, MaxY = mat->yLattice, MaxZ = mat->zLattice;
      fscanf(bu, "%lf %d %d %d\n", &timetemp, &xtemp, &ytemp, &ztemp);
      if ((xtemp != MaxX) || (ytemp != MaxY) || (ztemp != MaxZ) ) {
        cerr<<"Dimension of Lattices don't match to Backupfile"<<endl;
      } else {
        CTyp *prVm = &Vm->lat[0];

        // CTyp *prVe=&this->rf->lat[0];
        ML_CalcType prVe = 0.0;
        pvbEM *cmpdata   = &cmpl->lat[0];

        // pvbFM *fmpdata=&fmpl->lat[0];
        pvbEM *cmenddata = &cmpl->lat[MaxX*MaxY*MaxZ];
        int sizeofCTyp   = sizeof(CTyp);
        for (; cmpdata < cmenddata; cmpdata++, prVm++) {
          kaRead(prVm, sizeofCTyp, 1, bu);
          kaRead(&prVe, sizeofCTyp, 1, bu);
          (*cmpdata)->ReadStatus(bu);

          // (*fmpdata)->ReadStatus(bu);
          fread(NULL, 0, 1, bu);
        }
        ret = true;
      }
    } else if (strcasecmp("acCELLerateBackupVersion1.0", tempstr) == 0) {
      printf("\t\tstyle: acCELLerateBackupVersion1.0\n");
      double timetemp;
      int MaxIndex;
      fscanf(bu, "%lf %d\n", &timetemp, &MaxIndex);
      if (MaxIndex != mat->xyzLattice) {
        cerr<<"Dimension of Lattices don't match to Backupfile"<<endl;
      } else {
        CTyp *prVm     = &Vm->lat[0];
        pvbEM *cmpdata = &cmpl->lat[0];

        // pvbFM *fmpdata=&fmpl->lat[0];
        pvbEM *cmenddata = &cmpl->lat[MaxIndex];
        int sizeofCTyp   = sizeof(CTyp);
        for (IndexType index = 0; cmpdata < cmenddata; cmpdata++, prVm++, index++) {
          int matnr = mat->lat[index];
          initElphyModel<CTyp>(&*cmpdata, cellmodels[matnr].pep, cellmodels[matnr].emt);
          kaRead(prVm, sizeofCTyp, 1, bu);
          (*cmpdata)->ReadStatus(bu);

          // (*fmpdata)->ReadStatus(bu);
          fread(NULL, 0, 1, bu);
        }
        ret = true;
      }
    } else {
      cerr<<"This Backup is incompatible to xCELLent version 1.2 or acCELLerate version 1.0"<<endl;
    }
    fclose(bu);
  } else {
    cerr<<"Attention! Could not open backup file: "<<backupFile<<endl;
  }
  if (opt->verbose)
    cerr<<"backup successfully read!\n";
  return ret;
} // initXCLT::loadBackup

void initXCLT::writeMyBackup(kaLattice<u_int8_t> *_mat, kaLattice<pvbEM> *_cmpl, kaLattice<ML_CalcType> *_Vm,
                             string backupFileName) {
  if (opt->verbose) {
    printf("\t\twriting backup to %s ... this may take a while!\n", backupFileName.c_str());
    if (opt->oldStyle)
      printf("\t\tstyle: xCELLentBackupVersion1.2\n");
    else
      printf("\t\tstyle: acCELLerateBackupVersion1.0\n");
  }
  int MaxXYZ       = _mat->xyzLattice;
  pvbEM *cmpdata   = &_cmpl->lat[0];
  pvbEM *cmenddata = &_cmpl->lat[MaxXYZ];
  ML_CalcType prVe = 0.0;
  FILE *bu         = fopen(backupFileName.c_str(), "wb");
  if (!bu)
    throw kaBaseException("error during opening %s!", backupFileName.c_str());
  if (opt->oldStyle) {
    fprintf(bu, "xCELLentBackupVersion1.2\n");
    const IndexType MaxX = _mat->xLattice, MaxY = _mat->yLattice, MaxZ = _mat->zLattice;
    fprintf(bu, "%lf %d %d %d\n", 0.00, MaxX, MaxY, MaxZ);
  } else {
    fprintf(bu, "acCELLerateBackupVersion1.0\n");
    fprintf(bu, "%lf %d\n", 0.00, MaxXYZ);
  }
  int sizeofCTyp = sizeof(CTyp);
  for (IndexType index = 0; cmpdata < cmenddata; cmpdata++, index++) {
    kaWrite(&_Vm->lat[index], sizeofCTyp, 1, bu);

    // Vm
    if (opt->oldStyle)
      kaWrite(&prVe, sizeofCTyp, 1, bu);

    // Ve for xCELLentBackup1.2 schreiben

    _cmpl->lat[index]->WriteStatus(bu);

    // CellModelPointer
    // fwrite((ML_CalcType*)NULL, 0, 1, bu);
    // empty ForceModelPointer
  }
  fclose(bu);
  if (opt->verbose)
    printf("\t\tbackup was written to %s\n", backupFileName.c_str());
} // initXCLT::writeMyBackup

void initXCLT::writeBackup() {
  char buf[1024];

  if (opt->output.length())
    sprintf(buf, "%s", opt->output.c_str());
  else
    sprintf(buf, "%s/initXCLT.%ld.abp", getenv("HOME"), (long)getpid());
  writeMyBackup(mat, cmpl, Vm, buf);
}

int initXCLT::FindSimilarCells(pvbEM *myCmpdata, ML_CalcType *myVm_data, IndexType myIndex, int currentStage) {
  if (opt->verbose)
    printf("\t\tfind similar cells to index %i ...\n", myIndex);
  int MaxXYZ           = mat->xyzLattice;
  pvbEM *cmpdata       = &cmpl->lat[0];
  int   *isDone        = &doneAtStage->lat[0];
  ML_CalcType *Vm_data = &Vm->lat[0];
  int mymatnr          = mat->lat[myIndex];
  pvbEM *cmenddata     = &cmpl->lat[MaxXYZ];
  int cntSimilarCells  = 0;
  int cntInRange       = 0;
  dwArray<float> allowedVariance(HLF.lBound(), HLF.uBound());
  for (IndexType x = allowedVariance.lBound(); x <= allowedVariance.uBound(); x++) {
    if (HLF[x].allowedVarianceForInitializeXCLT >= 0)
      allowedVariance[x] = HLF[x].allowedVarianceForInitializeXCLT/100*HLF[x].lat->lat[myIndex];
    else
      allowedVariance[x] = opt->allowedVariance/100*HLF[x].lat->lat[myIndex];
    if ((opt->verbose) && (allowedVariance[x]))
      printf(
        "\t\t\t%s:\tcurrent value %.4f, allowed %.2f %% relative variance, absolute variance: %.4f (%.4f ... %.4f)\n",
        HLF[x].key.c_str(), HLF[x].lat->lat[myIndex], allowedVariance[x]/HLF[x].lat->lat[myIndex]*100, allowedVariance[x],
        HLF[x].lat->lat[myIndex]-allowedVariance[x], HLF[x].lat->lat[myIndex]+allowedVariance[x]);
  }

  // precalculation of the absolute allowed variance per heterogeneous parameter to save calculation time ...
  for (IndexType index = 0; cmpdata < cmenddata; cmpdata++, index++, isDone++, Vm_data++) {
    int matnr    = mat->lat[index];
    bool inMask  = opt->msk ? opt->msk->m[opt->mskLattice->lat[index]] : true;
    bool isValid = (mymatnr == matnr) && (index != myIndex) && inMask;
    if (isValid) {
      if (!(*isDone)) {
        bool inRange = true;
        bool isEqual = true;

        if (cellmodels[matnr].emt > EMT_Dummy) {
          for (IndexType x = HLF.lBound(); x <= HLF.uBound(); x++) {
            isEqual = isEqual && (HLF[x].lat->lat[index] == HLF[x].lat->lat[myIndex]);
            if (inRange)
              inRange = inRange && (abs(HLF[x].lat->lat[index] - HLF[x].lat->lat[myIndex]) <= allowedVariance[x]);
          }
        }

        if ((opt->verbose) && (inRange) && (!isEqual))
          cntInRange++;
        if (inRange) {
          // cerr<<"index "<<index<<" is in range of index "<<myIndex<<"\n";
          *Vm_data = *myVm_data;
          initElphyModel<CTyp>(cmpdata, cellmodels[matnr].pep, cellmodels[matnr].emt);
          (*cmpdata)->Set(**myCmpdata);

          // cerr<<"skipping set in empty!\n";
          *isDone = currentStage;
          cntSimilarCells++;
        }
      } else {
        // cerr<<"FindSimilarCells: is done!\n";
      }
    }
  }

  if (opt->verbose) {
    if (cntInRange) {
      printf(
        "\t\t%i parameter sets comparable to index %i were found, %i of them (%.1f %%) were only similar but not equal parameter sets)!\n", cntSimilarCells, myIndex, cntInRange,
        (double)cntInRange/cntSimilarCells*100.);
    } else {
      printf("\t\t%i equal parameter sets to index %i were found\n", cntSimilarCells, myIndex);
    }
  }

  return cntSimilarCells;
} // initXCLT::FindSimilarCells

bool initXCLT::IsInRange(ML_CalcType v1, ML_CalcType v2) {
  return v1 == v2;
}

void initXCLT::PreCalc() {
  // initialize values
  int MaxXYZ           = mat->xyzLattice;
  pvbEM *cmpdata       = &cmpl->lat[0];
  int   *isDone        = &doneAtStage->lat[0];
  ML_CalcType *Vm_data = &Vm->lat[0];
  int cnt              = 0; // counts bins
  pvbEM *cmenddata     = &cmpl->lat[MaxXYZ];
  int   *binSize       = new int[MaxXYZ];


  // sort the cells into bins according to similiarity
  for (IndexType index = 0; cmpdata < cmenddata; cmpdata++, index++, isDone++, Vm_data++) {  // iterate over cells
    int matnr = mat->lat[index];
    if (!*isDone) {
      bool inMask = opt->msk ? opt->msk->m[opt->mskLattice->lat[index]] : true;
      if (inMask) {
        initElphyModel<CTyp>(cmpdata, cellmodels[matnr].pep, cellmodels[matnr].emt);
        (*cmpdata)->Set(*cellmodels[matnr].prepem);
        doneAtStage->lat[index] = cnt+1;
        int size = FindSimilarCells(cmpdata, Vm_data, index, cnt+1);
        binSize[cnt] = size+1;
        cnt++;  // increase counter for next bin
      }
    }
  }

#pragma omp parallel for
  for (int binIndex = 0; binIndex < cnt; binIndex++) { // iterate over bins
    cout << "Processing bin #" << binIndex << " of " << cnt << "..." << endl;
    int *binIndeces = new int[binSize[binIndex]];
    int  placement  = 0;
    isDone = &doneAtStage->lat[0];

    for (int index2 = 0; index2 < MaxXYZ; isDone++, index2++) { // gathers indeces for each bin
      if (*isDone == binIndex + 1) {                            // For every bin type
        binIndeces[placement] = index2;
        placement++;
      }
    }

    // sort bins based on phase value
    if (opt->usePhase) {
      binIndeces = sort(binIndeces, binSize[binIndex]);  // find a better sort algorithm
      cout << "Finished sorting! \n";
    }
    pvbEM *firstCell     = &cmpl->lat[binIndeces[0]];
    ML_CalcType *firstVM = &Vm->lat[binIndeces[0]];
    int currentMaterial  = mat->lat[binIndeces[0]];
    initElphyModel<CTyp>(firstCell, cellmodels[currentMaterial].pep, cellmodels[currentMaterial].emt);  // prepare info
    (*firstCell)->Set(*cellmodels[currentMaterial].prepem);
    if (cellmodels[currentMaterial].emt > EMT_Dummy) {
      for (int x = HLF.lBound(); x <= HLF.uBound(); x++)
        (*firstCell)->AddHeteroValue(HLF[x].key, HLF[x].lat->lat[binIndeces[0]]);
      PreCalcAModel(firstCell, firstVM, binIndeces[0]);  // sends pulse through first cell for time t or set number of
                                                         // cycles
    }

    if (opt->usePhase) {
      ML_CalcType cycleLength   = opt->text; // cycle length
      ML_CalcType degreeTime    = (((2 * M_PI)/360) * cycleLength); // one degree
      ML_CalcType count         = 0;
      ML_CalcType previousPhase = 0;

      for (IndexType index2 = 0; index2 < binSize[binIndex]; index2++) { // iterate over cells in bin
        ML_CalcType *currentVM   = &Vm->lat[binIndeces[index2]];
        pvbEM *currentCell       = &cmpl->lat[binIndeces[index2]];
        ML_CalcType currentPhase = phase->lat[binIndeces[index2]];  // the next phase value
        currentMaterial = mat->lat[binIndeces[index2]];

        // phase info
        ML_CalcType timeDifference = ((currentPhase - previousPhase)/(2 * M_PI)) * (cycleLength);  // the time
                                                                                                   // difference between
                                                                                                   // the two cells
        ML_CalcType timeInCycle    = (previousPhase)/(2 * M_PI) * (cycleLength); // start of the current simulation

        initElphyModel<CTyp>(currentCell, cellmodels[currentMaterial].pep, cellmodels[currentMaterial].emt);  // prepare
                                                                                                              // info
        (*currentCell)->Set(**firstCell);  // transfer previous data
        *currentVM = *firstVM;

        // cout<< "PHASE: " << currentPhase << "\n";

        if (timeDifference > degreeTime) {
          count += timeDifference;

          // run for the time difference
          if (opt->verbose)
            cout<< "PHASE: " << currentPhase << "\n";
          PreCalcAModelPhase(currentCell, currentVM, timeDifference, timeInCycle);  // watch out for vm
          previousPhase = currentPhase;
          (*firstCell)->Set(**currentCell);
          if (opt->verbose) {
            cout<< "NEW VM: " << *currentVM << "\n";
            cout<< "COUNT: " << count << "\n";
          }
          *firstVM = *currentVM;
        }
      }
    } else {
      for (IndexType cellInBin = 1; cellInBin < binSize[binIndex]; cellInBin++) {
        (cmpl->lat[binIndeces[cellInBin]])->Set(**firstCell);
        Vm->lat[binIndeces[cellInBin]] = *firstVM;
      }
    }
    delete[] binIndeces;
  }


  cerr << "Precalculation was finished after "<<cnt<<" steps!\n";
  for (int x = HLF.lBound(); x <= HLF.uBound(); x++) {
    cerr<<"deleting "<<HLF[x].file.c_str()<<endl;
    delete HLF[x].lat;
  }
} // initXCLT::PreCalc

int* initXCLT::sort(int binIndeces[], int size) {
  int n        = size;
  bool swapped = true;

  while (swapped != false) {
    swapped = false;
    for (int i = 1; i < n; i++) {
      if (phase->lat[binIndeces[i]] < phase->lat[binIndeces[i-1]]) {
        int temp = binIndeces[i];
        binIndeces[i]     = binIndeces[i - 1];
        binIndeces[i - 1] = temp;
        swapped           = true;
      }
    }
    n = n - 1;
  }

  return binIndeces;
}

void initXCLT::PreCalcAModel(pvbEM *cmpdata, ML_CalcType *Vm_data, int index) {
  ML_CalcType istim       = opt->amplitude > 0 ? opt->amplitude : (*cmpdata)->GetAmplitude();
  ML_CalcType defStimTime = opt->textlen > 0 ? opt->textlen : (*cmpdata)->GetStimTime();

  ML_CalcType Vm_loc = (*cmpdata)->GetVm();

  cout<< "LOCAL Vm: " << Vm_loc << "\n";

  if (opt->verbose)
    printf("\tindex # %i: stiming for %4.2f ms with I=%4.2f\n", index, (double)defStimTime * 1000, (double)istim);
  ML_CalcType tend      = opt->numStims * opt->text;
  ML_CalcType tinc      = opt->tinc;
  ML_CalcType textbegin = 0;

  for (double t = 0; t < tend; t += tinc) {
    CalcType i_external = ((t >= textbegin) && (t <= textbegin+defStimTime)) ? istim : 0.0;
    ML_CalcType dV      = (*cmpdata)->Calc(tinc, Vm_loc, i_external);
    Vm_loc += dV;

    // cout<< "i_ext: " << i_external << " , dV: " << dV << " , Vm: " << Vm_loc << "\n";
    if (t > textbegin+defStimTime) {
      textbegin += opt->text;
      if (opt->ssi)
        printf("\t\tnext stimulation at t=%4.2f ms...\n", textbegin*1000);
    }
  }

  *Vm_data = Vm_loc;
  cout<< "LOCAL Vm after " << opt->numStims << " full cycles: " << *Vm_data << "\n";
} // initXCLT::PreCalcAModel

// to use previous Vm
void initXCLT::PreCalcAModelPhase(pvbEM *cmpdata, ML_CalcType *Vm_data, ML_CalcType timeDuration,
                                  ML_CalcType timeInCycle) {
  ML_CalcType istim       = opt->amplitude > 0 ? opt->amplitude : (*cmpdata)->GetAmplitude();
  ML_CalcType defStimTime = opt->textlen > 0 ? opt->textlen : (*cmpdata)->GetStimTime();

  ML_CalcType Vm_loc    = *Vm_data;
  ML_CalcType tend      = timeDuration;
  ML_CalcType tinc      = opt->tinc;
  ML_CalcType textbegin = 0;

  for (double t = 0; t < tend; t += tinc) {
    CalcType i_external = ((t+timeInCycle >= textbegin) && (t+timeInCycle <= textbegin+defStimTime)) ? istim : 0.0;
    Vm_loc += (*cmpdata)->Calc(tinc, Vm_loc, i_external);
  }
  *Vm_data = Vm_loc;
}

initXCLT::initXCLT(const char *xcltFile, initXCLTOptions *myOptions, bool loadCMP) {
  try {
    opt = myOptions;
    ifstream in(xcltFile);
    if (!in.is_open())
      throw kaBaseException("can't open %s", xcltFile);

    LatName        = "";
    HLFFile        = "";
    PCAFile        = "";
    removeTmpFiles = true;

    char instr[4096];
    char key[4096];
    char value[4096];
    while (!in.eof()) {
      in.getline(instr, sizeof(instr));
      int rc = sscanf(instr, "%s %s", key, value);
      if (rc == 2) {
        if (!strcmp(key, "Latname")) {
          LatName = value;
        } else if (!strcmp(key, "Material")) {
          char value_temp[4096];
          strncpy(value_temp, value, strlen(value)-3);
          strcat(value_temp, "lat\0");
          LatName = value_temp;
        } else if (!strcmp(key, "HeterogeneousLatticesFile")) {
          HLFFile = value;
        } else if (!strcmp(key, "PCAFile")) {
          PCAFile = value;
        } else if (!strcmp(key, "CellModelFile")) {
          setCMF(value);
        }
      }
    }

    nskaGlobal::FileCheck FC;
    if (!LatName.length())
      throw kaBaseException("LatName not defined in %s!\n", xcltFile);
    FC.Check(LatName.c_str());
    if (!FC.Exist())
      throw kaBaseException("File %s does not exist!", LatName.c_str());

    if (HLFFile.length()) {
      FC.Check(HLFFile.c_str());
      if (!FC.Exist())
        throw kaBaseException("File %s does not exist!", HLFFile.c_str());
      else
        readHLFFile();
    }
    initLattices(PCAFile.length() || loadCMP);
    if (PCAFile.length()) {
      FC.Check(PCAFile.c_str());
      if (!FC.Exist()) {
        throw kaBaseException("File %s does not exist!", PCAFile.c_str());
      } else {
        if (opt->usePCAFile) {
          if (opt->verbose)
            cerr<<"using PCAFile from "<<PCAFile.c_str()<<endl;
          if (!loadBackup(PCAFile.c_str()))
            throw kaBaseException("Error during loading of the backup in ", PCAFile.c_str());
        } else {
          cerr<<"WARNING: PCAFile "<<PCAFile.c_str()<<" defined but the option to use it was not set!\n";
        }
      }
    }
  } catch (kaBaseException& e) {
    cerr << " Error: " << e << endl;
    exit(-1);
  }
}

void initXCLT::setCMF(string cmfilename) {
  cerr<<"reading "<<cmfilename.c_str()<<" ...\n";
  ifstream cmfile(cmfilename.c_str());
  if (cmfile) {
    cerr<<"initializing cell models ...\n";
    nskaGlobal::FileCheck FC;
    for (int i = 0; i < 256; i++)
      cellmodels[i].clear();

    IndexType cmfnr;

    while (!cmfile.eof()) {
      cmfile>>cmfnr;
      cmfile>>cellmodels[cmfnr].emd;
      cmfile>>cellmodels[cmfnr].fmd;

      // cerr<<cmfnr<<"-"<<cellmodels[cmfnr].emd<<"-"<<cellmodels[cmfnr].fmd<<endl;
    }

    IndexType index = 0;
    /* for(index=0;index<256; index++)
        if (cellmodels[index].emd.size()>0){
            int modtyp=0;
            for (modtyp=(int) EMT_Dummy; modtyp<=(int) EMT_Last; modtyp++) {
                //if (checkAndSetIfStdElphy(index, (ElphyModelType) modtyp))
                //    break;
                //if (modtyp==(int) EMT_Last)
                cellmodels[index].emt=FindElphyModelFileType(cellmodels[index].emd.c_str());
                cerr<<cellmodels[index].pep<<endl;
                //initElphyModel<CTyp>(&cellmodels[index].prepem, cellmodels[index].pep, cellmodels[index].emt);
            }

        }*/
    /*        bool isFile;
       for(index=0;index<256; index++){
        isFile = false;
        if (!cellmodels[index].emd.length()){
            cerr<<"setting #"<<index<<" to ElphyDummy\n";
            cellmodels[index].emd=EMT_Dummy;
        }
        cerr<<atoi(cellmodels[index].emd.c_str())<<endl;
        if (atoi(cellmodels[index].emd.c_str()) <= EMT_Last){
            cerr<<"converting "<<cellmodels[index].emd.c_str()<<endl;
            cellmodels[index].emd=FindElphyModelDescriptor(FindElphyModelType(cellmodels[index].emd.c_str()));
            cerr<<FindElphyModelType(cellmodels[index].emd.c_str())<<endl;
            cerr<<"emd="<<cellmodels[index].emd.c_str()<<endl;
        }
        cellmodels[index].emt=FindElphyModelType(cellmodels[index].emd.c_str());
        if (cellmodels[index].emt==EMT_Last){
            cerr<<"was last\n";
            cellmodels[index].emt=FindElphyModelFileType(cellmodels[index].emd.c_str());
            isFile=true;
        }
        cerr<<"index="<<index<<", emt="<<cellmodels[index].emt<<endl;
        cerr<<"initializing class #"<<index<<": "<<FindElphyModelDescriptor(cellmodels[index].emt)<<" model from
           "<<cellmodels[index].emd.c_str()<<" ..."<<endl;
        if (isFile){
            FC.Check(cellmodels[index].emd.c_str());
            if(!FC.Exist())
                throw kaBaseException("file %s, defined in %s does not
                   exist!\n",cellmodels[index].emd.c_str(),cmfilename.c_str());
        }
        initElphyParameters<CTyp>(&cellmodels[index].pep, cellmodels[index].emd.c_str(), cellmodels[index].emt);
        initElphyModel<CTyp>(&cellmodels[index].prepem, cellmodels[index].pep, cellmodels[index].emt);
       }
     */

    /*        for(index=0;index<256; index++){
        if (cellmodels[index].emd.length()){
            //cerr<<cellmodels[index].emd.c_str()<<endl;
            cellmodels[index].emt=FindElphyModelFileType(cellmodels[index].emd.c_str());
            //cerr<<"index="<<index<<", emt="<<cellmodels[index].emt<<endl;
            cerr<<"initializing class #"<<index<<": "<<FindElphyModelDescriptor(cellmodels[index].emt)<<" model from
               "<<cellmodels[index].emd.c_str()<<" ..."<<endl;
            FC.Check(cellmodels[index].emd.c_str());
            if(!FC.Exist())
                throw kaBaseException("file %s, defined in %s does not
                   exist!\n",cellmodels[index].emd.c_str(),cmfilename.c_str());
            initElphyParameters<CTyp>(&cellmodels[index].pep, cellmodels[index].emd.c_str(), cellmodels[index].emt);
            initElphyModel<CTyp>(&cellmodels[index].prepem, cellmodels[index].pep, cellmodels[index].emt);
        }
       }
     */
    for (index = 0; index < 256; index++) {
      // if (opt->verbose)
      // cerr<<"index #"<<index<<": emd="<<cellmodels[index].emd.c_str()<<endl;
      fprintf(stderr, "processed %i of 255 classes ...\r", (int)index);
      if (cellmodels[index].emd.size() > 0) {
        int modtyp = 0;
        for (modtyp = (int)EMT_Dummy; modtyp <= (int)EMT_Last; modtyp++) {
          if (checkAndSetIfStdElphy(index, (ElphyModelType)modtyp))
            break;
          if (modtyp == (int)EMT_Last)
            cellmodels[index].emt = FindElphyModelFileType(cellmodels[index].emd.c_str());
        }
      }
      if (cellmodels[index].emd.length() && opt->verbose)
        cerr<<"#"<<index<<": "<<FindElphyModelDescriptor(cellmodels[index].emt)<<" model with "<<
        cellmodels[index].emd.c_str()<<endl;
      initElphyParameters<CTyp>(&cellmodels[index].pep, cellmodels[index].emd.c_str(), cellmodels[index].emt,
                                opt->tinc);
      initElphyModel<CTyp>(&cellmodels[index].prepem, cellmodels[index].pep, cellmodels[index].emt);
    }
    cerr<<"\n";
  }
} // initXCLT::setCMF

bool initXCLT::checkAndSetIfStdElphy(IndexType ind, ElphyModelType emt_local) {
  const char *descr = FindElphyModelDescriptor(emt_local);
  char temp[3];

  sprintf(temp, "%d", (int)emt_local);
  if ((cellmodels[ind].emd == descr) || (cellmodels[ind].emd == temp) ) {
    cellmodels[ind].emt = emt_local;
    cellmodels[ind].emd = FindElphyModelFileName(emt_local);
    return true;
  } else {
    return false;
  }
}

void initXCLT::initLattices(bool loadCMP) {
  cout << "Beginning to initialize lattices" << endl;
  kaLatticeCreate cls;
  if (LatName.substr(LatName.length()-4, 4) == ".vtu") {
    if (opt->verbose)
      cerr<<"loading material "<<LatName<<" ...";
    vtkSmartPointer<vtkDataSet> mesh = VTKIOHelper::Read(LatName);
    if (opt->verbose)
      cerr<<" done!\n";
    cls.xLattice = mesh->GetNumberOfPoints();
    cls.yLattice = 1;
    cls.zLattice = 1;
    sprintf(cls.Name, "/tmp/init_lattice.%ld.dlat", (long)getpid());
    mat = new kaLattice<u_int8_t>(cls, true);
    if (opt->verbose)
      cerr<<"filling lattice"<<cls.Name<<" ...";
    vtkSmartPointer<vtkUnsignedCharArray> matArray =
      vtkUnsignedCharArray::SafeDownCast(mesh->GetPointData()->GetArray("Material"));
    for (int iPoint = 0; iPoint < mesh->GetNumberOfPoints(); ++iPoint) {
      mat->lat[iPoint] = *(matArray->GetTuple(iPoint));
    }
    if (opt->verbose)
      cerr<<" done!\n";
    vtkSmartPointer<vtkFloatArray> phaseArray = NULL;
    if (opt->usePhase) {
      phaseArray = vtkFloatArray::SafeDownCast(mesh->GetPointData()->GetArray("Phase"));
      sprintf(cls.Name, "/tmp/init_phase.%ld.dlat", (long)getpid());
      phase = new kaLattice<float>(cls, true);
      for (int iPoint = 0; iPoint < mesh->GetNumberOfPoints(); ++iPoint) {
        phase->lat[iPoint] = *(phaseArray->GetTuple(iPoint));
      }
    }
  } else {
    strcpy(cls.Name, LatName.c_str());
    if (opt->verbose)
      cerr<<"loading material "<<cls.Name<<" ...";
    mat = new kaLattice<u_int8_t>(cls);
    if (opt->verbose)
      cerr<<" done!\n";

    // if (opt->msk)
    //   opt->mskLattice->DimMatch(mat);
    cls.xLattice = mat->xLattice;
    cls.yLattice = mat->yLattice;
    cls.zLattice = mat->zLattice;
    if (opt->usePhase) {
      string phaseLatName = LatName.substr(0, LatName.length()-4)+".phase.flat";
      strcpy(cls.Name, phaseLatName.c_str());
      if (opt->verbose)
        cerr<<"loading phase "<<cls.Name<<" ...";
      phase = new kaLattice<float>(cls);
      if (opt->verbose)
        cerr<<" done!\n";
    }
  }

  int MaxXYZ = mat->xyzLattice;

  //! Create a new cell model pointer lattice

  if (loadCMP) {
    strcpy(cls.Name, getCMPFile(false).c_str());
    if (opt->verbose)
      cerr<<"loading CMP "<<cls.Name<<" ...";
    cmpl = new kaLattice<pvbEM>(cls, true);
    if (opt->verbose)
      cerr<<" done!\n";
    (cmpl)->m = mat->m;
    cmpl->DimMatch(cls);
    cmpl->FillWith(NULL);
    cntMaterial = MaxXYZ;


    sprintf(cls.Name, "/tmp/initXCLT_doneAtStage.%ld.ilat", (long)getpid());
    if (opt->verbose)
      cerr<<"loading "<<cls.Name<<" ...";
    doneAtStage = new kaLattice<int>(cls, true);
    if (opt->verbose)
      cerr<<" done!\n";
    (doneAtStage)->m = mat->m;
    doneAtStage->DimMatch(cls);
    doneAtStage->FillWith(0);

    strcpy(cls.Name, getVmFile(false).c_str());
    if (opt->verbose)
      cerr<<"loading "<<cls.Name<<" ...";
    Vm = new kaLattice<ML_CalcType>(cls, true);
    if (opt->verbose)
      cerr<<" done!\n";
    (Vm)->m = mat->m;
    Vm->DimMatch(cls);
    Vm->FillWith(0);
  } else {
    if (opt->verbose)
      cerr<<"skipping initialization of the cellular models and temporary lattices!\n";
  }

  for (int x = HLF.lBound(); x <= HLF.uBound(); x++) {
    if (opt->verbose)
      cerr<<"loading "<<HLF[x].file.c_str()<<" ...";
    strcpy(cls.Name, HLF[x].file.c_str());
    HLF[x].lat = new kaLattice<float>(cls, true);
    if (opt->verbose)
      cerr<<" done!\n";
    HLF[x].lat->m = mat->m;
    HLF[x].lat->DimMatch(cls);
  }

  cout << "Finished initializing lattices!";
} // initXCLT::initLattices

void initXCLT::readHLFFile(void) {
  cerr<<"reading "<<HLFFile.c_str()<<" ...\n";
  nskaGlobal::FileCheck FC;
  ifstream in(HLFFile.c_str());
  char instr[4096];
  char file[4096];
  char key[4096];
  float allowedVariance;
  HLFInfo *myHLF;
  while (!in.eof()) {
    in.getline(instr, sizeof(instr));
    int rc = sscanf(instr, "%s %s %f", file, key, &allowedVariance);
    if ((rc == 2) || (rc == 3)) {
      FC.Check(file);
      if (FC.Exist()) {
        myHLF       = new HLFInfo;
        myHLF->file = file;
        myHLF->key  = key;
        if (rc == 3)
          myHLF->allowedVarianceForInitializeXCLT = allowedVariance;
        HLF.addEntry(*myHLF);
      } else {
        throw kaBaseException("file %s, defined in %s does not exist!", file, HLFFile.c_str());
      }
    }
  }
} // initXCLT::readHLFFile
