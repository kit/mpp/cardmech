/*
 *  initializeXCLT.cpp
 *  initializeXCLT
 *
 *  Created by dw on 02.08.07.
 *  Copyright 2007 IBT Uni Karlsruhe. All rights reserved.
 *
 */

/* Changelog of initializeXCLT.cpp
 *
 * 17.10.2007 - changes by mi096
 *
 * - added deleting of temporary files if existent at program start
 * - added output of program version
 *
 * 22.10.2007 - changes by dw
 *
 * - temporary file names include the process id and were only kept if the -keep option was set
 * - *.pca file will be written to the user's home directory instead of /tmp/
 * - altered output format
 *
 * 21.02.2008 - changes by dw
 *
 * - generates backup files for acCELLerate (if the -oldStyle option is not set)
 *
 * 20.08.2009 - changey by cr116
 *
 * - added support for .aclt files
 * - usePCA bug (segmentation fault) fixed
 */

#include <kaParser.h>
#include <initXCLTOptions.h>
#include <initXCLT.h>

const string programVersion = "1.7 / 20.08.2009";

PrintParameterModus PrintParameterMode = PrintParameterModeOff;

int main(int argc, char **argv) {
  cerr << argv[0] << " Version " << programVersion << endl;

  // insert code here...
  if (argc < 2) {
    cerr << argv[0] << " <xcltFile>" << endl;
    cerr << "\t[-tinc\t\t<double: internal incrementation of the cellular model>; default: .00001]" << endl;
    cerr << "\t[-text\t\t<double: basic cycle length>; default: 1]" << endl;
    cerr << "\t[-textlen\t<double: length of one stimulation>; default: value from the cellular model]" << endl;
    cerr << "\t[-amplitude\t<double: height / amplitude of one stimulation>; default: value from the cellular model]" <<
    endl;
    cerr <<
    "\t[-variance\t<double: global allowed variance in percent to find similar parameter sets>; default: 0]\n\t\t\t\t\t(parameter specific values defined in the third column of the *.hlf file will not be overwritten)"
         << endl;
    cerr << "\t[-ns\t\t<int: number of stimulations that will be calculated>; default: 10]" << endl;

    //        cerr << "\t[-msk <mskLattice> <msk>\t<mask lattice: the mask defines the processed range>]" << endl;
    cerr << "\t[-o <filename>\t<filename of the output>]" << endl;
    cerr << "\t[-ssi\t\t<show stimulation info: shows additional information about the stimulation>]" << endl;
    cerr <<
    "\t[-usePCA\t<uses previously pre-calculated values when the link to the PCAFile was found in the xcltFile>]" <<
    endl;
    cerr <<
    "\t[-keep\t\t<keeps the temporary file /tmp/initXCLT_Vm.PID.dlat and /tmp/initXCLT_doneAtStage.PID.dlat>]" << endl;
    cerr << "\t[-oldStyle\t<generates an backup for xCELLent>]" << endl;
    cerr <<
    "\t[-phase\t\t<look for point array \"Phase\" or name.phase.flat respectively and initialize time as well>]" <<
    endl;
    cerr << "\t[-verbose\t<enables verbose mode>]" << endl;
    return 0;
  }

  try {
    cerr << "Setting environment variable kaNoShm for this process ...";
    if (0 == setenv("kaNoShm", "", 1) )
      cout << "OK!" << endl;
    else
      cout << "Failed!" << endl;

    kaParser CL(argc, argv);
    initXCLTOptions opt(.00001, 1.0, -1, -1, 10, 0);
    bool keep = false;
    if (CL.IsOption("-tinc"))
      opt.tinc = atof(CL.GetOption("-tinc", 1));
    if (CL.IsOption("-text"))
      opt.text = atof(CL.GetOption("-text", 1));
    if (CL.IsOption("-textlen"))
      opt.textlen = atof(CL.GetOption("-textlen", 1));
    if (CL.IsOption("-amplitude"))
      opt.amplitude = atof(CL.GetOption("-amplitude", 1));
    if (CL.IsOption("-variance"))
      opt.allowedVariance = atof(CL.GetOption("-variance", 1));
    if (CL.IsOption("-ns"))
      opt.numStims = atof(CL.GetOption("-ns", 1));
    if (CL.IsOption("-verbose"))
      opt.verbose = true;
    if (CL.IsOption("-usePCA"))
      opt.usePCAFile = true;
    if (CL.IsOption("-ssi"))
      opt.ssi = true;
    if (CL.IsOption("-keep"))
      keep = true;
    if (CL.IsOption("-oldStyle"))
      opt.oldStyle = true;
    /*        if (CL.IsOption("-msk")){
                opt.msk = new kaMask(false);
                            opt.msk->Set(CL.GetArg(CL.GetArgNum("-msk")+2), true);
                kaLatticeCreate cls;
                sprintf(cls.Name,CL.GetArg(CL.GetArgNum("-msk")+1));
                opt.mskLattice = new kaLattice<u_int8_t>(cls);
            }*/
    if (CL.IsOption("-o"))
      opt.output = CL.GetOption("-o", 1);
    if (CL.IsOption("-phase"))
      opt.usePhase = true;
    initXCLT my(argv[1], &opt, true);
    my.removeTmpFiles = !keep;

    my.PreCalc();
    my.writeBackup();

    // opt.show();
    return 0;
  } catch (kaBaseException& e) {
    cerr << argv[0] << " Error: " << e << endl;
    exit(-1);
  }
} // main
