/*
 *  Condition2ConditionSub.h
 *  Condition2Condition
 *
 *  Created by Daniel Weiss on 27.01.06.
 *  Copyright 2006 IBT Universitt Karlsruhe. All rights reserved.
 *
 */

typedef double FDTyp;

#include <LatticeStuff.h>
#include <DomainConditions.h>
#include <dwArray.h>

enum ampLeftType { AL_min = 1, AL_max = 2 };

class Condition {
 public:
  Condition(int Px, int Py, int Pz, double Amp, SingleCondition<FDTyp, BaseCondition> H, char ct[2], int);
  Condition() {  // cerr<<"do nothing\n";
  }

  void Print(bool);

  kaPoint<int> P;
  int petscindex;
  double amp;
  SingleCondition<FDTyp, BaseCondition> h;
  char CT[2];
};

class Cond2Cond {
 public:
  Cond2Cond(int argc, char *argv[]);
  ~Cond2Cond();
  void Convert(char *srcLat, char *destLat);

 private:
  ifstream *in;
  bool verbose;
  bool setAmp;
  bool setToff;
  bool setTlen;
  bool setText;
  bool setPETSc;
  bool only_if_in_tissue;
  double fAmp;
  double Amp;
  double Toff;
  double Tlen;
  double Text;
  kaLattice<uint8_t> *dLat;
  kaLattice<uint8_t> *sLat;
  void convertCondition(char *buffer);
  dwArray<Condition> myConds;
  ampLeftType ampLeft;
  bool processAll;
  bool addMsgShown;
  int cntInArray;
}; // class Cond2Cond
