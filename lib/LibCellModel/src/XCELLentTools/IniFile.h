/**@file IniFile.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef INIFILE_H
#define INIFILE_H

#include <kaExceptions.h>

const int dim           = 128;
const char nothing[dim] = "NOTHING";
char option[dim];
char retValue[dim];

bool getLine(ifstream& in, char a[dim], char b[dim]) {
  char instr[1024];

  do {
    in.getline(instr, sizeof(instr));
    if (in.eof())
      return false;
  } while (instr[0] == '#');
  sscanf(instr, "%s\t%s", a, b);

  // cerr<<"instr="<<instr<<", a="<<a<<", b="<<b<<endl;
  return true;
}

bool FindOptionValue(const char *infile, char Option[dim], char retValue[dim], bool relevant = false) {
  ifstream in(infile);

  if (!in)
    throw kaBaseException("cannot open %s!\n", infile);
  char targetOption[dim+2];
  char option[dim+2];
  char value[dim];
  char ret[dim];
  strcpy(ret, nothing);
  sprintf(targetOption, "[%s]", Option);
  while (getLine(in, option, value))
    if (!strcmp(targetOption, option))
      sprintf(ret, "%s", value);
  strcpy(retValue, ret);
  if (!strcmp(retValue, nothing))
    if (relevant)
      throw kaBaseException("can't read relevant option %s from %s -> exiting", targetOption, infile);
    else
      return false;
  else
    return true;
}

int FindOptionValue(const char *infile, char Option[dim], bool relevant) {
  char ret[dim];

  if (FindOptionValue(infile, Option, ret, relevant))
    return atoi(ret);

  return -1;
}

#endif // ifndef INIFILE_H
