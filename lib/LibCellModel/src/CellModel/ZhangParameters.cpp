/*! \file ZhangParameters.cpp
   \brief Implementation of Zhang et al sinus node model
   AJP 2000, JCE 2002 (implementation partial), JCE 2003

   \author unknown, Universitaet Karlsruhe (TH)
 */


#include <ZhangParameters.h>


ZhangParameters::ZhangParameters(const char *initFile, ML_CalcType tinc) {
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

void ZhangParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "ZhangParameters:" << endl;
  for (int i = vtFirst; i != vtLast; i++)
    cout << P[i].name << "\t" << P[i].value << endl;
}

void ZhangParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "ZhangParameters:init " << initFile << endl;
#endif // if KADEBUG

  // Initialization of the Parameters ...
  P[VT_dCell].name = "dCell";
  P[VT_R].name = "R";
  P[VT_Tx].name = "Tx";
  P[VT_F].name = "F";
  P[VT_Nao].name = "Nao";
  P[VT_Nai].name = "Nai";
  P[VT_Ko].name = "Ko";
  P[VT_Ki].name = "Ki";
  P[VT_Cao].name = "Cao";
  P[VT_ECaL].name = "ECaL";
  P[VT_ECaT].name = "ECaT";
  P[VT_Vcell].name = "Vcell";

  P[VT_CmCenter].name = "CmCenter";
  P[VT_CmPeriphery].name = "CmPeriphery";
  P[VT_gNaCenter].name = "gNaCenter";
  P[VT_gNaPeriphery].name = "gNaPeriphery";
  P[VT_gCaLCenter].name = "gCaLCenter";
  P[VT_gCaLPeriphery].name = "gCaLPeriphery";
  P[VT_gCaTCenter].name = "gCaTCenter";
  P[VT_gCaTPeriphery].name = "gCaTPeriphery";
  P[VT_gtoCenter].name = "gtoCenter";
  P[VT_gtoPeriphery].name = "gtoPeriphery";
  P[VT_gsusCenter].name = "gsusCenter";
  P[VT_gsusPeriphery].name = "gsusPeriphery";
  P[VT_gKrCenter].name = "gKrCenter";
  P[VT_gKrPeriphery].name = "gKrPeriphery";
  P[VT_tau_pi].name = "tau_pi";
  P[VT_gKsCenter].name = "gKsCenter";
  P[VT_gKsPeriphery].name = "gKsPeriphery";
  P[VT_gfNaCenter].name = "gfNaCenter";
  P[VT_gfNaPeriphery].name = "gfNaPeriphery";
  P[VT_gfKCenter].name = "gfKCenter";
  P[VT_gfKPeriphery].name = "gfKPeriphery";
  P[VT_gbNaCenter].name = "gbNaCenter";
  P[VT_gbNaPeriphery].name = "gbNaPeriphery";
  P[VT_gbCaCenter].name = "gbCaCenter";
  P[VT_gbCaPeriphery].name = "gbCaPeriphery";
  P[VT_gbKCenter].name = "gbKCenter";
  P[VT_gbKPeriphery].name = "gbKPeriphery";
  P[VT_kNaCaCenter].name = "kNaCaCenter";
  P[VT_kNaCaPeriphery].name = "kNaCaPeriphery";
  P[VT_yNaCa].name = "yNaCa";
  P[VT_dNaCa].name = "dNaCa";
  P[VT_ipmaxCenter].name = "ipmaxCenter";
  P[VT_ipmaxPeriphery].name = "ipmaxPeriphery";
  P[VT_KmNa].name = "KmNa";
  P[VT_KmK].name = "KmK";
  P[VT_init_h1].name = "init_h1";
  P[VT_init_h2].name = "init_h2";
  P[VT_init_m].name = "init_m";
  P[VT_init_fL].name = "init_fL";
  P[VT_init_dL].name = "init_dL";
  P[VT_init_dT].name = "init_dT";
  P[VT_init_fT].name = "init_fT";
  P[VT_init_q].name = "init_q";
  P[VT_init_r].name = "init_r";
  P[VT_init_paf].name = "init_paf";
  P[VT_init_pas].name = "init_pas";
  P[VT_init_pi_m].name = "init_pi_m";
  P[VT_init_xs].name = "init_xs";
  P[VT_init_y].name = "init_y";
  P[VT_init_Cai].name = "init_Cai";
  P[VT_init_Vm].name = "init_Vm";
  P[VT_paf_inf1].name = "paf_inf1";
  P[VT_paf_inf2].name = "paf_inf2";
  P[VT_tau_paf1].name = "tau_paf1";
  P[VT_tau_paf2].name = "tau_paf2";
  P[VT_tau_paf3].name = "tau_paf3";
  P[VT_tau_paf4].name = "tau_paf4";
  P[VT_tau_paf5].name = "tau_paf5";
  P[VT_KQ10_tau_paf].name = "KQ10_tau_paf";
  P[VT_tau_pas1].name = "tau_pas1";
  P[VT_tau_pas2].name = "tau_pas2";
  P[VT_tau_pas3].name = "tau_pas3";
  P[VT_tau_pas4].name = "tau_pas4";
  P[VT_tau_pas5].name = "tau_pas5";
  P[VT_pi_inf1].name = "pi_inf1";
  P[VT_pi_inf2].name = "pi_inf2";
  P[VT_KQ10_tau_pas].name = "KQ10_tau_pas";
  P[VT_C1FCell].name = "C1FCell";
  P[VT_C2FCell].name = "C2FCell";
  P[VT_C3FCell].name = "C3FCell";
  P[VT_C4FCell].name = "C4FCell";
  P[VT_C5FCell].name = "C5FCell";

#ifdef ZHANG03
  P[VT_ACh].name    = "ACh";
  P[VT_bmax].name   = "bmax";
  P[VT_kCaACh].name = "kCaACh";
  P[VT_smax].name   = "smax";
  P[VT_nfACh].name  = "nfACh";
  P[VT_kfACh].name  = "kfACh";
  P[VT_gKACh].name  = "gKACh";
  P[VT_nKACh].name  = "nKACh";
  P[VT_kKACh].name  = "kKACh";
  P[VT_j].name      = "j";
  P[VT_k].name      = "k";
#endif // ifdef ZHANG03

  P[VT_RTdF].name = "RTdF";
  P[VT_ENa].name = "ENa";
  P[VT_EK].name = "EK";
  P[VT_EKs].name = "EKs";
  P[VT_FCell].name = "dCell";
  P[VT_Cm].name = "Cm";
  P[VT_gNa].name = "gNa";
  P[VT_gCaL].name = "gCaL";
  P[VT_gCaT].name = "gCaT";
  P[VT_gto].name = "gto";
  P[VT_gsus].name = "gsus";
  P[VT_gKr].name = "gKr";
  P[VT_gKs].name = "gKs";
  P[VT_gfNa].name = "gfNa";
  P[VT_gfK].name = "gfK";
  P[VT_gbNa].name = "gbNa";
  P[VT_gbCa].name = "gbCa";
  P[VT_gbK].name = "gbK";
  P[VT_kNaCa].name = "kNaCa";
  P[VT_ipmax].name = "ipmax";
  P[VT_Amp].name = "Amp";

  P[VT_RTdF].readFromFile = false;
  P[VT_ENa].readFromFile = false;
  P[VT_EK].readFromFile = false;
  P[VT_EKs].readFromFile = false;
  P[VT_FCell].readFromFile = false;
  P[VT_Cm].readFromFile = false;
  P[VT_gNa].readFromFile = false;
  P[VT_gCaL].readFromFile = false;
  P[VT_gCaT].readFromFile = false;
  P[VT_gto].readFromFile = false;
  P[VT_gsus].readFromFile = false;
  P[VT_gKr].readFromFile = false;
  P[VT_gKs].readFromFile = false;
  P[VT_gfNa].readFromFile = false;
  P[VT_gfK].readFromFile = false;
  P[VT_gbNa].readFromFile = false;
  P[VT_gbCa].readFromFile = false;
  P[VT_gbK].readFromFile = false;
  P[VT_kNaCa].readFromFile = false;
  P[VT_ipmax].readFromFile = false;


  ParameterLoader EPL(initFile, EMT_Zhang);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
  InitTable(tinc);
} // ZhangParameters::Init

void ZhangParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "#ZhangParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_RTdF].value = P[VT_R].value * (P[VT_Tx].value) / (P[VT_F].value);
  P[VT_ENa].value = P[VT_RTdF].value * log(P[VT_Nao].value / (P[VT_Nai].value));
  P[VT_EK].value = P[VT_RTdF].value * log(P[VT_Ko].value / (P[VT_Ki].value));
  P[VT_EKs].value = P[VT_RTdF].value *
                    log((P[VT_Ko].value + 0.03 * (P[VT_Nao].value)) /
                        (P[VT_Ki].value + 0.03 * (P[VT_Nai].value)));
  P[VT_FCell].value = (P[VT_C1FCell].value * P[VT_dCell].value) /
                      (1 + (P[VT_C2FCell].value * exp((-((P[VT_C3FCell].value * P[VT_dCell].value) -
                                                         P[VT_C4FCell].value)) /
                                                      P[VT_C5FCell].value)));
  P[VT_Cm].value =
      P[VT_CmCenter].value + (P[VT_FCell].value * (P[VT_CmPeriphery].value - P[VT_CmCenter].value));
  P[VT_gNa].value = P[VT_gNaCenter].value +
                    (P[VT_FCell].value * (P[VT_gNaPeriphery].value - P[VT_gNaCenter].value));
  P[VT_gCaL].value = P[VT_gCaLCenter].value +
                     (P[VT_FCell].value * (P[VT_gCaLPeriphery].value - P[VT_gCaLCenter].value));
  P[VT_gCaT].value = P[VT_gCaTCenter].value +
                     (P[VT_FCell].value * (P[VT_gCaTPeriphery].value - P[VT_gCaTCenter].value));
  P[VT_gto].value = P[VT_gtoCenter].value +
                    (P[VT_FCell].value * (P[VT_gtoPeriphery].value - P[VT_gtoCenter].value));
  P[VT_gsus].value = P[VT_gsusCenter].value +
                     (P[VT_FCell].value * (P[VT_gsusPeriphery].value - P[VT_gsusCenter].value));
  P[VT_gKr].value = P[VT_gKrCenter].value +
                    (P[VT_FCell].value * (P[VT_gKrPeriphery].value - P[VT_gKrCenter].value));
  P[VT_gKs].value = P[VT_gKsCenter].value +
                    (P[VT_FCell].value * (P[VT_gKsPeriphery].value - P[VT_gKsCenter].value));
  P[VT_gfNa].value = P[VT_gfNaCenter].value +
                     (P[VT_FCell].value * (P[VT_gfNaPeriphery].value - P[VT_gfNaCenter].value));
  P[VT_gfK].value = P[VT_gfKCenter].value +
                    (P[VT_FCell].value * (P[VT_gfKPeriphery].value - P[VT_gfKCenter].value));
  P[VT_gbNa].value = P[VT_gbNaCenter].value +
                     (P[VT_FCell].value * (P[VT_gbNaPeriphery].value - P[VT_gbNaCenter].value));
  P[VT_gbCa].value = P[VT_gbCaCenter].value +
                     (P[VT_FCell].value * (P[VT_gbCaPeriphery].value - P[VT_gbCaCenter].value));
  P[VT_gbK].value = P[VT_gbKCenter].value +
                    (P[VT_FCell].value * (P[VT_gbKPeriphery].value - P[VT_gbKCenter].value));
  P[VT_kNaCa].value = P[VT_kNaCaCenter].value +
                      (P[VT_FCell].value * (P[VT_kNaCaPeriphery].value - P[VT_kNaCaCenter].value));
  P[VT_ipmax].value = P[VT_ipmaxCenter].value +
                      (P[VT_FCell].value * (P[VT_ipmaxPeriphery].value - P[VT_ipmaxCenter].value));
} // ZhangParameters::Calculate

void ZhangParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr << "ZhangParameters - InitTable()" << endl;
#endif // if KADEBUG
  tinc *= -1.0;  // neg. for exptau calculation
  for (double V = -RangeTabhalf + .0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int) (DivisionTab * (RangeTabhalf + V) + .5);
    FNa[Vi] = 9.518e-2 * exp(-6.306e-2 * (V + 34.4)) / (1.0 + 1.662 * exp(-0.2251 * (V + 63.7))) +
              8.693e-2;
    CCaL[Vi] = 0.006 / (1.0 + exp(-(V + 14.1) / 6.0));
    double ipNai = P[VT_Nai].value / (P[VT_KmNa].value + P[VT_Nai].value);
    double KoKmK = P[VT_Ko].value / (P[VT_KmK].value + P[VT_Ko].value);
    Cip[Vi] = P[VT_ipmax].value * ipNai * ipNai * ipNai * KoKmK * KoKmK * 1.6 /
              (1.5 + exp(-(V + 60.0) / 40.0));
    m_inf[Vi] = pow(1.0 / (1.0 + exp(-(V + 30.32) / 5.46)), 1.0 / 3.0);
    exptau_m[Vi] =
        exp(tinc /
            (((6.247E-4) / ((0.8322166 * exp((-0.33566) * (V + (56.7062)))) +
                            (0.6274 * exp(0.0823 * (V + (65.0131)))))) + (4.569E-5)));
    h1_inf[Vi] = 1.0 / (1.0 + exp((V + 66.1) / 6.4));
    h2_inf[Vi] = h1_inf[Vi];
    exptau_h1[Vi] =
        exp(tinc / (((((3.717E-6) * exp((-0.2815) * (V + (17.11)))) /
                      (1 + ((0.003732) * exp((-0.3426) * (V + (37.76)))))) + (5.977E-4))));
    exptau_h2[Vi] =
        exp(tinc / (((((3.186E-8) * exp((-0.6219) * (V + (18.8)))) /
                      (1 + ((7.189E-5) * exp((-0.6683) * (V + (34.07)))))) + (0.003556))));

    double alpha_dL =
        -28.39 * (V + 35.0) / (exp(-(V + 35.0) / 2.5) - 1.0) - 84.9 * V / (exp(-0.208 * V) - 1.0);
    double beta_dL = 11.43 * (V - 5) / (exp(0.4 * (V - 5.0)) - 1.0);
    exptau_dL[Vi] = exp(tinc / 2.0 * (alpha_dL + beta_dL));
    dL_inf[Vi] = 1.0 / (1.0 + exp(-(V + 22.3 + 0.8 * (P[VT_FCell].value)) / 6.0));
    double alpha_fL = 3.75 * (V + 28.0) / (exp((V + 28.0) / 4.0) - 1.0);
    double beta_fL = 30.0 / (1.0 + exp(-(V + 28.0) / 4.0));
    exptau_fL[Vi] = exp(tinc / ((1.2 - 0.2 * (P[VT_FCell].value)) / (alpha_fL + beta_fL)));
    fL_inf[Vi] = 1.0 / (1.0 + exp((V + 45.0) / 5.0));

    double alpha_dT = 1068.0 * exp((V + 26.3) / 30.0);
    double beta_dT = 1068.0 * exp(-(V + 26.3) / 30.0);
    exptau_dT[Vi] = exp(tinc * (alpha_dT + beta_dT));
    dT_inf[Vi] = 1.0 / (1.0 + exp(-(V + 37.0) / 6.8));
    double alpha_fT = 15.3 * exp(-(V + 71.0 + 0.7 * (P[VT_FCell].value)) / 83.3);
    double beta_fT = 15.0 * exp((V + 71.0) / 15.38);
    exptau_fT[Vi] = exp(tinc * (alpha_fT + beta_fT));
    fT_inf[Vi] = 1.0 / (1.0 + exp((V + 71.0) / 9.0));

    q_inf[Vi] = 1.0 / (1.0 + exp((V + 59.37) / 13.1));
    exptau_q[Vi] =
        exp(tinc /
            (((0.001) / 3.0) *
             ((30.31) +
              ((195.5) /
               ((0.5686 * exp(((-1) * (0.08161)) * ((V + (39.0)) + (10 * P[VT_FCell].value)))) +
                ((0.7174) * exp(((0.2719) - ((0.1719) * P[VT_FCell].value)) *
                                ((V + (40.93)) + ((10) * P[VT_FCell].value)))))))));
    r_inf[Vi] = 1.0 / (1.0 + exp(-(V - 10.93) / 19.7));
    exptau_r[Vi] =
        exp(tinc / (0.0025 * ((1.191) + ((7.838) / ((1.037 * exp((0.09012) * (V + (30.61)))) +
                                                    (0.369 * exp((-0.119) * (V + (23.84)))))))));

    paf_inf[Vi] = 1.0 / (1.0 + exp(-(V + P[VT_paf_inf1].value) / P[VT_paf_inf2].value));
    pas_inf[Vi] = paf_inf[Vi];
    exptau_paf[Vi] =
        exp(tinc /
            (P[VT_KQ10_tau_paf].value /
             (P[VT_tau_paf1].value * exp((V - P[VT_tau_paf2].value) / P[VT_tau_paf3].value) +
              P[VT_tau_paf4].value *
              exp(-(V - P[VT_tau_paf2].value) / P[VT_tau_paf5].value))));
    exptau_pas[Vi] =
        exp(tinc /
            (P[VT_KQ10_tau_pas].value /
             (P[VT_tau_pas1].value * exp((V - P[VT_tau_pas2].value) / P[VT_tau_pas3].value) +
              P[VT_tau_pas4].value *
              exp(-(V - P[VT_tau_pas2].value) / P[VT_tau_pas5].value))));
    pi_inf[Vi] = 1.0 / (1.0 + exp((V + P[VT_pi_inf1].value) / P[VT_pi_inf2].value));

    const ML_CalcType alpha_xs = 14.0 / (1.0 + exp(-(V - 40.0) / 9.0));
    const ML_CalcType beta_xs = exp(-V / 45.0);
    xs_inf[Vi] = alpha_xs / (alpha_xs + beta_xs);
    exptau_xs[Vi] = exp(tinc * (alpha_xs + beta_xs));

#ifdef ZHANG03
    beta_j[Vi] = 120./(1.*exp(-(V+50.)/15.));
    beta_k[Vi] = 5.82/(1.*exp(-(V+50.)/15.));
    const double sAChn = pow(P[VT_ACh].value, P[VT_nfACh].value);
    const double s     = P[VT_smax].value*sAChn/(sAChn+pow(P[VT_kKACh].value, P[VT_nfACh].value));
    double alpha_y     = exp(-(V+78.91-s)/26.63);
    double beta_y      = exp((V+75.13-s)/21.25);
#else // ifdef ZHANG03
    double alpha_y = exp(-(V + 78.91) / 26.63);
    double beta_y = exp((V + 75.13) / 21.25);
#endif // ifdef ZHANG03
    y_inf[Vi] = alpha_y / (alpha_y + beta_y);
    exptau_y[Vi] = exp(tinc * (alpha_y + beta_y));
  }
} // ZhangParameters::InitTable
