/**@file PetersonParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <PetersonParameters.h>

PetersonParameters::PetersonParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void PetersonParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "PetersonParameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void PetersonParameters::Init(const char *initFile) {
  P[VT_P1a].name = "P1";
  P[VT_P0a].name = "P0";
  P[VT_N1a].name = "N1";
  P[VT_p_k_on].name = "p_k_on";
  P[VT_p_k_on_s].name = "p_k_on_s";
  P[VT_p_k_off].name = "p_k_off";
  P[VT_p_k_off_s].name = "p_k_off_s";
  P[VT_p_f].name = "p_f";
  P[VT_p_g].name = "p_g";
  P[VT_p_g_s].name = "p_g_s";

  P[VT_Fmax].name = "Fmax";
  P[VT_dFmax].name = "dFmax";
  P[VT_Fmax].readFromFile = false;
  P[VT_dFmax].readFromFile = false;

  ParameterLoader FPL(initFile, FMT_Peterson);
  this->setOverlapParameters(FPL.getOverlapString());
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = FPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
}

void PetersonParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();

  P[VT_Fmax].value =
      ((P[VT_p_f].value) * (P[VT_p_k_on].value) * 100 *
       ((P[VT_p_g_s].value) + (P[VT_p_k_on_s].value) * 100 + (P[VT_p_k_off_s].value))) /
      ((P[VT_p_f].value) * (P[VT_p_k_on].value) * 100 * (P[VT_p_k_off_s].value) +
       (P[VT_p_g_s].value) *
       ((P[VT_p_f].value) * (P[VT_p_k_on].value) * 100 +
        (P[VT_p_g].value) * ((P[VT_p_k_off].value) + (P[VT_p_k_on].value) * 100) +
        (P[VT_p_k_off_s].value) *
        ((P[VT_p_f].value) + (P[VT_p_k_off].value) + (P[VT_p_k_on].value) * 100)) +
       ((P[VT_p_f].value) * (P[VT_p_k_on].value) * 100 +
        (P[VT_p_g].value) * ((P[VT_p_k_off].value) + (P[VT_p_k_on].value) * 100)) *
       (P[VT_p_k_on_s].value) * 100);

  // maximale Kraft bei 100 uM Calcium und stretch=1
  P[VT_dFmax].value = 1.0 / (P[VT_Fmax].value);
}
