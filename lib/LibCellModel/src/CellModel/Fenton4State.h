/*      File: Fenton4State.h */

/*  Version 1.1 (2008-06-25) by fmw */

/*      Version 1.1 (2008-06-25)

 * Included 3 new ev-file parameters: Vol, Amp, and StimTime
 * LongPrint() now outputs J_fi, J_si and J_so
 */

#ifndef FENTON4STATE
#define FENTON4STATE

#include <Fenton4StateParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_Fenton4StateParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pCmP->P[NS_Fenton4StateParameters::a].value
#endif // ifdef HETERO

class Fenton4State : public vbElphyModel<ML_CalcType> {
public:
  Fenton4StateParameters *pCmP;
  ML_CalcType u, v, w, s;
  ML_CalcType J_fi_tinc, J_so_tinc, J_si_tinc;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  Fenton4State(Fenton4StateParameters *pp);

  ~Fenton4State();

  virtual inline bool AddHeteroValue(string desc, double val);

  /*    virtual inline  ML_CalcType Volume(){ return  1.208*10e-15 ;};
      virtual inline  ML_CalcType GetAmplitude() { return (13); };              // 0.15 ???
      virtual inline  ML_CalcType GetStimTime() { return (0.003); };    // 0.003
   */
  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vol); } // before: return 1.208*10e-15;

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }              // 0.15 ???

  virtual inline ML_CalcType GetStimTime() { return CELLMODEL_PARAMVALUE(VT_StimTime); } // 0.003

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_Vm_init); }         // ??

  virtual inline ML_CalcType GetCai() { return 0; }

  virtual inline ML_CalcType GetCao() { return 0; }

  virtual inline ML_CalcType GetNai() { return 0; }

  virtual inline ML_CalcType GetNao() { return 0; }

  virtual inline ML_CalcType GetKi() { return 0; }

  virtual inline ML_CalcType GetKo() { return 0; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &u; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                           int euler);

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class Fenton4State
#endif // ifndef FENTON4STATE
