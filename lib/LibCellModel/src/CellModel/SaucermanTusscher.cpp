/*! \file SaucermanTusscher.cpp
   \human cell model of ten Tusscher including Saucermans intracellular model adrenergic signaling

   \version 0.0.0

   \date Created Carola Otto (21.02.2009)\n
   template (00.00.00)\n
   man Gunnar Seemann (27.02.03)\n
   Last Modified Eike Wuelfers (03.02.10)

   \author Carola Otto\n
   Institute of Biomedical Engineering\n
   Universitaet Karlsruhe (TH)\n
   http://www.ibt.uni-karlsruhe.de\n
   Copyright 2000-2009 - All rights reserved.

   // \sa  \ref SaucermanTusscher
 */

#include "SaucermanTusscher.h"

// Prototypes of functions called by IDA
static int fT(realtype t, N_Vector w, N_Vector wd, N_Vector rr, void *rdata);

static void PrintFinalStats(void *ida_mem);

static int check_flag(void *flagvalue, char *funcname, int opt);

#define useCaTab

SaucermanTusscher::SaucermanTusscher(SaucermanTusscherParameters *pp) {
  ptTeaP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(ptTeaP, NS_SaucermanTusscherParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

SaucermanTusscher::~SaucermanTusscher() {
  // Free w, wd, id and constraints vector
  N_VDestroy_Serial(id);

  // N_VDestroy_Serial(constraints);
  N_VDestroy_Serial(w);
  N_VDestroy_Serial(wd);

  // Free integrator memory
  IDAFree(&ida_mem);
}

#ifdef HETERO

inline bool SaucermanTusscher::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool SaucermanTusscher::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

/*! \fn int SaucermanTusscher::GetSize(void)
 *  \brief Return the size of the memory that has to be included into the backup.
 *  \return Size of memory to be used for backup.
 *  This function returns the size of memory to use for the backup snapshot. Be sure to exclude all variables that are
 *not to be backuped.
 *  These are especially the variables used for modeling ischemia and the InitTableDone variable. If this was not done a
 *cell model inited
 *  with pcafile option and a memory snapshot would not be able to use values from an ev-file or through the init
 *function.
 */
inline int SaucermanTusscher::GetSize(void) {
  // -sizeof(bool) is for excluding the InitTableDone variable from the backup
  // be aware of 32 and 64 bit:
  /*
     64 bit:

     sizeof(SaucermanTusscher) = 656
     sizeof(vbElphyModel<ML_CalcType>) = 8
     sizeof(SaucermanTusscherParameters *) = 8
     sizeof(ParameterSwitch *) = 8
     sizeof( ML_CalcType ) = 8

     32 bit:

     sizeof(SaucermanTusscher) = 648
     sizeof(vbElphyModel<ML_CalcType>) = 4
     sizeof(SaucermanTusscherParameters *) = 4
     sizeof(ParameterSwitch *) = 4
     sizeof( ML_CalcType ) = 8
   */

  return (&Vm_old - &Ca_i + 1) * sizeof(ML_CalcType);
}

inline ML_CalcType *SaucermanTusscher::GetBase(void) {
  return &Ca_i;
}

inline unsigned char SaucermanTusscher::getSpeed(ML_CalcType adVm) {
  return (unsigned char) 5;
}

void SaucermanTusscher::alloc_ida() {
  ida_mem = NULL;
  w = wd = id = NULL;
  constraints = NULL;


  neq = 49;
  tout1 = 0.000001;

  y0[0] = trel;
  y0[1] = L;
  y0[2] = R;
  y0[3] = Gs;
  y0[4] = b1ARtot;
  y0[5] = b1ARd;
  y0[6] = b1ARp;
  y0[7] = Gsagtptot;
  y0[8] = Gsagdp;
  y0[9] = Gsbg;

  // cAMP module
  y0[10] = Gsa_gtp;
  y0[11] = Fsk;
  y0[12] = AC;
  y0[13] = PDE;
  y0[14] = IBMX;
  y0[15] = cAMPtot;

  // PKA module
  y0[16] = cAMP;
  y0[17] = PKACI;
  y0[18] = PKACII;

  // Target Protein Complexes
  y0[19] = PLBs;
  y0[20] = Inhib1ptot;
  y0[21] = Inhib1p;
  y0[22] = PP1;
  y0[23] = LCCap;
  y0[24] = LCCbp;
  y0[25] = RyRp;
  y0[26] = TnIp;
  y0[27] = Iks;
  y0[28] = Yotiao;
  y0[29] = Iksp;

  // Gates
  y0[30] = m;
  y0[31] = h;
  y0[32] = j;
  y0[33] = d;
  y0[34] = f;
  y0[35] = f2;
  y0[36] = fCa;
  y0[37] = Rq;
  y0[38] = r;
  y0[39] = s;
  y0[40] = xr1;
  y0[41] = xr2;
  y0[42] = xs;

  // Ion concentrations
  y0[43] = CaSR;
  y0[44] = CaSS;
  y0[45] = Na_i;
  y0[46] = K_i;
  y0[47] = Ca_i;
  y0[48] = Vm_old;  // in V


  // Allocate N-vectors.
  w = N_VNew_Serial(neq);

  // if(check_flag((void *)w, "N_VNew_Serial", 0)) exit(1);
  wd = N_VNew_Serial(neq);

  // if(check_flag((void *)wd, "N_VNew_Serial", 0)) exit(1);
  id = N_VNew_Serial(neq);

  // if(check_flag((void *)id, "N_VNew_Serial", 0)) exit(1);
  // constraints = N_VNew_Serial(neq);
  // if(check_flag((void *)constraints, "N_VNew_Serial", 0)) exit(1);
  // beta-AR module

  // fill w with values from y0
  for (int i = 0; i < 49; i++) {
    NV_Ith_S(w, i) = (realtype) y0[i];
  }

  // Initialize vector with constraint definitions
  /*for(int i=0; i<49; i++)
     {NV_Ith_S(constraints,i)=(realtype)1.0;}
     NV_Ith_S(constraints,48)=0.0;*/

  for (int i = 0; i < 49; i++) {
    NV_Ith_S(wd, i) = 0;
  }

  for (int i = 0; i < 49; i++) {
    NV_Ith_S(id, i) = m0[i];
  }

  // Instantiate an IDA solver object
  ida_mem = IDACreate();

  // if(check_flag((void *)ida_mem, "IDACreate", 0)) exit(1);

  // Provide required problem and solution specifications, allocate internal memory, initialize IDA
  flag = IDAInit(ida_mem, fT, t, w, wd);
  flag = IDASStolerances(ida_mem, reltol, abstol);

  // if(check_flag(&flag, "IDAMalloc", 1)) exit(1);

  // Specify algebraic / differential components in the w vector
  flag = IDASetId(ida_mem, id);

  // if(check_flag(&flag, "IDASetId", 1)) exit(1);
  // Specify the user data block and attach it to the main IDA memory block
  IDASetUserData(ida_mem, rdata);

  // Specify maximum number of steps to be taken by the solver in its attempt to reach the next output time
  IDASetMaxNumSteps(ida_mem, mxsteps);

  // Set maximum absolute value of the step size
  IDASetMaxStep(ida_mem, hmax);

  // Define inequality constraints for each component of the solution vector w
  // IDASetConstraints(ida_mem, constraints);

  // Call IDADense and set up the linear solver.
  flag = IDADense(ida_mem, neq);

  // if(check_flag(&flag, "IDADense", 1)) exit(1);

  // Calculate consistent initial conditions (correct initial values of w and wd at time t0)
  // tout1: first value at which a solution will be requested, to determine direction of integration and rough scale in
  // the independent variable t
  flag = IDACalcIC(ida_mem, IDA_YA_YDP_INIT, tout1);

  // if(check_flag(&flag, "IDACalcIC", 1)) exit(1);
} // SaucermanTusscher::alloc_ida

void SaucermanTusscher::Init() {
#if KADEBUG
  cerr << "#initializing Class: SaucermanTusscher ... " << endl;
#endif // if KADEBUG
  abstol = CELLMODEL_PARAMVALUE(VT_abstol);
  reltol = CELLMODEL_PARAMVALUE(VT_reltol);
  hmax = CELLMODEL_PARAMVALUE(VT_hmax);
  mxsteps = CELLMODEL_PARAMVALUE(VT_mxsteps);


  m = CELLMODEL_PARAMVALUE(VT_m_init);
  h = CELLMODEL_PARAMVALUE(VT_h_init);
  j = CELLMODEL_PARAMVALUE(VT_j_init);
  xr1 = CELLMODEL_PARAMVALUE(VT_xr1_init);
  xr2 = CELLMODEL_PARAMVALUE(VT_xr2_init);
  xs = CELLMODEL_PARAMVALUE(VT_xs_init);
  r = CELLMODEL_PARAMVALUE(VT_r_init);
  s = CELLMODEL_PARAMVALUE(VT_s_init);
  d = CELLMODEL_PARAMVALUE(VT_d_init);
  f = CELLMODEL_PARAMVALUE(VT_f_init);
  f2 = CELLMODEL_PARAMVALUE(VT_f2_init); // new
  fCa = CELLMODEL_PARAMVALUE(VT_fCa_init);
  Rq = CELLMODEL_PARAMVALUE(VT_Rq_init); // new
  O = CELLMODEL_PARAMVALUE(VT_O_init); // new
  Ca_i = CELLMODEL_PARAMVALUE(VT_Cai_init);
  CaSR = CELLMODEL_PARAMVALUE(VT_CaSR_init);
  CaSS = CELLMODEL_PARAMVALUE(VT_CaSS_init); // new
  Na_i = CELLMODEL_PARAMVALUE(VT_Nai_init);
  K_i = CELLMODEL_PARAMVALUE(VT_Ki_init);
  L = (CELLMODEL_PARAMVALUE(VT_L_init));
  R = (CELLMODEL_PARAMVALUE(VT_R_init));
  Gs = (CELLMODEL_PARAMVALUE(VT_Gs_init));
  b1ARd = (CELLMODEL_PARAMVALUE(VT_b1ARd_init));
  b1ARtot = (CELLMODEL_PARAMVALUE(VT_b1ARtot_init));
  b1ARp = (CELLMODEL_PARAMVALUE(VT_b1ARp_init));
  Gsagtptot = (CELLMODEL_PARAMVALUE(VT_Gsagtptot_init));
  Gsagdp = (CELLMODEL_PARAMVALUE(VT_Gsagdp_init));
  Gsbg = (CELLMODEL_PARAMVALUE(VT_Gsbg_init));
  Gsa_gtp = (CELLMODEL_PARAMVALUE(VT_Gsa_gtp_init));
  AC = (CELLMODEL_PARAMVALUE(VT_AC_init));
  Fsk = (CELLMODEL_PARAMVALUE(VT_Fsk_init));
  PDE = (CELLMODEL_PARAMVALUE(VT_PDE_init));
  cAMP = (CELLMODEL_PARAMVALUE(VT_cAMP_init));
  IBMX = (CELLMODEL_PARAMVALUE(VT_IBMX_init));
  PKACI = (CELLMODEL_PARAMVALUE(VT_PKACI_init));
  PKACII = (CELLMODEL_PARAMVALUE(VT_PKACII_init));
  cAMPtot = (CELLMODEL_PARAMVALUE(VT_cAMPtot_init));
  PLBs = (CELLMODEL_PARAMVALUE(VT_PLBs_init));
  PP1 = (CELLMODEL_PARAMVALUE(VT_PP1_init));
  Inhib1ptot = (CELLMODEL_PARAMVALUE(VT_Inhib1ptot_init));
  Inhib1p = (CELLMODEL_PARAMVALUE(VT_Inhib1p_init));
  LCCap = (CELLMODEL_PARAMVALUE(VT_LCCap_init));
  LCCbp = (CELLMODEL_PARAMVALUE(VT_LCCbp_init));
  RyRp = (CELLMODEL_PARAMVALUE(VT_RyRp_init));
  TnIp = (CELLMODEL_PARAMVALUE(VT_TnIp_init));
  Iks = (CELLMODEL_PARAMVALUE(VT_Iks_init));
  Yotiao = (CELLMODEL_PARAMVALUE(VT_Yotiao_init));
  Iksp = (CELLMODEL_PARAMVALUE(VT_Iksp_init));
  trel = (CELLMODEL_PARAMVALUE(VT_trel_init));
  t = (CELLMODEL_PARAMVALUE(VT_t_init));
  xs05 = (CELLMODEL_PARAMVALUE(VT_xs05_init));
  Vm_old = (CELLMODEL_PARAMVALUE(VT_V_init)) * 1000;

  // Vm_old=(CELLMODEL_PARAMVALUE(VT_V_init));

  InitTableDone = false;

  // Initialize constant parameters as user data
  stdata.p[0] = 50.5;                  // S2_time
  stdata.p[1] = (CELLMODEL_PARAMVALUE(VT_Ltotmax));       // Ltotmax   [uM] ** apply agonist concentration here **
  stdata.p[2] = 0.028;                 // sumb1AR   [uM]
  stdata.p[3] = (CELLMODEL_PARAMVALUE(VT_Gstot));         // Gstot     [uM]
  stdata.p[4] = (CELLMODEL_PARAMVALUE(VT_Kl));            // Kl        [uM]
  stdata.p[5] = (CELLMODEL_PARAMVALUE(VT_Kr));            // Kr        [uM]
  stdata.p[6] = (CELLMODEL_PARAMVALUE(VT_Kc));            // Kc        [uM]
  stdata.p[7] = (CELLMODEL_PARAMVALUE(VT_k_barkp));       // k_barkp   [1/sec]
  stdata.p[8] = (CELLMODEL_PARAMVALUE(VT_k_barkm));       // k_barkm   [1/sec]
  stdata.p[9] = (CELLMODEL_PARAMVALUE(VT_k_pkap));        // k_pkap    [1/sec/uM]
  stdata.p[10] = (CELLMODEL_PARAMVALUE(VT_k_pkam));  // k_pkam    [1/sec]
  stdata.p[11] = (CELLMODEL_PARAMVALUE(VT_k_gact));        // k_gact    [1/sec]
  stdata.p[12] = (CELLMODEL_PARAMVALUE(VT_k_hyd));         // k_hyd     [1/sec]
  stdata.p[13] = (CELLMODEL_PARAMVALUE(VT_k_reassoc));  // k_reassoc [1/sec/uM]
  // cAMP module
  stdata.p[14] = (CELLMODEL_PARAMVALUE(VT_AC_tot));        // AC_tot    [uM]
  stdata.p[15] = (CELLMODEL_PARAMVALUE(VT_ATP));           // ATP       [uM]
  stdata.p[16] = (CELLMODEL_PARAMVALUE(VT_PDE3tot));       // PDE3tot   [uM]
  stdata.p[17] = (CELLMODEL_PARAMVALUE(VT_PDE4tot));       // PDE4tot   [uM]
  stdata.p[18] = (CELLMODEL_PARAMVALUE(VT_IBMXtot));       // IBMXtot   [uM]
  // stdata.p[19] = (CELLMODEL_PARAMVALUE(VT_Fsk));    // Fsktot    [uM] [10 uM when used]
  stdata.p[19] = 0.0;                   // Fsktot    [uM] [10 uM when used]
  stdata.p[20] = (CELLMODEL_PARAMVALUE(VT_k_ac_basal));    // k_ac_basal[1/sec]
  stdata.p[21] = (CELLMODEL_PARAMVALUE(VT_k_ac_gsa));      // k_ac_gsa  [1/sec]
  stdata.p[22] = (CELLMODEL_PARAMVALUE(VT_k_ac_fsk));      // k_ac_fsk  [1/sec]
  stdata.p[23] = (CELLMODEL_PARAMVALUE(VT_Km_basal));  // Km_basal  [uM]
  stdata.p[24] = (CELLMODEL_PARAMVALUE(VT_Km_gsa));        // Km_gsa    [uM]
  stdata.p[25] = (CELLMODEL_PARAMVALUE(VT_Km_fsk));        // Km_fsk    [uM]
  stdata.p[26] = (CELLMODEL_PARAMVALUE(VT_Kgsa));          // Kgsa      [uM]
  stdata.p[27] = (CELLMODEL_PARAMVALUE(VT_Kfsk));          // Kfsk      [uM]
  stdata.p[28] = (CELLMODEL_PARAMVALUE(VT_k_pde3));        // k_pde3    [1/sec]
  stdata.p[29] = (CELLMODEL_PARAMVALUE(VT_Km_pde3));       // Km_pde3   [uM]
  stdata.p[30] = (CELLMODEL_PARAMVALUE(VT_k_pde4));        // k_pde4    [1/sec]
  stdata.p[31] = (CELLMODEL_PARAMVALUE(VT_Km_pde4));       // Km_pde4   [uM]
  stdata.p[32] = (CELLMODEL_PARAMVALUE(VT_Ki_ibmx));       // Ki_ibmx   [uM]
  // PKA module
  stdata.p[33] = (CELLMODEL_PARAMVALUE(VT_PKAItot));       // PKAItot   [uM]
  stdata.p[34] = (CELLMODEL_PARAMVALUE(VT_PKAIItot));      // PKAIItot  [uM]
  stdata.p[35] = (CELLMODEL_PARAMVALUE(VT_PKItot));        // PKItot    [uM]
  stdata.p[36] = (CELLMODEL_PARAMVALUE(VT_Ka));            // Ka        [uM]
  stdata.p[37] = (CELLMODEL_PARAMVALUE(VT_Kb));            // Kb        [uM]
  stdata.p[38] = (CELLMODEL_PARAMVALUE(VT_Kd));            // Kd        [uM]
  stdata.p[39] = (CELLMODEL_PARAMVALUE(VT_Ki_pki));  // Ki_pki    [uM]
  // PLB module
  stdata.p[40] = (CELLMODEL_PARAMVALUE(VT_epsilon));       // epsilon   [none]
  stdata.p[41] = (CELLMODEL_PARAMVALUE(VT_PLBtot));        // PLBtot    [uM]
  stdata.p[42] = (CELLMODEL_PARAMVALUE(VT_PP1tot));        // PP1tot    [uM]
  stdata.p[43] = (CELLMODEL_PARAMVALUE(VT_Inhib1tot));     // Inhib1tot [uM]
  stdata.p[44] = (CELLMODEL_PARAMVALUE(VT_k_pka_plb));     // k_pka_plb     [1/sec]
  stdata.p[45] = (CELLMODEL_PARAMVALUE(VT_Km_pka_plb));    // Km_pka_plb    [uM]
  stdata.p[46] = (CELLMODEL_PARAMVALUE(VT_k_pp1_plb));     // k_pp1_plb     [1/sec]
  stdata.p[47] = (CELLMODEL_PARAMVALUE(VT_Km_pp1_plb));    // Km_pp1_plb    [uM]
  stdata.p[48] = (CELLMODEL_PARAMVALUE(VT_k_pka_i1));      // k_pka_i1      [1/sec]
  stdata.p[49] = (CELLMODEL_PARAMVALUE(VT_Km_pka_i1));     // Km_pka_i1     [uM]
  stdata.p[50] = (CELLMODEL_PARAMVALUE(VT_Vmax_pp2a_i1));  // Vmax_pp2a_i1  [uM/sec]
  stdata.p[51] = (CELLMODEL_PARAMVALUE(VT_Km_pp2a_i1));    // Km_pp2a_i1    [uM]
  stdata.p[52] = (CELLMODEL_PARAMVALUE(VT_Ki_inhib1));     // Ki_inhib1     [uM]
  // LCC module
  stdata.p[53] = (CELLMODEL_PARAMVALUE(VT_LCCtot));        // LCCtot        [uM]
  stdata.p[54] = (CELLMODEL_PARAMVALUE(VT_PKAIIlcctot));   // PKAIIlcctot   [uM]
  stdata.p[55] = (CELLMODEL_PARAMVALUE(VT_PP1lcctot));     // PP1lcctot     [uM]
  stdata.p[56] = (CELLMODEL_PARAMVALUE(VT_PP2Alcctot));    // PP2Alcctot    [uM]
  stdata.p[57] = (CELLMODEL_PARAMVALUE(VT_k_pka_lcc));     // k_pka_lcc     [1/sec]
  stdata.p[58] = (CELLMODEL_PARAMVALUE(VT_Km_pka_lcc));    // Km_pka_lcc    [uM]
  stdata.p[59] = (CELLMODEL_PARAMVALUE(VT_k_pp1_lcc));     // k_pp1_lcc     [1/sec]
  stdata.p[60] = (CELLMODEL_PARAMVALUE(VT_Km_pp1_lcc));    // Km_pp1_lcc    [uM]
  stdata.p[61] = (CELLMODEL_PARAMVALUE(VT_k_pp2a_lcc));    // k_pp2a_lcc    [1/sec]
  stdata.p[62] = (CELLMODEL_PARAMVALUE(VT_Km_pp2a_lcc));   // Km_pp2a_lcc   [uM]
  // RyR module
  stdata.p[63] = (CELLMODEL_PARAMVALUE(VT_RyRtot));        // RyRtot        [uM]
  stdata.p[64] = (CELLMODEL_PARAMVALUE(VT_PKAIIryrtot));   // PKAIIryrtot   [uM]
  stdata.p[65] = (CELLMODEL_PARAMVALUE(VT_PP1ryr));        // PP1ryr        [uM]
  stdata.p[66] = (CELLMODEL_PARAMVALUE(VT_PP2Aryr));       // PP2Aryr       [uM]
  stdata.p[67] = (CELLMODEL_PARAMVALUE(VT_kcat_pka_ryr));  // kcat_pka_ryr  [1/sec]
  stdata.p[68] = (CELLMODEL_PARAMVALUE(VT_Km_pka_ryr));    // Km_pka_ryr    [uM]
  stdata.p[69] = (CELLMODEL_PARAMVALUE(VT_kcat_pp1_ryr));  // kcat_pp1_ryr  [1/sec]
  stdata.p[70] = (CELLMODEL_PARAMVALUE(VT_Km_pp1_ryr));    // Km_pp1_ryr    [uM]
  stdata.p[71] = (CELLMODEL_PARAMVALUE(VT_kcat_pp2a_ryr)); // kcat_pp2a_ryr [1/sec]
  stdata.p[72] = (CELLMODEL_PARAMVALUE(VT_Km_pp2a_ryr));   // Km_pp2a_ryr   [uM]
  // TnI module
  stdata.p[73] = (CELLMODEL_PARAMVALUE(VT_TnItot));        // TnItot        [uM]
  stdata.p[74] = (CELLMODEL_PARAMVALUE(VT_PP2Atni));       // PP2Atni       [uM]
  stdata.p[75] = (CELLMODEL_PARAMVALUE(VT_kcat_pka_tni));  // kcat_pka_tni  [1/sec]
  stdata.p[76] = (CELLMODEL_PARAMVALUE(VT_Km_pka_tni));    // Km_pka_tni    [uM]
  stdata.p[77] = (CELLMODEL_PARAMVALUE(VT_kcat_pp2a_tni)); // kcat_pp2a_tni [1/sec]
  stdata.p[78] = (CELLMODEL_PARAMVALUE(VT_Km_pp2a_tni));   // Km_pp2a_tni   [uM]
  // Iks module
  stdata.p[79] = (CELLMODEL_PARAMVALUE(VT_Iks_tot));       // Iks_tot       [uM]
  stdata.p[80] = (CELLMODEL_PARAMVALUE(VT_Yotiao_tot));    // Yotiao_tot    [uM]
  stdata.p[81] = (CELLMODEL_PARAMVALUE(VT_K_yotiao));  // K_yotiao      [uM] ** apply G589D mutation here **
  stdata.p[82] = (CELLMODEL_PARAMVALUE(VT_PKAII_ikstot));  // PKAII_ikstot  [uM]
  stdata.p[83] = (CELLMODEL_PARAMVALUE(VT_PP1_ikstot));    // PP1_ikstot    [uM]
  stdata.p[84] = (CELLMODEL_PARAMVALUE(VT_k_pka_iks));     // k_pka_iks     [1/sec]
  stdata.p[85] = (CELLMODEL_PARAMVALUE(VT_Km_pka_iks));    // Km_pka_iks    [uM]
  stdata.p[86] = (CELLMODEL_PARAMVALUE(VT_k_pp1_iks));     // k_pp1_iks     [1/sec]
  stdata.p[87] = (CELLMODEL_PARAMVALUE(VT_Km_pp1_iks));    // Km_pp1_iks    [uM]
  //// ---- EC Coupling model parameters ------
  // universal parameters
  stdata.p[88] = (CELLMODEL_PARAMVALUE(VT_inverseRTONF));  // Vmyo  [uL]
  stdata.p[89] = (CELLMODEL_PARAMVALUE(VT_Vc));            // Vc  [uL]
  stdata.p[90] = (CELLMODEL_PARAMVALUE(VT_Vsr));           // Vsr  [uL]
  stdata.p[91] = (CELLMODEL_PARAMVALUE(VT_Vss));           // Vsr  [uL]
  stdata.p[92] = (CELLMODEL_PARAMVALUE(VT_Vrel));          // Vrel  [uL]
  stdata.p[93] = (CELLMODEL_PARAMVALUE(VT_Vxfer));
  stdata.p[94] = (CELLMODEL_PARAMVALUE(VT_Vmaxup));
  stdata.p[95] = (CELLMODEL_PARAMVALUE(VT_VcdVsr));
  stdata.p[96] = (CELLMODEL_PARAMVALUE(VT_Tx)); // Temp  [K]
  stdata.p[97] = (CELLMODEL_PARAMVALUE(VT_F));
  stdata.p[98] = (CELLMODEL_PARAMVALUE(VT_RTONF));

  // extracellular concentrations
  stdata.p[99] = (CELLMODEL_PARAMVALUE(VT_Na_o)); // Extracellular Na  [mM]
  stdata.p[100] = (CELLMODEL_PARAMVALUE(VT_K_o));  // Extracellular K   [mM]
  stdata.p[101] = (CELLMODEL_PARAMVALUE(VT_Ca_o)); // Extracellular Ca  [mM]
  stdata.p[102] = (CELLMODEL_PARAMVALUE(VT_n));

  // current conductances
  stdata.p[103] = (CELLMODEL_PARAMVALUE(VT_g_Na));  // G_Na      [mS/uF]
  stdata.p[104] = (CELLMODEL_PARAMVALUE(VT_g_to));  // G_to      [mS/uF]
  stdata.p[105] = (CELLMODEL_PARAMVALUE(VT_g_Kr));  // G_kro     [mS/uF]
  stdata.p[106] = (CELLMODEL_PARAMVALUE(VT_g_K1));  // G_kibar   [mS/uF]
  stdata.p[107] = (CELLMODEL_PARAMVALUE(VT_g_pK));  // G_kp      [mS/uF]
  stdata.p[108] = (CELLMODEL_PARAMVALUE(VT_g_Ks));  // Gkso       [mS/uF]
  stdata.p[109] = (CELLMODEL_PARAMVALUE(VT_g_CaL));  // G_CaL       [mS/uF]
  stdata.p[110] = (CELLMODEL_PARAMVALUE(VT_g_pCa)); // ibarpca   [uA/uF]
  stdata.p[111] = (CELLMODEL_PARAMVALUE(VT_g_bCa)); // G_Cab     [uA/uF]
  stdata.p[112] = (CELLMODEL_PARAMVALUE(VT_g_bNa));  // G_Nab     [uA/uF]
  stdata.p[113] = (CELLMODEL_PARAMVALUE(VT_pKNa));
  stdata.p[114] = (CELLMODEL_PARAMVALUE(VT_KpCa));
  stdata.p[115] = (CELLMODEL_PARAMVALUE(VT_knak)); // ibarnak   [uA/uF]
  stdata.p[116] = (CELLMODEL_PARAMVALUE(VT_KmK));  // Km_Ko     [mM]
  stdata.p[117] = (CELLMODEL_PARAMVALUE(VT_KmNa)); // Km_Na     [mM]
  stdata.p[118] = (CELLMODEL_PARAMVALUE(VT_max_sr));
  stdata.p[119] = (CELLMODEL_PARAMVALUE(VT_min_sr));
  stdata.p[120] = (CELLMODEL_PARAMVALUE(VT_EC));
  stdata.p[121] = (CELLMODEL_PARAMVALUE(VT_ks1));
  stdata.p[122] = (CELLMODEL_PARAMVALUE(VT_ks2));
  stdata.p[123] = (CELLMODEL_PARAMVALUE(VT_k3));
  stdata.p[124] = (CELLMODEL_PARAMVALUE(VT_k4));
  stdata.p[125] = (CELLMODEL_PARAMVALUE(VT_Bufsr));
  stdata.p[126] = (CELLMODEL_PARAMVALUE(VT_Kbufsr));
  stdata.p[127] = (CELLMODEL_PARAMVALUE(VT_Bufss));
  stdata.p[128] = (CELLMODEL_PARAMVALUE(VT_Kbufss));
  stdata.p[129] = (CELLMODEL_PARAMVALUE(VT_inversevssF2));
  stdata.p[130] = (CELLMODEL_PARAMVALUE(VT_C));
  stdata.p[131] = (CELLMODEL_PARAMVALUE(VT_Bufc));
  stdata.p[132] = (CELLMODEL_PARAMVALUE(VT_Kbufc));
  stdata.p[133] = (CELLMODEL_PARAMVALUE(VT_inverseviF2));
  stdata.p[134] = (CELLMODEL_PARAMVALUE(VT_BufcPKbufc));
  stdata.p[135] = (CELLMODEL_PARAMVALUE(VT_inverseviF));
  stdata.p[136] = (CELLMODEL_PARAMVALUE(VT_KopKNaNao));
  stdata.p[137] = (CELLMODEL_PARAMVALUE(VT_Kupsquare));
  stdata.p[138] = (CELLMODEL_PARAMVALUE(VT_s_inf_vHalf));
  stdata.p[139] = (CELLMODEL_PARAMVALUE(VT_tau_s_f1));
  stdata.p[140] = (CELLMODEL_PARAMVALUE(VT_tau_s_vHalf1));
  stdata.p[141] = (CELLMODEL_PARAMVALUE(VT_tau_s_slope1));
  stdata.p[142] = (CELLMODEL_PARAMVALUE(VT_tau_s_enable));
  stdata.p[143] = (CELLMODEL_PARAMVALUE(VT_tau_s_add));
  stdata.p[144] = (CELLMODEL_PARAMVALUE(VT_kNaCa));
  stdata.p[145] = (CELLMODEL_PARAMVALUE(VT_KmNai));
  stdata.p[146] = (CELLMODEL_PARAMVALUE(VT_KmCa));
  stdata.p[147] = (CELLMODEL_PARAMVALUE(VT_ksat));
  stdata.p[148] = 0;  // later I_app
  stdata.p[149] = (CELLMODEL_PARAMVALUE(VT_p_PLB));
  stdata.p[150] = 0;  // clamp voltage
  stdata.p[151] = 0; // clamp voltage set?
  stdata.p[152] = 1;  // first calc loop?
  // INaK module
  stdata.p[153] = (CELLMODEL_PARAMVALUE(VT_fac_KmNa));
  stdata.p[154] = (CELLMODEL_PARAMVALUE(VT_fac_knak));
  stdata.p[155] = (CELLMODEL_PARAMVALUE(VT_PKACI_init));

  // Give solver access to user data/ parameters via rdata
  rdata = &stdata;

  // Initialize mass matrix (1=ODE, 0=DAE)
  for (int i = 0; i < 49; i++) {
    m0[i] = 1.0;
  }
  m0[1] = 0;
  m0[2] = 0;
  m0[3] = 0;
  m0[10] = 0;
  m0[11] = 0;
  m0[12] = 0;
  m0[13] = 0;
  m0[14] = 0;
  m0[16] = 0;
  m0[17] = 0;
  m0[18] = 0;
  m0[21] = 0;
  m0[22] = 0;
  m0[27] = 0;
  m0[28] = 0;
} // SaucermanTusscher::Init

ML_CalcType SaucermanTusscher::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                                    ML_CalcType stretch = 1., int euler = 2) {
  if (!InitTableDone) {
    if (CELLMODEL_PARAMVALUE(VT_InitTableDone) == 0) {
      ptTeaP->InitTableWithtinc(tinc);
    }
    InitTableDone = true;
  }

  ML_CalcType svolt = V * 1000;  // membrane voltage in mV
  // const int Vi=(int)(DivisionTab*(RangeTabhalf+svolt)+.5); //array position
  // ECA Tabellisierung

  stdata.p[148] = i_external;  // later I_app
  y0[0] = trel;

  // beta-AR and Gs_alpha module
  y0[1] = L;
  y0[2] = R;
  y0[3] = Gs;
  y0[4] = b1ARtot;
  y0[5] = b1ARd;
  y0[6] = b1ARp;
  y0[7] = Gsagtptot;
  y0[8] = Gsagdp;
  y0[9] = Gsbg;

  // cAMP module
  y0[10] = Gsa_gtp;
  y0[11] = Fsk;
  y0[12] = AC;
  y0[13] = PDE;
  y0[14] = IBMX;
  y0[15] = cAMPtot;

  // PKA module
  y0[16] = cAMP;
  y0[17] = PKACI;
  y0[18] = PKACII;

  // Target protein modules (PLB and LCC, Ryanodine receptor, Troponin I, I_Ks modules)
  y0[19] = PLBs;
  y0[20] = Inhib1ptot;
  y0[21] = Inhib1p;
  y0[22] = PP1;
  y0[23] = LCCap;
  y0[24] = LCCbp;
  y0[25] = RyRp;
  y0[26] = TnIp;
  y0[27] = Iks;
  y0[28] = Yotiao;
  y0[29] = Iksp;

  // Gates
  y0[30] = m;
  y0[31] = h;
  y0[32] = j;
  y0[33] = d;
  y0[34] = f;
  y0[35] = f2;
  y0[36] = fCa;
  y0[37] = Rq;
  y0[38] = r;
  y0[39] = s;
  y0[40] = xr1;
  y0[41] = xr2;
  y0[42] = xs;

  // Ion concentrations
  y0[43] = CaSR;
  y0[44] = CaSS;
  y0[45] = Na_i;
  y0[46] = K_i;
  y0[47] = Ca_i;

  ML_CalcType upper_boarder = 0;
  ML_CalcType lower_boarder = 0;

  float tolerance = y0[48] / 10000;
  if (y0[48] > 0) {
    upper_boarder = y0[48] + tolerance;
    lower_boarder = y0[48] - tolerance;
  } else {
    lower_boarder = y0[48] + tolerance;
    upper_boarder = y0[48] - tolerance;
  }

  if ((stdata.p[152] == 1) && ((lower_boarder > svolt) || (svolt > upper_boarder))) {
    stdata.p[151] = 1;  // clamped voltage!
  }
  y0[48] = svolt; // in mV
  stdata.p[152] = 0;  // first loop passed

  /*for(int i=0; i<49; i++)
     {NV_Ith_S(w,i)=y0[i];}*/

  // Define time of next solver output
  tout = t + tinc;

  stdata.p[150] = svolt;

  // Integrate the DAE over an interval in t
  flag = IDASolve(ida_mem, tout, &t, w, wd,
                  IDA_NORMAL);  // IDA_NORMAL: solver may pass the user specified tout time
  // and returns an interpolated value
  // if(check_flag(&flag, "IDASolve", 1)) exit(1);
  // PrintFinalStats(ida_mem);


  ML_CalcType Vm = (ML_CalcType) NV_Ith_S(w, 48);

  if (stdata.p[151] == 1) {
    Vm = svolt;
  }

  // Gates
  m = (ML_CalcType) NV_Ith_S(w, 30);
  h = (ML_CalcType) NV_Ith_S(w, 31);
  j = (ML_CalcType) NV_Ith_S(w, 32);
  d = (ML_CalcType) NV_Ith_S(w, 33);
  f = (ML_CalcType) NV_Ith_S(w, 34);
  f2 = (ML_CalcType) NV_Ith_S(w, 35);
  fCa = (ML_CalcType) NV_Ith_S(w, 36);
  Rq = (ML_CalcType) NV_Ith_S(w, 37);
  r = (ML_CalcType) NV_Ith_S(w, 38);
  s = (ML_CalcType) NV_Ith_S(w, 39);
  xr1 = (ML_CalcType) NV_Ith_S(w, 40);
  xr2 = (ML_CalcType) NV_Ith_S(w, 41);
  xs = (ML_CalcType) NV_Ith_S(w, 42);

  // Ion concentrations
  CaSR = (ML_CalcType) NV_Ith_S(w, 43);
  CaSS = (ML_CalcType) NV_Ith_S(w, 44);
  Na_i = (ML_CalcType) NV_Ith_S(w, 45);
  K_i = (ML_CalcType) NV_Ith_S(w, 46);
  Ca_i = (ML_CalcType) NV_Ith_S(w, 47);
  trel = (ML_CalcType) NV_Ith_S(w, 0);

  // Beta_Adrenergic Signaling
  L = (ML_CalcType) NV_Ith_S(w, 1);
  R = (ML_CalcType) NV_Ith_S(w, 2);
  Gs = (ML_CalcType) NV_Ith_S(w, 3);
  b1ARtot = (ML_CalcType) NV_Ith_S(w, 4);
  b1ARd = (ML_CalcType) NV_Ith_S(w, 5);
  b1ARp = (ML_CalcType) NV_Ith_S(w, 6);
  Gsagtptot = (ML_CalcType) NV_Ith_S(w, 7);
  Gsagdp = (ML_CalcType) NV_Ith_S(w, 8);
  Gsbg = (ML_CalcType) NV_Ith_S(w, 9);
  Gsa_gtp = (ML_CalcType) NV_Ith_S(w, 10);
  Fsk = (ML_CalcType) NV_Ith_S(w, 11);
  AC = (ML_CalcType) NV_Ith_S(w, 12);
  PDE = (ML_CalcType) NV_Ith_S(w, 13);
  IBMX = (ML_CalcType) NV_Ith_S(w, 14);
  cAMPtot = (ML_CalcType) NV_Ith_S(w, 15);
  cAMP = (ML_CalcType) NV_Ith_S(w, 16);
  PKACI = (ML_CalcType) NV_Ith_S(w, 17);
  PKACII = (ML_CalcType) NV_Ith_S(w, 18);
  PLBs = (ML_CalcType) NV_Ith_S(w, 19);
  Inhib1ptot = (ML_CalcType) NV_Ith_S(w, 20);
  Inhib1p = (ML_CalcType) NV_Ith_S(w, 21);
  PP1 = (ML_CalcType) NV_Ith_S(w, 22);
  LCCap = (ML_CalcType) NV_Ith_S(w, 23);
  LCCbp = (ML_CalcType) NV_Ith_S(w, 24);
  RyRp = (ML_CalcType) NV_Ith_S(w, 25);
  TnIp = (ML_CalcType) NV_Ith_S(w, 26);
  Iks = (ML_CalcType) NV_Ith_S(w, 27);
  Yotiao = (ML_CalcType) NV_Ith_S(w, 28);
  Iksp = (ML_CalcType) NV_Ith_S(w, 29);


  ML_CalcType dVm = Vm_old - Vm;
  Vm_old = Vm;

  return -dVm / 1000;
} // SaucermanTusscher::Calc

// Function to be solved by IDA
static int fT(realtype t, N_Vector w, N_Vector wd, N_Vector rr, void *rdata) {
  // Initialize an array with variable values (gates, concentrations, voltage, ..., see top)
  realtype *w_data = NV_DATA_S(w);
  realtype y[49] = {0.0};

  for (int kappa = 0; kappa < 49; kappa++) {
    y[kappa] = w_data[kappa];
  }

  // Give user data (constants) to vector/array p
  STDATA *stdata;
  stdata = static_cast<STDATA *>(rdata);
  realtype *p = &(stdata->p[0]);

  const realtype Vm_non_clamp = y[48];

  if (p[151] == 1) {
    y[48] = p[150];
  }

  // rr / rval is the vector / array that has to be optimized to ZERO
  realtype *rval;
  rval = NV_DATA_S(rr);

  // array for the differential values of w
  realtype *ypval;
  ypval = NV_DATA_S(wd);

  // Introduce array for equation value allocation
  realtype ydot[49] = {0.0};


  //// b-AR module
  realtype LR = y[1] * y[2] / p[4];
  realtype LRG = LR * y[3] / p[5];
  realtype RG = y[2] * y[3] / p[6];
  realtype BARKDESENS = p[7] * (LR + LRG);
  realtype BARKRESENS = p[8] * y[5];
  realtype PKADESENS = p[9] * y[17] * y[4];
  realtype PKARESENS = p[10] * y[6];
  realtype GACT = p[11] * (RG + LRG);
  realtype HYD = p[12] * y[7];
  realtype REASSOC = p[13] * y[8] * y[9];
  ydot[1] = p[1] - LR - LRG - y[1];
  ydot[2] = y[4] - LR - LRG - RG - y[2];
  ydot[3] = p[3] - LRG - RG - y[3];
  ydot[4] = (BARKRESENS - BARKDESENS) + (PKARESENS - PKADESENS);
  ydot[5] = BARKDESENS - BARKRESENS;
  ydot[6] = PKADESENS - PKARESENS;
  ydot[7] = GACT - HYD;
  ydot[8] = HYD - REASSOC;
  ydot[9] = GACT - REASSOC;

  // end b-AR module

  //// cAMP module
  realtype Gsa_gtp_AC = y[10] * y[12] / p[26];
  realtype Fsk_AC = y[11] * y[12] / p[27];
  realtype AC_ACT_BASAL = p[20] * y[12] * p[15] / (p[23] + p[15]);
  realtype AC_ACT_GSA = p[21] * Gsa_gtp_AC * p[15] / (p[24] + p[15]);
  realtype AC_ACT_FSK = p[22] * Fsk_AC * p[15] / (p[25] + p[15]);
  realtype PDE3_ACT = p[28] * y[13] * y[16] / (p[29] + y[16]);
  realtype PDE4_ACT = p[30] * y[13] * y[16] / (p[31] + y[16]);
  realtype PDE_IBMX = y[13] * y[14] / p[32];
  ydot[10] = y[7] - Gsa_gtp_AC - y[10];
  ydot[11] = p[19] - Fsk_AC - y[11];
  ydot[12] = p[14] - Gsa_gtp_AC -
             y[12]; // note: assumes Fsk = 0.  Change Gsa_gtp_AC to Fsk_AC for Forskolin.
  ydot[13] = p[17] - PDE_IBMX - y[13];
  ydot[14] = p[18] - PDE_IBMX - y[14];
  ydot[15] = AC_ACT_BASAL + AC_ACT_GSA + AC_ACT_FSK - PDE3_ACT - PDE4_ACT;

  // end cAMP module

  //// PKA module
  realtype PKI = p[35] * p[39] / (p[39] + y[17] + y[18]);
  realtype A2RC_I = (y[17] / p[38]) * y[17] * (1 + PKI / p[39]);
  realtype A2R_I = y[17] * (1 + PKI / p[39]);
  realtype A2RC_II = (y[18] / p[38]) * y[18] * (1 + PKI / p[39]);
  realtype A2R_II = y[18] * (1 + PKI / p[39]);
  realtype ARC_I = (p[36] / y[16]) * A2RC_I;
  realtype ARC_II = (p[36] / y[16]) * A2RC_II;
  ydot[16] = y[15] - (ARC_I + 2 * A2RC_I + 2 * A2R_I) - (ARC_II + 2 * A2RC_II + 2 * A2R_II) - y[16];
  realtype PKAtemp = p[36] * p[37] / p[38] + p[36] * y[16] / p[38] + y[16] * y[16] / p[38];
  ydot[17] =
      2 * p[33] * y[16] * y[16] - y[17] * (1 + PKI / p[39]) * (PKAtemp * y[17] + y[16] * y[16]);
  ydot[18] =
      2 * p[34] * y[16] * y[16] - y[18] * (1 + PKI / p[39]) * (PKAtemp * y[18] + y[16] * y[16]);

  // end PKA module

  //// PLB module
  realtype PLB = p[41] - y[19];
  realtype PLB_PHOSPH = p[44] * y[17] * PLB / (p[45] + PLB);
  realtype PLB_DEPHOSPH = p[46] * y[22] * y[19] / (p[47] + y[19]);
  ydot[19] = PLB_PHOSPH - PLB_DEPHOSPH;

  realtype Inhib1 = p[43] - y[20];
  realtype Inhib1p_PP1 = y[21] * y[22] / p[52];
  realtype Inhib1_PHOSPH = p[48] * y[17] * Inhib1 / (p[49] + Inhib1);
  realtype Inhib1_DEPHOSPH = p[50] * y[20] / (p[51] + y[20]);
  ydot[20] = Inhib1_PHOSPH - Inhib1_DEPHOSPH;
  ydot[21] = y[20] - Inhib1p_PP1 - y[21];
  ydot[22] = p[42] - Inhib1p_PP1 - y[22];

  // realtype fracPLBp = y[19]/p[41];
  realtype fracPLB = PLB / p[41];
  realtype fracPLBo = 0.9926;

  // end PLB module

  //// LCC module
  realtype PKAClcc = (p[54] / p[34]) * y[18];
  realtype LCCa = p[53] - y[23];
  realtype LCCa_PHOSPH = p[40] * p[57] * PKAClcc * LCCa / (p[58] + p[40] * LCCa);
  realtype LCCa_DEPHOSPH = p[40] * p[61] * p[56] * y[23] / (p[62] + p[40] * y[23]);
  ydot[23] = LCCa_PHOSPH - LCCa_DEPHOSPH;
  realtype fracLCCap = y[23] / p[53];
  realtype fracLCCapo = 0.028;

  realtype LCCb = p[53] - y[24];
  realtype LCCb_PHOSPH = p[40] * p[57] * PKAClcc * LCCb / (p[58] + p[40] * LCCb);
  realtype LCCb_DEPHOSPH = p[40] * p[59] * p[55] * y[24] / (p[60] + p[40] * y[24]);
  ydot[24] = LCCb_PHOSPH - LCCb_DEPHOSPH;
  realtype fracLCCbp = y[24] / p[53];
  realtype fracLCCbpo = 0.0328;

  // end LCC module

  //// RyR module
  realtype PKACryr = (p[64] / p[34]) * y[18];
  realtype RyR = p[63] - y[25];
  realtype RyRPHOSPH = p[40] * p[67] * PKACryr * RyR / (p[68] + p[40] * RyR);
  realtype RyRDEPHOSPH1 = p[40] * p[69] * p[65] * y[25] / (p[70] + p[40] * y[25]);
  realtype RyRDEPHOSPH2A = p[40] * p[71] * p[66] * y[25] / (p[72] + p[40] * y[25]);
  ydot[25] = RyRPHOSPH - RyRDEPHOSPH1 - RyRDEPHOSPH2A;

  // realtype fracRyRp = y[25]/p[63];
  // realtype fracRyRpo = 0.0244;
  // end RyR module

  //// TnI module
  realtype TnI = p[73] - y[26];
  realtype TnIPHOSPH = p[75] * y[17] * TnI / (p[76] + TnI);
  realtype TnIDEPHOSPH = p[77] * p[74] * y[26] / (p[78] + y[26]);
  ydot[26] = TnIPHOSPH - TnIDEPHOSPH;

  // realtype fracTnIp = y[26]/p[73];
  // realtype fracTnIpo = 0.0031;
  // end TnI module

  //// Iks module
  realtype IksYot = y[27] * y[28] / p[81]; // [uM]
  ydot[27] = p[79] - IksYot - y[27];   // [uM]
  ydot[28] = p[80] - IksYot - y[28];   // [uM]
  realtype PKACiks = (IksYot / p[79]) * (p[82] / p[34]) * y[18];
  realtype PP1iks = (IksYot / p[79]) * p[83];
  realtype Iks = p[79] - y[29];
  realtype IKS_PHOSPH = p[40] * p[84] * PKACiks * Iks / (p[85] + p[40] * Iks);
  realtype IKS_DEPHOSPH = p[40] * p[86] * PP1iks * y[29] / (p[87] + p[40] * y[29]);
  ydot[29] = IKS_PHOSPH - IKS_DEPHOSPH;
  realtype fracIksp = y[29] / p[79];
  realtype fracIkspo = 1.8e-3 / p[79];

  // end Iks module
  // -------- END SIGNALING MODEL ---------

  // Give array content back its names
  // idee : #define VT_inverseRTONF p[88]
  realtype VT_inverseRTONF = p[88];
  realtype VT_Vc = p[89];
  realtype VT_Vsr = p[90];
  realtype VT_Vss = p[91];
  realtype VT_Vrel = p[92];
  realtype VT_Vxfer = p[93];
  realtype VT_Vmaxup = p[94];
  realtype VT_VcdVsr = p[95];

  // realtype VT_Tx=p[96];
  realtype VT_F = p[97];
  realtype VT_RTONF = p[98];
  realtype VT_Na_o = p[99];
  realtype VT_K_o = p[100];
  realtype VT_Ca_o = p[101];
  realtype VT_n = p[102];
  realtype VT_g_Na = p[103];
  realtype VT_g_to = p[104];
  realtype VT_g_Kr = p[105];
  realtype VT_g_K1 = p[106];
  realtype VT_g_pK = p[107];
  realtype VT_g_Ks = p[108];
  realtype VT_g_CaL = p[109];
  realtype VT_g_pCa = p[110];
  realtype VT_g_bCa = p[111];
  realtype VT_g_bNa = p[112];
  realtype VT_pKNa = p[113];
  realtype VT_KpCa = p[114];
  realtype VT_knak = p[115];
  realtype VT_KmK = p[116];
  realtype VT_KmNa = p[117];
  realtype VT_max_sr = p[118];
  realtype VT_min_sr = p[119];
  realtype VT_EC = p[120];
  realtype VT_ks1 = p[121];
  realtype VT_ks2 = p[122];
  realtype VT_k3 = p[123];
  realtype VT_k4 = p[124];
  realtype VT_Bufsr = p[125];
  realtype VT_Kbufsr = p[126];
  realtype VT_Bufss = p[127];
  realtype VT_Kbufss = p[128];
  realtype VT_inversevssF2 = p[129];
  realtype VT_C = p[130];
  realtype VT_Bufc = p[131];
  realtype VT_Kbufc = p[132];
  realtype VT_inverseviF2 = p[133];

  // realtype VT_BufcPKbufc=p[134];
  realtype VT_inverseviF = p[135];
  realtype VT_KopKNaNao = p[136];
  realtype VT_Kupsquare = p[137];
  realtype VT_s_inf_vHalf = p[138];
  realtype VT_tau_s_f1 = p[139];
  realtype VT_tau_s_vHalf1 = p[140];
  realtype VT_tau_s_slope1 = p[141];
  realtype VT_tau_s_enable = p[142];
  realtype VT_tau_s_add = p[143];
  realtype VT_kNaCa = p[144];
  realtype VT_KmNai = p[145];
  realtype VT_KmCa = p[146];
  realtype VT_ksat = p[147];
  realtype I_app = p[148];
  realtype VT_p_PLB = p[149];
  realtype VT_fac_KmNa = p[153];
  realtype VT_fac_knak = p[154];
  realtype VT_PKA0 = p[155];

  realtype svolt = y[48];
  realtype m = y[30];
  realtype h = y[31];
  realtype j = y[32];
  realtype d = y[33];
  realtype f = y[34];
  realtype f2 = y[35];
  realtype fCa = y[36];
  realtype Rq = y[37];
  realtype r = y[38];
  realtype s = y[39];
  realtype xr1 = y[40];
  realtype xr2 = y[41];
  realtype xs = y[42];
  realtype CaSR = y[43];
  realtype CaSS = y[44];
  realtype Na_i = y[45];
  realtype K_i = y[46];
  realtype Ca_i = y[47];

  // Calculate Currents


  /*#ifdef useCaTab
     static double CaiTabConst1=-CELLMODEL_PARAMVALUE(VT_CaiMin)/CELLMODEL_PARAMVALUE(VT_StepCai)+0.5;
     static double CaiTabConst2=1./CELLMODEL_PARAMVALUE(VT_StepCai);
     int Caitab = (int)(CaiTabConst2*Ca_i+CaiTabConst1);
   #endif*/

  // Needed to compute currents
  // Nernst Potentials
  const realtype EK = VT_RTONF * (log((VT_K_o / K_i)));                    // 25
  const realtype EKs = VT_RTONF * (log(VT_KopKNaNao / (K_i + VT_pKNa * Na_i))); // 26
  const realtype ECa = 0.5 * VT_RTONF * (log((VT_Ca_o / Ca_i)));              // 25
  const realtype ENa = VT_RTONF * (log(VT_Na_o / Na_i));                    // 25

  // Potassium currents
  const realtype VminEK = svolt - EK;
  const realtype rec_ipK = VT_g_pK / (1. + exp((25. - svolt) / 5.98));  // new_g
  const realtype I_pK = rec_ipK * VminEK;                       // 83

  const realtype I_to =
      VT_g_to * r * s * VminEK;                      // 55 Transient Outward Current

  const realtype I_Kr =
      VT_g_Kr * sqrt(VT_K_o / 5.4) * xr1 * xr2 * VminEK; // 67 Rapid Delayed Rectifier Current

  const realtype p_Iks = 0.2 * fracIksp / fracIkspo + 0.8;  // PHOSPHOREGULATION
  const realtype I_Ks =
      VT_g_Ks * p_Iks * xs * xs * (svolt - EKs);        // 62 Slow Delayed Rectifier Current


  const realtype I_K = I_Kr + I_Ks;

  const realtype AK1 = 0.1 / (1. + exp(0.06 * (VminEK -
                                               200)));                                                  // 77
  const realtype BK1 = (3. * exp(0.0002 * (svolt - EK + 100)) + exp(0.1 * (svolt - EK - 10))) /
                       (1. + exp(-0.5 * (svolt - EK))); // 78
  const realtype rec_iK1 = AK1 / (AK1 + BK1);
  const realtype I_K1 =
      VT_g_K1 * rec_iK1 * VminEK;                                                           // 76
  // Inward
  // Rectifier
  // K+
  // Current

  // Backgraound & pump currents
  const realtype VminENa = svolt - ENa;
  const realtype I_bNa = VT_g_bNa * VminENa; // 84 Background Currents
  // #ifdef useCaTab
  // const  realtype I_bCa=VT_g_bCa*(svolt-(ptTeaP->ECA[Caitab]));
  // #else
  const realtype I_bCa = VT_g_bCa * (svolt - ECa);         // 85 Background Currents
  // #endif
  const realtype I_pCa = VT_g_pCa * Ca_i / (VT_KpCa + Ca_i); // 82

  // Compute scalar currents
  const realtype I_Na = VT_g_Na * m * m * m * h * j * VminENa;     // 27 Fast Na+ Current

  const realtype CaL_P1 =
      4 * (svolt - 15.) * VT_F / VT_RTONF * (0.25 * exp(2 * (svolt - 15.) / VT_RTONF)) /
      (exp(2 * (svolt - 15.) / VT_RTONF) - 1.);
  const realtype CaL_P2 = 4 * (svolt - 15.) * (VT_F / VT_RTONF) * (1 * VT_Ca_o) /
                          (exp(2 * (svolt - 15.) / VT_RTONF) - 1.);

  const realtype favail =
      1 * (0.05 * fracLCCbp / fracLCCbpo + 0.95); // PHOSPHOREGULATION //compare weight of Saucerman
  const realtype fpo = 1 * (0.03 * fracLCCap / fracLCCapo + 0.97); // PHOSPHOREGULATION

  // const  realtype favail = 1*(0.04*fracLCCbp/fracLCCbpo+0.96);    // PHOSPHOREGULATION //reduced weights to avoid to
  // high I_CaL
  // const  realtype fpo = 1*(0.02*fracLCCap/fracLCCapo+0.98);       // PHOSPHOREGULATION


  const realtype I_CaL = VT_g_CaL * d * f * f2 * fCa * favail * fpo * (CaL_P1 * CaSS - CaL_P2);

  const realtype VT_KmNai3 = VT_KmNai * VT_KmNai * VT_KmNai;
  const realtype VT_Nao3 = VT_Na_o * VT_Na_o * VT_Na_o;
  const realtype NaCa_P1 =
      (VT_kNaCa * (1. / (VT_KmNai3 + VT_Nao3)) * (1. / (VT_KmCa + VT_Ca_o)) *
       (1. / (1. + VT_ksat * exp((VT_n - 1.) * svolt * VT_inverseRTONF)))) *
      (exp(VT_n * svolt * VT_inverseRTONF) *
       VT_Ca_o); // (exp(n*vv/RTONF)*Nai*Nai*NaiP[VT_C].valueao);
  const realtype NaCa_P2 =
      (VT_kNaCa * (1. / (VT_KmNai3 + VT_Nao3)) * (1. / (VT_KmCa + VT_Ca_o)) *
       (1. / (1. + VT_ksat * exp((VT_n - 1.) * svolt * VT_inverseRTONF)))) *
      (exp((VT_n - 1) * svolt / VT_RTONF) * VT_Na_o * VT_Na_o * VT_Na_o * 2.5); // *7.5);//*2.5);
  const realtype I_NaCa = NaCa_P1 * Na_i * Na_i * Na_i - NaCa_P2 * Ca_i;


  // INaK
  const realtype KmNaIso = VT_KmNa * (VT_fac_KmNa * (VT_PKA0 / y[17]) +
                                      (1 - VT_fac_KmNa)); // new weights for NaK pump current
  // according to Despa et al.
  // Circulation 2008;117:1849-1855
  const realtype knakIso = VT_knak * (VT_fac_knak * (y[17] / VT_PKA0) +
                                      (1 - VT_fac_knak)); // new weights for NaK pump current
  // according to Despa et al.
  // Circulation 2008;117:1849-1855
  // const realtype KmNaIso = VT_KmNa * (0.34 * (0.0083/y[17]) + 0.66);
  // const realtype knakIso = VT_knak * (0.011 * (y[17]/0.0083) + 0.989);

  const realtype rec_iNaK = (1. / (1. + 0.1245 * exp(-0.1 * svolt * VT_inverseRTONF) +
                                   0.0353 * exp(-svolt * VT_inverseRTONF)));
  const realtype I_NaK =
      knakIso * (VT_K_o / (VT_K_o + VT_KmK)) * (Na_i / (Na_i + KmNaIso)) * rec_iNaK;


  const realtype I_tot =
      I_K + I_K1 + I_to + I_Na + I_bNa + I_CaL + I_bCa + I_NaK + I_NaCa + I_pCa + I_pK

      // -i_external;
      - I_app;

  ydot[48] = -I_tot * 1000;

  // update concentrations
  const realtype Caisquare = Ca_i * Ca_i;

  // const  realtype CaSRsquare=CaSR*CaSR;
  const realtype CaSSsquare = CaSS * CaSS;    // new
  const realtype kCaSR =
      VT_max_sr - ((VT_max_sr - VT_min_sr) / (1 + pow((VT_EC / CaSR), 2))); // new n37
  const realtype k2 = VT_ks2 * kCaSR; // new n36
  const realtype k1 = VT_ks1 / kCaSR; // new n35
  // const  realtype A=0.016464*CaSRsquare/(0.0625+CaSRsquare)+0.008232;
  // I_rel=A*d*g;                                                               //88
  // update Rq und O

  // const  realtype fracRyRp = y[25]/p[63];
  // const  realtype fracRyRpo = 0.0244;

  realtype dRq = 1000 * ((-k2 * CaSS * Rq) + (VT_k4 * (1 - Rq))); // new n34
  ydot[37] = dRq;

  // Rq+=tinc*dRq;                              //new

  /*const  realtype k3=VT_k3*(2.0/3.0*((1.0-fracRyRp)/(1.0-fracRyRpo))+1.0/3.0);
     const  realtype O=k1*CaSSsquare*Rq/(k3+k1*CaSSsquare);*/

  /*const  realtype k1p=k1/(2.0/3.0*((1.0-fracRyRp)/(1.0-fracRyRpo))+1.0/3.0);
     const  realtype O=k1p*CaSSsquare*Rq/(VT_k3+k1p*CaSSsquare);*/

  const realtype O = k1 * CaSSsquare * Rq / (VT_k3 + k1 * CaSSsquare);  // new n33


  const realtype I_rel = VT_Vrel * O * (CaSR - CaSS); // new n31
  // I_leak=0.00008*(CaSR-Ca_i);                                                      //86
  const realtype I_leak = 0.00036 * (CaSR - Ca_i);  // new n29
  const realtype I_xfer = VT_Vxfer * (CaSS - Ca_i); // new n32

  // *** CaSR ***
  // const  realtype p_SERCA=0.75*fracPLB/fracPLBo+0.25; //PHOSPHOREGULATION
  const realtype p_SERCA = VT_p_PLB * fracPLB / fracPLBo + (1 - VT_p_PLB); // PHOSPHOREGULATION
  const realtype p_SERCA_square = p_SERCA * p_SERCA;
  const realtype SERCA = (VT_Vmaxup /
                          (1. + (VT_Kupsquare * p_SERCA_square / Caisquare))); // 87 n30 I_up

  // const  realtype SERCA=(0.006375/(1.+(VT_Kupsquare/Caisquare)));            //87 n30 I_up
  const realtype Bsr =
      1 / (1 + (VT_Bufsr) * (VT_Kbufsr) / (((VT_Kbufsr) + CaSR) * ((VT_Kbufsr) + CaSR)));
  const realtype dCaSR = Bsr * 1000 * (SERCA - I_rel - I_leak); // new n41
  ydot[43] = dCaSR;

  /*const  realtype CaCSQN=VT_Bufsr*CaSR/(CaSR+VT_Kbufsr);                              //95
     //const  realtype CaSum_1=CaCSQN+dCaSR+CaSR;
     const  realtype bjsr=VT_Bufsr-CaCSQN-dCaSR-CaSR+VT_Kbufsr; //CaSum_1;
     const  realtype cjsr=VT_Kbufsr*(CaCSQN+dCaSR+CaSR); //CaSum_1;
     CaSR=(sqrt(bjsr*bjsr+4*cjsr)-bjsr)/2.;                             // Lösung der quadr. Gleichung:
        CaSR^2+bjsr*CaSR-cjsr=0 //vgl. Zeng 1995*/


  // *** CaSS ***
  const ML_CalcType Bss =
      1 / (1 + (VT_Bufss) * (VT_Kbufss) / (((VT_Kbufss) + CaSS) * ((VT_Kbufss) + CaSS))); // new n42
  const ML_CalcType dCaSS = Bss * 1000 *
                            (-I_xfer * ((VT_Vc) / (VT_Vss)) + I_rel * ((VT_Vsr) / (VT_Vss)) +
                             (-I_CaL * (VT_inversevssF2) * (VT_C)));
  ydot[44] = dCaSS;
  /*const  realtype CaSSBuf=VT_Bufss*CaSS/(CaSS+VT_Kbufss);                             //new n42
     const  realtype dCaSS=tinc*1000*(-I_xfer*(VT_Vc/VT_Vss)+I_rel*(VT_Vsr/VT_Vss)+(-I_CaL*VT_inversevssF2*VT_C));
     const  realtype bjss=VT_Bufss-CaSSBuf-dCaSS-CaSS+VT_Kbufss; //CaSum_3;                             //new
     const  realtype cjss=VT_Kbufss*(CaSSBuf+dCaSS+CaSS); //CaSum_3;                                            //new
     CaSS=(sqrt(bjss*bjss+4.*cjss)-bjss)/2.;                            //new Lösung der quadr. Gleichung:
        CaSS^2+bjss*CaSS-cjss=0

     // *** Ca_i ***/

  // const  ML_CalcType kmtrpn=(1.45-0.45*(1-fracTnIp)/(1-fracTnIpo)); //PHOSPHOREGULATION
  const ML_CalcType Bc =
      1 / (1 + (VT_Bufc) * (VT_Kbufc) / (((VT_Kbufc) + Ca_i) * ((VT_Kbufc) + Ca_i))); // new n42
  // const  ML_CalcType Bc=1/(1+(VT_Bufc)*(VT_Kbufc*kmtrpn)/(((VT_Kbufc*kmtrpn)+Ca_i)*((VT_Kbufc*kmtrpn)+Ca_i)));                               //new
  // n42
  const ML_CalcType dCa_i = Bc * 1000 *
                            ((-(I_bCa + I_pCa - 2 * I_NaCa) * (VT_inverseviF2) * (VT_C)) -
                             (SERCA - I_leak) * (1 / (VT_VcdVsr)) + I_xfer);
  ydot[47] = dCa_i;

  /*const  realtype CaBuf=VT_Bufc*Ca_i/(Ca_i+VT_Kbufc);                         //93
     const  realtype dCai=tinc*1000*((-(I_bCa+I_pCa-2*I_NaCa)*VT_inverseviF2*VT_C)-(SERCA-I_leak)*(1/VT_VcdVsr)+I_xfer);
     const  realtype bc=VT_BufcPKbufc-CaBuf-dCai-Ca_i;
     const  realtype cc=VT_Kbufc*(CaBuf+dCai+Ca_i);
     Ca_i=(sqrt(bc*bc+4*cc)-bc)/2.;                                             // Lösung der quadr. Gleichung:
        Ca_i^2+bc*Ca_i-cc=0 //94 //vgl. Zeng 1995*/

  // Sodium and Potassium Dynamics
  const realtype dNai = -(I_Na + I_bNa + 3 * I_NaK + 3 * I_NaCa) * VT_inverseviF * VT_C; // 97
  ydot[45] = dNai * 1000;

  // Na_i+=tinc*dNai*1000;
  const realtype dKi = -(-I_app + I_K1 + I_to + I_K - 2 * I_NaK + I_pK) * VT_inverseviF *
                       VT_C;  // new obwohl nicht in Paper, aber in
  // TT2 Source Code
  ydot[46] = dKi * 1000;

  // K_i+=tinc*dKi*1000;

  const realtype fCa_inf = 0.6 / (1 + pow((CaSS / 0.05), 2)) + 0.4;
  const realtype tau_fCa = 80.0 / (1 + (CaSS / 0.05) * (CaSS / 0.05)) + 2.0;


  // set update gates prerequisites

  // m
  const double a_m = 1. / (1. + exp((-60. - svolt) / 5.));
  const double b_m = 0.1 / (1. + exp((svolt + 35.) / 5.)) + 0.10 / (1. + exp((svolt - 50.) / 200.));
  const realtype tau_m = a_m * b_m;
  const realtype m_inf =
      1. / ((1. + exp((-56.86 - svolt) / 9.03)) * (1. + exp((-56.86 - svolt) / 9.03)));

  realtype tau_h;
  realtype tau_j;

  // h
  if (svolt >= -40.) {
    const realtype AH_1 = 0.;
    const realtype BH_1 = (0.77 / (0.13 * (1. + exp(-(svolt + 10.66) / 11.1))));
    tau_h = 1.0 / (AH_1 + BH_1);
  } else {
    const realtype AH_2 = (0.057 * exp(-(svolt + 80.) / 6.8));
    const realtype BH_2 = (2.7 * exp(0.079 * svolt) + (3.1e5) * exp(0.3485 * svolt));
    tau_h = 1.0 / (AH_2 + BH_2);
  }
  const realtype h_inf =
      1. / ((1. + exp((svolt + 71.55) / 7.43)) * (1. + exp((svolt + 71.55) / 7.43)));

  // j
  if (svolt >= -40.) {
    const realtype AJ_1 = 0.;
    const realtype BJ_1 = (0.6 * exp((0.057) * svolt) / (1. + exp(-0.1 * (svolt + 32.))));
    tau_j = 1.0 / (AJ_1 + BJ_1);
  } else {
    const realtype AJ_2 =
        (((-2.5428e4) * exp(0.2444 * svolt) - (6.948e-6) * exp(-0.04391 * svolt)) *
         (svolt + 37.78) / (1. + exp(0.311 * (svolt + 79.23))));
    const realtype BJ_2 = (0.02424 * exp(-0.01052 * svolt) / (1. + exp(-0.1378 * (svolt + 40.14))));
    tau_j = 1.0 / (AJ_2 + BJ_2);
  }
  const realtype j_inf = h_inf;

  // xr1
  const realtype xr1_inf = 1. / (1. + exp((-26. - svolt) / 7.));
  const realtype a_Xr1 = 450. / (1. + exp((-45. - svolt) / 10.));
  const realtype b_Xr1 = 6. / (1. + exp((svolt - (-30.)) / 11.5));
  const realtype tau_xr1 = a_Xr1 * b_Xr1;

  // xr2
  const realtype xr2_inf = 1. / (1. + exp((svolt - (-88.)) / 24.));
  const realtype a_Xr2 = 3. / (1. + exp((-60. - svolt) / 20.));
  const realtype b_Xr2 = 1.12 / (1. + exp((svolt - 60.) / 20.));
  const realtype tau_xr2 = a_Xr2 * b_Xr2;

  // xs
  const realtype xs05 = -(1.5 * fracIksp / fracIkspo - 1.5);  // PHOSPHOREGULATION
  // const realtype xs05=1.5-(1.5*fracIksp/fracIkspo-1.5); //PHOSPHOREGULATION
  const realtype xs_inf = 1. / (1. + exp((-5. - (svolt - xs05)) / 14.));
  const realtype tau_xs =
      (1400. / (sqrt(1. + exp((5. - svolt) / 6)))) * (1. / (1. + exp((svolt - 35.) / 15.))) + 80;

  // r
  const realtype r_inf = 1. / (1. + exp((20 - svolt) / 6.));                    // 56
  const realtype tau_r = 9.5 * exp(-(svolt + 40.) * (svolt + 40.) / 1800.) + 0.8;   // 57

  // s
  const ML_CalcType s_inf =
      1. / (1. + exp((svolt + ((VT_s_inf_vHalf))) / 5.)); // =1./(1.+exp((V+20)/5.));     //58
  const ML_CalcType tau_s = (VT_tau_s_f1) *
                            exp(-(svolt + ((VT_tau_s_vHalf1))) * (svolt + ((VT_tau_s_vHalf1))) /
                                (VT_tau_s_slope1)) + (VT_tau_s_enable) * 5. /
                                                     (1. + exp((svolt - 20.) / 5.)) +
                            (VT_tau_s_add);


  // d
  const realtype d_inf = 1. / (1. + exp((-8. - svolt) / 7.5));  // 41 n7

  // const realtype d05=0.46+(0.722*fracLCCbp/fracLCCbpo-0.722);                // shift reduced to 0.46 in
  // non-phosphorylated state according to Chen 2002 ( L-type Ca2+ Channel Density and Regulation Are Altered in Failing
  // Human ...)
  // const realtype d_inf=1./(1.+exp((-d05-svolt)/7.5)); //41 n7                // steady state for activation gate
  // including phosphoregulation dependent shift d05

  const realtype a_d = 1.4 / (1. + exp((-35 - svolt) / 13)) + 0.25;
  const realtype b_d = 1.4 / (1. + exp((svolt + 5) / 5));
  const realtype c_d = 1. / (1. + exp((50 - svolt) / 20));
  const realtype tau_d = a_d * b_d + c_d;

  // f
  const realtype f_inf = 1. / (1. + exp((svolt + 20) / 7)); // 46 n12
  const realtype V27square = (svolt + 27) * (svolt + 27);              // new
  const realtype exsquare = V27square / (15 * 15);                  // new
  const realtype a_f = 1102.5 * exp(-exsquare);              // new n13
  const realtype b_f = 200. / (1. + exp((13 - svolt) / 10.));      // new n14
  const realtype g_f = (180. / (1. + exp((svolt + 30) / 10.))) + 20.; // new n15
  const realtype tau_f = a_f + b_f + g_f;                        // new n16
  // f2
  const realtype f2_inf = (.67 / (1. + exp((svolt + 35.) / 7.))) + .33;    // new n17
  const realtype a_f2 = 600. * exp(-(svolt + 25) * (svolt + 25) / 170.); // new n18
  const realtype b_f2 = 31. / (1. + exp((25 - svolt) / 10.));          // new n19
  const realtype c_f2 = 16. / (1. + exp((svolt + 30) / 10.));          // new n20
  const realtype tau_f2 = a_f2 + b_f2 + c_f2;                        // new n21

  // update gates

  const realtype dm = 1000 * (m_inf - m) / tau_m;
  ydot[30] = dm;
  const realtype dh = 1000 * (h_inf - h) / tau_h;
  ydot[31] = dh;
  const realtype dj = 1000 * (j_inf - j) / tau_j;
  ydot[32] = dj;
  const realtype dd = 1000 * (d_inf - d) / tau_d;
  ydot[33] = dd;
  const realtype df = 1000 * (f_inf - f) / tau_f;
  ydot[34] = df;
  const realtype df2 = 1000 * (f2_inf - f2) / tau_f2;
  ydot[35] = df2;
  const realtype dfCa = 1000 * (fCa_inf - fCa) / tau_fCa;
  ydot[36] = dfCa;

  const realtype dr = 1000 * (r_inf - r) / tau_r;
  ydot[38] = dr;

  const realtype ds = 1000 * (s_inf - s) / tau_s;
  ydot[39] = ds;

  const realtype dxr1 = 1000 * (xr1_inf - xr1) / tau_xr1;
  ydot[40] = dxr1;

  const realtype dxr2 = 1000 * (xr2_inf - xr2) / tau_xr2;
  ydot[41] = dxr2;

  const realtype dxs = 1000 * (xs_inf - xs) / tau_xs;
  ydot[42] = dxs;

  if (p[151] == 1) {
    y[48] = Vm_non_clamp;
    ydot[48] = 0;
  }

  // Bring dy/dt to the right side of the equation to have "0" on the left side (DAE: dy/dt=0); ydot represents the
  // equation (see above)
  rval[0] = ydot[0] - ypval[0];
  rval[1] = ydot[1];
  rval[2] = ydot[2];
  rval[3] = ydot[3];
  rval[4] = ydot[4] - ypval[4];
  rval[5] = ydot[5] - ypval[5];
  rval[6] = ydot[6] - ypval[6];
  rval[7] = ydot[7] - ypval[7];
  rval[8] = ydot[8] - ypval[8];
  rval[9] = ydot[9] - ypval[9];
  rval[10] = ydot[10];
  rval[11] = ydot[11];
  rval[12] = ydot[12];
  rval[13] = ydot[13];
  rval[14] = ydot[14];
  rval[15] = ydot[15] - ypval[15];
  rval[16] = ydot[16];
  rval[17] = ydot[17];
  rval[18] = ydot[18];
  rval[19] = ydot[19] - ypval[19];
  rval[20] = ydot[20] - ypval[20];
  rval[21] = ydot[21];
  rval[22] = ydot[22];
  rval[23] = ydot[23] - ypval[23];
  rval[24] = ydot[24] - ypval[24];
  rval[25] = ydot[25] - ypval[25];
  rval[26] = ydot[26] - ypval[26];
  rval[27] = ydot[27];
  rval[28] = ydot[28];
  rval[29] = ydot[29] - ypval[29];
  rval[30] = ydot[30] - ypval[30];
  rval[31] = ydot[31] - ypval[31];
  rval[32] = ydot[32] - ypval[32];
  rval[33] = ydot[33] - ypval[33];
  rval[34] = ydot[34] - ypval[34];
  rval[35] = ydot[35] - ypval[35];
  rval[36] = ydot[36] - ypval[36];
  rval[37] = ydot[37] - ypval[37];
  rval[38] = ydot[38] - ypval[38];
  rval[39] = ydot[39] - ypval[39];
  rval[40] = ydot[40] - ypval[40];
  rval[41] = ydot[41] - ypval[41];
  rval[42] = ydot[42] - ypval[42];
  rval[43] = ydot[43] - ypval[43];
  rval[44] = ydot[44] - ypval[44];
  rval[45] = ydot[45] - ypval[45];
  rval[46] = ydot[46] - ypval[46];
  rval[47] = ydot[47] - ypval[47];
  rval[48] = ydot[48] - ypval[48];

  return 0;
} // fT

void SaucermanTusscher::Print(ostream &tempstr, double tArg, ML_CalcType V) {
  // Don't forget the blank (' ') at the end!!

  tempstr << tArg << ' ' << V << ' '
          << m << ' ' << h << ' ' << j << ' ' << d << ' '
          << f << ' ' << f2 << ' ' << fCa << ' ' << Rq << ' ' << xr1 << ' ' << xr2 << ' '
          << xs << ' ' << r << ' ' << s << ' ' << Ca_i << ' ' << CaSR << ' ' << CaSS << ' ' << Na_i
          << ' ' << K_i << ' ' << O << ' ';
}

void SaucermanTusscher::LongPrint(ostream &tempstr, double tArg, ML_CalcType V) {
  Print(tempstr, tArg, V);

  const ML_CalcType svolt = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + svolt) + .5);

  const ML_CalcType Caisquare = Ca_i * Ca_i;

  // const  ML_CalcType CaSRsquare=CaSR*CaSR;
  const ML_CalcType CaSSsquare = CaSS * CaSS;

  // Nernst Potentials
  const ML_CalcType EK = CELLMODEL_PARAMVALUE(VT_RTONF) * (log((vK_o / K_i))); // 25
  const ML_CalcType VminEK = svolt - EK;
  const ML_CalcType ENa = CELLMODEL_PARAMVALUE(VT_RTONF) * (log((CELLMODEL_PARAMVALUE(VT_Na_o) / Na_i)));
  const ML_CalcType VminENa = svolt - ENa;
  const ML_CalcType EKs = CELLMODEL_PARAMVALUE(VT_RTONF) * (log(CELLMODEL_PARAMVALUE(VT_KopKNaNao) / (K_i + CELLMODEL_PARAMVALUE(VT_pKNa) * Na_i))); // 26

  const ML_CalcType I_pCa = CELLMODEL_PARAMVALUE(VT_g_pCa) * Ca_i / (CELLMODEL_PARAMVALUE(VT_KpCa) + Ca_i);                         // 82
  const ML_CalcType I_pK = ptTeaP->rec_ipK[Vi] * VminEK;                                 // 83
  const ML_CalcType I_to = CELLMODEL_PARAMVALUE(VT_g_to) * r * s *
                           VminEK;                                      // 55 Transient Outward Current
  const ML_CalcType I_Kr = CELLMODEL_PARAMVALUE(VT_g_Kr) * sqrt(vK_o / 5.4) * xr1 * xr2 *
                           VminEK;                   // 67 Rapid Delayed Rectifier
  // Current

  realtype fracIksp = Iksp / stdata.p[79];
  realtype fracIkspo = 1.8e-3 / stdata.p[79];
  const realtype p_Iks = 0.2 * fracIksp / fracIkspo + 0.8; // PHOSPHOREGULATION
  const realtype I_Ks =
      CELLMODEL_PARAMVALUE(VT_g_Ks) * p_Iks * xs * xs * (svolt - EKs); // 62 Slow Delayed Rectifier Current with
  // phosphoregulation
  // const  ML_CalcType I_Ks=CELLMODEL_PARAMVALUE(VT_g_Ks)*xs*xs*(svolt-EKs);                      //62 Slow Delayed Rectifier Current

  const ML_CalcType I_K = I_Kr + I_Ks;
  const ML_CalcType AK1 = 0.1 / (1. + exp(0.06 * (VminEK -
                                                  200)));                                                  // 77
  const ML_CalcType BK1 = (3. * exp(0.0002 * (svolt - EK + 100)) + exp(0.1 * (svolt - EK - 10))) /
                          (1. + exp(-0.5 * (svolt - EK))); // 78
  const ML_CalcType rec_iK1 = AK1 / (AK1 + BK1);
  const ML_CalcType I_K1 =
      CELLMODEL_PARAMVALUE(VT_g_K1) * rec_iK1 * VminEK;                                                        // 76
  // Inward
  // Rectifier
  // K+
  // Current
  const ML_CalcType I_bNa =
      CELLMODEL_PARAMVALUE(VT_g_bNa) * VminENa;                                                              // 84
  // Background
  // Currents
  const ML_CalcType ECa =
      0.5 * CELLMODEL_PARAMVALUE(VT_RTONF) * (log((CELLMODEL_PARAMVALUE(VT_Ca_o) / Ca_i)));                                         // 25
  const ML_CalcType I_bCa =
      CELLMODEL_PARAMVALUE(VT_g_bCa) * (svolt - ECa);                                                          // 85
  // Background
  // Currents

  // Compute scalar currents
  const ML_CalcType I_Na = vg_Na * m * m * m * h * j *
                           VminENa;                                                             // 27
  // Fast
  // Na+
  // Current
  const realtype CaL_P1 =
      4 * (svolt - 15.) * CELLMODEL_PARAMVALUE(VT_F) / CELLMODEL_PARAMVALUE(VT_RTONF) * (0.25 * exp(2 * (svolt - 15.) / CELLMODEL_PARAMVALUE(VT_RTONF))) /
      (exp(2 * (svolt - 15.) / CELLMODEL_PARAMVALUE(VT_RTONF)) - 1.);
  const realtype CaL_P2 = 4 * (svolt - 15.) * (CELLMODEL_PARAMVALUE(VT_F) / CELLMODEL_PARAMVALUE(VT_RTONF)) * (1 * CELLMODEL_PARAMVALUE(VT_Ca_o)) /
                          (exp(2 * (svolt - 15.) / CELLMODEL_PARAMVALUE(VT_RTONF)) - 1.);

  const realtype fracLCCap = LCCap / stdata.p[53];
  const realtype fracLCCapo = 0.028;
  const realtype fracLCCbp = LCCbp / stdata.p[53];
  const realtype fracLCCbpo = 0.0328;
  const realtype favail =
      1 * (0.05 * fracLCCbp / fracLCCbpo + 0.95); // PHOSPHOREGULATION weights in original Saucerman
  const realtype fpo =
      1 * (0.03 * fracLCCap / fracLCCapo + 0.97); // PHOSPHOREGULATION weights in original Saucerman
  // const  realtype favail = 1*(0.04*fracLCCbp/fracLCCbpo+0.96);    // PHOSPHOREGULATION reduced weights for
  // phosphoregulation
  // const  realtype fpo = 1*(0.02*fracLCCap/fracLCCapo+0.98);       // PHOSPHOREGULATION reduced weights for
  // phosphoregulation
  const realtype I_CaL = CELLMODEL_PARAMVALUE(VT_g_CaL) * d * f * f2 * fCa * favail * fpo * (CaL_P1 * CaSS - CaL_P2);

  const ML_CalcType I_NaCa =
      (ptTeaP->NaCa_P1[Vi]) * Na_i * Na_i * Na_i - (ptTeaP->NaCa_P2[Vi]) * Ca_i;

  // INaK
  const realtype KmNaIso = CELLMODEL_PARAMVALUE(VT_KmNa) * (CELLMODEL_PARAMVALUE(VT_fac_KmNa) * (stdata.p[155] / PKACI) +
                                         (1 - CELLMODEL_PARAMVALUE(VT_fac_KmNa)));  // new weights for
  // NaK pump current
  const realtype knakIso = CELLMODEL_PARAMVALUE(VT_knak) * (CELLMODEL_PARAMVALUE(VT_fac_knak) * (PKACI / stdata.p[155]) +
                                         (1 - CELLMODEL_PARAMVALUE(VT_fac_knak)));  // new weights for
  // NaK pump current
  // const realtype KmNaIso = CELLMODEL_PARAMVALUE(VT_KmNa) * (0.34 * (0.0083/PKACI) + 0.66);
  // const realtype knakIso = CELLMODEL_PARAMVALUE(VT_knak) * (0.011 * (PKACI/0.0083) + 0.989);
  const ML_CalcType I_NaK =
      knakIso * (vK_o / (vK_o + CELLMODEL_PARAMVALUE(VT_KmK))) * (Na_i / (Na_i + KmNaIso)) * (ptTeaP->NaK_P1[Vi]);

  const ML_CalcType kCaSR =
      CELLMODEL_PARAMVALUE(VT_max_sr) - ((CELLMODEL_PARAMVALUE(VT_max_sr) - CELLMODEL_PARAMVALUE(VT_min_sr)) / (1 + pow((CELLMODEL_PARAMVALUE(VT_EC) / CaSR), 2)));  // new n37
  // const  ML_CalcType k2=CELLMODEL_PARAMVALUE(VT_ks2)*kCaSR;                                             //new n36
  const ML_CalcType k1 = CELLMODEL_PARAMVALUE(VT_ks1) / kCaSR; // new n35

  // const  realtype fracRyRp = RyRp/stdata.p[63];
  // const  realtype fracRyRpo = 0.0244;
  /*const  realtype k3=CELLMODEL_PARAMVALUE(VT_k3)*(2.0/3.0*((1.0-fracRyRp)/(1.0-fracRyRpo))+1.0/3.0);
     O=k1*CaSSsquare*Rq/(k3+k1*CaSSsquare);                             //new n33*/

  // const  realtype k1p=k1/(2.0/3.0*((1.0-fracRyRp)/(1.0-fracRyRpo))+1.0/3.0);
  /*const  realtype k1p=k1/(1.0/3.0*((1.0-fracRyRp)/(1.0-fracRyRpo))+2.0/3.0);
     O=k1p*CaSSsquare*Rq/(CELLMODEL_PARAMVALUE(VT_k3)+k1p*CaSSsquare);*/

  O = k1 * CaSSsquare * Rq / (CELLMODEL_PARAMVALUE(VT_k3) + k1 * CaSSsquare);        // new n33

  const ML_CalcType I_rel = CELLMODEL_PARAMVALUE(VT_Vrel) * O * (CaSR - CaSS); // new n31
  const ML_CalcType I_leak = 0.00036 * (CaSR - Ca_i);      // new n29
  const ML_CalcType I_xfer = CELLMODEL_PARAMVALUE(VT_Vxfer) * (CaSS - Ca_i);  // new n32

  const realtype PLB = stdata.p[41] - PLBs;
  const realtype fracPLB = PLB / stdata.p[41];
  const realtype fracPLBo = 0.9926;

  // const  realtype p_SERCA=0.75*fracPLB/fracPLBo+0.25; //PHOSPHOREGULATION
  const realtype p_SERCA =
      CELLMODEL_PARAMVALUE(VT_p_PLB) * fracPLB / fracPLBo + (1 - CELLMODEL_PARAMVALUE(VT_p_PLB)); // PHOSPHOREGULATION
  const realtype p_SERCA_square = p_SERCA * p_SERCA;
  const realtype SERCA = (CELLMODEL_PARAMVALUE(VT_Vmaxup) /
                          (1. + (CELLMODEL_PARAMVALUE(VT_Kupsquare) * p_SERCA_square / Caisquare))); // 87 n30 I_up
  // const  ML_CalcType SERCA=(0.006375/(1.+(CELLMODEL_PARAMVALUE(VT_Kupsquare)/(Ca_i*Ca_i))));


  const ML_CalcType I_mem =
      I_K + I_K1 + I_to + I_Na + I_bNa + I_CaL + I_bCa + I_NaK + I_NaCa + I_pCa + I_pK;

  // currents are all in pA/pF

  tempstr << I_Na << ' ' << I_CaL << ' ' << I_bCa << ' ' << I_pCa << ' ' << I_to << ' ' << I_Ks
          << ' ' << I_Kr << ' ' << I_K1
          << ' ' << I_pK << ' ' << I_bNa << ' ' << I_NaK << ' ' << I_NaCa << ' ' << I_rel
          << ' ' << I_leak << ' ' << I_xfer << ' ' << SERCA << ' ' << I_mem << ' '
          << L << ' ' << R << ' ' << Gs << ' ' << b1ARtot << ' ' << b1ARd << ' ' << b1ARp << ' '
          << Gsagtptot << ' ' << Gsagdp << ' ' << Gsbg << ' '
          << Gsa_gtp << ' ' << Fsk << ' ' << AC << ' ' << PDE << ' ' << IBMX << ' ' << cAMPtot
          << ' ' << cAMP << ' ' << PKACI << ' ' << PKACII << ' '
          << PLBs << ' ' << Inhib1ptot << ' ' << Inhib1p << ' ' << PP1 << ' ' << LCCap << ' '
          << LCCbp << ' ' << RyRp << ' ' << TnIp << ' ' << Iks << ' ' <<
          Yotiao << ' ' << Iksp << ' ';
} // SaucermanTusscher::LongPrint

void SaucermanTusscher::GetParameterNames(vector<string> &getpara) {
  const int numpara = 19;
  const string ParaNames[numpara] =
      {"m", "h", "j", "d", "f", "f2", "fCa", "Rq", "Xr1", "Xr2", "Xs", "r", "s", "Cai", "CaSR",
       "CaSS", "Nai", "Ki", "O"};

  // const string ParaNames[numpara] = {"m", "h", "j", "d", "f", "f2", "fCa", "Rq", "Xr1", "Xr2", "Xs", "r", "s", "Cai",
  // "CaSR", "CaSS", "Nai", "Ki"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void SaucermanTusscher::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);

  // const int numpara=17;
  const int numpara = 46;

  // const string ParaNames[numpara] = {"I_Na", "I_CaL", "I_bCa", "I_pCa", "I_to", "I_Ks", "I_Kr", "I_K1", "I_pK",
  // "I_bNa", "I_NaK", "I_NaCa", "I_rel", "I_leak", "I_xfer", "SERCA", "I_mem"};
  const string ParaNames[numpara] =
      {"I_Na", "I_CaL", "I_bCa", "I_pCa", "I_to", "I_Ks", "I_Kr", "I_K1",
       "I_pK",
       "I_bNa",
       "I_NaK",
       "I_NaCa", "I_rel",
       "I_leak", "I_xfer", "SERCA", "I_mem", "L", "R", "Gs", "b1ARtot",
       "b1ARd",
       "b1ARp",
       "Gsagtptot",
       "Gsagdp", "Gsbg",
       "Gsa_gtp", "Fsk", "AC", "PDE", "IBMX", "cAMPtot", "cAMP", "PKACI",
       "PKACII",
       "PLBs",
       "Inhib1ptot",
       "Inhib1p", "PP1",
       "LCCap", "LCCbp", "RyRp", "TnIp", "Iks", "Yotiao", "Iksp"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

// function to check if the memory could be allocated successfully
static int check_flag(void *flagvalue, char *funcname, int opt) {
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if ((opt == 0) && (flagvalue == NULL)) {
    fprintf(stderr,
            "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
            funcname);
    return 1;
  } else if (opt == 1) {
    /* Check if flag < 0 */
    errflag = (int *) flagvalue;
    if (*errflag < 0) {
      fprintf(stderr,
              "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
              funcname, *errflag);
      return 1;
    }
  } else if ((opt == 2) && (flagvalue == NULL)) {
    /* Check if function returned NULL pointer - no memory allocated */
    fprintf(stderr,
            "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
            funcname);
    return 1;
  }

  return 0;
} // check_flag

// function to get information about the solver behavior and success
static void PrintFinalStats(void *mem) {
  int retval;
  long int nst, nni, nje, nre, nreLS, netf, ncfn, nge;

  retval = IDAGetNumSteps(mem, &nst);

  // check_flag(&retval, "IDAGetNumSteps", 1);
  retval = IDAGetNumResEvals(mem, &nre);

  // check_flag(&retval, "IDAGetNumResEvals", 1);
  retval = IDADlsGetNumJacEvals(mem, &nje);

  // check_flag(&retval, "IDADenseGetNumJacEvals", 1);
  retval = IDAGetNumNonlinSolvIters(mem, &nni);

  // check_flag(&retval, "IDAGetNumNonlinSolvIters", 1);
  retval = IDAGetNumErrTestFails(mem, &netf);

  // check_flag(&retval, "IDAGetNumErrTestFails", 1);
  retval = IDAGetNumNonlinSolvConvFails(mem, &ncfn);

  // check_flag(&retval, "IDAGetNumNonlinSolvConvFails", 1);
  retval = IDADlsGetNumResEvals(mem, &nreLS);

  // check_flag(&retval, "IDADenseGetNumResEvals", 1);
  retval = IDAGetNumGEvals(mem, &nge);

  // check_flag(&retval, "IDAGetNumGEvals", 1);

  printf("\nFinal Run Statistics: \n\n");
  printf("Number of steps                    = %ld\n", nst);
  printf("Number of residual evaluations     = %ld\n", nre + nreLS);
  printf("Number of Jacobian evaluations     = %ld\n", nje);
  printf("Number of nonlinear iterations     = %ld\n", nni);
  printf("Number of error test failures      = %ld\n", netf);
  printf("Number of nonlinear conv. failures = %ld\n", ncfn);
  printf("Number of root fn. evaluations     = %ld\n", nge);
} // PrintFinalStats
