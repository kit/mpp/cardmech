/* File: ShannonEtAlParameters.cpp
    Robin Moss - UHZ Freiburg/Bad Krozingen
    Based on https://models.physiomeproject.org/exposure/aeec124feedddcd9139685b2ff1131ae
    xml can also be found in the originals folder*/

#include <ShannonEtAlParameters.h>
ShannonEtAlParameters::ShannonEtAlParameters(const char *initFile, ML_CalcType tinc) {
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

ShannonEtAlParameters::~ShannonEtAlParameters() {}

void ShannonEtAlParameters::PrintParameters() {
  cout << "ShannonEtAlParameters:"<<endl;
  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= "     << P[i].value <<endl;
  }
}

void ShannonEtAlParameters::Init(const char *initFile, ML_CalcType tinc) {
  P[VT_T].name                       = "T";
  P[VT_R].name                       = "R";
  P[VT_F].name                       = "F";
  P[VT_Cm].name                      = "Cm";
  P[VT_cell_length].name             = "cell_length";
  P[VT_cell_radius].name             = "cell_radius";
  P[VT_stim_offset].name             = "stim_offset";
  P[VT_stim_period].name             = "stim_period";
  P[VT_stim_duration].name           = "stim_duration";
  P[VT_stim_amplitude].name          = "stim_amplitude";
  P[VT_Na_o].name                    = "Na_o";
  P[VT_Ca_o].name                    = "Ca_o";
  P[VT_K_o].name                     = "K_o";
  P[VT_K_i].name                     = "K_i";
  P[VT_Cl_o].name                    = "Cl_o";
  P[VT_Cl_i].name                    = "Cl_i";
  P[VT_Mg_i].name                    = "Mg_i";
  P[VT_g_INa].name                   = "g_INa";
  P[VT_Fx_Na_SL].name                = "Fx_Na_SL";
  P[VT_Fx_Na_jct].name               = "Fx_Na_jct";
  P[VT_g_INaBK].name                 = "g_INaBK";
  P[VT_Fx_NaBk_SL].name              = "Fx_NaBk_SL";
  P[VT_Fx_NaBk_jct].name             = "Fx_NaBk_jct";
  P[VT_g_NaK].name                   = "g_NaK";
  P[VT_Fx_NaK_jct].name              = "Fx_NaK_jct";
  P[VT_Fx_NaK_SL].name               = "Fx_NaK_SL";
  P[VT_Km_Nai].name                  = "Km_Nai";
  P[VT_Km_Ko].name                   = "Km_Ko";
  P[VT_H_NaK].name                   = "H_NaK";
  P[VT_g_Kr].name                    = "g_Kr";
  P[VT_g_Ks].name                    = "g_Ks";
  P[VT_pKNa].name                    = "pKNa";
  P[VT_Fx_Ks_jct].name               = "Fx_Ks_jct";
  P[VT_Fx_Ks_SL].name                = "Fx_Ks_SL";
  P[VT_g_Kp].name                    = "g_Kp";
  P[VT_g_tos].name                   = "g_tos";
  P[VT_g_tof].name                   = "g_tof";
  P[VT_g_Ki].name                    = "g_Ki";
  P[VT_g_Cl].name                    = "g_Cl";
  P[VT_Fx_Cl_jct].name               = "Fx_Cl_jct";
  P[VT_Fx_Cl_SL].name                = "Fx_Cl_SL";
  P[VT_Kd_ClCa].name                 = "Kd_ClCa";
  P[VT_g_Clbk].name                  = "g_Clbk";
  P[VT_g_CaL].name                   = "g_CaL";
  P[VT_PCa].name                     = "PCa";
  P[VT_PNa].name                     = "PNa";
  P[VT_PK].name                      = "PK";
  P[VT_Fx_ICaL_jct].name             = "Fx_ICaL_jct";
  P[VT_Fx_ICaL_SL].name              = "Fx_ICaL_SL";
  P[VT_gamma_Cai].name               = "gamma_Cai";
  P[VT_gamma_Cao].name               = "gamma_Cao";
  P[VT_gamma_Nai].name               = "gamma_Nai";
  P[VT_gamma_Nao].name               = "gamma_Nao";
  P[VT_gamma_Ki].name                = "gamma_Ki";
  P[VT_gamma_Ko].name                = "gamma_Ko";
  P[VT_Q10_CaL].name                 = "Q10_CaL";
  P[VT_g_NCX].name                   = "g_NCX";
  P[VT_Fx_NCX_jct].name              = "Fx_NCX_jct";
  P[VT_Fx_NCX_SL].name               = "Fx_NCX_SL";
  P[VT_Q10_NCX].name                 = "Q10_NCX";
  P[VT_K_mNai].name                  = "K_mNai";
  P[VT_K_mCao].name                  = "K_mCao";
  P[VT_K_mNao].name                  = "K_mNao";
  P[VT_K_mCai].name                  = "K_mCai";
  P[VT_Kd_act].name                  = "Kd_act";
  P[VT_ksat].name                    = "ksat";
  P[VT_eta].name                     = "eta";
  P[VT_HNa].name                     = "HNa";
  P[VT_g_Cap].name                   = "g_Cap";
  P[VT_Fx_SLCaP_jct].name            = "Fx_SLCaP_jct";
  P[VT_Fx_SLCaP_SL].name             = "Fx_SLCaP_SL";
  P[VT_Q10_SLCaP].name               = "Q10_SLCaP";
  P[VT_HCap].name                    = "HCap";
  P[VT_Km].name                      = "Km";
  P[VT_g_CaBK].name                  = "g_CaBK";
  P[VT_Fx_CaBk_jct].name             = "Fx_CaBk_jct";
  P[VT_Fx_CaBk_SL].name              = "Fx_CaBk_SL";
  P[VT_Max_SR].name                  = "Max_SR";
  P[VT_Min_SR].name                  = "Min_SR";
  P[VT_EC50_SR].name                 = "EC50_SR";
  P[VT_ks].name                      = "ks";
  P[VT_koCa].name                    = "koCa";
  P[VT_kom].name                     = "kom";
  P[VT_kiCa].name                    = "kiCa";
  P[VT_kim].name                     = "kim";
  P[VT_HSR].name                     = "HSR";
  P[VT_KSRleak].name                 = "KSRleak";
  P[VT_KSRPump].name                 = "KSRPump";
  P[VT_Q10_SRCaP].name               = "Q10_SRCaP";
  P[VT_Kmf].name                     = "Kmf";
  P[VT_Kmr].name                     = "Kmr";
  P[VT_HSRPump].name                 = "HSRPump";
  P[VT_Bmax_SL].name                 = "Bmax_SL";
  P[VT_Bmax_jct].name                = "Bmax_jct";
  P[VT_kon].name                     = "kon";
  P[VT_koff].name                    = "koff";
  P[VT_Bmax_SLB_SL].name             = "Bmax_SLB_SL";
  P[VT_Bmax_SLB_jct].name            = "Bmax_SLB_jct";
  P[VT_Bmax_SLHigh_SL].name          = "Bmax_SLHigh_SL";
  P[VT_Bmax_SLHigh_jct].name         = "Bmax_SLHigh_jct";
  P[VT_Bmax_Calsequestrin].name      = "Bmax_Calsequestrin";
  P[VT_kon_SL].name                  = "kon_SL";
  P[VT_kon_Calsequestrin].name       = "kon_Calsequestrin";
  P[VT_koff_SLB].name                = "koff_SLB";
  P[VT_koff_SLHigh].name             = "koff_SLHigh";
  P[VT_koff_Calsequestrin].name      = "koff_Calsequestrin";
  P[VT_Bmax_TroponinC].name          = "Bmax_TroponinC";
  P[VT_Bmax_TroponinC_Ca_Mg_Ca].name = "Bmax_TroponinC_Ca_Mg_Ca";
  P[VT_Bmax_TroponinC_Ca_Mg_Mg].name = "Bmax_TroponinC_Ca_Mg_Mg";
  P[VT_Bmax_Calmodulin].name         = "Bmax_Calmodulin";
  P[VT_Bmax_Myosin_Ca].name          = "Bmax_Myosin_Ca";
  P[VT_Bmax_Myosin_Mg].name          = "Bmax_Myosin_Mg";
  P[VT_Bmax_SRB].name                = "Bmax_SRB";
  P[VT_kon_TroponinC].name           = "kon_TroponinC";
  P[VT_kon_TroponinC_Ca_Mg_Ca].name  = "kon_TroponinC_Ca_Mg_Ca";
  P[VT_kon_TroponinC_Ca_Mg_Mg].name  = "kon_TroponinC_Ca_Mg_Mg";
  P[VT_kon_Calmodulin].name          = "kon_Calmodulin";
  P[VT_kon_Myosin_Ca].name           = "kon_Myosin_Ca";
  P[VT_kon_Myosin_Mg].name           = "kon_Myosin_Mg";
  P[VT_kon_SRB].name                 = "kon_SRB";
  P[VT_koff_TroponinC].name          = "koff_TroponinC";
  P[VT_koff_TroponinC_Ca_Mg_Ca].name = "koff_TroponinC_Ca_Mg_Ca";
  P[VT_koff_TroponinC_Ca_Mg_Mg].name = "koff_TroponinC_Ca_Mg_Mg";
  P[VT_koff_Calmodulin].name         = "koff_Calmodulin";
  P[VT_koff_Myosin_Ca].name          = "koff_Myosin_Ca";
  P[VT_koff_Myosin_Mg].name          = "koff_Myosin_Mg";
  P[VT_koff_SRB].name                = "koff_SRB";
  P[VT_V_init].name                  = "V_init";
  P[VT_m_init].name                  = "m_init";
  P[VT_h_init].name                  = "h_init";
  P[VT_j_init].name                  = "j_init";
  P[VT_Xr_init].name                 = "Xr_init";
  P[VT_Xs_init].name                 = "Xs_init";
  P[VT_Xtos_init].name               = "Xtos_init";
  P[VT_Ytos_init].name               = "Ytos_init";
  P[VT_Xtof_init].name               = "Xtof_init";
  P[VT_Ytof_init].name               = "Ytof_init";
  P[VT_Rtos_init].name               = "Rtos_init";
  P[VT_d_init].name                  = "d_init";
  P[VT_f_init].name                  = "f_init";
  P[VT_fCaB_SL_init].name            = "fCaB_SL_init";
  P[VT_fCaB_jct_init].name           = "fCaB_jct_init";
  P[VT_R_init].name                  = "R_init";
  P[VT_I_init].name                  = "I_init";
  P[VT_O_init].name                  = "O_init";
  P[VT_Na_SL_buf_init].name          = "Na_SL_buf_init";
  P[VT_Na_jct_buf_init].name         = "Na_jct_buf_init";
  P[VT_Na_i_init].name               = "Na_i_init";
  P[VT_Na_SL_init].name              = "Na_SL_init";
  P[VT_Na_jct_init].name             = "Na_jct_init";
  P[VT_Ca_SLB_SL_init].name          = "Ca_SLB_SL_init";
  P[VT_Ca_SLB_jct_init].name         = "Ca_SLB_jct_init";
  P[VT_Ca_SLHigh_SL_init].name       = "Ca_SLHigh_SL_init";
  P[VT_Ca_SLHigh_jct_init].name      = "Ca_SLHigh_jct_init";
  P[VT_Ca_Calsequestrin_init].name   = "Ca_Calsequestrin_init";
  P[VT_Ca_SR_init].name              = "Ca_SR_init";
  P[VT_Ca_SL_init].name              = "Ca_SL_init";
  P[VT_Ca_jct_init].name             = "Ca_jct_init";
  P[VT_Ca_i_init].name               = "Ca_i_init";
  P[VT_Ca_TroponinC_init].name       = "Ca_TroponinC_init";
  P[VT_Ca_TroponinC_Ca_Mg_init].name = "Ca_TroponinC_Ca_Mg_init";
  P[VT_Mg_TroponinC_Ca_Mg_init].name = "Mg_TroponinC_Ca_Mg_init";
  P[VT_Ca_Calmodulin_init].name      = "Ca_Calmodulin_init";
  P[VT_Ca_Myosin_init].name          = "Ca_Myosin_init";
  P[VT_Mg_Myosin_init].name          = "Mg_Myosin_init";
  P[VT_Ca_SRB_init].name             = "Ca_SRB_init";
  P[VT_RTdF].name                    = "VT_RTdF";
  P[VT_FdRT].name                    = "VT_FdRT";
  P[VT_sigma].name                   = "VT_sigma";
  P[VT_E_K].name                     = "VT_E_K";
  P[VT_Q_CaL].name                   = "VT_Q_CaL";
  P[VT_Q_NCX].name                   = "VT_Q_NCX";
  P[VT_Q_SLCaP].name                 = "VT_Q_SLCaP";
  P[VT_Q_SRCaP].name                 = "VT_Q_SRCaP";
  P[VT_Vol_Cell].name                = "VT_Vol_Cell";
  P[VT_Vol_SR].name                  = "VT_Vol_SR";
  P[VT_Vol_SL].name                  = "VT_Vol_SL";
  P[VT_Vol_jct].name                 = "VT_Vol_jct";
  P[VT_Vol_myo].name                 = "VT_Vol_myo";
  P[VT_RTdF].readFromFile            = false;
  P[VT_FdRT].readFromFile            = false;
  P[VT_sigma].readFromFile           = false;
  P[VT_E_K].readFromFile             = false;
  P[VT_Q_CaL].readFromFile           = false;
  P[VT_Q_NCX].readFromFile           = false;
  P[VT_Q_SLCaP].readFromFile         = false;
  P[VT_Q_SRCaP].readFromFile         = false;
  P[VT_Vol_Cell].readFromFile        = false;
  P[VT_Vol_SR].readFromFile          = false;
  P[VT_Vol_SL].readFromFile          = false;
  P[VT_Vol_jct].readFromFile         = false;
  P[VT_Vol_myo].readFromFile         = false;


  ParameterLoader EPL(initFile, EMT_ShannonEtAl);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);
  Calculate();
  InitTable(tinc);
} // ShannonEtAlParameters::Init

void ShannonEtAlParameters::Calculate() {
  P[VT_RTdF].value     = P[VT_R].value*(P[VT_T].value)/(P[VT_F].value);
  P[VT_FdRT].value     = 1.0/(P[VT_RTdF].value);
  P[VT_sigma].value    = ((exp(((P[VT_Na_o].value)/67.3000))) - 1.00000)/7.00000;
  P[VT_E_K].value      = (P[VT_RTdF].value)*(log((P[VT_K_o].value/P[VT_K_i].value)));
  P[VT_Q_CaL].value    = pow((P[VT_Q10_CaL].value), ((P[VT_T].value) - 310.000)/10.0000);
  P[VT_Q_NCX].value    = pow((P[VT_Q10_NCX].value), ((P[VT_T].value) - 310.000)/10.0000);
  P[VT_Q_SLCaP].value  = pow((P[VT_Q10_SLCaP].value), ((P[VT_T].value) - 310.000)/10.0000);
  P[VT_Q_SRCaP].value  = pow((P[VT_Q10_SRCaP].value), ((P[VT_T].value) - 310.000)/10.0000);
  P[VT_Vol_Cell].value = (3.14159*pow((P[VT_cell_radius].value)/1000.00, 2.00000)* (P[VT_cell_length].value))/pow(
      1000.00, 3.00000);
  P[VT_Vol_SR].value  =  0.0350000*(P[VT_Vol_Cell].value);
  P[VT_Vol_SL].value  =  0.0200000*(P[VT_Vol_Cell].value);
  P[VT_Vol_jct].value =  0.0539000*0.0100000*(P[VT_Vol_Cell].value);
  P[VT_Vol_myo].value =  0.650000*(P[VT_Vol_Cell].value);
}

void ShannonEtAlParameters::InitTable(ML_CalcType tinc) {
  ML_CalcType HT = tinc * 1000;

  for (double V = -RangeTabhalf+.0001; V < RangeTabhalf; V += dDivisionTab) {
    const int Vi = (int)(DivisionTab*(RangeTabhalf+V)+.5);

    // ina
    ah[Vi] = (V < -40.0000 ?  0.135000*(exp(((80.0000+V)/ -6.80000))) : 0.00000);
    bh[Vi] =
      (V <
       -40.0000 ?  3.56000*(exp((0.0790000*V)))+ 310000.*(exp((0.350000*V))) : 1.00000/
       (0.130000*(1.00000+(exp(((V+10.6600)/ -11.1000))))));
    aj[Vi] =
      (V <
       -40.0000 ? ( (-127140.*(exp((0.244400*V))) -  3.47400e-05*(exp((-0.0439100*V))))*1.00000*(V+37.7800))/
       (1.00000+(exp((0.311000*(V+79.2300))))) : 0.00000);
    bj[Vi] =
      (V <
       -40.0000 ? (0.121200*(exp((-0.0105200*V))))/
       (1.00000+(exp((-0.137800*(V+40.1400))))) : (0.300000*(exp((-2.53500e-07*V))))/
       (1.00000+(exp((-0.100000*(V+32.0000))))));
    am[Vi] =
      ((fabs((V+47.1300))) >
       0.00100000 ? (0.320000*1.00000*(V+47.1300))/(1.00000 - (exp((-0.100000*(V+47.1300))))) : 3.20000);
    bm[Vi] =  0.0800000*(exp((-V/11.0000)));

    // inak
    f_NaK[Vi] = 1.00000/
      (1.00000+ 0.124500*(exp((-0.100000*V*(P[VT_FdRT].value))))+ 0.0365000*(P[VT_sigma].value)*
       (exp((-V*(P[VT_FdRT].value)))));

    // ikr
    const ML_CalcType Xrv1 =
      ((fabs((V+7.00000))) >
       0.00100000 ? (0.00138000*1.00000*(V+7.00000))/(1.00000 - (exp((-0.123000*(V+7.00000))))) : 0.00138000/0.123000);
    const ML_CalcType Xrv2 =
      ((fabs((V+10.0000))) >
       0.00100000 ? (0.000610000*1.00000*(V+10.0000))/((exp((0.145000*(V+10.0000)))) - 1.00000) : 0.000610000/0.145000);
    Xrtau[Vi] = 1.00000/(Xrv1+Xrv2);
    Xrinf[Vi] = 1.00000/(1.00000+(exp((-(V+50.0000)/7.50000))));
    rr[Vi]    = 1.00000/(1.00000+(exp(((V+33.0000)/22.4000))));

    // iks
    const ML_CalcType Xsv1 = (7.19e-5 * (V + 30))/(1-exp(-0.148*(V +30)));
    const ML_CalcType Xsv2 = (1.31e-4 * (V + 30))/(-1+exp(0.0687*(V +30)));
    Xstau[Vi] = 1.00000/(Xsv1+Xsv2);
    Xsinf[Vi] = 1.00000/(1.00000+(exp((-(V-1.500000)/16.70000))));

    // itos
    Xtosinf[Vi] = 1.00000/(1.00000+exp(-(V+3.00000)/15.0000));
    Xtostau[Vi] = 9.00000/(1.00000+exp((V+3.00000)/15.0000))+0.500000;
    Ytosinf[Vi] = 1.00000/(1.00000+exp((V+33.5000)/10.0000));
    Ytostau[Vi] = 3000.00/(1.00000+exp((V+60.0000)/10.0000))+30.0000;
    Rtosinf[Vi] = 1.00000/(1.00000+exp((V+33.5000)/10.0000));
    Rtostau[Vi] = 2800.00/(1.00000+exp((V+60.0000)/10.0000))+220.000;

    // itof
    Xtoftau[Vi] =  3.50000*exp(-pow(V/30.0000, 2.00000))+1.50000;
    Ytoftau[Vi] =  20.0000/(1.00000+exp((V+33.5000)/10.0000))+20.0000;

    // ik1
    const ML_CalcType ak1 =  1.02000/(1.00000+exp(0.238500*(V - ((P[VT_E_K].value)+59.2150))));
    const ML_CalcType bk1 =
      (0.491240*exp(0.0803200*((V - (P[VT_E_K].value))+5.47600))+ 1.00000*
       exp(0.0617500*(V - ((P[VT_E_K].value)+594.310))))/(1.00000+exp(-0.514300*((V - (P[VT_E_K].value))+4.75300)));
    k1inf[Vi] = (ak1) / (ak1 + bk1);

    // icaL
    dinf[Vi] = 1.00000 / (1.00000 + exp(-(V + 14.50000)/6.00000));
    dtau[Vi] = dinf[Vi] * (1.0000 - exp(-(V + 14.50000)/6.00000)) / (0.035 * (V +14.5));

    finf[Vi] = (1.00000 / (1.00000 + exp((V + 35.06000)/3.60000))) +
      (0.60000 / (1.00000 + exp((50.00000 - V)/20.00000)));
    ftau[Vi] = 1 / (0.02 + 0.0197 * exp(-(pow(0.0337*(V+14.5), 2))));
  }
} // ShannonEtAlParameters::InitTable
