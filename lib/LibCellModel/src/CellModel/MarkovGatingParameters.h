/*! \file MarkovGatingParameters.h
   \brief Implementation of Markov model with gating current calculation
   (Hille, Ion Channels of Excitable Membranes)

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef MAKROVGATINGPARAMETERS_H
#define MAKROVGATINGPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_MarkovGatingParameters {
enum varType {
  VT_Tx = vtFirst,
  VT_E,
  VT_G,
  VT_A,
  VT_B,
  VT_C,
  VT_KABa,
  VT_KABz,
  VT_KBAa,
  VT_KBAz,
  VT_zgAB,
  VT_KBCa,
  VT_KBCz,
  VT_KCBa,
  VT_KCBz,
  VT_zgBC,
  VT_Amp,
  vtLast
};
}

using namespace NS_MarkovGatingParameters;

class MarkovGatingParameters : public vbNewElphyParameters {
 public:
  MarkovGatingParameters(const char*);
  ~MarkovGatingParameters() {}

  void PrintParameters();

  inline void Calculate() {}

  inline void InitTable() {}

  void Init(const char*);
};

#endif // ifndef MAKROVGATINGPARAMETERS_H
