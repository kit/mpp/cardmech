/**@file IyerMazhariWinslowNaParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef IYER_MAZHARI_WINSLOW_NA_PARAMETERS
#define IYER_MAZHARI_WINSLOW_NA_PARAMETERS

#include <ParameterLoader.h>

namespace NS_IyerMazhariWinslowNaParameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_Nao,
    VT_Nai,

    VT_C0Na,
    VT_C1Na,
    VT_C2Na,
    VT_C3Na,
    VT_C4Na,
    VT_O1Na,
    VT_O2Na,
    VT_CI0Na,
    VT_CI1Na,
    VT_CI2Na,
    VT_CI3Na,
    VT_CI4Na,
    VT_INa,

    VT_GNa,

    VT_Naa1,
    VT_Naa2,
    VT_Naa3,
    VT_Nab1,
    VT_Nab2,
    VT_Nab3,
    VT_Nag1,
    VT_Nag2,
    VT_Nag3,
    VT_Nad1,
    VT_Nad2,
    VT_Nad3,
    VT_NaOn1,
    VT_NaOn2,
    VT_NaOn3,
    VT_NaOf1,
    VT_NaOf2,
    VT_NaOf3,
    VT_Nagg1,
    VT_Nagg2,
    VT_Nagg3,
    VT_Nadd1,
    VT_Nadd2,
    VT_Nadd3,
    VT_Nae1,
    VT_Nae2,
    VT_Nae3,
    VT_NaO1,
    VT_NaO2,
    VT_NaO3,
    VT_Naeta1,
    VT_Naeta2,
    VT_Naeta3,
    VT_Nanu1,
    VT_Nanu2,
    VT_Nanu3,
    VT_NaCn1,
    VT_NaCn2,
    VT_NaCn3,
    VT_NaCf1,
    VT_NaCf2,
    VT_NaCf3,
    VT_NaScalinga,
    VT_NaQ,
    VT_Amp,
    vtLast
  };
} // namespace NS_IyerMazhariWinslowNaParameters

using namespace NS_IyerMazhariWinslowNaParameters;

class IyerMazhariWinslowNaParameters : public vbNewElphyParameters {
public:
  IyerMazhariWinslowNaParameters(const char *);

  ~IyerMazhariWinslowNaParameters() {}

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&NaQ-&Tx+1)*sizeof(T);};
  // virtual inline T* GetBase(void) {return Tx;};
  // virtual int GetNumParameters() { return 61; };
  void Init(const char *);

  void InitTable() {}

  void Calculate();
};

#endif // ifndef IYER_MAZHARI_WINSLOW_NA_PARAMETERS
