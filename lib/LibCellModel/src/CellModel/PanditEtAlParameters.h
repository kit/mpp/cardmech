/*      File: PanditEtAlParameters.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

/*! \file PanditEtAl.h
   \brief Implementation of electrophysiological model for describing rat ventricular myocytes
   modified/corrected from Pandit et al., Biophys J 2001

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef PANDITETALPARAMETERS_H
#define PANDITETALPARAMETERS_H

#define TASK 0
#define ChR2 0  // ChR2 Model by Williams et al. Plos Comp Biol 2013 9(9):e1003220

#include <ParameterLoader.h>

namespace NS_PanditEtAlParameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_V_cell,
    VT_V_nucleus,
    VT_V_mitochondria,
    VT_V_SS,
    VT_V_SR,
    VT_V_JSR,
    VT_V_NSR,
    VT_V_myo,
    VT_K_o,
    VT_Na_o,
    VT_Ca_o,
    VT_C_m,
    VT_g_Na,
    VT_g_CaL,
    VT_g_t,
    VT_It_frac_s,
    VT_g_ss,
    VT_g_K1,
    VT_g_BNa,
    VT_g_BCa,
    VT_g_BK,
    VT_g_f,
    VT_g_K_slow,
    VT_I_NaK,
    VT_K_mNa,
    VT_K_mK,
    VT_I_CaP,
    VT_k_NaCa,
    VT_d_NaCa,
    VT_Y_NaCa,
    VT_t_sc1,
    VT_t_sc2,
    VT_t_sc3,
    VT_t_sc4,
    VT_t_ssc1,
    VT_t_ssc2,
    VT_t_ssc3,
    VT_t_ssc4,
    VT_rc1,
    VT_rc2,
    VT_sc1,
    VT_sc2,
    VT_t_rssc1,
    VT_rssc1,
    VT_rssc2,
    VT_v1,
    VT_K_fb,
    VT_K_rb,
    VT_K_SR,
    VT_N_fb,
    VT_N_rb,
    VT_v_maxf,
    VT_v_maxr,
    VT_t_tr,
    VT_t_xfer,
    VT_Kpa,
    VT_Kma,
    VT_Kpb,
    VT_Kmb,
    VT_Kpc,
    VT_Kmc,
    VT_P_n,
    VT_P_m,
    VT_LTRPN,
    VT_HTRPN,
    VT_Kphtrpn,
    VT_Kmhtrpn,
    VT_Kpltrpn,
    VT_Kmltrpn,
    VT_CMDN,
    VT_CSQN,
    VT_EGTA,
    VT_KmCMDN,
    VT_KmCSQN,
    VT_KmEGTA,
    VT_no_s_ss,
    VT_Vm,
    VT_m,
    VT_h,
    VT_j,
    VT_d,
    VT_f11,
    VT_f12,
    VT_Ca_inact,
    VT_r,
    VT_s,
    VT_s_slow,
    VT_r_ss,
    VT_s_ss,
    VT_r_K_slow,
    VT_s_K_slow,
    VT_y,
    VT_Na_i,
    VT_K_i,
    VT_Ca_i,
    VT_Ca_NSR,
    VT_Ca_SS,
    VT_Ca_JSR,
    VT_PC1,
    VT_PO1,
    VT_PO2,
    VT_PC2,
    VT_ltrpn,
    VT_htrpn,
    VT_P_TASK,
    VT_O_TASK,
    VT_F,
    VT_RTdF,
    VT_RTd2F,
    VT_FdRT,
    VT_dC_m,
    VT_Amp,
#if ChR2
    VT_g_ChR,
    VT_g_KChR_frac,
    VT_g_NaChR_frac,
    VT_g_CaChR_frac,
    VT_ChRlambda,
    VT_ChRgamma,
    VT_ChRw_loss,
    VT_ChRepsilon1,
    VT_ChRepsilon2,
    VT_ChRG_d2,
    VT_ChRe_12dark,
    VT_ChRe_21dark,
    VT_ChRc_121,
    VT_ChRc_122,
    VT_ChRc_211,
    VT_ChRc_212,
    VT_ChRIrr,
    VT_ChRtau,
    VT_ChRtemp,
    VT_ChRp,
    VT_ChRO1,
    VT_ChRO2,
    VT_ChRC1,
    VT_ChRC2,
    VT_ChRF,
    VT_ChRS_0,
    VT_ChRe_12,
    VT_ChRe_21,
#endif // if ChR2
    vtLast
  };
} // namespace NS_PanditEtAlParameters

using namespace NS_PanditEtAlParameters;

class PanditEtAlParameters : public vbNewElphyParameters {
public:
  PanditEtAlParameters(const char *, ML_CalcType);

  ~PanditEtAlParameters();

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType);

  void Init(const char *, ML_CalcType);

  ML_CalcType et_Ca_inact;
  ML_CalcType et_m[RTDT];
  ML_CalcType et_h[RTDT];
  ML_CalcType et_j[RTDT];
  ML_CalcType minf[RTDT];
  ML_CalcType hinf[RTDT];
  ML_CalcType jinf[RTDT];
  ML_CalcType dinf[RTDT];
  ML_CalcType f11inf[RTDT];
  ML_CalcType f12inf[RTDT];
  ML_CalcType et_d[RTDT];
  ML_CalcType et_f11[RTDT];
  ML_CalcType et_f12[RTDT];
  ML_CalcType rinf[RTDT];
  ML_CalcType sinf[RTDT];
  ML_CalcType s_slowinf[RTDT];
  ML_CalcType et_r[RTDT];
  ML_CalcType et_s[RTDT];
  ML_CalcType et_s_slow[RTDT];
  ML_CalcType r_ssinf[RTDT];
  ML_CalcType s_ssinf[RTDT];
  ML_CalcType et_r_ss[RTDT];
  ML_CalcType et_s_ss[RTDT];
  ML_CalcType r_K_slowinf[RTDT];
  ML_CalcType s_K_slowinf[RTDT];
  ML_CalcType et_r_K_slow[RTDT];
  ML_CalcType et_s_K_slow[RTDT];
  ML_CalcType yinf[RTDT];
  ML_CalcType et_y[RTDT];
#if ChR2
  ML_CalcType ChRG[RTDT];
  ML_CalcType ChRG_d1[RTDT];
  ML_CalcType ChRG_r[RTDT];
#endif // if ChR2
}; // class PanditEtAlParameters
#endif // ifndef PANDITETALPARAMETERS_H
