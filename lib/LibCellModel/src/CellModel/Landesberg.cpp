/**@file Landesberg.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <Landesberg.h>

Landesberg::Landesberg(LandesbergParameters *pp) {
  lp = pp;
  Init();
}

void Landesberg::Init() {
  P1 = CELLMODEL_PARAMVALUE(VT_P1a);
  P0 = CELLMODEL_PARAMVALUE(VT_P0a);
  N1 = CELLMODEL_PARAMVALUE(VT_N1a);
}

ML_CalcType
Landesberg::Calc(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType &Ca,
                 int euler = 1) {
  return ForceEulerNEuler(euler, tinc / euler, stretch, Ca);
}

inline ML_CalcType
Landesberg::ForceEulerNEuler(int r, ML_CalcType tinc, ML_CalcType stretch, ML_CalcType &Ca) {
  double Force;

  while (r--) {
    Force =
        Overlap(stretch, lp->getOverlapID(), lp->getOverlapParameters()) * (P1 + N1) * CELLMODEL_PARAMVALUE(VT_dFmax);
    ML_CalcType p_k_off = CELLMODEL_PARAMVALUE(VT_p_k_on) /
                          (CELLMODEL_PARAMVALUE(VT_Kapp0) + CELLMODEL_PARAMVALUE(VT_Kapp1) * Force * +CELLMODEL_PARAMVALUE(VT_Kapp2) * Force * Force +
                           CELLMODEL_PARAMVALUE(VT_Kapp3) * Force * Force * Force);
    ML_CalcType p_k_on_Ca = CELLMODEL_PARAMVALUE(VT_p_k_on) * Ca;

    double kP0mN0 = p_k_off * P0 - p_k_on_Ca * (1.0 - P0 - P1 - N1);
    double kP0mP1 = CELLMODEL_PARAMVALUE(VT_p_f) * P0 - CELLMODEL_PARAMVALUE(VT_p_g) * P1;
    double kP1mN1 = p_k_off * P1 - p_k_on_Ca * N1;

    P0 += tinc * (-kP0mN0 - kP0mP1);
    P1 += tinc * (kP0mP1 - kP1mN1);
    N1 += tinc * (kP1mN1 - CELLMODEL_PARAMVALUE(VT_p_g) * N1);

    // Ca-=tinc*(kP0mN0+kP1mN1);
  }
  return Overlap(stretch, lp->getOverlapID(), lp->getOverlapParameters()) * (P1 + N1) * CELLMODEL_PARAMVALUE(VT_dFmax);
}

void Landesberg::Print(ostream &tempstr) {
  tempstr << (1 - P0 - P1 - N1) << ' ' << N1 << ' '
          << P0 << ' ' << P1 << ' ';
}

void Landesberg::GetParameterNames(vector<string> &getpara) {
  const int numpara = 4;
  const string ParaNames[numpara] = {"N0", "N1", "P0", "P1"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
