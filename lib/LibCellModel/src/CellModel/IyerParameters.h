/**@file IyerParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef IYER_PARAMETERS
#define IYER_PARAMETERS

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#include <ParameterLoader.h>
#include <HandleManager.h>

namespace NS_IyerParameters {
  enum varType {
    VT_R = vtFirst,
    VT_T_x,
    VT_F,
    VT_init_Vm,
    VT_init_Nai,
    VT_init_Nao,
    VT_init_Ki,
    VT_init_Ko,
    VT_init_Cai,
    VT_init_Cao,
    VT_init_IKv14Na,
    VT_init_CaSS,
    VT_init_ICaK,

    VT_init_ICa,
    VT_init_ICab,
    VT_init_INaCa,
    VT_init_INa,
    VT_init_INab,
    VT_init_INaK,
    VT_init_IKr,
    VT_init_IKs,
    VT_init_IK1,
    VT_init_Ito,
    VT_init_IpCa,
    VT_init_ICaK2,
    VT_RTdF,
    VT_Amp,
    vtLast
  };
} // namespace NS_IyerParameters

using namespace NS_IyerParameters;

class IyerParameters : public vbNewElphyParameters {
public:
  HandleManager<ML_CalcType> *HM;
  vbElphyParameters<ML_CalcType> **pEP;
  void **handle;

  IyerParameters(const char *);

  ~IyerParameters();

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&init_ICaK2-&R+1)*sizeof(T);};
  // virtual inline T* GetBase(void) {return R;};
  // virtual int GetNumParameters() { return 25; };
  void Init(const char *);

  void Calculate();

  void InitTable();
};

#endif // ifndef IYER_PARAMETERS

// Alter code:
/*template<class T>
   class IyerParameters: public vbElphyParameters<ML_CalcType>
   {
   public:
        IyerParameters(const char * initFile) {
                Init(initFile);
                HM=new HandleManager<ML_CalcType>(initFile);
                handle=HM->getHandle();
                pEP=HM->getpEP();
                //num=HM->num;
        };
        ~IyerParameters() {
                cerr<<"deleting handles ...\n";
                if (handle) delete[] handle;
                if(pEP) delete[] pEP;
                if(HM ) delete HM;
        };

        HandleManager<ML_CalcType> *HM;
        ML_CalcType *R;
        ML_CalcType *T_x;
        ML_CalcType *F;
        ML_CalcType *init_Vm;
        ML_CalcType *init_Nai;
        ML_CalcType *init_Nao;
        ML_CalcType *init_Ki;
        ML_CalcType *init_Ko;
        ML_CalcType *init_Cai;
        ML_CalcType *init_Cao;
        ML_CalcType *init_IKv14Na;
        ML_CalcType *init_CaSS;
        ML_CalcType *init_ICaK;

        ML_CalcType *init_ICa;
        ML_CalcType *init_ICab;
        ML_CalcType *init_INaCa;
        ML_CalcType *init_INa;
        ML_CalcType *init_INab;
        ML_CalcType *init_INaK;
        ML_CalcType *init_IKr;
        ML_CalcType *init_IKs;
        ML_CalcType *init_IK1;
        ML_CalcType *init_Ito;
        ML_CalcType *init_IpCa;
        ML_CalcType *init_ICaK2;

        vbElphyParameters<ML_CalcType> **pEP;
        void **handle;

        ML_CalcType RTdF;

        void PrintParameters()
        {
                cout << "#IyerParameter:\n";
                cout << "#*R = "<< *R << endl;
                cout << "#*T_x = "<< *T_x << endl;
                cout << "#*F = "<< *F << endl;
                cout << "#*init_Vm = "<< *init_Vm << endl;
                cout << "#*init_Nai = "<< *init_Nai << endl;
                cout << "#*init_Nao = "<< *init_Nao << endl;
                cout << "#*init_Ki = "<< *init_Ki << endl;
                cout << "#*init_Ko = "<< *init_Ko << endl;
                cout << "#*init_Cai = "<< *init_Cai << endl;
                cout << "#*init_Cao = "<< *init_Cao << endl;
                cout << "#*init_IKv14Na = "<< *init_IKv14Na << endl;
                cout << "#*init_CaSS = "<< *init_CaSS << endl;
                cout << "#*init_ICaK = " << *init_ICaK << endl;

                cout <<"#*init_ICa= " << *init_ICa << endl;
                cout <<"#*init_ICab= " << *init_ICab << endl;
                cout <<"#*init_INaCa= " << *init_INaCa << endl;
                cout <<"#*init_INa= " << *init_INa << endl;
                cout <<"#*init_INab= " << *init_INab << endl;
                cout <<"#*init_INaK= " << *init_INaK << endl;
                cout <<"#*init_IKr= " << *init_IKr << endl;
                cout <<"#*init_IKs= " << *init_IKs << endl;
                cout <<"#*init_IK1= " << *init_IK1 << endl;
                cout <<"#*init_Ito= " << *init_Ito << endl;
                cout <<"#*init_IpCa= " << *init_IpCa << endl;
                cout <<"#*init_ICaK2= " << *init_ICaK2 << endl;

        }

        virtual inline int GetSize(void) {return (&init_ICaK2-&R+1)*sizeof(T);};
        virtual inline T* GetBase(void) {return R;};
        virtual int GetNumParameters() { return 25; };

        void Init(const char * initFile) {
 #if KADEBUG
                cerr << "IyerParameters:init " << initFile << endl;
 #endif
                CellModelValues cmv;
                cmv.HeaderCheck(initFile, ElphyModelDescriptor[EMT_Iyer]);
                if (GetNumParameters() != cmv.getnumElements())
                        throw kaBaseException("IyerParameters:Init - Number of Parameters is incorrect: %d!=%d !",
                           GetNumParameters(), cmv.getnumElements());

                shmCreate(initFile, true, GetSize());

                T* addr=(T*)this->getAddress();
                R = addr;
                T_x=++addr;
                F=++addr;
                init_Vm=++addr;
                init_Nai=++addr;
                init_Nao=++addr;
                init_Ki=++addr;
                init_Ko=++addr;
                init_Cai=++addr;
                init_Cao=++addr;
                init_IKv14Na=++addr;
                init_CaSS=++addr;
                init_ICaK=++addr;

                init_ICa=++addr;
                init_ICab=++addr;
                init_INaCa=++addr;
                init_INa=++addr;
                init_INab=++addr;
                init_INaK=++addr;
                init_IKr=++addr;
                init_IKs=++addr;
                init_IK1=++addr;
                init_Ito=++addr;
                init_IpCa=++addr;
                init_ICaK2=++addr;

                if (this->numAttach()==1) {
                        cmv.Load(initFile, MT_ELPHY);

                        CellModelConstant *pcmc=&cmv.mc[0];

 * R = pcmc->Value;
 * T_x = (++pcmc)->Value;
 * F = (++pcmc)->Value;
 * init_Vm = (++pcmc)->Value;
 * init_Nai = (++pcmc)->Value;
 * init_Nao = (++pcmc)->Value;
 * init_Ki = (++pcmc)->Value;
 * init_Ko = (++pcmc)->Value;
 * init_Cai = (++pcmc)->Value;
 * init_Cao = (++pcmc)->Value;
 * init_IKv14Na = (++pcmc)->Value;
 * init_CaSS = (++pcmc)->Value;
 * init_ICaK = (++pcmc)->Value;

 * init_ICa = (++pcmc)->Value;
 * init_ICab = (++pcmc)->Value;
 * init_INaCa = (++pcmc)->Value;
 * init_INa = (++pcmc)->Value;
 * init_INab = (++pcmc)->Value;
 * init_INaK = (++pcmc)->Value;
 * init_IKr = (++pcmc)->Value;
 * init_IKs = (++pcmc)->Value;
 * init_IK1 = (++pcmc)->Value;
 * init_Ito = (++pcmc)->Value;
 * init_IpCa = (++pcmc)->Value;
 * init_ICaK2 = (++pcmc)->Value;

                }
                else{
 #if KADEBUG
                        cerr << "numAttach = " << this->numAttach() << ", reading the values from shared memory ...\n";
 #endif
                }
                Calculate();
                InitTable();
                nskaIPC::IPCKey::cleanUp();
        };
        void Calculate() {
                if (PrintParameterMode == PrintParameterModeOn) PrintParameters();
 #if KADEBUG
                cerr << "IyerParameters - Calculate ..." << endl;
 #endif
                RTdF=*R*(*T_x)/(*F);
        }
        void InitTable() {
 #if KADEBUG
                cerr << "IyerParameters - InitTable()" << endl;
                cerr << "no table 2 init ...\n";
 #endif
        }
   };*/
