/**@file BondarenkoCaParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef BONDARENKO_PARAMETERS
#define BONDARENKO_PARAMETERS

#include <ElphyModelBasis.h>
#include <ParameterLoader.h>

namespace NS_BondarenkoCaParameters {
  enum varType {
    VT_C_m = vtFirst,
    VT_g_CaL,
    VT_E_CaL,
    VT_C1,
    VT_C2,
    VT_C3,
    VT_C4,
    VT_I1,
    VT_I2,
    VT_I3,
    VT_O,
    VT_a1,
    VT_a2,
    VT_a3,
    VT_a4,
    VT_a5,
    VT_a6,
    VT_a7,
    VT_a8,
    VT_a9,
    VT_a10,
    VT_a11,
    VT_a12,
    VT_b1,
    VT_b2,
    VT_b3,
    VT_K_pcmax,
    VT_K_pchalf,
    VT_Ca_ss,
    VT_K_pcf1,
    VT_K_pcf2,
    VT_K_pcf3,
    VT_K_pcb,
    VT_dC_m,
    VT_Amp,
    vtLast
  };
} // namespace NS_BondarenkoCaParameters

using namespace NS_BondarenkoCaParameters;

class BondarenkoCaParameters : public vbNewElphyParameters {
public:
  BondarenkoCaParameters(const char *);

  ~BondarenkoCaParameters() {}

  void PrintParameters();

  // virtual inline int GetSize(void) { return (&dC_m-&C_m+1)*sizeof(T);};
  // virtual inline T* GetBase(void) { return C_m; };
  // virtual int GetNumParameters() { return 33; };
  void Init(const char *);

  void Calculate();

  void InitTable();
};


#endif /* BONDARENKO_PARAMETERS */
