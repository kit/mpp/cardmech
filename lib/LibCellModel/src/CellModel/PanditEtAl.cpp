/*! \file PanditEtAl.cpp
   \brief Implementation of Pandit et al rat ventricular myocyte model
   Biophys J 2001

   \author fs, CVRTI - University of Utah 2006
 */


#include <PanditEtAl.h>

PanditEtAl::PanditEtAl(PanditEtAlParameters *pp) {
  pPeaP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pPeaP, NS_PanditEtAlParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

PanditEtAl::~PanditEtAl() {}

inline bool PanditEtAl::AddHeteroValue(string desc, double val) {
#ifdef HETERO
  Parameter EP(desc, val);
  return PS->addDynamicParameter(EP);

#else // ifdef HETERO
  throw kaBaseException("function needs to be compiled with HETERO");
#endif // ifdef HETERO
}

inline int PanditEtAl::GetSize(void) {
  return sizeof(PanditEtAl) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(PanditEtAlParameters *)
#ifdef HETERO
    -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
      ;
}

void PanditEtAl::Init() {
#if KADEBUG
  cerr << "initializing Class: PanditEtAl ... " << endl;
#endif // if KADEBUG
  m = CELLMODEL_PARAMVALUE(VT_m);
  h = CELLMODEL_PARAMVALUE(VT_h);
  j = CELLMODEL_PARAMVALUE(VT_j);
  d = CELLMODEL_PARAMVALUE(VT_d);
  f11 = CELLMODEL_PARAMVALUE(VT_f11);
  f12 = CELLMODEL_PARAMVALUE(VT_f12);
  Ca_inact = CELLMODEL_PARAMVALUE(VT_Ca_inact);
  r = CELLMODEL_PARAMVALUE(VT_r);
  s = CELLMODEL_PARAMVALUE(VT_s);
  s_slow = CELLMODEL_PARAMVALUE(VT_s_slow);
  r_ss = CELLMODEL_PARAMVALUE(VT_r_ss);
  s_ss = CELLMODEL_PARAMVALUE(VT_s_ss);
  r_K_slow = CELLMODEL_PARAMVALUE(VT_r_K_slow);
  s_K_slow = CELLMODEL_PARAMVALUE(VT_s_K_slow);
  y = CELLMODEL_PARAMVALUE(VT_y);
  Na_i = CELLMODEL_PARAMVALUE(VT_Na_i);
  K_i = CELLMODEL_PARAMVALUE(VT_K_i);
  Ca_i = CELLMODEL_PARAMVALUE(VT_Ca_i);
  Ca_NSR = CELLMODEL_PARAMVALUE(VT_Ca_NSR);
  Ca_SS = CELLMODEL_PARAMVALUE(VT_Ca_SS);
  Ca_JSR = CELLMODEL_PARAMVALUE(VT_Ca_JSR);
  PC1 = CELLMODEL_PARAMVALUE(VT_PC1);
  PO1 = CELLMODEL_PARAMVALUE(VT_PO1);
  PO2 = CELLMODEL_PARAMVALUE(VT_PO2);
  PC2 = CELLMODEL_PARAMVALUE(VT_PC2);
  ltrpn = CELLMODEL_PARAMVALUE(VT_ltrpn);
  htrpn = CELLMODEL_PARAMVALUE(VT_htrpn);
#if ChR2
  ChRC1 = CELLMODEL_PARAMVALUE(VT_ChRC1);
  ChRO1 = CELLMODEL_PARAMVALUE(VT_ChRO1);
  ChRO2 = CELLMODEL_PARAMVALUE(VT_ChRO2);
  ChRC2 = CELLMODEL_PARAMVALUE(VT_ChRC2);
  ChRp  = CELLMODEL_PARAMVALUE(VT_ChRp);
#endif // if ChR2
} // PanditEtAl::Init

ML_CalcType
PanditEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
                 int euler = 1) {
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double Na_o = CELLMODEL_PARAMVALUE(VT_Na_o);
  const double K_o = CELLMODEL_PARAMVALUE(VT_K_o);
  const double Ca_o = CELLMODEL_PARAMVALUE(VT_Ca_o);
  const double F = CELLMODEL_PARAMVALUE(VT_F);
  const double FdRT = CELLMODEL_PARAMVALUE(VT_FdRT);
  const double VFdRT = V_int * CELLMODEL_PARAMVALUE(VT_FdRT);
  const double dexpVFdRT = exp(-VFdRT);
  const double E_Na = ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(Na_o / Na_i));
  const double minf = pPeaP->minf[Vi];
  const double hinf = pPeaP->hinf[Vi];
  const double jinf = pPeaP->jinf[Vi];

  m = minf - (minf - m) * pPeaP->et_m[Vi];
  h = hinf - (hinf - h) * pPeaP->et_h[Vi];
  j = jinf - (jinf - j) * pPeaP->et_j[Vi];
  const double I_Na = CELLMODEL_PARAMVALUE(VT_g_Na) * m * m * m * h * j * (V_int - E_Na);
  const double E_CaL = 65.;
  const double dinf = pPeaP->dinf[Vi];
  const double f11inf = pPeaP->f11inf[Vi];
  const double f12inf = pPeaP->f12inf[Vi];
  const double Ca_inactinf = 1. / (1. + Ca_SS / 0.01);

  // const double et_Ca_inact=exp(-tinc/0.009);
  d = dinf - (dinf - d) * pPeaP->et_d[Vi];
  f11 = f11inf - (f11inf - f11) * pPeaP->et_f11[Vi];
  f12 = f12inf - (f12inf - f12) * pPeaP->et_f12[Vi];
  Ca_inact = Ca_inactinf - (Ca_inactinf - Ca_inact) * pPeaP->et_Ca_inact;
  const double I_CaL =
      CELLMODEL_PARAMVALUE(VT_g_CaL) * d * ((0.9 + Ca_inact / 10) * f11 + (0.1 - Ca_inact / 10) * f12) *
      (V_int - E_CaL);
  const double E_K = ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_o / K_i));
  const double rinf = pPeaP->rinf[Vi];
  const double sinf = pPeaP->sinf[Vi];
  const double s_slowinf = pPeaP->s_slowinf[Vi];
  r = rinf - (rinf - r) * pPeaP->et_r[Vi];
  s = sinf - (sinf - s) * pPeaP->et_s[Vi];
  s_slow = s_slowinf - (s_slowinf - s_slow) * pPeaP->et_s_slow[Vi];
  const double a = CELLMODEL_PARAMVALUE(VT_It_frac_s);
  const double b = 1.0 - a;
  const double I_t = CELLMODEL_PARAMVALUE(VT_g_t) * r * (a * s + b * s_slow) * (V_int - E_K);
  const double r_ssinf = pPeaP->r_ssinf[Vi];
  const double s_ssinf = pPeaP->s_ssinf[Vi];
  r_ss = r_ssinf - (r_ssinf - r_ss) * pPeaP->et_r_ss[Vi];
  s_ss = s_ssinf - (s_ssinf - s_ss) * pPeaP->et_s_ss[Vi];
  if (CELLMODEL_PARAMVALUE(VT_no_s_ss) == 1.0)
    s_ss = 1.0;
  double I_ss = CELLMODEL_PARAMVALUE(VT_g_ss) * r_ss * s_ss * (V_int - E_K);

  const double r_K_slowinf = pPeaP->r_K_slowinf[Vi];
  const double s_K_slowinf = pPeaP->s_K_slowinf[Vi];
  r_K_slow = r_K_slowinf - (r_K_slowinf - r_K_slow) * pPeaP->et_r_K_slow[Vi];
  s_K_slow = s_K_slowinf - (s_K_slowinf - s_K_slow) * pPeaP->et_s_K_slow[Vi];
  const double I_K_slow = CELLMODEL_PARAMVALUE(VT_g_K_slow) * r_K_slow * s_K_slow * (V_int - E_K);

#if TASK
  double I_TASK = CELLMODEL_PARAMVALUE(VT_P_TASK)*VFdRT*F*(K_i-K_o*dexpVFdRT)/(1.-dexpVFdRT);
  I_ss   -= I_TASK; // Correction for I_ss
  I_TASK *= CELLMODEL_PARAMVALUE(VT_O_TASK);
#endif // if TASK
  const double I_K1 = (48. / (exp((V_int + 37.) / 25.) + exp((V_int + 37) / -25.)) + 10.)
                      * 0.0001 / (1. + exp((V_int - E_K - 76.77) / -17.))
                      + CELLMODEL_PARAMVALUE(VT_g_K1) * (V_int - E_K - 1.73)
                        / (1. + exp(1.613 * ((V_int - E_K - 1.73) * FdRT)) *
                                (1. + exp((K_o - 0.9988) / -0.124)));
  const double yinf = pPeaP->yinf[Vi];
  y = yinf - (yinf - y) * pPeaP->et_y[Vi];
  const double f_Na = 0.2, f_K = 0.8;
  const double I_fNa = CELLMODEL_PARAMVALUE(VT_g_f) * y * (f_Na * (V_int - E_Na));
  const double I_fK = CELLMODEL_PARAMVALUE(VT_g_f) * y * (f_K * (V_int - E_K));
  const double I_f = I_fNa + I_fK;
  const double I_BNa = CELLMODEL_PARAMVALUE(VT_g_BNa) * (V_int - E_Na);
  const double I_BK = CELLMODEL_PARAMVALUE(VT_g_BK) * (V_int - E_K);
  const double I_BCa = CELLMODEL_PARAMVALUE(VT_g_BCa) * (V_int - E_CaL);
  const double I_B = I_BNa + I_BK + I_BCa;
  const double sigma = (exp(Na_o / 67.3) - 1.) / 7.0;
  const double I_NaK =
      CELLMODEL_PARAMVALUE(VT_I_NaK) * (1. / (1. + 0.1245 * exp(-0.1 * VFdRT) + 0.0365 * sigma * dexpVFdRT))
      * K_o / (K_o + CELLMODEL_PARAMVALUE(VT_K_mK))
      * 1. / (1. + pow(CELLMODEL_PARAMVALUE(VT_K_mNa) / Na_i, 1.5));
  const double I_CaP = CELLMODEL_PARAMVALUE(VT_I_CaP) * (Ca_i / (Ca_i + 0.0004));
  const double Na_i3Ca_o = Na_i * Na_i * Na_i * Ca_o;
  const double Na_o3Ca_i = Na_o * Na_o * Na_o * Ca_i;
  const double Y_NaCa = CELLMODEL_PARAMVALUE(VT_Y_NaCa);
  const double I_NaCa = CELLMODEL_PARAMVALUE(VT_k_NaCa) * (Na_i3Ca_o * exp(0.03743 * Y_NaCa * V_int)
                                        - Na_o3Ca_i * exp(0.03743 * (Y_NaCa - 1.) * V_int))
                        / (1. + CELLMODEL_PARAMVALUE(VT_d_NaCa) * (Na_i3Ca_o + Na_o3Ca_i));
  const double Ca_SSn = pow(Ca_SS, CELLMODEL_PARAMVALUE(VT_P_n));
  const double Ca_SSm = pow(Ca_SS, CELLMODEL_PARAMVALUE(VT_P_m));
  const int RyRMaxi = 20;
  const double tincRyR = tinc / RyRMaxi;
  for (int i = 0; i < RyRMaxi; i++) {
    PC1 += tincRyR * (-CELLMODEL_PARAMVALUE(VT_Kpa) * Ca_SSn * PC1 + CELLMODEL_PARAMVALUE(VT_Kma) * PO1);
    if (PC1 < 0.)
      PC1 = 0.;
    else if (PC1 > 1.)
      PC1 = 1.;
    PO1 += tincRyR *
           (CELLMODEL_PARAMVALUE(VT_Kpa) * Ca_SSn * PC1 - CELLMODEL_PARAMVALUE(VT_Kma) * PO1 - CELLMODEL_PARAMVALUE(VT_Kpb) * Ca_SSm * PO1 +
            CELLMODEL_PARAMVALUE(VT_Kmb) * PO2 - CELLMODEL_PARAMVALUE(VT_Kpc) * PO1 + CELLMODEL_PARAMVALUE(VT_Kmc) * PC2);
    if (PO1 < 0.)
      PO1 = 0.;
    else if (PO1 > 1.)
      PO1 = 1.;
    PO2 += tincRyR * (CELLMODEL_PARAMVALUE(VT_Kpb) * Ca_SSm * PO1 - CELLMODEL_PARAMVALUE(VT_Kmb) * PO2);
    if (PO2 < 0.)
      PO2 = 0.;
    else if (PO2 > 1.)
      PO2 = 1.;
    PC2 = 1. - PC1 - PO1 - PO2;
    if (PC2 < 0.)
      PC2 = 0.;
    else if (PC2 > 1.)
      PC2 = 1.;
  }

#if ChR2
  const double ChRk_1 = CELLMODEL_PARAMVALUE(VT_ChRepsilon1)*CELLMODEL_PARAMVALUE(VT_ChRF)* ChRp;
  const double ChRk_2 = CELLMODEL_PARAMVALUE(VT_ChRepsilon2)*CELLMODEL_PARAMVALUE(VT_ChRF)* ChRp;

  ChRp  += 1000*tinc*((CELLMODEL_PARAMVALUE(VT_ChRS_0)-ChRp)/CELLMODEL_PARAMVALUE(VT_ChRtau));
  ChRC1 += 1000*tinc*(pPeaP->ChRG_r[Vi]*ChRC2 + pPeaP->ChRG_d1[Vi]*ChRO1 - ChRk_1*ChRC1);
  if (ChRC1 < 0.)
    ChRC1 = 0.; else if (ChRC1 > 1.)
    ChRC1 = 1.;
  ChRO1 += 1000*tinc*(ChRk_1*ChRC1 + CELLMODEL_PARAMVALUE(VT_ChRe_21)*ChRO2 - (pPeaP->ChRG_d1[Vi] + CELLMODEL_PARAMVALUE(VT_ChRe_12))*ChRO1);
  if (ChRO1 < 0.)
    ChRO1 = 0.; else if (ChRO1 > 1.)
    ChRO1 = 1.;
  ChRO2 += 1000*tinc*(ChRk_2*ChRC2 + CELLMODEL_PARAMVALUE(VT_ChRe_12)*ChRO1 - (CELLMODEL_PARAMVALUE(VT_ChRG_d2) + CELLMODEL_PARAMVALUE(VT_ChRe_21))*ChRO2);
  if (ChRO2 < 0.)
    ChRO2 = 0.; else if (ChRO2 > 1.)
    ChRO2 = 1.;
  ChRC2 = 1.-ChRC1-ChRO1-ChRO2;
  if (ChRC2 < 0.)
    ChRC2 = 0.; else if (ChRC2 > 1.)
    ChRC2 = 1.;

  const double ChRConductance = CELLMODEL_PARAMVALUE(VT_g_ChR)*pPeaP->ChRG[Vi]*(ChRO1+CELLMODEL_PARAMVALUE(VT_ChRgamma)*ChRO2);
  const double I_ChR2Na       = CELLMODEL_PARAMVALUE(VT_g_NaChR_frac)*ChRConductance*(V_int-E_Na);
  const double I_ChR2K        = CELLMODEL_PARAMVALUE(VT_g_KChR_frac)*ChRConductance*(V_int-E_K);
  const double I_ChR2Ca       = CELLMODEL_PARAMVALUE(VT_g_CaChR_frac)*ChRConductance*(V_int-E_CaL);
#endif // if ChR2

  const double J_rel = CELLMODEL_PARAMVALUE(VT_v1) * (PO1 + PO2) * (Ca_JSR - Ca_SS);
  const double f_b = pow(Ca_i / (CELLMODEL_PARAMVALUE(VT_K_fb)), CELLMODEL_PARAMVALUE(VT_N_fb));
  const double r_b = pow(Ca_NSR / (CELLMODEL_PARAMVALUE(VT_K_rb)), CELLMODEL_PARAMVALUE(VT_N_rb));
  const double J_up = CELLMODEL_PARAMVALUE(VT_K_SR) * (CELLMODEL_PARAMVALUE(VT_v_maxf) * f_b - CELLMODEL_PARAMVALUE(VT_v_maxr) * r_b) / (1. + f_b + r_b);
  const double J_tr = (Ca_NSR - Ca_JSR) / (CELLMODEL_PARAMVALUE(VT_t_tr));
  const double J_xfer = (Ca_SS - Ca_i) / (CELLMODEL_PARAMVALUE(VT_t_xfer));
  const double dHTRPNCa = (CELLMODEL_PARAMVALUE(VT_Kphtrpn) * Ca_i * (CELLMODEL_PARAMVALUE(VT_HTRPN) - htrpn) - CELLMODEL_PARAMVALUE(VT_Kmhtrpn) * htrpn);
  htrpn += tinc * dHTRPNCa;
  const double dLTRPNCa = (CELLMODEL_PARAMVALUE(VT_Kpltrpn) * Ca_i * (CELLMODEL_PARAMVALUE(VT_LTRPN) - ltrpn) - CELLMODEL_PARAMVALUE(VT_Kmltrpn) * ltrpn);
  ltrpn += tinc * dLTRPNCa;
  const double J_trpn = dHTRPNCa + dLTRPNCa;

  //   cerr << "I: " << I_Na << " " << I_BNa << " " << I_NaCa << " " << I_NaK << " " << I_fNa << endl;
  //    cerr << Na_i << endl;
  const double V_myo = CELLMODEL_PARAMVALUE(VT_V_myo);
  Na_i -= tinc * (I_Na
                  #if ChR2
                  +I_ChR2Na
                  #endif // if ChR2
                  + I_BNa + 3. * I_NaCa + 3. * I_NaK + I_fNa) / (V_myo * F);

  //    cerr << Na_i << endl;
  K_i -= tinc * (I_ss
                 #if TASK
                 +I_TASK
                 #endif // if TASK
                 #if ChR2
                 +I_ChR2K
                 #endif // if ChR2
                 + I_BK + I_t + I_K_slow + I_K1 + I_fK - 2. * I_NaK) / (V_myo * F);
  const double CMDN = CELLMODEL_PARAMVALUE(VT_CMDN);
  const double KmCMDN = CELLMODEL_PARAMVALUE(VT_KmCMDN);
  const double EGTA = CELLMODEL_PARAMVALUE(VT_EGTA);
  const double KmEGTA = CELLMODEL_PARAMVALUE(VT_KmEGTA);
  const double beta_i = 1. / (1. + CMDN * KmCMDN / ((KmCMDN + Ca_i) * (KmCMDN + Ca_i)) +
                              EGTA * KmEGTA / ((KmEGTA + Ca_i) * (KmEGTA + Ca_i)));
  Ca_i += tinc * beta_i * (J_xfer - J_up - J_trpn - (I_BCa - 2. * I_NaCa + I_CaP
#if ChR2
                                                        +I_ChR2Ca
#endif // if ChR2
                                                    ) / (2. * V_myo * F));
  const double beta_ss = 1. / (1. + CMDN * KmCMDN / ((KmCMDN + Ca_SS) * (KmCMDN + Ca_SS)));
  const double V_JSR = CELLMODEL_PARAMVALUE(VT_V_JSR);
  Ca_SS += tinc * beta_ss * (J_rel * V_JSR - J_xfer * V_myo - I_CaL / (2. * F)) / (CELLMODEL_PARAMVALUE(VT_V_SS));
  const double CSQN = CELLMODEL_PARAMVALUE(VT_CSQN);
  const double KmCSQN = CELLMODEL_PARAMVALUE(VT_KmCSQN);
  const double beta_JSR = 1. / (1. + CSQN * KmCSQN / ((KmCSQN + Ca_JSR) * (KmCSQN + Ca_JSR)));
  Ca_JSR += tinc * beta_JSR * (J_tr - J_rel);
  Ca_NSR += tinc * (J_up * V_myo - J_tr * V_JSR) / (CELLMODEL_PARAMVALUE(VT_V_NSR));
  return -(I_Na + I_CaL + I_t + I_K_slow + I_ss
           #if TASK
           +I_TASK
           #endif // if TASK
           #if ChR2
           +I_ChR2Na+I_ChR2K+I_ChR2Ca
           #endif // if ChR2
           + I_f + I_K1 + I_B + I_NaK + I_NaCa + I_CaP - i_external) * (CELLMODEL_PARAMVALUE(VT_dC_m)) * tinc;
} // PanditEtAl::Calc

void PanditEtAl::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' ';
  tempstr << m << ' ' << h << ' ' << j << ' ' << d << ' ' << f11 << ' ' << f12 << ' ';
  tempstr << Ca_inact << ' ' << r << ' ' << s << ' ' << s_slow << ' ' << r_ss << ' ' << s_ss << ' '
          << r_K_slow <<
          ' ' << s_K_slow << ' ' << y << ' ';
  tempstr << Na_i << ' ' << K_i << ' ' << Ca_i << ' ' << Ca_NSR << ' ' << Ca_SS << ' ' << Ca_JSR
          << ' ';
  tempstr << PC1 << ' ' << PO1 << ' ' << PO2 << ' ' << PC2 << ' ';
  tempstr << ltrpn << ' ' << htrpn << ' ';
#if ChR2
  tempstr << ChRC1 << ' ' <<  ChRC2 << ' ' <<  ChRO1 << ' ' <<  ChRO2 << ' ' <<  ChRp << ' ';
#endif // if ChR2
}

void PanditEtAl::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double Na_o = CELLMODEL_PARAMVALUE(VT_Na_o);
  const double K_o = CELLMODEL_PARAMVALUE(VT_K_o);
  const double Ca_o = CELLMODEL_PARAMVALUE(VT_Ca_o);

  // const double F=CELLMODEL_PARAMVALUE(VT_F);
  const double FdRT = CELLMODEL_PARAMVALUE(VT_FdRT);
  const double VFdRT = V_int * CELLMODEL_PARAMVALUE(VT_FdRT);
  const double dexpVFdRT = exp(-VFdRT);
  const double E_Na = ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(Na_o / Na_i));
  const double I_Na = CELLMODEL_PARAMVALUE(VT_g_Na) * m * m * m * h * j * (V_int - E_Na);

  /*    tempstr << Na_i <<'\n'; */
  /*    tempstr << E_Na <<'\n'; */
  tempstr << I_Na << ' ';
  const double E_CaL = 65.;
  const double I_CaL =
      CELLMODEL_PARAMVALUE(VT_g_CaL) * d * ((0.9 + Ca_inact / 10) * f11 + (0.1 - Ca_inact / 10) * f12) *
      (V_int - E_CaL);
  tempstr << I_CaL << ' ';
  const double E_K = ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_o / K_i));
  const double a = CELLMODEL_PARAMVALUE(VT_It_frac_s);
  const double b = 1.0 - a;
  const double I_t = CELLMODEL_PARAMVALUE(VT_g_t) * r * (a * s + b * s_slow) * (V_int - E_K);
  const double I_K_slow = CELLMODEL_PARAMVALUE(VT_g_K_slow) * r_K_slow * s_K_slow * (V_int - E_K);
  double I_ss = CELLMODEL_PARAMVALUE(VT_g_ss) * r_ss * s_ss * (V_int - E_K);
#if TASK
  double I_TASK = CELLMODEL_PARAMVALUE(VT_P_TASK)*VFdRT*F*(K_i-K_o*dexpVFdRT)/(1.-dexpVFdRT);
  I_ss   -= I_TASK;
  I_TASK *= CELLMODEL_PARAMVALUE(VT_O_TASK);
#endif // if TASK
  const double I_K1 = (48. / (exp((V_int + 37.) / 25.) + exp((V_int + 37) / -25.)) + 10.)
                      * 0.0001 / (1. + exp((V_int - E_K - 76.77) / -17.))
                      + CELLMODEL_PARAMVALUE(VT_g_K1) * (V_int - E_K - 1.73)
                        / (1. + exp(1.613 * ((V_int - E_K - 1.73) * FdRT)) *
                                (1. + exp((K_o - 0.9988) / -0.124)));
  tempstr << I_t << ' ' << I_K_slow << ' ' << I_ss << ' ' << I_K1 << ' ';
  const double f_Na = 0.2, f_K = 0.8;
  const double I_fNa = CELLMODEL_PARAMVALUE(VT_g_f) * y * (f_Na * (V_int - E_Na));
  const double I_fK = CELLMODEL_PARAMVALUE(VT_g_f) * y * (f_K * (V_int - E_K));
  tempstr << I_fNa << ' ' << I_fK << ' ';
  const double I_BNa = CELLMODEL_PARAMVALUE(VT_g_BNa) * (V_int - E_Na);
  const double I_BK = CELLMODEL_PARAMVALUE(VT_g_BK) * (V_int - E_K);
  const double I_BCa = CELLMODEL_PARAMVALUE(VT_g_BCa) * (V_int - E_CaL);
  tempstr << I_BNa << ' ' << I_BK << ' ' << I_BCa << ' ';
  const double sigma = (exp(CELLMODEL_PARAMVALUE(VT_Na_o) / 67.3) - 1.) / 7.0;
  const double I_NaK =
      CELLMODEL_PARAMVALUE(VT_I_NaK) * (1. / (1. + 0.1245 * exp(-0.1 * VFdRT) + 0.0365 * sigma * dexpVFdRT))
      * K_o / (K_o + CELLMODEL_PARAMVALUE(VT_K_mK))
      * 1. / (1. + pow(CELLMODEL_PARAMVALUE(VT_K_mNa) / Na_i, 1.5));
  const double I_CaP = CELLMODEL_PARAMVALUE(VT_I_CaP) * (Ca_i / (Ca_i + 0.0004));
  const double Na_i3Ca_o = Na_i * Na_i * Na_i * Ca_o;
  const double Na_o3Ca_i = Na_o * Na_o * Na_o * Ca_i;
  const double Y_NaCa = CELLMODEL_PARAMVALUE(VT_Y_NaCa);
  const double I_NaCa = CELLMODEL_PARAMVALUE(VT_k_NaCa) * (Na_i3Ca_o * exp(0.03743 * Y_NaCa * V_int)
                                        - Na_o3Ca_i * exp(0.03743 * (Y_NaCa - 1.) * V_int))
                        / (1. + CELLMODEL_PARAMVALUE(VT_d_NaCa) * (Na_i3Ca_o + Na_o3Ca_i));
  tempstr << I_NaK << ' ' << I_CaP << ' ' << I_NaCa << ' ';
  const double J_rel = CELLMODEL_PARAMVALUE(VT_v1) * (PO1 + PO2) * (Ca_JSR - Ca_SS);
  const double f_b = pow(Ca_i / (CELLMODEL_PARAMVALUE(VT_K_fb)), CELLMODEL_PARAMVALUE(VT_N_fb));
  const double r_b = pow(Ca_NSR / (CELLMODEL_PARAMVALUE(VT_K_rb)), CELLMODEL_PARAMVALUE(VT_N_rb));
  const double J_up = CELLMODEL_PARAMVALUE(VT_K_SR) * (CELLMODEL_PARAMVALUE(VT_v_maxf) * f_b - CELLMODEL_PARAMVALUE(VT_v_maxr) * r_b) / (1. + f_b + r_b);
  const double J_tr = (Ca_NSR - Ca_JSR) / (CELLMODEL_PARAMVALUE(VT_t_tr));
  const double J_xfer = (Ca_SS - Ca_i) / (CELLMODEL_PARAMVALUE(VT_t_xfer));
  tempstr << J_rel << ' ' << J_up << ' ' << J_tr << ' ' << J_xfer << ' ';
  const double dHTRPNCa = (CELLMODEL_PARAMVALUE(VT_Kphtrpn) * Ca_i * (CELLMODEL_PARAMVALUE(VT_HTRPN) - htrpn) - CELLMODEL_PARAMVALUE(VT_Kmhtrpn) * htrpn);
  const double dLTRPNCa = (CELLMODEL_PARAMVALUE(VT_Kpltrpn) * Ca_i * (CELLMODEL_PARAMVALUE(VT_LTRPN) - ltrpn) - CELLMODEL_PARAMVALUE(VT_Kmltrpn) * ltrpn);
  const double J_trpn = dHTRPNCa + dLTRPNCa;
  tempstr << J_trpn << ' ';
#if TASK
  tempstr << I_TASK <<' ';
#endif // if TASK
#if ChR2
  const double ChRConductance = CELLMODEL_PARAMVALUE(VT_g_ChR)*pPeaP->ChRG[Vi]*(ChRO1+CELLMODEL_PARAMVALUE(VT_ChRgamma)*ChRO2);
  const double I_ChR2Na       = CELLMODEL_PARAMVALUE(VT_g_NaChR_frac)*ChRConductance*(V_int-E_Na);
  const double I_ChR2K        = CELLMODEL_PARAMVALUE(VT_g_KChR_frac)*ChRConductance*(V_int-E_K);
  const double I_ChR2Ca       = CELLMODEL_PARAMVALUE(VT_g_CaChR_frac)*ChRConductance*(V_int-E_CaL);
  tempstr << I_ChR2Na <<' ' << I_ChR2K <<' ' << I_ChR2Ca <<' ';
#endif // if ChR2
  const double I_mem = I_Na + I_CaL + I_t + I_ss
                       #if TASK
                       +I_TASK
                       #endif // if TASK
                       #if ChR2
                       +I_ChR2Na+I_ChR2K
                       #endif // if ChR2
                       + I_fNa + I_fK + I_K1 + I_BNa + I_BK + I_BCa + I_NaK + I_NaCa + I_CaP;
  tempstr << I_mem << ' ';
} // PanditEtAl::LongPrint

void PanditEtAl::GetParameterNames(vector<string> &getpara) {
  const string ParaNames[] =
      {"m", "h", "j", "d", "f11",
       "f12",
       "Ca_inact",
       "r",
       "s", "s_slow", "r_ss", "s_ss",
       "r_K_slow", "s_K_slow", "y",
       "Na_i", "K_i", "Ca_i", "Ca_NSR", "Ca_SS",
       "Ca_JSR",
       "PC1",
       "PO1", "PO2", "PC2",
       "ltrpn", "htrpn"
#if ChR2
          ,           "ChRC1",           "ChRC2",            "ChRO1",              "ChRO2",
          "ChRp"
#endif // if ChR2
      };

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void PanditEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const string ParaNames[] =
      {"I_Na [nA]", "I_CaL [nA]", "I_t [nA]", "I_K_slow [nA]",
       "I_ss [nA]", "I_K1 [nA]", "I_fNa [nA]",
       "I_fK [nA]", "I_BNa [nA]",
       "I_BK n[A]", "I_BCa [nA]", "I_NaK [nA]", "I_CaP [nA]",
       "I_NaCa [nA]",
       "J_rel", "J_up", "J_tr", "J_xfer", "J_trpn"
#if TASK
          ,              "I_TASK [nA]"
#endif // if TASK
#if ChR2
          ,              "I_ChR2Na [nA]",              "I_ChR2K [nA]",                "I_ChR2Ca [nA]"
#endif // if ChR2
          , "I_mem"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}
