/**@file NobleEtAlParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef NOBLEETAL_PARAMETERS
#define NOBLEETAL_PARAMETERS

#include <ParameterLoader.h>

namespace NS_NobleEtAlParameters {
  enum varType {
    VT_R = vtFirst,
    VT_Tx,
    VT_F,
    VT_RTdF,
    VT_FdRT,
    VT_RTd2F,
    VT_FdRT1k25,
    VT_F2dRT,
    VT_C_m,
    VT_dC_m,
    VT_k_cachoff,
    VT_k_mk1,
    VT_k_mk,
    VT_k_mNa,
    VT_k_kNa,
    VT_k_dsoff,
    VT_k_mCaDS,
    VT_k_decay,
    VT_R_decay,
    VT_G_to,
    VT_G_pNa,
    VT_k_mCaCyt,
    VT_G_bNa,
    VT_G_Kr1,
    VT_G_Kr2,
    VT_K_Kr,
    VT_G_Ks,
    VT_I_NaKmax,
    VT_G_Na,
    VT_G_k1,
    VT_G_bCa,
    VT_k_NaCa,
    VT_d_NaCa,
    VT_Ca_o,
    VT_Na_o,
    VT_NNNO,
    VT_Na_ok3,
    VT_Fractl_CaL,
    VT_Fractl_NaCA,
    VT_K_b,
    VT_diffCa,
    VT_g,
    VT_n_NaCa,
    VT_k_cyCa,
    VT_k_xcs,
    VT_k_srCa,
    VT_radius,
    VT_length,
    VT_Vol,
    VT_V_cell,
    VT_V_ecs,
    VT_V_i,
    VT_V_up,
    VT_V_rel,
    VT_V_SRup,
    VT_V_SRrel,
    VT_V_ds,
    VT_k_1dV_iF,
    VT_k_1dV_i2F,
    VT_k_1dV_i2zF,
    VT_k_VupdVrel,
    VT_k_Vspez,
    VT_k_V_idV_SRup,
    VT_k_kckxdk_s,
    VT_k_kckx,
    VT_a_troponin,
    VT_b_troponin,
    VT_Troponin,
    VT_Calmod,
    VT_k_cont1,
    VT_k_cont2,
    VT_k_cont3,
    VT_k_cont4,
    VT_KCalTrop,
    VT_CBden,
    VT_Trop_IT,
    VT_Trop_SL,
    VT_SR_SL,
    VT_SR_IT,
    VT_SACSL,
    VT_SACSL2,
    VT_SACIT,
    VT_SL,
    VT_SLHST,
    VT_ITHST,
    VT_G_Castretch,
    VT_G_Nastretch,
    VT_G_Kstretch,
    VT_G_NSstretch,
    VT_G_ANstretch,
    VT_E_ANstretch,
    VT_E_ANstretch1k5,
    VT_E_NSstretch,
    VT_G_fibro,  // fibro is not used
    VT_C_fibro,  // fibro is not used
    VT_G_fibro_stretch,  // fibro is not used
    VT_C_fibro_stretch,  // fibro is not used
    VT_CellE,  // fibro is not used


    // Initialisierung

    VT_init_Basis_f,
    VT_init_Basis_f2,
    VT_init_Basis_f2ds,
    VT_init_Basis_x_r1,
    VT_init_Basis_x_r2,
    VT_init_Basis_x_s,
    VT_init_Basis_s,
    VT_init_Basis_f_activator,
    VT_init_Basis_f_product,
    VT_init_Basis_K_o,
    VT_init_Basis_Ca_i,
    VT_init_Basis_Ca_calmod,
    VT_init_Basis_Ca_troponin,
    VT_init_Basis_Ca_ds,
    VT_init_Basis_Ca_up,
    VT_init_Basis_Ca_rel,

    VT_init_m,
    VT_init_h,
    VT_init_d,
    VT_init_r,
    VT_init_Na_i,
    VT_init_K_i,

    VT_init_Stretch_Complex_f_LightChain,
    VT_init_Stretch_Complex_f_CrossBridge,
    VT_init_Vm,
    VT_Amp,
    vtLast
  };
} // namespace NS_NobleEtAlParameters

using namespace NS_NobleEtAlParameters;

class NobleEtAlParameters : public vbNewElphyParameters {
public:
  NobleEtAlParameters(const char *, ElphyModelType, ML_CalcType);

  ~NobleEtAlParameters() {}

  double exp50;  // =exp(50.0*FdRT);//dw
  double exp50_2;  // =exp(50.0*F2dRT);//dw
  double m_inf[RTDT];
  double exptau_m[RTDT];
  double h_inf[RTDT];
  double exptau_h[RTDT];
  double d_inf[RTDT];
  double exptau_d[RTDT];
  double f_inf[RTDT];
  double exptau_f[RTDT];
  double x_r1_inf[RTDT];
  double exptau_x_r1[RTDT];
  double x_r2_inf[RTDT];
  double exptau_x_r2[RTDT];
  double x_s_inf[RTDT];
  double exptau_x_s[RTDT];
  double s_inf[RTDT];
  double exptau_s[RTDT];
  double array_r[RTDT];
  double KI_Kr[RTDT];
  double KIpNa[RTDT];
  double KINaCaxg[RTDT];
  double KINaCaxg2[RTDT];
  double KCaLCa[RTDT];
  double KCaLx[RTDT];
  double KCaLCahinten[RTDT];
  double KCaLKhinten[RTDT];
  double KCaLNahinten[RTDT];

  void PrintParameters();

  // virtual inline int GetSize(void){return (&Vm-&R+1)*sizeof(T);};
  // virtual inline T* GetBase(void){return R;};
  // virtual int GetNumParameters() { return 95; };

  void Init(const char *, ElphyModelType, ML_CalcType);

  void Calculate();

  void InitTable(ML_CalcType);
}; // class NobleEtAlParameters

#endif // ifndef NOBLEETAL_PARAMETERS
