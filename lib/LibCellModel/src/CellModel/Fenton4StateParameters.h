/*      File: Fenton4StateParameters.h */

/*  Version 1.1 (2008-06-25) by fmw */

/*      Version 1.1 (2008-06-25)

 * Included 3 new ev-file parameters: Vol, Amp, and StimTime
 * LongPrint() now outputs J_fi, J_si and J_so
 */

#ifndef FENTON4STATEPARAMETERS_H
#define FENTON4STATEPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_Fenton4StateParameters {
  enum varType {
    VT_tau_v1_minus = vtFirst,
    VT_tau_v2_minus,
    VT_tau_v_plus,
    VT_tau_w1_minus,
    VT_tau_w2_minus,
    VT_tau_w_plus,
    VT_tau_fi,
    VT_tau_o1,
    VT_tau_o2,
    VT_tau_so1,
    VT_tau_so2,
    VT_tau_s1,
    VT_tau_s2,
    VT_tau_si,
    VT_tau_w_inf,
    VT_u_o,
    VT_u_u,
    VT_u_m,
    VT_u_p,
    VT_u_q,
    VT_u_r,
    VT_u_w_minus,
    VT_u_so,
    VT_u_s,
    VT_k_w_minus,
    VT_k_so,
    VT_k_s,
    VT_w_inf_asterisk,
    VT_Cm,
    VT_Vol,
    VT_Amp,
    VT_StimTime,
    VT_Vm_u_factor,
    VT_Vm_init,
    VT_Init_u,
    VT_Init_v,
    VT_Init_w,
    VT_Init_s,
    VT_minus_tau_v_plus_inv_tinc,
    VT_minus_tau_w_plus_inv_tinc,
    VT_minus_tau_si_inv_tinc,
    VT_Cm_Vm_u_factor_inv_tinc,
    VT_tinc,

    //            VT_Vm_u_factor_div1000,
    vtLast
  };

  const float uTabMin = -1;
  const float uTabMax = 3;
  const float uTabStepSize = 0.0001;
  const float uTabStepSizeInv = 1 / uTabStepSize;

  const unsigned int uTabArraySize = 40001;  // (const unsigned int) ( (uTabMax - uTabMin) / uTabStepSize ) +1;
} // namespace NS_Fenton4StateParameters

using namespace NS_Fenton4StateParameters;

class Fenton4StateParameters : public vbNewElphyParameters {
public:
  Fenton4StateParameters(const char *, ML_CalcType);

  ~Fenton4StateParameters();

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType);

  void Init(const char *, ML_CalcType);

  ML_CalcType tau_so_inv[uTabArraySize];
  ML_CalcType tau_w_minus_inv_tinc[uTabArraySize];
  ML_CalcType tau_v_minus_inv_tinc[uTabArraySize];
  ML_CalcType tau_s_inv_tinc[uTabArraySize];
  ML_CalcType tau_o_inv[uTabArraySize];

  ML_CalcType v_inf[uTabArraySize];
  ML_CalcType w_inf[uTabArraySize];

  ML_CalcType s_expr_tanh[uTabArraySize];
  ML_CalcType J_fi_expr_tinc[uTabArraySize];
  ML_CalcType J_so_tinc[uTabArraySize];
};

#endif // ifndef FENTON4STATEPARAMETERS_H
