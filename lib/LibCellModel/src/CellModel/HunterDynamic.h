/**@file HunterDynamic.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef HUNTERDYMAMIC_H
#define HUNTERDYMAMIC_H

#include <HunterDynamicParameters.h>

#undef CELLMODEL_PARAMVALUE
#define CELLMODEL_PARAMVALUE(a) hdp->P[NS_HunterDynamicParameters::a].value

class HunterDynamic : public vbForceModel<ML_CalcType> {
public:
  HunterDynamicParameters *hdp;
  ML_CalcType TCa;  // Troponin
  ML_CalcType z;  // Tropomyosin

  HunterDynamic(HunterDynamicParameters *);

  ~HunterDynamic() {}

  virtual inline int GetSize(void) {
    return sizeof(HunterDynamic) - sizeof(HunterDynamicParameters *) -
           sizeof(vbForceModel<ML_CalcType>);
  }

  virtual inline ML_CalcType *GetBase(void) { return 0; }

  virtual void Init();

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType &, int);

  virtual inline ML_CalcType ForceEulerNEuler(int, ML_CalcType, ML_CalcType, ML_CalcType &);

  virtual void steadyState(ML_CalcType, ML_CalcType);

  virtual void Print(ostream &);

  virtual void GetParameterNames(vector<string> &);
};

#endif // ifndef HUNTERDYMAMIC_H
