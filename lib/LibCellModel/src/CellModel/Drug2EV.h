/*! \file DrugEV.h
   \brief Functions for program DrugEV

   \version 1.0.0

   \date Created Christian Rombach (11.05.09) \n

   \author      Christian Rombach\n
   Institute of Biomedical Engineering\n
   Universitaet Karlsruhe (TH)\n
   http://www.ibt.uni-karlsruhe.de\n
 */
double
hill(double, double, double);  //!< Hill equation for modifying the conductance of the ion channels
