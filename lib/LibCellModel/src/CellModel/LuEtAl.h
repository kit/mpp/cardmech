/*! \file LuEtAl.h
   \brief Implementation of electrophysiological model for describing hERG channels
   from Lu et al., J Phys 2001

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef LUETAL
#define LUETAL

#include <LuEtAlParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_LuEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pIMW->P[NS_LuEtAlParameters::a].value
#endif // ifdef HETERO

class LuEtAl : public vbElphyModel<ML_CalcType> {
public:
  double Ki;
  double C0Kr;
  double C1Kr;
  double C2Kr;
  double OKr;
  double IKr;
  double Qb;
  LuEtAlParameters *pIMW;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  LuEtAl(LuEtAlParameters *pIMWArg);

  inline ~LuEtAl() {}

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual void Init();

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vmyo); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp);  /*0.*/  }

  virtual inline ML_CalcType GetStimTime() { return 0.; }

  virtual inline ML_CalcType GetVm() { return -.09066; }

  virtual inline ML_CalcType GetCai() { return 0; }

  virtual inline ML_CalcType GetCao() { return 0; }

  virtual inline ML_CalcType GetNai() { return 0; }

  virtual inline ML_CalcType GetNao() { return 0; }

  virtual inline ML_CalcType GetKi() { return Ki; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_Ko); }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return (ML_CalcType *) &Ki; }

  virtual inline void SetCai(ML_CalcType val) {}

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);

  virtual ML_CalcType
  Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim, ML_CalcType stretch, int euler);

  virtual int GetNumStatus() { return 6; }

  virtual void GetStatus(double *p) const;

  virtual void SetStatus(const double *p);
}; // class LuEtAl
#endif // ifndef LUETAL
