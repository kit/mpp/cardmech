/* File: MahajanEtAl.h
        automatically created by CellML2Elphymodel.pl
        Institute of Biomedical Engineering, Universität Karlsruhe (TH) */

#ifndef MAHAJANETAL
#define MAHAJANETAL

#include <MahajanEtAlParameters.h>

#define HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_MahajanEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) ptTeaP->P[NS_MahajanEtAlParameters::a].value
#endif // ifdef HETERO

class MahajanEtAl : public vbElphyModel<ML_CalcType> {
public:
  MahajanEtAlParameters *ptTeaP;
  ML_CalcType Ca_i;
  ML_CalcType xm;
  ML_CalcType xh;
  ML_CalcType xj;
  ML_CalcType Ca_dyad;
  ML_CalcType c1;
  ML_CalcType c2;
  ML_CalcType xi1ca;
  ML_CalcType xi1ba;
  ML_CalcType xi2ca;
  ML_CalcType xi2ba;
  ML_CalcType xr;
  ML_CalcType xs1;
  ML_CalcType xs2;
  ML_CalcType xtos;
  ML_CalcType ytos;
  ML_CalcType xtof;
  ML_CalcType ytof;
  ML_CalcType Na_i;
  ML_CalcType Ca_submem;
  ML_CalcType Ca_NSR;
  ML_CalcType Ca_JSR;
  ML_CalcType xir;
  ML_CalcType tropi;
  ML_CalcType trops;

  MahajanEtAl(MahajanEtAlParameters *pp);

  ~MahajanEtAl();

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType SurfaceToVolumeRatio() { return 1.0; }

  virtual inline ML_CalcType Volume() { return 1.15606568e-12; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_V_init); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return CELLMODEL_PARAMVALUE(VT_K_i); }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline ML_CalcType *GetBase(void) { return &Ca_i; }

  virtual inline ML_CalcType GetIto() { return 0.0; }

  virtual inline ML_CalcType GetIKr() { return 0.0; }

  virtual inline ML_CalcType GetIKs() { return 0.0; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType GetSpeedupMax(void) { return .0; }

  virtual ML_CalcType GetAmplitude(void) { return CELLMODEL_PARAMVALUE(VT_stim_amplitude); }

  virtual inline ML_CalcType GetStimTime() { return CELLMODEL_PARAMVALUE(VT_stim_duration); }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                           ML_CalcType stretch = 1., int euler = 2);

  virtual void Print(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);

  virtual inline ML_CalcType GetTCa() { return tropi / 1000.0; }

  virtual inline bool OutIsTCa() { return true; }
}; // class MahajanEtAl
#endif // ifndef MAHAJANETAL
