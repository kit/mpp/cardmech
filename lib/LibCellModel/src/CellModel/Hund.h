/*
 *  Hund.h
 *
 *
 *  Created by Christian Rombach on 04.08.08.
 *  Last modified Christian Rombach (15.09.09)
 *  Institute of Biomedical Engineering\n
 *  Universitaet Karlsruhe (TH)\n
 *  http://www.ibt.uni-karlsruhe.de\n
 *
 */

#ifndef HUND
#define HUND

#include <HundParameters.h>

#define HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_HundParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) ptTeaP->P[NS_HundParameters::a].value
#endif // ifdef HETERO

class Hund : public vbElphyModel<ML_CalcType> {
public:
  HundParameters *ptHP;

  // state variables (voltage & time)
  ML_CalcType Na_i, K_i, Cl_i;
  ML_CalcType Ca_ss, Ca_nsr, Ca_jsr, Ca_i;  // Ca component
  ML_CalcType H, m, J;  // Na
  ML_CalcType d, dp, f, f2, fca, fca2;  // ICaL
  ML_CalcType xs1, xs2;  // IKs
  ML_CalcType xr;  // IKr
  ML_CalcType ydv, ydv2, zdv;  // Ito
  ML_CalcType AA;  // Ito2
  ML_CalcType mL, hL;  // INal
  ML_CalcType ro, ri;  // Irel
  ML_CalcType CaMKtrap;

  Hund(HundParameters *pp);

  ~Hund();

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType SurfaceToVolumeRatio() { return 1.0; }

  virtual inline ML_CalcType Volume() { return 3.8 * 10e-13; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_V_init); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline ML_CalcType GetIto() { return 0.0; }

  virtual inline ML_CalcType GetIKr() { return 0.0; }

  virtual inline ML_CalcType GetIKs() { return 0.0; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &Ca_i; }

  virtual inline ML_CalcType GetSpeedupMax(void) { return .0; }

  virtual ML_CalcType GetAmplitude(void) { return CELLMODEL_PARAMVALUE(VT_stim_amplitude); }

  virtual inline ML_CalcType GetStimTime() { return CELLMODEL_PARAMVALUE(VT_stim_duration); }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                           ML_CalcType stretch = 1., int euler = 2);

  virtual void Print(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class Hund
#endif // ifndef HUND
