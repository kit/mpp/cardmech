/*      File: PanditEtAlParameters.cpp
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */


#include <PanditEtAlParameters.h>
PanditEtAlParameters::PanditEtAlParameters(const char *initFile, ML_CalcType tinc) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

PanditEtAlParameters::~PanditEtAlParameters() {
  // Destruktor
}

void PanditEtAlParameters::PrintParameters() {
  // print the parameter to the stdout
  cout<<"PanditEtAlParameters:"<<endl;
  for (int i = vtFirst; i < vtLast; i++)
    cout << P[i].name << " = " << P[i].value << endl;
}

void PanditEtAlParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "Loading the PanditEtAl parameter from " << initFile << " ...\n";
#endif // if KADEBUG

  // Initialization of the Parameters ...
  P[VT_Tx].name             =  "T";
  P[VT_V_cell].name         =      "V_cell";
  P[VT_V_nucleus].name      =   "V_nucleus";
  P[VT_V_mitochondria].name =      "V_mitochondria";
  P[VT_V_SS].name           =        "V_SS";
  P[VT_V_SR].name           =        "V_SR";
  P[VT_V_JSR].name          =       "V_JSR";
  P[VT_V_NSR].name          =       "V_NSR";
  P[VT_V_myo].name          =       "V_myo";
  P[VT_K_o].name            = "K_o";
  P[VT_Na_o].name           =        "Na_o";
  P[VT_Ca_o].name           =        "Ca_o";
  P[VT_C_m].name            = "C_m";
  P[VT_g_Na].name           =        "g_Na";
  P[VT_g_CaL].name          =       "g_CaL";
  P[VT_g_t].name            = "g_t";
  P[VT_It_frac_s].name      =       "It_frac_s";
  P[VT_g_ss].name           =        "g_ss";
  P[VT_g_K1].name           =        "g_K1";
  P[VT_g_BNa].name          =       "g_BNa";
  P[VT_g_BCa].name          =       "g_BCa";
  P[VT_g_BK].name           =        "g_BK";
  P[VT_g_f].name            = "g_f";
  P[VT_g_K_slow].name       =        "g_K_slow";
  P[VT_I_NaK].name          =       "I_NaK";
  P[VT_K_mNa].name          =       "K_mNa";
  P[VT_K_mK].name           =        "K_mK";
  P[VT_I_CaP].name          =       "I_CaP";
  P[VT_k_NaCa].name         =      "k_NaCa";
  P[VT_d_NaCa].name         =      "d_NaCa";
  P[VT_Y_NaCa].name         =      "Y_NaCa";
  P[VT_t_sc1].name          =       "t_sc1";
  P[VT_t_sc2].name          =       "t_sc2";
  P[VT_t_sc3].name          =       "t_sc3";
  P[VT_t_sc4].name          =       "t_sc4";
  P[VT_t_ssc1].name         =      "t_ssc1";
  P[VT_t_ssc2].name         =      "t_ssc2";
  P[VT_t_ssc3].name         =      "t_ssc3";
  P[VT_t_ssc4].name         =      "t_ssc4";
  P[VT_rc1].name            =     "rc1";
  P[VT_rc2].name            =     "rc2";
  P[VT_sc1].name            =     "sc1";
  P[VT_sc2].name            =     "sc2";
  P[VT_t_rssc1].name        = "t_rssc1";
  P[VT_rssc1].name          =   "rssc1";
  P[VT_rssc2].name          =   "rssc2";
  P[VT_v1].name             =  "v1";
  P[VT_K_fb].name           =        "K_fb";
  P[VT_K_rb].name           =        "K_rb";
  P[VT_K_SR].name           =        "K_SR";
  P[VT_N_fb].name           =        "N_fb";
  P[VT_N_rb].name           =        "N_rb";
  P[VT_v_maxf].name         =      "v_maxf";
  P[VT_v_maxr].name         =      "v_maxr";
  P[VT_t_tr].name           =        "t_tr";
  P[VT_t_xfer].name         =      "t_xfer";
  P[VT_Kpa].name            = "Kpa";
  P[VT_Kma].name            = "Kma";
  P[VT_Kpb].name            = "Kpb";
  P[VT_Kmb].name            = "Kmb";
  P[VT_Kpc].name            = "Kpc";
  P[VT_Kmc].name            = "Kmc";
  P[VT_P_n].name            = "P_n";
  P[VT_P_m].name            = "P_m";
  P[VT_LTRPN].name          =       "LTRPN";
  P[VT_HTRPN].name          =       "HTRPN";
  P[VT_Kphtrpn].name        =     "Kphtrpn";
  P[VT_Kmhtrpn].name        =     "Kmhtrpn";
  P[VT_Kpltrpn].name        =     "Kpltrpn";
  P[VT_Kmltrpn].name        =     "Kmltrpn";
  P[VT_CMDN].name           =        "CMDN";
  P[VT_CSQN].name           =        "CSQN";
  P[VT_EGTA].name           =        "EGTA";
  P[VT_KmCMDN].name         =      "KmCMDN";
  P[VT_KmCSQN].name         =      "KmCSQN";
  P[VT_KmEGTA].name         =      "KmEGTA";
  P[VT_no_s_ss].name        = "no_s_ss";
  P[VT_Vm].name             =  "Init_Vm";
  P[VT_m].name              =   "Init_m";
  P[VT_h].name              =   "Init_h";
  P[VT_j].name              =   "Init_j";
  P[VT_d].name              =   "Init_d";
  P[VT_f11].name            = "Init_f11";
  P[VT_f12].name            = "Init_f12";
  P[VT_Ca_inact].name       =    "Init_Ca_inact";
  P[VT_r].name              =   "Init_r";
  P[VT_s].name              =   "Init_s";
  P[VT_s_slow].name         =      "Init_s_slow";
  P[VT_r_ss].name           =        "Init_r_ss";
  P[VT_s_ss].name           =        "Init_s_ss";
  P[VT_y].name              =   "Init_y";
  P[VT_Na_i].name           =        "Init_Na_i";
  P[VT_K_i].name            = "Init_K_i";
  P[VT_Ca_i].name           =        "Init_Ca_i";
  P[VT_Ca_NSR].name         =      "Init_Ca_NSR";
  P[VT_Ca_SS].name          =       "Init_Ca_SS";
  P[VT_Ca_JSR].name         =      "Init_Ca_JSR";
  P[VT_PC1].name            = "Init_PC1";
  P[VT_PO1].name            = "Init_PO1";
  P[VT_PO2].name            = "Init_PO2";
  P[VT_PC2].name            = "Init_PC2";
  P[VT_ltrpn].name          =       "Init_ltrpn";
  P[VT_htrpn].name          =       "Init_htrpn";
  P[VT_r_K_slow].name       =        "Init_r_K_slow";
  P[VT_s_K_slow].name       =        "Init_s_K_slow";
  P[VT_P_TASK].name         =      "P_TASK";
  P[VT_O_TASK].name         =      "O_TASK";
  P[VT_F].name              =   "F";
  P[VT_RTdF].name           =        "RTdF";
  P[VT_RTd2F].name          =       "RTd2F";
  P[VT_FdRT].name           =        "FdRT";
  P[VT_dC_m].name           =        "dC_m";
  P[VT_Amp].name            =         "Amp";

  P[VT_F].readFromFile     = false;
  P[VT_RTdF].readFromFile  = false;
  P[VT_RTd2F].readFromFile = false;
  P[VT_FdRT].readFromFile  = false;
  P[VT_dC_m].readFromFile  = false;
#if TASK
#else
  P[VT_P_TASK].readFromFile = false;
  P[VT_O_TASK].readFromFile = false;
#endif // if TASK

#if ChR2
  P[VT_g_ChR].name           =   "g_ChR";
  P[VT_g_KChR_frac].name     =     "g_KChR_frac";
  P[VT_g_NaChR_frac].name    =    "g_NaChR_frac";
  P[VT_g_CaChR_frac].name    =    "g_CaChR_frac";
  P[VT_ChRlambda].name       =       "ChRlambda";
  P[VT_ChRgamma].name        =        "ChRgamma";
  P[VT_ChRw_loss].name       =       "ChRw_loss";
  P[VT_ChRepsilon1].name     =     "ChRepsilon1";
  P[VT_ChRepsilon2].name     =     "ChRepsilon2";
  P[VT_ChRG_d2].name         = "ChRG_d2";
  P[VT_ChRe_12dark].name     =     "ChRe_12dark";
  P[VT_ChRe_21dark].name     =     "ChRe_21dark";
  P[VT_ChRc_121].name        =        "ChRc_121";
  P[VT_ChRc_122].name        =        "ChRc_122";
  P[VT_ChRc_211].name        =        "ChRc_211";
  P[VT_ChRc_212].name        =        "ChRc_212";
  P[VT_ChRIrr].name          =  "ChRIrr";
  P[VT_ChRtau].name          =  "ChRtau";
  P[VT_ChRtemp].name         = "ChRtemp";
  P[VT_ChRp].name            =    "Init_ChRp";
  P[VT_ChRO1].name           =   "Init_ChRO1";
  P[VT_ChRO2].name           =   "Init_ChRO2";
  P[VT_ChRC1].name           =   "Init_ChRC1";
  P[VT_ChRC2].name           =   "Init_ChRC2";
  P[VT_ChRF].name            =    "ChRF";
  P[VT_ChRS_0].name          =  "ChRS_0";
  P[VT_ChRe_12].name         = "ChRe_12";
  P[VT_ChRe_21].name         = "ChRe_21";
  P[VT_ChRF].readFromFile    = false;
  P[VT_ChRS_0].readFromFile  = false;
  P[VT_ChRe_12].readFromFile = false;
  P[VT_ChRe_21].readFromFile = false;
#endif // if ChR2
  ParameterLoader EPL(initFile, EMT_PanditEtAl);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  // End Initialization of the Parameters ...

  Calculate();
  InitTable(tinc);
} // PanditEtAlParameters::Init

void PanditEtAlParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "PanditEtAlParameters - Calculate ..."<<endl;
#endif // if KADEBUG
  P[VT_F].value     = ElphyModelConstants::F*1000.;
  P[VT_RTdF].value  = ElphyModelConstants::R*(P[VT_Tx].value)/ElphyModelConstants::F;
  P[VT_RTd2F].value = P[VT_RTdF].value*.5;
  P[VT_FdRT].value  = 1.0/(P[VT_RTdF].value);
  P[VT_dC_m].value  = 0.001/(P[VT_C_m].value);
#if ChR2
  P[VT_ChRe_21dark].value = P[VT_ChRe_21dark].value*pow(1.95, (P[VT_ChRtemp].value-22.)/10.);
  P[VT_ChRe_12dark].value = P[VT_ChRe_12dark].value*pow(1.1, (P[VT_ChRtemp].value-22.)/10.);
  P[VT_ChRG_d2].value     = P[VT_ChRG_d2].value*pow(1.77, (P[VT_ChRtemp].value-22.)/10.);
  P[VT_ChRepsilon1].value = P[VT_ChRepsilon1].value*pow(1.46, (P[VT_ChRtemp].value-22.)/10.);
  P[VT_ChRepsilon2].value = P[VT_ChRepsilon2].value*pow(2.77, (P[VT_ChRtemp].value-22.)/10.);

  P[VT_ChRF].value   = 0.0006040939*P[VT_ChRIrr].value*P[VT_ChRlambda].value/P[VT_ChRw_loss].value;
  P[VT_ChRS_0].value = 0.5*(1.0+tanh(120.0*(100.0*P[VT_ChRIrr].value-0.1)));
  double logphi012 = 0.0;
  double logphi021 = 0.0;
  if (P[VT_ChRIrr].value > 0) {
    logphi012 = log10(1.0+P[VT_ChRIrr].value/P[VT_ChRc_122].value);
    logphi021 = log10(1.0+P[VT_ChRIrr].value/P[VT_ChRc_212].value);
  }
  P[VT_ChRe_12].value = P[VT_ChRe_12dark].value+P[VT_ChRc_121].value*logphi012;
  P[VT_ChRe_21].value = P[VT_ChRe_21dark].value+P[VT_ChRc_211].value*logphi021;

#endif // if ChR2
} // PanditEtAlParameters::Calculate

void PanditEtAlParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr << "PanditEtAlParameters - InitTable ...\n";
#endif // if KADEBUG
  tinc *= -1;  // Negation for exp(-dt/tau)

  et_Ca_inact = exp(tinc/0.009);

  for (double V = -RangeTabhalf+.0001; V < RangeTabhalf; V += dDivisionTab) {
    const ML_CalcType V_int = V;
    const int Vi            = (int)(DivisionTab*(RangeTabhalf+V)+.5);
    et_m[Vi] = exp(tinc / (0.00136/(0.32*(V_int+47.13)/(1.-exp(-0.1*(V_int+47.13)))+0.08*exp(-V_int/11.))));
    if (V_int >= -40) {
      et_h[Vi] = exp(tinc / (0.0004537*(1.0+exp((V_int+10.66)/ -11.1))));
      et_j[Vi] = exp(tinc / (0.01163*(1.0+exp(-0.1*(V_int+32)))/exp(-2.535e-7*V_int)));
    } else {
      et_h[Vi] = exp(tinc / (0.00349/(0.135*exp((V_int+80.)/ -6.8)+3.56*exp(0.079*V_int)+3.1e5*exp(0.35*V_int))));
      et_j[Vi] = exp(tinc / (0.00349/((V_int+37.78)/(1.+exp(0.311*(V_int+79.23)))
                                      *(-1.27140e5*exp(0.2444*V_int)-3.474e-5*exp(-0.04391*V_int))
                                      +0.1212*exp(-0.01052*V_int)/(1.+exp(-.1378*(V_int+40.14))))));
    }
    minf[Vi]   = 1./(1+exp((V_int+45)/ -6.5));
    hinf[Vi]   = 1./(1+exp((V_int+76.1)/6.07));
    jinf[Vi]   = hinf[Vi];
    dinf[Vi]   = 1./(1.+exp((V_int+15.3)/ -5.));
    f11inf[Vi] = 1./(1.+exp((V_int+26.7)/5.4));
    f12inf[Vi] = f11inf[Vi];
    et_d[Vi]   = exp(tinc / (0.00305*exp(-0.0045*(V_int+7.)*(V_int+7.))
                             +0.00105*exp(-0.002*(V_int-18.)*(V_int-18.))+0.00025));
    et_f11[Vi] = exp(tinc / (0.105*exp(-((V_int+45.)/12.0*(V_int+45.)/12))
                             +0.04/(1.+exp((-V_int+25.)/25.))
                             +0.015/(1.+exp((V_int+75.)/25.))+.0017));
    et_f12[Vi] = exp(tinc / (0.041*exp(-((V_int+47.)/12.0*(V_int+47.)/12))
                             +0.08/(1.+exp((V_int+55.)/ -5.))
                             +0.015/(1.+exp((V_int+75.)/25.))+.0017));
    rinf[Vi]      = 1./(1.+exp((V_int+P[VT_rc1].value)/ -P[VT_rc2].value));
    sinf[Vi]      = 1./(1.+exp((V_int+P[VT_sc1].value)/P[VT_sc2].value));
    s_slowinf[Vi] = sinf[Vi];
    et_r[Vi]      = exp(tinc * (45.16*exp(0.03577*(V_int+50))+98.9*exp(-0.1*(V_int+38.))));
    et_s[Vi]      =
      exp(tinc /
          (P[VT_t_sc1].value*
           exp(-(V_int+P[VT_t_sc2].value)/(P[VT_t_sc3].value)*(V_int+P[VT_t_sc2].value)/(P[VT_t_sc3].value))+
           P[VT_t_sc4].value));
    et_s_slow[Vi] =
      exp(tinc /
          (P[VT_t_ssc1].value*
           exp(-(V_int+P[VT_t_ssc2].value)/(P[VT_t_ssc3].value)*(V_int+P[VT_t_ssc2].value)/(P[VT_t_ssc3].value))+
           P[VT_t_ssc4].value));
    r_ssinf[Vi]     = 1./(1.+exp((V_int+P[VT_rssc1].value)/ -P[VT_rssc2].value));
    s_ssinf[Vi]     = 1./(1.+exp((V_int+87.5)/10.3));
    et_r_ss[Vi]     = exp(tinc / (P[VT_t_rssc1].value/(45.16*exp(0.03577*(V_int+50))+98.9*exp(-0.1*(V_int+38.)))));
    et_s_ss[Vi]     = exp(tinc / (2.1));
    r_K_slowinf[Vi] = 1./(1.+exp((V_int+12.5)/ -7.7));
    s_K_slowinf[Vi] = 1./(1.+exp((V_int+37.6)/5.9));
    et_r_K_slow[Vi] = exp(tinc / (1./(45.16*exp(0.03577*(V_int+50))+98.9*exp(-0.1*(V_int+38.)))));
    et_s_K_slow[Vi] = exp(tinc / (1.174));
    yinf[Vi]        = 1./(1.+exp((V_int+138.6)/10.48));
    et_y[Vi]        = exp(tinc * (0.11885*exp((V_int+80)/28.37)+0.5623*exp((V_int+80)/ -14.19)));
#if ChR2
    ChRG[Vi]    = (1.-exp(-V_int/40.9819))/(V_int/15.2245);
    ChRG_d1[Vi] = (0.075+0.043*tanh((V_int+20)/ -20))*pow(1.97, (P[VT_ChRtemp].value-22.)/10.);
    ChRG_r[Vi]  = 4.34587 *1e-5* exp(-0.0211539274*V_int)*pow(2.56, (P[VT_ChRtemp].value-22.)/10.);
#endif // if ChR2
  }
} // PanditEtAlParameters::InitTable
