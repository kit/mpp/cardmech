/*! \file SaucermanParameters.cpp

   \cell model of rat modeling adrenergic influence

   \version 0.0.0

   \date Created Carola Otto (12.03.2009)\n
       template (00.00.00)\n
       man Gunnar Seemann (27.02.03)\n
       Last Modified Eike Wuelfers (03.02.10)

   \author Carola Otto\n
         Institute of Biomedical Engineering\n
         Universitaet Karlsruhe (TH)\n
         http://www.ibt.uni-karlsruhe.de\n
         Copyright 2000-2009 - All rights reserved.

   // \sa  \ref Saucerman
 */

#include <SaucermanParameters.h>

SaucermanParameters::SaucermanParameters(const char *initFile) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile);
}

SaucermanParameters::~SaucermanParameters() {
  // Destruktor
}

void SaucermanParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "SaucermanParameters:" << endl;
  for (int i = vtFirst; i < vtLast; i++)
    cout << P[i].name << "\t = " << P[i].value << endl;
}

void SaucermanParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "#Loading the Saucerman parameter from " << initFile << " ...\n";
#endif // if KADEBUG


  // Initialization of the Parameters ...
  P[VT_R].name = "R";
  P[VT_Tx].name = "Tx";
  P[VT_F].name = "F";
  P[VT_m_init].name = "init_m";
  P[VT_h_init].name = "init_h";
  P[VT_j_init].name = "init_j";
  P[VT_xr1_init].name = "init_xr1";
  P[VT_xr2_init].name = "init_Xr2";
  P[VT_xs_init].name = "init_xs";
  P[VT_r_init].name = "init_r";
  P[VT_s_init].name = "init_s";
  P[VT_d_init].name = "init_d";
  P[VT_f_init].name = "init_f";
  P[VT_fCa_init].name = "init_fCa";
  P[VT_g_init].name = "init_g";
  P[VT_K_o].name = "K_o";
  P[VT_Ca_o].name = "Ca_o";
  P[VT_Na_o].name = "Na_o";
  P[VT_Vc].name = "Vc";
  P[VT_Vsr].name = "Vsr";
  P[VT_Bufc].name = "Bufc";
  P[VT_Kbufc].name = "Kbufc";
  P[VT_Bufsr].name = "Bufsr";
  P[VT_Kbufsr].name = "Kbufsr";
  P[VT_taufca].name = "taufca";
  P[VT_taug].name = "taug";
  P[VT_Vmaxup].name = "Vmaxup";
  P[VT_Kup].name = "Kup";
  P[VT_C].name = "C";
  P[VT_g_Kr].name = "g_Kr";
  P[VT_pKNa].name = "pKNa";
  P[VT_g_Ks].name = "g_Ks";
  P[VT_g_K1].name = "g_K1";
  P[VT_g_to].name = "g_to";
  P[VT_g_Na].name = "g_Na";
  P[VT_g_bNa].name = "g_bNa";
  P[VT_KmK].name = "KmK";
  P[VT_KmNa].name = "KmNa";
  P[VT_knak].name = "knak";
  P[VT_g_CaL].name = "g_CaL";
  P[VT_g_bCa].name = "g_bCa";
  P[VT_kNaCa].name = "kNaCa";
  P[VT_KmNai].name = "KmNai";
  P[VT_KmCa].name = "KmCa";
  P[VT_Km_Ca].name = "Km_Ca";
  P[VT_ksat].name = "ksat";
  P[VT_n].name = "n";
  P[VT_g_pCa].name = "g_pCa";
  P[VT_KpCa].name = "KpCa";
  P[VT_g_pK].name = "g_pK";
  P[VT_V_init].name = "init_V_m";
  P[VT_Cai_init].name = "init_Ca_i";
  P[VT_CaSR_init].name = "init_Ca_SR";
  P[VT_Nai_init].name = "init_Na_i";
  P[VT_Ki_init].name = "init_K_i";
  P[VT_s_inf_vHalf].name = "s_inf_vHalf";
  P[VT_tau_s_f1].name = "tau_s_f1";
  P[VT_tau_s_slope1].name = "tau_s_slope1";
  P[VT_tau_s_vHalf1].name = "tau_s_vHalf1";
  P[VT_tau_s_f2].name = "tau_s_f2";
  P[VT_tau_s_f3].name = "tau_s_f3";
  P[VT_m_Xr1_1].name = "m_Xr1_1";
  P[VT_m_Xr1_2].name = "m_Xr1_2";
  P[VT_a_Xr1_1].name = "a_Xr1_1";
  P[VT_a_Xr1_2].name = "a_Xr1_2";
  P[VT_b_Xr1_1].name = "b_Xr1_1";
  P[VT_b_Xr1_2].name = "b_Xr1_2";
  P[VT_K_Q10Xr1].name = "K_Q10Xr1";
  P[VT_m_Xr2_1].name = "m_Xr2_1";
  P[VT_m_Xr2_2].name = "m_Xr2_2";
  P[VT_a_Xr2_1].name = "a_Xr2_1";
  P[VT_a_Xr2_2].name = "a_Xr2_2";
  P[VT_b_Xr2_1].name = "b_Xr2_1";
  P[VT_b_Xr2_2].name = "b_Xr2_2";
  P[VT_K_Q10Xr2].name = "K_Q10Xr2";
  P[VT_inverseVcF2].name = "inverseVcF2";
  P[VT_inverseVcF2C].name = "inverseVcF2C";
  P[VT_inverseVcFC].name = "inverseVcFC";
  P[VT_VcdVsr].name = "VcdVsr";
  P[VT_Kupsquare].name = "Kupsquare";
  P[VT_BufcPKbufc].name = "BufcPKbufc";
  P[VT_Kbufcsquare].name = "Kbufcsquare";
  P[VT_Kbufc2].name = "Kbufc2";
  P[VT_BufsrPKbufsr].name = "BufsrPKbufsr";
  P[VT_Kbufsrsquare].name = "Kbufsrsquare";
  P[VT_Kbufsr2].name = "Kbufsr2";
  P[VT_KopKNaNao].name = "KopKNaNao";
  P[VT_KmNa3].name = "KmNa3";
  P[VT_Nao3].name = "Nao3";
  P[VT_inverseRTONF].name = "inverseRTONF";
  P[VT_RTONF].name = "RTONF";
  P[VT_I_Ks_tot].name = "I_Ks_tot";
  P[VT_I_Ksp_init].name = "init_I_Ksp";
  P[VT_Nifi].name = "Nifi";
  P[VT_IC50_nif].name = "IC50_nif";
  P[VT_g_CaKL].name = "g_CaKL";
  P[VT_g_CaNaL].name = "g_CaNaL";
  P[VT_p_ns].name = "p_ns";
  P[VT_Kmns].name = "Kmns";
  P[VT_CSQNbar].name = "CSQNbar";
  P[VT_KmCsqn].name = "KmCsqn";
  P[VT_KmUp].name = "KmUp";
  P[VT_I_upbar].name = "I_upbar";
  P[VT_Ca_jsr_init].name = "init_Ca_jsr";
  P[VT_Ca_nsr_init].name = "init_Ca_nsr";
  P[VT_nsrbar].name = "nsrbar";
  P[VT_tau_tr].name = "tau_tr";
  P[VT_Vjsr].name = "Vjsr";
  P[VT_Vmyo].name = "Vmyo";
  P[VT_Vnsr].name = "Vnsr";

  P[VT_L_init].name = "init_L";
  P[VT_R_init].name = "init_R";
  P[VT_Gs_init].name = "init_Gs";
  P[VT_b1ARd_init].name = "init_b1ARd";
  P[VT_b1ARtot_init].name = "init_b1ARtot";
  P[VT_b1ARp_init].name = "init_b1ARp";
  P[VT_Gsagtptot_init].name = "init_Gsagtptot";
  P[VT_Gsagdp_init].name = "init_Gsagdp";
  P[VT_Gsbg_init].name = "init_Gsbg";
  P[VT_Gsa_gtp_init].name = "init_Gsa_gtp";
  P[VT_AC_init].name = "init_AC";
  P[VT_Fsk_init].name = "init_Fsk";
  P[VT_cAMP_init].name = "init_cAMP";
  P[VT_PDE_init].name = "init_PDE";
  P[VT_IBMX_init].name = "init_IBMX";
  P[VT_PKACI_init].name = "init_PKACI";
  P[VT_PKACII_init].name = "init_PKACII";
  P[VT_cAMPtot_init].name = "init_cAMPtot";
  P[VT_PLBs_init].name = "init_PLBs";
  P[VT_PP1_init].name = "init_PP1";
  P[VT_Inhib1ptot_init].name = "init_Inhib1ptot";
  P[VT_Inhib1p_init].name = "init_Inhib1p";
  P[VT_LCCap_init].name = "init_LCCap";
  P[VT_LCCbp_init].name = "init_LCCbp";
  P[VT_RyRp_init].name = "init_RyRp";
  P[VT_TnIp_init].name = "init_TnIp";
  P[VT_Iks_init].name = "init_Iks";
  P[VT_Yotiao_init].name = "init_Yotiao";
  P[VT_Iksp_init].name = "init_Iksp";
  P[VT_trel_init].name = "init_trel";
  P[VT_Kl].name = "Kl";
  P[VT_Kr].name = "Kr";
  P[VT_Kc].name = "Kc";
  P[VT_k_barkp].name = "k_barkp";
  P[VT_k_barkm].name = "k_barkm";
  P[VT_k_pkap].name = "k_pkap";
  P[VT_k_pkam].name = "k_pkam";
  P[VT_k_gact].name = "k_gact";
  P[VT_k_hyd].name = "k_hyd";
  P[VT_k_reassoc].name = "k_reassoc";
  P[VT_Ltotmax].name = "Ltotmax";
  P[VT_Gstot].name = "Gstot";
  P[VT_Kgsa].name = "Kgsa";
  P[VT_Kfsk].name = "Kfsk";
  P[VT_k_ac_basal].name = "k_ac_basal";
  P[VT_ATP].name = "ATP";
  P[VT_Km_basal].name = "Km_basal";
  P[VT_k_ac_gsa].name = "k_ac_gsa";
  P[VT_Km_gsa].name = "Km_gsa";
  P[VT_k_ac_fsk].name = "k_ac_fsk";
  P[VT_Km_fsk].name = "Km_fsk";
  P[VT_k_pde3].name = "k_pde3";
  P[VT_Km_pde3].name = "Km_pde3";
  P[VT_k_pde4].name = "k_pde4";
  P[VT_Km_pde4].name = "Km_pde4";
  P[VT_Ki_ibmx].name = "Ki_ibmx";
  P[VT_Fsktot].name = "Fsktot";
  P[VT_AC_tot].name = "AC_tot";
  P[VT_PDE4tot].name = "PDE4tot";
  P[VT_IBMXtot].name = "IBMXtot";
  P[VT_PKItot].name = "PKItot";
  P[VT_Ki_pki].name = "Ki_pki";
  P[VT_Kd].name = "Kd";
  P[VT_Ka].name = "Ka";
  P[VT_Kb].name = "Kb";
  P[VT_PKAItot].name = "PKAItot";
  P[VT_PKAIItot].name = "PKAIItot";
  P[VT_PLBtot].name = "PLBtot";
  P[VT_k_pka_plb].name = "k_pka_plb";
  P[VT_Km_pka_plb].name = "Km_pka_plb";
  P[VT_k_pp1_plb].name = "k_pp1_plb";
  P[VT_Km_pp1_plb].name = "Km_pp1_plb";
  P[VT_Inhib1tot].name = "Inhib1tot";
  P[VT_Ki_inhib1].name = "Ki_inhib1";
  P[VT_k_pka_i1].name = "k_pka_i1";
  P[VT_Km_pka_i1].name = "Km_pka_i1";
  P[VT_Vmax_pp2a_i1].name = "Vmax_pp2a_i1";
  P[VT_Km_pp2a_i1].name = "Km_pp2a_i1";
  P[VT_PP1tot].name = "PP1tot";
  P[VT_PKAIIlcctot].name = "PKAIIlcctot";
  P[VT_LCCtot].name = "LCCtot";
  P[VT_epsilon].name = "epsilon";
  P[VT_k_pka_lcc].name = "k_pka_lcc";
  P[VT_Km_pka_lcc].name = "Km_pka_lcc";
  P[VT_k_pp2a_lcc].name = "k_pp2a_lcc";
  P[VT_PP2Alcctot].name = "PP2Alcctot";
  P[VT_Km_pp2a_lcc].name = "Km_pp2a_lcc";
  P[VT_k_pp1_lcc].name = "k_pp1_lcc";
  P[VT_PP1lcctot].name = "PP1lcctot";
  P[VT_Km_pp1_lcc].name = "Km_pp1_lcc";
  P[VT_PKAIIryrtot].name = "PKAIIryrtot";
  P[VT_RyRtot].name = "RyRtot";
  P[VT_kcat_pka_ryr].name = "kcat_pka_ryr";
  P[VT_Km_pka_ryr].name = "Km_pka_ryr";
  P[VT_kcat_pp1_ryr].name = "kcat_pp1_ryr";
  P[VT_PP1ryr].name = "PP1ryr";
  P[VT_Km_pp1_ryr].name = "Km_pp1_ryr";
  P[VT_kcat_pp2a_ryr].name = "kcat_pp2a_ryr";
  P[VT_PP2Aryr].name = "PP2Aryr";
  P[VT_Km_pp2a_ryr].name = "Km_pp2a_ryr";
  P[VT_TnItot].name = "TnItot";
  P[VT_kcat_pka_tni].name = "kcat_pka_tni";
  P[VT_Km_pka_tni].name = "Km_pka_tni";
  P[VT_kcat_pp2a_tni].name = "kcat_pp2a_tni";
  P[VT_PP2Atni].name = "PP2Atni";
  P[VT_Km_pp2a_tni].name = "Km_pp2a_tni";
  P[VT_K_yotiao].name = "K_yotiao";
  P[VT_Iks_tot].name = "Iks_tot";
  P[VT_Yotiao_tot].name = "Yotiao_tot";
  P[VT_PKAII_ikstot].name = "PKAII_ikstot";
  P[VT_PP1_ikstot].name = "PP1_ikstot";
  P[VT_k_pka_iks].name = "k_pka_iks";
  P[VT_Km_pka_iks].name = "Km_pka_iks";
  P[VT_k_pp1_iks].name = "k_pp1_iks";
  P[VT_Km_pp1_iks].name = "Km_pp1_iks";
  P[VT_gmaxrel].name = "gmaxrel";
  P[VT_Km_csqn].name = "Km_csqn";
  P[VT_CSQNth].name = "CSQNth";
  P[VT_gmaxrelol].name = "gmaxrelol";
  P[VT_Km_trpn].name = "Km_trpn";
  P[VT_TRPNbar].name = "TRPNbar";
  P[VT_CMDNbar].name = "CMDNbar";
  P[VT_Km_cmdn].name = "Km_cmdn";
  P[VT_INDObar].name = "INDObar";
  P[VT_Km_indo].name = "Km_indo";
  P[VT_ACap].name = "ACap";
  P[VT_ACap].name = "ACap";
  P[VT_t_init].name = "init_t";
  P[VT_jsrol_time_init].name = "init_jsrol_time";
  P[VT_Pns].name = "Pns";
  P[VT_xs05_init].name = "init_xs05";
  P[VT_Vm_init].name = "init_Vm";
  P[VT_PDE3tot].name = "PDE3tot";
  P[VT_Amp].name = "Amp";


  P[VT_RTONF].readFromFile = false;
  P[VT_inverseRTONF].readFromFile = false;
  P[VT_inverseVcF2C].readFromFile = false;
  P[VT_inverseVcF2].readFromFile = false;

  // inverseVcF2 is a unused value! (dw)
  P[VT_inverseVcFC].readFromFile = false;
  P[VT_VcdVsr].readFromFile = false;
  P[VT_Kupsquare].readFromFile = false;
  P[VT_BufcPKbufc].readFromFile = false;
  P[VT_Kbufcsquare].readFromFile = false;
  P[VT_Kbufc2].readFromFile = false;
  P[VT_BufsrPKbufsr].readFromFile = false;
  P[VT_Kbufsrsquare].readFromFile = false;
  P[VT_Kbufsr2].readFromFile = false;
  P[VT_KopKNaNao].readFromFile = false;
  P[VT_KmNa3].readFromFile = false;
  P[VT_Nao3].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_Saucerman);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile) {
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);
    }
      // End Initialization of the Parameters ...
    else {
#if KADEBUG
#endif
    }
  Calculate();
  InitTable();
#if KADEBUG
  cerr << "#Init() done ...\n";
#endif // if KADEBUG
} // SaucermanParameters::Init

void SaucermanParameters::Calculate() {
#if KADEBUG
  cerr << "#SaucermanParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_RTONF].value = P[VT_R].value * (P[VT_Tx].value) / (P[VT_F].value);
  P[VT_inverseRTONF].value = 1 / (P[VT_RTONF].value);
  P[VT_inverseVcF2C].value = (1 / (2 * (P[VT_Vc].value) * (P[VT_F].value))) * (P[VT_C].value);
  P[VT_inverseVcFC].value = (1. / ((P[VT_Vc].value) * (P[VT_F].value))) * (P[VT_C].value);
  P[VT_VcdVsr].value = P[VT_Vc].value / (P[VT_Vsr].value);
  P[VT_Kupsquare].value = P[VT_Kup].value * (P[VT_Kup].value);
  P[VT_BufcPKbufc].value = P[VT_Bufc].value + (P[VT_Kbufc].value);
  P[VT_Kbufcsquare].value = P[VT_Kbufc].value * (P[VT_Kbufc].value);
  P[VT_Kbufc2].value = 2 * (P[VT_Kbufc].value);
  P[VT_BufsrPKbufsr].value = P[VT_Bufsr].value + (P[VT_Kbufsr].value);
  P[VT_Kbufsrsquare].value = P[VT_Kbufsr].value * (P[VT_Kbufsr].value);
  P[VT_Kbufsr2].value = 2 * (P[VT_Kbufsr].value);
  P[VT_KopKNaNao].value = P[VT_K_o].value + (P[VT_pKNa].value) * (P[VT_Na_o].value);
  P[VT_KmNa3].value = P[VT_KmNa].value * (P[VT_KmNa].value) * (P[VT_KmNa].value);
  P[VT_Nao3].value = P[VT_Na_o].value * (P[VT_Na_o].value) * (P[VT_Na_o].value);
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
}

void SaucermanParameters::InitTable() {
#if KADEBUG
  cerr << "#SaucermanParameters - InitTable ..." << endl;
#endif // if KADEBUG
  for (double V = -RangeTabhalf + .0001; V < RangeTabhalf; V += dDivisionTab) {
    // V in mV
    int Vi = (int) (DivisionTab * (RangeTabhalf + V) + .5);

    const ML_CalcType rec_iNaK =
        (1. /
         (1. + 0.1245 * exp(-0.1 * V * (P[VT_inverseRTONF].value)) +
          0.0365 * ((exp(P[VT_Na_o].value / 67.3) - 1) / 7) *
          exp(-V * (P[VT_inverseRTONF].value))));

    NaK_P1[Vi] =
        P[VT_knak].value * (P[VT_K_o].value / (P[VT_K_o].value + (P[VT_KmK].value))) * rec_iNaK;

    rec_ipK[Vi] = 1. / (1. + exp((7.488 - V) / 5.98));


    // Gates m, h, j for fast Natrium current
    const ML_CalcType A_m = 0.32 * (V + 47.13) / (1 - exp(-0.1 * (V + 47.13)));
    const ML_CalcType B_m = 0.08 * exp(-V / 11.0);
    a_m[Vi] = A_m;
    b_m[Vi] = B_m;

    if (V >= -40.) {
      const ML_CalcType AH_1 = 0.0;
      const ML_CalcType BH_1 = 1 / (0.13 * (1 + exp(-(V + 10.66) / 11.1)));
      a_h[Vi] = AH_1;
      b_h[Vi] = BH_1;
    } else {
      const ML_CalcType AH_2 = (0.135 * exp((80.0 + V) / -6.8));
      const ML_CalcType BH_2 = (3.56 * exp(0.079 * V) + 3.1e5 * exp(0.35 * V));
      a_h[Vi] = AH_2;
      b_h[Vi] = BH_2;
    }


    if (V >= -40.) {
      const ML_CalcType AJ_1 = 0.;
      const ML_CalcType BJ_1 = (0.3 * exp(-2.535e-7 * V) / (1. + exp(-0.1 * (V + 32.))));
      a_j[Vi] = AJ_1;
      b_j[Vi] = BJ_1;
    } else {
      const ML_CalcType AJ_2 =
          ((-1.2714e5 * exp(0.2444 * V) - (3.474e-5 * exp(-0.04391 * V))) * (V + 37.78) /
           (1 + exp(0.311 * (V + 79.23))));
      const ML_CalcType BJ_2 = (0.1212 * exp(-0.01052 * V) / (1. + exp(-0.1378 * (V + 40.14))));
      a_j[Vi] = AJ_2;
      b_j[Vi] = BJ_2;
    }


    // gate for I_Kr (only one as rkr)
    Rkr[Vi] = 1. / (1 + exp((V + 33) / 22.4));
    Xr1_inf[Vi] = 1. / (1.0 + exp(-(V + 50) / 7.5));
    tau_Xr1[Vi] = 1. / (1.38e-3 * (V + 7) / (1 - exp(-0.123 * (V + 7))) +
                        6.1e-4 * (V + 10) / (exp(0.145 * (V + 10)) - 1));

    // gate for I_Ks
    tau_Xs[Vi] = 1. / (7.19e-5 * (V + 30) / (1 - exp(-0.148 * (V + 30))) +
                       1.31e-4 * (V + 30) / (exp(0.0687 * (V + 30)) - 1));
    s_help[Vi] = V;

    // gates for I_to
    const ML_CalcType axto = 0.04561 * exp(0.03577 * V);
    const ML_CalcType bxto = 0.0989 * exp(-0.06237 * V);
    const ML_CalcType ayto =
        0.005415 * exp(-(V + 33.5) / 5) / (1 + 0.051335 * exp(-(V + 33.5) / 5));
    const ML_CalcType byto = 0.005415 * exp((V + 33.5) / 5) / (1 + 0.05133 * exp((V + 33.5) / 5));

    a_r[Vi] = axto;
    b_r[Vi] = bxto;

    a_s[Vi] = ayto;
    b_s[Vi] = byto;

    // gates for L type calcium channel
    d_inf[Vi] = 1. / (1. + exp(-(0.01 + V) / 6.24));
    const ML_CalcType d_P1 = (1 - exp(-(V + 0.01) / 6.24)) / (0.035 * (V + 0.01));
    tau_d[Vi] = d_inf[Vi] * d_P1;

    f_inf[Vi] = 1. / (1. + exp((V + 35.06) / 8.6)) + 0.6 / (1 + exp((50 - V) / 20));
    tau_f[Vi] = 1 / (0.0197 * exp(-((0.0337 * (V + 0.01)) * (0.0337 * (V + 0.01)))) + 0.02);

    // I_NaCa: for na ca exchange current
    const ML_CalcType NaCaP1 =
        (P[VT_kNaCa].value) * (1. / ((P[VT_KmNa3].value) + (P[VT_Nao3].value))) *
        (1. / ((P[VT_Km_Ca].value) + (P[VT_Ca_o].value)));
    const ML_CalcType NaCaP2 = 1. / (1. + (P[VT_ksat].value) * exp(((P[VT_n].value) - 1.) * V *
                                                                   (P[VT_inverseRTONF].value)));
    const ML_CalcType NaCaP3 =
        exp((P[VT_n].value) * V * (P[VT_inverseRTONF].value)) * (P[VT_Ca_o].value);  // s4/Na_i^3
    const ML_CalcType NaCaP4 = exp(((P[VT_n].value) - 1) * V * (P[VT_inverseRTONF].value)) *
                               (P[VT_Nao3].value);  // s5/Ca_i
    NaCa_P1[Vi] = NaCaP1 * NaCaP2;
    NaCa_P2[Vi] = NaCaP3;  // s4/Na_i^3
    NaCa_P3[Vi] = NaCaP4;  // s5/Ca_i

    // ibarca
    const ML_CalcType CaLP1 = 2 * V * (P[VT_inverseRTONF].value);
    const ML_CalcType CaLP2 = 2 * (P[VT_g_CaL].value) * CaLP1 * (P[VT_F].value) / (exp(CaLP1) - 1.);
    CaL_P1[Vi] = CaLP2 * exp(CaLP1);
    CaL_P2[Vi] = -CaLP2 * 0.341 * (P[VT_Ca_o].value);

    // ibark
    const ML_CalcType CaKLP1 = V * (P[VT_inverseRTONF].value);
    const ML_CalcType CaKLP2 = (P[VT_g_CaKL].value) * CaKLP1 * (P[VT_F].value) / (exp(CaKLP1) - 1.);
    CaKL_P1[Vi] = CaKLP2 * exp(CaKLP1) * 0.75;
    CaKL_P2[Vi] = CaKLP2 * -0.75 * (P[VT_K_o].value);

    // ibarna
    const ML_CalcType CaNaLP1 = V * (P[VT_inverseRTONF].value);
    const ML_CalcType CaNaLP2 =
        (P[VT_g_CaNaL].value) * CaNaLP1 * (P[VT_F].value) / (exp(CaNaLP1) - 1.);
    CaNaL_P1[Vi] = CaNaLP2 * exp(CaNaLP1) * 0.75;
    CaNaL_P2[Vi] = CaNaLP2 * -0.75 * (P[VT_Na_o].value);

    // ibarnsk
    const ML_CalcType nsP1 = V * (P[VT_inverseRTONF].value);
    const ML_CalcType nsP2 = V * (P[VT_inverseRTONF].value) * (P[VT_F].value);
    nsK_P1[Vi] = nsP1;
    nsK_P2[Vi] = nsP2;

    // ibarnsna
    nsNa_P1[Vi] = nsP1;
    nsNa_P2[Vi] = nsP2;
  }
} // SaucermanParameters::InitTable
