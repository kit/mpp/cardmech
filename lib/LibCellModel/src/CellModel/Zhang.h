/*! \file Zhang.h
   \brief Implementation of Zhang et al sinus node model
   AJP 2000, JCE 2002 (implementation partial), JCE 2003

   \author unknown, Universitaet Karlsruhe (TH)
 */
#ifndef ZHANG
#define ZHANG

#undef HETERO
#undef CELLMODEL_PARAMVALUE
#

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_ZhangParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pZP->P[NS_ZhangParameters::a].value
#endif // ifdef HETERO

#include <ZhangParameters.h>

class Zhang : public vbElphyModel<ML_CalcType> {
public:
  ZhangParameters *pZP;
  double h1, h2, m, dL, fL, dT, fT, q, r, paf, pas, pi_m;
  double xs, y, Cai;

#ifdef ZHANG03
  double j, k;
#endif // ifdef ZHANG03

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  Zhang(ZhangParameters *pp);

  ~Zhang();

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vcell); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp);  /*0.0;*/ }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_init_Vm); }

  virtual inline ML_CalcType GetCai() { return Cai; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Cao); }

  virtual inline ML_CalcType GetNai() { return CELLMODEL_PARAMVALUE(VT_Nai); }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Nao); }

  virtual inline ML_CalcType GetKi() { return CELLMODEL_PARAMVALUE(VT_Ki); }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_Ko); }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &h1; }

  virtual inline void SetCai(ML_CalcType val) { Cai = val; }

  virtual void Init();

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);

  virtual ML_CalcType
  Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
       int euler = 1);
}; // class Zhang
#endif // ifndef ZHANG
