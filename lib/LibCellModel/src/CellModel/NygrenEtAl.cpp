/**@file NygrenEtAl.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <NygrenEtAl.h>

NygrenEtAl::NygrenEtAl(NygrenEtAlParameters *pp) {
  pNyeaP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pNyeaP, NS_NygrenEtAlParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

#ifdef HETERO

inline bool NygrenEtAl::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool NygrenEtAl::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

void NygrenEtAl::Init() {
#if KADEBUG
  cerr << "initializing Class: NygrenEtAlBasis ... " << endl;
#endif // if KADEBUG
  h1 = CELLMODEL_PARAMVALUE(VT_init_h1);
  h2 = CELLMODEL_PARAMVALUE(VT_init_h2);
  fl2 = CELLMODEL_PARAMVALUE(VT_init_fl2);
  s = CELLMODEL_PARAMVALUE(VT_init_s);
  ssus = CELLMODEL_PARAMVALUE(VT_init_ssus);
#ifdef NYEA_TASK
  C1_TASK = CELLMODEL_PARAMVALUE(VT_init_C1_TASK);
  C2_TASK = CELLMODEL_PARAMVALUE(VT_init_C2_TASK);
  O_TASK = CELLMODEL_PARAMVALUE(VT_init_O_TASK);
#endif // ifdef NYEA_TASK
  n = CELLMODEL_PARAMVALUE(VT_init_n);
  pa = CELLMODEL_PARAMVALUE(VT_init_pa);
  Ca_i = CELLMODEL_PARAMVALUE(VT_init_Ca_i);
  Ca_d = CELLMODEL_PARAMVALUE(VT_init_Ca_d);
  O_c = CELLMODEL_PARAMVALUE(VT_init_O_c);
  O_tc = CELLMODEL_PARAMVALUE(VT_init_O_tc);
  O_tmgc = CELLMODEL_PARAMVALUE(VT_init_O_tmgc);
  O_calse = CELLMODEL_PARAMVALUE(VT_init_O_calse);
  Ca_up = CELLMODEL_PARAMVALUE(VT_init_Ca_up);
  Ca_rel = CELLMODEL_PARAMVALUE(VT_init_Ca_rel);
  Fl1 = CELLMODEL_PARAMVALUE(VT_init_Fl1);
  Fl2 = CELLMODEL_PARAMVALUE(VT_init_Fl2);
  m = CELLMODEL_PARAMVALUE(VT_init_m);
  dl = CELLMODEL_PARAMVALUE(VT_init_dl);
  fl1 = CELLMODEL_PARAMVALUE(VT_init_fl1);
  r = CELLMODEL_PARAMVALUE(VT_init_r);
  rsus = CELLMODEL_PARAMVALUE(VT_init_rsus);
  Na_i = CELLMODEL_PARAMVALUE(VT_init_Na_i);
  Na_c = CELLMODEL_PARAMVALUE(VT_init_Na_c);
  K_i = CELLMODEL_PARAMVALUE(VT_init_K_i);
  K_c = CELLMODEL_PARAMVALUE(VT_init_K_c);
  Ca_c = CELLMODEL_PARAMVALUE(VT_init_Ca_c);
  O_tmgmg = CELLMODEL_PARAMVALUE(VT_init_O_tmgmg);
} // NygrenEtAl::Init

ML_CalcType
NygrenEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = 0.0, ML_CalcType stretch = 1.,
                 int euler = 1) {
  tinc *= 1000.0;
  const double V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);

  const double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(Na_c / Na_i));
  const double VmE_K = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_c / K_i));
  const double VmE_Ca = V_int - ((CELLMODEL_PARAMVALUE(VT_RTd2F)) * log(Ca_c / Ca_i));

  h1 = pNyeaP->h_[Vi] - (pNyeaP->h_[Vi] - h1) * pNyeaP->exptau_h1[Vi];
  h2 = pNyeaP->h_[Vi] - (pNyeaP->h_[Vi] - h2) * pNyeaP->exptau_h2[Vi];
  fl2 = pNyeaP->fl_[Vi] - (pNyeaP->fl_[Vi] - fl2) * pNyeaP->exptau_fl2[Vi];
  s = pNyeaP->s_[Vi] - (pNyeaP->s_[Vi] - s) * pNyeaP->exptau_s[Vi];
  ssus = pNyeaP->ssus_[Vi] - (pNyeaP->ssus_[Vi] - ssus) * pNyeaP->exptau_ssus[Vi];
  n = pNyeaP->n_[Vi] - (pNyeaP->n_[Vi] - n) * pNyeaP->exptau_n[Vi];
  pa = pNyeaP->pa_[Vi] - (pNyeaP->pa_[Vi] - pa) * pNyeaP->exptau_pa[Vi];
  m = pNyeaP->m_[Vi] - (pNyeaP->m_[Vi] - m) * pNyeaP->exptau_m[Vi];
  dl = pNyeaP->dl_[Vi] - (pNyeaP->dl_[Vi] - dl) * pNyeaP->exptau_dl[Vi];
  fl1 = pNyeaP->fl_[Vi] - (pNyeaP->fl_[Vi] - fl1) * pNyeaP->exptau_fl1[Vi];
  r = pNyeaP->r_[Vi] - (pNyeaP->r_[Vi] - r) * pNyeaP->exptau_r[Vi];
  rsus = pNyeaP->rsus_[Vi] - (pNyeaP->rsus_[Vi] - rsus) * pNyeaP->exptau_rsus[Vi];

#ifdef NYEA_TASK
  const double aCC = CELLMODEL_PARAMVALUE(VT_aCC_TASK) * exp(CELLMODEL_PARAMVALUE(VT_zaCC_TASK) * V_int * CELLMODEL_PARAMVALUE(VT_FdRT));
  const double bCC = CELLMODEL_PARAMVALUE(VT_bCC_TASK) * exp(-CELLMODEL_PARAMVALUE(VT_zbCC_TASK) * V_int * CELLMODEL_PARAMVALUE(VT_FdRT));
  const double aCO = CELLMODEL_PARAMVALUE(VT_aCO_TASK) * exp(CELLMODEL_PARAMVALUE(VT_zCO_TASK) * V_int * CELLMODEL_PARAMVALUE(VT_FdRT));
  const double aOC = CELLMODEL_PARAMVALUE(VT_aOC_TASK) * exp(-CELLMODEL_PARAMVALUE(VT_zOC_TASK) * V_int * CELLMODEL_PARAMVALUE(VT_FdRT));

  C1_TASK += 0.001 * tinc * (-aCC * C1_TASK + bCC * C2_TASK);
  C2_TASK += 0.001 * tinc * (-(bCC + aCO) * C2_TASK + aCC * C1_TASK + aOC * O_TASK);
  O_TASK += 0.001 * tinc * (-aOC * O_TASK + aCO * C2_TASK);
  const double VFdRT = V_int * CELLMODEL_PARAMVALUE(VT_FdRT);
  const double dexpVFdRT = exp(-VFdRT);
  double I_TASK = CELLMODEL_PARAMVALUE(VT_init_O_TASK) * CELLMODEL_PARAMVALUE(VT_P_TASK) * VFdRT * CELLMODEL_PARAMVALUE(VT_F) * (K_i - K_c * dexpVFdRT) /
                  (1. - dexpVFdRT) * K_c /
                  (CELLMODEL_PARAMVALUE(VT_Koh_TASK) + K_c);
#endif // ifdef NYEA_TASK

  const double I_Na = pNyeaP->C_Na[Vi] * m * m * m * (0.9 * h1 + 0.1 * h2) * Na_c *
                      (exp(VmE_Na * (CELLMODEL_PARAMVALUE(VT_FdRT))) - 1.0);

  const double f_Ca = Ca_d / (Ca_d + (CELLMODEL_PARAMVALUE(VT_K_Ca)));
  const double I_CaL =
      (CELLMODEL_PARAMVALUE(VT_g_CaL)) * dl * (f_Ca * fl1 + (1.0 - f_Ca) * fl2) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_Caapp)));

  const double I_K = (((CELLMODEL_PARAMVALUE(VT_g_K1)) * (pow((double) K_c, 0.4457))) /
                      (1.0 + exp(1.5 * (VmE_K + 3.6) * (CELLMODEL_PARAMVALUE(VT_FdRT))))
                      + (CELLMODEL_PARAMVALUE(VT_g_t)) * r * s
                      + (CELLMODEL_PARAMVALUE(VT_g_sus)) * rsus * ssus
                      + (CELLMODEL_PARAMVALUE(VT_g_Ks)) * n + (CELLMODEL_PARAMVALUE(VT_g_Kr)) * pa * pNyeaP->pi[Vi]) *
                     VmE_K; // I_K1+I_Kr+I_Ks+I_sus+I_TASK+I_t

  const double I_BNa = (CELLMODEL_PARAMVALUE(VT_g_BNa)) * VmE_Na;
  const double I_BCa = (CELLMODEL_PARAMVALUE(VT_g_BCa)) * VmE_Ca;

  const double KNAK = Na_i * sqrt(Na_i); // ==pow((double)Na_i, 1.5));
  const double I_NaK =
      pNyeaP->C_NaK[Vi] * (K_c / (K_c + (CELLMODEL_PARAMVALUE(VT_K_NaKK))) * KNAK / (KNAK + (CELLMODEL_PARAMVALUE(VT_K_NaKNapow))));

  const double I_CaP = (CELLMODEL_PARAMVALUE(VT_I_CaP_)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_K_CaP)));

  const double NNNICC = (Na_i * Na_i * Na_i * Ca_c);
  const double NNNCCI = (Na_c * Na_c * Na_c * Ca_i);
  const double I_NaCa = pNyeaP->C_NaCa[Vi] * (NNNICC - NNNCCI * pNyeaP->dexpV[Vi]) /
                        (1.0 + (CELLMODEL_PARAMVALUE(VT_d_NaCa)) * (NNNCCI + NNNICC));

  const double dOcdt = 200000. * Ca_i * (1.0 - O_c) - 476. * O_c;
  O_c += dOcdt * tinc * 1e-3;
  const double dOtcdt = 78400. * Ca_i * (1.0 - O_tc) - 392. * O_tc;
  O_tc += dOtcdt * tinc * 1e-3;
  const double dOtmgcdt = 200000. * Ca_i * (1.0 - O_tmgc - O_tmgmg) - 6.6 * O_tmgc;
  O_tmgc += dOtmgcdt * tinc * 1e-3;
  O_tmgmg += (2000. * (CELLMODEL_PARAMVALUE(VT_Mg_i)) * (1.0 - O_tmgc - O_tmgmg) - 666. * O_tmgmg) * tinc * 1e-3;
  const double dOdt = .08 * dOtcdt + .16 * dOtmgcdt + .045 * dOcdt;

  double I_up = CELLMODEL_PARAMVALUE(VT_I_up_) * (Ca_i * CELLMODEL_PARAMVALUE(VT_dKcyca) - CELLMODEL_PARAMVALUE(VT_Kxcsh2dKsrca) * Ca_up) /
                ((Ca_i + (CELLMODEL_PARAMVALUE(VT_K_cyca))) * (CELLMODEL_PARAMVALUE(VT_dKcyca)) +
                 (CELLMODEL_PARAMVALUE(VT_KxcsdKsrca)) * (Ca_up + (CELLMODEL_PARAMVALUE(VT_K_srca))));

  double I_tr = (Ca_up - Ca_rel) * (CELLMODEL_PARAMVALUE(VT_FVr2dt));

  double I_rel = Fl2 / (Fl2 + .25);
  I_rel *= I_rel * (CELLMODEL_PARAMVALUE(VT_arel)) * (Ca_rel - Ca_i);

  const double dOcalsedt = 480. * Ca_rel * (1.0 - O_calse) - 400. * O_calse;
  O_calse += dOcalsedt * tinc * 1e-3;

  Ca_rel += (((I_tr - I_rel) * (CELLMODEL_PARAMVALUE(VT_Ad2VrF))) - 31.0 * dOcalsedt) * tinc * 1e-3;
  Ca_up += (I_up - I_tr) * (CELLMODEL_PARAMVALUE(VT_Ad2VuF)) * tinc * 1e-3;

  double r_Cai4 = Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_K_reli)));
  r_Cai4 *= r_Cai4;
  r_Cai4 *= r_Cai4;
  double r_Cad4 = Ca_d / (Ca_d + (CELLMODEL_PARAMVALUE(VT_K_reld)));
  r_Cad4 *= r_Cad4;
  r_Cad4 *= r_Cad4;
  double ract = .2038 * (r_Cai4 + r_Cad4);

  Fl1 += ((CELLMODEL_PARAMVALUE(VT_rrecov)) * (1.0 - Fl1 - Fl2) - ract * Fl1) * tinc;
  Fl2 += (ract * Fl1 - (.03396 + .3396 * r_Cai4) * Fl2) * tinc;

  const double I_Cages = I_BCa + I_CaP - 2.0 * I_NaCa;
  const double I_di = (Ca_d - Ca_i) * (CELLMODEL_PARAMVALUE(VT_FVd2dt));

  const double INa = I_Na + I_BNa + 3.0 * (I_NaK + I_NaCa) + (CELLMODEL_PARAMVALUE(VT_I_Naen_));
  const double IK = I_K - 2.0 * I_NaK;

  Na_i -= INa * (CELLMODEL_PARAMVALUE(VT_AdViF)) * tinc * 1e-3;
  K_i -= IK * (CELLMODEL_PARAMVALUE(VT_AdViF)) * tinc * 1e-3;
  Na_c += (((CELLMODEL_PARAMVALUE(VT_Na_b)) - Na_c) * (CELLMODEL_PARAMVALUE(VT_dt_Na)) + INa * (CELLMODEL_PARAMVALUE(VT_AdVcF))) * tinc * 1e-3;
  K_c += (((CELLMODEL_PARAMVALUE(VT_K_b)) - K_c) * (CELLMODEL_PARAMVALUE(VT_dt_K)) + IK * (CELLMODEL_PARAMVALUE(VT_AdVcF))) * tinc * 1e-3;
  Ca_c +=
      (((CELLMODEL_PARAMVALUE(VT_Ca_b)) - Ca_c) * (CELLMODEL_PARAMVALUE(VT_dt_Ca)) + (I_Cages + I_CaL) * (CELLMODEL_PARAMVALUE(VT_AdVc2F))) * tinc * 1e-3;
  Ca_i -= ((-I_di + I_up - I_rel + I_Cages) * (CELLMODEL_PARAMVALUE(VT_AdVi2F)) + dOdt) * tinc * 1e-3;
  Ca_d -= (I_CaL + I_di) * (CELLMODEL_PARAMVALUE(VT_Ad2VdF)) * tinc * 1e-3;

  return -(I_Na + I_CaL + I_K + I_BNa + I_BCa + I_NaK + I_CaP + I_NaCa
           #ifdef NYEA_TASK
           + I_TASK
           #endif // ifdef NYEA_TASK
           - i_external * 1000) * (CELLMODEL_PARAMVALUE(VT_dC_m)) * tinc * 1e-3;
} // NygrenEtAl::Calc

void NygrenEtAl::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << m << ' ' << h1 << ' ' << h2 << ' '
          << dl << ' ' << fl1 << ' ' << fl2 << ' '
          << n << ' ' << pa << ' '
          << r << ' ' << s << ' '
          << rsus << ' ' << ssus << ' '
          #ifdef NYEA_TASK
          << C1_TASK << ' ' << C2_TASK << ' ' << O_TASK << ' '
          #endif // ifdef NYEA_TASK
          << Na_i << ' ' << Na_c << ' '
          << K_i << ' ' << K_c << ' '
          << Ca_i << ' ' << Ca_c << ' ' << Ca_d << ' '
          << O_c << ' ' << O_tc << ' ' << O_tmgc << ' ' << O_tmgmg << ' ' << O_calse << ' '
          << Ca_up << ' ' << Ca_rel << ' '
          << Fl1 << ' ' << Fl2 << ' ';
}

void NygrenEtAl::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  const double V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(Na_c / Na_i));
  const double VmE_K = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_c / K_i));
  const double VmE_Ca = V_int - ((CELLMODEL_PARAMVALUE(VT_RTd2F)) * log(Ca_c / Ca_i));
  const double f_Ca = Ca_d / (Ca_d + (CELLMODEL_PARAMVALUE(VT_K_Ca)));
  const double NNNICC = (Na_i * Na_i * Na_i * Ca_c);
  const double NNNCCI = (Na_c * Na_c * Na_c * Ca_i);
  const double KNAK = Na_i * sqrt(Na_i); // ==pow((double)Na_i, 1.5));

  double I_Ksus = (CELLMODEL_PARAMVALUE(VT_g_sus)) * rsus * ssus * VmE_K;
#ifdef NYEA_TASK
  const double VFdRT = V_int * CELLMODEL_PARAMVALUE(VT_FdRT);
  const double dexpVFdRT = exp(-VFdRT);
  double I_TASK = CELLMODEL_PARAMVALUE(VT_init_O_TASK) * CELLMODEL_PARAMVALUE(VT_P_TASK) * VFdRT * CELLMODEL_PARAMVALUE(VT_F) * (K_i - K_c * dexpVFdRT) /
                  (1. - dexpVFdRT) * K_c /
                  (CELLMODEL_PARAMVALUE(VT_Koh_TASK) + K_c);
#endif // ifdef NYEA_TASK

  tempstr << pNyeaP->C_Na[Vi] * m * m * m * (0.9 * h1 + 0.1 * h2) * Na_c *
             (exp(VmE_Na * (CELLMODEL_PARAMVALUE(VT_FdRT))) - 1.0) << ' '  // I_Na
          << (CELLMODEL_PARAMVALUE(VT_g_CaL)) * dl * (f_Ca * fl1 + (1.0 - f_Ca) * fl2) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_Caapp)))
          << ' ' // I_CaL
          << ((CELLMODEL_PARAMVALUE(VT_g_K1)) * (pow((double) K_c, 0.4457))) /
             (1.0 + exp(1.5 * (VmE_K + 3.6) * (CELLMODEL_PARAMVALUE(VT_FdRT)))) * VmE_K << ' ' // I_K1
          << (CELLMODEL_PARAMVALUE(VT_g_t)) * r * s * VmE_K << ' ' // I_t
          << I_Ksus << ' ' // I_Ksus
          #ifdef NYEA_TASK
          << I_TASK << ' '  // I_TASK
          #endif // ifdef NYEA_TASK
          << (CELLMODEL_PARAMVALUE(VT_g_Ks)) * n * VmE_K << ' '  // I_Ks
          << (CELLMODEL_PARAMVALUE(VT_g_Kr)) * pa * pNyeaP->pi[Vi] * VmE_K << ' ' // I_Kr
          << (CELLMODEL_PARAMVALUE(VT_g_BNa)) * VmE_Na << ' ' // I_BNa
          << (CELLMODEL_PARAMVALUE(VT_g_BCa)) * VmE_Ca << ' ' // I_BCa
          << pNyeaP->C_NaK[Vi] * (K_c / (K_c + (CELLMODEL_PARAMVALUE(VT_K_NaKK))) * KNAK / (KNAK + (CELLMODEL_PARAMVALUE(VT_K_NaKNapow))))
          << ' ' // I_NaK
          << (CELLMODEL_PARAMVALUE(VT_I_CaP_)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_K_CaP))) << ' ' // I_CaP
          << pNyeaP->C_NaCa[Vi] * (NNNICC - NNNCCI * pNyeaP->dexpV[Vi]) /
             (1.0 + (CELLMODEL_PARAMVALUE(VT_d_NaCa)) * (NNNCCI + NNNICC)) << ' '; // I_NaCa

  tempstr << (CELLMODEL_PARAMVALUE(VT_I_up_)) *
             ((Ca_i * (CELLMODEL_PARAMVALUE(VT_dKcyca)) - (CELLMODEL_PARAMVALUE(VT_Kxcsh2dKsrca)) * Ca_up) /
              ((Ca_i + (CELLMODEL_PARAMVALUE(VT_K_cyca))) * (CELLMODEL_PARAMVALUE(VT_dKcyca)) +
               (CELLMODEL_PARAMVALUE(VT_KxcsdKsrca)) * (Ca_up + (CELLMODEL_PARAMVALUE(VT_K_srca))))) << ' '  // I_up
          << (Ca_up - Ca_rel) * (CELLMODEL_PARAMVALUE(VT_FVr2dt)) << ' ' // I_tr
          << Fl2 / (Fl2 + .25) * Fl2 / (Fl2 + .25) * (CELLMODEL_PARAMVALUE(VT_arel)) * (Ca_rel - Ca_i) << ' ' // I_rel
          << (Ca_d - Ca_i) * (CELLMODEL_PARAMVALUE(VT_FVd2dt)) << ' '; // I_di
} // NygrenEtAl::LongPrint

void NygrenEtAl::GetParameterNames(vector<string> &getpara) {
  const string ParaNames[] =
      {"m", "h1", "h2", "dl", "fl1", "fl2", "n",
       "pa",
       "r", "s", "rsus", "ssus",
#ifdef NYEA_TASK
       "C1_TASK", "C2_TASK", "O_TASK",
#endif // ifdef NYEA_TASK
       "Na_i", "Na_c", "K_i", "K_c", "Ca_i", "Ca_c", "Ca_d",
       "O_c", "O_tc", "O_tmgc", "O_tmgmg", "O_calse", "Ca_up", "Ca_rel",
       "Fl1", "Fl2"};

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void NygrenEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const string ParaNames[] = {"I_Na [pA]", "I_CaL [pA]", "I_K1 [pA]", "I_t [pA]", "I_Ksus [pA]",
#ifdef NYEA_TASK
                              "I_TASK [pA]",
#endif // ifdef NYEA_TASK
                              "I_Ks [pA]", "I_Kr [pA]", "I_bNa [pA]", "I_bCa [pA]", "I_NaK [pA]",
                              "I_pCa [pA]",
                              "I_NaCa [pA]",
                              "I_up [pA]", "I_tr [pA]", "I_rel [pA]", "I_di [pA]"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}
