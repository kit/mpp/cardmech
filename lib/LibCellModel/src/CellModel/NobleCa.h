/**@file NobleCa.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef NOBLECA_H
#define NOBLECA_H

#include <NobleEtAl.h>

class NobleCaSingleCell : public NobleSingleCell {
public:
  NobleCaSingleCell(NobleEtAlParameters *pp) : NobleSingleCell(pp) {}

  ~NobleCaSingleCell() {}

  virtual inline ML_CalcType GetKTrop(ML_CalcType stretch) { return 0.; }

  virtual inline ML_CalcType GetTCa() { return 0.; }

  virtual inline bool OutIsTCa() { return false; }
};


class NobleCaStretchSimple : public NobleStretchSimple {
public:
  NobleCaStretchSimple(NobleEtAlParameters *pp) : NobleStretchSimple(pp) {}

  ~NobleCaStretchSimple() {}

  virtual inline ML_CalcType GetKTrop(ML_CalcType stretch) { return 0.; }

  virtual inline ML_CalcType GetTCa() { return 0.; }

  virtual inline bool OutIsTCa() { return false; }
};


class NobleCaStretch : public NobleStretch {
public:
  NobleCaStretch(NobleEtAlParameters *pp) : NobleStretch(pp) {}

  ~NobleCaStretch() {}

  virtual inline ML_CalcType GetKTrop(ML_CalcType stretch) { return 0.; }

  virtual inline ML_CalcType GetTCa() { return 0.; }

  virtual inline bool OutIsTCa() { return false; }
};


class NobleCaStretchComplex : public NobleStretchComplex {
public:
  NobleCaStretchComplex(NobleEtAlParameters *pp) : NobleStretchComplex(pp) {}

  ~NobleCaStretchComplex() {}

  virtual inline ML_CalcType GetKTrop(ML_CalcType stretch) { return 0.; }

  virtual inline ML_CalcType GetTCa() { return 0.; }

  virtual inline bool OutIsTCa() { return false; }
};

#endif // ifndef NOBLECA_H
