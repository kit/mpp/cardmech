/*! \file Saucerman.h

   \cell model of rat modeling adrenergic influence

   \version 0.0.0

   \date Created Carola Otto (27.11.2008)\n
       template (00.00.00)\n
       man Gunnar Seemann (27.02.03)\n
       Last Modified Eike Wuelfers (03.02.10)

   \author Carola Otto\n
         Institute of Biomedical Engineering\n
         Universitaet Karlsruhe (TH)\n
         http://www.ibt.uni-karlsruhe.de\n
         Copyright 2000-2009 - All rights reserved.

   // \sa  \ref Saucerman
 */

#ifndef SAUCERMAN
#define SAUCERMAN

#include <SaucermanParameters.h>
#include <ida/ida.h>
#include <ida/ida_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>

#define HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_SaucermanParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) ptTeaP->P[NS_SaucermanParameters::a].value
#endif // ifdef HETERO

// used to give user data to the DAE solver
struct UserData {
  // realtype p[145];
  realtype p[148];
};


class Saucerman : public vbElphyModel<ML_CalcType> {
public:
  SaucermanParameters *ptTeaP;

  // state variables (voltage & time)
  ML_CalcType Ca_i, CaSR, Na_i, K_i;
  ML_CalcType m, h, j;      // I_Na
  ML_CalcType xr1, rkr, xs; // IKr & IKs
  ML_CalcType r, s;         // Ito
  ML_CalcType d, f, fCa;    // ICa
  ML_CalcType I_Ksp;        // needed to calculate the phosphorylated part of I_Ks
  ML_CalcType Ca_jsr;
  ML_CalcType Ca_nsr;
  ML_CalcType L, R, Gs;
  ML_CalcType b1ARd, b1ARtot, b1ARp;
  ML_CalcType Gsagtptot, Gsagdp, Gsbg, Gsa_gtp;
  ML_CalcType AC, Fsk, PDE, cAMP, IBMX;
  ML_CalcType PKACI, PKACII, cAMPtot;
  ML_CalcType PLBs, PP1;
  ML_CalcType Inhib1ptot, Inhib1p, LCCap, LCCbp, RyRp;
  ML_CalcType TnIp;
  ML_CalcType Iks, Yotiao, Iksp;
  ML_CalcType trel;
  ML_CalcType t;
  ML_CalcType xs05;
  ML_CalcType Vm_old;


  // required to set initial solver conditions
  int neq;              // number of equations to be solved
  realtype y0[49];      // initial variable conditions
  realtype m0[49];      // mass vector
  UserData sdata;       // user data parameters
  void *rdata;
  N_Vector w;           // y in solver
  N_Vector wd;          // y' in solver

  realtype abstol;      // absolute tolerance
  realtype reltol;      // relative tolerance
  realtype tout;        // time at which next solver output is desired
  realtype hmax;        // maximal time step of solver integration
  realtype mxsteps;     // maximal number of steps until an error flag of the solver is set
  void *ida_mem;        // required for solver memory allocation
  N_Vector id;          // contains mass matrix information in a N_Vector
  N_Vector constraints; // only required if solver should be contraint (e.g. y[46]>=0)
  int flag;             // used do get information about memory allocation and solver initialization success
  realtype *yval, *ypval;
  realtype tout1;

  // realtype I_app;            // uncomment, if internal stimulation is desired


  Saucerman(SaucermanParameters *pp);

  ~Saucerman();

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType SurfaceToVolumeRatio() { return 1.0; }

  virtual inline ML_CalcType Volume() { return 2.064403e-13 * 5.6; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_V_init); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline ML_CalcType GetIto() { return 0.0; }

  virtual inline ML_CalcType GetIKr() { return 0.0; }

  virtual inline ML_CalcType GetIKs() { return 0.0; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &Ca_i; }

  virtual inline ML_CalcType GetSpeedupMax(void) { return .0; }

  virtual ML_CalcType GetAmplitude(void) { return CELLMODEL_PARAMVALUE(VT_Amp);  /*10.*/ }

  virtual inline ML_CalcType GetStimTime() { return 0.005; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual void alloc();

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                           ML_CalcType stretch = 1., int euler = 2);

  virtual void Print(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class Saucerman
#endif // ifndef SAUCERMAN
