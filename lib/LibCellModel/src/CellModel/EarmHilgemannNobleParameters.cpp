/*      File: EarmHilgemannNobleParameters.cpp
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */


#include <EarmHilgemannNobleParameters.h>

EarmHilgemannNobleParameters::EarmHilgemannNobleParameters(const char *initFile, ML_CalcType tinc) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

EarmHilgemannNobleParameters::~EarmHilgemannNobleParameters() {
  // Destruktor
}

void EarmHilgemannNobleParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "EarmHilgemannNobleParameters:" << endl;
  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void EarmHilgemannNobleParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "Loading the EarmHilgemannNoble parameter from " << initFile << " ...\n";
#endif // if KADEBUG

  // Initialization of the Parameters ...
  P[VT_R].name = "R";
  P[VT_Tx].name = "T";
  P[VT_F].name = "F";
  P[VT_RTdF].name = "RTdF";
  P[VT_RTd2F].name = "RTd2F";
  P[VT_FdRT].name = "FdRT";
  P[VT_F2dRT].name = "F2dRT";
  P[VT_C_m].name = "C_m";
  P[VT_dC_m].name = "dC_m";
  P[VT_k_cachoff].name = "k_cachoff";
  P[VT_K_mk1].name = "K_mk1";
  P[VT_k_mk].name = "k_mk";
  P[VT_k_mNa].name = "k_mNa";
  P[VT_k_NaCa].name = "k_NaCa";
  P[VT_d_NaCa].name = "d_NaCa";
  P[VT_diffCa].name = "diffCa";
  P[VT_k_mCa].name = "k_mCa";
  P[VT_I_NaKmax].name = "I_NaKmax";
  P[VT_G_Na].name = "G_Na";
  P[VT_G_to].name = "G_to";
  P[VT_G_bK].name = "G_bK";
  P[VT_G_k1].name = "G_k1";
  P[VT_G_bNa].name = "G_bNa";
  P[VT_G_bCa].name = "G_bCa";
  P[VT_P_Ca].name = "P_Ca";
  P[VT_P_CaK].name = "P_CaK";
  P[VT_P_CaNa].name = "P_CaNa";
  P[VT_Ca_b].name = "Ca_b";
  P[VT_K_o].name = "K_o";
  P[VT_Na_o].name = "Na_o";
  P[VT_NapK].name = "NapK";
  P[VT_Na_od140].name = "Na_od140";
  P[VT_NNNO].name = "NNNO";
  P[VT_g].name = "g";
  P[VT_k_cyCa].name = "k_cyCa";
  P[VT_k_xcs].name = "k_xcs";
  P[VT_k_srCa].name = "k_srCa";
  P[VT_KI_K1].name = "KI_K1";
  P[VT_KI_NaK].name = "KI_NaK";
  P[VT_V_ecs].name = "V_ecs";
  P[VT_radius].name = "radius";
  P[VT_lenght].name = "lenght";
  P[VT_Vol].name = "Vol";
  P[VT_V_cell].name = "V_cell";
  P[VT_V_up].name = "V_up";
  P[VT_V_rel].name = "V_rel";
  P[VT_V_i].name = "V_i";
  P[VT_V_SRup].name = "V_SRup";
  P[VT_k_mCa2].name = "k_mCa2";
  P[VT_M_trop].name = "M_trop";
  P[VT_C_trop].name = "C_trop";
  P[VT_k_1dV_iF].name = "k_1dV_iF";
  P[VT_k_1dV_i2F].name = "k_1dV_i2F";
  P[VT_k_VupdVrel].name = "k_VupdVrel";
  P[VT_k_Vspez].name = "k_Vspez";
  P[VT_k_V_idV_SRup].name = "k_V_idV_SRup";
  P[VT_k_Vspez2].name = "k_Vspez2";
  P[VT_k_kckxdk_s].name = "k_kckxdk_s";
  P[VT_k_kckxp].name = "k_kckxp";
  P[VT_K_i].name = "Init_K_i";
  P[VT_dK_i].name = "dK_i";
  P[VT_Na_i].name = "Init_Na_i";
  P[VT_dNa_i].name = "dNa_i";
  P[VT_E_Namh].name = "E_Namh";
  P[VT_E_Na].name = "E_Na";
  P[VT_I_NaK].name = "I_NaK";
  P[VT_Ca_o].name = "Init_Ca_o";
  P[VT_E_K].name = "E_K";
  P[VT_NNNICO].name = "NNNICO";
  P[VT_r].name = "Init_r";
  P[VT_f_activator].name = "Init_f_activator";
  P[VT_f_product].name = "Init_f_product";
  P[VT_Ca_i].name = "Init_Ca_i";
  P[VT_Ca_calmod].name = "Init_Ca_calmod";
  P[VT_Ca_troponin].name = "Init_Ca_troponin";
  P[VT_Ca_up].name = "Init_Ca_up";
  P[VT_Ca_rel].name = "Init_Ca_rel";
  P[VT_m].name = "Init_m";
  P[VT_h].name = "Init_h";
  P[VT_d].name = "Init_d";
  P[VT_f].name = "Init_f";
  P[VT_q].name = "Init_q";
  P[VT_Vm].name = "Init_Vm";
  P[VT_Amp].name = "Amp";

  P[VT_RTdF].readFromFile = false;
  P[VT_RTd2F].readFromFile = false;
  P[VT_FdRT].readFromFile = false;
  P[VT_F2dRT].readFromFile = false;
  P[VT_dC_m].readFromFile = false;
  P[VT_NapK].readFromFile = false;
  P[VT_Na_od140].readFromFile = false;
  P[VT_NNNO].readFromFile = false;
  P[VT_KI_K1].readFromFile = false;
  P[VT_KI_NaK].readFromFile = false;
  P[VT_Vol].readFromFile = false;
  P[VT_V_cell].readFromFile = false;
  P[VT_V_i].readFromFile = false;
  P[VT_V_SRup].readFromFile = false;
  P[VT_k_1dV_iF].readFromFile = false;
  P[VT_k_1dV_i2F].readFromFile = false;
  P[VT_k_VupdVrel].readFromFile = false;
  P[VT_k_Vspez].readFromFile = false;
  P[VT_k_V_idV_SRup].readFromFile = false;
  P[VT_k_Vspez2].readFromFile = false;
  P[VT_k_kckxdk_s].readFromFile = false;
  P[VT_k_kckxp].readFromFile = false;
  P[VT_dK_i].readFromFile = false;
  P[VT_dNa_i].readFromFile = false;
  P[VT_E_Namh].readFromFile = false;
  P[VT_E_Na].readFromFile = false;
  P[VT_I_NaK].readFromFile = false;
  P[VT_E_K].readFromFile = false;
  P[VT_NNNICO].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_EarmHilgemannNoble);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  // End Initialization of the Parameters ...

  Calculate();
  InitTable(tinc);
} // EarmHilgemannNobleParameters::Init

void EarmHilgemannNobleParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "EarmHilgemannNobleParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_RTdF].value = P[VT_R].value * (P[VT_Tx].value) / (P[VT_F].value);
  P[VT_RTd2F].value = P[VT_RTdF].value * .5;
  P[VT_FdRT].value = 1.0 / (P[VT_RTdF].value);
  P[VT_F2dRT].value = 1.0 / (P[VT_RTd2F].value);
  P[VT_dC_m].value = .001 / (P[VT_C_m].value);
  P[VT_NapK].value = (P[VT_Na_o].value + .12 * (P[VT_K_o].value));
  P[VT_Na_od140].value = P[VT_Na_o].value / 140.0;
  P[VT_NNNO].value = P[VT_Na_o].value * (P[VT_Na_o].value) * (P[VT_Na_o].value);
  P[VT_KI_K1].value =
      P[VT_G_k1].value * (P[VT_K_o].value) / (P[VT_K_o].value + (P[VT_K_mk1].value));
  P[VT_KI_NaK].value =
      P[VT_I_NaKmax].value * (P[VT_K_o].value) / (P[VT_K_o].value + (P[VT_k_mk].value));
  P[VT_Vol].value = M_PI * (P[VT_radius].value) * (P[VT_radius].value) * (P[VT_lenght].value);
  P[VT_V_cell].value = P[VT_Vol].value * 1e9;
  P[VT_V_i].value =
      (1.0 - (P[VT_V_ecs].value) - (P[VT_V_up].value) - (P[VT_V_rel].value)) * (P[VT_V_cell].value);
  P[VT_V_SRup].value = P[VT_V_cell].value * (P[VT_V_up].value);
  P[VT_k_1dV_iF].value = 1.0 / (P[VT_V_i].value * (P[VT_F].value));
  P[VT_k_1dV_i2F].value = P[VT_k_1dV_iF].value * .5;
  P[VT_k_VupdVrel].value = P[VT_V_up].value / (P[VT_V_rel].value);
  P[VT_k_Vspez].value = 1.0 / (2.0 * (P[VT_V_cell].value) * (P[VT_V_ecs].value) * (P[VT_F].value));
  P[VT_k_V_idV_SRup].value = P[VT_V_i].value / (P[VT_V_SRup].value);
  P[VT_k_Vspez2].value = 1.0 / (P[VT_k_V_idV_SRup].value * (P[VT_k_VupdVrel].value));
  P[VT_k_kckxdk_s].value = P[VT_k_cyCa].value * (P[VT_k_xcs].value) / (P[VT_k_srCa].value);
  P[VT_k_kckxp].value = P[VT_k_cyCa].value * (P[VT_k_xcs].value) + (P[VT_k_cyCa].value);
  P[VT_dK_i].value = 1.0 / (P[VT_K_i].value);
  P[VT_dNa_i].value = 1.0 / (P[VT_Na_i].value);
  P[VT_E_Namh].value =
      P[VT_RTdF].value * log(P[VT_NapK].value / (P[VT_Na_i].value + .12 * (P[VT_K_i].value)));
  P[VT_E_Na].value = P[VT_RTdF].value * log(P[VT_Na_o].value / (P[VT_Na_i].value));
  P[VT_I_NaK].value =
      P[VT_KI_NaK].value * (P[VT_Na_i].value) / (P[VT_Na_i].value + (P[VT_k_mNa].value));
  P[VT_E_K].value = P[VT_RTdF].value * log(P[VT_K_o].value * (P[VT_dK_i].value));
  P[VT_NNNICO].value =
      P[VT_Na_i].value * (P[VT_Na_i].value) * (P[VT_Na_i].value) * (P[VT_Ca_o].value);
  exp50 = exp(50.0 * (P[VT_FdRT].value));
  exp50_2 = exp(50.0 * (P[VT_F2dRT].value));
} // EarmHilgemannNobleParameters::Calculate

void EarmHilgemannNobleParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr << "EarmHilgemannNoble::InitTable()\n";
#endif // if KADEBUG
  double a, b;
  tinc *= -1;  // Neg. for exptau calculation
  for (double V = -RangeTabhalf + .0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int) (DivisionTab * (RangeTabhalf + V) + .5);
    a = 200.0 * (V + 41.0) / (1.0 - exp(-.1 * (V + 41.0)));
    b = a + 8000.0 * exp(-.056 * (V + 66.0));
    m_inf[Vi] = a / b;
    exptau_m[Vi] = exp(tinc * b);

    a = 20.0 * exp(-.125 * (V + 75.0));
    b = a + 2000.0 / (1.0 + 320.0 * exp(-.1 * (V + 75.0)));
    h_inf[Vi] = a / b;
    exptau_h[Vi] = exp(tinc * b);

    a = 90.0 * (V + 19.0) / (1.0 - exp(-(V + 19.0) * .25));
    b = a + 36.0 * (V + 19.0) / (exp((V + 19.0) * .1) - 1.0);
    d_inf[Vi] = a / b;
    exptau_d[Vi] = exp(tinc * b);

    a_f[Vi] = 12.0 / (1.0 + exp((V + 34.0) * 0.25));
    b_f[Vi] = 6.25 * (V + 34.0) / (exp((V + 34.0) * 0.25) - 1.0);

    // q_[Vi]=1/(1+exp(-(V+4)/5));
    q_[Vi] = 1.0 / (1.0 + exp(-(V + 10.0) * 0.2)); // fs

    a = 0.033 * exp(-V / 17.0);
    b = a + 33.0 / (1.0 + exp(-(V + 10.0) * 0.125));
    r_inf[Vi] = a / b;
    exptau_r[Vi] = exp(tinc * b);

    double V50RF = (V - 50.0) * (P[VT_FdRT].value);
    dexpV[Vi] = exp(-V * (P[VT_FdRT].value));
    KexpV[Vi] = (P[VT_K_o].value) * dexpV[Vi];
    NaexpV[Vi] = (P[VT_Na_o].value) * dexpV[Vi];
    dexpV50_2[Vi] = exp(-V * (P[VT_F2dRT].value));
    VdV[Vi] =
        (P[VT_P_Ca].value) * (P[VT_k_cachoff].value) * V50RF / (1.0 - dexpV[Vi] * exp50) * exp50;
    VdV2[Vi] = 4.0 * (P[VT_P_Ca].value) * (P[VT_k_cachoff].value) * V50RF /
               (1.0 - dexpV50_2[Vi] * exp50_2) * exp50_2 / VdV[Vi];
    KINaCa[Vi] = (P[VT_k_NaCa].value) * exp((P[VT_g].value) * V * (P[VT_FdRT].value));
    sechs100exp[Vi] = 600.0 * exp((V - 40.0) * .08);
    CI_si[Vi] = (P[VT_P_CaK].value) * (P[VT_K_i].value - (P[VT_K_o].value) * dexpV[Vi]) +
                (P[VT_P_CaNa].value) *
                (P[VT_Na_i].value - (P[VT_Na_o].value) * dexpV[Vi]);
  }
} // EarmHilgemannNobleParameters::InitTable
