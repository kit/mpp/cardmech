/*
 *  SharedModelParameters.cpp
 *  CellModelTest
 *
 *  Created by Manuel Ifland on 16.03.07.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */

#include <SharedModelParameters.h>

SharedModelParameters::SharedModelParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
  HM     = new HandleManager<ML_CalcType>(initFile);
  handle = HM->getHandle();
  pEP    = HM->getpEP();

  // num=HM->num;
}

SharedModelParameters::~SharedModelParameters() {
  delete HM;
}

void SharedModelParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "SharedModelParameters:init " << initFile << endl;
#endif // if KADEBUG
  P[VT_R].name            = "R";
  P[VT_T_x].name          = "T_x";
  P[VT_F].name            = "F";
  P[VT_C_m].name          = "C_m";
  P[VT_pNaKs].name        = "pNaKs";
  P[VT_init_Vm].name      = "init_Vm";
  P[VT_init_Nai].name     = "init_Nai";
  P[VT_init_Nao].name     = "init_Nao";
  P[VT_init_Ki].name      = "init_Ki";
  P[VT_init_Ko].name      = "init_Ko";
  P[VT_init_Cai].name     = "init_Cai";
  P[VT_init_Cao].name     = "init_Cao";
  P[VT_init_IKv14Na].name = "init_IKv14Na";
  P[VT_init_CaSS].name    = "init_CaSS";
  P[VT_init_ICaK].name    = "init_ICaK";
  P[VT_init_ICa].name     = "init_ICa";
  P[VT_init_ICab].name    = "init_ICab";
  P[VT_init_INaCa].name   = "init_INaCa";
  P[VT_init_INa].name     = "init_INa";
  P[VT_init_INab].name    = "init_INab";
  P[VT_init_INaK].name    = "init_INaK";
  P[VT_init_IKr].name     = "init_IKr";
  P[VT_init_IKs].name     = "init_IKs";
  P[VT_init_IK1].name     = "init_IK1";
  P[VT_init_Ito].name     = "init_Ito";
  P[VT_init_IpCa].name    = "init_IpCa";
  P[VT_init_ICaK2].name   = "init_ICaK2";
  P[VT_Amp].name          = "Amp";

  P[VT_CmdF].readFromFile = false;
  P[VT_RTdF].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_SharedModel);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
  InitTable();
} // SharedModelParameters::Init

void SharedModelParameters::PrintParameters() {
  cout<<"SharedModelParameters:"<<endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void SharedModelParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "SharedModelParameters - Calculate ..." << endl;
#endif // if KADEBUG

  /*!< These are constant values used at different locations and stored in the global array (see SharedModel::Init()) */
  P[VT_RTdF].value = P[VT_R].value * (P[VT_T_x].value) / (P[VT_F].value);
  P[VT_CmdF].value = (P[VT_C_m].value) / (P[VT_F].value);
}

void SharedModelParameters::InitTable() {
#if KADEBUG
  cerr << "SharedModelParameters - InitTable()" << endl;
  cerr << "no table 2 init ...\n";
#endif // if KADEBUG
}
