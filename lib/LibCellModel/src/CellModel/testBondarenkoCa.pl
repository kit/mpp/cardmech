#!/usr/bin/perl -w
#step depolarisation script 


system("rm t.min");
for ($i=-30;$i<=40;$i+=30)
{
    printf("%d mV\n", $i);
    system("echo \"Vm 0 -0.08\" >t.clamp");
    system("echo \"Vm 1 -0.08\" >>t.clamp");
    system("echo \"Vm .25 ".($i/1000.)."\" >>t.clamp");
    system("echo \"Vm .002 -0.08\" >>t.clamp");
    system("echo \"Vm .25 ".($i/1000.)."\" >>t.clamp");
    
    system("ElphyModelTest -BondarenkoCa -clamp t.clamp -tend 1.6 -tinc 1e-6 -toutinc 1000 -toutbegin 0.99 -lp > t".$i);

    system("echo -n ".($i/1000)." \" \" >>t.min");
    system("APAnalysis t".$i." -min -col 11 >>t.min");
}
