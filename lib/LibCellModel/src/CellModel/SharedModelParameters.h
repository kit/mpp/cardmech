/*
 *  SharedModelParameters.h
 *  CellModelTest
 *
 *  Created by Manuel Ifland on 16.03.07.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SharedModelParameters_H
#define SharedModelParameters_H

#include <HandleManager.h>
#include <ParameterLoader.h>

namespace NS_SharedModelParameters {
  enum varType {
    VT_R = vtFirst,
    VT_T_x,
    VT_F,
    VT_C_m,
    VT_pNaKs,
    VT_init_Vm,
    VT_init_Nai,
    VT_init_Nao,
    VT_init_Ki,
    VT_init_Ko,
    VT_init_Cai,
    VT_init_Cao,
    VT_init_IKv14Na,
    VT_init_CaSS,
    VT_init_ICaK,
    VT_init_ICa,
    VT_init_ICab,
    VT_init_INaCa,
    VT_init_INa,
    VT_init_INab,
    VT_init_INaK,
    VT_init_IKr,
    VT_init_IKs,
    VT_init_IK1,
    VT_init_Ito,
    VT_init_IpCa,
    VT_init_ICaK2,

    VT_RTdF,
    VT_CmdF,
    VT_Amp,
    vtLast
  };
} // namespace NS_SharedModelParameters

using namespace NS_SharedModelParameters;

class SharedModelParameters : public vbNewElphyParameters {
public:
  SharedModelParameters(const char *);

  ~SharedModelParameters();

  void Init(const char *);

  void PrintParameters();

  void Calculate();

  void InitTable();

  // virtual inline int GetSize( void );
  // virtual inline ML_CalcType* GetBase( void );
  // virtual int GetNumParameters();

  HandleManager<ML_CalcType> *HM;

  vbElphyParameters<ML_CalcType> **pEP;
  void **handle;
};

#endif // ifndef SharedModelParameters_H
