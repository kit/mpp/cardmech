/**@file IyerMazhariWinslowKr.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowKr.h>

IyerMazhariWinslowKr::IyerMazhariWinslowKr(IyerMazhariWinslowKrParameters *pIMWArg) {
  pIMW = pIMWArg;
#ifdef HETERO
  PS = new ParameterSwitch(pIMW, NS_IyerMazhariWinslowKrParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

void IyerMazhariWinslowKr::Init() {
  Ki = CELLMODEL_PARAMVALUE(VT_Ki);

  C1Kr = CELLMODEL_PARAMVALUE(VT_C1Kr);
  C2Kr = CELLMODEL_PARAMVALUE(VT_C2Kr);
  C3Kr = CELLMODEL_PARAMVALUE(VT_C3Kr);
  OKr = CELLMODEL_PARAMVALUE(VT_OKr);
  IKr = CELLMODEL_PARAMVALUE(VT_IKr);

  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);
  Qb = pow(3.3, (Tx - 296.) / 10.);  // Iyer et al.
}

void IyerMazhariWinslowKr::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' '  // 1
          << V << ' '
          << Ki << ' '
          << C1Kr << ' '
          << C2Kr << ' ' // 5
          << C3Kr << ' '
          << OKr << ' '
          << IKr << ' ';
}

void IyerMazhariWinslowKr::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);

  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);
  const double Acap = CELLMODEL_PARAMVALUE(VT_Acap);
  const double Vmyo = CELLMODEL_PARAMVALUE(VT_Vmyo);

  const double V_int = V * 1000.0;
  const double RTdF = ElphyModelConstants::R * Tx / ElphyModelConstants::F;

  const double Ko = CELLMODEL_PARAMVALUE(VT_Ko);

  const double EK = RTdF * log(Ko / Ki); // -92 mV for resting 1 Hz

  const double f = sqrt(Ko * .25);
  const double I_Kr = CELLMODEL_PARAMVALUE(VT_GKr) * f * OKr * (V_int - EK);

  tempstr << ' ' << I_Kr;  // 9
}

void IyerMazhariWinslowKr::GetParameterNames(vector<string> &getpara) {
  const char *ParaNames[] = {"Ki", "C1Kr", "C2Kr", "C3Kr", "OKr", "IKr"};

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void IyerMazhariWinslowKr::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const char *ParaNames[] = {"I_Kr"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType IyerMazhariWinslowKr::Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim = .0,
                                       ML_CalcType stretch = 1.,
                                       int euler = 1) {
  tinc *= 1000.0;
  const double V_int = V * 1000.0;

  const double Kra0 = CELLMODEL_PARAMVALUE(VT_Kra0a) * exp(CELLMODEL_PARAMVALUE(VT_Kra0b) * V_int);
  const double Krb0 = CELLMODEL_PARAMVALUE(VT_Krb0a) * exp(CELLMODEL_PARAMVALUE(VT_Krb0b) * V_int);
  const double Kra1 = CELLMODEL_PARAMVALUE(VT_Kra1a) * exp(CELLMODEL_PARAMVALUE(VT_Kra1b) * V_int);
  const double Krb1 = CELLMODEL_PARAMVALUE(VT_Krb1a) * exp(CELLMODEL_PARAMVALUE(VT_Krb1b) * V_int);
  const double Krai = CELLMODEL_PARAMVALUE(VT_Kraia) * exp(CELLMODEL_PARAMVALUE(VT_Kraib) * V_int);
  const double Krbi = CELLMODEL_PARAMVALUE(VT_Krbia) * exp(CELLMODEL_PARAMVALUE(VT_Krbib) * V_int);
  const double Krai3 = CELLMODEL_PARAMVALUE(VT_Krai3a) * exp(CELLMODEL_PARAMVALUE(VT_Krai3b) * V_int);
  const double Krkf = CELLMODEL_PARAMVALUE(VT_Krkf);
  const double Krkb = CELLMODEL_PARAMVALUE(VT_Krkb);

  const double psi = Krb1 * Krbi * Krai3 / (Kra1 * Krai);

  const double dC1Kr = (-Kra0 * C1Kr + Krb0 * C2Kr);
  const double dC2Kr = (-(Krb0 + Krkf) * C2Kr + Kra0 * C1Kr + Krkb * C3Kr);
  const double dC3Kr = (-(Kra1 + Krai3 + Krkb) * C3Kr + Krkf * C2Kr + Krb1 * OKr + psi * IKr);

  //    const double dOKr= (-(Krb1+Krai)      *OKr +Kra1* C3Kr+Krbi*IKr);
  const double dIKr = (-(psi + Krbi) * IKr + Krai3 * C3Kr + Krai * OKr);

  C1Kr += tinc * dC1Kr * Qb;
  C2Kr += tinc * dC2Kr * Qb;
  C3Kr += tinc * dC3Kr * Qb;

  // OKr+=tinc*dOKr*Qb;
  IKr += tinc * dIKr * Qb;
  OKr = 1. - C1Kr - C2Kr - C3Kr - IKr;

  assert(C1Kr >= 0. && C1Kr <= 1.);
  assert(C2Kr >= 0. && C2Kr <= 1.);
  assert(C3Kr >= 0. && C3Kr <= 1.);
  assert(OKr >= 0. && OKr <= 1.);
  assert(IKr >= 0. && IKr <= 1.);

  return 0.;
} // IyerMazhariWinslowKr::Calc

void IyerMazhariWinslowKr::GetStatus(double *p) const {
  p[0] = Ki;
  p[1] = C1Kr;
  p[2] = C2Kr;
  p[3] = C3Kr;
  p[4] = OKr;
  p[5] = IKr;
}

void IyerMazhariWinslowKr::SetStatus(const double *p) {
  Ki = p[0];
  C1Kr = p[1];
  C2Kr = p[2];
  C3Kr = p[3];
  OKr = p[4];
  IKr = p[5];
}
