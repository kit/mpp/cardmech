/**@file IyerParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerParameters.h>

IyerParameters::IyerParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);

  HM = new HandleManager<ML_CalcType>(initFile);
  handle = HM->getHandle();
  pEP = HM->getpEP();
}

IyerParameters::~IyerParameters() {
  cerr << "deleting handles ...\n";
  if (handle)
    delete[] handle;
  if (pEP)
    delete[] pEP;
  if (HM)
    delete HM;
}

void IyerParameters::PrintParameters() {
  cout << "IyerParameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void IyerParameters::Init(const char *initFile) {
  P[VT_R].name = "R";
  P[VT_T_x].name = "T";
  P[VT_F].name = "F";
  P[VT_init_Vm].name = "init_V_m";
  P[VT_init_Nai].name = "init_Nai";
  P[VT_init_Nao].name = "init_Nao";
  P[VT_init_Ki].name = "init_Ki";
  P[VT_init_Ko].name = "init_Ko";
  P[VT_init_Cai].name = "init_Cai";
  P[VT_init_Cao].name = "init_Cao";
  P[VT_init_IKv14Na].name = "init_IKv14Na";
  P[VT_init_CaSS].name = "init_ICaSS";
  P[VT_init_ICaK].name = "init_ICaK";

  P[VT_init_ICa].name = "init_ICa";
  P[VT_init_ICab].name = "init_ICab";
  P[VT_init_INaCa].name = "init_INaCa";
  P[VT_init_INa].name = "init_INa";
  P[VT_init_INab].name = "init_INab";
  P[VT_init_INaK].name = "init_INaK";
  P[VT_init_IKr].name = "init_IKr";
  P[VT_init_IKs].name = "init_IKs";
  P[VT_init_IK1].name = "init_IK1";
  P[VT_init_Ito].name = "init_Ito";
  P[VT_init_IpCa].name = "init_IpCa";
  P[VT_init_ICaK2].name = "init_ICaK2";
  P[VT_Amp].name = "Amp";
  P[VT_RTdF].name = "RTdF";

  P[VT_RTdF].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_Iyer);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
  InitTable();
} // IyerParameters::Init

void IyerParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "IyerParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_RTdF].value = P[VT_R].value * (P[VT_T_x].value) / (P[VT_F].value);
}

void IyerParameters::InitTable() {
#if KADEBUG
  cerr << "IyerParameters - InitTable()" << endl;
  cerr << "no table 2 init ...\n";
#endif // if KADEBUG
}
