/* File: LindbladEtAlParameters.cpp
        automatically created by CellML2Elphymodel.pl
        Institute of Biomedical Engineering, Universität Karlsruhe (TH) */

#include <LindbladEtAlParameters.h>
LindbladEtAlParameters::LindbladEtAlParameters(const char *initFile, ML_CalcType tinc) {
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

LindbladEtAlParameters::~LindbladEtAlParameters() {}

void LindbladEtAlParameters::PrintParameters() {
  cout << "LindbladEtAlParameters:"<<endl;
  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= "     << P[i].value <<endl;
  }
}

void LindbladEtAlParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "Loading the LindbladEtAl parameter from " << initFile << " ...\n";
#endif // if KADEBUG

  P[VT_R].name              = "R";
  P[VT_T].name              = "T";
  P[VT_F].name              = "F";
  P[VT_Cm].name             = "Cm";
  P[VT_stim_start].name     = "stim_start";
  P[VT_stim_end].name       = "stim_end";
  P[VT_stim_period].name    = "stim_period";
  P[VT_stim_duration].name  = "stim_duration";
  P[VT_stim_amplitude].name = "stim_amplitude";
  P[VT_P_Na].name           = "P_Na";
  P[VT_Na_c].name           = "Na_c";
  P[VT_g_Ca_L].name         = "g_Ca_L";
  P[VT_E_Ca_app].name       = "E_Ca_app";
  P[VT_g_Ca_T].name         = "g_Ca_T";
  P[VT_E_Ca_T].name         = "E_Ca_T";
  P[VT_g_to].name           = "g_to";
  P[VT_K_c].name            = "K_c";
  P[VT_g_Ks].name           = "g_Ks";
  P[VT_g_Kr].name           = "g_Kr";
  P[VT_g_K1].name           = "g_K1";
  P[VT_KmK1].name           = "KmK1";
  P[VT_steepK1].name        = "steepK1";
  P[VT_shiftK1].name        = "shiftK1";
  P[VT_g_B_Na].name         = "g_B_Na";
  P[VT_g_B_Ca].name         = "g_B_Ca";
  P[VT_g_B_Cl].name         = "g_B_Cl";
  P[VT_Ca_c].name           = "Ca_c";
  P[VT_Cl_c].name           = "Cl_c";
  P[VT_Cl_i].name           = "Cl_i";
  P[VT_k_NaK_K].name        = "k_NaK_K";
  P[VT_k_NaK_Na].name       = "k_NaK_Na";
  P[VT_i_NaK_max].name      = "i_NaK_max";
  P[VT_i_CaP_max].name      = "i_CaP_max";
  P[VT_k_CaP].name          = "k_CaP";
  P[VT_k_NaCa].name         = "k_NaCa";
  P[VT_d_NaCa].name         = "d_NaCa";
  P[VT_gamma].name          = "gamma";
  P[VT_Vol_i].name          = "Vol_i";
  P[VT_Vol_Ca].name         = "Vol_Ca";
  P[VT_Mg_i].name           = "Mg_i";
  P[VT_I_up_max].name       = "I_up_max";
  P[VT_k_cyca].name         = "k_cyca";
  P[VT_k_srca].name         = "k_srca";
  P[VT_k_xcs].name          = "k_xcs";
  P[VT_alpha_rel].name      = "alpha_rel";
  P[VT_Vol_up].name         = "Vol_up";
  P[VT_Vol_rel].name        = "Vol_rel";
  P[VT_tau_tr].name         = "tau_tr";
  P[VT_k_rel].name          = "k_rel";
  P[VT_k_F3].name           = "k_F3";
  P[VT_E_Cl].name           = "E_Cl";
  P[VT_E_B_Cl].name         = "E_B_Cl";
  P[VT_V_init].name         = "V_init";
  P[VT_Na_i_init].name      = "Na_i_init";
  P[VT_m_init].name         = "m_init";
  P[VT_h1_init].name        = "h1_init";
  P[VT_h2_init].name        = "h2_init";
  P[VT_d_L_init].name       = "d_L_init";
  P[VT_f_L_init].name       = "f_L_init";
  P[VT_d_T_init].name       = "d_T_init";
  P[VT_f_T_init].name       = "f_T_init";
  P[VT_K_i_init].name       = "K_i_init";
  P[VT_r_init].name         = "r_init";
  P[VT_s1_init].name        = "s1_init";
  P[VT_s2_init].name        = "s2_init";
  P[VT_s3_init].name        = "s3_init";
  P[VT_z_init].name         = "z_init";
  P[VT_p_a_init].name       = "p_a_init";
  P[VT_p_i_init].name       = "p_i_init";
  P[VT_Ca_i_init].name      = "Ca_i_init";
  P[VT_O_C_init].name       = "O_C_init";
  P[VT_O_TC_init].name      = "O_TC_init";
  P[VT_O_TMgC_init].name    = "O_TMgC_init";
  P[VT_O_TMgMg_init].name   = "O_TMgMg_init";
  P[VT_Ca_rel_init].name    = "Ca_rel_init";
  P[VT_Ca_up_init].name     = "Ca_up_init";
  P[VT_O_Calse_init].name   = "O_Calse_init";
  P[VT_F1_init].name        = "F1_init";
  P[VT_F2_init].name        = "F2_init";
  P[VT_RTdF].name           = "RTdF";
  P[VT_powKc].name          = "powKc";
  P[VT_powNac].name         = "powNac";
  P[VT_powk_NaK_Na].name    = "powk_NaK_Na";

  P[VT_E_Cl].readFromFile        = false;
  P[VT_E_B_Cl].readFromFile      = false;
  P[VT_RTdF].readFromFile        = false;
  P[VT_powKc].readFromFile       = false;
  P[VT_powNac].readFromFile      = false;
  P[VT_powk_NaK_Na].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_LindbladEtAl);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);
  Calculate();
  InitTable(tinc);
#if KADEBUG
  cerr << "#Init() done ... \n";
#endif // if KADEBUG
} // LindbladEtAlParameters::Init

void LindbladEtAlParameters::Calculate() {
#if KADEBUG
  cerr << "#LindbladEtAlParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_E_Cl].value =   (P[VT_R].value)*(P[VT_T].value)/(P[VT_F].value)*
    log((P[VT_Cl_i].value)/(P[VT_Cl_c].value));
  P[VT_E_B_Cl].value      = (P[VT_E_Cl].value) -  0.490000*((P[VT_E_Cl].value)+30.5900);
  P[VT_RTdF].value        = P[VT_R].value*(P[VT_T].value)/(P[VT_F].value);
  P[VT_powKc].value       = pow(P[VT_K_c].value/(P[VT_K_c].value+P[VT_KmK1].value), 3.00000);
  P[VT_powk_NaK_Na].value = pow(P[VT_k_NaK_Na].value, 1.50000);
  P[VT_powNac].value      = pow(P[VT_Na_c].value, 3.0);
}

// InitTableWithtinc

void LindbladEtAlParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr << "#LindbladEtAlParameters - InitTable ..." << endl;
#endif // if KADEBUG
  ML_CalcType HT = tinc * 1000;
  for (double V = -RangeTabhalf+.0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int)(DivisionTab*(RangeTabhalf+V)+.5);
    exptau_s1[Vi]   = exp(-tinc / (0.546600/(1.00000+exp((V+32.8000)/0.100000))+0.0204000));
    s1_infinity[Vi] = 1.00000/(1.00000+exp((V+28.2900)/7.06000));
    exptau_s2[Vi]   =
      exp(-tinc / (5.75000/(1.00000+exp((V+32.8000)/0.100000))+0.450000/(1.00000+exp((V - 13.5400)/ -13.9700))));
    s2_infinity[Vi] = 1.00000/(1.00000+exp((V+28.2900)/7.06000));
    exptau_s3[Vi]   = exp(-tinc / (7.50000/(1.00000+exp((V+23.0000)/0.500000))+0.500000));
    s3_infinity[Vi] = (1.00000/(1.00000+exp((V+50.6700)/27.3800))+0.666000)/1.66600;
    double E0_m    = V+44.4000;
    double alpha_m = -460.000*E0_m/(exp(E0_m/ -12.6730) - 1.00000);
    double beta_m  =  18400.0*exp(E0_m/ -12.6730);
    m_infinity[Vi] = alpha_m/(alpha_m+beta_m);
    exptau_m[Vi]   = exp(-tinc / (1.0/(alpha_m+beta_m)));
    double alpha_h =  44.9000*exp((V+66.9000)/ -5.57000);
    double beta_h  = 1491.00/(1.00000+ 323.300*exp((V+94.6000)/ -12.9000));
    h_infinity[Vi] = alpha_h/(alpha_h+beta_h);
    exptau_h1[Vi]  = exp(-tinc / (0.0300000/(1.00000+exp((V+40.0000)/6.00000))+0.000350000));
    exptau_h2[Vi]  = exp(-tinc / (0.120000/(1.00000+exp((V+60.0000)/2.00000))+0.00295000));
    double alpha_r =  386.600*exp(V/12.0000);
    double beta_r  =  8.01100*exp(V/ -7.20000);
    exptau_r[Vi]   = exp(-tinc / (1.00000/(alpha_r+beta_r)+0.000400000));
    r_infinity[Vi] = 1.00000/(1.00000+exp((V+15.0000)/ -5.63300));
    double alpha_z =  1.66000*exp(V/69.4520);
    double beta_z  =  0.300000*exp(V/ -21.8260);
    exptau_z[Vi]   = exp(-tinc / (1.00000/(alpha_z+beta_z)+0.0600000));
    z_infinity[Vi] = 1.00000/(1.00000+exp((V - 0.900000)/ -13.8000));
    double alpha_p_a =  9.00000*exp(V/25.3710);
    double beta_p_a  =  1.30000*exp(V/ -13.0260);
    exptau_p_a[Vi]   = exp(-tinc / (1.00000/(alpha_p_a+beta_p_a)));
    p_a_infinity[Vi] = 1.00000/(1.00000+exp((V+5.10000)/ -7.40000));
    double alpha_p_i =  100.000*exp(V/ -54.6450);
    double beta_p_i  =  656.000*exp(V/106.157);
    exptau_p_i[Vi]   = exp(-tinc / (1.00000/(alpha_p_i+beta_p_i)));
    p_i_infinity[Vi] = 1.00000/(1.00000+exp((V+47.3921)/18.6603));
    double E0_d_T    = V+23.0000;
    double E0_f_T    = V+75.0000;
    double alpha_d_T =  674.173*exp(E0_d_T/30.0000);
    double beta_d_T  =  674.173*exp(E0_d_T/ -30.0000);
    d_T_infinity[Vi] = 1.00000/(1.00000+exp(E0_d_T/ -6.10000));
    exptau_d_T[Vi]   = exp(-tinc / (1.00000/(alpha_d_T+beta_d_T)));
    double alpha_f_T =  9.63700*exp(E0_f_T/ -83.3330);
    double beta_f_T  =  9.63700*exp(E0_f_T/15.3850);
    f_T_infinity[Vi] = alpha_f_T/(alpha_f_T+beta_f_T);
    exptau_f_T[Vi]   = exp(-tinc / (1.00000/(alpha_f_T+beta_f_T)));

    double E0_alpha_d_L = V+35.0000;
    double E0_beta_d_L  = V - 5.00000;
    double E0_f_L       = V+28.0000;
    double x_f          = (V+37.4270)/20.2130;
    double alpha_d_L    =  -16.7200*E0_alpha_d_L/(exp(E0_alpha_d_L/ -2.50000) - 1.00000)+ -50.0000*V/
      (exp(V/ -4.80800) - 1.00000);
    double beta_d_L =  4.48000*E0_beta_d_L/(exp(E0_beta_d_L/2.50000) - 1.00000);
    d_L_infinity[Vi] = 1.00000/(1.00000+exp((V+0.950000)/ -6.60000));
    exptau_d_L[Vi]   = exp(-tinc / (1.00000/(alpha_d_L+beta_d_L)));
    double alpha_f_L =  8.49000*E0_f_L/(exp(E0_f_L/4.00000) - 1.00000);
    double beta_f_L  = 67.9220/(1.00000+exp(E0_f_L/ -4.00000));
    f_L_infinity[Vi] = alpha_f_L/(alpha_f_L+beta_f_L);
    exptau_f_L[Vi]   =  exp(-tinc / (0.211000*exp(-x_f*x_f)+0.0150000));


    d_prime[Vi] = 1.00000/(1.00000+exp((V - 33.0000)/ -12.0000));
    i_B_Cl[Vi]  =  (P[VT_g_B_Cl].value)*(V - (P[VT_E_B_Cl].value))*
      (1.00000+exp((V - ((P[VT_E_Cl].value)+36.9500))/74.5140));
    i_p_Const[Vi]  = P[VT_K_c].value/(P[VT_K_c].value+P[VT_k_NaK_K].value)*1.60000/(1.50000+exp((V+60.0000)/ -40.0000));
    i_Na_Const[Vi] = P[VT_Na_c].value*V/P[VT_RTdF].value*P[VT_F].value/(exp(V/P[VT_RTdF].value) - 1.00000);
  }
} // LindbladEtAlParameters::InitTable
