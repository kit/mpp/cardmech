/**@file NobleForceParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <NobleForceParameters.h>

NobleForceParameters::NobleForceParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void NobleForceParameters::PrintParameters() {
  // print the parameter to the stdout
  cout<<"NobleForceParameters:"<<endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void NobleForceParameters::Init(const char *initFile) {
  P[VT_aCa_calmod].name     = "aCa_calmod";
  P[VT_aTCa].name           = "aTCa";
  P[VT_af_LightChain].name  = "af_LightChain";
  P[VT_af_CrossBridge].name = "af_CrossBridge";
  P[VT_a_troponin].name     = "a_troponin";
  P[VT_b_troponin].name     = "b_troponin";
  P[VT_Calmod].name         = "Calmod";
  P[VT_Troponin].name       = "Troponin";
  P[VT_KCalTrop].name       = "KCalTrop";
  P[VT_SL].name             = "SL";
  P[VT_k_cont2].name        = "k_cont2";
  P[VT_k_cont3].name        = "k_cont3";
  P[VT_k_cont4].name        = "k_cont4";
  P[VT_CBden].name          = "CBden";

  ParameterLoader FPL(initFile, FMT_NobleForce);
  this->setOverlapParameters(FPL.getOverlapString());
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = FPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
}

void NobleForceParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
}
