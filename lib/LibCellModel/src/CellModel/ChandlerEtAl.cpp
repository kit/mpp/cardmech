/*      File: ChandlerEtAl.cpp
   Created by al128 (23.01.2013)
   Institute of Biomedical Engineering, KIT
 */

#include <ChandlerEtAl.h>

Chandler::Chandler(ChandlerParameters *pp) {
  pCmP = pp;
  Init();
}

Chandler::~Chandler() {}

inline int Chandler::GetSize(void) {
  // return ( &fT - &Ca_i + 1 ) * sizeof( ML_CalcType );
  return sizeof(Chandler) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(ChandlerParameters *)
#ifdef HETERO
    -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
      ;
}

inline unsigned char Chandler::getSpeed(ML_CalcType adVm) {
  return (unsigned char) (adVm < 1e-6 ? 2 : 1);
}

void Chandler::Init() {
#if KADEBUG
  cerr << "Chandler::Init" << endl;
#endif // if KADEBUG
  Na_i = vCh(VT_Init_Na_i);
  K_i = vCh(VT_Init_K_i);
  m = vCh(VT_Init_m);
  oa = vCh(VT_Init_oa);
  ua = vCh(VT_Init_ua);
  d = vCh(VT_Init_d);
  f_Ca = vCh(VT_Init_f_Ca);
  Ca_i = vCh(VT_Init_Ca_i);
  Ca_up = vCh(VT_Init_Ca_up);
  Ca_rel = vCh(VT_Init_Ca_rel);
  h = vCh(VT_Init_h);
  j = vCh(VT_Init_j);
  oi = vCh(VT_Init_oi);
  ui = vCh(VT_Init_ui);
  Xr = vCh(VT_Init_Xr);
  Xrhet = vCh(VT_Init_Xrhet);
  Xs = vCh(VT_Init_Xs);
  f = vCh(VT_Init_f);
  u = vCh(VT_Init_u);
  v = vCh(VT_Init_v);
  w = vCh(VT_Init_w);
  dT = vCh(VT_Init_dT);
  fT = vCh(VT_Init_fT);
  a = vCh(VT_Init_a);
  periph = (vCh(VT_periph) == 1);
} // Chandler::Init

ML_CalcType
Chandler::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
               int euler = 2) {
  tinc *= 1000.0;
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double dNa_i = 1.0 / Na_i;
  const double VmE_Na = V_int - ((vCh(VT_RTdF)) * log((vCh(VT_Na_o)) * dNa_i));
  const double VmE_K = V_int - ((vCh(VT_RTdF)) * log((vCh(VT_K_o)) / K_i));
  const double VmE_Ca = V_int - ((vCh(VT_RTd2F)) * log((vCh(VT_Ca_o)) / (double) Ca_i));
  const double m_m = pCmP->m_m[Vi];
  m = m_m + (m - m_m) * pCmP->exptau_m[Vi];
  checkGatingVariable(m);
  const double m_h = pCmP->m_h[Vi];
  h = m_h + (h - m_h) * pCmP->exptau_h[Vi];
  const double m_j = pCmP->m_j[Vi];
  j = m_j + (j - m_j) * pCmP->exptau_j[Vi];
  const double m_oa = pCmP->m_oa[Vi];
  oa = m_oa + (oa - m_oa) * pCmP->exptau_oa[Vi];
  const double m_oi = pCmP->m_oi[Vi];
  oi = m_oi + (oi - m_oi) * pCmP->exptau_oi[Vi];
  const double m_ua = pCmP->m_ua[Vi];
  ua = m_ua + (ua - m_ua) * pCmP->exptau_ua[Vi];
  const double m_ui = pCmP->m_ui[Vi];
  ui = m_ui + (ui - m_ui) * pCmP->exptau_ui[Vi];
  const double m_Xr = pCmP->m_Xr[Vi];
  Xr = m_Xr + (Xr - m_Xr) * pCmP->exptau_Xr[Vi];
  const double m_Xrhet = pCmP->m_Xrhet[Vi];
  Xrhet = m_Xrhet + (Xrhet - m_Xrhet) * pCmP->exptau_Xrhet[Vi];
  const double m_Xs = pCmP->m_Xs[Vi];
  Xs = m_Xs + (Xs - m_Xs) * pCmP->exptau_Xs[Vi];
  const double m_d = pCmP->m_d[Vi];
  d = m_d + (d - m_d) * pCmP->exptau_d[Vi];
  const double m_f = pCmP->m_f[Vi];
  f = m_f + (f - m_f) * pCmP->exptau_f[Vi];
  const double m_fCa = 1.0 / (1.0 + Ca_i * 2857.1429);
  f_Ca += (1.0 / (1.0 + Ca_i * 2857.1429) - f_Ca) * 0.5 * tinc;
  const double m_dT = pCmP->m_dT[Vi];
  dT = m_dT + (dT - m_dT) * pCmP->exptau_dT[Vi];
  const double m_fT = pCmP->m_fT[Vi];
  fT = m_fT + (fT - m_fT) * pCmP->exptau_fT[Vi];
  const double m_a = pCmP->m_a[Vi];
  a = m_a + (a - m_a) * pCmP->exptau_a[Vi];
  const double I_Na = (vCh(VT_g_Na)) * m * m * m * h * j * VmE_Na;
  const double I_to = (vCh(VT_g_to)) * oa * oa * oa * oi * VmE_K;
  const double I_Kur = pCmP->g_Kur[Vi] * ua * ua * ua * ui * VmE_K;
  const double I_Kr = pCmP->CKr[Vi] * Xr * VmE_K;
  const double I_Krhet = pCmP->CKrhet[Vi] * Xrhet * VmE_K;

  // const double I_Kr = pCmP->CKr[Vi]*Xr*VmE_K*sqrt(vCh(VT_K_o)/5.4); // [mwk] adaption to dialysis
  const double I_Ks = (vCh(VT_g_Ks)) * Xs * Xs * VmE_K;
  const double I_CaL = (vCh(VT_g_CaL)) * d * f * f_Ca * (V_int - (vCh(VT_E_rL)));
  const double I_CaT = (vCh(VT_g_CaT)) * fT * dT * (V_int - (vCh(VT_E_CaT)));
  const double I_f = (vCh(VT_g_f)) * a * (V_int - (vCh(VT_E_f)));
  const double I_K1 = pCmP->CK1[Vi] * VmE_K;

  // const double I_K1 = pCmP->CK1[Vi]*VmE_K*sqrt(vCh(VT_K_o)/5.4); // [mwk] adaption to dialysis
  const double I_bNa = (vCh(VT_g_bNa)) * VmE_Na;
  const double I_bK = (vCh(VT_g_bK)) * VmE_K;
  const double I_bCa = (vCh(VT_g_bCa)) * VmE_Ca;
  const double temp = (vCh(VT_k_mNai)) * dNa_i;
  const double I_NaK = pCmP->CNaK[Vi] / (1.0 + temp * sqrt(temp));
  const double I_pCa = (vCh(VT_I_pCamax)) * Ca_i / (Ca_i + (vCh(VT_k_mpCa)));
  const double I_NaCa =
      pCmP->CNaCa[Vi] * ((Na_i * Na_i * Na_i * (vCh(VT_Ca_o))) - pCmP->expVm[Vi] * Ca_i);
  if (periph == 1) {
    // u, v, w fixed (see code from Henggui Zhang)
    // const double m_w = pCmP->m_w[Vi];
    // w = m_w + (w - m_w) * pCmP->exptau_w[Vi];
    const double I_rel = (vCh(VT_k_rel)) * u * u * v * w * (Ca_rel - Ca_i);
    const double I_up = (vCh(VT_I_upmax)) * Ca_i / (Ca_i + (vCh(VT_k_up)));
    const double I_upleak = (vCh(VT_kupleak)) * Ca_up;
    const double I_tr = (Ca_up - Ca_rel) * (vCh(VT_dt_tr));
    Ca_up += tinc * (I_up - I_tr * (vCh(VT_VreldVup)) - I_upleak);
    Ca_rel += tinc * ((I_tr - I_rel) / (1.0 + (vCh(VT_csqnkm)) /
                                              (Ca_rel * Ca_rel + Ca_rel * (vCh(VT_kmcsqnm2)) +
                                               (vCh(VT_kkmcsqn)))));
    Na_i += tinc * (-3.0 * I_NaK - 3.0 * I_NaCa - I_bNa - I_Na) * (vCh(VT_CmdFvi));
    K_i += tinc * (2.0 * I_NaK - I_K1 - I_to - I_Kur - I_Kr - I_Ks - I_bK) * (vCh(VT_CmdFvi));

    // Adjustements according to (Oosterom & Jacquemet, CinC 2009)
    Na_i = (1 - vCh(VT_alpha)) * Na_i + vCh(VT_alpha) * vCh(VT_Init_Na_i);
    K_i = (1 - vCh(VT_alpha)) * K_i + vCh(VT_alpha) * vCh(VT_Init_K_i);

    Ca_i += tinc *
            ((2.0 * I_NaCa - I_pCa - I_CaL - I_bCa) * .5 * (vCh(VT_CmdFvi)) +
             (I_upleak - I_up) * (vCh(VT_VupdVi)) + I_rel * (vCh(VT_VreldVi))) /
            (1.0 +
             (vCh(VT_trpnkm)) / (Ca_i * Ca_i + Ca_i * (vCh(VT_kmtrpnm2)) + (vCh(VT_kkmtrpn))) +
             (vCh(VT_cmdnkm)) /
             (Ca_i * Ca_i + Ca_i * (vCh(VT_kmcmdnm2)) + (vCh(VT_kkmcmdn))));

    // const double Fn = exp(-((vCh(VT_Vrel))*I_rel-(0.5*I_CaL-0.2*I_NaCa)*(vCh(VT_Cmd2F)))*731.5289);
    // const double m_u = 1.0/(1.0+Fn*pCmP->exp250);
    // u+=(m_u-u)*0.125*tinc;
    // const double m_v=1.0-1.0/(1.0+Fn*pCmP->exp50);
    // const double t_v = 1.91+2.09*m_u;
    // v+=(m_v-v)*tinc/t_v;
  }


  // 2013.07.17 al128 added second Kr current (I_Krhet) for heterozygous mutation model; Xrhet_fact determines weight of
  // I_Kr and I_Krhet
  double I_tot = I_Na + I_CaL + I_pCa + I_K1 + I_to + I_Kur + (1 - vCh(VT_Xrhet_fact)) * (I_Kr) +
                 (vCh(VT_Xrhet_fact)) * (I_Krhet) + I_Ks +
                 I_bNa + I_bK + I_bCa + I_NaK + I_NaCa + I_CaT + I_f - i_external;
  return -.001 * I_tot * tinc;
} // Chandler::Calc

void Chandler::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << Ca_i << ' ' << Ca_up << ' ' << Ca_rel << ' '
          << h << ' ' << j << ' '
          << oi << ' ' << ui << ' '
          << Xr << ' ' << Xrhet << ' ' << Xs << ' '
          << f << ' ' << u << ' ' << v << ' '
          << w << ' ' << Na_i << ' ' << K_i << ' '
          << m << ' ' << oa << ' ' << ua << ' '
          << d << ' ' << f_Ca << ' '
          << dT << ' ' << fT << ' ' << a << ' ';
}

void Chandler::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double dNa_i = 1.0 / Na_i;
  const double VmE_Na = V_int - ((vCh(VT_RTdF)) * log((vCh(VT_Na_o)) * dNa_i));
  const double VmE_K = V_int - ((vCh(VT_RTdF)) * log((vCh(VT_K_o)) / K_i));
  const double VmE_Ca = V_int - ((vCh(VT_RTd2F)) * log((vCh(VT_Ca_o)) / (double) Ca_i));
  const double I_Na = (vCh(VT_g_Na)) * m * m * m * h * j * VmE_Na;
  const double I_to = (vCh(VT_g_to)) * oa * oa * oa * oi * VmE_K;
  const double I_Kur = pCmP->g_Kur[Vi] * ua * ua * ua * ui * VmE_K;
  const double I_Kr = pCmP->CKr[Vi] * Xr * VmE_K;
  const double I_Krhet = pCmP->CKrhet[Vi] * Xrhet * VmE_K;
  const double I_Ks = (vCh(VT_g_Ks)) * Xs * Xs * VmE_K;
  const double I_CaL = (vCh(VT_g_CaL)) * d * f * f_Ca * (V_int - (vCh(VT_E_rL)));
  const double I_CaT = (vCh(VT_g_CaT)) * fT * dT * (V_int - (vCh(VT_E_CaT)));
  const double I_f = (vCh(VT_g_f)) * a * (V_int - (vCh(VT_E_f)));
  const double I_K1 = pCmP->CK1[Vi] * VmE_K;
  const double I_bNa = (vCh(VT_g_bNa)) * VmE_Na;
  const double I_bK = (vCh(VT_g_bK)) * VmE_K;
  const double I_bCa = (vCh(VT_g_bCa)) * VmE_Ca;
  const double temp = (vCh(VT_k_mNai)) * dNa_i;
  const double I_NaK = pCmP->CNaK[Vi] / (1.0 + temp * sqrt(temp));
  const double I_pCa = (vCh(VT_I_pCamax)) * Ca_i / (Ca_i + (vCh(VT_k_mpCa)));
  const double I_NaCa =
      pCmP->CNaCa[Vi] * ((Na_i * Na_i * Na_i * (vCh(VT_Ca_o))) - pCmP->expVm[Vi] * Ca_i);
  const double I_rel = (vCh(VT_k_rel)) * u * u * v * w * (Ca_rel - Ca_i);
  const double I_up = (vCh(VT_I_upmax)) * Ca_i / (Ca_i + (vCh(VT_k_up)));
  const double I_upleak = (vCh(VT_kupleak)) * Ca_up;
  const double I_tr = (Ca_up - Ca_rel) * (vCh(VT_dt_tr));
  const double I_mem =
      I_Na + I_CaL + I_pCa + I_K1 + I_to + I_Kur + (1 - vCh(VT_Xrhet_fact)) * (I_Kr) +
      (vCh(VT_Xrhet_fact)) *
      (I_Krhet) + I_Ks + I_bNa + I_bK + I_bCa + I_NaK + I_NaCa;
  tempstr << I_Na << ' ' << I_bNa << ' '
          << I_K1 << ' ' << I_to << ' '
          << I_Kur << ' ' << I_Kr << ' ' << I_Krhet << ' '
          << I_Ks << ' ' << I_CaL << ' '
          << I_NaK << ' ' << I_NaCa << ' '
          << I_bCa << ' ' << I_pCa << ' '
          << I_rel << ' ' << I_tr << ' '
          << I_up << ' ' << I_upleak << ' '
          << I_CaT << ' ' << I_bK << ' '
          << I_f << ' ' << I_mem << ' ';
} // Chandler::LongPrint

void Chandler::GetParameterNames(vector<string> &getpara) {
  const int numpara = 24;
  const string ParaNames[numpara] =
      {"Ca_i", "Ca_up", "Ca_rel", "h", "j", "o_i", "u_i", "Xr",
       "Xrhet",
       "Xs",
       "f",
       "u",
       "v", "w", "Na_i", "K_i", "m", "o_a", "u_a", "d",
       "f_Ca",
       "dT",
       "fT",
       "a"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void Chandler::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 21;
  const string ParaNames[numpara] =
      {"I_Na", "I_bNa", "I_K1", "I_to", "I_Kur", "I_Kr", "I_Krhet", "I_Ks", "I_CaL", "I_NaK",
       "I_NaCa",
       "I_bCa",
       "I_pCa",
       "I_rel", "I_tr", "I_up", "I_upleak", "I_CaT", "I_bK", "I_f", "I_mem"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
