/*      File: PanditEtAl.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

// typical parameters for ElphyModelTest
// tinc 1e-6
// textlen 5e-3
// textbegin 0.02

#ifndef PANDITETAL
#define PANDITETAL

#include <PanditEtAlParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_PanditEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pPeaP->P[NS_PanditEtAlParameters::a].value
#endif // ifdef HETERO

class PanditEtAl : public vbElphyModel<ML_CalcType> {
public:
  PanditEtAlParameters *pPeaP;
  ML_CalcType m, h, j, d, f11, f12, Ca_inact, r, s, s_slow, r_ss, s_ss, r_K_slow, s_K_slow, y;
  ML_CalcType Na_i, K_i, Ca_i, Ca_NSR, Ca_SS, Ca_JSR;
  ML_CalcType PC1, PO1, PO2, PC2;
  ML_CalcType ltrpn, htrpn;
#if ChR2
  ML_CalcType ChRC1, ChRC2, ChRO1, ChRO2, ChRp;
#endif // if ChR2
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  PanditEtAl(PanditEtAlParameters *pp);

  ~PanditEtAl();

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_V_cell); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp);  /*.6*/ }

  virtual inline ML_CalcType GetStimTime() { return 0.005; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_Vm); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &m; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual void Init();

  virtual ML_CalcType
  Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch, int euler);

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class PanditEtAl
#endif // ifndef PANDITETAL
