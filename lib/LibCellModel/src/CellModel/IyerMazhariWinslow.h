/*! \file IyerMazhariWinslow.h
   \brief Implementation of electrophysiological model for describing human ventricular myocytes
   modified/corrected from Iyer et al., Biophys J 2004

   \author fs, CVRTI - University of Utah, USA
 */

// Definition of TIMOTHY, TIMOTHY2, SPONTANEOUSRELEASE and IKSOPEN --> IyerMazhariWinslowParameters.h

// typical parameters for ElphyModelTest
// tinc 1e-6
// amplitude 110
// textlen 5e-4
// textbegin 0.01

#ifndef IYER_MAZHARI_WINSLOW_H
#define IYER_MAZHARI_WINSLOW_H

#include <IyerMazhariWinslowParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_IyerMazhariWinslowParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pIMW->P[NS_IyerMazhariWinslowParameters::a].value
#endif // ifdef HETERO

class IyerMazhariWinslow : public vbElphyModel<ML_CalcType> {
public:
  double Nai;
  double Ki;
  double Cai;
  double CaNSR;
  double CaSS;
  double CaJSR;
  double PC1;
  double PO1;
  double PC2;
  double PO2;
  double C0L;
  double C1L;
  double C2L;
  double C3L;
  double C4L;
  double OL;
  double CCa0L;
  double CCa1L;
  double CCa2L;
  double CCa3L;
  double CCa4L;
  double y;
#ifdef TIMOTHY
  double ytimothy;
#endif // ifdef TIMOTHY
#ifdef TIMOTHY2
  double C0Ltimothy;
  double C1Ltimothy;
  double C2Ltimothy;
  double C3Ltimothy;
  double C4Ltimothy;
  double OLtimothy;
  double CCa0Ltimothy;
  double CCa1Ltimothy;
  double CCa2Ltimothy;
  double CCa3Ltimothy;
  double CCa4Ltimothy;
  double ytimothy;
#endif // ifdef TIMOTHY2
  double HTRPNCa;
  double LTRPNCa;

  double C0Kvf;
  double C1Kvf;
  double C2Kvf;
  double C3Kvf;
  double OKvf;
  double CI0Kvf;
  double CI1Kvf;
  double CI2Kvf;
  double CI3Kvf;
  double OIKvf;

  double C0Kvs;
  double C1Kvs;
  double C2Kvs;
  double C3Kvs;
  double OKvs;
  double CI0Kvs;
  double CI1Kvs;
  double CI2Kvs;
  double CI3Kvs;
  double OIKvs;

  double C1Kr;
  double C2Kr;
  double C3Kr;
  double OKr;
  double IKr;

  double C0Ks;
  double C1Ks;
  double O1Ks;
  double O2Ks;

  double C0Na;
  double C1Na;
  double C2Na;
  double C3Na;
  double C4Na;
  double O1Na;
  double O2Na;
  double CI0Na;
  double CI1Na;
  double CI2Na;
  double CI3Na;
  double CI4Na;
  double INa;

  IyerMazhariWinslowParameters *pIMW;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  IyerMazhariWinslow(IyerMazhariWinslowParameters *);

  ~IyerMazhariWinslow() {}

  virtual void Init();

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vmyo); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return -.09066; }

  virtual inline ML_CalcType GetCai() { return Cai; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Cao); }

  virtual inline ML_CalcType GetNai() { return Nai; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Nao); }

  virtual inline ML_CalcType GetKi() { return Ki; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_Ko); }

  virtual inline int GetSize(void) {
    return sizeof(IyerMazhariWinslow) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(IyerMazhariWinslowParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return (ML_CalcType *) &Nai; }

  virtual inline void SetCai(ML_CalcType val) { Cai = val; }

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);
}; // class IyerMazhariWinslow


#endif // ifndef IYER_MAZHARI_WINSLOW_H
