/**@file Rice5Parameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef RICE5_PARAMETERS
#define RICE5_PARAMETERS

#include <ParameterLoader.h>

namespace NS_Rice5Parameters {
  enum varType {
    VT_P1a = vtFirst,
    VT_P0a,
    VT_N1a,
    VT_P2a,
    VT_P3a,
    VT_TCaa,
    VT_k_m1,
    VT_f,
    VT_k_on,
    VT_k_off,
    VT_K_Ca,

    VT_g_10,
    VT_g_21,
    VT_g_32,
    VT_f_01,
    VT_f_12,
    VT_f_23,
    VT_N_1,
    VT_N_2,
    VT_gewForce,
    VT_TCaMax,
    VT_dFmax,
    VT_g_stern,

    VT_mfakt1,
    VT_mfakt2,
    VT_mfakt3,
    vtLast
  };
} // namespace NS_Rice5Parameters

using namespace NS_Rice5Parameters;

class Rice5Parameters : public vbNewForceParameters {
public:
  Rice5Parameters();

  Rice5Parameters(const char *, ForceModelType);

  virtual ~Rice5Parameters() {}

  // virtual inline int GetSize(void) { return (&mfakt3-&P1a)*sizeof(T); };
  // virtual inline T* GetBase(void) { return P1a; };
  // virtual int GetNumParameters(void)=0;

  void Init(const char *, ForceModelType);

  void PrintParameters();

  void Calculate();
};

class Rice5_1Parameters : public Rice5Parameters {
public:
  Rice5_1Parameters(const char *initFile) {
    this->Init(initFile, FMT_Rice5_1);
  }

  ~Rice5_1Parameters() {}

  // virtual int GetNumParameters() { return 15; };
};

class Rice5_2Parameters : public Rice5Parameters {
public:
  Rice5_2Parameters(const char *initFile) {
    this->Init(initFile, FMT_Rice5_2);
  }

  ~Rice5_2Parameters() {}

  // virtual int GetNumParameters() { return 18; };
};

class Rice5_3Parameters : public Rice5Parameters {
public:
  Rice5_3Parameters(const char *initFile) {
    this->Init(initFile, FMT_Rice5_3);
  }

  ~Rice5_3Parameters() {}

  // virtual int GetNumParameters() { return 21; };
};

#endif // ifndef RICE5_PARAMETERS
