/**@file NobleEtAl.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef NOBLEETAL_H
#define NOBLEETAL_H

#include <NobleEtAlParameters.h>

/*
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * K_i wurde fixiert, da es kontinuierlich abfaellt.
 * Es muessen noch Wege (Stroeme) gefunden werden, um diesen
 * Effekt abzustellen.
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_NobleEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pNoeaP->P[NS_NobleEtAlParameters::a].value
#endif // ifdef HETERO


class NobleEtAl : public vbElphyModel<ML_CalcType> {
public:
  NobleEtAlParameters *pNoeaP;

  ML_CalcType Ca_i, K_o;

  ML_CalcType f, f2, s, f2ds, x_r1, x_r2, x_s;
  ML_CalcType f_activator, f_product;

  ML_CalcType Ca_ds, Ca_up, Ca_rel;
  ML_CalcType Ca_calmod, Ca_troponin;  // , Ca_indicator;

  ML_CalcType Na_i, K_i;
  ML_CalcType m, h, d, r;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  NobleEtAl() {}

  ~NobleEtAl() {}

  virtual void Init();

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vol); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_init_Vm); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return K_o; }

  virtual inline int GetSize() = 0;

  virtual inline ML_CalcType *GetBase() = 0;

  virtual inline ML_CalcType GetT_isometric(ML_CalcType tinc, ML_CalcType stretch) { return 0.; }

  virtual inline ML_CalcType
  GetI_stretch(ML_CalcType VmE_Ca, ML_CalcType VmE_Na, ML_CalcType VmE_K, ML_CalcType V_int,
               ML_CalcType stretch) { return 0.; }

  virtual inline ML_CalcType GetSRLeak(ML_CalcType stretch) { return .05; }

  virtual inline ML_CalcType GetKTrop(ML_CalcType stretch) { return CELLMODEL_PARAMVALUE(VT_a_troponin); }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline void SetKo(ML_CalcType val) { K_o = val; }

  virtual inline ML_CalcType GetTCa() { return Ca_troponin; }

  virtual inline bool OutIsTCa() { return true; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char) (adVm < .5e-6 ? 5 : (adVm < 1e-6 ? 4 : (adVm < 2e-6 ? 3 : (adVm < 4e-6
                                                                                      ? 2 : 1))));
  }

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);

  virtual bool AddHeteroValue(string, double);
}; // class NobleEtAl


class NobleSingleCell : public NobleEtAl {
public:
  NobleSingleCell(NobleEtAlParameters *pp) {
    this->pNoeaP = pp;
#ifdef HETERO
    PS = new ParameterSwitch(pNoeaP, NS_NobleEtAlParameters::vtLast);
#endif // ifdef HETERO
    this->Init();
  }

  ~NobleSingleCell() {}

  virtual inline int GetSize(void) {
    return sizeof(NobleSingleCell) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(NobleEtAlParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return &this->Ca_i; }
};


class NobleStretchSimple : public NobleEtAl {
public:
  NobleStretchSimple(NobleEtAlParameters *pp) {
    this->pNoeaP = pp;
#ifdef HETERO
    PS = new ParameterSwitch(pNoeaP, NS_NobleEtAlParameters::vtLast);
#endif // ifdef HETERO
    this->Init();
  }

  ~NobleStretchSimple() {}

  virtual inline int GetSize(void) {
    return sizeof(NobleStretchSimple) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(NobleEtAlParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return &this->Ca_i; }

  virtual inline ML_CalcType
  GetI_stretch(ML_CalcType VmE_Ca, ML_CalcType VmE_Na, ML_CalcType VmE_K, ML_CalcType V_int,
               ML_CalcType stretch) {
    return ((CELLMODEL_PARAMVALUE(VT_G_ANstretch)) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_ANstretch1k5))) +
            (CELLMODEL_PARAMVALUE(VT_G_NSstretch)) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_NSstretch)))) /
           (1.0 + exp(-(CELLMODEL_PARAMVALUE(VT_SACSL2)) * (stretch * (CELLMODEL_PARAMVALUE(VT_SL)) - (CELLMODEL_PARAMVALUE(VT_SLHST)))));
  }
}; // class NobleStretchSimple

class NobleStretch : public NobleEtAl {
public:
  NobleStretch(NobleEtAlParameters *pp) {
    this->pNoeaP = pp;
#ifdef HETERO
    PS = new ParameterSwitch(pNoeaP, NS_NobleEtAlParameters::vtLast);
#endif // ifdef HETERO
    this->Init();
  }

  ~NobleStretch() {}

  virtual inline int GetSize(void) {
    return sizeof(NobleStretch) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(NobleEtAlParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return &this->Ca_i; }

  virtual inline ML_CalcType GetT_isometric(ML_CalcType tinc, ML_CalcType stretch) { return .0; }

  virtual inline ML_CalcType
  GetI_stretch(ML_CalcType VmE_Ca, ML_CalcType VmE_Na, ML_CalcType VmE_K, ML_CalcType V_int,
               ML_CalcType stretch) {
    return ((CELLMODEL_PARAMVALUE(VT_G_ANstretch)) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_ANstretch))) + (CELLMODEL_PARAMVALUE(VT_G_Kstretch)) * VmE_K +
            (CELLMODEL_PARAMVALUE(VT_G_Nastretch)) * VmE_Na +
            (CELLMODEL_PARAMVALUE(VT_G_Castretch)) * VmE_Ca) /
           (1.0 + exp(-(CELLMODEL_PARAMVALUE(VT_SACSL2)) * ((CELLMODEL_PARAMVALUE(VT_SL)) * stretch - (CELLMODEL_PARAMVALUE(VT_SLHST)))));
  }

  virtual inline ML_CalcType GetSRLeak(ML_CalcType stretch) { return .05 * exp((CELLMODEL_PARAMVALUE(VT_SR_SL)) *
                                                                               (CELLMODEL_PARAMVALUE(VT_SL)) *
                                                                               stretch);
  }

  virtual inline ML_CalcType GetKTrop(ML_CalcType stretch) {
    return (CELLMODEL_PARAMVALUE(VT_a_troponin)) * exp((CELLMODEL_PARAMVALUE(VT_Trop_SL)) * (CELLMODEL_PARAMVALUE(VT_SL)) * stretch);
  }
}; // class NobleStretch


class NobleStretchComplex : public NobleEtAl {
public:
  NobleStretchComplex(NobleEtAlParameters *pp) {
    this->pNoeaP = pp;
    Init();
  }

  ~NobleStretchComplex() {}

  ML_CalcType f_LightChain, f_CrossBridge;

  virtual void Init() {
#if KADEBUG
    cerr << "NobleStretchComplex::Init" << endl;
#endif // if KADEBUG
    NobleEtAl::Init();
    f_LightChain = CELLMODEL_PARAMVALUE(VT_init_Stretch_Complex_f_LightChain); // .0000332;
    f_CrossBridge = CELLMODEL_PARAMVALUE(VT_init_Stretch_Complex_f_CrossBridge);  // .0000809;
  }

  virtual inline int GetSize(void) {
    return sizeof(NobleStretchComplex) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(NobleEtAlParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return &this->Ca_i; }

  virtual inline ML_CalcType GetT_isometric(ML_CalcType tinc, ML_CalcType stretch) {
    f_LightChain += tinc *
                    ((CELLMODEL_PARAMVALUE(VT_KCalTrop)) * this->Ca_calmod * this->Ca_calmod * this->Ca_troponin *
                     (1.0 - f_LightChain) - (CELLMODEL_PARAMVALUE(VT_k_cont2)) *
                                            f_LightChain);
    f_CrossBridge += tinc * ((CELLMODEL_PARAMVALUE(VT_k_cont3)) * f_LightChain * (1.0 - f_CrossBridge) -
                             (CELLMODEL_PARAMVALUE(VT_k_cont4)) * f_CrossBridge);

    const double SarcomereLength = stretch * (CELLMODEL_PARAMVALUE(VT_SL));
    const double T_act = (SarcomereLength > 1.0 ? 1.0 - exp(-3.0 * (SarcomereLength - 1.0)) : .0);
    const double Overlap = (SarcomereLength > 2.0 ? 1.0 - 0.625 * (SarcomereLength - 2.0) : 1.0);
    return f_CrossBridge * (CELLMODEL_PARAMVALUE(VT_CBden)) * Overlap * T_act +
           .0002 * exp(SarcomereLength + SarcomereLength);
  }

  virtual inline ML_CalcType
  GetI_stretch(ML_CalcType VmE_Ca, ML_CalcType VmE_Na, ML_CalcType VmE_K, ML_CalcType V_int,
               ML_CalcType stretch) {
    return ((CELLMODEL_PARAMVALUE(VT_G_ANstretch)) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_ANstretch))) + (CELLMODEL_PARAMVALUE(VT_G_Kstretch)) * VmE_K +
            (CELLMODEL_PARAMVALUE(VT_G_Nastretch)) * VmE_Na +
            (CELLMODEL_PARAMVALUE(VT_G_Castretch)) * VmE_Ca) /
           (1.0 + exp(-(CELLMODEL_PARAMVALUE(VT_SACSL2)) * (stretch * (CELLMODEL_PARAMVALUE(VT_SL)) - (CELLMODEL_PARAMVALUE(VT_SLHST)))));
  }

  virtual void Print(ostream &tempstr, double t, ML_CalcType V) {
    NobleEtAl::Print(tempstr, t, V);
    tempstr << f_LightChain << ' ' << f_CrossBridge << ' ';
  }

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V) {
    NobleEtAl::LongPrint(tempstr, t, V);
    tempstr << f_LightChain << ' ' << f_CrossBridge << ' ';
  }

  virtual void GetParameterNames(vector<string> &getpara) {
    NobleEtAl::GetParameterNames(getpara);
    getpara.push_back("f_LightChain");
    getpara.push_back("f_CrossBridge");
  }

  virtual void GetLongParameterNames(vector<string> &getpara) {
    NobleEtAl::GetLongParameterNames(getpara);
    getpara.push_back("f_LightChain");
    getpara.push_back("f_CrossBridge");
  }
}; // class NobleStretchComplex

#endif // ifndef NOBLEETAL_H
