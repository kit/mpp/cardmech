/**@file IyerMazhariWinslowK1Parameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef IYER_MAZHARI_WINSLOW_K1_PARAMETERS
#define IYER_MAZHARI_WINSLOW_K1_PARAMETERS

#include <ParameterLoader.h>

namespace NS_IyerMazhariWinslowK1Parameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_Acap,
    VT_Vmyo,

    VT_Ko,
    VT_Ki,

    VT_GK1,
    VT_P1K1,
    VT_P2K1,
    VT_Amp,
    vtLast
  };
}

using namespace NS_IyerMazhariWinslowK1Parameters;

class IyerMazhariWinslowK1Parameters : public vbNewElphyParameters {
public:
  IyerMazhariWinslowK1Parameters(const char *);

  ~IyerMazhariWinslowK1Parameters() {}

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&P2K1-&Tx+1)*sizeof(T);};
  // virtual inline T* GetBase(void){return Tx;};
  // virtual int GetNumParameters() { return 8; };
  void Init(const char *);

  void InitTable() {}

  void Calculate();
};

#endif // ifndef IYER_MAZHARI_WINSLOW_K1_PARAMETERS
