/**@file IyerMazhariWinslowK1.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowK1.h>

IyerMazhariWinslowK1::IyerMazhariWinslowK1(IyerMazhariWinslowK1Parameters *pIMWArg) {
  pIMW = pIMWArg;
#ifdef HETERO
  PS = new ParameterSwitch(pIMW, NS_IyerMazhariWinslowK1Parameters::vtLast);
#endif // ifdef HETERO
  Init();
}

void IyerMazhariWinslowK1::Init() {
  Ki = CELLMODEL_PARAMVALUE(VT_Ki);
}

void IyerMazhariWinslowK1::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' '  // 1
          << V << ' '
          << Ki << ' ';
}

void IyerMazhariWinslowK1::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);

  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);

  const double V_int = V * 1000.0;
  const double RTdF = ElphyModelConstants::R * Tx / ElphyModelConstants::F;
  const double FdRT = 1. / RTdF;

  const double Ko = CELLMODEL_PARAMVALUE(VT_Ko);

  const double EK = RTdF * log(Ko / Ki); // -92 mV for resting 1 Hz

  const double K1 = 1. / (CELLMODEL_PARAMVALUE(VT_P1K1) + exp(CELLMODEL_PARAMVALUE(VT_P2K1) * FdRT * (V_int - EK)));
  const double I_K1 = CELLMODEL_PARAMVALUE(VT_GK1) * K1 * sqrt(Ko) * (V_int - EK);

  tempstr << ' ' << I_K1;
}

void IyerMazhariWinslowK1::GetParameterNames(vector<string> &getpara) {
  const int numpara = 1;
  const string ParaNames[numpara] = {"Ki"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void IyerMazhariWinslowK1::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 1;
  const string ParaNames[numpara] = {"I_K1"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType IyerMazhariWinslowK1::Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim = .0,
                                       ML_CalcType stretch = 1.,
                                       int euler = 1) {
  /*    const double Tx=CELLMODEL_PARAMVALUE(VT_Tx); */
  /*     const double Acap=CELLMODEL_PARAMVALUE(VT_Acap); */
  /*     const double Vmyo=CELLMODEL_PARAMVALUE(VT_Vmyo); */

  /*     tinc*=1000.0; */
  /*     const double V_int=V*1000.0; */
  /*     const double RTdF=R*Tx/F; */
  /*     const double FdRT=1./RTdF; */

  /*     const double Ko=CELLMODEL_PARAMVALUE(VT_Ko); */

  /*     const double EK=RTdF*log(Ko/Ki); */

  /*     const double K1=1./(CELLMODEL_PARAMVALUE(VT_P1K1)+exp(CELLMODEL_PARAMVALUE(VT_P2K1)*FdRT*(V_int-EK))); */
  /*     double I_K1=CELLMODEL_PARAMVALUE(VT_GK1)*K1*sqrt(Ko)*(V_int-EK); */

  /*     return -tinc*(I_Ks-I_Stim); */

  return 0.;
}
