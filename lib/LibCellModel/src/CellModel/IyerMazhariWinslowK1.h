/*! \file IyerMazhariWinslowK1.h
   \brief Implementation of I_K1 model from Iyer et al., Biophys J 2004

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef IYER_MAZHARI_WINSLOW_K1_H
#define IYER_MAZHARI_WINSLOW_K1_H

#include <IyerMazhariWinslowK1Parameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_IyerMazhariWinslowK1Parameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pIMW->P[NS_IyerMazhariWinslowK1Parameters::a].value
#endif // ifdef HETERO

class IyerMazhariWinslowK1 : public vbElphyModel<ML_CalcType> {
public:
  double Ki;

  IyerMazhariWinslowK1Parameters *pIMW;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  IyerMazhariWinslowK1(IyerMazhariWinslowK1Parameters *);

  ~IyerMazhariWinslowK1() {}

  virtual void Init();

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vmyo); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.; }

  virtual inline ML_CalcType GetVm() { return -.09066; }

  virtual inline ML_CalcType GetCai() { return 0; }

  virtual inline ML_CalcType GetCao() { return 0; }

  virtual inline ML_CalcType GetNai() { return 0; }

  virtual inline ML_CalcType GetNao() { return 0; }

  virtual inline ML_CalcType GetKi() { return Ki; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_Ko); }

  virtual inline int GetSize(void) {
    return sizeof(IyerMazhariWinslowK1) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(IyerMazhariWinslowK1Parameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return (ML_CalcType *) &Ki; }

  virtual inline void SetCai(ML_CalcType val) {}

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);
}; // class IyerMazhariWinslowK1


#endif // ifndef IYER_MAZHARI_WINSLOW_K1_H
