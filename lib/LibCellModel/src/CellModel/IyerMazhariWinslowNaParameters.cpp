/**@file IyerMazhariWinslowNaParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowNaParameters.h>

IyerMazhariWinslowNaParameters::IyerMazhariWinslowNaParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void IyerMazhariWinslowNaParameters::PrintParameters() {
  cout << "#IyerMazhariWinslowNaParameter:\n";

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void IyerMazhariWinslowNaParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "IyerMazhariWinslowNaParameters:init " << initFile << endl;
#endif // if KADEBUG
  P[VT_Tx].name = "Tx";
  P[VT_Nao].name = "Nao";
  P[VT_Nai].name = "Nai";

  P[VT_C0Na].name = "C0Na";
  P[VT_C1Na].name = "C1Na";
  P[VT_C2Na].name = "C2Na";
  P[VT_C3Na].name = "C3Na";
  P[VT_C4Na].name = "C4Na";
  P[VT_O1Na].name = "O1Na";
  P[VT_O2Na].name = "O2Na";
  P[VT_CI0Na].name = "CI0Na";
  P[VT_CI1Na].name = "CI1Na";
  P[VT_CI2Na].name = "CI2Na";
  P[VT_CI3Na].name = "CI3Na";
  P[VT_CI4Na].name = "CI4Na";
  P[VT_INa].name = "INa";

  P[VT_GNa].name = "GNa";

  P[VT_Naa1].name = "Naa1";
  P[VT_Naa2].name = "Naa2";
  P[VT_Naa3].name = "Naa3";
  P[VT_Nab1].name = "Nab1";
  P[VT_Nab2].name = "Nab2";
  P[VT_Nab3].name = "Nab3";
  P[VT_Nag1].name = "Nag1";
  P[VT_Nag2].name = "Nag2";
  P[VT_Nag3].name = "Nag3";
  P[VT_Nad1].name = "Nad1";
  P[VT_Nad2].name = "Nad2";
  P[VT_Nad3].name = "Nad3";
  P[VT_NaOn1].name = "NaOn1";
  P[VT_NaOn2].name = "NaOn2";
  P[VT_NaOn3].name = "NaOn3";
  P[VT_NaOf1].name = "NaOf1";
  P[VT_NaOf2].name = "NaOf2";
  P[VT_NaOf3].name = "NaOf3";
  P[VT_Nagg1].name = "Nagg1";
  P[VT_Nagg2].name = "Nagg2";
  P[VT_Nagg3].name = "Nagg3";
  P[VT_Nadd1].name = "Nadd1";
  P[VT_Nadd2].name = "Nadd2";
  P[VT_Nadd3].name = "Nadd3";
  P[VT_Nae1].name = "Nae1";
  P[VT_Nae2].name = "Nae2";
  P[VT_Nae3].name = "Nae3";
  P[VT_NaO1].name = "NaO1";
  P[VT_NaO2].name = "NaO2";
  P[VT_NaO3].name = "NaO3";
  P[VT_Naeta1].name = "Naeta1";
  P[VT_Naeta2].name = "Naeta2";
  P[VT_Naeta3].name = "Naeta3";
  P[VT_Nanu1].name = "Nanu1";
  P[VT_Nanu2].name = "Nanu2";
  P[VT_Nanu3].name = "Nanu3";
  P[VT_NaCn1].name = "NaCn1";
  P[VT_NaCn2].name = "NaCn2";
  P[VT_NaCn3].name = "NaCn3";
  P[VT_NaCf1].name = "NaCf1";
  P[VT_NaCf2].name = "NaCf2";
  P[VT_NaCf3].name = "NaCf3";
  P[VT_NaScalinga].name = "NaScalinga";
  P[VT_NaQ].name = "NaQ";
  P[VT_Amp].name = "Amp";

  ParameterLoader EPL(initFile, EMT_IyerMazhariWinslowNa);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
} // IyerMazhariWinslowNaParameters::Init

void IyerMazhariWinslowNaParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
}
