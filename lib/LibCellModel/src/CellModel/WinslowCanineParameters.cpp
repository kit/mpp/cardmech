/**@file WinslowCanineParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <WinslowCanineParameters.h>

WinslowCanineParameters::WinslowCanineParameters(const char *initFile, ML_CalcType tinc) {
  // cerr << "ATTENTION: the implemention of the parameters is not finished!!!" << endl << "the values are HARDCODED" <<
  // endl;
#if KADEBUG
  cerr << "WinslowCanineParameters::WinslowCanineParameters "<< initFile << endl;
#endif // if KADEBUG
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

void WinslowCanineParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "WinslowCanineParameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void WinslowCanineParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "Loading the WinslowCanine parameter from " << initFile << " ...\n";
#endif // if KADEBUG

  // Initialization of parameter names
  P[VT_F].name = "F";
  P[VT_T].name = "T";
  P[VT_R].name = "R";
  P[VT_RTdF].name = "RTdF";
  P[VT_RTd2F].name = "RTd2F";
  P[VT_FdRT].name = "FdRT";
  P[VT_FdRTm1k5].name = "FdRTm1k5";

  P[VT_Acap].name = "Acap";
  P[VT_Vmyo].name = "Vmyo";
  P[VT_Vol].name = "Vol";
  P[VT_VJSR].name = "VJSR";
  P[VT_VNSR].name = "VNSR";
  P[VT_VSS].name = "VSS";
  P[VT_VmyodVNSR].name = "VmyodVNSR";
  P[VT_VJSRdVNSR].name = "VJSRdVNSR";
  P[VT_VJSRdVSS].name = "VJSRdVSS";
  P[VT_VmyodVSS].name = "VmyodVSS";
  P[VT_AcapdVmyo].name = "AcapdVmyo";
  P[VT_Acapd2Vmyo].name = "Acapd2Vmyo";
  P[VT_AcapDVSS].name = "AcapDVSS";

  P[VT_K_o].name = "K_o";
  P[VT_Na_o].name = "Na_o";
  P[VT_Na_oK].name = "Na_oK";
  P[VT_Ca_o].name = "Ca_o";
  P[VT_sigma].name = "sigma";
  P[VT_NNNO].name = "NNNO";
  P[VT_GKr].name = "GKr";
  P[VT_GKs].name = "GKs";
  P[VT_GK1].name = "GK1";
  P[VT_GKp].name = "GKp";
  P[VT_GNa].name = "GNa";
  P[VT_kNaCa].name = "kNaCa";
  P[VT_KmNa].name = "KmNa";
  P[VT_KkNaCa].name = "KkNaCa";
  P[VT_KmCa].name = "KmCa";
  P[VT_KmK1].name = "KmK1";
  P[VT_ksat].name = "ksat";
  P[VT_eta].name = "eta";
  P[VT_etam1].name = "etam1";
  P[VT_INaKmax].name = "INaKmax";
  P[VT_KmNai].name = "KmNai";
  P[VT_KmKo].name = "KmKo";
  P[VT_IpCamax].name = "IpCamax";
  P[VT_KmpCa].name = "KmpCa";
  P[VT_GCab].name = "GCab";
  P[VT_GNab].name = "GNab";
  P[VT_KI_K1].name = "KI_K1";

  P[VT_v1].name = "v1";
  P[VT_Kfb].name = "Kfb";
  P[VT_dKfb].name = "dKfb";
  P[VT_Krb].name = "Krb";
  P[VT_dKrb].name = "dKrb";
  P[VT_KSR].name = "KSR";
  P[VT_Nfb].name = "Nfb";
  P[VT_Nrb].name = "Nrb";
  P[VT_vmaxf].name = "vmaxf";
  P[VT_vmaxr].name = "vmaxr";
  P[VT_tautr].name = "tautr";
  P[VT_dtautr].name = "dtautr";
  P[VT_tauxfer].name = "tauxfer";
  P[VT_dtauxfer].name = "dtauxfer";
  P[VT_kaplus].name = "kaplus";
  P[VT_kaminus].name = "kaminus";
  P[VT_kbplus].name = "kbplus";
  P[VT_kbminus].name = "kbminus";
  P[VT_kcplus].name = "kcplus";
  P[VT_kcminus].name = "kcminus";
  P[VT_ncoop].name = "ncoop";
  P[VT_mcoop].name = "mcoop";

  P[VT_fL].name = "fL";
  P[VT_gL].name = "gL";
  P[VT_bL].name = "bL";
  P[VT_dbL].name = "dbL";
  P[VT_aL].name = "aL";
  P[VT_omega].name = "omega";
  P[VT_Pscale].name = "Pscale";
  P[VT_PCa].name = "PCa";
  P[VT_PK].name = "PK";
  P[VT_ICahalf].name = "ICahalf";
  P[VT_dICahalf].name = "dICahalf";

  P[VT_alphaa0Kv43].name = "alphaa0Kv43";
  P[VT_aaKv43].name = "aaKv43";
  P[VT_betaa0Kv43].name = "betaa0Kv43";
  P[VT_baKv43].name = "baKv43";

  P[VT_alphai0Kv43].name = "alphai0Kv43";
  P[VT_aiKv43].name = "aiKv43";
  P[VT_betai0Kv43].name = "betai0Kv43";
  P[VT_biKv43].name = "biKv43";
  P[VT_alphaa0Kv14].name = "alphaa0Kv14";
  P[VT_aaKv14].name = "aaKv14";
  P[VT_betaa0Kv14].name = "betaa0Kv14";
  P[VT_baKv14].name = "baKv14";
  P[VT_alphai0Kv14].name = "alphai0Kv14";
  P[VT_betai0Kv14].name = "betai0Kv14";

  P[VT_f1Kv43].name = "f1Kv43";
  P[VT_df1Kv43].name = "df1Kv43";
  P[VT_f2Kv43].name = "f2Kv43";
  P[VT_df2Kv43].name = "df2Kv43";
  P[VT_f3Kv43].name = "f3Kv43";
  P[VT_df3Kv43].name = "df3Kv43";
  P[VT_f4Kv43].name = "f4Kv43";
  P[VT_df4Kv43].name = "df4Kv43";
  P[VT_b1Kv43].name = "b1Kv43";
  P[VT_db1Kv43].name = "db1Kv43";
  P[VT_b2Kv43].name = "b2Kv43";
  P[VT_db2Kv43].name = "db2Kv43";
  P[VT_b3Kv43].name = "b3Kv43";
  P[VT_db3Kv43].name = "db3Kv43";
  P[VT_b4Kv43].name = "b4Kv43";
  P[VT_db4Kv43].name = "db4Kv43";
  P[VT_f1Kv14].name = "f1Kv14";
  P[VT_df1Kv14].name = "df1Kv14";
  P[VT_f2Kv14].name = "f2Kv14";
  P[VT_df2Kv14].name = "df2Kv14";
  P[VT_f3Kv14].name = "f3Kv14";
  P[VT_df3Kv14].name = "df3Kv14";
  P[VT_f4Kv14].name = "f4Kv14";
  P[VT_df4Kv14].name = "df4Kv14";
  P[VT_b1Kv14].name = "b1Kv14";
  P[VT_db1Kv14].name = "db1Kv14";
  P[VT_b2Kv14].name = "b2Kv14";
  P[VT_db2Kv14].name = "db2Kv14";
  P[VT_b3Kv14].name = "b3Kv14";
  P[VT_db3Kv14].name = "db3Kv14";
  P[VT_b4Kv14].name = "b4Kv14";
  P[VT_db4Kv14].name = "db4Kv14";

  P[VT_C0Kv14_to_CI0Kv14].name = "C0Kv14_to_CI0Kv14";
  P[VT_C1Kv14_to_CI1Kv14].name = "C1Kv14_to_CI1Kv14";
  P[VT_C2Kv14_to_CI2Kv14].name = "C2Kv14_to_CI2Kv14";
  P[VT_C3Kv14_to_CI3Kv14].name = "C3Kv14_to_CI3Kv14";
  P[VT_OKv14_to_OIKv14].name = "OKv14_to_OIKv14  ";
  P[VT_CI0Kv14_to_C0Kv14].name = "CI0Kv14_to_C0Kv14";
  P[VT_CI1Kv14_to_C1Kv14].name = "CI1Kv14_to_C1Kv14";
  P[VT_CI2Kv14_to_C2Kv14].name = "CI2Kv14_to_C2Kv14";
  P[VT_CI3Kv14_to_C3Kv14].name = "CI3Kv14_to_C3Kv14";
  P[VT_OIKv14_to_OKv14].name = "OIKv14_to_OKv14  ";

  P[VT_kb2Kv43].name = "kb2Kv43";
  P[VT_kb3Kv43].name = "kb3Kv43";
  P[VT_kb4Kv43].name = "kb4Kv43";
  P[VT_kf1Kv43].name = "kf1Kv43";
  P[VT_kf2Kv43].name = "kf2Kv43";
  P[VT_kf3Kv43].name = "kf3Kv43";
  P[VT_kb2Kv14].name = "kb2Kv14";
  P[VT_kb3Kv14].name = "kb3Kv14";
  P[VT_kb4Kv14].name = "kb4Kv14";
  P[VT_kf1Kv14].name = "kf1Kv14";
  P[VT_kf2Kv14].name = "kf2Kv14";
  P[VT_kf3Kv14].name = "kf3Kv14";

  P[VT_KvScale].name = "KvScale";
  P[VT_Kv43Frac].name = "Kv43Frac";
  P[VT_GKv43].name = "GKv43";
  P[VT_PKv14].name = "PKv14";

  P[VT_LTRPNtot].name = "LTRPNtot";
  P[VT_HTRPNtot].name = "HTRPNtot";
  P[VT_khtrpn_plus].name = "khtrpn_plus";
  P[VT_khtrpn_minus].name = "khtrpn_minus";
  P[VT_kltrpn_plus].name = "kltrpn_plus";
  P[VT_kltrpn_minus].name = "kltrpn_minus";
  P[VT_CMDNtot].name = "CMDNtot";
  P[VT_CSQNtot].name = "CSQNtot";
  P[VT_EGTAtot].name = "EGTAtot";
  P[VT_KmCMDN].name = "KmCMDN";
  P[VT_KmCSQN].name = "KmCSQN";
  P[VT_KmEGTA].name = "KmEGTA";
  P[VT_K1bSS].name = "K1bSS";
  P[VT_K2bSS].name = "K2bSS";
  P[VT_KbJSR].name = "KbJSR";

  // Init parameters
  P[VT_init_mNa].name = "init_mNa";
  P[VT_init_hNa].name = "init_hNa";
  P[VT_init_jNa].name = "init_jNa";
  P[VT_init_Na_i].name = "init_Na_i";
  P[VT_init_K_i].name = "init_K_i";
  P[VT_init_Ca_i].name = "init_Ca_i";
  P[VT_init_Ca_NSR].name = "init_Ca_NSR";
  P[VT_init_Ca_SS].name = "init_Ca_SS";
  P[VT_init_Ca_JSR].name = "init_Ca_JSR";
  P[VT_init_C1_RyR].name = "init_C1_RyR";
  P[VT_init_O1_RyR].name = "init_O1_RyR";
  P[VT_init_O2_RyR].name = "init_O2_RyR";
  P[VT_init_C2_RyR].name = "init_C2_RyR";
  P[VT_init_xKr].name = "init_xKr";
  P[VT_init_xKs].name = "init_xKs";
  P[VT_init_C0].name = "init_C0";
  P[VT_init_C1].name = "init_C1";
  P[VT_init_C2].name = "init_C2";
  P[VT_init_C3].name = "init_C3";
  P[VT_init_C4].name = "init_C4";
  P[VT_init_Open].name = "init_Open";
  P[VT_init_CCa0].name = "init_CCa0";
  P[VT_init_CCa1].name = "init_CCa1";
  P[VT_init_CCa2].name = "init_CCa2";
  P[VT_init_CCa3].name = "init_CCa3";
  P[VT_init_CCa4].name = "init_CCa4";
  P[VT_init_yCa].name = "init_yCa";
  P[VT_init_LTRPNCa].name = "init_LTRPNCa";
  P[VT_init_HTRPNCa].name = "init_HTRPNCa";
  P[VT_init_C0Kv43].name = "init_C0Kv43";
  P[VT_init_C1Kv43].name = "init_C1Kv43";
  P[VT_init_C2Kv43].name = "init_C2Kv43";
  P[VT_init_C3Kv43].name = "init_C3Kv43";
  P[VT_init_OKv43].name = "init_OKv43";
  P[VT_init_CI0Kv43].name = "init_CI0Kv43";
  P[VT_init_CI1Kv43].name = "init_CI1Kv43";
  P[VT_init_CI2Kv43].name = "init_CI2Kv43";
  P[VT_init_CI3Kv43].name = "init_CI3Kv43";
  P[VT_init_OIKv43].name = "init_OIKv43";
  P[VT_init_C0Kv14].name = "init_C0Kv14";
  P[VT_init_C1Kv14].name = "init_C1Kv14";
  P[VT_init_C2Kv14].name = "init_C2Kv14";
  P[VT_init_C3Kv14].name = "init_C3Kv14";
  P[VT_init_OKv14].name = "init_OKv14";
  P[VT_init_CI0Kv14].name = "init_CI0Kv14";
  P[VT_init_CI1Kv14].name = "init_CI1Kv14";
  P[VT_init_CI2Kv14].name = "init_CI2Kv14";
  P[VT_init_CI3Kv14].name = "init_CI3Kv14";
  P[VT_init_OIKv14].name = "init_OIKv14";
  P[VT_Amp].name = "Amp";

  P[VT_RTdF].readFromFile = false;
  P[VT_RTd2F].readFromFile = false;
  P[VT_FdRT].readFromFile = false;
  P[VT_FdRTm1k5].readFromFile = false;
  P[VT_Vol].readFromFile = false;
  P[VT_VmyodVNSR].readFromFile = false;
  P[VT_VJSRdVNSR].readFromFile = false;
  P[VT_VJSRdVSS].readFromFile = false;
  P[VT_VmyodVSS].readFromFile = false;
  P[VT_AcapdVmyo].readFromFile = false;
  P[VT_Acapd2Vmyo].readFromFile = false;
  P[VT_AcapDVSS].readFromFile = false;
  P[VT_Na_oK].readFromFile = false;
  P[VT_sigma].readFromFile = false;
  P[VT_etam1].readFromFile = false;
  P[VT_NNNO].readFromFile = false;
  P[VT_KkNaCa].readFromFile = false;
  P[VT_KI_K1].readFromFile = false;
  P[VT_dKfb].readFromFile = false;
  P[VT_dKrb].readFromFile = false;
  P[VT_dtautr].readFromFile = false;
  P[VT_dtauxfer].readFromFile = false;
  P[VT_dbL].readFromFile = false;
  P[VT_PCa].readFromFile = false;
  P[VT_PK].readFromFile = false;
  P[VT_dICahalf].readFromFile = false;
  P[VT_df1Kv43].readFromFile = false;
  P[VT_df2Kv43].readFromFile = false;
  P[VT_df3Kv43].readFromFile = false;
  P[VT_df4Kv43].readFromFile = false;
  P[VT_db1Kv43].readFromFile = false;
  P[VT_db2Kv43].readFromFile = false;
  P[VT_db3Kv43].readFromFile = false;
  P[VT_db4Kv43].readFromFile = false;
  P[VT_df1Kv14].readFromFile = false;
  P[VT_df2Kv14].readFromFile = false;
  P[VT_df3Kv14].readFromFile = false;
  P[VT_df4Kv14].readFromFile = false;
  P[VT_db1Kv14].readFromFile = false;
  P[VT_db2Kv14].readFromFile = false;
  P[VT_db3Kv14].readFromFile = false;
  P[VT_db4Kv14].readFromFile = false;
  P[VT_C0Kv14_to_CI0Kv14].readFromFile = false;
  P[VT_C1Kv14_to_CI1Kv14].readFromFile = false;
  P[VT_C2Kv14_to_CI2Kv14].readFromFile = false;
  P[VT_C3Kv14_to_CI3Kv14].readFromFile = false;
  P[VT_OKv14_to_OIKv14].readFromFile = false;
  P[VT_CI0Kv14_to_C0Kv14].readFromFile = false;
  P[VT_CI1Kv14_to_C1Kv14].readFromFile = false;
  P[VT_CI2Kv14_to_C2Kv14].readFromFile = false;
  P[VT_CI3Kv14_to_C3Kv14].readFromFile = false;
  P[VT_OIKv14_to_OKv14].readFromFile = false;
  P[VT_kb2Kv43].readFromFile = false;
  P[VT_kb3Kv43].readFromFile = false;
  P[VT_kb4Kv43].readFromFile = false;
  P[VT_kf1Kv43].readFromFile = false;
  P[VT_kf2Kv43].readFromFile = false;
  P[VT_kf3Kv43].readFromFile = false;
  P[VT_kb2Kv14].readFromFile = false;
  P[VT_kb3Kv14].readFromFile = false;
  P[VT_kb4Kv14].readFromFile = false;
  P[VT_kf1Kv14].readFromFile = false;
  P[VT_kf2Kv14].readFromFile = false;
  P[VT_kf3Kv14].readFromFile = false;
  P[VT_GKv43].readFromFile = false;
  P[VT_PKv14].readFromFile = false;
  P[VT_K1bSS].readFromFile = false;
  P[VT_K2bSS].readFromFile = false;
  P[VT_KbJSR].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_WinslowCanine);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
  InitTable(tinc);
} // WinslowCanineParameters::Init

void WinslowCanineParameters::Calculate() {
#if KADEBUG
  cerr << "WinslowCanineParameters::Calculate" << endl;
#endif // if KADEBUG

  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();

  P[VT_RTdF].value = P[VT_R].value * P[VT_T].value / P[VT_F].value;
  P[VT_RTd2F].value = P[VT_RTdF].value * .5;
  P[VT_FdRT].value = 1.0 / P[VT_RTdF].value;
  P[VT_FdRTm1k5].value = P[VT_FdRT].value * 1.5;

  P[VT_Vol].value = P[VT_Vmyo].value / 1e9;

  P[VT_VmyodVNSR].value = P[VT_Vmyo].value / P[VT_VNSR].value;
  P[VT_VJSRdVNSR].value = P[VT_VJSR].value / P[VT_VNSR].value;
  P[VT_VJSRdVSS].value = P[VT_VJSR].value / P[VT_VSS].value;
  P[VT_VmyodVSS].value = P[VT_Vmyo].value / P[VT_VSS].value;
  P[VT_AcapdVmyo].value = P[VT_Acap].value / (P[VT_Vmyo].value * P[VT_F].value * 1000.0);
  P[VT_Acapd2Vmyo].value = P[VT_AcapdVmyo].value * .5;
  P[VT_AcapDVSS].value = P[VT_Acap].value / (2.0 * P[VT_VSS].value * P[VT_F].value * 1000.0);
  P[VT_Na_oK].value = .01833 * P[VT_Na_o].value;
  P[VT_etam1].value = P[VT_eta].value - 1.0;
  P[VT_sigma].value = (exp(P[VT_Na_o].value * .014858841) - 1.0) * .14285714;
  P[VT_NNNO].value = P[VT_Na_o].value * P[VT_Na_o].value * P[VT_Na_o].value;
  P[VT_KkNaCa].value = P[VT_kNaCa].value * 5000.0 /
                       (P[VT_KmNa].value * P[VT_KmNa].value * P[VT_KmNa].value + P[VT_NNNO].value);
  P[VT_KI_K1].value = P[VT_GK1].value * (P[VT_K_o].value / (P[VT_K_o].value + P[VT_KmK1].value));
  P[VT_dKfb].value = 1.0 / P[VT_Kfb].value;
  P[VT_dKrb].value = 1.0 / P[VT_Krb].value;
  P[VT_dtautr].value = 1.0 / P[VT_tautr].value;
  P[VT_dtauxfer].value = 1.0 / P[VT_tauxfer].value;
  P[VT_dbL].value = 1.0 / P[VT_bL].value;
  P[VT_PCa].value = (P[VT_Pscale].value * 2.469e-4);
  P[VT_PK].value = (P[VT_Pscale].value * 4.574e-7);
  P[VT_dICahalf].value = 1.0 / P[VT_ICahalf].value;
  P[VT_df1Kv43].value = 1.0 / P[VT_f1Kv43].value;
  P[VT_df2Kv43].value = 1.0 / P[VT_f2Kv43].value;
  P[VT_df3Kv43].value = 1.0 / P[VT_f3Kv43].value;
  P[VT_df4Kv43].value = 1.0 / P[VT_f4Kv43].value;
  P[VT_db1Kv43].value = 1.0 / P[VT_b1Kv43].value;
  P[VT_db2Kv43].value = 1.0 / P[VT_b2Kv43].value;
  P[VT_db3Kv43].value = 1.0 / P[VT_b3Kv43].value;
  P[VT_db4Kv43].value = 1.0 / P[VT_b4Kv43].value;
  P[VT_df1Kv14].value = 1.0 / P[VT_f1Kv14].value;
  P[VT_df2Kv14].value = 1.0 / P[VT_f2Kv14].value;
  P[VT_df3Kv14].value = 1.0 / P[VT_f3Kv14].value;
  P[VT_df4Kv14].value = 1.0 / P[VT_f4Kv14].value;
  P[VT_db1Kv14].value = 1.0 / P[VT_b1Kv14].value;
  P[VT_db2Kv14].value = 1.0 / P[VT_b2Kv14].value;
  P[VT_db3Kv14].value = 1.0 / P[VT_b3Kv14].value;
  P[VT_db4Kv14].value = 1.0 / P[VT_b4Kv14].value;
  P[VT_C0Kv14_to_CI0Kv14].value = P[VT_betai0Kv14].value;
  P[VT_C1Kv14_to_CI1Kv14].value = P[VT_f1Kv14].value * P[VT_betai0Kv14].value;
  P[VT_C2Kv14_to_CI2Kv14].value = P[VT_f2Kv14].value * P[VT_betai0Kv14].value;
  P[VT_C3Kv14_to_CI3Kv14].value = P[VT_f3Kv14].value * P[VT_betai0Kv14].value;
  P[VT_OKv14_to_OIKv14].value = P[VT_f4Kv14].value * P[VT_betai0Kv14].value;
  P[VT_CI0Kv14_to_C0Kv14].value = P[VT_alphai0Kv14].value;
  P[VT_CI1Kv14_to_C1Kv14].value = P[VT_alphai0Kv14].value * P[VT_db1Kv14].value;
  P[VT_CI2Kv14_to_C2Kv14].value = P[VT_alphai0Kv14].value * P[VT_db2Kv14].value;
  P[VT_CI3Kv14_to_C3Kv14].value = P[VT_alphai0Kv14].value * P[VT_db3Kv14].value;
  P[VT_OIKv14_to_OKv14].value = P[VT_alphai0Kv14].value * P[VT_db4Kv14].value;

  P[VT_kb2Kv43].value = P[VT_b2Kv43].value * P[VT_db1Kv43].value;
  P[VT_kb3Kv43].value = P[VT_b3Kv43].value * P[VT_db2Kv43].value;
  P[VT_kb4Kv43].value = P[VT_b4Kv43].value * P[VT_db3Kv43].value;
  P[VT_kf1Kv43].value = P[VT_f1Kv43].value * P[VT_df2Kv43].value;
  P[VT_kf2Kv43].value = P[VT_f2Kv43].value * P[VT_df3Kv43].value;
  P[VT_kf3Kv43].value = P[VT_f3Kv43].value * P[VT_df4Kv43].value;
  P[VT_kb2Kv14].value = P[VT_b2Kv14].value * P[VT_db1Kv14].value;
  P[VT_kb3Kv14].value = P[VT_b3Kv14].value * P[VT_db2Kv14].value;
  P[VT_kb4Kv14].value = P[VT_b4Kv14].value * P[VT_db3Kv14].value;
  P[VT_kf1Kv14].value = P[VT_f1Kv14].value * P[VT_df2Kv14].value;
  P[VT_kf2Kv14].value = P[VT_f2Kv14].value * P[VT_df3Kv14].value;
  P[VT_kf3Kv14].value = P[VT_f3Kv14].value * P[VT_df4Kv14].value;
  P[VT_GKv43].value = (P[VT_Kv43Frac].value * P[VT_KvScale].value * .1);
  P[VT_PKv14].value = ((1.0 - P[VT_Kv43Frac].value) * P[VT_KvScale].value * 4.792933e-7);
  P[VT_K1bSS].value = P[VT_CMDNtot].value * P[VT_KmCMDN].value;
  P[VT_K2bSS].value = P[VT_EGTAtot].value * P[VT_KmEGTA].value;
  P[VT_KbJSR].value = P[VT_CSQNtot].value * P[VT_KmCSQN].value;
} // WinslowCanineParameters::Calculate

void WinslowCanineParameters::InitTable(ML_CalcType tinc) {
  tinc *= -1000.0;  // ms->sec, neg. for exptau calculation
  double a, b;
  for (double V = -RangeTabhalf + 0.0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int) (DivisionTab * (RangeTabhalf + V) + .5);

    if ((double) fabs(V + 47.13) <= 1.0e-3)
      alpha_m[Vi] = .32 / (.1 - .005 * (V + 47.13));
    else
      alpha_m[Vi] = .32 * (V + 47.13) / (1.0 - exp(-.1 * (V + 47.13)));
    beta_m[Vi] = alpha_m[Vi] + .08 * exp(-V * .090909091);

    if (V < -40.0) {
      a = .135 * exp((80.0 + V) * -.14705882);
      b = a + 3.56 * exp(.079 * V) + 310000.0 * exp(.35 * V);
      h_inf[Vi] = a / b;
      exptau_h[Vi] = exp(tinc * b);
      a = ((-127140.0 * exp(.2444 * V)) - (3.474e-5 * exp(-.04391 * V))) * (V + 37.78) /
          (1.0 + exp(.311 * (V + 79.23)));
      b = a + .1212 * exp(-.01052 * V) / (1.0 + exp(-.1378 * (V + 40.14)));
      j_inf[Vi] = a / b;
      exptau_j[Vi] = exp(tinc * b);
    } else {
      b = 1.0 / (.13 * (1.0 + exp((V + 10.66) * -.090909091)));
      h_inf[Vi] = 0.;
      exptau_h[Vi] = exp(tinc * b);
      b = .3 * exp(-2.535e-7 * V) / (1.0 + exp(-.1 * (V + 32.0)));
      j_inf[Vi] = 0.;
      exptau_j[Vi] = exp(tinc * b);
    }

    double k12 = exp(-5.4950 + .1691 * V);
    double k21 = exp(-7.677 - .0128 * V);
    xKr_inf[Vi] = k12 / (k12 + k21);
    exptau_xKr[Vi] = exp(tinc / (1.0 / (k12 + k21) + 27.0));

    xKs_inf[Vi] = 1.0 / (1.0 + exp(-(V - 24.7) * .073529412));
    exptau_xKs[Vi] =
        exp(tinc * ((7.19e-5 * (V - 10.0) / (1.0 - exp(-.148 * (V - 10.0)))) +
                    (1.31e-4 * (V - 10.0) / (exp(.0687 * (V - 10.0)) - 1.0))));

    RV[Vi] = P[VT_GKr].value * sqrt(P[VT_K_o].value * .25) / (1.0 + 1.4945 * exp(.0446 * V));
    KpV[Vi] = P[VT_GKp].value / (1.0 + exp((7.488 - V) * .16722408));
    alphaarray[Vi] = .416 * exp((V + 2.0) * .1);
    bettaarray[Vi] = .049 * exp(-(V + 2.0) * .076923077);

    yCa_inf[Vi] = .745 / (1.0 + exp((V + 12.5) * .2)) + .255;
    exptau_yCa[Vi] = exp(tinc * (.012 / (.5 + exp(-V * .22222222)) + .0035 * exp(-V * .027548209)));

    alpha_act43array[Vi] = P[VT_alphaa0Kv43].value * exp(P[VT_aaKv43].value * V);
    beta_act43array[Vi] = P[VT_betaa0Kv43].value * exp(-P[VT_baKv43].value * V);
    alpha_inact43array[Vi] = P[VT_alphai0Kv43].value * exp(-P[VT_aiKv43].value * V);
    beta_inact43array[Vi] = P[VT_betai0Kv43].value * exp(P[VT_biKv43].value * V);

    alpha_act14array[Vi] = P[VT_alphaa0Kv14].value * exp(P[VT_aaKv14].value * V);
    beta_act14array[Vi] = P[VT_betaa0Kv14].value * exp(-P[VT_baKv14].value * V);

    double VFdRT = V * P[VT_FdRT].value;
    expVFdRT[Vi] = exp(VFdRT);
    double exp2VFdRT = expVFdRT[Vi] * expVFdRT[Vi];
    double dexpVFdRTm1 = 1.0 / (expVFdRT[Vi] - 1.0);
    expm1VFdRT[Vi] = exp(-VFdRT);
    double VFsqdRT = P[VT_F].value * 1000.0 * VFdRT;
    KI_Kv14_K[Vi] = P[VT_PKv14].value * VFsqdRT * dexpVFdRTm1;
    KI_Kv14_Na[Vi] = 0.02 * P[VT_PKv14].value * VFsqdRT * dexpVFdRTm1;
    KI_Camax[Vi] = P[VT_PCa].value * 4.0 * VFsqdRT / (exp2VFdRT - 1.0) *
                   (1.0e-3 * exp2VFdRT - P[VT_Ca_o].value * .341);
    KI_Ca[Vi] = P[VT_PK].value * VFsqdRT * dexpVFdRTm1;
    KI_NaK[Vi] = P[VT_INaKmax].value /
                 (1.0 + .1245 * exp(-.1 * VFdRT) + .0365 * P[VT_sigma].value * expm1VFdRT[Vi]) *
                 (P[VT_K_o].value / (P[VT_K_o].value + P[VT_KmKo].value));
    KI_NaCa[Vi] = P[VT_KkNaCa].value * exp(P[VT_eta].value * VFdRT) /
                  ((1.0 + P[VT_ksat].value * exp(P[VT_etam1].value * VFdRT)) *
                   (P[VT_KmCa].value + P[VT_Ca_o].value));
  }
} // WinslowCanineParameters::InitTable
