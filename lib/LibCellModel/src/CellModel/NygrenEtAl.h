/**@file NygrenEtAl.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef NYGRENETAL_H
#define NYGRENETAL_H

#include <NygrenEtAlParameters.h>

// #define TASK 1 <-- wird in NygrenEtAlParameters.h definiert!
// #define KADEBUG 1

#define HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_NygrenEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pNyeaP->P[NS_NygrenEtAlParameters::a].value
#endif // ifdef HETERO

class NygrenEtAl : public vbElphyModel<ML_CalcType> {
public:
  NygrenEtAlParameters *pNyeaP;
  ML_CalcType Ca_i, Ca_d;

  ML_CalcType h1, h2, fl2;
  ML_CalcType s, ssus;
#ifdef NYEA_TASK
  ML_CalcType C1_TASK, C2_TASK, O_TASK;
#endif // ifdef NYEA_TASK

  ML_CalcType n, pa;
  ML_CalcType O_c, O_tc, O_tmgc, O_calse;

  ML_CalcType Ca_up, Ca_rel;
  ML_CalcType Fl1, Fl2;

  ML_CalcType Ca_c, Na_i, Na_c, K_i, K_c;

  ML_CalcType m, dl, fl1, r, rsus, O_tmgmg;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  NygrenEtAl(NygrenEtAlParameters *);

  ~NygrenEtAl() {}

  virtual inline bool AddHeteroValue(string, double);

  virtual inline ML_CalcType Volume() { return (CELLMODEL_PARAMVALUE(VT_V_i)) * 1e-12 * (CELLMODEL_PARAMVALUE(VT_V_corrcell)); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); } // in nA

  virtual inline ML_CalcType GetStimTime() { return CELLMODEL_PARAMVALUE(VT_stim_duration); }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_init_Vm); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return Ca_c; }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return Na_c; }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return K_c; }

  virtual inline int GetSize() {
    return (&O_tmgmg - &Ca_i + 1) * sizeof(ML_CalcType);
  }

  virtual inline ML_CalcType *GetBase() { return &Ca_i; }

  virtual inline void SetCao(ML_CalcType val) { Ca_c = val; }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetNao(ML_CalcType val) { Na_c = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual inline void SetKo(ML_CalcType val) { K_c = val; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char) (adVm < .5e-6 ? 5 : (adVm < 1e-6 ? 4 : (adVm < 2e-6 ? 3 : (adVm < 4e-6
                                                                                      ? 2 : 1))));
  }

  virtual void Init();

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);
}; // class NygrenEtAl

#endif // ifndef NYGRENETAL_H
