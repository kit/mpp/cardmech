/*
 *  HundParameters.cpp
 *
 *  Created by Christian Rombach on 04.08.08.
 *  Last modified Christian Rombach (15.09.09)
 *  Institute of Biomedical Engineering\n
 *  Universitaet Karlsruhe (TH)\n
 *  http://www.ibt.uni-karlsruhe.de\n
 *
 */

#include <HundParameters.h>

HundParameters::HundParameters(const char *initFile, ML_CalcType tinc) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

HundParameters::~HundParameters() {
  // Destruktor
}

void HundParameters::PrintParameters() {
  // print the parameter to de stdout
  cout << "HundParameters:" << endl;
  for (int i = vtFirst; i < vtLast; i++)
    cout << P[i].name << " = " << P[i].value << endl;
}

void HundParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "#Loading the Hund parameter from " << initFile << " ...\n";
#endif // if KADEBUG

  // Initialization of the Parameters ...
  P[VT_eta].name = "eta";
  P[VT_KmNai].name = "KmNai";
  P[VT_KmNao].name = "KmNao";
  P[VT_KmCai].name = "KmCai";
  P[VT_KmCao].name = "KmCao";
  P[VT_ibarpca].name = "ibarpca";
  P[VT_kmpca].name = "kmpca";
  P[VT_PCl].name = "PCl";
  P[VT_AA_init].name = "init_AA";
  P[VT_Kmto2].name = "Kmto2";
  P[VT_GClb].name = "GClb";
  P[VT_GNaL].name = "GNaL";
  P[VT_mL_init].name = "init_mL";
  P[VT_hL_init].name = "init_hL";
  P[VT_prnak].name = "prnak";
  P[VT_dtau_rel_max].name = "dtau_rel_max";
  P[VT_CaMK0].name = "CaMK0";
  P[VT_Km].name = "Km";
  P[VT_KmCaMK].name = "KmCaMK";
  P[VT_CaMKtrap_init].name = "init_CaMKtrap";
  P[VT_ro_init].name = "init_ro";
  P[VT_ri_init].name = "init_ri";
  P[VT_dKmPLBmax].name = "dKmPLBmax";
  P[VT_dJupmax].name = "dJupmax";
  P[VT_F].name = "F";
  P[VT_K_o].name = "K_o";
  P[VT_Ca_o].name = "Ca_o";
  P[VT_Na_o].name = "Na_o";
  P[VT_Cl_o].name = "Cl_o";
  P[VT_FonRT].name = "FonRT";
  P[VT_tissue].name = "tissue";
  P[VT_V_init].name = "init_V";
  P[VT_l].name = "l";
  P[VT_a].name = "a";
  P[VT_stim_offset].name = "stim_offset";
  P[VT_stim_period].name = "stim_period";
  P[VT_stim_duration].name = "stim_duration";
  P[VT_stim_amplitude].name = "stim_amplitude";
  P[VT_H_init].name = "init_H";
  P[VT_m_init].name = "init_m";
  P[VT_J_init].name = "init_J";
  P[VT_d_init].name = "init_d";
  P[VT_dp_init].name = "init_dp";
  P[VT_f_init].name = "init_f";
  P[VT_fca_init].name = "init_fca";
  P[VT_fca2_init].name = "init_fca2";
  P[VT_f2_init].name = "init_f2";
  P[VT_pca].name = "pca";
  P[VT_gacai].name = "gacai";
  P[VT_gacao].name = "gacao";
  P[VT_iupmax].name = "iupmax";
  P[VT_Kmup].name = "Kmup";
  P[VT_nsrmax].name = "nsrmax";
  P[VT_Na_i_init].name = "init_Na_i";
  P[VT_CTNaClmax].name = "CTNaClmax";
  P[VT_K_i_init].name = "init_K_i";
  P[VT_CTKClmax].name = "CTKClmax";
  P[VT_Cl_i_init].name = "init_Cl_i";
  P[VT_Ca_i_init].name = "init_Ca_i";
  P[VT_Ca_jsr_init].name = "init_Ca_jsr";
  P[VT_Ca_nsr_init].name = "init_Ca_nsr";
  P[VT_Ca_ss_init].name = "init_Ca_ss";
  P[VT_kmt].name = "kmt";
  P[VT_kmc].name = "kmc";
  P[VT_tbar].name = "tbar";
  P[VT_cbar].name = "cbar";
  P[VT_kmcsqn].name = "kmcsqn";
  P[VT_csqnbar].name = "csqnbar";
  P[VT_BSRmax].name = "BSRmax";
  P[VT_KmBSR].name = "KmBSR";
  P[VT_BSLmax].name = "BSLmax";
  P[VT_KmBSL].name = "KmBSL";
  P[VT_xr_init].name = "init_xr";
  P[VT_xs1_init].name = "init_xs1";
  P[VT_xs2_init].name = "init_xs2";
  P[VT_gitodv].name = "gitodv";
  P[VT_ydv_init].name = "init_ydv";
  P[VT_ydv2_init].name = "init_ydv2";
  P[VT_zdv_init].name = "init_zdv";
  P[VT_kmnai].name = "kmnai";
  P[VT_kmko].name = "kmko";
  P[VT_ibarnak].name = "ibarnak";
  P[VT_KmCa].name = "KmCa";
  P[VT_NCXmax].name = "NCXmax";
  P[VT_ksat].name = "ksat";
  P[VT_GNa].name = "GNa";
  P[VT_vcell].name = "vcell";
  P[VT_Acap].name = "Acap";
  P[VT_ageo].name = "ageo";
  P[VT_vmyo].name = "vmyo";
  P[VT_vmito].name = "vmito";
  P[VT_vsr].name = "vsr";
  P[VT_vnsr].name = "vnsr";
  P[VT_vjsr].name = "vjsr";
  P[VT_vss].name = "vss";
  P[VT_AF].name = "AF";
  P[VT_FONRT].name = "FONRT";
  P[VT_sigma].name = "sigma";


  P[VT_vcell].readFromFile = false;
  P[VT_ageo].readFromFile = false;
  P[VT_Acap].readFromFile = false;
  P[VT_vmyo].readFromFile = false;
  P[VT_vmito].readFromFile = false;
  P[VT_vsr].readFromFile = false;
  P[VT_vnsr].readFromFile = false;
  P[VT_vjsr].readFromFile = false;
  P[VT_vss].readFromFile = false;
  P[VT_AF].readFromFile = false;
  P[VT_FONRT].readFromFile = false;
  P[VT_sigma].readFromFile = false;
  P[VT_GNa].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_Hund);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile) {
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);
    }
      // End Initialization of the Parameters ...
    else {
#if KADEBUG
#endif
    }
  Calculate();
  InitTable(tinc);
#if KADEBUG
  cerr << "#Init() done ...\n";
#endif // if KADEBUG
} // HundParameters::Init

void HundParameters::Calculate() {
#if KADEBUG
  cerr << "#HundParameters - Calculate ..." << endl;
#endif // if KADEBUG


  P[VT_vcell].value = 1000. * M_PI * (P[VT_a].value) * (P[VT_a].value) * (P[VT_l].value);
  P[VT_ageo].value = (2. * M_PI * (P[VT_a].value) * (P[VT_a].value) +
                      2. * M_PI * (P[VT_a].value) * (P[VT_l].value));
  P[VT_Acap].value = (P[VT_ageo].value) * 2.;
  P[VT_vmyo].value = (P[VT_vcell].value) * 0.68;
  P[VT_vmito].value = (P[VT_vcell].value) * 0.26;
  P[VT_vsr].value = (P[VT_vcell].value) * 0.06;
  P[VT_vnsr].value = (P[VT_vcell].value) * 0.0552;
  P[VT_vjsr].value = (P[VT_vcell].value) * 0.0048;
  P[VT_vss].value = (P[VT_vcell].value) * 0.02;
  P[VT_AF].value = (P[VT_Acap].value) / (P[VT_F].value);
  P[VT_FONRT].value = P[VT_FonRT].value;
  P[VT_sigma].value = ((exp(P[VT_Na_o].value / (67.3)) - 1) / 7);
  P[VT_GNa].value = 4. * 8.25;
  if (P[VT_tissue].value == 0)
    P[VT_GNa].value = 8.25;

  // P[VT_GNa].value = 8.25;

  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
} // HundParameters::Calculate

void HundParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr << "#HundParameters - InitTable ..." << endl;
#endif // if KADEBUG
  /*cout << "#" << " " << "1:V" << " " << "2:ah" << " " << "3:aj" << " " << "4:bh" << " "  << "5:bj" << " " << "6:am" <<
     " " << "7:bm" << " " << "8:dss" << " "
     << "9:taud" << " " << "10:fss" << " " << "11:f2ss" << " " << "12:tauf" << " " << "13:tauf2" << " " << "14:dpss" <<
        " " << "15:r" << " " << "16:xrss" << " "
     << "17:tauxr" << " "<< "18:xss" << " " << "19:tauxs" << " " << "20:rv" << " " <<"21:ay" << " " << "22:by" << " " <<
        "23:ay2" << " " << "24:by2" << " "
     << "25:ay3" << " " << "26:by3" << " " << "27:amL" << " " << "28:bmL" << " " << "29:hLss" << endl;
   */

  ML_CalcType a, b;

  tinc *= -1000; // sec -> ms; neg. for exptau

  et1 = exp(tinc);
  et3 = exp(tinc / 3.);
  et10 = exp(tinc / 10.);
  et600 = exp(tinc / 600.);

  for (double V = -RangeTabhalf + .0001; V < RangeTabhalf; V += dDivisionTab) {
    // V in mV

    int Vi = (int) (DivisionTab * (RangeTabhalf + V) + .5);

    if (V >= -40.) {
      a = 0.;
      b = (1) / (.13 * (1 + exp((V + (10.66)) / ((-1) * (11.1)))));
    } else {
      a = (.135) * exp(((80) + V) / ((-1) * (6.8)));
      b = a + ((3.56) * exp((.079) * V)) + ((3.1e5) * exp((.35) * V));
    }
    hss[Vi] = a / b;
    eth[Vi] = exp(tinc * b);  // exp( - dt / tau); tau = 1/(a+b)

    if (V >= -40.) {
      // a still eq. 0
      b = ((.3) * exp(((-1) * (2.535e-7)) * V)) / (1 + exp(((-1) * (.1)) * (V + (32))));
    } else {
      a = ((((((-1) * (127140)) * exp((.2444) * V)) - ((3.474e-5) * exp(((-1) * (.04391)) * V))) *
            (1)) * (V + (37.78))) /
          (1 + exp((.311) * (V + (79.23))));
      b = a + ((.1212) * exp(((-1) * (.01052)) * V)) / (1 + exp(((-1) * (.1378)) * (V + (40.14))));
    }
    jss[Vi] = a / b;
    etj[Vi] = exp(tinc * b);

    a = ((((.32) * (1)) * (V + (47.13))) / (1 - exp(((-1) * (.1)) * (V + (47.13)))));
    b = a + ((.08) * exp(((-1) * V) / (11)));
    mss[Vi] = a / b;
    etm[Vi] = exp(tinc * b);

    dss[Vi] = (1 / (1 + exp(((-1) * (V - (4))) / (6.74))));
    etd[Vi] = exp(
        tinc / (((.59) + (((.8) * exp((.052) * (V + (13)))) / (1 + exp((.132) * (V + (13))))))));

    fss[Vi] = ((.7 / (1 + exp((V + (17.12)) / (7)))) + .3);
    f2ss[Vi] = ((.77 / (1 + exp((V + (17.12)) / (7)))) + .23);
    etf[Vi] = exp(tinc *
                  ((.2411 * exp((-1) * (((.045) * (V - (9.6914))) * ((.045) * (V - (9.6914)))))) +
                   .0529));
    etf2[Vi] = exp(tinc * ((.0423 *
                            exp((-1) * (((.059) * (V - (18.5726))) * ((.059) * (V - (18.5726)))))) +
                           .0054));

    dpss[Vi] = (9 - (8 / (1 + exp(((-1) * (V + (65))) / (3.4)))));

    r[Vi] = (1 / (1 + exp((V + (10)) / (15.4))));

    xrss[Vi] = (1 / (1 + exp(((-1) * (V + (10.085))) / (4.25))));
    etxr[Vi] =
        exp(tinc *
            ((((6e-4) * (V - (1.7384))) / (1 - exp(((-1) * (.136)) * (V - (1.7384))))) +
             (((3e-4) * (V + (38.3608))) / (exp((.1522) * (V + (38.3608))) - 1))));

    xss[Vi] = (1 / (1 + exp(((-1) * (V - (10.5))) / (24.7))));
    a =
        ((((7.61e-5) * (V + (44.6))) / (1 - exp(((-1) * (9.97)) * (V + (44.6))))) +
         (((3.6e-4) * (V - (.55))) / (exp((.128) * (V - (.55))) - 1)));
    etxs[Vi] = exp(tinc * a);
    etxs2[Vi] = exp(tinc * a / 2);

    rv[Vi] = exp(V / (300));

    a = (((25) * exp((V - (40)) / (25))) / (1 + exp((V - (40)) / (25))));
    b = a + (((25) * exp(((-1) * (V + (90))) / (25))) / (1 + exp(((-1) * (V + (90))) / (25))));
    yss[Vi] = a / b;
    ety[Vi] = exp(tinc * b);

    a = ((.03) / (1 + exp((V + (60)) / (5))));
    b = a + (((.2) * exp((V + (25)) / (5))) / (1 + exp((V + (25)) / (5))));
    y2ss[Vi] = a / b;
    ety2[Vi] = exp(tinc * b);

    a = ((.00225) / (1 + exp((V + (60)) / (5))));
    b = a + (((.1) * exp((V + (25)) / (5))) / (1 + exp((V + (25)) / (5))));
    zss[Vi] = a / b;
    etz[Vi] = exp(tinc * b);

    a = ((((.32) * (1)) * (V + (47.13))) / (1 - exp(((-1) * (.1)) * (V + (47.13)))));
    b = a + ((.08) * exp(((-1) * V) / (11)));
    mLss[Vi] = a / b;
    etmL[Vi] = exp(tinc * b);

    hLss[Vi] = (1 / (1 + exp((V + (91)) / (6.1))));


    /*cout << V << " " << ah[Vi] << " " << aj[Vi] << " " << bh[Vi] << " " << bj[Vi] << " " << am[Vi] << " " << bm[Vi] <<
       " "
       << dss[Vi] << " " << taud[Vi] << " " << fss[Vi] << " " << f2ss[Vi] << " " << tauf[Vi] << " " << tauf2[Vi] << " "
          << dpss[Vi] << " "
       << r[Vi] << " " << xrss[Vi] << " " << tauxr[Vi] << " " << xss[Vi] << " " << tauxs[Vi] << " " << rv[Vi] << " " <<
          ay[Vi] << " "
       << by[Vi] << " " << ay2[Vi] << " " << by2[Vi] << " " << ay3[Vi] << " " << by3[Vi] << " " << amL[Vi] << " " <<
          bmL[Vi] << " " << hLss[Vi]  << endl;
     */
  }

  // exit (-1);
} // HundParameters::InitTable
