/*      File: DiFrancescoNoble.cpp
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#include <DiFrancescoNoble.h>

DiFrancescoNoble::DiFrancescoNoble(DiFrancescoNobleParameters *pp) {
  pDFNP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pDFNP, NS_DiFrancescoNobleParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

DiFrancescoNoble::~DiFrancescoNoble() {}

#ifdef HETERO

inline bool DiFrancescoNoble::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool DiFrancescoNoble::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int DiFrancescoNoble::GetSize(void) {
  return sizeof(DiFrancescoNoble) - sizeof(vbElphyModel<ML_CalcType>) -
         sizeof(DiFrancescoNobleParameters *)
#ifdef HETERO
    -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
      ;
}

inline unsigned char DiFrancescoNoble::getSpeed(ML_CalcType adVm) {
  return (unsigned char) (adVm < .125e-6 ? 5 : (adVm < .25e-6 ? 4 : (adVm < .5e-6 ? 3 : (adVm < 1e-6
                                                                                         ? 2
                                                                                         : 1))));
}

void DiFrancescoNoble::Init() {
#if KADEBUG
  cerr << "initializing Class: DiFrancescoNoble ... " << endl;
#endif // if KADEBUG
  m = CELLMODEL_PARAMVALUE(VT_m);
  d = CELLMODEL_PARAMVALUE(VT_d);
  Na_i = CELLMODEL_PARAMVALUE(VT_Na_i);
  K_i = CELLMODEL_PARAMVALUE(VT_K_i);
  y = CELLMODEL_PARAMVALUE(VT_y);
  x = CELLMODEL_PARAMVALUE(VT_x);
  h = CELLMODEL_PARAMVALUE(VT_h);
  f = CELLMODEL_PARAMVALUE(VT_f);
  f2 = CELLMODEL_PARAMVALUE(VT_f2);
  r = CELLMODEL_PARAMVALUE(VT_r);
  p = CELLMODEL_PARAMVALUE(VT_p);
  K_o = CELLMODEL_PARAMVALUE(VT_K_o);
  Ca_i = CELLMODEL_PARAMVALUE(VT_Ca_i);
  Ca_up = CELLMODEL_PARAMVALUE(VT_Ca_up);
  Ca_rel = CELLMODEL_PARAMVALUE(VT_Ca_rel);
}

inline void
DiFrancescoNoble::SetNaiKiconc(ML_CalcType tinc, const ML_CalcType I_mNa, const ML_CalcType I_mK) {
  Na_i -= tinc * (CELLMODEL_PARAMVALUE(VT_dViF)) * I_mNa;
  K_i -= tinc * (CELLMODEL_PARAMVALUE(VT_dViF)) * I_mK;
}

ML_CalcType DiFrancescoNoble::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = 0.0,
                                   ML_CalcType stretch = 1., int euler = 1) {
  const double V_int = V * 1000.0;
  const double VmE_K = V_int - (CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_o / K_i);
  const double VmE_Ca = V_int - (CELLMODEL_PARAMVALUE(VT_RTd2F)) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / (double) Ca_i);
  const double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_i));
  const double VmE_mh =
      V_int - (CELLMODEL_PARAMVALUE(VT_RTdF)) * log(((CELLMODEL_PARAMVALUE(VT_Na_o)) + 0.12 * K_o) * (1.0 / (Na_i + 0.12 * K_i)));
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double y_inf = pDFNP->y_inf[Vi];

  y = y_inf + (y - y_inf) * pDFNP->exptau_y[Vi];
  const double x_inf = pDFNP->x_inf[Vi];
  x = x_inf + (x - x_inf) * pDFNP->exptau_x[Vi];
  const double r_inf = pDFNP->r_inf[Vi];
  r = r_inf + (r - r_inf) * pDFNP->exptau_r[Vi];
  const double h_inf = pDFNP->h_inf[Vi];
  h = h_inf + (h - h_inf) * pDFNP->exptau_h[Vi];
  const double f_inf = pDFNP->f_inf[Vi];
  f = f_inf + (f - f_inf) * pDFNP->exptau_f[Vi];

  f2 += tinc * (10.0 - (10.0 + 10.0 / (CELLMODEL_PARAMVALUE(VT_k_mf2)) * Ca_i) * f2);

  p = f_inf + (p - f_inf) * pDFNP->exptau_p[Vi];
  const double m_inf = pDFNP->m_inf[Vi];
  m = m_inf + (m - m_inf) * pDFNP->exptau_m[Vi];
  const double d_inf = pDFNP->d_inf[Vi];
  d = d_inf + (d - d_inf) * pDFNP->exptau_d[Vi];

  const double ykdkk = y * K_o / (K_o + (CELLMODEL_PARAMVALUE(VT_K_mf)));
  const double I_fK = ykdkk * (CELLMODEL_PARAMVALUE(VT_g_fK)) * VmE_K;
  const double I_fNa = ykdkk * (CELLMODEL_PARAMVALUE(VT_g_fNa)) * VmE_Na;
  const double I_K = x * (CELLMODEL_PARAMVALUE(VT_I_Kmaxd)) * (K_i - K_o * pDFNP->expmV[Vi]);
  const double I_K1 = (CELLMODEL_PARAMVALUE(VT_g_k1)) * K_o / (K_o + (CELLMODEL_PARAMVALUE(VT_K_m1))) * VmE_K /
                      (1.0 + exp((VmE_K + 10.0) * (CELLMODEL_PARAMVALUE(VT_F2dRT))));
  const double I_too = r * (.2 + K_o / ((CELLMODEL_PARAMVALUE(VT_K_mto)) + K_o)) * pDFNP->Vp10dexp[Vi] *
                       (K_i - K_o * pDFNP->expm4V[Vi]);
  const double I_to = I_too * Ca_i / ((CELLMODEL_PARAMVALUE(VT_K_act4)) + Ca_i);
  const double I_bNa = (CELLMODEL_PARAMVALUE(VT_g_bNa)) * VmE_Na;
  const double I_bCa = (CELLMODEL_PARAMVALUE(VT_g_bCa)) * VmE_Ca;
  const double I_bK = (CELLMODEL_PARAMVALUE(VT_g_bK)) * VmE_K;
  const double I_NaK =
      K_o / (K_o + (CELLMODEL_PARAMVALUE(VT_k_mK))) * (CELLMODEL_PARAMVALUE(VT_I_NaKmax)) * (Na_i / (Na_i + (CELLMODEL_PARAMVALUE(VT_k_mNa))));
  const double NNNICO = (Na_i * Na_i * Na_i * (CELLMODEL_PARAMVALUE(VT_Ca_o)));
  const double NNNOCI = (CELLMODEL_PARAMVALUE(VT_NNNO)) * Ca_i;
  double I_NaCa = pDFNP->expstoch[Vi] * (NNNICO - pDFNP->expmstoch[Vi] * NNNOCI) /
                  (1.0 + (CELLMODEL_PARAMVALUE(VT_d_NaCa)) * (NNNICO + NNNOCI));
  const double I_Na = (CELLMODEL_PARAMVALUE(VT_g_Na)) * VmE_mh * m * m * m * h;
  const double dff2P = d * f * f2 * (CELLMODEL_PARAMVALUE(VT_P_si));
  const double I_siCa = 4.0 * dff2P * pDFNP->VdV2[Vi] * (Ca_i * pDFNP->exp50_2 - pDFNP->Cadexp[Vi]);
  const double I_siK =
      .01 * dff2P * pDFNP->VdV[Vi] * (K_i * pDFNP->exp50 - K_o * pDFNP->dexpV50[Vi]);
  const double CPR = Ca_i * Ca_i;
  const double I_up = ((CELLMODEL_PARAMVALUE(VT_F2VidCa_up)) * Ca_i * ((CELLMODEL_PARAMVALUE(VT_Ca_upmax)) - Ca_up) -
                       (CELLMODEL_PARAMVALUE(VT_b_up)) * Ca_up);
  const double I_tr = ((CELLMODEL_PARAMVALUE(VT_F2Vreldtrep)) * p * (Ca_up - Ca_rel));
  const double I_rel = (CELLMODEL_PARAMVALUE(VT_F2Vreldtrel)) * Ca_rel * CPR / (CPR + (CELLMODEL_PARAMVALUE(VT_k_mCa)));
  const double I_mK = I_K1 + I_K + I_siK - 2.0 * I_NaK + I_bK + I_fK + I_too;
  const double I_mCa = I_siCa + I_bCa - (CELLMODEL_PARAMVALUE(VT_tdn)) * I_NaCa;
  const double I_SAC = -(V_int - (CELLMODEL_PARAMVALUE(VT_Vr))) * (CELLMODEL_PARAMVALUE(VT_stretchfactor));
  SetNaiKiconc(tinc, I_Na + I_bNa + I_fNa + 3.0 * I_NaK + (CELLMODEL_PARAMVALUE(VT_ndn_NaCa)) * I_NaCa, I_mK);
  K_o += tinc * ((CELLMODEL_PARAMVALUE(VT_P_std)) * ((CELLMODEL_PARAMVALUE(VT_K_b)) - K_o) + I_mK * (CELLMODEL_PARAMVALUE(VT_dVeF)));
  Ca_i -= tinc * (I_mCa + I_up - I_rel) * (CELLMODEL_PARAMVALUE(VT_dF2Vi));
  Ca_up += tinc * (I_up - I_tr) * (CELLMODEL_PARAMVALUE(VT_dF2Vup));
  Ca_rel += tinc * (I_tr - I_rel) * (CELLMODEL_PARAMVALUE(VT_dF2Vrel));
  return -tinc *
         (I_SAC + I_fK + I_fNa + I_K + I_K1 + I_to + I_bNa + I_bCa + I_bK + I_NaK + I_NaCa + I_Na +
          I_siCa + I_siK - i_external) * (CELLMODEL_PARAMVALUE(VT_dC_m));
} // DiFrancescoNoble::Calc

void DiFrancescoNoble::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << y << ' ' << x << ' '
          << m << ' ' << h << ' '
          << d << ' ' << f << ' ' << f2 << ' '
          << r << ' ' << p << ' '
          << Ca_i << ' ' << Na_i << ' '
          << K_o << ' ' << K_i << ' '
          << Ca_up << ' ' << Ca_rel << ' ';
}

void DiFrancescoNoble::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  const double V_int = V * 1000.0;
  const double VmE_K = V_int - (CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_o / K_i);
  const double VmE_Ca = V_int - (CELLMODEL_PARAMVALUE(VT_RTd2F)) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / (double) Ca_i);
  const double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_i));
  const double VmE_mh =
      V_int - (CELLMODEL_PARAMVALUE(VT_RTdF)) * log(((CELLMODEL_PARAMVALUE(VT_Na_o)) + 0.12 * K_o) * (1.0 / (Na_i + 0.12 * K_i)));
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double ykdkk = y * K_o / (K_o + (CELLMODEL_PARAMVALUE(VT_K_mf)));
  const double NNNICO = (Na_i * Na_i * Na_i * (CELLMODEL_PARAMVALUE(VT_Ca_o)));
  const double NNNOCI = (CELLMODEL_PARAMVALUE(VT_NNNO)) * Ca_i;
  const double dff2P = d * f * f2 * (CELLMODEL_PARAMVALUE(VT_P_si));
  const double CPR = Ca_i * Ca_i;
  tempstr << ykdkk * (CELLMODEL_PARAMVALUE(VT_g_fK)) * VmE_K << ' '  // I_fK
          << ykdkk * (CELLMODEL_PARAMVALUE(VT_g_fNa)) * VmE_Na << ' ' // I_fNa
          << x * (CELLMODEL_PARAMVALUE(VT_I_Kmaxd)) * (K_i - K_o * pDFNP->expmV[Vi]) << ' ' // I_K
          << (CELLMODEL_PARAMVALUE(VT_g_k1)) * K_o / (K_o + (CELLMODEL_PARAMVALUE(VT_K_m1))) * VmE_K /
             (1.0 + exp((VmE_K + 10.0) * (CELLMODEL_PARAMVALUE(VT_F2dRT)))) << ' ' // I_K1
          << Ca_i / ((CELLMODEL_PARAMVALUE(VT_K_act4)) + Ca_i) * r * (.2 + K_o / ((CELLMODEL_PARAMVALUE(VT_K_mto)) + K_o)) *
             pDFNP->Vp10dexp[Vi] * (K_i - K_o * pDFNP->expm4V[Vi]) <<
          ' ' // I_to
          << (CELLMODEL_PARAMVALUE(VT_g_bNa)) * VmE_Na << ' ' // I_bNa
          << (CELLMODEL_PARAMVALUE(VT_g_bCa)) * VmE_Ca << ' ' // I_bCa
          << (CELLMODEL_PARAMVALUE(VT_g_bK)) * VmE_K << ' ' // I_bK
          << K_o / (K_o + (CELLMODEL_PARAMVALUE(VT_k_mK))) * (CELLMODEL_PARAMVALUE(VT_I_NaKmax)) * (Na_i / (Na_i + (CELLMODEL_PARAMVALUE(VT_k_mNa))))
          << ' ' // I_NaK
          << pDFNP->expstoch[Vi] * (NNNICO - pDFNP->expmstoch[Vi] * NNNOCI) /
             (1.0 + (CELLMODEL_PARAMVALUE(VT_d_NaCa)) * (NNNICO + NNNOCI)) << ' ' // I_NaCa
          << (CELLMODEL_PARAMVALUE(VT_g_Na)) * VmE_mh * m * m * m * h << ' ' // I_Na
          << 4.0 * dff2P * pDFNP->VdV2[Vi] * (Ca_i * pDFNP->exp50_2 - pDFNP->Cadexp[Vi])
          << ' ' // I_siCa
          << .01 * dff2P * pDFNP->VdV[Vi] * (K_i * pDFNP->exp50 - K_o * pDFNP->dexpV50[Vi])
          << ' ' // I_siK
          << ((CELLMODEL_PARAMVALUE(VT_F2VidCa_up)) * Ca_i * ((CELLMODEL_PARAMVALUE(VT_Ca_upmax)) - Ca_up) - (CELLMODEL_PARAMVALUE(VT_b_up)) * Ca_up)
          << ' ' // I_up
          << ((CELLMODEL_PARAMVALUE(VT_F2Vreldtrep)) * p * (Ca_up - Ca_rel)) << ' ' // I_tr
          << (CELLMODEL_PARAMVALUE(VT_F2Vreldtrel)) * Ca_rel * CPR / (CPR + (CELLMODEL_PARAMVALUE(VT_k_mCa))) << ' ' // I_rel
          << -(V_int - (CELLMODEL_PARAMVALUE(VT_Vr))) * (CELLMODEL_PARAMVALUE(VT_stretchfactor)) << ' '; // I_SAC
} // DiFrancescoNoble::LongPrint

void DiFrancescoNoble::GetParameterNames(vector<string> &getpara) {
  const int numpara = 15;
  const string ParaNames[numpara] =
      {"y", "x", "m", "h", "d", "f", "f2", "r", "p", "Ca_i", "Na_i", "K_o", "K_i", "Ca_up",
       "Ca_rel"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void DiFrancescoNoble::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 17;
  const string ParaNames[numpara] =
      {"I_fK", "I_fNa", "I_K", "I_K1", "I_to", "I_bNa",
       "I_bCa",
       "I_bK", "I_NaK",
       "I_NaCa", "I_Na", "I_siCa", "I_siK", "I_up", "I_tr",
       "I_rel",
       "I_SAC"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
