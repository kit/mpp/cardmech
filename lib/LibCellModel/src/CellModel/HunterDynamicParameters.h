/**@file HunterDynamicParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef HUNTERDYNAMIC_PARAMETERS
#define HUNTERDYNAMIC_PARAMETERS

#include <ParameterLoader.h>

namespace NS_HunterDynamicParameters {
enum varType {
  VT_TCaa = vtFirst,
  VT_Ca_bmax,
  VT_f,
  VT_g,
  VT_gamma,
  VT_dgamma,
  VT_pC50ref,
  VT_z0,
  VT_alpha,
  VT_beta0,
  VT_beta1,
  VT_beta2,
  VT_nref,
  VT_Fref,

  /*        VT_Fmax, */
  /*        VT_dFmax, */
  vtLast
};
}

using namespace NS_HunterDynamicParameters;

class HunterDynamicParameters : public vbNewForceParameters {
 public:
  HunterDynamicParameters(const char*);
  ~HunterDynamicParameters() {}

  // virtual inline int GetSize(void){return (&TCaa-&Fref)*sizeof(T);};
  // virtual inline T* GetBase(void){return Fref;};
  // virtual int GetNumParameters() { return 8; };
  void Init(const char*);
  void Calculate();
  void PrintParameters();
};

#endif // ifndef HUNTERDYNAMIC_PARAMETERS
