/**@file NobleEtAl.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <NobleEtAl.h>

void NobleEtAl::Init() {
#if KADEBUG
  cerr << "NobleEtAlBasis::Init\n";
#endif // if KADEBUG
  f = CELLMODEL_PARAMVALUE(VT_init_Basis_f);
  f2 = CELLMODEL_PARAMVALUE(VT_init_Basis_f2);
  f2ds = CELLMODEL_PARAMVALUE(VT_init_Basis_f2ds);
  x_r1 = CELLMODEL_PARAMVALUE(VT_init_Basis_x_r1);
  x_r2 = CELLMODEL_PARAMVALUE(VT_init_Basis_x_r2);
  x_s = CELLMODEL_PARAMVALUE(VT_init_Basis_x_s);
  s = CELLMODEL_PARAMVALUE(VT_init_Basis_s);
  f_activator = CELLMODEL_PARAMVALUE(VT_init_Basis_f_activator);
  f_product = CELLMODEL_PARAMVALUE(VT_init_Basis_f_product);
  K_o = CELLMODEL_PARAMVALUE(VT_init_Basis_K_o);
  Ca_i = CELLMODEL_PARAMVALUE(VT_init_Basis_Ca_i);
  Ca_calmod = CELLMODEL_PARAMVALUE(VT_init_Basis_Ca_calmod);
  Ca_troponin = CELLMODEL_PARAMVALUE(VT_init_Basis_Ca_troponin);
  Ca_ds = CELLMODEL_PARAMVALUE(VT_init_Basis_Ca_ds);
  Ca_up = CELLMODEL_PARAMVALUE(VT_init_Basis_Ca_up);
  Ca_rel = CELLMODEL_PARAMVALUE(VT_init_Basis_Ca_rel);

  m = CELLMODEL_PARAMVALUE(VT_init_m);
  h = CELLMODEL_PARAMVALUE(VT_init_h);
  d = CELLMODEL_PARAMVALUE(VT_init_d);
  r = CELLMODEL_PARAMVALUE(VT_init_r);
  Na_i = CELLMODEL_PARAMVALUE(VT_init_Na_i);
  K_i = CELLMODEL_PARAMVALUE(VT_init_K_i);
} // NobleEtAl::Init

#ifdef HETERO

bool NobleEtAl::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

bool NobleEtAl::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

ML_CalcType
NobleEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = 0, ML_CalcType stretch = 1.,
                int euler = 1) {
  const double V_int = V * 1000.0;

  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);

  const double VmE_K = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_o / K_i));
  const double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_i));
  const double VmE_Ca = V_int - (CELLMODEL_PARAMVALUE(VT_RTd2F)) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / (double) Ca_i);
  const double VmE_mh = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((K_o + (CELLMODEL_PARAMVALUE(VT_Na_ok3))) / (K_i + .03 * Na_i)));
  const double VmE_Namh =
      V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(((CELLMODEL_PARAMVALUE(VT_Na_o)) + .12 * K_o) / (Na_i + .12 * K_i)));

  // s+=tinc*(pNoeaP->a_s[Vi]-pNoeaP->b_s[Vi]*s);
  s = pNoeaP->s_inf[Vi] + (s - pNoeaP->s_inf[Vi]) * pNoeaP->exptau_s[Vi];

  // f+=tinc*(pNoeaP->a_f[Vi]-pNoeaP->b_f[Vi]*f);
  f = pNoeaP->f_inf[Vi] + (f - pNoeaP->f_inf[Vi]) * pNoeaP->exptau_f[Vi];
  f2 += tinc * (1.0 - Ca_i / (100000.0 + Ca_i) - f2);
  f2ds += tinc * (20.0 * (1 - Ca_ds / (0.001 + Ca_ds) - f2ds));

  // x_r1+=tinc*(pNoeaP->a_x_r1[Vi]-pNoeaP->b_x_r1[Vi]*x_r1);
  x_r1 = pNoeaP->x_r1_inf[Vi] + (x_r1 - pNoeaP->x_r1_inf[Vi]) * pNoeaP->exptau_x_r1[Vi];

  // x_r2+=tinc*(pNoeaP->a_x_r1[Vi]-pNoeaP->b_x_r2[Vi]*x_r2);
  x_r2 = pNoeaP->x_r2_inf[Vi] + (x_r2 - pNoeaP->x_r2_inf[Vi]) * pNoeaP->exptau_x_r2[Vi];

  // x_s+=tinc*(pNoeaP->a_x_s[Vi]-pNoeaP->b_x_s[Vi]*x_s);
  x_s = pNoeaP->x_s_inf[Vi] + (x_s - pNoeaP->x_s_inf[Vi]) * pNoeaP->exptau_x_s[Vi];

  // m+=tinc*(pNoeaP->a_m[Vi]-pNoeaP->b_m[Vi]*m);
  m = pNoeaP->m_inf[Vi] + (m - pNoeaP->m_inf[Vi]) * pNoeaP->exptau_m[Vi];

  // h+=tinc*(pNoeaP->a_h[Vi]-pNoeaP->b_h[Vi]*h);
  h = pNoeaP->h_inf[Vi] + (h - pNoeaP->h_inf[Vi]) * pNoeaP->exptau_h[Vi];

  // d+=tinc*(pNoeaP->a_d[Vi]-pNoeaP->b_d[Vi]*d);
  d = pNoeaP->d_inf[Vi] + (d - pNoeaP->d_inf[Vi]) * pNoeaP->exptau_d[Vi];
  r += tinc * 333.0 * (pNoeaP->array_r[Vi] - r);

  double r_activation = Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mCaCyt)));
  r_activation += (1.0 - r_activation) * Ca_ds / (Ca_ds + (CELLMODEL_PARAMVALUE(VT_k_mCaDS)));
  r_activation *= 500.0 * r_activation;
  const double r_inactivation = r_activation + 60.0;

  double f_precursor = 1.0 - f_activator - f_product;

  const double tinc_rel = (V_int < -50.0 ? tinc * 5.0 : tinc);

  f_precursor += tinc_rel * (.6 * f_product - f_precursor * r_activation);
  f_activator += tinc_rel * (f_precursor * r_activation - f_activator * r_inactivation);
  f_product += tinc_rel * (f_activator * r_inactivation - f_product * .6);

  const double T_isometric = GetT_isometric(tinc, stretch);

  const double I_K =
      ((CELLMODEL_PARAMVALUE(VT_G_k1)) * K_o / (K_o + (CELLMODEL_PARAMVALUE(VT_k_mk1))) / (1.0 + exp((VmE_K - 10.0) * (CELLMODEL_PARAMVALUE(VT_FdRT1k25)))) +
       s * r * (CELLMODEL_PARAMVALUE(VT_G_to)) +
       (x_r1 + (CELLMODEL_PARAMVALUE(VT_K_Kr)) * x_r2) * pNoeaP->KI_Kr[Vi]) * VmE_K;  // I_K1+I_to+I_Kr

  const double I_Ks = (CELLMODEL_PARAMVALUE(VT_G_Ks)) * x_s * x_s * VmE_mh;

  const double I_Na = (CELLMODEL_PARAMVALUE(VT_G_Na)) * m * m * m * h * VmE_Namh +
                      (pNoeaP->KIpNa[Vi] + (CELLMODEL_PARAMVALUE(VT_G_bNa))) * VmE_Na;  // I_Na+I_pNa+I_bN

  const double I_CaLDS = d * f * f2ds *
                         (pNoeaP->KCaLx[Vi] * ((K_i - K_o * pNoeaP->KCaLKhinten[Vi]) +
                                               5.0 * (Na_i - pNoeaP->KCaLNahinten[Vi])) +
                          pNoeaP->KCaLCa[Vi] *
                          (Ca_i - pNoeaP->KCaLCahinten[Vi]));  // I_CaLNa+I_CaLK+I_CaLCa

  const double I_bCa = (CELLMODEL_PARAMVALUE(VT_G_bCa)) * VmE_Ca;

  const double I_NaK =
      (CELLMODEL_PARAMVALUE(VT_I_NaKmax)) * K_o / (K_o + (CELLMODEL_PARAMVALUE(VT_k_mk))) * (Na_i / (Na_i + (CELLMODEL_PARAMVALUE(VT_k_mNa))));

  const double NNNI = (Na_i * Na_i * Na_i);
  const double I_NaCa =
      pNoeaP->KINaCaxg[Vi] * 999.0 * (NNNI - pNoeaP->KINaCaxg2[Vi] * Ca_i) / (.0068999998 + Ca_i);
  const double I_NaCads =
      pNoeaP->KINaCaxg[Vi] * (NNNI - pNoeaP->KINaCaxg2[Vi] * Ca_ds) / (.0068999998 + Ca_ds);

  const double I_up = (.4 * Ca_i - .03 * Ca_up * (CELLMODEL_PARAMVALUE(VT_k_kckxdk_s))) /
                      (Ca_i + Ca_up * (CELLMODEL_PARAMVALUE(VT_k_kckxdk_s)) + (CELLMODEL_PARAMVALUE(VT_k_kckx)) + (CELLMODEL_PARAMVALUE(VT_k_cyCa)));
  const double I_tr = 50.0 * (Ca_up - Ca_rel);

  double I_rel = f_activator / (f_activator + .25);
  I_rel = (I_rel * I_rel * 250.0 + GetSRLeak(T_isometric)) * Ca_rel;

  const double IK = (CELLMODEL_PARAMVALUE(VT_k_1dV_iF)) * (I_K + I_Ks - 2.0 * I_NaK);  // +I_CaLK
  K_o += tinc * (IK - .7 * (K_o - (CELLMODEL_PARAMVALUE(VT_K_b))));

  Na_i -= tinc * (I_Na + 3.0 * (I_NaK + I_NaCa)) * (CELLMODEL_PARAMVALUE(VT_k_1dV_iF));

  // K_i-=tinc*IK;
  K_i = 141.334;

  Ca_ds += tinc * (-I_CaLDS * (CELLMODEL_PARAMVALUE(VT_k_1dV_i2zF)) - Ca_ds * (CELLMODEL_PARAMVALUE(VT_k_decay)));
  Ca_up += tinc * ((CELLMODEL_PARAMVALUE(VT_k_V_idV_SRup)) * I_up - I_tr);
  Ca_rel += tinc * ((CELLMODEL_PARAMVALUE(VT_k_VupdVrel)) * I_tr - I_rel);

  const double Ca_calmod_ = 1e5 * Ca_i * ((CELLMODEL_PARAMVALUE(VT_Calmod)) - Ca_calmod) - 50.0 * Ca_calmod;
  const double Ca_indicator_ = .0;  // (-1e5*Ca_i-84.0)*Ca_indicator;
  const double Ca_troponin_ = GetKTrop(T_isometric) * ((CELLMODEL_PARAMVALUE(VT_Troponin)) - Ca_troponin) * Ca_i -
                              (CELLMODEL_PARAMVALUE(VT_b_troponin)) * Ca_troponin;

  Ca_calmod += tinc * Ca_calmod_;
  Ca_troponin += tinc * Ca_troponin_;

  // Ca_indicator+=tinc*Ca_indicator_;

  Ca_i += tinc *
          (-(CELLMODEL_PARAMVALUE(VT_k_1dV_i2F)) * (I_bCa - 2.0 * I_NaCa) - I_up + I_rel * (CELLMODEL_PARAMVALUE(VT_k_Vspez)) -
           Ca_calmod_ - Ca_troponin_ - Ca_indicator_ + Ca_ds);
  if (Ca_i <= 0.0)
    Ca_i = 1e-15;

  const double I_stretch = GetI_stretch(VmE_Ca, VmE_Na, VmE_K, V_int, stretch);

  return -tinc * (I_K + I_Ks + I_NaK + I_NaCa + I_NaCads + I_bCa + I_Na + I_CaLDS + I_stretch -
                  i_external) * (CELLMODEL_PARAMVALUE(VT_dC_m));
} // NobleEtAl::Calc

void NobleEtAl::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << m << ' ' << h << ' '
          << d << ' ' << f << ' '
          << f2 << ' ' << f2ds << ' ' << x_r1 << ' '
          << x_r2 << ' ' << x_s << ' '
          << r << ' ' << s << ' '
          << f_activator << ' ' << f_product << ' '
          << Na_i << ' ' << K_i << ' '
          << K_o << ' ' << Ca_i << ' '
          << Ca_calmod << ' ' << Ca_troponin << ' '
          << Ca_ds << ' ' // <<Ca_indicator<<' '
          << Ca_up << ' ' << Ca_rel << ' ';
}

void NobleEtAl::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  const double V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double VmE_K = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_o / K_i));
  const double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_i));
  const double VmE_Ca = V_int - (CELLMODEL_PARAMVALUE(VT_RTd2F)) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / (double) Ca_i);
  const double VmE_mh = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((K_o + (CELLMODEL_PARAMVALUE(VT_Na_ok3))) / (K_i + .03 * Na_i)));
  const double VmE_Namh =
      V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(((CELLMODEL_PARAMVALUE(VT_Na_o)) + .12 * K_o) / (Na_i + .12 * K_i)));
  double I_rel = f_activator / (f_activator + .25);

  Print(tempstr, t, V);
  tempstr << CELLMODEL_PARAMVALUE(VT_G_k1) * K_o / (K_o + CELLMODEL_PARAMVALUE(VT_k_mk1)) / (1.0 + exp((VmE_K - 10.0) * CELLMODEL_PARAMVALUE(VT_FdRT1k25))) *
             VmE_K << ' '
          << s * r * CELLMODEL_PARAMVALUE(VT_G_to) * VmE_K << ' '
          << (x_r1 + CELLMODEL_PARAMVALUE(VT_K_Kr) * x_r2) * pNoeaP->KI_Kr[Vi] * VmE_K << ' '
          << CELLMODEL_PARAMVALUE(VT_G_Ks) * x_s * x_s * VmE_mh << ' '
          << CELLMODEL_PARAMVALUE(VT_G_Na) * m * m * m * h * VmE_Namh << ' '
          << pNoeaP->KIpNa[Vi] * VmE_Na << ' '
          << CELLMODEL_PARAMVALUE(VT_G_bNa) * VmE_Na << ' '
          << d * f * f2ds * pNoeaP->KCaLx[Vi] * (K_i - K_o * pNoeaP->KCaLKhinten[Vi]) << ' '
          << d * f * f2ds * pNoeaP->KCaLx[Vi] * 5.0 * (Na_i - pNoeaP->KCaLNahinten[Vi]) << ' '
          << d * f * f2ds * pNoeaP->KCaLCa[Vi] * (Ca_i - pNoeaP->KCaLCahinten[Vi]) << ' '
          << CELLMODEL_PARAMVALUE(VT_G_bCa) * VmE_Ca << ' '
          << CELLMODEL_PARAMVALUE(VT_I_NaKmax) * K_o / (K_o + CELLMODEL_PARAMVALUE(VT_k_mk)) * (Na_i / (Na_i + (CELLMODEL_PARAMVALUE(VT_k_mNa)))) << ' '
          << pNoeaP->KINaCaxg[Vi] * 999.0 * ((Na_i * Na_i * Na_i) - pNoeaP->KINaCaxg2[Vi] * Ca_i) /
             (.0068999998 + Ca_i) << ' '
          << pNoeaP->KINaCaxg[Vi] * ((Na_i * Na_i * Na_i) - pNoeaP->KINaCaxg2[Vi] * Ca_ds) /
             (.0068999998 + Ca_ds) << ' '
          << (.4 * Ca_i - .03 * Ca_up * (CELLMODEL_PARAMVALUE(VT_k_kckxdk_s))) /
             (Ca_i + Ca_up * (CELLMODEL_PARAMVALUE(VT_k_kckxdk_s)) + (CELLMODEL_PARAMVALUE(VT_k_kckx)) + (CELLMODEL_PARAMVALUE(VT_k_cyCa))) << ' '
          << 50.0 * (Ca_up - Ca_rel) << ' '
          << (I_rel * I_rel * 250.0 + GetSRLeak(0.0)) * Ca_rel;
} // NobleEtAl::LongPrint

void NobleEtAl::GetParameterNames(vector<string> &getpara) {
  const int numpara = 22;
  const string ParaNames[numpara] =
      {"m", "h", "d", "f", "f2",
       "f2ds",
       "x_r1",
       "x_r2",
       "x_s", "r", "s", "f_activator",
       "f_product",
       "Na_i", "K_i", "K_o", "Ca_i", "Ca_calmod",
       "Ca_troponin", "Ca_ds",
       "Ca_up",
       "Ca_rel"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void NobleEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 17;
  const string ParaNames[numpara] =
      {"I_K1", "I_to", "I_Kr", "I_Ks", "I_Na", "I_pNa",
       "I_bNa",
       "I_CaLNa",
       "I_CaLK", "I_CaLCa",
       "I_bCa", "I_NaK", "I_NaCa", "I_NaCads", "I_up", "I_tr",
       "I_rel"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
