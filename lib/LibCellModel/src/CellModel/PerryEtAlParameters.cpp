/*      File: PerryEtAlParameters.cpp
   automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
   Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
   send comments to dw@ibt.uka.de       */


#include <PerryEtAlParameters.h>

PerryEtAlParameters::PerryEtAlParameters(const char *initFile) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile);
}

PerryEtAlParameters::~PerryEtAlParameters() {
  // Destruktor
}

void PerryEtAlParameters::PrintParameters() {
  // print the parameter to the stdout
#if KADEBUG
  cerr << "PerryEtAlParameters:PrintParameters " << endl;
#endif // if KADEBUG

  cout << "PerryEtAlParameters:" << endl;
  for (int i = vtFirst; i < vtLast; i++)
    cout << P[i].name << "\t = " << P[i].value << endl;
}

void PerryEtAlParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "PerryEtAlParameters:init " << initFile << endl;
#endif // if KADEBUG

  // Initialization of the Parameters ...
  P[VT_Tx].name = "Tx";
  P[VT_Acap].name = "Acap";
  P[VT_Vmyo].name = "Vmyo";
  P[VT_Ko].name = "Ko";
  P[VT_Ki].name = "Ki";
  P[VT_RPR].name = "RPR";
  P[VT_C0Kr].name = "C0Kr";
  P[VT_C1Kr].name = "C1Kr";
  P[VT_C2Kr].name = "C2Kr";
  P[VT_OKr].name = "OKr";
  P[VT_IKr].name = "IKr";
  P[VT_C0Kr2].name = "C0Kr2";
  P[VT_C1Kr2].name = "C1Kr2";
  P[VT_C2Kr2].name = "C2Kr2";
  P[VT_OKr2].name = "OKr2";
  P[VT_IKr2].name = "IKr2";
  P[VT_GKr].name = "GKr";
  P[VT_Kra0a].name = "Kra0a";
  P[VT_Kra0z].name = "Kra0z";
  P[VT_Krb0a].name = "Krb0a";
  P[VT_Krb0z].name = "Krb0z";
  P[VT_Krfa].name = "Krfa";
  P[VT_Krfz].name = "Krfz";
  P[VT_Krba].name = "Krba";
  P[VT_Krbz].name = "Krbz";
  P[VT_Kra1a].name = "Kra1a";
  P[VT_Kra1z].name = "Kra1z";
  P[VT_Krb1a].name = "Krb1a";
  P[VT_Krb1z].name = "Krb1z";
  P[VT_Kraia].name = "Kraia";
  P[VT_Kraiz].name = "Kraiz";
  P[VT_Krbia].name = "Krbia";
  P[VT_Krbiz].name = "Krbiz";
  P[VT_Kracia].name = "Kracia";
  P[VT_Kraciz].name = "Kraciz";
  P[VT_Krbcia].name = "Krbcia";
  P[VT_Krbciz].name = "Krbciz";
  P[VT_Kra1a2].name = "Kra1a2";
  P[VT_Krb1a2].name = "Krb1a2";
  P[VT_Kraia2].name = "Kraia2";
  P[VT_Kraiz2].name = "Kraiz2";
  P[VT_Kra12].name = "Kra12";
  P[VT_Krb12].name = "Krb12";
  P[VT_Krexp12].name = "Krexp12";
  P[VT_Amp].name = "Amp";


  ParameterLoader EPL(initFile, EMT_PerryEtAl);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  // End Initialization of the Parameters ...

  Calculate();
} // PerryEtAlParameters::Init

void PerryEtAlParameters::InitTable() {}

void PerryEtAlParameters::Calculate() {
#if KADEBUG
  cerr << "PerryEtAlParameters:Calculate " << endl;
#endif // if KADEBUG

  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
}
