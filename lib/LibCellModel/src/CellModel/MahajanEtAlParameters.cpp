/* File: MahajanEtAlParameters.cpp
        automatically created by CellML2Elphymodel.pl
        Institute of Biomedical Engineering, Universität Karlsruhe (TH) */

#include <MahajanEtAlParameters.h>
MahajanEtAlParameters::MahajanEtAlParameters(const char *initFile, ML_CalcType tinc) {
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

MahajanEtAlParameters::~MahajanEtAlParameters() {}

void MahajanEtAlParameters::PrintParameters() {
  cout << "MahajanEtAlParameters:"<<endl;
  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= "     << P[i].value <<endl;
  }
}

void MahajanEtAlParameters::Init(const char *initFile, ML_CalcType tinc) {
  P[VT_R].name              = "R";
  P[VT_T].name              = "T";
  P[VT_F].name              = "F";
  P[VT_K_o].name            = "K_o";
  P[VT_Ca_o].name           = "Ca_o";
  P[VT_Na_o].name           = "Na_o";
  P[VT_wca].name            = "wca";
  P[VT_stim_offset].name    = "stim_offset";
  P[VT_stim_period].name    = "stim_period";
  P[VT_stim_duration].name  = "stim_duration";
  P[VT_stim_amplitude].name = "stim_amplitude";
  P[VT_gna].name            = "gna";
  P[VT_gca].name            = "gca";
  P[VT_pca].name            = "pca";
  P[VT_vth].name            = "vth";
  P[VT_s6].name             = "s6";
  P[VT_vx].name             = "vx";
  P[VT_sx].name             = "sx";
  P[VT_vy].name             = "vy";
  P[VT_sy].name             = "sy";
  P[VT_vyr].name            = "vyr";
  P[VT_syr].name            = "syr";
  P[VT_cat].name            = "cat";
  P[VT_cpt].name            = "cpt";
  P[VT_k2].name             = "k2";
  P[VT_k1t].name            = "k1t";
  P[VT_k2t].name            = "k2t";
  P[VT_r1].name             = "r1";
  P[VT_r2].name             = "r2";
  P[VT_s1t].name            = "s1t";
  P[VT_tca].name            = "tca";
  P[VT_taupo].name          = "taupo";
  P[VT_tau3].name           = "tau3";
  P[VT_gkix].name           = "gkix";
  P[VT_gkr].name            = "gkr";
  P[VT_gks].name            = "gks";
  P[VT_gtos].name           = "gtos";
  P[VT_gtof].name           = "gtof";
  P[VT_gNaK].name           = "gNaK";
  P[VT_xkmko].name          = "xkmko";
  P[VT_xkmnai].name         = "xkmnai";
  P[VT_gNaCa].name          = "gNaCa";
  P[VT_xkdna].name          = "xkdna";
  P[VT_xmcao].name          = "xmcao";
  P[VT_xmnao].name          = "xmnao";
  P[VT_xmnai].name          = "xmnai";
  P[VT_xmcai].name          = "xmcai";
  P[VT_cstar].name          = "cstar";
  P[VT_gryr].name           = "gryr";
  P[VT_gbarsr].name         = "gbarsr";
  P[VT_gdyad].name          = "gdyad";
  P[VT_ax].name             = "ax";
  P[VT_ay].name             = "ay";
  P[VT_av].name             = "av";
  P[VT_taua].name           = "taua";
  P[VT_taur].name           = "taur";
  P[VT_cup].name            = "cup";
  P[VT_kj].name             = "kj";
  P[VT_vup].name            = "vup";
  P[VT_gleak].name          = "gleak";
  P[VT_bcal].name           = "bcal";
  P[VT_xkcal].name          = "xkcal";
  P[VT_srmax].name          = "srmax";
  P[VT_srkd].name           = "srkd";
  P[VT_bmem].name           = "bmem";
  P[VT_kmem].name           = "kmem";
  P[VT_bsar].name           = "bsar";
  P[VT_ksar].name           = "ksar";
  P[VT_xkon].name           = "xkon";
  P[VT_xkoff].name          = "xkoff";
  P[VT_btrop].name          = "btrop";
  P[VT_taud].name           = "taud";
  P[VT_taups].name          = "taups";
  P[VT_K_i].name            = "K_i";
  P[VT_prNaK].name          = "prNaK";
  P[VT_FonRT].name          = "FonRT";
  P[VT_s2t].name            = "s2t";
  P[VT_sigma].name          = "sigma";
  P[VT_bv].name             = "bv";
  P[VT_ek].name             = "ek";
  P[VT_V_init].name         = "V_init";
  P[VT_xm_init].name        = "xm_init";
  P[VT_xh_init].name        = "xh_init";
  P[VT_xj_init].name        = "xj_init";
  P[VT_Ca_dyad_init].name   = "Ca_dyad_init";
  P[VT_c1_init].name        = "c1_init";
  P[VT_c2_init].name        = "c2_init";
  P[VT_xi1ca_init].name     = "xi1ca_init";
  P[VT_xi1ba_init].name     = "xi1ba_init";
  P[VT_xi2ca_init].name     = "xi2ca_init";
  P[VT_xi2ba_init].name     = "xi2ba_init";
  P[VT_xr_init].name        = "xr_init";
  P[VT_Ca_i_init].name      = "Ca_i_init";
  P[VT_xs1_init].name       = "xs1_init";
  P[VT_xs2_init].name       = "xs2_init";
  P[VT_xtos_init].name      = "xtos_init";
  P[VT_ytos_init].name      = "ytos_init";
  P[VT_xtof_init].name      = "xtof_init";
  P[VT_ytof_init].name      = "ytof_init";
  P[VT_Na_i_init].name      = "Na_i_init";
  P[VT_Ca_submem_init].name = "Ca_submem_init";
  P[VT_Ca_NSR_init].name    = "Ca_NSR_init";
  P[VT_Ca_JSR_init].name    = "Ca_JSR_init";
  P[VT_xir_init].name       = "xir_init";
  P[VT_tropi_init].name     = "tropi_init";
  P[VT_trops_init].name     = "trops_init";
  P[VT_cat3].name           = "cat3";
  P[VT_cptm4].name          = "cptm4";
  P[VT_r1r2k2].name         = "r1r2k2";
  P[VT_xkdna3].name         = "xkdna3";
  P[VT_xmnai3].name         = "xmnai3";


  P[VT_FonRT].readFromFile  = false;
  P[VT_s2t].readFromFile    = false;
  P[VT_sigma].readFromFile  = false;
  P[VT_bv].readFromFile     = false;
  P[VT_ek].readFromFile     = false;
  P[VT_cat3].readFromFile   = false;
  P[VT_cptm4].readFromFile  = false;
  P[VT_r1r2k2].readFromFile = false;
  P[VT_xkdna3].readFromFile = false;
  P[VT_xmnai3].readFromFile = false;


  ParameterLoader EPL(initFile, EMT_MahajanEtAl);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);
  Calculate();
  InitTable(tinc);
} // MahajanEtAlParameters::Init

void MahajanEtAlParameters::Calculate() {
  P[VT_FonRT].value  = (P[VT_F].value)/(P[VT_R].value*P[VT_T].value);
  P[VT_s2t].value    = ( (( (P[VT_s1t].value)*P[VT_r1].value)/P[VT_r2].value)*P[VT_k2t].value)/P[VT_k1t].value;
  P[VT_sigma].value  = ((exp(((P[VT_Na_o].value)/67.3000))) - 1.00000)/7.00000;
  P[VT_bv].value     =  (1.00000 - (P[VT_av].value))*P[VT_cstar].value - 50.0000;
  P[VT_ek].value     =  (1.00000/(P[VT_FonRT].value))*(log((P[VT_K_o].value/P[VT_K_i].value)));
  P[VT_cat3].value   = (P[VT_cat].value)*(P[VT_cat].value)*(P[VT_cat].value);
  P[VT_cptm4].value  = (1/(P[VT_cpt].value))*(1/(P[VT_cpt].value))*(1/(P[VT_cpt].value))*(1/(P[VT_cpt].value));
  P[VT_r1r2k2].value = (P[VT_r1].value)/(P[VT_r2].value)*P[VT_k2].value;
  P[VT_xkdna3].value = (P[VT_xkdna].value)*(P[VT_xkdna].value)*(P[VT_xkdna].value);
  P[VT_xmnai3].value = ((P[VT_xmnai].value))*(P[VT_xmnai].value)*(P[VT_xmnai].value);
}

void MahajanEtAlParameters::InitTable(ML_CalcType tinc) {
  ML_CalcType HT = tinc * 1000;

  for (double V = -RangeTabhalf+.0001; V < RangeTabhalf; V += dDivisionTab) {
    const int Vi = (int)(DivisionTab*(RangeTabhalf+V)+.5);
    ah[Vi] = (V < -40.0000 ?  0.135000*(exp(((80.0000+V)/ -6.80000))) : 0.00000);
    bh[Vi] =
      (V <
       -40.0000 ?  3.56000*(exp((0.0790000*V)))+ 310000.*(exp((0.350000*V))) : 1.00000/
       (0.130000*(1.00000+(exp(((V+10.6600)/ -11.1000))))));
    aj[Vi] =
      (V <
       -40.0000 ? ( (-127140.*(exp((0.244400*V))) -  3.47400e-05*(exp((-0.0439100*V))))*1.00000*(V+37.7800))/
       (1.00000+(exp((0.311000*(V+79.2300))))) : 0.00000);
    bj[Vi] =
      (V <
       -40.0000 ? (0.121200*(exp((-0.0105200*V))))/
       (1.00000+(exp((-0.137800*(V+40.1400))))) : (0.300000*(exp((-2.53500e-07*V))))/
       (1.00000+(exp((-0.100000*(V+32.0000))))));
    am[Vi] =
      ((fabs((V+47.1300))) >
       0.00100000 ? (0.320000*1.00000*(V+47.1300))/(1.00000 - (exp((-0.100000*(V+47.1300))))) : 3.20000);
    bm[Vi]     =  0.0800000*(exp((-V/11.0000)));
    xs1ss[Vi]  = 1.00000/(1.00000+(exp((-(V - 1.50000)/16.7000))));
    tauxs1[Vi] =
      ((fabs((V+30.0000))) <
       0.00100000/0.0687000 ? 1.00000/(7.19000e-05/0.148000+0.000131000/0.0687000) : 1.00000/
       ((7.19000e-05*(V+30.0000))/(1.00000 - (exp((-0.148000*(V+30.0000)))))+(0.000131000*(V+30.0000))/
        ((exp((0.0687000*(V+30.0000)))) - 1.00000)));
    const ML_CalcType xkrv1 =
      ((fabs((V+7.00000))) >
       0.00100000 ? (0.00138000*1.00000*(V+7.00000))/(1.00000 - (exp((-0.123000*(V+7.00000))))) : 0.00138000/0.123000);
    const ML_CalcType xkrv2 =
      ((fabs((V+10.0000))) >
       0.00100000 ? (0.000610000*1.00000*(V+10.0000))/((exp((0.145000*(V+10.0000)))) - 1.00000) : 0.000610000/0.145000);
    xkrinf[Vi] = 1.00000/(1.00000+(exp((-(V+50.0000)/7.50000))));
    const ML_CalcType rt1   = -(V+3.00000)/15.0000;
    const ML_CalcType rt4   = ( (-V/30.0000)*V)/30.0000;
    const ML_CalcType poinf = 1.00000/(1.00000+(exp((-(V - (P[VT_vth].value))/(P[VT_s6].value)))));
    recov[Vi] = 10.0000+ 4954.00*(exp((V/15.6000)));
    Pr[Vi]    = 1.00000 - 1.00000/(1.00000+(exp((-(V - (P[VT_vy].value))/(P[VT_sy].value)))));
    Ps[Vi]    = 1.00000/(1.00000+(exp((-(V - (P[VT_vyr].value))/(P[VT_syr].value)))));
    const ML_CalcType poi = 1.00000/(1.00000+(exp((-(V - (P[VT_vx].value))/(P[VT_sx].value)))));
    const ML_CalcType rt2 = (V+33.5000)/10.0000;
    const ML_CalcType rt3 = (V+60.0000)/10.0000;
    const ML_CalcType rt5 = (V+33.5000)/10.0000;
    za[Vi]     =  V*2.00000*(P[VT_FonRT].value);
    sparkV[Vi] = (exp((-(P[VT_ay].value)*(V+30.0000))))/(1.00000+(exp((-(P[VT_ay].value)*(V+30.0000)))));
    zw4[Vi]    = 1.00000+ 0.200000*(exp((V*(0.350000 - 1.00000)*(P[VT_FonRT].value))));
    fNaK[Vi]   = 1.00000/
      (1.00000+ 0.124500*(exp((-0.100000*V*(P[VT_FonRT].value))))+ 0.0365000*(P[VT_sigma].value)*
       (exp((-V*(P[VT_FonRT].value)))));
    aki[Vi] = 1.02000/(1.00000+(exp((0.238500*((V - (P[VT_ek].value)) - 59.2150)))));
    bki[Vi] =
      (0.491240*(exp((0.0803200*((V - (P[VT_ek].value))+5.47600))))+ 1.00000*
       (exp((0.0617500*((V - (P[VT_ek].value)) - 594.310)))))/
      (1.00000+(exp((-0.514300*((V - (P[VT_ek].value))+4.75300)))));
    rg[Vi]       = 1.00000/(1.00000+(exp(((V+33.0000)/22.4000))));
    taukr[Vi]    = 1.00000/(xkrv1+xkrv2);
    tauxs2[Vi]   =  4.00000*tauxs1[Vi];
    xtos_inf[Vi] = 1.00000/(1.00000+(exp(rt1)));
    txs[Vi]      = 9.00000/(1.00000+(exp((-rt1))))+0.500000;
    txf[Vi]      =  3.50000*(exp(rt4))+1.50000;
    alpha[Vi]    = poinf/(P[VT_taupo].value);
    beta[Vi]     = (1.00000 - poinf)/(P[VT_taupo].value);
    tauba[Vi]    =  (recov[Vi] - 450.000)*Pr[Vi]+450.000;
    k3[Vi]       = (1.00000 - poi)/(P[VT_tau3].value);
    ytos_inf[Vi] = 1.00000/(1.00000+(exp(rt2)));
    tys[Vi]      = 3000.00/(1.00000+(exp(rt3)))+30.0000;
    tyf[Vi]      = 20.0000/(1.00000+(exp(rt5)))+20.0000;
    xkin[Vi]     = (aki[Vi])/(aki[Vi]+bki[Vi]);
    rs_inf[Vi]   = 1.00000/(1.00000+(exp(rt2)));
    comp[Vi]     = (P[VT_gbarsr].value)*(exp((-(P[VT_ax].value)*(V+30.0000))))/
      (1.00000+(exp((-(P[VT_ax].value)*(V+30.0000)))));
    comp2[Vi] =  ((P[VT_Na_o].value)*(P[VT_Na_o].value)*(P[VT_Na_o].value))*
      (exp((V*(0.350000 - 1.00000)*(P[VT_FonRT].value))));
  }
} // MahajanEtAlParameters::InitTable
