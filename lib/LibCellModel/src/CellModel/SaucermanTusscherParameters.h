/*! \file SaucermanTusscherParamters.h
   \human cell model of ten Tusscher including Saucermans intracellular model adrenergic signaling

   \version 0.0.0

   \date Created Carola Otto (21.02.2009)\n
   template (00.00.00)\n
   man Gunnar Seemann (27.02.03)\n
   Last Modified Eike Wuelfers (03.02.10)

   \author Carola Otto\n
   Institute of Biomedical Engineering\n
   Universitaet Karlsruhe (TH)\n
   http://www.ibt.uni-karlsruhe.de\n
   Copyright 2000-2009 - All rights reserved.

   // \sa  \ref SaucermanTusscher
 */

#ifndef SAUCERMANTUSSCHERPARAMETERS_H
#define SAUCERMANTUSSCHERPARAMETERS_H

#include <ParameterLoader.h>

// #include <TenTusscher2IschemiaSetup.h>

namespace NS_SaucermanTusscherParameters {
  enum varType {
    VT_R = vtFirst,
    VT_Tx,
    VT_F,
    VT_m_init,
    VT_h_init,
    VT_j_init,
    VT_xr1_init,
    VT_xr2_init,
    VT_xs_init,
    VT_r_init,
    VT_s_init,
    VT_d_init,
    VT_f_init,
    VT_f2_init,
    VT_fCa_init,
    VT_Rq_init,
    VT_O_init,
    VT_K_o,
    VT_Ca_o,
    VT_Na_o,
    VT_Vc,
    VT_Vsr,
    VT_Vss,
    VT_Vrel,
    VT_ks1,
    VT_ks2,
    VT_k3,
    VT_k4,
    VT_EC,
    VT_max_sr,
    VT_min_sr,
    VT_Vxfer,
    VT_Bufc,
    VT_Kbufc,
    VT_Bufsr,
    VT_Kbufsr,
    VT_Bufss,
    VT_Kbufss,
    VT_taug,
    VT_Vmaxup,
    VT_Kup,
    VT_C,
    VT_g_Kr,
    VT_pKNa,
    VT_g_Ks,
    VT_g_K1,
    VT_g_to,
    VT_g_Na,
    VT_g_bNa,
    VT_KmK,
    VT_KmNa,
    VT_knak,
    VT_g_CaL,
    VT_g_bCa,
    VT_kNaCa,
    VT_KmNai,
    VT_KmCa,
    VT_ksat,
    VT_n,
    VT_g_pCa,
    VT_KpCa,
    VT_g_pK,
    VT_V_init,
    VT_Cai_init,
    VT_CaSR_init,
    VT_CaSS_init,
    VT_Nai_init,
    VT_Ki_init,
    VT_lenght,
    VT_radius,
    VT_pi,
    VT_CaiMin,
    VT_CaiMax,
    VT_s_inf_vHalf,
    VT_tau_s_f1,
    VT_tau_s_slope1,
    VT_tau_s_vHalf1,
    VT_tau_s_enable,
    VT_vol,
    VT_vi,
    VT_inverseviF,
    VT_inverseviF2,
    VT_inversevssF2,
    VT_volforCall,
    VT_INVERSECAPACITANCE,
    VT_VcdVsr,
    VT_Kupsquare,
    VT_BufcPKbufc,
    VT_Kbufcsquare,
    VT_Kbufc2,
    VT_BufsrPKbufsr,
    VT_Kbufsrsquare,
    VT_Kbufsr2,
    VT_KopKNaNao,
    VT_KmNai3,
    VT_Nao3,
    VT_StepCai,
    VT_inverseRTONF,
    VT_RTONF,
    VT_InitTableDone,
    VT_tincforTab,
    VT_tau_s_add,

    // Adrenergic Signaling
    // b-AR/ Gs module
    VT_Ltotmax,  // apply isoproterenol here
    VT_Gstot,
    VT_Kl,
    VT_Kr,
    VT_Kc,
    VT_k_barkp,
    VT_k_barkm,
    VT_k_pkap,
    VT_k_pkam,
    VT_k_gact,
    VT_k_hyd,
    VT_k_reassoc,

    // cAMP module
    VT_AC_tot,
    VT_ATP,
    VT_PDE3tot,
    VT_PDE4tot,
    VT_IBMXtot,
    VT_Fsktot,
    VT_k_ac_basal,
    VT_k_ac_gsa,
    VT_k_ac_fsk,
    VT_Km_basal,
    VT_Km_gsa,
    VT_Km_fsk,
    VT_Kgsa,
    VT_Kfsk,
    VT_k_pde3,
    VT_Km_pde3,
    VT_k_pde4,
    VT_Km_pde4,
    VT_Ki_ibmx,

    // PKA module
    VT_PKAItot,
    VT_PKAIItot,
    VT_PKItot,
    VT_Ka,
    VT_Kb,
    VT_Kd,
    VT_Ki_pki,

    // PLB module
    VT_epsilon,
    VT_PLBtot,
    VT_PP1tot,
    VT_Inhib1tot,
    VT_k_pka_plb,
    VT_Km_pka_plb,
    VT_k_pp1_plb,
    VT_Km_pp1_plb,
    VT_k_pka_i1,
    VT_Km_pka_i1,
    VT_Vmax_pp2a_i1,
    VT_Km_pp2a_i1,
    VT_Ki_inhib1,

    // LCC module
    VT_LCCtot,
    VT_PKAIIlcctot,
    VT_PP1lcctot,
    VT_PP2Alcctot,
    VT_k_pka_lcc,
    VT_Km_pka_lcc,
    VT_k_pp1_lcc,
    VT_Km_pp1_lcc,
    VT_k_pp2a_lcc,
    VT_Km_pp2a_lcc,

    // RyR module
    VT_RyRtot,
    VT_PKAIIryrtot,
    VT_PP1ryr,
    VT_PP2Aryr,
    VT_kcat_pka_ryr,
    VT_Km_pka_ryr,
    VT_kcat_pp1_ryr,
    VT_Km_pp1_ryr,
    VT_kcat_pp2a_ryr,
    VT_Km_pp2a_ryr,

    // TnI module
    VT_TnItot,
    VT_PP2Atni,
    VT_kcat_pka_tni,
    VT_Km_pka_tni,
    VT_kcat_pp2a_tni,
    VT_Km_pp2a_tni,

    // Iks module
    VT_Iks_tot,
    VT_Yotiao_tot,
    VT_K_yotiao,  // apply G589D mutation here
    VT_PKAII_ikstot,
    VT_PP1_ikstot,
    VT_k_pka_iks,
    VT_Km_pka_iks,
    VT_k_pp1_iks,
    VT_Km_pp1_iks,

    // Signaling pathway variables
    VT_L_init,
    VT_R_init,
    VT_Gs_init,
    VT_b1ARd_init,
    VT_b1ARtot_init,
    VT_b1ARp_init,
    VT_Gsagtptot_init,
    VT_Gsagdp_init,
    VT_Gsbg_init,
    VT_Gsa_gtp_init,
    VT_AC_init,
    VT_Fsk_init,
    VT_PDE_init,
    VT_IBMX_init,
    VT_cAMPtot_init,
    VT_cAMP_init,
    VT_PKACI_init,
    VT_PKACII_init,
    VT_PLBs_init,
    VT_Inhib1ptot_init,
    VT_Inhib1p_init,
    VT_PP1_init,
    VT_LCCap_init,
    VT_LCCbp_init,
    VT_RyRp_init,
    VT_TnIp_init,
    VT_Iks_init,
    VT_Yotiao_init,
    VT_Iksp_init,
    VT_t_init,
    VT_xs05_init,
    VT_trel_init,
    VT_abstol,
    VT_reltol,
    VT_hmax,
    VT_mxsteps,
    VT_p_PLB,

    // INaK module
    VT_fac_KmNa,
    VT_fac_knak,
    VT_Amp,
#ifdef ACTIVATE_IKATP_CHANNEL
    VT_f_T,
    VT_nicholsarea,
    VT_gkatp,
    VT_gammaconst,
    VT_Mgi,
    VT_atpi,
    VT_adpi,
    VT_Km_factor,
# ifdef ISCHEMIA
    VT_IschemiaStart,
    VT_IschemiaStage1,
    VT_IschemiaStage2,
    VT_ZoneFactor,
    VT_DiffusionFactor,
    VT_RestoreIschemia,
    VT_Ko_ZoneFactor_Begin,
    VT_Ko_ZoneFactor_End,
    VT_fpH_ZoneFactor_Begin,
    VT_fpH_ZoneFactor_End,
    VT_pO_ZoneFactor_Begin,
    VT_pO_ZoneFactor_End,
    VT_K_o_stage1,
    VT_K_o_stage2,
    VT_gCaL_stage1,
    VT_gCaL_stage2,
    VT_gNa_stage1,
    VT_gNa_stage2,
    VT_Mgi_stage1,
    VT_Mgi_stage2,
    VT_ATP_stage1,
    VT_ATP_stage2,
    VT_ADP_stage1,
    VT_ADP_stage2,
    VT_dVmNa_stage0,
    VT_dVmNa_stage1,
    VT_dVmNa_stage2,
# endif // ifdef ISCHEMIA
#endif // ifdef ACTIVATE_IKATP_CHANNEL
    vtLast
  };
} // namespace NS_SaucermanTusscherParameters

using namespace NS_SaucermanTusscherParameters;

class SaucermanTusscherParameters : public vbNewElphyParameters {
public:
  SaucermanTusscherParameters(const char *);

  ~SaucermanTusscherParameters();

  void PrintParameters();

  void Calculate();

  void InitTable();

  void Init(const char *);

  void InitTableWithtinc(ML_CalcType);

  ML_CalcType rec_ipK[RTDT];
  ML_CalcType d_inf[RTDT];
  ML_CalcType f_inf[RTDT];
  ML_CalcType f2_inf[RTDT];
  ML_CalcType m_inf[RTDT];
  ML_CalcType h_inf[RTDT];
  ML_CalcType j_inf[RTDT];
  ML_CalcType Xr1_inf[RTDT];
  ML_CalcType Xr2_inf[RTDT];
  ML_CalcType Xs_inf[RTDT];
  ML_CalcType r_inf[RTDT];
  ML_CalcType s_inf[RTDT];
  ML_CalcType NaCa_P1[RTDT];
  ML_CalcType NaCa_P2[RTDT];
  ML_CalcType NaK_P1[RTDT];
  ML_CalcType CaL_P1[RTDT];
  ML_CalcType CaL_P2[RTDT];
  ML_CalcType exptau_m[RTDT];
  ML_CalcType exptau_h[RTDT];
  ML_CalcType exptau_j[RTDT];
  ML_CalcType exptau_Xr1[RTDT];
  ML_CalcType exptau_Xr2[RTDT];
  ML_CalcType exptau_Xs[RTDT];
  ML_CalcType exptau_s[RTDT];
  ML_CalcType exptau_r[RTDT];
  ML_CalcType exptau_d[RTDT];
  ML_CalcType exptau_f[RTDT];
  ML_CalcType exptau_f2[RTDT];
  ML_CalcType tau_m[RTDT];
  ML_CalcType tau_h[RTDT];
  ML_CalcType tau_j[RTDT];
  ML_CalcType tau_Xr1[RTDT];
  ML_CalcType tau_Xr2[RTDT];
  ML_CalcType tau_Xs[RTDT];
  ML_CalcType tau_s[RTDT];
  ML_CalcType tau_r[RTDT];
  ML_CalcType tau_d[RTDT];
  ML_CalcType tau_f[RTDT];
  ML_CalcType tau_f2[RTDT];
  ML_CalcType ECA[RTDT];
  ML_CalcType KhNa[RTDT];
}; // class SaucermanTusscherParameters
#endif // ifndef SAUCERMANTUSSCHERPARAMETERS_H
