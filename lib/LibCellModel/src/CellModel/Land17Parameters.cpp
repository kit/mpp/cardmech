// created by Robin Moss

// based on the available Matlab code from cemrg.co.uk

#include <Land17Parameters.h>

Land17Parameters::Land17Parameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void Land17Parameters::PrintParameters() {
  cout << "Land17Parameters:" << endl;
  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void Land17Parameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "Loading the Land17 parameter from " << initFile << " ...\n";
#endif  // if KADEBUG
  P[VT_perm50].name = "perm50";
  P[VT_TRPN_n].name = "TRPN_n";
  P[VT_koff].name = "koff";
  P[VT_dr].name = "dr";
  P[VT_wfrac].name = "wfrac";
  P[VT_TOT_A].name = "TOT_A";
  P[VT_ktm_unblock].name = "ktm_unblock";
  P[VT_beta_1].name = "beta_1";
  P[VT_beta_0].name = "beta_0";
  P[VT_gamma].name = "gamma";
  P[VT_gamma_wu].name = "gamma_wu";
  P[VT_phi].name = "phi";
  P[VT_par_k].name = "par_k";
  P[VT_b].name = "b";
  P[VT_eta_l].name = "eta_l";
  P[VT_eta_s].name = "eta_s";
  P[VT_a].name = "a";
  P[VT_skinned].name = "skinned";
  P[VT_passive_contribution].name = "passive_contribution";
  P[VT_nperm_skinned].name = "nperm_skinned";
  P[VT_ca50_skinned].name = "ca50_skinned";
  P[VT_Tref_skinned].name = "Tref_skinned";
  P[VT_nu_skinned].name = "nu_skinned";
  P[VT_mu_skinned].name = "mu_skinned";
  P[VT_nperm_intact].name = "nperm_intact";
  P[VT_ca50_intact].name = "ca50_intact";
  P[VT_Tref_intact].name = "Tref_intact";
  P[VT_nu_intact].name = "nu_intact";
  P[VT_mu_intact].name = "mu_intact";
  P[VT_k_ws].name = "VT_k_ws";
  P[VT_k_uw].name = "VT_k_uw";
  P[VT_cdw].name = "VT_cdw";
  P[VT_cds].name = "VT_cds";
  P[VT_k_wu].name = "VT_k_wu";
  P[VT_k_su].name = "VT_k_su";
  P[VT_A].name = "VT_A";
  P[VT_nperm].name = "VT_nperm";
  P[VT_ca50].name = "VT_ca50";
  P[VT_Tref].name = "VT_Tref";
  P[VT_nu].name = "VT_nu";
  P[VT_mu].name = "VT_mu";
  P[VT_XS_init].name = "XS_init";
  P[VT_XW_init].name = "XW_init";
  P[VT_CaTRPN_init].name = "CaTRPN_init";
  P[VT_B_init].name = "B_init";
  P[VT_ZETAS_init].name = "ZETAS_init";
  P[VT_ZETAW_init].name = "ZETAW_init";
  P[VT_Cd_init].name = "Cd_init";


  P[VT_k_ws].readFromFile = false;
  P[VT_k_uw].readFromFile = false;
  P[VT_cdw].readFromFile = false;
  P[VT_cds].readFromFile = false;
  P[VT_k_wu].readFromFile = false;
  P[VT_k_su].readFromFile = false;
  P[VT_A].readFromFile = false;
  P[VT_nperm].readFromFile = false;
  P[VT_ca50].readFromFile = false;
  P[VT_Tref].readFromFile = false;
  P[VT_nu].readFromFile = false;
  P[VT_mu].readFromFile = false;


  ParameterLoader FPL(initFile, FMT_Land17);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = FPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);
  Calculate();

#if KADEBUG
  cerr << "#Init() done ... \n";
#endif  // if KADEBUG
}  // Land17Parameters::Init

void Land17Parameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "#Land17Parameters - Calculate ..." << endl;
#endif  // if KADEBUG
  if (P[VT_skinned].value == 1) {
    P[VT_nperm].value = P[VT_nperm_skinned].value;
    P[VT_ca50].value = P[VT_ca50_skinned].value;
    P[VT_Tref].value = P[VT_Tref_skinned].value;
    P[VT_nu].value = P[VT_nu_skinned].value;
    P[VT_mu].value = P[VT_mu_skinned].value;
  } else {
    P[VT_nperm].value = P[VT_nperm_intact].value;
    P[VT_ca50].value = P[VT_ca50_intact].value;
    P[VT_Tref].value = P[VT_Tref_intact].value;
    P[VT_nu].value = P[VT_nu_intact].value;
    P[VT_mu].value = P[VT_mu_intact].value;
  }

  P[VT_k_ws].value = 0.004 * P[VT_mu].value;
  P[VT_k_uw].value = 0.026 * P[VT_nu].value;


  P[VT_cdw].value =
      P[VT_phi].value * P[VT_k_uw].value * (1 - P[VT_dr].value) * (1 - P[VT_wfrac].value) /
      ((1 - P[VT_dr].value) * P[VT_wfrac].value);
  P[VT_cds].value = P[VT_phi].value * P[VT_k_ws].value * (1 - P[VT_dr].value) * P[VT_wfrac].value /
                    P[VT_dr].value;


  P[VT_k_wu].value = P[VT_k_uw].value * (1 / P[VT_wfrac].value - 1) - P[VT_k_ws].value;
  P[VT_k_su].value = P[VT_k_ws].value * (1 / P[VT_dr].value - 1) * P[VT_wfrac].value;
  P[VT_A].value =
      (0.25 * P[VT_TOT_A].value) / ((1 - P[VT_dr].value) * P[VT_wfrac].value + P[VT_dr].value) *
      (P[VT_dr].value / 0.25);
}  // Land17Parameters::Calculate
