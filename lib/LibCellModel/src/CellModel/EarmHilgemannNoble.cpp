/*      File: EarmHilgemannNoble.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#include <EarmHilgemannNoble.h>

EarmHilgemannNoble::EarmHilgemannNoble(EarmHilgemannNobleParameters *pp) {
  pEHNP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pEHNP, NS_EarmHilgemannNobleParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

EarmHilgemannNoble::~EarmHilgemannNoble() {}

#ifdef HETERO

inline bool EarmHilgemannNoble::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool EarmHilgemannNoble::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int EarmHilgemannNoble::GetSize(void) {
  return sizeof(EarmHilgemannNoble) - sizeof(vbElphyModel<ML_CalcType>) -
         sizeof(EarmHilgemannNobleParameters *)
         #ifdef HETERO
         - sizeof(ParameterSwitch *)
#endif // ifdef HETERO
      ;
}

void EarmHilgemannNoble::Init() {
#if KADEBUG
  cerr << "initializing Class: EarmHilgemannNoble ... " << endl;
#endif // if KADEBUG
  r = CELLMODEL_PARAMVALUE(VT_r);
  f_activator = CELLMODEL_PARAMVALUE(VT_f_activator);
  f_product = CELLMODEL_PARAMVALUE(VT_f_product);
  Ca_i = CELLMODEL_PARAMVALUE(VT_Ca_i);
  Ca_calmod = CELLMODEL_PARAMVALUE(VT_Ca_calmod);
  Ca_troponin = CELLMODEL_PARAMVALUE(VT_Ca_troponin);
  Ca_up = CELLMODEL_PARAMVALUE(VT_Ca_up);
  Ca_rel = CELLMODEL_PARAMVALUE(VT_Ca_rel);
  m = CELLMODEL_PARAMVALUE(VT_m);
  h = CELLMODEL_PARAMVALUE(VT_h);
  d = CELLMODEL_PARAMVALUE(VT_d);
  f = CELLMODEL_PARAMVALUE(VT_f);
  q = CELLMODEL_PARAMVALUE(VT_q);
  Na_i = CELLMODEL_PARAMVALUE(VT_Na_i);
  K_i = CELLMODEL_PARAMVALUE(VT_K_i);
  Ca_o = CELLMODEL_PARAMVALUE(VT_Ca_o);
}

inline unsigned char EarmHilgemannNoble::getSpeed(ML_CalcType adVm) {
  return (unsigned char) (adVm < .5e-6 ? 4 : (adVm < 1e-6 ? 3 : (adVm < 2e-6 ? 2 : 1)));
}

ML_CalcType EarmHilgemannNoble::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                                     ML_CalcType stretch = 1., int euler = 1) {
  const ML_CalcType V_int = V * 1000.0;
  const double VmE_K = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_K_o)) / K_i));
  const double VmE_Namh = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_NapK)) / (Na_i + 0.12 * K_i)));
  const double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_i));
  const double VmE_Ca = V_int - (CELLMODEL_PARAMVALUE(VT_RTd2F)) * log(Ca_o / (double) Ca_i);
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double dk_cachoffpCa_i = 1.0 / ((CELLMODEL_PARAMVALUE(VT_k_cachoff)) + Ca_i);
  const double r_inf = pEHNP->r_inf[Vi];

  r = r_inf + (r - r_inf) * pEHNP->exptau_r[Vi];
  double CaCa500 = Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mCa)));
  CaCa500 *= 500.0 * CaCa500;
  f_activator += tinc * ((1.0 - f_activator - f_product) * (CaCa500 + pEHNP->sechs100exp[Vi]) -
                         f_activator * (CaCa500 + 60.0));
  f_product += tinc * (f_activator * (CaCa500 + 60.0) - f_product);
  const double m_inf = pEHNP->m_inf[Vi];
  m = m_inf + (m - m_inf) * pEHNP->exptau_m[Vi];
  const double h_inf = pEHNP->h_inf[Vi];
  h = h_inf + (h - h_inf) * pEHNP->exptau_h[Vi];
  const double d_inf = pEHNP->d_inf[Vi];
  d = d_inf + (d - d_inf) * pEHNP->exptau_d[Vi];

  f += tinc *
       (pEHNP->a_f[Vi] * (119.0 * Ca_i * dk_cachoffpCa_i + 1.0) * (1.0 - f) - pEHNP->b_f[Vi] * f);
  q += tinc * 333.0 * (pEHNP->q_[Vi] - q);
  if (q > 1.0)
    q = 1.0;
  const double I_K =
      ((CELLMODEL_PARAMVALUE(VT_KI_K1)) / (1.0 + exp((VmE_K - 10.0) * (CELLMODEL_PARAMVALUE(VT_F2dRT)))) + (CELLMODEL_PARAMVALUE(VT_G_to)) * r * q +
       (CELLMODEL_PARAMVALUE(VT_G_bK))) * VmE_K; // I_K1+I_to+I_bK
  const double I_Na = (CELLMODEL_PARAMVALUE(VT_G_Na)) * m * m * m * h * VmE_Namh;
  const double I_bNa = (CELLMODEL_PARAMVALUE(VT_G_bNa)) * VmE_Na;
  const double I_si = d * (1.0 - f) * dk_cachoffpCa_i * pEHNP->VdV[Vi] *
                      (pEHNP->VdV2[Vi] * (Ca_i - Ca_o * pEHNP->dexpV50_2[Vi]) +
                       ((CELLMODEL_PARAMVALUE(VT_P_CaK)) * (K_i - pEHNP->KexpV[Vi]) +
                        (CELLMODEL_PARAMVALUE(VT_P_CaNa)) * (Na_i - pEHNP->NaexpV[Vi])));
  const double I_bCa = (CELLMODEL_PARAMVALUE(VT_G_bCa)) * VmE_Ca;
  const double I_NaK = ((CELLMODEL_PARAMVALUE(VT_KI_NaK)) * Na_i / (Na_i + (CELLMODEL_PARAMVALUE(VT_k_mNa))));
  const double NNNICO = (Na_i * Na_i * Na_i * Ca_o);
  const double NNNOCI = (CELLMODEL_PARAMVALUE(VT_NNNO)) * Ca_i;
  const double I_NaCa = pEHNP->KINaCa[Vi] * (NNNICO - pEHNP->dexpV[Vi] * NNNOCI) /
                        (1.0 + (CELLMODEL_PARAMVALUE(VT_d_NaCa)) * (NNNICO + NNNOCI));
  const double I_up = (3.0 * Ca_i - .23 * Ca_up * (CELLMODEL_PARAMVALUE(VT_k_kckxdk_s))) /
                      (Ca_i + Ca_up * (CELLMODEL_PARAMVALUE(VT_k_kckxdk_s)) + (CELLMODEL_PARAMVALUE(VT_k_kckxp)));
  const double I_tr = 50.0 * (Ca_up - Ca_rel);
  double I_rel = f_activator / (f_activator + .25);
  I_rel *= I_rel * (CELLMODEL_PARAMVALUE(VT_k_mCa2)) * Ca_rel;
  const double Ca_calmod_ = 1e5 * ((CELLMODEL_PARAMVALUE(VT_M_trop)) - Ca_calmod) * Ca_i - 50.0 * Ca_calmod;
  const double Ca_troponin_ = 1e5 * ((CELLMODEL_PARAMVALUE(VT_C_trop)) - Ca_troponin) * Ca_i - 200.0 * Ca_troponin;
  Ca_calmod += tinc * Ca_calmod_;
  Ca_troponin += tinc * Ca_troponin_;
  const double ICa = I_si + I_bCa - 2.0 * I_NaCa;
  Na_i -= tinc * (CELLMODEL_PARAMVALUE(VT_k_1dV_iF)) * (I_Na + I_bNa * (CELLMODEL_PARAMVALUE(VT_Na_od140)) + 3.0 * (I_NaK + I_NaCa));
  K_i -= tinc * (CELLMODEL_PARAMVALUE(VT_k_1dV_iF)) * (I_K - 2.0 * I_NaK);
  Ca_o += tinc * ((CELLMODEL_PARAMVALUE(VT_k_Vspez)) * ICa - (CELLMODEL_PARAMVALUE(VT_diffCa)) * (Ca_o - (CELLMODEL_PARAMVALUE(VT_Ca_b))));
  Ca_i += tinc *
          (-(CELLMODEL_PARAMVALUE(VT_k_1dV_i2F)) * ICa - I_up + I_rel * (CELLMODEL_PARAMVALUE(VT_k_Vspez2)) - Ca_calmod_ - Ca_troponin_);
  Ca_up += tinc * ((CELLMODEL_PARAMVALUE(VT_k_V_idV_SRup)) * I_up - I_tr);
  Ca_rel += tinc * ((CELLMODEL_PARAMVALUE(VT_k_VupdVrel)) * I_tr - I_rel);
  return -tinc * (I_K + I_si + I_NaK + I_Na + I_bNa + I_NaCa + I_bCa - i_external) * (CELLMODEL_PARAMVALUE(VT_dC_m));
} // EarmHilgemannNoble::Calc

void EarmHilgemannNoble::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << m << ' ' << h << ' '
          << d << ' ' << f << ' '
          << q << ' ' << r << ' '
          << f_activator << ' ' << f_product << ' '
          << Na_i << ' ' << K_i << ' '
          << Ca_o << ' ' << Ca_i << ' '
          << Ca_up << ' ' << Ca_rel << ' '
          << Ca_calmod << ' ' << Ca_troponin << ' ';
}

void EarmHilgemannNoble::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  const ML_CalcType V_int = V * 1000.0;
  const double VmE_K = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_K_o)) / K_i));
  const double VmE_Namh = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_NapK)) / (Na_i + 0.12 * K_i)));
  const double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_i));

  // const double VmE_Ca=V_int-(CELLMODEL_PARAMVALUE(VT_RTd2F))*log(Ca_o/(double)Ca_i);
  const double NNNICO = (Na_i * Na_i * Na_i * Ca_o);
  const double NNNOCI = (CELLMODEL_PARAMVALUE(VT_NNNO)) * Ca_i;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double dk_cachoffpCa_i = 1.0 / ((CELLMODEL_PARAMVALUE(VT_k_cachoff)) + Ca_i);
  double C_rel = f_activator / (f_activator + .25);
  tempstr << (CELLMODEL_PARAMVALUE(VT_KI_K1)) / (1.0 + exp((VmE_K - 10.0) * (CELLMODEL_PARAMVALUE(VT_F2dRT)))) * VmE_K << ' '  // I_K
          << (CELLMODEL_PARAMVALUE(VT_G_to)) * r * q * VmE_K << ' ' // I_to
          << (CELLMODEL_PARAMVALUE(VT_G_bK)) * VmE_K << ' ' // I_bK
          << (CELLMODEL_PARAMVALUE(VT_G_Na)) * m * m * m * h * VmE_Namh << ' ' // I_Na
          << (CELLMODEL_PARAMVALUE(VT_G_bNa)) * VmE_Na << ' ' // I_bNa
          << d * (1.0 - f) * dk_cachoffpCa_i * pEHNP->VdV[Vi] *
             (pEHNP->VdV2[Vi] * (Ca_i - Ca_o * pEHNP->dexpV50_2[Vi]) +
              ((CELLMODEL_PARAMVALUE(VT_P_CaK)) * (K_i - pEHNP->KexpV[Vi]) +
               (CELLMODEL_PARAMVALUE(VT_P_CaNa)) * (Na_i - pEHNP->NaexpV[Vi]))) << ' ' // I_si
          << ((CELLMODEL_PARAMVALUE(VT_KI_NaK)) * Na_i / (Na_i + (CELLMODEL_PARAMVALUE(VT_k_mNa)))) << ' ' // I_NaK
          << pEHNP->KINaCa[Vi] * (NNNICO - pEHNP->dexpV[Vi] * NNNOCI) /
             (1.0 + (CELLMODEL_PARAMVALUE(VT_d_NaCa)) * (NNNICO + NNNOCI)) << ' ' // I_
          << (3.0 * Ca_i - .23 * Ca_up * (CELLMODEL_PARAMVALUE(VT_k_kckxdk_s))) /
             (Ca_i + Ca_up * (CELLMODEL_PARAMVALUE(VT_k_kckxdk_s)) + (CELLMODEL_PARAMVALUE(VT_k_kckxp))) << ' ' // I_up
          << 50.0 * (Ca_up - Ca_rel) << ' ' // I_tr
          << C_rel * C_rel * (CELLMODEL_PARAMVALUE(VT_k_mCa2)) * Ca_rel << ' '; // I_rel
}

void EarmHilgemannNoble::GetParameterNames(vector<string> &getpara) {
  const int numpara = 16;
  const string ParaNames[numpara] =
      {"m", "h", "d", "f", "q", "r", "f_activator", "f_product",
       "Na_i",
       "K_i",
       "Ca_o",
       "Ca_i",
       "Ca_up",
       "Ca_rel",
       "Ca_calmod", "Ca_trop"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void EarmHilgemannNoble::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 11;
  const string ParaNames[numpara] =
      {"I_K", "I_to", "I_bK", "I_Na", "I_bNa", "I_si", "I_NaK", "I_NaCa", "I_up", "I_tr", "I_rel"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
