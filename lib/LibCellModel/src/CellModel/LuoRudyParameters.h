/*      File: LuoRudyParameters.h
    automatically created by ExtractParameterClass.pl - done by dw (19.09.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#ifndef LUORUDYPARAMETERS_H
#define LUORUDYPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_LuoRudyParameters {
  enum varType {
    VT_R = vtFirst,
    VT_Tx,
    VT_F,
    VT_RTdF,
    VT_FdRT,
    VT_d2F,
    VT_RTd2F,
    VT_C_m,
    VT_dC_m,
    VT_TRPN_max,
    VT_CMDN_max,
    VT_taudiff,
    VT_dtaudiff,
    VT_k_mTRPN,
    VT_k_mCMDN,
    VT_k_mCSQN,
    VT_CSQN_th,
    VT_k_mCa,
    VT_eta,
    VT_k_mNai,
    VT_kk_mNai,
    VT_k_mKo,
    VT_k_mnsCa,
    VT_kkk_mnsCa,
    VT_k_mpCa,
    VT_k_mup,
    VT_I_NaKmax,
    VT_I_pCamax,
    VT_I_upmax,
    VT_g_Na,
    VT_g_Kp,
    VT_g_Cab,
    VT_g_CaT,
    VT_g_Nab,
    VT_g_to,
    VT_g_K1,
    VT_g_maxrel,
    VT_g_KNa,
    VT_n_KNa,
    VT_kdk_Na,
    VT_t_on,
    VT_dt_on,
    VT_t_off,
    VT_dt_off,
    VT_t_tr,
    VT_dt_tr,
    VT_P_Ca,
    VT_P_Na,
    VT_P_K,
    VT_P_NaK,
    VT_P_nsCa,
    VT_gamm_Cai,
    VT_gamm_Cao,
    VT_gamm_Nai,
    VT_gamm_Nao,
    VT_gamm_Ki,
    VT_gamm_Ko,
    VT_KgC,
    VT_K_b,
    VT_Ca_b,
    VT_Ca_NSRmax,
    VT_IupdCa,
    VT_Na_b,
    VT_natp,
    VT_nicholsarea,
    VT_atpi,
    VT_hatp,
    VT_katp,
    VT_I_Katpmax,
    VT_radius,
    VT_lenght,
    VT_Vol,
    VT_V_cell,
    VT_A_Geo,
    VT_R_CG,
    VT_A_Cap,
    VT_V_myo,
    VT_dV_myo,
    VT_AdVmF,
    VT_V_mito,
    VT_V_SR,
    VT_V_NSR,
    VT_V_JSR,
    VT_VJdVN,
    VT_V_cleft,
    VT_AdVcF,
    VT_AdVcFd2,
    VT_Ca_i,
    VT_Ca_o,
    VT_Na_i,
    VT_Na_o,
    VT_K_i,
    VT_K_o,
    VT_Ca_JSR,
    VT_Ca_NSR,
    VT_dCa_iont,
    VT_t_CICR,
    VT_t_jsrol,
    VT_m,
    VT_h,
    VT_j,
    VT_d,
    VT_f,
    VT_b,
    VT_g,
    VT_Xr,
    VT_Xs1,
    VT_Xs2,
    VT_ato,
    VT_ito,
    VT_grelbarjsrol,
    VT_flip,
    VT_init_V_m,
    VT_KgN,
    VT_CSQN_max,
    vtLast
  };
} // namespace NS_LuoRudyParameters

using namespace NS_LuoRudyParameters;

class LuoRudyParameters : public vbNewElphyParameters {
public:
  LuoRudyParameters(const char *, ElphyModelType, ML_CalcType);

  ~LuoRudyParameters();

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType);

  void Init(const char *, ElphyModelType, ML_CalcType);

  ML_CalcType m_inf[RTDT];
  ML_CalcType et_m[RTDT];

  // ML_CalcType a_h[RTDT];
  // ML_CalcType b_h[RTDT];
  ML_CalcType h_inf[RTDT];
  ML_CalcType et_h[RTDT];

  // ML_CalcType a_j[RTDT];
  // ML_CalcType b_j[RTDT];
  ML_CalcType j_inf[RTDT];
  ML_CalcType et_j[RTDT];

  // ML_CalcType a_d[RTDT];
  // ML_CalcType b_d[RTDT];
  ML_CalcType d_inf[RTDT];
  ML_CalcType et_d[RTDT];

  // ML_CalcType a_f[RTDT];
  // ML_CalcType b_f[RTDT];
  ML_CalcType f_inf[RTDT];
  ML_CalcType et_f[RTDT];

  // ML_CalcType a_b[RTDT];
  // ML_CalcType b_b[RTDT];
  ML_CalcType b_inf[RTDT];
  ML_CalcType et_b[RTDT];

  // ML_CalcType a_g[RTDT];
  // ML_CalcType b_g[RTDT];
  ML_CalcType g_inf[RTDT];
  ML_CalcType et_g[RTDT];

  // ML_CalcType a_Xr[RTDT];
  // ML_CalcType b_Xr[RTDT];
  ML_CalcType Xr_inf[RTDT];
  ML_CalcType et_Xr[RTDT];

  // ML_CalcType a_Xs1[RTDT];
  // ML_CalcType b_Xs1[RTDT];
  ML_CalcType Xs1_inf[RTDT];
  ML_CalcType et_Xs1[RTDT];

  // ML_CalcType a_Xs2[RTDT];
  // ML_CalcType b_Xs2[RTDT];
  ML_CalcType Xs2_inf[RTDT];
  ML_CalcType et_Xs2[RTDT];

  // ML_CalcType a_ato[RTDT];
  // ML_CalcType b_ato[RTDT];
  ML_CalcType ato_inf[RTDT];
  ML_CalcType et_ato[RTDT];

  // ML_CalcType a_ito[RTDT];
  // ML_CalcType b_ito[RTDT];
  ML_CalcType ito_inf[RTDT];
  ML_CalcType et_ito[RTDT];
  ML_CalcType Kp[RTDT];
  ML_CalcType Rinf[RTDT];
  ML_CalcType expV[RTDT];
  ML_CalcType expV_2[RTDT];
  ML_CalcType expVm[RTDT];
  ML_CalcType expV_mk1[RTDT];
  ML_CalcType etaV[RTDT];
  ML_CalcType C_Ca[RTDT];
  ML_CalcType C_NaK[RTDT];
}; // class LuoRudyParameters
#endif // ifndef LUORUDYPARAMETERS_H
