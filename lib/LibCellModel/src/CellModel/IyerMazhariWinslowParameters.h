/**@file IyerMazhariWinslowParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef IYER_MAZHARI_WINSLOW_PARAMETERS
#define IYER_MAZHARI_WINSLOW_PARAMETERS

#include <ParameterLoader.h>

// TIMOTHY defines ratio of mutated to WT channels
// for WT (original): #undef TIMOTHY
// for WT (Splawski): #define TIMOTHY 0
// implemented for original version of CaL!

#undef TIMOTHY

// #define TIMOTHY 0.385

#undef TIMOTHY2
#define TIMOTHY2 0.5

// SPONTANEOUSRELEASE allows modeling of spontaneous Ca release from JSR
// disable by #undef SPONTANEOUSRELEASE
// for paramterization similar to LuoRudy 94: set SPONTANEOUSRELEASE to 0.7
// 100 % ~ [Ca]CSQN = 10 mM ~ [Ca]JSR=1.6 mM
//  70 % ~ [Ca]CSQN =  7 mM ~ [Ca]JSR=0.7 mM

// #define SPONTANEOUSRELEASE 0.7

// IKSOPEN defines ratio of mutated open to WT channels
// for WT: #undef IKSOPEN or #define IKSOPEN 0

#undef IKSOPEN

// #define IKSOPEN 0.5

namespace NS_IyerMazhariWinslowParameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_Acap,
    VT_Vmyo,
    VT_VJSR,
    VT_VNSR,
    VT_VSS,
    VT_CSC,

    VT_Nao,
    VT_Ko,
    VT_Cao,
    VT_Nai,
    VT_Ki,
    VT_Cai,
    VT_CaNSR,
    VT_CaSS,
    VT_CaJSR,
    VT_PC1,
    VT_PO1,
    VT_PC2,
    VT_PO2,
    VT_C0L,
    VT_C1L,
    VT_C2L,
    VT_C3L,
    VT_C4L,
    VT_OL,
    VT_CCa0L,
    VT_CCa1L,
    VT_CCa2L,
    VT_CCa3L,
    VT_CCa4L,
    VT_y,
#ifdef TIMOTHY
    VT_ytimothy,
#endif // ifdef TIMOTHY
#ifdef TIMOTHY2
    VT_CaLShift,
    VT_C0Ltimothy,
    VT_C1Ltimothy,
    VT_C2Ltimothy,
    VT_C3Ltimothy,
    VT_C4Ltimothy,
    VT_OLtimothy,
    VT_CCa0Ltimothy,
    VT_CCa1Ltimothy,
    VT_CCa2Ltimothy,
    VT_CCa3Ltimothy,
    VT_CCa4Ltimothy,
    VT_ytimothy,
    VT_y1,
    VT_y2,
    VT_y3,
    VT_tytimothyscale,
    VT_PCatimothy,
#endif // ifdef TIMOTHY2
    VT_HTRPNCa,
    VT_LTRPNCa,

    VT_C0Kvf,
    VT_C1Kvf,
    VT_C2Kvf,
    VT_C3Kvf,
    VT_OKvf,
    VT_CI0Kvf,
    VT_CI1Kvf,
    VT_CI2Kvf,
    VT_CI3Kvf,
    VT_OIKvf,

    VT_C0Kvs,
    VT_C1Kvs,
    VT_C2Kvs,
    VT_C3Kvs,
    VT_OKvs,
    VT_CI0Kvs,
    VT_CI1Kvs,
    VT_CI2Kvs,
    VT_CI3Kvs,
    VT_OIKvs,

    VT_C1Kr,
    VT_C2Kr,
    VT_C3Kr,
    VT_OKr,
    VT_IKr,

    VT_C0Ks,
    VT_C1Ks,
    VT_O1Ks,
    VT_O2Ks,

    VT_C0Na,
    VT_C1Na,
    VT_C2Na,
    VT_C3Na,
    VT_C4Na,
    VT_O1Na,
    VT_O2Na,
    VT_CI0Na,
    VT_CI1Na,
    VT_CI2Na,
    VT_CI3Na,
    VT_CI4Na,
    VT_INa,

    VT_GNa,
    VT_GNab,
    VT_GKr,
    VT_GKs,
    VT_GKv43,
    VT_PKv14,
    VT_GK1,

    VT_Naa1,
    VT_Naa2,
    VT_Naa3,
    VT_Nab1,
    VT_Nab2,
    VT_Nab3,
    VT_Nag1,
    VT_Nag2,
    VT_Nag3,
    VT_Nad1,
    VT_Nad2,
    VT_Nad3,
    VT_NaOn1,
    VT_NaOn2,
    VT_NaOn3,
    VT_NaOf1,
    VT_NaOf2,
    VT_NaOf3,
    VT_Nagg1,
    VT_Nagg2,
    VT_Nagg3,
    VT_Nadd1,
    VT_Nadd2,
    VT_Nadd3,
    VT_Nae1,
    VT_Nae2,
    VT_Nae3,
    VT_NaO1,
    VT_NaO2,
    VT_NaO3,
    VT_Naeta1,
    VT_Naeta2,
    VT_Naeta3,
    VT_Nanu1,
    VT_Nanu2,
    VT_Nanu3,
    VT_NaCn1,
    VT_NaCn2,
    VT_NaCn3,
    VT_NaCf1,
    VT_NaCf2,
    VT_NaCf3,
    VT_NaScalinga,
    VT_NaQ,

    VT_Kra0,
    VT_Krb0,
    VT_Kra1,
    VT_Krb1,
    VT_Krai,
    VT_Krbi,
    VT_Krai3,
    VT_Krkf,
    VT_Krkb,

    VT_Ksa,
    VT_Ksb,
    VT_Ksg,
    VT_Ksd,
    VT_Kse,
    VT_Kso,

    VT_Kv43aa,
    VT_Kv43ba,
    VT_Kv43ai,
    VT_Kv43bi,
    VT_Kv43f1,
    VT_Kv43f2,
    VT_Kv43f3,
    VT_Kv43f4,
    VT_Kv43b1,
    VT_Kv43b2,
    VT_Kv43b3,
    VT_Kv43b4,

    VT_Kv14aa,
    VT_Kv14ba,
    VT_Kv14ai,
    VT_Kv14bi,
    VT_Kv14f1,
    VT_Kv14f2,
    VT_Kv14f3,
    VT_Kv14f4,
    VT_Kv14b1,
    VT_Kv14b2,
    VT_Kv14b3,
    VT_Kv14b4,

    VT_P1K1,
    VT_P2K1,

    VT_KNaCa,
    VT_KmNa,
    VT_KmCa,
    VT_ksat,
    VT_NaCaeta,
    VT_kNaK,
    VT_KmNai,
    VT_KmKo,

    VT_IpCa,
    VT_KmpCa,
    VT_GCab,

    VT_CaLf,
    VT_CaLg,
    VT_CaLa,
    VT_CaLb,
    VT_CaLo,
    VT_PScale,
    VT_PCa,
    VT_PK,
    VT_ICahalf,

    VT_SRKap,
    VT_SRKam,
    VT_SRKbp,
    VT_SRKbm,
    VT_SRKcp,
    VT_SRKcm,
    VT_SRn,
    VT_SRm,
    VT_SRv1,
    VT_SRKfb,
    VT_SRNfb,
    VT_SRKrb,
    VT_SRNrb,
    VT_SRvmaxf,
    VT_SRvmaxr,
    VT_SRKSR,

    VT_ttr,
    VT_txfer,
    VT_HTRPNtot,
    VT_LTRPNtot,
    VT_KHTRPNp,
    VT_KHTRPNm,
    VT_KLTRPNp,
    VT_KLTRPNm,
    VT_KmCMDN,
    VT_KmCSQN,
    VT_KmEGTA,
    VT_EGTAtot,
    VT_CMDNtot,
    VT_CSQNtot,
    VT_Amp,
    vtLast
  };
} // namespace NS_IyerMazhariWinslowParameters

using namespace NS_IyerMazhariWinslowParameters;

class IyerMazhariWinslowParameters : public vbNewElphyParameters {
public:
  IyerMazhariWinslowParameters(const char *);

  ~IyerMazhariWinslowParameters() {}

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&CSQNtot-&Tx+1)*sizeof(T);};
  // virtual inline T* GetBase(void){return Tx;};
  /*virtual int GetNumParameters() { return 219
   #ifdef TIMOTHY
   +1
   #endif
   #ifdef TIMOTHY2
   +17
   #endif
      ; };*/
  void Init(const char *);

  void InitTable() {}

  void Calculate();
};

#endif // ifndef IYER_MAZHARI_WINSLOW_PARAMETERS
