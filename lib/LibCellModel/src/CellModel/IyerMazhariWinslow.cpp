/**@file IyerMazhariWinslow.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslow.h>

IyerMazhariWinslow::IyerMazhariWinslow(IyerMazhariWinslowParameters *pIMWArg) {
  pIMW = pIMWArg;
#ifdef HETERO
  PS = new ParameterSwitch(pIMW, NS_IyerMazhariWinslowParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

void IyerMazhariWinslow::Init() {
  Nai = CELLMODEL_PARAMVALUE(VT_Nai);
  Ki = CELLMODEL_PARAMVALUE(VT_Ki);
  Cai = CELLMODEL_PARAMVALUE(VT_Cai);
  CaNSR = CELLMODEL_PARAMVALUE(VT_CaNSR);
  CaSS = CELLMODEL_PARAMVALUE(VT_CaSS);
  CaJSR = CELLMODEL_PARAMVALUE(VT_CaJSR);
  PC1 = CELLMODEL_PARAMVALUE(VT_PC1);
  PO1 = CELLMODEL_PARAMVALUE(VT_PO1);
  PC2 = CELLMODEL_PARAMVALUE(VT_PC2);
  PO2 = CELLMODEL_PARAMVALUE(VT_PO2);
  C0L = CELLMODEL_PARAMVALUE(VT_C0L);
  C1L = CELLMODEL_PARAMVALUE(VT_C1L);
  C2L = CELLMODEL_PARAMVALUE(VT_C2L);
  C3L = CELLMODEL_PARAMVALUE(VT_C3L);
  C4L = CELLMODEL_PARAMVALUE(VT_C4L);
  OL = CELLMODEL_PARAMVALUE(VT_OL);
  CCa0L = CELLMODEL_PARAMVALUE(VT_CCa0L);
  CCa1L = CELLMODEL_PARAMVALUE(VT_CCa1L);
  CCa2L = CELLMODEL_PARAMVALUE(VT_CCa2L);
  CCa3L = CELLMODEL_PARAMVALUE(VT_CCa3L);
  CCa4L = CELLMODEL_PARAMVALUE(VT_CCa4L);
  y = CELLMODEL_PARAMVALUE(VT_y);
#ifdef TIMOTHY
  ytimothy = CELLMODEL_PARAMVALUE(VT_ytimothy);
#endif // ifdef TIMOTHY
#ifdef TIMOTHY2
  C0Ltimothy = CELLMODEL_PARAMVALUE(VT_C0Ltimothy);
  C1Ltimothy = CELLMODEL_PARAMVALUE(VT_C1Ltimothy);
  C2Ltimothy = CELLMODEL_PARAMVALUE(VT_C2Ltimothy);
  C3Ltimothy = CELLMODEL_PARAMVALUE(VT_C3Ltimothy);
  C4Ltimothy = CELLMODEL_PARAMVALUE(VT_C4Ltimothy);
  OLtimothy = CELLMODEL_PARAMVALUE(VT_OLtimothy);
  CCa0Ltimothy = CELLMODEL_PARAMVALUE(VT_CCa0Ltimothy);
  CCa1Ltimothy = CELLMODEL_PARAMVALUE(VT_CCa1Ltimothy);
  CCa2Ltimothy = CELLMODEL_PARAMVALUE(VT_CCa2Ltimothy);
  CCa3Ltimothy = CELLMODEL_PARAMVALUE(VT_CCa3Ltimothy);
  CCa4Ltimothy = CELLMODEL_PARAMVALUE(VT_CCa4Ltimothy);
  ytimothy = CELLMODEL_PARAMVALUE(VT_ytimothy);
#endif // ifdef TIMOTHY2
  HTRPNCa = CELLMODEL_PARAMVALUE(VT_HTRPNCa);
  LTRPNCa = CELLMODEL_PARAMVALUE(VT_LTRPNCa);

  C0Kvf = CELLMODEL_PARAMVALUE(VT_C0Kvf);
  C1Kvf = CELLMODEL_PARAMVALUE(VT_C1Kvf);
  C2Kvf = CELLMODEL_PARAMVALUE(VT_C2Kvf);
  C3Kvf = CELLMODEL_PARAMVALUE(VT_C3Kvf);
  OKvf = CELLMODEL_PARAMVALUE(VT_OKvf);
  CI0Kvf = CELLMODEL_PARAMVALUE(VT_CI0Kvf);
  CI1Kvf = CELLMODEL_PARAMVALUE(VT_CI1Kvf);
  CI2Kvf = CELLMODEL_PARAMVALUE(VT_CI2Kvf);
  CI3Kvf = CELLMODEL_PARAMVALUE(VT_CI3Kvf);
  OIKvf = CELLMODEL_PARAMVALUE(VT_OIKvf);

  C0Kvs = CELLMODEL_PARAMVALUE(VT_C0Kvs);
  C1Kvs = CELLMODEL_PARAMVALUE(VT_C1Kvs);
  C2Kvs = CELLMODEL_PARAMVALUE(VT_C2Kvs);
  C3Kvs = CELLMODEL_PARAMVALUE(VT_C3Kvs);
  OKvs = CELLMODEL_PARAMVALUE(VT_OKvs);
  CI0Kvs = CELLMODEL_PARAMVALUE(VT_CI0Kvs);
  CI1Kvs = CELLMODEL_PARAMVALUE(VT_CI1Kvs);
  CI2Kvs = CELLMODEL_PARAMVALUE(VT_CI2Kvs);
  CI3Kvs = CELLMODEL_PARAMVALUE(VT_CI3Kvs);
  OIKvs = CELLMODEL_PARAMVALUE(VT_OIKvs);

  C1Kr = CELLMODEL_PARAMVALUE(VT_C1Kr);
  C2Kr = CELLMODEL_PARAMVALUE(VT_C2Kr);
  C3Kr = CELLMODEL_PARAMVALUE(VT_C3Kr);
  OKr = CELLMODEL_PARAMVALUE(VT_OKr);
  IKr = CELLMODEL_PARAMVALUE(VT_IKr);

  C0Ks = CELLMODEL_PARAMVALUE(VT_C0Ks);
  C1Ks = CELLMODEL_PARAMVALUE(VT_C1Ks);
  O1Ks = CELLMODEL_PARAMVALUE(VT_O1Ks);
  O2Ks = CELLMODEL_PARAMVALUE(VT_O2Ks);

  C0Na = CELLMODEL_PARAMVALUE(VT_C0Na);
  C1Na = CELLMODEL_PARAMVALUE(VT_C1Na);
  C2Na = CELLMODEL_PARAMVALUE(VT_C2Na);
  C3Na = CELLMODEL_PARAMVALUE(VT_C3Na);
  C4Na = CELLMODEL_PARAMVALUE(VT_C4Na);
  O1Na = CELLMODEL_PARAMVALUE(VT_O1Na);
  O2Na = CELLMODEL_PARAMVALUE(VT_O2Na);
  CI0Na = CELLMODEL_PARAMVALUE(VT_CI0Na);
  CI1Na = CELLMODEL_PARAMVALUE(VT_CI1Na);
  CI2Na = CELLMODEL_PARAMVALUE(VT_CI2Na);
  CI3Na = CELLMODEL_PARAMVALUE(VT_CI3Na);
  CI4Na = CELLMODEL_PARAMVALUE(VT_CI4Na);
  INa = CELLMODEL_PARAMVALUE(VT_INa);
} // IyerMazhariWinslow::Init

void IyerMazhariWinslow::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' '  // 1
          << V << ' '
          << Nai << ' '
          << Ki << ' '
          << Cai << ' ' // 5
          << CaNSR << ' '
          << CaSS << ' '
          << CaJSR << ' '
          << PC1 << ' '
          << PO1 << ' ' // 10
          << PC2 << ' '
          << PO2 << ' '
          << C0L << ' '
          << C1L << ' '
          << C2L << ' ' // 15
          << C3L << ' '
          << C4L << ' '
          << OL << ' '
          << CCa0L << ' '
          << CCa1L << ' ' // 20
          << CCa2L << ' '
          << CCa3L << ' '
          << CCa4L << ' '
          << y << ' '
          #ifdef TIMOTHY
          << ytimothy << ' '
          #endif // ifdef TIMOTHY
          #ifdef TIMOTHY2
          << C0Ltimothy << ' '
          << C1Ltimothy << ' '
          << C2Ltimothy << ' '
          << C3Ltimothy << ' '
          << C4Ltimothy << ' '
          << OLtimothy << ' '
          << CCa0Ltimothy << ' '
          << CCa1Ltimothy << ' '
          << CCa2Ltimothy << ' '
          << CCa3Ltimothy << ' '
          << CCa4Ltimothy << ' '
          << ytimothy << ' '
          #endif // ifdef TIMOTHY2
          << HTRPNCa << ' '  // 25 - add 1 for TIMOTHY, add 16 for TIMOTHY2
          << LTRPNCa << ' '
          << C0Kvf << ' '
          << C1Kvf << ' '
          << C2Kvf << ' '
          << C3Kvf << ' ' // 30
          << OKvf << ' '
          << CI0Kvf << ' '
          << CI1Kvf << ' '
          << CI2Kvf << ' '
          << CI3Kvf << ' ' // 35
          << OIKvf << ' '
          << C0Kvs << ' '
          << C1Kvs << ' '
          << C2Kvs << ' '
          << C3Kvs << ' ' // 40
          << OKvs << ' '
          << CI0Kvs << ' '
          << CI1Kvs << ' '
          << CI2Kvs << ' '
          << CI3Kvs << ' ' // 45
          << OIKvs << ' '
          << C1Kr << ' '
          << C2Kr << ' '
          << C3Kr << ' '
          << OKr << ' ' // 50
          << IKr << ' '
          << C0Ks << ' '
          << C1Ks << ' '
          << O1Ks << ' '
          << O2Ks << ' ' // 55
          << C0Na << ' '
          << C1Na << ' '
          << C2Na << ' '
          << C3Na << ' '
          << C4Na << ' ' // 60
          << O1Na << ' '
          << O2Na << ' '
          << CI0Na << ' '
          << CI1Na << ' '
          << CI2Na << ' ' // 65
          << CI3Na << ' '
          << CI4Na << ' '
          << INa;
} // IyerMazhariWinslow::Print

void IyerMazhariWinslow::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);

  const double Acap = CELLMODEL_PARAMVALUE(VT_Acap);
  const double Vmyo = CELLMODEL_PARAMVALUE(VT_Vmyo);

  const double V_int = V * 1000.0;
  const double RTdF = ElphyModelConstants::R * CELLMODEL_PARAMVALUE(VT_Tx) / ElphyModelConstants::F;
  const double FdRT = 1. / RTdF;
  const double VFdRT = V_int * FdRT;
  const double F = ElphyModelConstants::F * 1000;
  const double VFFdRT = VFdRT * F;
  const double expVFdRT = exp(VFdRT);

  const double Nao = CELLMODEL_PARAMVALUE(VT_Nao);
  const double Ko = CELLMODEL_PARAMVALUE(VT_Ko);
  const double Cao = CELLMODEL_PARAMVALUE(VT_Cao);

  const double EK = RTdF * log(Ko / Ki); // -92 mV for resting 1 Hz
  const double ENa = RTdF * log(Nao / Nai);  // 70 mV for resting 1 Hz
  const double ECa = .5 * RTdF * log(Cao / Cai);  // 35 mV for resting 1 Hz

  const double I_Na = CELLMODEL_PARAMVALUE(VT_GNa) * (O1Na + O2Na) * (V_int - ENa);
  const double f = sqrt(Ko / 4.);
  const double I_Kr = CELLMODEL_PARAMVALUE(VT_GKr) * f * OKr * (V_int - EK);
#ifdef IKSOPEN
  double I_Ks = CELLMODEL_PARAMVALUE(VT_GKs)*((O1Ks+O2Ks)*(1.-IKSOPEN)+IKSOPEN)*(V_int-EK);
#else // ifdef IKSOPEN
  double I_Ks = CELLMODEL_PARAMVALUE(VT_GKs) * (O1Ks + O2Ks) * (V_int - EK);
#endif // ifdef IKSOPEN
  const double I_Kv43 = CELLMODEL_PARAMVALUE(VT_GKv43) * OKvf * (V_int - EK);
  const double I_Kv14Na =
      0.02 * CELLMODEL_PARAMVALUE(VT_PKv14) * OKvs * VFFdRT * (Nai * expVFdRT - Nao) / (expVFdRT - 1.);
  const double I_Kv14K = CELLMODEL_PARAMVALUE(VT_PKv14) * OKvs * VFFdRT * (Ki * expVFdRT - Ko) / (expVFdRT - 1.);

  //    const double I_Kv14=I_Kv14K+I_Kv14Na;
  const double K1 = 1. / (CELLMODEL_PARAMVALUE(VT_P1K1) + exp(CELLMODEL_PARAMVALUE(VT_P2K1) * FdRT * (V_int - EK)));
  const double I_K1 = CELLMODEL_PARAMVALUE(VT_GK1) * K1 * sqrt(Ko) * (V_int - EK);
  const double KmNa = CELLMODEL_PARAMVALUE(VT_KmNa);
  const double KNaCa = CELLMODEL_PARAMVALUE(VT_KNaCa);
  const double KmCa = CELLMODEL_PARAMVALUE(VT_KmCa);
  const double ksat = CELLMODEL_PARAMVALUE(VT_ksat);

  const double NaCaeta = CELLMODEL_PARAMVALUE(VT_NaCaeta);
  const double I_NaCa = KNaCa * 1. / ((KmNa * KmNa * KmNa + Nao * Nao * Nao) * (KmCa + Cao) *
                                      (1. + ksat * exp((NaCaeta - 1.) * VFdRT)))
                        * (exp(NaCaeta * VFdRT) * Nai * Nai * Nai * Cao -
                           exp((NaCaeta - 1.) * VFdRT) * Nao * Nao * Nao * Cai);
  const double I_Nab = CELLMODEL_PARAMVALUE(VT_GNab) * (V_int - ENa);

  const double sigmaNaK = 1. / 7. * (exp(Nao / 67.3) - 1.);
  const double fNaK = 1 / (1 + .1245 * exp(-.1 * VFdRT) + 0.0365 * sigmaNaK * exp(-1.33 * VFdRT));
  const double KmKo = CELLMODEL_PARAMVALUE(VT_KmKo);
  const double KmNai = CELLMODEL_PARAMVALUE(VT_KmNai);
  const double kNaK = CELLMODEL_PARAMVALUE(VT_kNaK);
  const double I_NaK =
      kNaK * fNaK * 1. / (1. + pow(KmNai / Nai, (double) 1.5)) * (Ko / (Ko + KmKo));
  const double I_pCa = CELLMODEL_PARAMVALUE(VT_IpCa) * Cai / (CELLMODEL_PARAMVALUE(VT_KmpCa) + Cai);
  const double I_Cab = CELLMODEL_PARAMVALUE(VT_GCab) * (V_int - ECa);
  const double PCa = CELLMODEL_PARAMVALUE(VT_PCa) * CELLMODEL_PARAMVALUE(VT_PScale);
  const double ICas =
      PCa * 4. * VFFdRT * (.001 * exp(2. * VFdRT) - 0.341 * Cao) / (exp(2. * VFdRT) - 1.);

#ifdef TIMOTHY
  const double I_Ca = ICas*((1.-TIMOTHY)*y+TIMOTHY*ytimothy)*OL;
#else // ifdef TIMOTHY
# ifdef TIMOTHY2
  const double PCatimothy = CELLMODEL_PARAMVALUE(VT_PCatimothy) * CELLMODEL_PARAMVALUE(VT_PScale);
  const double ICastimothy = ICas / PCa * PCatimothy;
  const double I_Ca =
      ICas * ((1. - TIMOTHY2) * y * OL) + ICastimothy * TIMOTHY2 * ytimothy * OLtimothy;
# else // ifdef TIMOTHY2
  const double I_Ca = ICas*y*OL;
# endif // ifdef TIMOTHY2
#endif // ifdef TIMOTHY

  double PK = CELLMODEL_PARAMVALUE(VT_PK) * CELLMODEL_PARAMVALUE(VT_PScale);
  PK /= (1. + min(0., ICas) / (CELLMODEL_PARAMVALUE(VT_ICahalf)));

#ifdef TIMOTHY
  const double I_CaK = PK*((1.-TIMOTHY)*y+TIMOTHY*ytimothy)*OL*VFFdRT*((Ki*expVFdRT-Ko)/(expVFdRT-.1));
#else // ifdef TIMOTHY
# ifdef TIMOTHY2
  const double PKtimothy = PK / PCa * PCatimothy;
  const double I_CaK = (PK * (1. - TIMOTHY2) * y + PKtimothy * TIMOTHY2 * ytimothy) * OL * VFFdRT *
                       ((Ki * expVFdRT - Ko) / (expVFdRT - .1));
# else // ifdef TIMOTHY2
  const double I_CaK = PK*y*OL*VFFdRT*((Ki*expVFdRT-Ko)/(expVFdRT-.1));
# endif // ifdef TIMOTHY2
#endif // ifdef TIMOTHY

  const double SRv1 = CELLMODEL_PARAMVALUE(VT_SRv1);
  const double Jrel = SRv1 * (PO1 + PO2) * (CaJSR - CaSS);

  const double SRKfb = CELLMODEL_PARAMVALUE(VT_SRKfb);
  const double SRNfb = CELLMODEL_PARAMVALUE(VT_SRNfb);
  const double SRKrb = CELLMODEL_PARAMVALUE(VT_SRKrb);
  const double SRNrb = CELLMODEL_PARAMVALUE(VT_SRNrb);
  const double SRvmaxf = CELLMODEL_PARAMVALUE(VT_SRvmaxf);
  const double SRvmaxr = CELLMODEL_PARAMVALUE(VT_SRvmaxr);
  const double SRKSR = CELLMODEL_PARAMVALUE(VT_SRKSR);
  const double fb = pow(Cai / SRKfb, SRNfb);
  const double rb = pow(CaNSR / SRKrb, SRNrb);

  const double Jup = SRKSR * (SRvmaxf * fb - SRvmaxr * rb) / (1. + fb + rb);

  const double txfer = CELLMODEL_PARAMVALUE(VT_txfer);
  const double Jxfer = (CaSS - Cai) / txfer;

  const double HTRPNtot = CELLMODEL_PARAMVALUE(VT_HTRPNtot);
  const double LTRPNtot = CELLMODEL_PARAMVALUE(VT_LTRPNtot);
  const double KHTRPNp = CELLMODEL_PARAMVALUE(VT_KHTRPNp);
  const double KHTRPNm = CELLMODEL_PARAMVALUE(VT_KHTRPNm);
  const double KLTRPNp = CELLMODEL_PARAMVALUE(VT_KLTRPNp);
  const double KLTRPNm = CELLMODEL_PARAMVALUE(VT_KLTRPNm);
  const double dHTRPN = (KHTRPNp * Cai * (1. - HTRPNCa) - KHTRPNm * HTRPNCa);
  const double dLTRPN = (KLTRPNp * Cai * (1. - LTRPNCa) - KLTRPNm * LTRPNCa);
  const double Jtrpn = (HTRPNtot * dHTRPN + LTRPNtot * dLTRPN);
  const double ACVmyoF = Acap / (Vmyo * F);

  tempstr << ' ' << I_Na
          << ' ' << I_Kr
          << ' ' << I_Ks
          << ' ' << I_Kv43
          << ' ' << I_Kv14Na
          << ' ' << I_Kv14K
          << ' ' << I_K1
          << ' ' << I_NaCa
          << ' ' << I_Nab
          << ' ' << I_NaK
          << ' ' << I_pCa
          << ' ' << I_Cab
          << ' ' << I_Ca
          << ' ' << I_CaK
          << ' ' << Jrel / ACVmyoF
          << ' ' << Jup / ACVmyoF
          << ' ' << Jtrpn / ACVmyoF
          << ' ' << Jxfer / ACVmyoF;
} // IyerMazhariWinslow::LongPrint

void IyerMazhariWinslow::GetParameterNames(vector<string> &getpara) {
  const char *ParaNames[] =
      {"Nai", "Ki", "Cai",
       "CaNSR", "CaSS", "CaJSR",
       "PC1", "PO1", "PC2", "PO2",
       "C0L", "C1L", "C2L", "C3L",
       "C4L", "OL",
       "CCa0L",
       "CCa1L", "CCa2L", "CCa3L", "CCa4L",
       "y",
#ifdef TIMOTHY
          "ytimothy",
#endif // ifdef TIMOTHY
#ifdef TIMOTHY2
       "C0Ltimothy", "C1Ltimothy", "C2Ltimothy", "C3Ltimothy",
       "C4Ltimothy",
       "OLtimothy",
       "CCa0Ltimothy", "CCa1Ltimothy", "CCa2Ltimothy", "CCa3Ltimothy",
       "CCa4Ltimothy", "ytimothy",
#endif // ifdef TIMOTHY2
       "HTRPNCa", "LTRPNCa",
       "C0Kvf", "C1Kvf", "C2Kvf", "C3Kvf",
       "OKvf", "CI0Kvf",
       "CI1Kvf", "CI2Kvf", "CI3Kvf", "OIKvf",
       "C0Kvs", "C1Kvs", "C2Kvs", "C3Kvs",
       "OKvs", "CI0Kvs",
       "CI1Kvs", "CI2Kvs", "CI3Kvs", "OIKvs",
       "C1Kr", "C2Kr", "C3Kr", "OKr",
       "IKr",
       "C0Ks", "C1Ks", "O1Ks", "O2Ks",
       "C0Na", "C1Na", "C2Na", "C3Na",
       "C4Na", "O1Na",
       "O2Na",
       "CI0Na", "CI1Na", "CI2Na", "CI3Na",
       "CI4Na", "INa"};

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void IyerMazhariWinslow::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const char *ParaNames[] = {
      "I_Na",
      "I_Kr",
      "I_Ks",
      "I_Kv43",
      "I_Kv14Na",
      "I_Kv14K",
      "I_K1",
      "I_NaCa",
      "I_Nab",
      "I_NaK",
      "I_pCa",
      "I_Cab",
      "I_Ca",
      "I_CaK",
      "Jrel",
      "Jup",
      "Jtrpn",
      "Jxfer"
  };
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType IyerMazhariWinslow::Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim = .0,
                                     ML_CalcType stretch = 1.,
                                     int euler = 1) {
  /*     cerr << "Calc " << endl; */
  /*     cerr << "V " << V << endl; */
  /*     cerr << "tinc " << tinc << endl; */
  /*     cerr << "I_Stim " << I_Stim << endl; */

  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);
  const double Acap = CELLMODEL_PARAMVALUE(VT_Acap);
  const double Vmyo = CELLMODEL_PARAMVALUE(VT_Vmyo);
  const double VJSR = CELLMODEL_PARAMVALUE(VT_VJSR);
  const double VNSR = CELLMODEL_PARAMVALUE(VT_VNSR);
  const double VSS = CELLMODEL_PARAMVALUE(VT_VSS);

  tinc *= 1000.0;
  const double V_int = V * 1000.0;
  const double RTdF = ElphyModelConstants::R * Tx / ElphyModelConstants::F;
  const double FdRT = 1. / RTdF;
  const double VFdRT = V_int * FdRT;
  const double F = ElphyModelConstants::F * 1000;
  const double VFFdRT = VFdRT * F;
  const double expVFdRT = exp(VFdRT);

  const double Nao = CELLMODEL_PARAMVALUE(VT_Nao);
  const double Ko = CELLMODEL_PARAMVALUE(VT_Ko);
  const double Cao = CELLMODEL_PARAMVALUE(VT_Cao);

  const double EK = RTdF * log(Ko / Ki);
  const double ENa = RTdF * log(Nao / Nai);
  const double ECa = .5 * RTdF * log(Cao / Cai);

  const double Naa1 = CELLMODEL_PARAMVALUE(VT_Naa1), Naa2 = CELLMODEL_PARAMVALUE(VT_Naa2), Naa3 = CELLMODEL_PARAMVALUE(VT_Naa3);
  const double Nab1 = CELLMODEL_PARAMVALUE(VT_Nab1), Nab2 = CELLMODEL_PARAMVALUE(VT_Nab2), Nab3 = CELLMODEL_PARAMVALUE(VT_Nab3);
  const double Nag1 = CELLMODEL_PARAMVALUE(VT_Nag1), Nag2 = CELLMODEL_PARAMVALUE(VT_Nag2), Nag3 = CELLMODEL_PARAMVALUE(VT_Nag3);
  const double Nad1 = CELLMODEL_PARAMVALUE(VT_Nad1), Nad2 = CELLMODEL_PARAMVALUE(VT_Nad2), Nad3 = CELLMODEL_PARAMVALUE(VT_Nad3);
  const double NaOn1 = CELLMODEL_PARAMVALUE(VT_NaOn1), NaOn2 = CELLMODEL_PARAMVALUE(VT_NaOn2), NaOn3 = CELLMODEL_PARAMVALUE(VT_NaOn3);
  const double NaOf1 = CELLMODEL_PARAMVALUE(VT_NaOf1), NaOf2 = CELLMODEL_PARAMVALUE(VT_NaOf2), NaOf3 = CELLMODEL_PARAMVALUE(VT_NaOf3);
  const double Nagg1 = CELLMODEL_PARAMVALUE(VT_Nagg1), Nagg2 = CELLMODEL_PARAMVALUE(VT_Nagg2), Nagg3 = CELLMODEL_PARAMVALUE(VT_Nagg3);
  const double Nadd1 = CELLMODEL_PARAMVALUE(VT_Nadd1), Nadd2 = CELLMODEL_PARAMVALUE(VT_Nadd2), Nadd3 = CELLMODEL_PARAMVALUE(VT_Nadd3);
  const double Nae1 = CELLMODEL_PARAMVALUE(VT_Nae1), Nae2 = CELLMODEL_PARAMVALUE(VT_Nae2), Nae3 = CELLMODEL_PARAMVALUE(VT_Nae3);
  const double NaO1 = CELLMODEL_PARAMVALUE(VT_NaO1), NaO2 = CELLMODEL_PARAMVALUE(VT_NaO2), NaO3 = CELLMODEL_PARAMVALUE(VT_NaO3);
  const double Naeta1 = CELLMODEL_PARAMVALUE(VT_Naeta1), Naeta2 = CELLMODEL_PARAMVALUE(VT_Naeta2), Naeta3 = CELLMODEL_PARAMVALUE(VT_Naeta3);
  const double Nanu1 = CELLMODEL_PARAMVALUE(VT_Nanu1), Nanu2 = CELLMODEL_PARAMVALUE(VT_Nanu2), Nanu3 = CELLMODEL_PARAMVALUE(VT_Nanu3);
  const double NaCn1 = CELLMODEL_PARAMVALUE(VT_NaCn1), NaCn2 = CELLMODEL_PARAMVALUE(VT_NaCn2), NaCn3 = CELLMODEL_PARAMVALUE(VT_NaCn3);
  const double NaCf1 = CELLMODEL_PARAMVALUE(VT_NaCf1), NaCf2 = CELLMODEL_PARAMVALUE(VT_NaCf2), NaCf3 = CELLMODEL_PARAMVALUE(VT_NaCf3);
  const double NaScalinga = CELLMODEL_PARAMVALUE(VT_NaScalinga);
  const double NaQ = CELLMODEL_PARAMVALUE(VT_NaQ);

  /*     cerr << "Naa " << Naa1 << " " << Naa2 << " " << Naa3 << endl;  */
  /*     cerr << " " << Tx << " " << ElphyModelConstants::k << " " << ElphyModelConstants::h << endl;  */
  /*     cerr << " " << ElphyModelConstants::R << " " << VFdRT << " " << NaQ << endl;  */

  const double TNa = 294.16;
  const double kTNadh = ElphyModelConstants::k * TNa / ElphyModelConstants::h;
  const double RTNa = ElphyModelConstants::R * TNa;
  const double VFdRTNa = V_int / (RTNa / ElphyModelConstants::F);

  const double Naa =
      NaQ * kTNadh * exp(-Naa1 / RTNa + Naa2 / ElphyModelConstants::R + Naa3 * VFdRTNa);
  const double Nab =
      NaQ * kTNadh * exp(-Nab1 / RTNa + Nab2 / ElphyModelConstants::R + Nab3 * VFdRTNa);
  const double Nag =
      NaQ * kTNadh * exp(-Nag1 / RTNa + Nag2 / ElphyModelConstants::R + Nag3 * VFdRTNa);
  const double Nad =
      NaQ * kTNadh * exp(-Nad1 / RTNa + Nad2 / ElphyModelConstants::R + Nad3 * VFdRTNa);
  const double NaOn =
      NaQ * kTNadh * exp(-NaOn1 / RTNa + NaOn2 / ElphyModelConstants::R + NaOn3 * VFdRTNa);
  const double NaOf =
      NaQ * kTNadh * exp(-NaOf1 / RTNa + NaOf2 / ElphyModelConstants::R + NaOf3 * VFdRTNa);
  const double Nagg =
      NaQ * kTNadh * exp(-Nagg1 / RTNa + Nagg2 / ElphyModelConstants::R + Nagg3 * VFdRTNa);
  const double Nadd =
      NaQ * kTNadh * exp(-Nadd1 / RTNa + Nadd2 / ElphyModelConstants::R + Nadd3 * VFdRTNa);
  const double Nae =
      NaQ * kTNadh * exp(-Nae1 / RTNa + Nae2 / ElphyModelConstants::R + Nae3 * VFdRTNa);
  const double NaO =
      NaQ * kTNadh * exp(-NaO1 / RTNa + NaO2 / ElphyModelConstants::R + NaO3 * VFdRTNa);
  const double Naeta =
      NaQ * kTNadh * exp(-Naeta1 / RTNa + Naeta2 / ElphyModelConstants::R + Naeta3 * VFdRTNa);
  const double Nanu =
      NaQ * kTNadh * exp(-Nanu1 / RTNa + Nanu2 / ElphyModelConstants::R + Nanu3 * VFdRTNa);
  const double NaCn =
      NaQ * kTNadh * exp(-NaCn1 / RTNa + NaCn2 / ElphyModelConstants::R + NaCn3 * VFdRTNa);
  const double NaCf =
      NaQ * kTNadh * exp(-NaCf1 / RTNa + NaCf2 / ElphyModelConstants::R + NaCf3 * VFdRTNa);

  //    if (Nag>1e6) Nag=1e6;
  //    if (Naeta>1e6) Naeta=1e6;

  /*    cerr << Naa2 << " " << Naa2/R << " " << exp(Naa2/R) << endl; */
  /*            cerr << "NaRates: " << Naa << ' ' //1 */
  /*           << Nab << ' '  */
  /*           << Nag << ' '  */
  /*           << Nad << ' '  */
  /*           << NaOn << ' ' //5 */
  /*           << NaOf << ' '  */
  /*           << Nagg << ' '  */
  /*           << Nadd << ' '  */
  /*           << Nae << ' '  */
  /*           << NaO << ' ' //10 */
  /*           << Naeta << ' '  */
  /*           << Nanu  << ' '  */
  /*           << NaCn << ' '  */
  /*           << NaCf << endl;  */


  const double NaScalinga2 = NaScalinga * NaScalinga;
  const double NaScalinga3 = NaScalinga2 * NaScalinga;
  const double NaScalinga4 = NaScalinga3 * NaScalinga;

  const double dC0Na = (-(NaCn + 4. * Naa) * C0Na + Nab * C1Na + NaCf * CI0Na);
  const double dC1Na = (-(Nab + NaCn * NaScalinga + 3. * Naa) * C1Na + 4. * Naa * C0Na +
                        2. * Nab * C2Na + NaCf / NaScalinga * CI1Na);
  const double dC2Na =
      (-(2. * Nab + NaCn * NaScalinga2 + 2. * Naa) * C2Na + 3. * Naa * C1Na + 3. * Nab * C3Na +
       NaCf / NaScalinga2 * CI2Na);
  const double dC3Na =
      (-(3. * Nab + NaCn * NaScalinga3 + Naa) * C3Na + 2. * Naa * C2Na + 4. * Nab * C4Na +
       NaCf / NaScalinga3 * CI3Na);
  const double dC4Na =
      (-(4. * Nab + NaCn * NaScalinga4 + Nag + Naeta) * C4Na + Naa * C3Na + Nad * O1Na +
       Nanu * O2Na + NaCf / NaScalinga4 * CI4Na);
  const double dO1Na = (-(Nad + Nae + NaOn) * O1Na + Nag * C4Na + NaO * O2Na + NaOf * INa);
  const double dO2Na = (-(NaO + Nanu) * O2Na + Nae * O1Na + Naeta * C4Na);
  const double dCI0Na =
      (-(4. * Naa * NaScalinga + NaCf) * CI0Na + Nab / NaScalinga * CI1Na + NaCn * C0Na);
  const double dCI1Na =
      (-(Nab / NaScalinga + 3. * Naa * NaScalinga + NaCf / NaScalinga) * CI1Na +
       4. * Naa * NaScalinga * CI0Na + 2. * Nab / NaScalinga * CI2Na + NaCn *
                                                                       NaScalinga * C1Na);
  const double dCI2Na =
      (-(2. * Nab / NaScalinga + 2. * Naa * NaScalinga + NaCf / NaScalinga2) * CI2Na +
       3. * Naa * NaScalinga * CI1Na + 3. * Nab / NaScalinga * CI3Na + NaCn *
                                                                       NaScalinga2 * C2Na);
  const double dCI3Na =
      (-(3. * Nab / NaScalinga + Naa * NaScalinga + NaCf / NaScalinga3) * CI3Na +
       2. * Naa * NaScalinga * CI2Na + 4. * Nab / NaScalinga * CI4Na + NaCn *
                                                                       NaScalinga3 * C3Na);
  const double dCI4Na =
      (-(4. * Nab / NaScalinga + Nagg + NaCf / NaScalinga4) * CI4Na + Naa * NaScalinga * CI3Na +
       Nadd * INa + NaCn *
                    NaScalinga4 * C4Na);

  C0Na += tinc * dC0Na;
  C1Na += tinc * dC1Na;
  C2Na += tinc * dC2Na;
  C3Na += tinc * dC3Na;
  C4Na += tinc * dC4Na;
  if (C4Na < 0)
    C4Na = 0;
  O1Na += tinc * dO1Na;
  O2Na += tinc * dO2Na;
  CI0Na += tinc * dCI0Na;
  CI1Na += tinc * dCI1Na;
  CI2Na += tinc * dCI2Na;
  CI3Na += tinc * dCI3Na;
  CI4Na += tinc * dCI4Na;
  INa = 1. - C0Na - C1Na - C2Na - C3Na - C4Na - O1Na - O2Na - CI0Na - CI1Na - CI2Na - CI3Na - CI4Na;

  /*         cerr << "NaState: " << C0Na << ' ' //1 */
  /*         << C1Na << ' '  */
  /*         << C2Na << ' '   */
  /*         << C3Na << ' '  */
  /*         << C4Na << ' ' //5 */
  /*         << O1Na << ' '  */
  /*         << O2Na << ' '  */
  /*         << CI0Na << ' '  */
  /*         << CI1Na << ' '  */
  /*         << CI2Na << ' '//10  */
  /*         << CI3Na << ' '  */
  /*         << CI4Na << ' '  */
  /*         << INa;  */
  /*    //        cerr << ' '  << I_Na */
  /*    //        << endl;    */

  assert(C0Na >= 0);
  assert(C1Na >= 0);
  assert(C2Na >= 0);
  assert(C3Na >= 0);
  assert(C4Na >= 0);
  assert(O1Na >= 0);
  assert(O2Na >= 0);
  assert(CI0Na >= 0);
  assert(CI1Na >= 0);
  assert(CI2Na >= 0);
  assert(CI3Na >= 0);
  assert(CI4Na >= 0);
  assert(INa >= 0);

  const double I_Na = CELLMODEL_PARAMVALUE(VT_GNa) * (O1Na + O2Na) * (V_int - ENa);

  const double Kra0 = CELLMODEL_PARAMVALUE(VT_Kra0) * exp(.0330 * V_int);
  const double Krb0 = CELLMODEL_PARAMVALUE(VT_Krb0) * exp(-.0431 * V_int);
  const double Kra1 = CELLMODEL_PARAMVALUE(VT_Kra1) * exp(.0262 * V_int);
  const double Krb1 = CELLMODEL_PARAMVALUE(VT_Krb1) * exp(-.0269 * V_int);
  const double Krai = CELLMODEL_PARAMVALUE(VT_Krai) * exp(.0057 * V_int);
  const double Krbi = CELLMODEL_PARAMVALUE(VT_Krbi) * exp(-.0454 * V_int);
  const double Krai3 = CELLMODEL_PARAMVALUE(VT_Krai3) * exp(6.98e-7 * V_int);
  const double Krkf = CELLMODEL_PARAMVALUE(VT_Krkf);
  const double Krkb = CELLMODEL_PARAMVALUE(VT_Krkb);

  const double psi = Krb1 * Krbi * Krai3 / (Kra1 * Krai);

  const double dC1Kr = (-Kra0 * C1Kr + Krb0 * C2Kr);
  const double dC2Kr = (-(Krb0 + Krkf) * C2Kr + Kra0 * C1Kr + Krkb * C3Kr);
  const double dC3Kr = (-(Kra1 + Krai3 + Krkb) * C3Kr + Krkf * C2Kr + Krb1 * OKr + psi * IKr);

  //    const double dOKr= (-(Krb1+Krai)      *OKr +Kra1* C3Kr+Krbi*IKr);
  const double dIKr = (-(psi + Krbi) * IKr + Krai3 * C3Kr + Krai * OKr);

  const double Q10Herg = 5.32;
  C1Kr += Q10Herg * tinc * dC1Kr;
  C2Kr += Q10Herg * tinc * dC2Kr;
  C3Kr += Q10Herg * tinc * dC3Kr;

  // OKr+=Q10Herg*tinc*OKr;
  IKr += Q10Herg * tinc * dIKr;
  OKr = 1. - C1Kr - C2Kr - C3Kr - IKr;

  assert(C1Kr >= 0. && C1Kr <= 1.);
  assert(C2Kr >= 0. && C2Kr <= 1.);
  assert(C3Kr >= 0. && C3Kr <= 1.);
  assert(OKr >= 0. && OKr <= 1.);
  assert(IKr >= 0. && IKr <= 1.);

  const double f = sqrt(Ko / 4.);
  double I_Kr = CELLMODEL_PARAMVALUE(VT_GKr) * f * OKr * (V_int - EK);

  const double Ksa = CELLMODEL_PARAMVALUE(VT_Ksa);
  const double Ksb = CELLMODEL_PARAMVALUE(VT_Ksb) * exp(-.00002 * V_int);
  const double Ksg = CELLMODEL_PARAMVALUE(VT_Ksg);
  const double Ksd = CELLMODEL_PARAMVALUE(VT_Ksd) * exp(-.15 * V_int);
  double Kse = CELLMODEL_PARAMVALUE(VT_Kse) * exp(.087 * V_int);
  const double Kso = CELLMODEL_PARAMVALUE(VT_Kso) * exp(-.014 * V_int);

  const int KsMaxi = 5;
  const double tincKs = tinc / KsMaxi;

  for (int i = 0; i < KsMaxi; i++) {
    const double dC0Ks = (-Ksa * C0Ks + Ksb * C1Ks);
    const double dC1Ks = (-(Ksb + Ksg) * C1Ks + Ksa * C0Ks + Ksd * O1Ks);
    const double dO1Ks = (-(Ksd + Kse) * O1Ks + Ksg * C1Ks + Kso * O2Ks);

    C0Ks += tincKs * dC0Ks;
    C1Ks += tincKs * dC1Ks;
    O1Ks += tincKs * dO1Ks;
    O2Ks = 1. - C0Ks - C1Ks - O1Ks;
    if (O2Ks < 0)
      O2Ks = 0;

    assert(C0Ks >= 0. && C0Ks <= 1.);
    assert(C1Ks >= 0. && C1Ks <= 1.);
    assert(O1Ks >= 0. && O1Ks <= 1.);
    assert(O2Ks >= 0. && O2Ks <= 1.);
  }

#ifdef IKSOPEN
  double I_Ks = CELLMODEL_PARAMVALUE(VT_GKs)*((O1Ks+O2Ks)*(1.-IKSOPEN)+IKSOPEN)*(V_int-EK);
#else // ifdef IKSOPEN
  double I_Ks = CELLMODEL_PARAMVALUE(VT_GKs) * (O1Ks + O2Ks) * (V_int - EK);
#endif // ifdef IKSOPEN
  const double Kv43aa = CELLMODEL_PARAMVALUE(VT_Kv43aa) * exp(.028983 * V_int);
  const double Kv43ba = CELLMODEL_PARAMVALUE(VT_Kv43ba) * exp(-.0468437 * V_int);
  const double Kv43ai = CELLMODEL_PARAMVALUE(VT_Kv43ai) * exp(-0.000373016 * V_int);
  const double Kv43bi = CELLMODEL_PARAMVALUE(VT_Kv43bi) * exp(0.00000005374 * V_int);
  const double Kv43f1 = CELLMODEL_PARAMVALUE(VT_Kv43f1);
  const double Kv43f2 = CELLMODEL_PARAMVALUE(VT_Kv43f2);
  const double Kv43f3 = CELLMODEL_PARAMVALUE(VT_Kv43f3);
  const double Kv43f4 = CELLMODEL_PARAMVALUE(VT_Kv43f4);
  const double Kv43b1 = CELLMODEL_PARAMVALUE(VT_Kv43b1);
  const double Kv43b2 = CELLMODEL_PARAMVALUE(VT_Kv43b2);
  const double Kv43b3 = CELLMODEL_PARAMVALUE(VT_Kv43b3);
  const double Kv43b4 = CELLMODEL_PARAMVALUE(VT_Kv43b4);

  const double dC1Kvf = -(Kv43ba + 3. * Kv43aa + Kv43f1 * Kv43bi) * C1Kvf + 4. * Kv43aa * C0Kvf +
                        2. * Kv43ba * C2Kvf + Kv43ai / Kv43b1 * CI1Kvf;
  const double dC2Kvf =
      -(2. * Kv43ba + 2. * Kv43aa + Kv43f2 * Kv43bi) * C2Kvf + 3. * Kv43aa * C1Kvf +
      3. * Kv43ba * C3Kvf + Kv43ai / Kv43b2 *
                            CI2Kvf;
  const double dC3Kvf =
      -(3. * Kv43ba + Kv43aa + Kv43f3 * Kv43bi) * C3Kvf + 2. * Kv43aa * C2Kvf + 4. * Kv43ba * OKvf +
      Kv43ai / Kv43b3 *
      CI2Kvf;
  const double dOKvf = -(4. * Kv43ba + +Kv43f4 * Kv43bi) * OKvf + Kv43aa * C3Kvf + Kv43ai / Kv43b4 *
                                                                                   CI3Kvf;
  const double dCI0Kvf = -(Kv43b1 * 4. * Kv43aa + Kv43ai) * CI0Kvf +
                         Kv43ba / Kv43f1 * CI1Kvf + Kv43bi * C0Kvf;
  const double dCI1Kvf =
      -(Kv43ba / Kv43f1 + Kv43b2 * 3. * Kv43aa / Kv43b1 + Kv43ai / Kv43b1) * CI1Kvf +
      Kv43b1 * 4. * Kv43aa * CI0Kvf +
      Kv43f1 * 2. * Kv43ba / Kv43f2 * CI2Kvf + Kv43f1 * Kv43bi * C1Kvf;
  const double dCI2Kvf =
      -(Kv43f1 * 2. * Kv43ba / Kv43f2 + Kv43b3 * 2. * Kv43aa / Kv43b2 + Kv43ai / Kv43b2) * CI2Kvf +
      Kv43b2 * 3. * Kv43aa /
      Kv43b1 * CI1Kvf + Kv43f2 * 3. * Kv43ba / Kv43f3 * CI3Kvf + Kv43f2 * Kv43bi * C2Kvf;
  const double dCI3Kvf =
      -(Kv43f2 * 3. * Kv43ba / Kv43f3 + Kv43b4 * Kv43aa / Kv43b3 + Kv43ai / Kv43b3) * CI3Kvf +
      Kv43b3 * 2. * Kv43aa /
      Kv43b2 * CI2Kvf + Kv43f3 * 4. * Kv43ba / Kv43f4 * OIKvf + Kv43f3 * Kv43bi * C3Kvf;
  const double dOIKvf =
      -(Kv43f3 * 4. * Kv43ba / Kv43f4 + Kv43ai / Kv43b4) * OIKvf + Kv43b4 * Kv43aa /
                                                                   Kv43b3 * CI3Kvf +
      Kv43f4 * Kv43bi * OKvf;

  C1Kvf += tinc * dC1Kvf;
  C2Kvf += tinc * dC2Kvf;
  C3Kvf += tinc * dC3Kvf;
  OKvf += tinc * dOKvf;
  CI0Kvf += tinc * dCI0Kvf;
  CI1Kvf += tinc * dCI1Kvf;
  CI2Kvf += tinc * dCI2Kvf;
  CI3Kvf += tinc * dCI3Kvf;
  OIKvf += tinc * dOIKvf;
  C0Kvf = 1. - C1Kvf - C2Kvf - C3Kvf - OKvf - CI0Kvf - CI1Kvf - CI2Kvf - CI3Kvf - OIKvf;

  double I_Kv43 = CELLMODEL_PARAMVALUE(VT_GKv43) * OKvf * (V_int - EK);

  const double Kv14aa = CELLMODEL_PARAMVALUE(VT_Kv14aa) * exp(.0077 * V_int);
  const double Kv14ba = CELLMODEL_PARAMVALUE(VT_Kv14ba) * exp(-.0779 * V_int);
  const double Kv14ai = CELLMODEL_PARAMVALUE(VT_Kv14ai);
  const double Kv14bi = CELLMODEL_PARAMVALUE(VT_Kv14bi);
  const double Kv14f1 = CELLMODEL_PARAMVALUE(VT_Kv14f1);
  const double Kv14f2 = CELLMODEL_PARAMVALUE(VT_Kv14f2);
  const double Kv14f3 = CELLMODEL_PARAMVALUE(VT_Kv14f3);
  const double Kv14f4 = CELLMODEL_PARAMVALUE(VT_Kv14f4);
  const double Kv14b1 = CELLMODEL_PARAMVALUE(VT_Kv14b1);
  const double Kv14b2 = CELLMODEL_PARAMVALUE(VT_Kv14b2);
  const double Kv14b3 = CELLMODEL_PARAMVALUE(VT_Kv14b3);
  const double Kv14b4 = CELLMODEL_PARAMVALUE(VT_Kv14b4);

  const double dC1Kvs = -(Kv14ba + 3. * Kv14aa + Kv14f1 * Kv14bi) * C1Kvs + 4. * Kv14aa * C0Kvs +
                        2. * Kv14ba * C2Kvs + Kv14ai / Kv14b1 * CI1Kvs;
  const double dC2Kvs =
      -(2. * Kv14ba + 2. * Kv14aa + Kv14f2 * Kv14bi) * C2Kvs + 3. * Kv14aa * C1Kvs +
      3. * Kv14ba * C3Kvs + Kv14ai / Kv14b2 *
                            CI2Kvs;
  const double dC3Kvs =
      -(3. * Kv14ba + Kv14aa + Kv14f3 * Kv14bi) * C3Kvs + 2. * Kv14aa * C2Kvs + 4. * Kv14ba * OKvs +
      Kv14ai / Kv14b3 *
      CI2Kvs;
  const double dOKvs = -(4. * Kv14ba + +Kv14f4 * Kv14bi) * OKvs + Kv14aa * C3Kvs + Kv14ai / Kv14b4 *
                                                                                   CI3Kvs;
  const double dCI0Kvs = -(Kv14b1 * 4. * Kv14aa + Kv14ai) * CI0Kvs + Kv14ba /
                                                                     Kv14f1 * CI1Kvs +
                         Kv14bi * C0Kvs;
  const double dCI1Kvs =
      -(Kv14ba / Kv14f1 + Kv14b2 * 3. * Kv14aa / Kv14b1 + Kv14ai / Kv14b1) * CI1Kvs +
      Kv14b1 * 4. * Kv14aa * CI0Kvs +
      Kv14f1 * 2. * Kv14ba / Kv14f2 * CI2Kvs + Kv14f1 * Kv14bi * C1Kvs;
  const double dCI2Kvs =
      -(Kv14f1 * 2. * Kv14ba / Kv14f2 + Kv14b3 * 2. * Kv14aa / Kv14b2 + Kv14ai / Kv14b2) * CI2Kvs +
      Kv14b2 * 3. * Kv14aa /
      Kv14b1 * CI1Kvs + Kv14f2 * 3. * Kv14ba / Kv14f3 * CI3Kvs + Kv14f2 * Kv14bi * C2Kvs;
  const double dCI3Kvs =
      -(Kv14f2 * 3. * Kv14ba / Kv14f3 + Kv14b4 * Kv14aa / Kv14b3 + Kv14ai / Kv14b3) * CI3Kvs +
      Kv14b3 * 2. * Kv14aa /
      Kv14b2 * CI2Kvs + Kv14f3 * 4. * Kv14ba / Kv14f4 * OIKvs + Kv14f3 * Kv14bi * C3Kvs;
  const double dOIKvs =
      -(Kv14f3 * 4. * Kv14ba / Kv14f4 + Kv14ai / Kv14b4) * OIKvs + Kv14b4 * Kv14aa /
                                                                   Kv14b3 * CI3Kvs +
      Kv14f4 * Kv14bi * OKvs;

  C1Kvs += tinc * dC1Kvs;
  C2Kvs += tinc * dC2Kvs;
  C3Kvs += tinc * dC3Kvs;
  OKvs += tinc * dOKvs;
  CI0Kvs += tinc * dCI0Kvs;
  CI1Kvs += tinc * dCI1Kvs;
  CI2Kvs += tinc * dCI2Kvs;
  CI3Kvs += tinc * dCI3Kvs;
  OIKvs += tinc * dOIKvs;
  C0Kvs = 1. - C1Kvs - C2Kvs - C3Kvs - OKvs - CI0Kvs - CI1Kvs - CI2Kvs - CI3Kvs - OIKvs;

  double I_Kv14Na = 0.02 * CELLMODEL_PARAMVALUE(VT_PKv14) * OKvs * VFFdRT * (Nai * expVFdRT - Nao) / (expVFdRT - 1.);
  double I_Kv14K = CELLMODEL_PARAMVALUE(VT_PKv14) * OKvs * VFFdRT * (Ki * expVFdRT - Ko) / (expVFdRT - 1.);
  double I_Kv14 = I_Kv14K + I_Kv14Na;


  const double K1 = 1. / (CELLMODEL_PARAMVALUE(VT_P1K1) + exp(CELLMODEL_PARAMVALUE(VT_P2K1) * FdRT * (V_int - EK)));
  double I_K1 = CELLMODEL_PARAMVALUE(VT_GK1) * K1 * sqrt(Ko) * (V_int - EK);

  const double KmNa = CELLMODEL_PARAMVALUE(VT_KmNa);
  const double KNaCa = CELLMODEL_PARAMVALUE(VT_KNaCa);
  const double KmCa = CELLMODEL_PARAMVALUE(VT_KmCa);
  const double ksat = CELLMODEL_PARAMVALUE(VT_ksat);
  const double NaCaeta = CELLMODEL_PARAMVALUE(VT_NaCaeta);

  const double I_NaCa = KNaCa * 1. / ((KmNa * KmNa * KmNa + Nao * Nao * Nao)
                                      * (KmCa + Cao)
                                      * (1. + ksat * exp((NaCaeta - 1.) * VFdRT)))
                        * (exp(NaCaeta * VFdRT) * Nai * Nai * Nai * Cao -
                           exp((NaCaeta - 1.) * VFdRT) * Nao * Nao * Nao * Cai);

  const double I_Nab = CELLMODEL_PARAMVALUE(VT_GNab) * (V_int - ENa);

  const double sigmaNaK = 1. / 7. * (exp(Nao / 67.3) - 1.);
  const double fNaK = 1 / (1 + .1245 * exp(-.1 * VFdRT) + 0.0365 * sigmaNaK * exp(-1.33 * VFdRT));
  const double KmKo = CELLMODEL_PARAMVALUE(VT_KmKo);
  const double KmNai = CELLMODEL_PARAMVALUE(VT_KmNai);
  const double kNaK = CELLMODEL_PARAMVALUE(VT_kNaK);
  const double I_NaK =
      kNaK * fNaK * 1. / (1. + pow(KmNai / Nai, (double) 1.5)) * (Ko / (Ko + KmKo));

  const double I_pCa = CELLMODEL_PARAMVALUE(VT_IpCa) * Cai / (CELLMODEL_PARAMVALUE(VT_KmpCa) + Cai);

  const double I_Cab = CELLMODEL_PARAMVALUE(VT_GCab) * (V_int - ECa);

  const double CaLa = 1.997 * exp(0.012 * (V_int - 35));
  const double CaLb = 0.0882 * exp(-0.065 * (V_int - 22));
  const double CaLas = CaLa * CELLMODEL_PARAMVALUE(VT_CaLa);
  const double CaLbs = CaLb / (CELLMODEL_PARAMVALUE(VT_CaLb));
  const double CaLg = .0554 * CaSS;
  const double CaLo = CELLMODEL_PARAMVALUE(VT_CaLo);

  const double a1 = CELLMODEL_PARAMVALUE(VT_CaLa), b1 = CELLMODEL_PARAMVALUE(VT_CaLb);
  const double a2 = a1 * a1, b2 = b1 * b1;
  const double a3 = a2 * a1, b3 = b2 * b1;
  const double a4 = a3 * a1, b4 = b3 * b1;

  const double dC1L =
      -(3. * CaLa + CaLb + CaLg * a1) * C1L + 4. * CaLa * C0L + 2. * CaLb * C2L + CaLo / b1 * CCa1L;
  const double dC2L =
      -(2. * CaLa + 2. * CaLb + CaLg * a2) * C2L + 3. * CaLa * C1L + 3. * CaLb * C3L +
      CaLo / b2 * CCa2L;
  const double dC3L =
      -(CaLa + 3. * CaLb + CaLg * a3) * C3L + 2. * CaLa * C2L + 4. * CaLb * C4L + CaLo / b3 * CCa3L;
  const double dC4L = -(CELLMODEL_PARAMVALUE(VT_CaLf) + 4. * CaLb + CaLg * a4) * C4L + CaLa * C3L + CELLMODEL_PARAMVALUE(VT_CaLg) * OL +
                      CaLo / b4 * CCa4L;
  const double dOL = -CELLMODEL_PARAMVALUE(VT_CaLg) * OL + CELLMODEL_PARAMVALUE(VT_CaLf) * C4L;

  const double dCCa0L = -(4. * CaLas + CaLo) * CCa0L + CaLbs * CCa1L + CaLg * C0L;
  const double dCCa1L =
      -(3. * CaLas + CaLbs + CaLo / b1) * CCa1L + 4. * CaLas * CCa0L + 2. * CaLbs * CCa2L +
      CaLg * a1 * C1L;
  const double dCCa2L =
      -(2. * CaLas + 2. * CaLbs + CaLo / b2) * CCa2L + 3. * CaLas * CCa1L + 3. * CaLbs * CCa3L +
      CaLg * a2 * C2L;
  const double dCCa3L =
      -(CaLas + 3. * CaLbs + CaLo / b3) * CCa3L + 2. * CaLas * CCa2L + 4. * CaLbs * CCa4L +
      CaLg * a3 * C3L;
  const double dCCa4L = -(4. * CaLbs + CaLo / b4) * CCa4L + CaLas * CCa3L + +CaLg * a4 * C4L;


  C1L += tinc * dC1L;
  C2L += tinc * dC2L;
  C3L += tinc * dC3L;
  C4L += tinc * dC4L;
  OL += tinc * dOL;
  CCa0L += tinc * dCCa0L;
  CCa1L += tinc * dCCa1L;
  CCa2L += tinc * dCCa2L;
  CCa3L += tinc * dCCa3L;
  CCa4L += tinc * dCCa4L;
  C0L = 1. - C1L - C2L - C3L - C4L - OL - CCa0L - CCa1L - CCa2L - CCa3L - CCa4L;

  assert(C1L >= 0. && C1L <= 1.);
  assert(C2L >= 0. && C2L <= 1.);
  assert(C3L >= 0. && C3L <= 1.);
  assert(C4L >= 0. && C4L <= 1.);
  assert(OL >= 0. && OL <= 1.);
  assert(CCa0L >= 0. && CCa0L <= 1.);
  assert(CCa1L >= 0. && CCa1L <= 1.);
  assert(CCa2L >= 0. && CCa2L <= 1.);
  assert(CCa3L >= 0. && CCa3L <= 1.);
  assert(C0L >= 0. && C0L <= 1.);

#ifdef TIMOTHY2
  {
    const double CaLa = 1.997 * exp(0.012 * (V_int - 35 - CELLMODEL_PARAMVALUE(VT_CaLShift) * 1000));
    const double CaLb = 0.0882 * exp(-0.065 * (V_int - 22 - CELLMODEL_PARAMVALUE(VT_CaLShift) * 1000));
    const double CaLas = CaLa * CELLMODEL_PARAMVALUE(VT_CaLa);
    const double CaLbs = CaLb / (CELLMODEL_PARAMVALUE(VT_CaLb));
    const double CaLg = .0554 * CaSS;
    const double CaLo = CELLMODEL_PARAMVALUE(VT_CaLo);

    const double dC1L = -(3. * CaLa + CaLb + CaLg * a1) * C1Ltimothy + 4. * CaLa * C0Ltimothy +
                        2. * CaLb * C2Ltimothy + CaLo / b1 *
                                                 CCa1Ltimothy;
    const double dC2L = -(2. * CaLa + 2. * CaLb + CaLg * a2) * C2Ltimothy + 3. * CaLa * C1Ltimothy +
                        3. * CaLb * C3Ltimothy + CaLo / b2 *
                                                 CCa2Ltimothy;
    const double dC3L = -(CaLa + 3. * CaLb + CaLg * a3) * C3Ltimothy + 2. * CaLa * C2Ltimothy +
                        4. * CaLb * C4Ltimothy + CaLo / b3 *
                                                 CCa3Ltimothy;
    const double dC4L = -(CELLMODEL_PARAMVALUE(VT_CaLf) + 4. * CaLb + CaLg * a4) * C4Ltimothy + CaLa * C3Ltimothy +
                        CELLMODEL_PARAMVALUE(VT_CaLg) * OLtimothy + CaLo / b4 *
                                                 CCa4Ltimothy;
    const double dOL = -CELLMODEL_PARAMVALUE(VT_CaLg) * OLtimothy + CELLMODEL_PARAMVALUE(VT_CaLf) * C4Ltimothy;

    const double dCCa0L = -(4. * CaLas + CaLo) * CCa0Ltimothy + CaLbs * CCa1Ltimothy +
                          CaLg * C0Ltimothy;
    const double dCCa1L =
        -(3. * CaLas + CaLbs + CaLo / b1) * CCa1Ltimothy + 4. * CaLas * CCa0Ltimothy +
        2. * CaLbs * CCa2Ltimothy +
        CaLg * a1 * C1Ltimothy;
    const double dCCa2L =
        -(2. * CaLas + 2. * CaLbs + CaLo / b2) * CCa2Ltimothy + 3. * CaLas * CCa1Ltimothy +
        3. * CaLbs * CCa3Ltimothy +
        CaLg * a2 * C2Ltimothy;
    const double dCCa3L =
        -(CaLas + 3. * CaLbs + CaLo / b3) * CCa3Ltimothy + 2. * CaLas * CCa2Ltimothy +
        4. * CaLbs * CCa4Ltimothy + CaLg *
                                    a3 * C3Ltimothy;
    const double dCCa4L =
        -(4. * CaLbs + CaLo / b4) * CCa4Ltimothy + CaLas * CCa3Ltimothy + +CaLg * a4 *
                                                                          C4Ltimothy;

    C1Ltimothy += tinc * dC1L;
    C2Ltimothy += tinc * dC2L;
    C3Ltimothy += tinc * dC3L;
    C4Ltimothy += tinc * dC4L;
    OLtimothy += tinc * dOL;
    CCa0Ltimothy += tinc * dCCa0L;
    CCa1Ltimothy += tinc * dCCa1L;
    CCa2Ltimothy += tinc * dCCa2L;
    CCa3Ltimothy += tinc * dCCa3L;
    CCa4Ltimothy += tinc * dCCa4L;
    C0Ltimothy = 1. - C1Ltimothy - C2Ltimothy - C3Ltimothy - C4Ltimothy - OLtimothy - CCa0Ltimothy -
                 CCa1Ltimothy - CCa2Ltimothy -
                 CCa3Ltimothy - CCa4Ltimothy;

    if (C0Ltimothy < 0.)
      C0Ltimothy = 0.;
    else if (C0Ltimothy > 1.)
      C0Ltimothy = 1.;
    if (C1Ltimothy < 0.)
      C1Ltimothy = 0.;
    else if (C1Ltimothy > 1.)
      C1Ltimothy = 1.;
    if (C2Ltimothy < 0.)
      C2Ltimothy = 0.;
    else if (C2Ltimothy > 1.)
      C2Ltimothy = 1.;
    if (C3Ltimothy < 0.)
      C3Ltimothy = 0.;
    else if (C3Ltimothy > 1.)
      C3Ltimothy = 1.;
    if (C4Ltimothy < 0.)
      C4Ltimothy = 0.;
    else if (C4Ltimothy > 1.)
      C4Ltimothy = 1.;
    if (CCa0Ltimothy < 0.)
      CCa0Ltimothy = 0.;
    else if (CCa0Ltimothy > 1.)
      CCa0Ltimothy = 1.;
    if (CCa1Ltimothy < 0.)
      CCa1Ltimothy = 0.;
    else if (CCa1Ltimothy > 1.)
      CCa1Ltimothy = 1.;
    if (CCa2Ltimothy < 0.)
      CCa2Ltimothy = 0.;
    else if (CCa2Ltimothy > 1.)
      CCa2Ltimothy = 1.;
    if (CCa3Ltimothy < 0.)
      CCa3Ltimothy = 0.;
    else if (CCa3Ltimothy > 1.)
      CCa3Ltimothy = 1.;
    if (CCa4Ltimothy < 0.)
      CCa4Ltimothy = 0.;
    else if (CCa4Ltimothy > 1.)
      CCa4Ltimothy = 1.;
  }
#endif // ifdef TIMOTHY2


#ifdef TIMOTHY
                                                                                                                          const double yinf = .985/(1.+exp((V_int+30)/ (7)))  // fit to Splawski 2004
    +.1/(1.+exp((-V_int+39)/ (20)))
    -0.021;
#else // ifdef TIMOTHY
# ifdef TIMOTHY2
  const double yinf = .9 / (1. + exp((V_int + 28.5) / 7.8)) + .1;
# else // ifdef TIMOTHY2
  const double yinf = .82/(1.+exp((V_int+28.5)/7.8))+.18;  // original Winslow
# endif // ifdef TIMOTHY2
#endif // ifdef TIMOTHY

#ifdef TIMOTHY

                                                                                                                          /*     const double yinftimothy=.6/(1.+exp((V_int+20)/ (10)))  */  // simplified fit to Splawski 2004

  /*       +1.8/(1.+exp((-V_int+55)/ (10))) */
  /*       +.4; */

  const double yinftimothy = .54/(1.+exp((V_int+13)/ (10)))  // improved fit to Splawski 2004
    +1.1/(1.+exp((-V_int+49)/ (8)))
    +.4;
#endif // ifdef TIMOTHY

  //    const double ty=1./(.00653/(.5+exp(-V_int/7.1))+.00512*exp(-V_int/39.8)); // rev 1.0
  double ty =
      1. / (.003363 / (.5 + exp(-V_int / 5.5389)) + .00779 * exp(-V_int / 49.51));  // rev 1.03

  y += tinc * (yinf - y) / ty;

  assert(y >= 0. && y <= 1.);

#ifdef TIMOTHY
                                                                                                                          ytimothy += tinc*(yinftimothy-ytimothy)/ty;

  if (ytimothy < 0.)
    ytimothy = 0.; else if (ytimothy > 1.)
    ytimothy = 1.;
#endif // ifdef TIMOTHY

#ifdef TIMOTHY2
  const double yinftimothy =
      CELLMODEL_PARAMVALUE(VT_y1) / (1. + exp((V_int + CELLMODEL_PARAMVALUE(VT_y2)) / (CELLMODEL_PARAMVALUE(VT_y3)))) + 1 - CELLMODEL_PARAMVALUE(VT_y1);
  assert(yinftimothy >= 0. && yinftimothy <= 1.);

  ty *= CELLMODEL_PARAMVALUE(VT_tytimothyscale);
  ytimothy += tinc * (yinftimothy - ytimothy) / ty;
  assert(ytimothy >= 0. && ytimothy <= 1.);
#endif // ifdef TIMOTHY2

  const double PCa = CELLMODEL_PARAMVALUE(VT_PCa) * CELLMODEL_PARAMVALUE(VT_PScale);
  const double ICas =
      PCa * 4. * VFFdRT * (.001 * exp(2. * VFdRT) - 0.341 * Cao) / (exp(2. * VFdRT) - 1.);

#ifdef TIMOTHY
  const double I_Ca = ICas*((1.-TIMOTHY)*y+TIMOTHY*ytimothy)*OL;
#else // ifdef TIMOTHY
# ifdef TIMOTHY2
  const double PCatimothy = CELLMODEL_PARAMVALUE(VT_PCatimothy) * CELLMODEL_PARAMVALUE(VT_PScale);
  const double ICastimothy = ICas / PCa * PCatimothy;
  const double I_Ca =
      ICas * ((1. - TIMOTHY2) * y * OL) + ICastimothy * TIMOTHY2 * ytimothy * OLtimothy;
# else // ifdef TIMOTHY2
  const double I_Ca = ICas*y*OL;
# endif // ifdef TIMOTHY2
#endif // ifdef TIMOTHY

  double PK = CELLMODEL_PARAMVALUE(VT_PK) * CELLMODEL_PARAMVALUE(VT_PScale);
  PK /= (1. + min(0., ICas) / (CELLMODEL_PARAMVALUE(VT_ICahalf)));


#ifdef TIMOTHY
  const double I_CaK = PK*((1.-TIMOTHY)*y+TIMOTHY*ytimothy)*OL*VFFdRT*((Ki*expVFdRT-Ko)/(expVFdRT-.1));
#else // ifdef TIMOTHY
# ifdef TIMOTHY2
  const double PKtimothy = PK / PCa * PCatimothy;
  const double I_CaK = (PK * (1. - TIMOTHY2) * y + PKtimothy * TIMOTHY2 * ytimothy) * OL * VFFdRT *
                       ((Ki * expVFdRT - Ko) / (expVFdRT - .1));
# else // ifdef TIMOTHY2
  const double I_CaK = PK*y*OL*VFFdRT*((Ki*expVFdRT-Ko)/(expVFdRT-.1));
# endif // ifdef TIMOTHY2
#endif // ifdef TIMOTHY

  const double SRn = CELLMODEL_PARAMVALUE(VT_SRn);
  const double SRm = CELLMODEL_PARAMVALUE(VT_SRm);
  const double CaSSn = pow(CaSS * 1000, SRn);
  const double CaSSm = pow(CaSS * 1000, SRm);

  const double SRKap = CELLMODEL_PARAMVALUE(VT_SRKap);
  const double SRKam = CELLMODEL_PARAMVALUE(VT_SRKam);
  const double SRKbp = CELLMODEL_PARAMVALUE(VT_SRKbp);
  const double SRKbm = CELLMODEL_PARAMVALUE(VT_SRKbm);
  const double SRKcp = CELLMODEL_PARAMVALUE(VT_SRKcp);
  const double SRKcm = CELLMODEL_PARAMVALUE(VT_SRKcm);
  const double SRv1 = CELLMODEL_PARAMVALUE(VT_SRv1);
  const double SRKfb = CELLMODEL_PARAMVALUE(VT_SRKfb);
  const double SRNfb = CELLMODEL_PARAMVALUE(VT_SRNfb);
  const double SRKrb = CELLMODEL_PARAMVALUE(VT_SRKrb);
  const double SRNrb = CELLMODEL_PARAMVALUE(VT_SRNrb);
  const double SRvmaxf = CELLMODEL_PARAMVALUE(VT_SRvmaxf);
  const double SRvmaxr = CELLMODEL_PARAMVALUE(VT_SRvmaxr);
  const double SRKSR = CELLMODEL_PARAMVALUE(VT_SRKSR);

  const int RyRMaxi = 20;
  const double tincRyR = tinc / RyRMaxi;

  for (int i = 0; i < RyRMaxi; i++) {
    const double dPC1 = -SRKap * CaSSn * PC1 + SRKam * PO1;
    const double dPO1 =
        -(SRKam + SRKcp + SRKbp * CaSSm) * PO1 + SRKap * CaSSn * PC1 + SRKbm * PO2 + SRKcm * PC2;
    const double dPO2 = -SRKbm * PO2 + SRKbp * CaSSm * PO1;

    PC1 += tincRyR * dPC1;
    PO1 += tincRyR * dPO1;
    PO2 += tincRyR * dPO2;
    PC2 = 1. - PC1 - PO1 - PO2;
  }
  assert(PC1 >= 0. && PC1 <= 1.);
  assert(PO1 >= 0. && PO1 <= 1.);
  assert(PC2 >= 0. && PC2 <= 1.);
  assert(PO2 >= 0. && PO2 <= 1.);

#ifdef SPONTANEOUSRELEASE
                                                                                                                          if ((CaJSR > SPONTANEOUSRELEASE) && ( (PO1+PO2) < 0.01) ) {
    PC1 = 0.09;
    PO1 = 0.012;
    PO2 = 0.06;
    PC2 = 1.-PC2-PO1-PO2;
  }
#endif // ifdef SPONTANEOUSRELEASE
  const double Jrel = SRv1 * (PO1 + PO2) * (CaJSR - CaSS) * 1000;

  const double fb = pow(Cai / SRKfb, SRNfb);
  const double rb = pow(CaNSR / SRKrb, SRNrb);

  const double Jup = SRKSR * (SRvmaxf * fb - SRvmaxr * rb) / (1. + fb + rb) * 1000;

  const double ttr = CELLMODEL_PARAMVALUE(VT_ttr);
  const double txfer = CELLMODEL_PARAMVALUE(VT_txfer);
  const double HTRPNtot = CELLMODEL_PARAMVALUE(VT_HTRPNtot);
  const double LTRPNtot = CELLMODEL_PARAMVALUE(VT_LTRPNtot);
  const double KHTRPNp = CELLMODEL_PARAMVALUE(VT_KHTRPNp);
  const double KHTRPNm = CELLMODEL_PARAMVALUE(VT_KHTRPNm);
  const double KLTRPNp = CELLMODEL_PARAMVALUE(VT_KLTRPNp);
  const double KLTRPNm = CELLMODEL_PARAMVALUE(VT_KLTRPNm);
  const double KmCMDN = CELLMODEL_PARAMVALUE(VT_KmCMDN);
  const double KmCSQN = CELLMODEL_PARAMVALUE(VT_KmCSQN);
  const double KmEGTA = CELLMODEL_PARAMVALUE(VT_KmEGTA);
  const double EGTAtot = CELLMODEL_PARAMVALUE(VT_EGTAtot);
  const double CSQNtot = CELLMODEL_PARAMVALUE(VT_CSQNtot);
  const double CMDNtot = CELLMODEL_PARAMVALUE(VT_CMDNtot);

  const double Jtr = (CaNSR - CaJSR) / ttr * 1000;
  const double Jxfer = (CaSS - Cai) / txfer * 1000;

  const double dHTRPN = (KHTRPNp * Cai * (1. - HTRPNCa) - KHTRPNm * HTRPNCa);
  const double dLTRPN = (KLTRPNp * Cai * (1. - LTRPNCa) - KLTRPNm * LTRPNCa);
  const double Jtrpn = (HTRPNtot * dHTRPN + LTRPNtot * dLTRPN) * 1000;

  HTRPNCa += tinc * dHTRPN;
  LTRPNCa += tinc * dLTRPN;

  const double ACVmyoF = Acap / (Vmyo * F);
  const double ACVSSF = Acap / (2. * VSS * F);
  const double bi = 1 / (1. + CMDNtot * KmCMDN / ((KmCMDN + Cai) * (KmCMDN + Cai)) +
                         EGTAtot * KmEGTA / ((KmEGTA + Cai) * (KmEGTA + Cai)));
  const double bSS = 1 /
                     (1. + CMDNtot * KmCMDN / ((KmCMDN + CaSS) * (KmCMDN + CaSS)) +
                      EGTAtot * KmEGTA / ((KmEGTA + CaSS) * (KmEGTA + CaSS)));
  const double bJSR = 1 / (1. + CSQNtot * KmCSQN / ((KmCSQN + CaJSR) * (KmCSQN + CaJSR)));

  tinc *= 0.001;

  Nai += tinc * -(I_Na + I_Nab + 3. * I_NaCa + 3. * I_NaK + I_Kv14Na) * ACVmyoF;
  Ki += tinc * -(I_Kr + I_Ks + I_Kv43 + I_Kv14K + I_K1 + I_CaK - 2. * I_NaK - I_Stim) * ACVmyoF;
  Cai += tinc * bi * (Jxfer - Jup - Jtrpn - (I_Cab - 2. * I_NaCa + I_pCa) * .5 * ACVmyoF);
  CaSS += tinc * bSS * ((Jrel * VJSR - Jxfer * Vmyo) / VSS - I_Ca * .5 * ACVSSF);
  CaJSR += tinc * bJSR * (Jtr - Jrel);
  CaNSR += tinc * ((Jup * Vmyo - Jtr * VJSR) / VNSR);

  return -tinc *
         (I_Na + I_Ca + I_CaK + I_Kr + I_Ks + I_K1 + I_NaCa + I_NaK + I_Kv14 + I_Kv43 + I_pCa +
          I_Cab + I_Nab - I_Stim);
} // IyerMazhariWinslow::Calc
