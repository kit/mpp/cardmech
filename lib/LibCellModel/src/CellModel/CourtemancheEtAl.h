/*      File: CourtemancheEtAl.h
    automatically created by ExtractParameterClass.pl - done by dw (01.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#ifndef COURTEMANCHE
#define COURTEMANCHE

#include <CourtemancheParameters.h>

#define HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_CourtemancheParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pCmP->P[NS_CourtemancheParameters::a].value
#endif // ifdef HETERO

class Courtemanche : public vbElphyModel<ML_CalcType> {
public:
  CourtemancheParameters *pCmP;
  ML_CalcType Ca_i;
  ML_CalcType Ca_up, Ca_rel;
  ML_CalcType h, j, oi, ui, Xr, Xs, u, v, w, y;
  ML_CalcType Na_i, K_i;
  ML_CalcType m, oa, ua, d, f, f_Ca, dT, fT;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  Courtemanche(CourtemancheParameters *pp);

  ~Courtemanche();

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vcell_outside) * 1e-18; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_Init_Vm); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &Ca_i; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                           int euler);

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class Courtemanche
#endif // ifndef COURTEMANCHE
