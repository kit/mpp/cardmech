# Install script for directory: /home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel

# Set the install prefix
if (NOT DEFINED CMAKE_INSTALL_PREFIX)
    set(CMAKE_INSTALL_PREFIX "/home/froehlich/M++/HeartModel/lib/LibCellModel")
endif ()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if (NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
    if (BUILD_TYPE)
        string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
                CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
    else ()
        set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
    endif ()
    message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif ()

# Set the component getting installed.
if (NOT CMAKE_INSTALL_COMPONENT)
    if (COMPONENT)
        message(STATUS "Install component: \"${COMPONENT}\"")
        set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
    else ()
        set(CMAKE_INSTALL_COMPONENT)
    endif ()
endif ()

# Install shared libraries without execute permission?
if (NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
    set(CMAKE_INSTALL_SO_NO_EXE "1")
endif ()

# Is this installation the result of a crosscompile?
if (NOT DEFINED CMAKE_CROSSCOMPILING)
    set(CMAKE_CROSSCOMPILING "FALSE")
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/linuxELF" TYPE STATIC_LIBRARY FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/lib/linuxELF/libCellModel.a")
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/linuxELF" TYPE EXECUTABLE FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/bin/linuxELF/HodgkinHuxley")
    if (EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/HodgkinHuxley" AND
            NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/HodgkinHuxley")
        if (CMAKE_INSTALL_DO_STRIP)
            execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/HodgkinHuxley")
        endif ()
    endif ()
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/linuxELF" TYPE EXECUTABLE FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/bin/linuxELF/ElphyModelTest")
    if (EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/ElphyModelTest" AND
            NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/ElphyModelTest")
        if (CMAKE_INSTALL_DO_STRIP)
            execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/ElphyModelTest")
        endif ()
    endif ()
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/linuxELF" TYPE EXECUTABLE FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/bin/linuxELF/CellModelTest")
    if (EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/CellModelTest" AND
            NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/CellModelTest")
        if (CMAKE_INSTALL_DO_STRIP)
            execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/CellModelTest")
        endif ()
    endif ()
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/linuxELF" TYPE EXECUTABLE FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/bin/linuxELF/ForceModelTest")
    if (EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/ForceModelTest" AND
            NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/ForceModelTest")
        if (CMAKE_INSTALL_DO_STRIP)
            execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/ForceModelTest")
        endif ()
    endif ()
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/linuxELF" TYPE EXECUTABLE FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/bin/linuxELF/ForceModelSteadyState")
    if (EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/ForceModelSteadyState" AND
            NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/ForceModelSteadyState")
        if (CMAKE_INSTALL_DO_STRIP)
            execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/ForceModelSteadyState")
        endif ()
    endif ()
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/linuxELF" TYPE EXECUTABLE FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/bin/linuxELF/APAnalysis")
    if (EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/APAnalysis" AND
            NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/APAnalysis")
        if (CMAKE_INSTALL_DO_STRIP)
            execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/APAnalysis")
        endif ()
    endif ()
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/linuxELF" TYPE EXECUTABLE FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/bin/linuxELF/evReplaceValue")
    if (EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/evReplaceValue" AND
            NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/evReplaceValue")
        if (CMAKE_INSTALL_DO_STRIP)
            execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/evReplaceValue")
        endif ()
    endif ()
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/linuxELF" TYPE EXECUTABLE FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/bin/linuxELF/GetEVFile")
    if (EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/GetEVFile" AND
            NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/GetEVFile")
        if (CMAKE_INSTALL_DO_STRIP)
            execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/GetEVFile")
        endif ()
    endif ()
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/linuxELF" TYPE EXECUTABLE FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/bin/linuxELF/CSV2Clamp")
    if (EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/CSV2Clamp" AND
            NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/CSV2Clamp")
        if (CMAKE_INSTALL_DO_STRIP)
            execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/CSV2Clamp")
        endif ()
    endif ()
endif ()

if ("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/linuxELF" TYPE EXECUTABLE FILES "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/bin/linuxELF/Drug2EV")
    if (EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/Drug2EV" AND
            NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/Drug2EV")
        if (CMAKE_INSTALL_DO_STRIP)
            execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/linuxELF/Drug2EV")
        endif ()
    endif ()
endif ()

if (CMAKE_INSTALL_COMPONENT)
    set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else ()
    set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif ()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
        "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/froehlich/M++/HeartModel/lib/LibCellModel/src/CellModel/${CMAKE_INSTALL_MANIFEST}"
        "${CMAKE_INSTALL_MANIFEST_CONTENT}")
