/**@file KohlNobleParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef KOHL_NOBLE_PARAMETERS
#define KOHL_NOBLE_PARAMETERS

#include <ParameterLoader.h>

namespace NS_KohlNobleParameters {
  enum varType {
    VT_C_m = vtFirst,
    VT_g_b,
    VT_g_stretch,
    VT_E_b,
    VT_E_stretch,
    VT_Vol,
    VT_dC_m,
    VT_Amp,
    vtLast
  };
}

using namespace NS_KohlNobleParameters;

class KohlNobleParameters : public vbNewElphyParameters {
public:
  KohlNobleParameters(const char *);

  ~KohlNobleParameters() {}

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&dC_m-&C_m+1)*sizeof(T);};
  // virtual inline T* GetBase(void) {return C_m;};
  // virtual int GetNumParameters() { return 6; };
  void Init(const char *);

  void Calculate();

  void InitTable();
};

#endif // ifndef KOHL_NOBLE_PARAMETERS
