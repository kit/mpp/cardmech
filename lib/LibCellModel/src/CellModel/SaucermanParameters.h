/*! \file SaucermanParameters.h

   \cell model of rat modeling adrenergic influence

   \version 0.0.0

   \date Created Carola Otto (27.11.2008)\n
       template (00.00.00)\n
       man Gunnar Seemann (27.02.03)\n
       Last Modified Eike Wuelfers (03.02.10)

   \author Carola Otto\n
         Institute of Biomedical Engineering\n
         Universitaet Karlsruhe (TH)\n
         http://www.ibt.uni-karlsruhe.de\n
         Copyright 2000-2009 - All rights reserved.

   // \sa  \ref Saucerman
 */


#ifndef SAUCERMANPARAMETERS_H
#define SAUCERMANPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_SaucermanParameters {
  enum varType {
    VT_R = vtFirst,
    VT_Tx,
    VT_F,
    VT_m_init,
    VT_h_init,
    VT_j_init,
    VT_xr1_init,
    VT_xr2_init,
    VT_xs_init,
    VT_r_init,
    VT_s_init,
    VT_d_init,
    VT_f_init,
    VT_fCa_init,
    VT_g_init,
    VT_K_o,
    VT_Ca_o,
    VT_Na_o,
    VT_Vc,
    VT_Vsr,
    VT_Bufc,
    VT_Kbufc,
    VT_Bufsr,
    VT_Kbufsr,
    VT_taufca,
    VT_taug,
    VT_Vmaxup,
    VT_Kup,
    VT_C,
    VT_g_Kr,
    VT_pKNa,
    VT_g_Ks,
    VT_g_K1,
    VT_g_to,
    VT_g_Na,
    VT_g_bNa,
    VT_KmK,
    VT_KmNa,
    VT_knak,
    VT_g_CaL,
    VT_g_bCa,
    VT_kNaCa,
    VT_KmNai,
    VT_Km_Ca,
    VT_ksat,
    VT_n,
    VT_g_pCa,
    VT_KpCa,
    VT_g_pK,
    VT_V_init,
    VT_Cai_init,
    VT_CaSR_init,
    VT_Nai_init,
    VT_Ki_init,
    VT_s_inf_vHalf,
    VT_tau_s_f1,
    VT_tau_s_slope1,
    VT_tau_s_vHalf1,
    VT_tau_s_f2,
    VT_tau_s_f3,
    VT_m_Xr1_1,
    VT_m_Xr1_2,
    VT_a_Xr1_1,
    VT_a_Xr1_2,
    VT_b_Xr1_1,
    VT_b_Xr1_2,
    VT_K_Q10Xr1,
    VT_m_Xr2_1,
    VT_m_Xr2_2,
    VT_a_Xr2_1,
    VT_a_Xr2_2,
    VT_b_Xr2_1,
    VT_b_Xr2_2,
    VT_K_Q10Xr2,
    VT_inverseVcF2,
    VT_inverseVcF2C,
    VT_inverseVcFC,
    VT_VcdVsr,
    VT_Kupsquare,
    VT_BufcPKbufc,
    VT_Kbufcsquare,
    VT_Kbufc2,
    VT_BufsrPKbufsr,
    VT_Kbufsrsquare,
    VT_Kbufsr2,
    VT_KopKNaNao,
    VT_KmNa3,
    VT_Nao3,
    VT_inverseRTONF,
    VT_RTONF,

    // vtLast=90
    VT_I_Ks_tot,
    VT_I_Ksp_init,
    VT_Nifi,
    VT_IC50_nif,
    VT_g_CaKL,
    VT_g_CaNaL,
    VT_p_ns,
    VT_Kmns,
    VT_CSQNbar,
    VT_KmCsqn,
    VT_KmUp,
    VT_I_upbar,
    VT_Ca_jsr_init,
    VT_Ca_nsr_init,
    VT_nsrbar,
    VT_tau_tr,
    VT_Vjsr,
    VT_Vmyo,
    VT_Vnsr,

    // vtLast=109

    VT_L_init,
    VT_R_init,
    VT_Gs_init,
    VT_b1ARd_init,
    VT_b1ARtot_init,
    VT_b1ARp_init,
    VT_Gsagtptot_init,
    VT_Gsagdp_init,
    VT_Gsbg_init,
    VT_Gsa_gtp_init,
    VT_AC_init,
    VT_Fsk_init,
    VT_cAMP_init,
    VT_PDE_init,
    VT_IBMX_init,
    VT_PKACI_init,
    VT_PKACII_init,
    VT_cAMPtot_init,
    VT_PLBs_init,
    VT_PP1_init,
    VT_Inhib1ptot_init,
    VT_Inhib1p_init,
    VT_LCCap_init,
    VT_LCCbp_init,
    VT_RyRp_init,
    VT_TnIp_init,
    VT_Iks_init,
    VT_Yotiao_init,
    VT_Iksp_init,
    VT_trel_init,
    VT_Kl,
    VT_Kr,
    VT_Kc,
    VT_k_barkp,
    VT_k_barkm,
    VT_k_pkap,
    VT_k_gact,
    VT_k_hyd,
    VT_k_reassoc,
    VT_Ltotmax,
    VT_Gstot,
    VT_Kgsa,
    VT_Kfsk,
    VT_k_ac_basal,
    VT_ATP,
    VT_Km_basal,
    VT_k_ac_gsa,
    VT_Km_gsa,
    VT_k_ac_fsk,
    VT_Km_fsk,
    VT_k_pde3,
    VT_Km_pde3,
    VT_k_pde4,
    VT_Km_pde4,
    VT_Ki_ibmx,
    VT_Fsktot,
    VT_AC_tot,
    VT_PDE4tot,
    VT_IBMXtot,
    VT_PKItot,
    VT_Ki_pki,
    VT_Kd,
    VT_Ka,
    VT_Kb,
    VT_PKAItot,
    VT_PKAIItot,
    VT_PLBtot,
    VT_k_pka_plb,
    VT_Km_pka_plb,
    VT_k_pp1_plb,
    VT_Km_pp1_plb,
    VT_Inhib1tot,
    VT_Ki_inhib1,
    VT_k_pka_i1,
    VT_Km_pka_i1,
    VT_Vmax_pp2a_i1,
    VT_Km_pp2a_i1,
    VT_PP1tot,
    VT_PKAIIlcctot,
    VT_LCCtot,
    VT_epsilon,
    VT_k_pka_lcc,
    VT_Km_pka_lcc,
    VT_k_pp2a_lcc,
    VT_PP2Alcctot,
    VT_Km_pp2a_lcc,
    VT_k_pp1_lcc,
    VT_PP1lcctot,
    VT_Km_pp1_lcc,
    VT_PKAIIryrtot,
    VT_RyRtot,
    VT_kcat_pka_ryr,
    VT_Km_pka_ryr,
    VT_kcat_pp1_ryr,
    VT_PP1ryr,
    VT_Km_pp1_ryr,
    VT_kcat_pp2a_ryr,
    VT_PP2Aryr,
    VT_Km_pp2a_ryr,
    VT_TnItot,
    VT_kcat_pka_tni,
    VT_Km_pka_tni,
    VT_kcat_pp2a_tni,
    VT_PP2Atni,
    VT_Km_pp2a_tni,
    VT_K_yotiao,
    VT_Iks_tot,
    VT_Yotiao_tot,
    VT_PKAII_ikstot,
    VT_PP1_ikstot,
    VT_k_pka_iks,
    VT_Km_pka_iks,
    VT_k_pp1_iks,
    VT_Km_pp1_iks,
    VT_gmaxrel,
    VT_Km_csqn,
    VT_CSQNth,
    VT_gmaxrelol,
    VT_Km_trpn,
    VT_TRPNbar,
    VT_CMDNbar,
    VT_Km_cmdn,
    VT_INDObar,
    VT_Km_indo,
    VT_ACap,
    VT_k_pkam,
    VT_t_init,
    VT_jsrol_time_init,
    VT_Pns,
    VT_xs05_init,
    VT_KmCa,
    VT_Vm_init,
    VT_PDE3tot,
    VT_Amp,
    vtLast
  };
} // namespace NS_SaucermanParameters

using namespace NS_SaucermanParameters;

class SaucermanParameters : public vbNewElphyParameters {
public:
  SaucermanParameters(const char *);

  ~SaucermanParameters();

  void PrintParameters();

  void Calculate();

  void InitTable();

  void Init(const char *);

  ML_CalcType rec_ipK[RTDT];
  ML_CalcType d_inf[RTDT];
  ML_CalcType f_inf[RTDT];
  ML_CalcType a_m[RTDT];
  ML_CalcType a_h[RTDT];
  ML_CalcType a_j[RTDT];
  ML_CalcType b_m[RTDT];
  ML_CalcType b_h[RTDT];
  ML_CalcType b_j[RTDT];
  ML_CalcType tau_Xr1[RTDT];
  ML_CalcType tau_Xs[RTDT];
  ML_CalcType a_r[RTDT];
  ML_CalcType a_s[RTDT];
  ML_CalcType b_r[RTDT];
  ML_CalcType b_s[RTDT];
  ML_CalcType Xr1_inf[RTDT];
  ML_CalcType Rkr[RTDT];
  ML_CalcType Xs_inf[RTDT];
  ML_CalcType s_help[RTDT];
  ML_CalcType tau_d[RTDT];
  ML_CalcType tau_f[RTDT];
  ML_CalcType NaCa_P1[RTDT];
  ML_CalcType NaCa_P2[RTDT];
  ML_CalcType NaCa_P3[RTDT];
  ML_CalcType NaK_P1[RTDT];
  ML_CalcType CaL_P1[RTDT];
  ML_CalcType CaL_P2[RTDT];
  ML_CalcType CaKL_P1[RTDT];
  ML_CalcType CaKL_P2[RTDT];
  ML_CalcType CaNaL_P1[RTDT];
  ML_CalcType CaNaL_P2[RTDT];
  ML_CalcType nsK_P1[RTDT];
  ML_CalcType nsK_P2[RTDT];
  ML_CalcType nsNa_P1[RTDT];
  ML_CalcType nsNa_P2[RTDT];
}; // class SaucermanParameters
#endif // ifndef SAUCERMANPARAMETERS_H
