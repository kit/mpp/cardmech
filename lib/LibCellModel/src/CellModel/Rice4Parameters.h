/**@file Rice4Parameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef RICE4_PARAMETERS
#define RICE4_PARAMETERS

#include <ParameterLoader.h>

namespace NS_Rice4Parameters {
  enum varType {
    VT_P1a = vtFirst,
    VT_P0a,
    VT_N1a,
    VT_P2a,
    VT_P3a,
    VT_TCaa,
    VT_k_m1,
    VT_f,
    VT_k_on,
    VT_k_off,
    VT_K_Ca,

    VT_g_10,
    VT_g_21,
    VT_g_32,
    VT_f_01,
    VT_f_12,
    VT_f_23,
    VT_TCaMax,
    VT_dFmax,
    VT_g_stern,

    VT_mfakt1,
    VT_mfakt2,
    VT_mfakt3,
    vtLast
  };
} // namespace NS_Rice4Parameters

using namespace NS_Rice4Parameters;

class Rice4Parameters : public vbNewForceParameters {
public:
  Rice4Parameters();

  Rice4Parameters(const char *, ForceModelType);

  ~Rice4Parameters() {}

  // virtual inline int GetSize(void) {return (&mfakt3-&P1a)*sizeof(T);};
  // virtual inline T* GetBase(void) {return P1a;};
  // virtual int GetNumParameters(void)=0;

  void Init(const char *, ForceModelType);

  void Calculate();

  void PrintParameters();
};


class Rice4_1Parameters : public Rice4Parameters {
public:
  Rice4_1Parameters(const char *initFile) {
    this->Init(initFile, FMT_Rice4_1);
  }

  ~Rice4_1Parameters() {}
};

class Rice4_2Parameters : public Rice4Parameters {
public:
  Rice4_2Parameters(const char *initFile) {
    this->Init(initFile, FMT_Rice4_2);
  }

  ~Rice4_2Parameters() {}
};

class Rice4_3Parameters : public Rice4Parameters {
public:
  Rice4_3Parameters(const char *initFile) {
    this->Init(initFile, FMT_Rice4_3);
  }

  ~Rice4_3Parameters() {}
};

#endif // ifndef RICE4_PARAMETERS
