/**@file Peterson.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <Peterson.h>

Peterson::Peterson(PetersonParameters *ap) {
  pp = ap;
  Init();
}

void Peterson::Init() {
  P1 = CELLMODEL_PARAMVALUE(VT_P1a);
  P0 = CELLMODEL_PARAMVALUE(VT_P0a);
  N1 = CELLMODEL_PARAMVALUE(VT_N1a);
}

ML_CalcType Peterson::Calc(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType &Ca,
                           int euler = 1) {
  return ForceEulerNEuler(euler, tinc / euler, stretch, Ca);
}

inline ML_CalcType
Peterson::ForceEulerNEuler(int r, ML_CalcType tinc, ML_CalcType stretch, ML_CalcType &Ca) {
  while (r--) {
    double kP0mN0 = CELLMODEL_PARAMVALUE(VT_p_k_off) * P0 - CELLMODEL_PARAMVALUE(VT_p_k_on) * Ca * (1.0 - P0 - P1 - N1);
    double kP0mP1 = CELLMODEL_PARAMVALUE(VT_p_f) * P0 - CELLMODEL_PARAMVALUE(VT_p_g) * P1;
    double kP1mN1 = CELLMODEL_PARAMVALUE(VT_p_k_off_s) * P1 - CELLMODEL_PARAMVALUE(VT_p_k_on_s) * Ca * N1;

    P0 += tinc * (-kP0mN0 - kP0mP1);
    P1 += tinc * (kP0mP1 - kP1mN1);
    N1 += tinc * (kP1mN1 - CELLMODEL_PARAMVALUE(VT_p_g_s) * N1);

    // Ca-=tinc*(kP0mN0+kP1mN1);
  }
  return Overlap(stretch, pp->getOverlapID(), pp->getOverlapParameters()) * (P1 + N1) * CELLMODEL_PARAMVALUE(VT_dFmax);
}

void Peterson::steadyState(ML_CalcType Ca, ML_CalcType stretch) {
  cout << Ca << ' ' <<
       (Overlap(stretch, pp->getOverlapID(),
                pp->getOverlapParameters()) *
        (CELLMODEL_PARAMVALUE(VT_p_f) * CELLMODEL_PARAMVALUE(VT_p_k_on) * Ca * (CELLMODEL_PARAMVALUE(VT_p_g_s) + CELLMODEL_PARAMVALUE(VT_p_k_on_s) * Ca + CELLMODEL_PARAMVALUE(VT_p_k_off_s))) /
        (CELLMODEL_PARAMVALUE(VT_p_f) * CELLMODEL_PARAMVALUE(VT_p_k_on) * Ca * CELLMODEL_PARAMVALUE(VT_p_k_off_s) + CELLMODEL_PARAMVALUE(VT_p_g_s) *
                                                           (CELLMODEL_PARAMVALUE(VT_p_f) * CELLMODEL_PARAMVALUE(VT_p_k_on) * Ca +
                                                            CELLMODEL_PARAMVALUE(VT_p_g) *
                                                            (CELLMODEL_PARAMVALUE(VT_p_k_off) + CELLMODEL_PARAMVALUE(VT_p_k_on) * Ca) +
                                                            CELLMODEL_PARAMVALUE(VT_p_k_off_s) *
                                                            (CELLMODEL_PARAMVALUE(VT_p_f) + CELLMODEL_PARAMVALUE(VT_p_k_off) +
                                                             CELLMODEL_PARAMVALUE(VT_p_k_on) * Ca)) +
         (CELLMODEL_PARAMVALUE(VT_p_f) * CELLMODEL_PARAMVALUE(VT_p_k_on) * Ca + CELLMODEL_PARAMVALUE(VT_p_g) * (CELLMODEL_PARAMVALUE(VT_p_k_off) + CELLMODEL_PARAMVALUE(VT_p_k_on) * Ca)) *
         CELLMODEL_PARAMVALUE(VT_p_k_on_s) * Ca)) * CELLMODEL_PARAMVALUE(VT_dFmax) << ' ' << CELLMODEL_PARAMVALUE(VT_p_f) << ' ' << CELLMODEL_PARAMVALUE(VT_p_g) << ' '
       << CELLMODEL_PARAMVALUE(VT_p_g_s) << ' ' << CELLMODEL_PARAMVALUE(VT_p_k_on) * Ca << ' ' << CELLMODEL_PARAMVALUE(
                                                                VT_p_k_on_s) * Ca << ' '
       << CELLMODEL_PARAMVALUE(VT_p_k_off) << ' ' << CELLMODEL_PARAMVALUE(VT_p_k_off_s) << ' '
       << endl; // pp->p_f/(pp->p_fCELLMODEL_PARAMVALUE(VT_p_g))<<endl;
}

void Peterson::Print(ostream &tempstr) {
  tempstr << 1.0 - N1 - P0 - P1 << ' ' << N1 << ' '
          << P0 << ' ' << P1 << ' ';
}

void Peterson::GetParameterNames(vector<string> &getpara) {
  const int numpara = 4;
  const string ParaNames[numpara] = {"N0", "N1", "P0", "P1"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
