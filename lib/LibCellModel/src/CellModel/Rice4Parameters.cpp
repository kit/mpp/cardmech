/**@file Rice4Parameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <Rice4Parameters.h>

Rice4Parameters::Rice4Parameters() {
  P = new Parameter[vtLast];
}

Rice4Parameters::Rice4Parameters(const char *initFile, ForceModelType fmt) {
  P = new Parameter[vtLast];
  Init(initFile, fmt);
}

void Rice4Parameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "Rice4Parameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void Rice4Parameters::Init(const char *initFile, ForceModelType fmt) {
  P[VT_P1a].name = "P1";
  P[VT_P0a].name = "P0";
  P[VT_N1a].name = "N1";
  P[VT_P2a].name = "P2";
  P[VT_P3a].name = "P3";
  P[VT_TCaa].name = "TCa";
  P[VT_k_m1].name = "k_m1";
  P[VT_f].name = "f";
  P[VT_k_on].name = "k_on";
  P[VT_k_off].name = "k_off";
  P[VT_K_Ca].name = "K_Ca";

  P[VT_g_10].name = "g_10";
  P[VT_g_21].name = "g_21";
  P[VT_g_32].name = "g_32";
  P[VT_f_01].name = "f_01";
  P[VT_f_12].name = "f_12";
  P[VT_f_23].name = "f_23";
  P[VT_TCaMax].name = "TCa_max";
  P[VT_dFmax].name = "dFmax";
  P[VT_g_stern].name = "g_stern";

  P[VT_mfakt1].name = "f_01";
  P[VT_mfakt2].name = "f_12";
  P[VT_mfakt3].name = "f_23";

  P[VT_f_01].readFromFile = false;
  P[VT_f_12].readFromFile = false;
  P[VT_f_23].readFromFile = false;
  P[VT_K_Ca].readFromFile = false;
  P[VT_dFmax].readFromFile = false;

  switch (fmt) {
    case FMT_Rice4_1:
      P[VT_g_21].readFromFile = false;
      P[VT_g_21].value = 1.0;
      P[VT_g_32].readFromFile = false;
      P[VT_g_32].value = 1.0;
      P[VT_mfakt2].readFromFile = false;
      P[VT_mfakt2].value = 0.0;
      P[VT_mfakt3].readFromFile = false;
      P[VT_mfakt3].value = 0.0;
      P[VT_P2a].readFromFile = false;
      P[VT_P2a].value = 0.0;
      P[VT_P3a].readFromFile = false;
      P[VT_P3a].value = 0.0;
      break;
    case FMT_Rice4_2:
      P[VT_g_32].readFromFile = false;
      P[VT_g_32].value = 1.0;
      P[VT_mfakt3].readFromFile = false;
      P[VT_mfakt3].value = 0.0;
      P[VT_P3a].readFromFile = false;
      P[VT_P3a].value = 0.0;
      break;
    case FMT_Rice4_3:
      break;
    default:
      cerr << "Wrong force model descriptor for Rice4Parameters: " << fmt;
      break;
  } // switch

  ParameterLoader FPL(initFile, fmt);
  this->setOverlapParameters(FPL.getOverlapString());
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = FPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
} // Rice4Parameters::Init

void Rice4Parameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();

  P[VT_K_Ca].value = P[VT_k_off].value / (P[VT_k_on].value);
  P[VT_f_01].value = P[VT_mfakt1].value * P[VT_f].value;
  P[VT_f_12].value = P[VT_mfakt2].value * P[VT_f].value;
  P[VT_f_23].value = P[VT_mfakt3].value * P[VT_f].value;

  ML_CalcType g_10stern = P[VT_g_10].value * P[VT_g_stern].value * 1.32987698;
  ML_CalcType g_21stern = P[VT_g_21].value * P[VT_g_stern].value * 1.32987698;
  ML_CalcType g_32stern = P[VT_g_32].value * P[VT_g_stern].value * 1.32987698;

  ML_CalcType k_1 = P[VT_k_m1].value * pow(ML_CalcType(1 + P[VT_K_Ca].value), ML_CalcType(6.5));

  P[VT_dFmax].value =
      (g_10stern * g_21stern * g_32stern * (k_1 + P[VT_k_m1].value) *
       (g_10stern + k_1 + P[VT_k_m1].value) + P[VT_f_01].value *
                                              (g_10stern + k_1) *
                                              (P[VT_f_12].value * (P[VT_f_23].value + g_32stern) *
                                               k_1 +
                                               g_21stern * g_32stern * (k_1 + P[VT_k_m1].value))) /
      (P[VT_f_01].value * g_21stern * g_32stern * k_1 * (P[VT_k_m1].value + g_10stern + k_1) +
       2 * P[VT_f_01].value * P[VT_f_12].value *
       g_32stern * k_1 * (g_10stern + k_1) +
       3 * P[VT_f_01].value * P[VT_f_12].value * P[VT_f_23].value * k_1 * (g_10stern + k_1));
}
