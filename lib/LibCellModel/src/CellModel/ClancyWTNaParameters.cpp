/**@file ClancyWTNaParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <ClancyWTNaParameters.h>

ClancyWTNaParameters::ClancyWTNaParameters(const char *initFile) {
#if KADEBUG
  cerr << "ClancyWTNaParameters::ClancyWTNaParameters "<< initFile << endl;
#endif // if KADEBUG
  P = new Parameter[vtLast];
  Init(initFile);
}

void ClancyWTNaParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "ClancyWTNaParameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void ClancyWTNaParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "ClancyWTNaParameters::init " << initFile << endl;
#endif // if KADEBUG

  P[VT_Tx].name = "Tx";
  P[VT_Nao].name = "Nao";
  P[VT_Nai].name = "Nai";
  P[VT_g_Na].name = "g_Na";
  P[VT_IC3].name = "IC3";
  P[VT_IC2].name = "IC2";
  P[VT_IF].name = "IF";
  P[VT_IM1].name = "IM1";
  P[VT_IM2].name = "IM2";
  P[VT_C1].name = "C1";
  P[VT_C2].name = "C2";
  P[VT_C3].name = "C3";
  P[VT_O].name = "O";
  P[VT_a11a].name = "a11a";
  P[VT_a11b].name = "a11b";
  P[VT_a11c].name = "a11c";
  P[VT_a11d].name = "a11d";
  P[VT_a11e].name = "a11e";
  P[VT_a12a].name = "a12a";
  P[VT_a12b].name = "a12b";
  P[VT_a12c].name = "a12c";
  P[VT_a12d].name = "a12d";
  P[VT_a12e].name = "a12e";
  P[VT_a13a].name = "a13a";
  P[VT_a13b].name = "a13b";
  P[VT_a13c].name = "a13c";
  P[VT_a13d].name = "a13d";
  P[VT_a13e].name = "a13e";
  P[VT_b11a].name = "b11a";
  P[VT_b11b].name = "b11b";
  P[VT_b11c].name = "b11c";
  P[VT_b12a].name = "b12a";
  P[VT_b12b].name = "b12b";
  P[VT_b12c].name = "b12c";
  P[VT_b13a].name = "b13a";
  P[VT_b13b].name = "b13b";
  P[VT_b13c].name = "b13c";
  P[VT_a2a].name = "a2a";
  P[VT_a2b].name = "a2b";
  P[VT_a3a].name = "a3a";
  P[VT_a3b].name = "a3b";
  P[VT_b3a].name = "b3a";
  P[VT_b3b].name = "b3b";
  P[VT_a4a].name = "a4a";
  P[VT_b4a].name = "b4a";
  P[VT_a5a].name = "a5a";
  P[VT_b5a].name = "b5a";
  P[VT_Amp].name = "Amp";

  ParameterLoader EPL(initFile, EMT_ClancyWTNa);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
  InitTable();
} // ClancyWTNaParameters::Init

void ClancyWTNaParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "ClancyWTNaParameters - Calculate ..." << endl;
#endif // if KADEBUG
}

void ClancyWTNaParameters::InitTable() {
#if KADEBUG
  cerr << "ClancyWTNaParameters - InitTable()" << endl;
#endif // if KADEBUG
}
