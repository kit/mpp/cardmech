/**@file NobleForceParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef NOBLE_FORCE_PARAMETERS
#define NOBLE_FORCE_PARAMETERS

#include <ParameterLoader.h>

namespace NS_NobleForceParameters {
  enum varType {
    VT_aCa_calmod = vtFirst,
    VT_aTCa,
    VT_af_LightChain,
    VT_af_CrossBridge,
    VT_a_troponin,
    VT_b_troponin,
    VT_Calmod,
    VT_Troponin,
    VT_KCalTrop,
    VT_SL,
    VT_k_cont2,
    VT_k_cont3,
    VT_k_cont4,
    VT_CBden,
    vtLast
  };
}

using namespace NS_NobleForceParameters;

class NobleForceParameters : public vbNewForceParameters {
public:
  NobleForceParameters(const char *);

  virtual ~NobleForceParameters() {}

  virtual void PrintParameters();

  // virtual inline int GetSize(void){return (&CBden-&aCa_calmod)*sizeof(T);};
  // virtual inline T* GetBase(void){return aCa_calmod;};
  // virtual int GetNumParameters() { return 14; };
  virtual void Init(const char *);

  virtual void Calculate();
};

#endif // ifndef NOBLE_FORCE_PARAMETERS
