/**@file Rice3Parameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef RICE3_PARAMETERS
#define RICE3_PARAMETERS

#include <ParameterLoader.h>

namespace NS_Rice3Parameters {
  enum varType {
    VT_P1a = vtFirst,
    VT_P0a,
    VT_N1a,
    VT_TCaa,
    VT_k_m1,
    VT_f,
    VT_g,
    VT_k_on,
    VT_k_off,
    VT_K_Ca,
    VT_TCaMax,
    VT_Fmax,
    VT_dFmax,
    vtLast
  };
}

using namespace NS_Rice3Parameters;

class Rice3Parameters : public vbNewForceParameters {
public:
  Rice3Parameters(const char *);

  ~Rice3Parameters() {}

  // virtual inline int GetSize(void) {return  (&dFmax-&P1a)*sizeof(T);};
  // virtual inline T* GetBase(void) {return P1a;};
  // virtual int GetNumParameters() { return 10; };
  void Init(const char *);

  void PrintParameters();

  void Calculate();
};

#endif // ifndef RICE3_PARAMETERS
