/**@file Rice5.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef RICE5_H
#define RICE5_H

#include <Rice5Parameters.h>

#undef CELLMODEL_PARAMVALUE
#define CELLMODEL_PARAMVALUE(a) r5p->P[NS_Rice5Parameters::a].value

class Rice5 : public vbForceModel<ML_CalcType> {
public:
  Rice5Parameters *r5p;

  ML_CalcType P1, P0, N1, N0, P2, P3;
  ML_CalcType TCa;

  Rice5(Rice5Parameters *);

  ~Rice5() {}

  virtual inline int GetSize(void) {
    return sizeof(Rice5) - sizeof(Rice5Parameters *) - sizeof(vbForceModel<ML_CalcType>);
  }

  virtual inline ML_CalcType *GetBase(void) { return &P1; }

  virtual void Init();

  virtual inline bool InIsTCa(void) { return true; }

  virtual ML_CalcType CalcTrop(double, ML_CalcType, ML_CalcType, ML_CalcType, int);

  virtual inline ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType &, int);

  virtual inline ML_CalcType ForceEulerNEuler(int, ML_CalcType, ML_CalcType);

  void Print(ostream &);

  virtual void GetParameterNames(vector<string> &);
}; // class Rice5
#endif // ifndef RICE5_H
