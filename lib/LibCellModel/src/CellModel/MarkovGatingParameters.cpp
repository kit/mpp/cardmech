/*! \file MarkovGatingParameters.cpp
   \brief Implementation of Markov model with gating current calculation
   (based on Hille, Ion Channels of Excitable Membranes)

   \author fs, CVRTI - University of Utah, USA
 */

#include <MarkovGatingParameters.h>

MarkovGatingParameters::MarkovGatingParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void MarkovGatingParameters::PrintParameters() {
  cout<<"MarkovGatingParameters:"<<endl;
  for (int x = 0; x < vtLast; x++)
    cout << P[x].value <<"\t" << P[x].value <<endl;
}

void MarkovGatingParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "MarkovGatingParameters:init " << initFile << endl;
#endif // if KADEBUG

  // Initialization of the Parameters ...
  P[VT_Tx].name   =        "Tx";
  P[VT_E].name    =         "E";
  P[VT_G].name    =         "G";
  P[VT_A].name    =         "A";
  P[VT_B].name    =         "B";
  P[VT_C].name    =         "C";
  P[VT_KABa].name =      "KABa";
  P[VT_KABz].name =      "KABz";
  P[VT_KBAa].name =      "KBAa";
  P[VT_KBAz].name =      "KBAz";
  P[VT_zgAB].name =      "zgAB";
  P[VT_KBCa].name =      "KBCa";
  P[VT_KBCz].name =      "KBCz";
  P[VT_KCBa].name =      "KCBa";
  P[VT_KCBz].name =      "KCBz";
  P[VT_zgBC].name =      "zgBC";
  P[VT_Amp].name  =      "Amp";

  ParameterLoader EPL(initFile, EMT_MarkovGating);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
} // MarkovGatingParameters::Init
