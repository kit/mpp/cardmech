/**@file Rice3.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef RICE3_H
#define RICE3_H

#include <Rice3Parameters.h>

#undef CELLMODEL_PARAMVALUE
#define CELLMODEL_PARAMVALUE(a) r3p->P[NS_Rice3Parameters::a].value

class Rice3 : public vbForceModel<ML_CalcType> {
public:
  Rice3Parameters *r3p;

  ML_CalcType P1, P0, N1, TCa;

  Rice3(Rice3Parameters *);

  ~Rice3() {}

  virtual inline int GetSize(void) {
    return sizeof(Rice3) - sizeof(Rice3Parameters *) - sizeof(vbForceModel<ML_CalcType>);
  }

  virtual inline ML_CalcType *GetBase(void) { return &P1; }

  void Init();

  virtual inline bool InIsTCa(void) { return true; }

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType &, int);

  virtual ML_CalcType CalcTrop(double, ML_CalcType, ML_CalcType, ML_CalcType, int);

  virtual inline ML_CalcType ForceEulerNEuler(int, ML_CalcType, ML_CalcType, ML_CalcType);

  virtual void steadyState(ML_CalcType, ML_CalcType);

  virtual void Print(ostream &);

  virtual void GetParameterNames(vector<string> &);
}; // class Rice3
#endif // ifndef RICE3_H
