/*      File: PuglisiBers.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
        Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
        send comments to dw@ibt.uka.de  */

// typical parameters for ElphyModelTest
// tinc 1e-6
// textlen 5e-3
// textbegin 0.02


#ifndef PUGLISIBERS
#define PUGLISIBERS

#include <PuglisiBersParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_PuglisiBersParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pP->P[NS_PuglisiBersParameters::a].value
#endif // ifdef HETERO

class PuglisiBers : public vbElphyModel<ML_CalcType> {
public:
  PuglisiBersParameters *pP;
  ML_CalcType Na_i, Na_o, Nab, K_o, K_b, K_i, Ca_o, Ca_i, Cl_o, Cl_i;
  ML_CalcType m, h, j, d, f, Xr, Xs, b, g, Xtof, Ytof, Xtos, Yto1s, Yto2s, Ca_JSR, Ca_NSR, Ca_iontold, dCa_iont;
  ML_CalcType t_rel, Ca_int_2ms, Ca_int_Jrel;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  PuglisiBers(PuglisiBersParameters *pp);

  ~PuglisiBers();

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_V_myo); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp);  /*20.;*/ }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_V); }

  virtual inline ML_CalcType GetCai() { return CELLMODEL_PARAMVALUE(VT_Ca_i); }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return CELLMODEL_PARAMVALUE(VT_Na_i); }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return CELLMODEL_PARAMVALUE(VT_K_i); }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &Na_i; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual void Init();

  virtual ML_CalcType
  Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch, int euler);

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class PuglisiBers
#endif // ifndef PUGLISIBERS
