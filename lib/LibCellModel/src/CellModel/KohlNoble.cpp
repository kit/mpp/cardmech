/**@file KohlNoble.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <KohlNoble.h>

KohlNoble::KohlNoble(KohlNobleParameters *pp) {
  pKNP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pKNP, NS_KohlNobleParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

#ifdef HETERO

inline bool KohlNoble::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool KohlNoble::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

void KohlNoble::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' ';
}

void KohlNoble::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);

  const double V_int = V * 1000.0;

  const double i_b = CELLMODEL_PARAMVALUE(VT_g_b) * (V_int - CELLMODEL_PARAMVALUE(VT_E_b));
  const double i_stretch = CELLMODEL_PARAMVALUE(VT_g_stretch) * (V_int - CELLMODEL_PARAMVALUE(VT_E_stretch));

  tempstr << i_b << ' ' << i_stretch << ' ';
}

void KohlNoble::GetParameterNames(vector<string> &getpara) {
  // const int numpara=0;
  // const string ParaNames[numpara] = {};
  // for (int i=0; i<numpara; i++)
  // getpara.push_back(ParaNames[i]);
}

void KohlNoble::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 2;
  const string ParaNames[numpara] = {"I_b", "I_stretch"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType
KohlNoble::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
                int euler = 1) {
  const double V_int = V * 1000.0;

  const double i_b = CELLMODEL_PARAMVALUE(VT_g_b) * (V_int - CELLMODEL_PARAMVALUE(VT_E_b));
  const double i_stretch = CELLMODEL_PARAMVALUE(VT_g_stretch) * (V_int - CELLMODEL_PARAMVALUE(VT_E_stretch));

  return tinc * (-i_b - i_stretch + i_external) * (CELLMODEL_PARAMVALUE(VT_dC_m));
}
