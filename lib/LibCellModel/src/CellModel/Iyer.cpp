/**@file Iyer.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifdef osMac_Vec

#define osMac

#include <Iyer.h>

Iyer::Iyer(IyerParameters *pp) {
  pIP = pp;

#ifdef HETERO
  PS = new ParameterSwitch(pIP, NS_IyerParameters::vtLast);
#endif // ifdef HETERO

  cerr<<"using "<<pIP->HM->numCurrents<<" ionic currents and "<<pIP->HM->numConcentrations<<
    " concentrations for calculation ...\n";
  shd = new SO_ionicCurrent<ML_CalcType>*[pIP->HM->numCurrents];
#if KADEBUG
  cerr<<"loading array of string and *SO_ionicCurrent<ML_CalcType> with " << pIP->HM->numCurrents << " fileNames ...\n";
  for (int x = 0; x < pIP->HM->numCurrents; x++)
    cerr<<"x="<<x<<", handle="<<pp->handle[x]<<endl;
#endif // if KADEBUG
  Init();
}

#ifdef HETERO

inline bool Iyer::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool Iyer::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO


Iyer::~Iyer() {
#if KADEBUG
  cerr<<"deleting shd[0 .." << pIP->HM->numCurrents-1 << "] and FN + EV\n";
#endif // if KADEBUG
  if (NA)
    delete NA;
  if (CA)
    delete CA;
  if (KA)
    delete KA;

  if (shd) {
    for (int x = 0; x < pIP->HM->numCurrents; x++)
      delete shd[x];
    delete[] shd;
  }
}

void Iyer::Init() {
#if KADEBUG
  cerr << "initializing Class: Iyer ... " << endl;
#endif // if KADEBUG
  Array[0]  = CELLMODEL_PARAMVALUE(VT_init_Vm)*1000; // Vm    -.09086
  Array[1]  = .00005; // tinc / tinc_1000
  Array[2]  = CELLMODEL_PARAMVALUE(VT_init_Nai); // Na_i
  Array[3]  = CELLMODEL_PARAMVALUE(VT_init_Nao); // Na_o
  Array[4]  = CELLMODEL_PARAMVALUE(VT_init_Ki); // K_i  125.55896
  Array[5]  = CELLMODEL_PARAMVALUE(VT_init_Ko); // K_o  bisher 4.0
  Array[6]  = CELLMODEL_PARAMVALUE(VT_init_Cai); // Ca_i
  Array[7]  = CELLMODEL_PARAMVALUE(VT_init_Cao); // Ca_o
  Array[8]  = CELLMODEL_PARAMVALUE(VT_RTdF); // RTdF
  Array[9]  = CELLMODEL_PARAMVALUE(VT_init_IKv14Na); // IKv14Na
  Array[10] = CELLMODEL_PARAMVALUE(VT_init_CaSS);  // CaSS
  Array[11] = CELLMODEL_PARAMVALUE(VT_init_ICaK);  // ICa,K

  E_to = Array[8]*log((0.043*Array[3]+Array[5])/(0.043*Array[2]+Array[4]));
  E_Na = Array[8]*log(Array[3]/Array[2]);
  E_K  = Array[8]*log(Array[5]/Array[4]);
  E_Ks = Array[8]*log((0.01833*Array[3]+Array[5])/(0.01833*Array[2]+Array[4]));
  E_Ca = 0.5*Array[8]*log(Array[7]/Array[6]);

  cICaK = CELLMODEL_PARAMVALUE(VT_init_ICaK2);  // 3.1106544E-16;
  iStim = GetAmplitude();

  indexINa   = -1;
  indexIto   = -1;
  indexIKr   = -1;
  indexIKs   = -1;
  indexINab  = -1;
  indexIK1   = -1;
  indexICa   = -1;
  indexICab  = -1;
  indexINaCa = -1;
  indexINaK  = -1;
  indexIpCa  = -1;

  cINa   = 0;
  cIto   = 0;
  cIKr   = 0;
  cIKs   = 0;
  cINab  = 0;
  cIKb   = 0;
  cIK1   = 0;
  cICa   = 0;
  cICab  = 0;
  cINaCa = 0;
  cINaK  = 0;
  cIpCa  = 0;

  for (int x = 0; x < pIP->HM->numCurrents; x++) {
    shd[x] = new SO_ionicCurrent<ML_CalcType>(pIP->handle[x], pIP->pEP[x], &Array[0], &Vi);
    if ((pIP->pEP[x])->ReInitRequired) {
      (pIP->pEP[x])->InitTableWithArrayStart(&Array[0]);
    }
    switch (shd[x]->GetEquilibriumType()) {
      case eqE_Na:
        shd[x]->SetEquilibrium(&E_Na);
        break;
      case eqE_to:
        shd[x]->SetEquilibrium(&E_to);
        break;
      case eqE_K:
        shd[x]->SetEquilibrium(&E_K);
        break;
      case eqE_Ks:
        shd[x]->SetEquilibrium(&E_Ks);
        break;
      case eqE_Ca:
        shd[x]->SetEquilibrium(&E_Ca);
        break;
      default:
        throw kaBaseException("no valid equilibrium type defined ...\n");
    }
    c_Type ct = (c_Type)LoadSymbol(pIP->handle[x], "CurrentType");

    if (ct() == INa) {
      indexINa = x;
      cINa     = CELLMODEL_PARAMVALUE(VT_init_INa); // -0.00016805366;
    } else if (ct() == Ito) {
      indexIto = x;
      cIto     = CELLMODEL_PARAMVALUE(VT_init_Ito); // -7.0666049E-07;
    } else if (ct() == IKr) {
      indexIKr = x;
      cIKr     = CELLMODEL_PARAMVALUE(VT_init_IKr); // 2.4815892E-07;
    } else if (ct() == IKs) {
      indexIKs = x;
      cIKs     = CELLMODEL_PARAMVALUE(VT_init_IKs); // 6.4875267E-08;
    } else if (ct() == INab) {
      indexINab = x;
      cINab     = CELLMODEL_PARAMVALUE(VT_init_INab); // -0.16131048;
    } else if (ct() == IK1) {
      indexIK1 = x;
      cIK1     = CELLMODEL_PARAMVALUE(VT_init_IK1); // 0.17502804;
    } else if (ct() == ICa) {
      indexICa = x;
      cICa     = CELLMODEL_PARAMVALUE(VT_init_ICa);     // -1.1989981E-08;
    } else if (ct() == ICab) {
      indexICab = x;
      cICab     = CELLMODEL_PARAMVALUE(VT_init_ICab);   // -0.017284324;
    } else if (ct() == INaCa) {
      indexINaCa = x;
      cINaCa     = CELLMODEL_PARAMVALUE(VT_init_INaCa); // -0.10363922;
    } else if (ct() == INaK) {
      indexINaK = x;
      cINaK     = CELLMODEL_PARAMVALUE(VT_init_INaK); // 0.10078957;
    } else if (ct() == IpCa) {
      indexIpCa = x;
      cIpCa     = CELLMODEL_PARAMVALUE(VT_init_IpCa); // 0.0073382782;
    } else {
      throw kaBaseException("unknown index %i=%i\n", x, ct());
    }

#if KADEBUG
    cerr<<"x="<<x<<": currentType: "<<ct()<<endl;
#endif // if KADEBUG
  }
#if KADEBUG
  cerr<<"initialization of ionic currents finished\n";
#endif // if KADEBUG
  if (pIP->HM->indexNaConcentration)
    NA =
      new SO_NaConc_Handling<ML_CalcType>(pIP->handle[pIP->HM->indexNaConcentration],
                                          pIP->pEP[pIP->HM->indexNaConcentration], &Array[0], &Vi, &dV, &cINa, &cINab,
                                          &cINaCa, &cINaK);
  else
    NA = NULL;
  if (pIP->HM->indexKConcentration)
    KA =
      new SO_KConc_Handling<ML_CalcType>(pIP->handle[pIP->HM->indexKConcentration],
                                         pIP->pEP[pIP->HM->indexKConcentration], &Array[0], &Vi, &dV, &cIKr, &cIKs,
                                         &cIto,
                                         &cIK1, &cINaK, &cICaK, &cIKb, NULL, &iStim);
  else
    KA = NULL;
  if (pIP->HM->indexCaConcentration)
    CA =
      new SO_CaConc_Handling<ML_CalcType>(pIP->handle[pIP->HM->indexCaConcentration],
                                          pIP->pEP[pIP->HM->indexCaConcentration], &Array[0], &Vi, &dV, &cICa, &cICab,
                                          &cINaCa, &cIpCa);
  else
    CA = NULL;
#if KADEBUG
  cerr<<"initialization of concentration handlings finished\n";
#endif // if KADEBUG
} // Iyer::Init

ML_CalcType Iyer::Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch, int euler) {
  const ML_CalcType V_int = V*1000.0;

  Array[0] = V_int;
  Array[1] = tinc*1000;

  iStim = i_external;
  Ca_i  = Array[6];

  Vi = (int)(DivisionTab*(RangeTabhalf+V_int)+.5);

  E_Ca = 0.5*Array[8]*log(Array[7]/Array[6]);

  // cerr<<"ECa: "<<Array[7]<<"\t"<<Array[6]<<"\t"<<log(Array[7]/Array[6])<<"\t"<<E_Ca<<endl;
  E_K  = Array[8]*log(Array[5]/Array[4]);
  E_Na = Array[8]*log(Array[3]/Array[2]);
  if (isnan(Array[6]))
    cerr<<"2:Cai is nan\n";

  if (indexINa > -1)
    cINa = shd[indexINa]->CalcNext();
  if (indexIto > -1)
    cIto = shd[indexIto]->CalcNext();
  if (indexIKr > -1)
    cIKr = shd[indexIKr]->CalcNext();
  if (indexIKs > -1)
    cIKs = shd[indexIKs]->CalcNext();
  if (indexINab > -1)
    cINab = shd[indexINab]->CalcNext();
  if (indexIK1 > -1)
    cIK1 = shd[indexIK1]->CalcNext();
  if (indexICa > -1)
    cICa = shd[indexICa]->CalcNext();
  if (indexICab > -1)
    cICab = shd[indexICab]->CalcNext();
  if (indexINaCa > -1)
    cINaCa = shd[indexINaCa]->CalcNext();
  if (indexINaK > -1)
    cINaK = shd[indexINaK]->CalcNext();
  if (indexIpCa > -1)
    cIpCa = shd[indexIpCa]->CalcNext();

  cICaK = Array[11];

  const ML_CalcType sumI = cINa+cIto+cIKr+cIKs+cINab+cIK1+cICa+cICab+cINaCa+cINaK+cIpCa;

  // cerr<<"sumI:"<<sumI<<endl;
  if (isnan(sumI))
    cerr<<"INa:"<<cINa<<"\tIto:"<<cIto<<"\tIKr:"<<cIKr<<"\tIKs:"<<cIKs<<"\tINab:"<<cINab<<"\tIK1:"<<cIK1<<"\tICa:"<<
      cICa<<"\tICab:"<<cICab<<"\tINaCa:"<<cINaCa<<"\tINaK:"<<cINaK<<"\tIpCa:"<<cIpCa<<"\tNai:"<<Array[2]<<"\tKi:"<<
      Array[4]<<"\tCai:"<<Array[6]<<"\n";
  dV = -tinc*(sumI-iStim);

  if (isnan(cICab))
    cerr<<"ICab\n";

  if (isnan(cINaCa))
    cerr<<"INaCa\n";
  if (isnan(cIpCa))
    cerr<<"IpCa\n";

  if (CA)
    CA->CalcNext();
  if (NA)
    NA->CalcNext();
  if (KA)
    KA->CalcNext();

  if (isnan(Array[6]))
    cerr<<"1:Cai is nan\n";

  return dV;
} // Iyer::Calc

#endif // ifdef osMac_Vec
