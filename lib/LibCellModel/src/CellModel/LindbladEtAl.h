/* File: LindbladEtAl.h
        automatically created by CellML2Elphymodel.pl
        Institute of Biomedical Engineering, Universität Karlsruhe (TH) */

#ifndef LINDBLADETAL
#define LINDBLADETAL

#include <LindbladEtAlParameters.h>

#define HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_LindbladEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) ptTeaP->P[NS_LindbladEtAlParameters::a].value
#endif // ifdef HETERO

class LindbladEtAl : public vbElphyModel<ML_CalcType> {
public:
  LindbladEtAlParameters *ptTeaP;
  ML_CalcType Na_i;
  ML_CalcType m;
  ML_CalcType h1;
  ML_CalcType h2;
  ML_CalcType d_L;
  ML_CalcType f_L;
  ML_CalcType d_T;
  ML_CalcType f_T;
  ML_CalcType K_i;
  ML_CalcType r;
  ML_CalcType s1;
  ML_CalcType s2;
  ML_CalcType s3;
  ML_CalcType z;
  ML_CalcType p_a;
  ML_CalcType p_i;
  ML_CalcType Ca_i;
  ML_CalcType O_C;
  ML_CalcType O_TC;
  ML_CalcType O_TMgC;
  ML_CalcType O_TMgMg;
  ML_CalcType Ca_rel;
  ML_CalcType Ca_up;
  ML_CalcType O_Calse;
  ML_CalcType F1;
  ML_CalcType F2;

  LindbladEtAl(LindbladEtAlParameters *pp);

  ~LindbladEtAl();

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType SurfaceToVolumeRatio() { return 1.0; }

  virtual inline ML_CalcType Volume() { return 1.15606568e-12; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_V_init); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_c); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_c); }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_c); }

  virtual inline ML_CalcType *GetBase(void) { return &Na_i; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType GetSpeedupMax(void) { return .0; }

  virtual ML_CalcType GetAmplitude(void) { return CELLMODEL_PARAMVALUE(VT_stim_amplitude); }

  virtual inline ML_CalcType GetStimTime() { return CELLMODEL_PARAMVALUE(VT_stim_duration); }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                           ML_CalcType stretch = 1., int euler = 2);

  virtual void Print(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class LindbladEtAl
#endif // ifndef LINDBLADETAL
