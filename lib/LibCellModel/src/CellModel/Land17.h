// created by Robin Moss

// based on the available Matlab code from cemrg.co.uk

#ifndef LAND17
#define LAND17

#include <Land17Parameters.h>

#undef CELLMODEL_PARAMVALUE
#define CELLMODEL_PARAMVALUE(a) L17p->P[NS_Land17Parameters::a].value

class Land17 : public vbForceModel<ML_CalcType> {
public:
  Land17Parameters *L17p;
  ML_CalcType XS;
  ML_CalcType XW;
  ML_CalcType CaTRPN;
  ML_CalcType B;
  ML_CalcType ZETAS;
  ML_CalcType ZETAW;
  ML_CalcType Cd;
  ML_CalcType dLambda;
  ML_CalcType lambda_dt;
  ML_CalcType Ta;
  ML_CalcType Tp;

  Land17(Land17Parameters *);

  ~Land17() {}

  virtual inline ML_CalcType *GetBase(void) { return &XS; }

  virtual inline int GetSize(void) {
    return sizeof(Land17) - sizeof(vbForceModel<ML_CalcType>) - sizeof(Land17Parameters *);
  }

  virtual inline bool InIsTCa(void) { return false; }

  void Init();

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType &, int);

  virtual void Print(ostream &);

  virtual void GetParameterNames(vector<string> &);
};  // class Land17
#endif  // ifndef LAND17
