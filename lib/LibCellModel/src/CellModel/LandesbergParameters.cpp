/**@file LandesbergParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <LandesbergParameters.h>

LandesbergParameters::LandesbergParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void LandesbergParameters::PrintParameters() {
  // print the parameter to the stdout
  cout<<"LandesbergParameters:"<<endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void LandesbergParameters::Init(const char *initFile) {
  P[VT_P1a].name           = "P1";
  P[VT_P0a].name           = "P0";
  P[VT_N1a].name           = "N1";
  P[VT_p_k_on].name        = "p_k_on";
  P[VT_Kapp0].name         = "Kapp0";
  P[VT_Kapp1].name         = "Kapp1";
  P[VT_Kapp2].name         = "Kapp2";
  P[VT_Kapp3].name         = "Kapp3";
  P[VT_p_f].name           = "p_f";
  P[VT_p_g].name           = "p_g";
  P[VT_Fmax].name          = "Fmax";
  P[VT_dFmax].name         = "dFmax";
  P[VT_Fmax].readFromFile  = false;
  P[VT_dFmax].readFromFile = false;

  ParameterLoader FPL(initFile, FMT_Landesberg);
  this->setOverlapParameters(FPL.getOverlapString());
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = FPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
}

void LandesbergParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();

  double p_k_off = P[VT_p_k_on].value/
    (P[VT_Kapp0].value+P[VT_Kapp1].value*0.79+P[VT_Kapp2].value*0.79*0.79+P[VT_Kapp3].value*0.79*0.79*0.79);
  P[VT_Fmax].value = 0.79*P[VT_p_f].value*P[VT_p_k_on].value*100*(P[VT_p_g].value+P[VT_p_k_on].value*100+p_k_off)/
    (P[VT_p_f].value*P[VT_p_k_on].value*100*p_k_off+P[VT_p_g].value*
     (P[VT_p_f].value*P[VT_p_k_on].value*100+P[VT_p_g].value*(p_k_off+P[VT_p_k_on].value*100)+p_k_off*
      (P[VT_p_f].value+p_k_off+P[VT_p_k_on].value*100))+
     (P[VT_p_f].value*P[VT_p_k_on].value*100+P[VT_p_g].value*(p_k_off+P[VT_p_k_on].value*100))*P[VT_p_k_on].value*100);

  P[VT_dFmax].value = 1/(P[VT_Fmax].value);
}
