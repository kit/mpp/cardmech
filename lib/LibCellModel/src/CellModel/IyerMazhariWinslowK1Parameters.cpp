/**@file IyerMazhariWinslowK1Parameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowK1Parameters.h>

IyerMazhariWinslowK1Parameters::IyerMazhariWinslowK1Parameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void IyerMazhariWinslowK1Parameters::PrintParameters() {
  cout << "#IyerMazhariWinslowK1Parameter:\n";

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void IyerMazhariWinslowK1Parameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "IyerMazhariWinslowParameters:init " << initFile << endl;
#endif // if KADEBUG

  P[VT_Tx].name = "Tx";
  P[VT_Acap].name = "Acap";
  P[VT_Vmyo].name = "Vmyo";

  P[VT_Ko].name = "Ko";
  P[VT_Ki].name = "Ki";

  P[VT_GK1].name = "GK1";
  P[VT_P1K1].name = "P1K1";
  P[VT_P2K1].name = "P2K1";
  P[VT_Amp].name = "Amp";

  ParameterLoader EPL(initFile, EMT_IyerMazhariWinslowK1);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
}

void IyerMazhariWinslowK1Parameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
}
