/*      File: Fenton4StateParameters.cpp */

/*  Version 1.1 (2008-06-25) by fmw */

/*      Version 1.1 (2008-06-25)

 * Included 3 new ev-file parameters: Vol, Amp, and StimTime
 * LongPrint() now outputs J_fi, J_si and J_so
 */

#include <Fenton4StateParameters.h>

Fenton4StateParameters::Fenton4StateParameters(const char *initFile, ML_CalcType tinc) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

Fenton4StateParameters::~Fenton4StateParameters() {
  // Destruktor
}

void Fenton4StateParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "Loading the Fenton4State parameter from " << initFile << " ...\n";
#endif // if KADEBUG

  // Init values
  // constant values used in Calculate()

  // Initialization of the Parameters ...

  P[VT_tau_v1_minus].name   = "tau_v1_minus";
  P[VT_tau_v2_minus].name   = "tau_v2_minus";
  P[VT_tau_v_plus].name     = "tau_v_plus";
  P[VT_tau_w1_minus].name   = "tau_w1_minus";
  P[VT_tau_w2_minus].name   = "tau_w2_minus";
  P[VT_tau_w_plus].name     = "tau_w_plus";
  P[VT_tau_fi].name         = "tau_fi";
  P[VT_tau_o1].name         = "tau_o1";
  P[VT_tau_o2].name         = "tau_o2";
  P[VT_tau_so1].name        = "tau_so1";
  P[VT_tau_so2].name        = "tau_so2";
  P[VT_tau_s1].name         = "tau_s1";
  P[VT_tau_s2].name         = "tau_s2";
  P[VT_tau_si].name         = "tau_si";
  P[VT_tau_w_inf].name      = "tau_w_inf";
  P[VT_u_o].name            = "u_o";
  P[VT_u_u].name            = "u_u";
  P[VT_u_m].name            = "u_m";
  P[VT_u_p].name            = "u_p";
  P[VT_u_q].name            = "u_q";
  P[VT_u_r].name            = "u_r";
  P[VT_u_w_minus].name      = "u_w_minus";
  P[VT_u_so].name           = "u_so";
  P[VT_u_s].name            = "u_s";
  P[VT_k_w_minus].name      = "k_w_minus";
  P[VT_k_so].name           = "k_so";
  P[VT_k_s].name            = "k_s";
  P[VT_w_inf_asterisk].name = "w_inf_asterisk";
  P[VT_Cm].name             = "Cm";
  P[VT_Vol].name            = "Vol";
  P[VT_Amp].name            = "Amp";
  P[VT_StimTime].name       = "StimTime";
  P[VT_Vm_u_factor].name    = "Vm_u_factor";
  P[VT_Vm_init].name        = "Vm_init";
  P[VT_Init_u].name         = "Init_u";
  P[VT_Init_v].name         = "Init_v";
  P[VT_Init_w].name         = "Init_w";
  P[VT_Init_s].name         = "Init_s";

  P[VT_minus_tau_v_plus_inv_tinc].readFromFile = false;
  P[VT_minus_tau_w_plus_inv_tinc].readFromFile = false;
  P[VT_minus_tau_si_inv_tinc].readFromFile     = false;
  P[VT_Cm_Vm_u_factor_inv_tinc].readFromFile   = false;
  P[VT_tinc].readFromFile                      = false;

  //    P[VT_Vm_u_factor_div1000].readFromFile=false;

  ParameterLoader EPL(initFile, EMT_Fenton4State);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  // End Initialization of the Parameters ...

  P[VT_tinc].value = tinc;

  Calculate();
  InitTable(tinc);
} // Fenton4StateParameters::Init

void Fenton4StateParameters::Calculate() {
#if KADEBUG
  cerr << "Fenton4StateParameters - Calculate ..." << endl;
#endif // if KADEBUG

  ML_CalcType tinc =  P[VT_tinc].value * 1000;  // sec -> ms

  P[VT_minus_tau_v_plus_inv_tinc].value = (-1 / P[VT_tau_v_plus].value) * tinc;
  P[VT_minus_tau_w_plus_inv_tinc].value =  (-1 / P[VT_tau_w_plus].value) * tinc;
  P[VT_minus_tau_si_inv_tinc].value     =   (-1 / P[VT_tau_si].value) * tinc;

  P[VT_Cm_Vm_u_factor_inv_tinc].value = 1 / (P[VT_Cm].value * P[VT_Vm_u_factor].value) * tinc;

  //    P[VT_Vm_u_factor_div1000].value = P[VT_Vm_u_factor].value * 0.001;

  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();

  // PrintParameters();
}

void Fenton4StateParameters::InitTable(ML_CalcType tinc) {
  ML_CalcType u;

  tinc *= 1000;  // sec -> ms

  for (int ui = 0; ui < uTabArraySize; ui++) {
    u = ui * uTabStepSize + uTabMin;

    //          P[VT_].value

    tau_so_inv[ui] = 1 /
      (P[VT_tau_so1].value + (P[VT_tau_so2].value - P[VT_tau_so1].value) *
       (1 + tanh(P[VT_k_so].value * (u - P[VT_u_so].value) ) ) / 2);
    tau_w_minus_inv_tinc[ui] =
      (1 /
       (P[VT_tau_w1_minus].value + (P[VT_tau_w2_minus].value - P[VT_tau_w1_minus].value) *
        (1 + tanh(P[VT_k_w_minus].value * (u - P[VT_u_w_minus].value) ) ) / 2)  ) * tinc;
    tau_v_minus_inv_tinc[ui] =
      (1 / ( (u < P[VT_u_q].value) ? P[VT_tau_v1_minus].value : P[VT_tau_v2_minus].value)  ) * tinc;
    tau_s_inv_tinc[ui] = (1 / ( (u < P[VT_u_p].value) ? P[VT_tau_s1].value : P[VT_tau_s2].value)  ) * tinc;
    tau_o_inv[ui]      = 1 / ( (u < P[VT_u_r].value) ? P[VT_tau_o1].value : P[VT_tau_o2].value);

    v_inf[ui] = (u < P[VT_u_q].value) ? 1.0 : 0.0;
    w_inf[ui] = (u < P[VT_u_r].value) ? (1 - u / P[VT_tau_w_inf].value) : P[VT_w_inf_asterisk].value;

    s_expr_tanh[ui]    = (1 + tanh(P[VT_k_s].value * (u - P[VT_u_s].value) ) ) / 2;
    J_fi_expr_tinc[ui] =
      ( (u <
         P[VT_u_m].value) ? 0.0 : (-1.0) * (u - P[VT_u_m].value) * (P[VT_u_u].value - u) / P[VT_tau_fi].value) * tinc;
    J_so_tinc[ui] = ( (u < P[VT_u_p].value) ? u * tau_o_inv[ui] : tau_so_inv[ui]) * tinc;
  } // for ui
}

void Fenton4StateParameters::PrintParameters() {
  // print the parameter to the stdout

  cout<<"Fenton4StateParameters:"<<endl;
  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= "     << P[i].value <<endl;
  }
}
