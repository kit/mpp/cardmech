/**@file Landesberg.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef LANDESBERG_H
#define LANDESBERG_H

#include <LandesbergParameters.h>

#undef CELLMODEL_PARAMVALUE
#define CELLMODEL_PARAMVALUE(a) lp->P[NS_LandesbergParameters::a].value

class Landesberg : public vbForceModel<ML_CalcType> {
public:
  LandesbergParameters *lp;

  ML_CalcType P1, P0, N1;

  Landesberg(LandesbergParameters *);

  ~Landesberg() {}

  virtual inline int GetSize(void) {
    return sizeof(Landesberg) - sizeof(LandesbergParameters *) - sizeof(vbForceModel<ML_CalcType>);
  }

  virtual inline ML_CalcType *GetBase(void) { return &P1; }

  void Init();


  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType &, int);

  virtual inline ML_CalcType ForceEulerNEuler(int, ML_CalcType, ML_CalcType, ML_CalcType &);

  // virtual void steadyState(T Ca, T stretch) {

  /*     double Force=Overlap(stretch, lp->getOverlapID(), lp->getOverlapParameters());  */
  /*      double p_k_off=*lp->p_k_on/(*lp->Kapp0+*lp->Kapp1*Force*+*lp->Kapp2*Force*Force+*lp->Kapp3*Force*Force*Force);
      */

  /*      cout<<Ca<<' '<<(Overlap(stretch, lp->getOverlapID(),
     lp->getOverlapParameters())**lp->p_f**lp->p_k_on*Ca*(*lp->p_g+*lp->p_k_on*Ca+p_k_off)/(*lp->p_f**lp->p_k_on*Ca*p_k_off+*lp->p_g*(*lp->p_f**lp->p_k_on*Ca+*lp->p_g*(p_k_off+*lp->p_k_on*Ca)+p_k_off*(*lp->p_f+p_k_off+*lp->p_k_on*Ca))+(*lp->p_f**lp->p_k_on*Ca+*lp->p_g*(p_k_off+*lp->p_k_on*Ca))**lp->p_k_on*Ca))**lp->dFmax<<'
     '<<*lp->p_f<<' '<<*lp->p_g<<' '<<' '<<*lp->p_k_on*Ca<<' '<<p_k_off<<endl; */

  //   }
  virtual void Print(ostream &);

  virtual void GetParameterNames(vector<string> &);
}; // class Landesberg
#endif // ifndef LANDESBERG_H
