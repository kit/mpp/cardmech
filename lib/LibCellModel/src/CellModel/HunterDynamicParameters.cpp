/**@file HunterDynamicParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <HunterDynamicParameters.h>

HunterDynamicParameters::HunterDynamicParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void HunterDynamicParameters::PrintParameters() {
  // print the parameter to the stdout
  cout<<"HunterDynamicParameters:"<<endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void HunterDynamicParameters::Init(const char *initFile) {
  P[VT_TCaa].name    = "TCa";
  P[VT_Ca_bmax].name = "Ca_bmax";
  P[VT_f].name       = "f";
  P[VT_g].name       = "g";
  P[VT_gamma].name   = "gamma";
  P[VT_dgamma].name  = "dgamma";
  P[VT_pC50ref].name = "pC50ref";
  P[VT_z0].name      = "z0";
  P[VT_alpha].name   = "alpha";
  P[VT_beta0].name   = "beta0";
  P[VT_beta1].name   = "beta1";
  P[VT_beta2].name   = "beta2";
  P[VT_nref].name    = "nref";
  P[VT_Fref].name    = "Fref";

  /*  P[VT_Fmax].name = "Fmax"; */
  /*  P[VT_dFmax].name = "dFmax"; */

  P[VT_dgamma].readFromFile = false;

  /*  P[VT_dFmax].readFromFile = false */

  ParameterLoader FPL(initFile, FMT_HunterDynamic);
  this->setOverlapParameters(FPL.getOverlapString());
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = FPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
} // HunterDynamicParameters::Init

void HunterDynamicParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();

  P[VT_dgamma].value = 1.0 / P[VT_gamma].value;

  /*  P[VT_dFmax].readFromFile = false; */
}
