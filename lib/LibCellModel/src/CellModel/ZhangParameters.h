/*! \file ZhangParameters.h
   \brief Implementation of Zhang et al sinus node model
   AJP 2000, JCE 2002 (implementation partial), JCE 2003

   \author unknown, Universitaet Karlsruhe (TH)
 */
#ifndef ZHANGPARAMETERS_H
#define ZHANGPARAMETERS_H

// ZHANG03 defines if ACh modulation of currents is included
// #define ZHANG03

#include <ParameterLoader.h>

namespace NS_ZhangParameters {
  enum varType {
    VT_dCell = vtFirst,
    VT_R,
    VT_Tx,
    VT_F,
    VT_Nao,
    VT_Nai,
    VT_Ko,
    VT_Ki,
    VT_Cao,
    VT_ECaL,
    VT_ECaT,
    VT_Vcell,

    VT_CmCenter,
    VT_CmPeriphery,
    VT_gNaCenter,
    VT_gNaPeriphery,
    VT_gCaLCenter,
    VT_gCaLPeriphery,
    VT_gCaTCenter,
    VT_gCaTPeriphery,
    VT_gtoCenter,
    VT_gtoPeriphery,
    VT_gsusCenter,
    VT_gsusPeriphery,
    VT_gKrCenter,
    VT_gKrPeriphery,
    VT_tau_pi,
    VT_gKsCenter,
    VT_gKsPeriphery,
    VT_gfNaCenter,
    VT_gfNaPeriphery,
    VT_gfKCenter,
    VT_gfKPeriphery,
    VT_gbNaCenter,
    VT_gbNaPeriphery,
    VT_gbCaCenter,
    VT_gbCaPeriphery,
    VT_gbKCenter,
    VT_gbKPeriphery,
    VT_kNaCaCenter,
    VT_kNaCaPeriphery,
    VT_yNaCa,
    VT_dNaCa,
    VT_ipmaxCenter,
    VT_ipmaxPeriphery,
    VT_KmNa,
    VT_KmK,
    VT_init_h1,
    VT_init_h2,
    VT_init_m,
    VT_init_fL,
    VT_init_dL,
    VT_init_dT,
    VT_init_fT,
    VT_init_q,
    VT_init_r,
    VT_init_paf,
    VT_init_pas,
    VT_init_pi_m,
    VT_init_xs,
    VT_init_y,
    VT_init_Cai,
    VT_init_Vm,
    VT_paf_inf1,
    VT_paf_inf2,
    VT_tau_paf1,
    VT_tau_paf2,
    VT_tau_paf3,
    VT_tau_paf4,
    VT_tau_paf5,
    VT_KQ10_tau_paf,
    VT_tau_pas1,
    VT_tau_pas2,
    VT_tau_pas3,
    VT_tau_pas4,
    VT_tau_pas5,
    VT_pi_inf1,
    VT_pi_inf2,
    VT_KQ10_tau_pas,
    VT_C1FCell,
    VT_C2FCell,
    VT_C3FCell,
    VT_C4FCell,
    VT_C5FCell,

    VT_RTdF,
    VT_ENa,
    VT_EK,
    VT_EKs,
    VT_FCell,
    VT_Cm,
    VT_gNa,
    VT_gCaL,
    VT_gCaT,
    VT_gto,
    VT_gsus,
    VT_gKr,
    VT_gKs,
    VT_gfNa,
    VT_gfK,
    VT_gbNa,
    VT_gbCa,
    VT_gbK,
    VT_kNaCa,
    VT_ipmax,
    VT_Amp,
#ifdef ZHANG03
    VT_ACh,
    VT_bmax,
    VT_kCaACh,
    VT_smax,
    VT_nfACh,
    VT_kfACh,
    VT_gKACh,
    VT_nKACh,
    VT_kKACh,
    VT_j,
    VT_k,
#endif // ifdef ZHANG03
    vtLast
  };
} // namespace NS_ZhangParameters

using namespace NS_ZhangParameters;

class ZhangParameters : public vbNewElphyParameters {
public:
  ZhangParameters(const char *, ML_CalcType);

  ~ZhangParameters() {}

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType);

  void Init(const char *, ML_CalcType);

  ML_CalcType FNa[RTDT];
  ML_CalcType CCaL[RTDT];
  ML_CalcType Cip[RTDT];
  ML_CalcType m_inf[RTDT];
  ML_CalcType exptau_m[RTDT];
  ML_CalcType h1_inf[RTDT];
  ML_CalcType h2_inf[RTDT];
  ML_CalcType exptau_h1[RTDT];
  ML_CalcType exptau_h2[RTDT];
  ML_CalcType exptau_dL[RTDT];
  ML_CalcType dL_inf[RTDT];
  ML_CalcType exptau_fL[RTDT];
  ML_CalcType fL_inf[RTDT];
  ML_CalcType exptau_dT[RTDT];
  ML_CalcType dT_inf[RTDT];
  ML_CalcType exptau_fT[RTDT];
  ML_CalcType fT_inf[RTDT];
  ML_CalcType q_inf[RTDT];
  ML_CalcType exptau_q[RTDT];
  ML_CalcType r_inf[RTDT];
  ML_CalcType exptau_r[RTDT];
  ML_CalcType paf_inf[RTDT];
  ML_CalcType pas_inf[RTDT];
  ML_CalcType exptau_paf[RTDT];
  ML_CalcType exptau_pas[RTDT];
  ML_CalcType pi_inf[RTDT];
  ML_CalcType xs_inf[RTDT];
  ML_CalcType exptau_xs[RTDT];
  ML_CalcType y_inf[RTDT];
  ML_CalcType exptau_y[RTDT];

#ifdef ZHANG03
  ML_CalcType beta_j[RTDT];
  ML_CalcType beta_k[RTDT];
#endif // ifdef ZHANG03
}; // class ZhangParameters

#endif // ifndef ZHANGPARAMETERS_H
