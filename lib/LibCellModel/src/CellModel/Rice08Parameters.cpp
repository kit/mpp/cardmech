/**@file Rice08Parameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <Rice08Parameters.h>

Rice08Parameters::Rice08Parameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void Rice08Parameters::PrintParameters() {
  cout << "Rice08Parameters:" << endl;
  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void Rice08Parameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "Loading the Rice08 parameter from " << initFile << " ...\n";
#endif // if KADEBUG

  P[VT_SLmax].name = "SLmax";
  P[VT_SLmin].name = "SLmin";
  P[VT_len_thin].name = "len_thin";
  P[VT_len_thick].name = "len_thick";
  P[VT_len_hbare].name = "len_hbare";
  P[VT_Qkon].name = "Qkon";
  P[VT_Qkoff].name = "Qkoff";
  P[VT_Qkn_p].name = "Qkn_p";
  P[VT_Qkp_n].name = "Qkp_n";
  P[VT_kon].name = "kon";
  P[VT_koffL].name = "koffL";
  P[VT_koffH].name = "koffH";
  P[VT_perm50].name = "perm50";
  P[VT_nperm].name = "nperm";
  P[VT_kn_p].name = "kn_p";
  P[VT_kp_n].name = "kp_n";
  P[VT_koffmod].name = "koffmod";
  P[VT_TmpC].name = "TmpC";
  P[VT_fapp].name = "fapp";
  P[VT_gapp].name = "gapp";
  P[VT_hf].name = "hf";
  P[VT_hb].name = "hb";
  P[VT_gxb].name = "gxb";
  P[VT_gslmod].name = "gslmod";
  P[VT_hfmdc].name = "hfmdc";
  P[VT_hbmdc].name = "hbmdc";
  P[VT_sigmap].name = "sigmap";
  P[VT_sigman].name = "sigman";
  P[VT_xbmodsp].name = "xbmodsp";
  P[VT_Qfapp].name = "Qfapp";
  P[VT_Qgapp].name = "Qgapp";
  P[VT_Qhf].name = "Qhf";
  P[VT_Qhb].name = "Qhb";
  P[VT_Qgxb].name = "Qgxb";
  P[VT_x_0].name = "x_0";
  P[VT_xPsi].name = "xPsi";
  P[VT_kxb].name = "kxb";
  P[VT_SL_c].name = "SL_c";
  P[VT_SLrest].name = "SLrest";
  P[VT_PCon_t].name = "PCon_t";
  P[VT_PExp_t].name = "PExp_t";
  P[VT_PCon_c].name = "PCon_c";
  P[VT_PExp_c].name = "PExp_c";
  P[VT_massf].name = "massf";
  P[VT_visc].name = "visc";
  P[VT_KSE].name = "KSE";
  P[VT_SEon].name = "SEon";
  P[VT_kxb].name = "kxb";
  P[VT_Trop_conc].name = "Trop_conc";
  P[VT_tau1].name = "tau1";
  P[VT_tau2].name = "tau2";
  P[VT_start_time].name = "start_time";
  P[VT_Ca_amplitude].name = "Ca_amplitude";
  P[VT_Ca_diastolic].name = "Ca_diastolic";
  P[VT_konT].name = "konT";
  P[VT_koffLT].name = "koffLT";
  P[VT_koffHT].name = "koffHT";
  P[VT_fappT].name = "fappT";
  P[VT_SSXBprer].name = "SSXBprer";
  P[VT_beta].name = "beta";
  P[VT_SSXBpostr].name = "SSXBpostr";
  P[VT_Fnordv].name = "Fnordv";
  P[VT_SL_init].name = "SL_init";
  P[VT_TRPNCaL_init].name = "TRPNCaL_init";
  P[VT_TRPNCaH_init].name = "TRPNCaH_init";
  P[VT_xXBpostr_init].name = "xXBpostr_init";
  P[VT_xXBprer_init].name = "xXBprer_init";
  P[VT_XBpostr_init].name = "XBpostr_init";
  P[VT_XBprer_init].name = "XBprer_init";
  P[VT_N_NoXB_init].name = "N_NoXB_init";
  P[VT_P_NoXB_init].name = "P_NoXB_init";
  P[VT_N_init].name = "N_init";
  P[VT_intf_init].name = "intf_init";
  P[VT_Tension_init].name = "Tension_init";
  P[VT_Fmax].name = "Fmax";


  P[VT_konT].readFromFile = false;
  P[VT_koffLT].readFromFile = false;
  P[VT_koffHT].readFromFile = false;
  P[VT_fappT].readFromFile = false;
  P[VT_SSXBprer].readFromFile = false;
  P[VT_beta].readFromFile = false;
  P[VT_SSXBpostr].readFromFile = false;
  P[VT_Fnordv].readFromFile = false;

  ParameterLoader FPL(initFile, FMT_Rice08);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = FPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);
  Calculate();

#if KADEBUG
  cerr << "#Init() done ... \n";
#endif // if KADEBUG
} // Rice08Parameters::Init

void Rice08Parameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "#Rice08Parameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_konT].value =
      (P[VT_kon].value) * pow((P[VT_Qkon].value), ((P[VT_TmpC].value) - 37.0000) / 10.0000);
  P[VT_koffLT].value = (P[VT_koffL].value) * (P[VT_koffmod].value) *
                       pow((P[VT_Qkoff].value), ((P[VT_TmpC].value) - 37.0000) / 10.0000);
  P[VT_koffHT].value = (P[VT_koffH].value) * (P[VT_koffmod].value) *
                       pow((P[VT_Qkoff].value), ((P[VT_TmpC].value) - 37.0000) / 10.0000);
  P[VT_fappT].value = (P[VT_fapp].value) * (P[VT_xbmodsp].value) *
                      pow((P[VT_Qfapp].value), ((P[VT_TmpC].value) - 37.0000) / 10.0000);
  P[VT_SSXBprer].value =
      ((P[VT_hb].value) * (P[VT_fapp].value) + (P[VT_gxb].value) * (P[VT_fapp].value)) /
      ((P[VT_fapp].value) * (P[VT_hf].value) + (P[VT_gxb].value) * (P[VT_hf].value) +
       (P[VT_gxb].value) * (P[VT_gapp].value) +
       (P[VT_hb].value) * (P[VT_fapp].value) + (P[VT_hb].value) * (P[VT_gapp].value) +
       (P[VT_gxb].value) * (P[VT_fapp].value));
  P[VT_beta].value =
      pow((P[VT_tau1].value) / (P[VT_tau2].value),
          -1.00000 / ((P[VT_tau1].value) / (P[VT_tau2].value) - 1.00000)) -
      pow((P[VT_tau1].value) / (P[VT_tau2].value),
          -1.00000 / (1.00000 - (P[VT_tau2].value) / (P[VT_tau1].value)));
  P[VT_SSXBpostr].value = ((P[VT_fapp].value) * (P[VT_hf].value)) /
                          ((P[VT_fapp].value) * (P[VT_hf].value) +
                           (P[VT_gxb].value) * (P[VT_hf].value) +
                           (P[VT_gxb].value) * (P[VT_gapp].value) +
                           (P[VT_hb].value) * (P[VT_fapp].value) +
                           (P[VT_hb].value) * (P[VT_gapp].value) +
                           (P[VT_gxb].value) * (P[VT_fapp].value));
  P[VT_Fnordv].value = (P[VT_kxb].value) * (P[VT_x_0].value) * (P[VT_SSXBpostr].value);
}
