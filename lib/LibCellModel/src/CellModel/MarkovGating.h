/*! \file MarkovGating.h
   \brief Implementation of Markov model with gating current calculation
   (based on Hille, Ion Channels of Excitable Membranes)

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef MARKOVGATING
#define MARKOVGATING

#include <MarkovGatingParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_MarkovGatingParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pIMW->P[NS_MarkovGatingParameters::a].value
#endif // ifdef HETERO

class MarkovGating : public vbElphyModel<ML_CalcType> {
public:
  double A, B, C;

  MarkovGatingParameters *pIMW;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  MarkovGating(MarkovGatingParameters *pIMWArg);

  inline ~MarkovGating() {}

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual void Init();

  virtual inline ML_CalcType Volume() { return 0; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.; }

  virtual inline ML_CalcType GetVm() { return 0; }

  virtual inline ML_CalcType GetCai() { return 0; }

  virtual inline ML_CalcType GetCao() { return 0; }

  virtual inline ML_CalcType GetNai() { return 0; }

  virtual inline ML_CalcType GetNao() { return 0; }

  virtual inline ML_CalcType GetKi() { return 0; }

  virtual inline ML_CalcType GetKo() { return 0; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return (ML_CalcType *) &A; }

  virtual inline void SetCai(ML_CalcType val) {}

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);

  virtual ML_CalcType
  Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim, ML_CalcType stretch, int euler);

  virtual int GetNumStatus() { return 1; }

  virtual void GetStatus(double *p) const;

  virtual void SetStatus(const double *p);
}; // class MarkovGating
#endif // ifndef MARKOVGATING
