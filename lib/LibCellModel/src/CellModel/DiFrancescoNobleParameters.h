/*      File: DiFrancescoNobleParameters.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#ifndef DIFRANCESCONOBLEPARAMETERS_H
#define DIFRANCESCONOBLEPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_DiFrancescoNobleParameters {
  enum varType {
    VT_R = vtFirst,
    VT_Tx,
    VT_F,
    VT_RTdF,
    VT_FdRT,
    VT_F2dRT,
    VT_RTd2F,
    VT_C_m,
    VT_dC_m,
    VT_k_mK,
    VT_k_mNa,
    VT_k_mf2,
    VT_k_NaCa,
    VT_d_NaCa,
    VT_K_mf,
    VT_K_m1,
    VT_K_mto,
    VT_K_act4,
    VT_K_b,
    VT_k_mCa,
    VT_Ca_upmax,
    VT_I_NaKmax,
    VT_I_Kmax,
    VT_I_Kmaxd,
    VT_n_NaCa,
    VT_n_NaCam2,
    VT_ndn_NaCa,
    VT_tdn,
    VT_P_std,
    VT_g_fK,
    VT_g_fNa,
    VT_g_k1,
    VT_g_bNa,
    VT_g_bCa,
    VT_g_bK,
    VT_g_Na,
    VT_g_to,
    VT_t_up,
    VT_t_rep,
    VT_t_rel,
    VT_b_up,
    VT_Na_o,
    VT_NNNO,
    VT_Ca_o,
    VT_P_si,
    VT_gamm,
    VT_V_ecs,
    VT_radius,
    VT_lenght,
    VT_Vol,
    VT_Vcell,
    VT_V_e,
    VT_dVeF,
    VT_V_i,
    VT_dViF,
    VT_dF2Vi,
    VT_F2VidCa_up,
    VT_V_up,
    VT_dF2Vup,
    VT_V_rel,
    VT_dF2Vrel,
    VT_F2Vreldtrep,
    VT_F2Vreldtrel,
    VT_Vr,
    VT_SL0,
    VT_SL,
    VT_G_Stretch,
    VT_roh,
    VT_alfa,
    VT_K,
    VT_A,
    VT_stretchfactor,
    VT_y,
    VT_x,
    VT_h,
    VT_f,
    VT_f2,
    VT_r,
    VT_p,
    VT_K_o,
    VT_Ca_i,
    VT_Ca_up,
    VT_Ca_rel,
    VT_m,
    VT_d,
    VT_Na_i,
    VT_K_i,
    VT_Vm,
    VT_Amp,
    vtLast
  };
} // namespace NS_DiFrancescoNobleParameters

using namespace NS_DiFrancescoNobleParameters;

class DiFrancescoNobleParameters : public vbNewElphyParameters {
public:
  DiFrancescoNobleParameters(const char *, ML_CalcType tinc);

  ~DiFrancescoNobleParameters();

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType tinc);

  void Init(const char *, ML_CalcType tinc);

  ML_CalcType exp50;
  ML_CalcType exp50_2;

  ML_CalcType y_inf[RTDT];
  ML_CalcType exptau_y[RTDT];
  ML_CalcType x_inf[RTDT];
  ML_CalcType exptau_x[RTDT];
  ML_CalcType m_inf[RTDT];
  ML_CalcType exptau_m[RTDT];
  ML_CalcType h_inf[RTDT];
  ML_CalcType exptau_h[RTDT];
  ML_CalcType d_inf[RTDT];
  ML_CalcType exptau_d[RTDT];
  ML_CalcType f_inf[RTDT];
  ML_CalcType exptau_f[RTDT];

  // ML_CalcType p_inf[RTDT]; // == f_inf
  ML_CalcType exptau_p[RTDT];
  ML_CalcType r_inf[RTDT];
  ML_CalcType exptau_r[RTDT];
  ML_CalcType dexpV50[RTDT];
  ML_CalcType expmV[RTDT];
  ML_CalcType VdV[RTDT];
  ML_CalcType VdV2[RTDT];
  ML_CalcType Vp10dexp[RTDT];
  ML_CalcType expm4V[RTDT];
  ML_CalcType expstoch[RTDT];
  ML_CalcType expmstoch[RTDT];
  ML_CalcType Nadexp[RTDT];
  ML_CalcType Cadexp[RTDT];
}; // class DiFrancescoNobleParameters
#endif // ifndef DIFRANCESCONOBLEPARAMETERS_H
