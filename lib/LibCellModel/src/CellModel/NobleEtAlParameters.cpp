/**@file NobleEtAlParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <NobleEtAlParameters.h>

NobleEtAlParameters::NobleEtAlParameters(const char *initFile, ElphyModelType emt,
                                         ML_CalcType tinc) {
  P = new Parameter[vtLast];
  Init(initFile, emt, tinc);
}

void NobleEtAlParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "NobleEtAlParameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void NobleEtAlParameters::Init(const char *initFile, ElphyModelType emt, ML_CalcType tinc) {
#if KADEBUG
  cerr << "NobleEtAlParameters::Init " << initFile << endl;
#endif // if KADEBUG

  // Initialization of parameter names
  P[VT_R].name = "R";
  P[VT_Tx].name = "T";
  P[VT_F].name = "F";
  P[VT_RTdF].name = "RTdF";
  P[VT_FdRT].name = "FdRT";
  P[VT_RTd2F].name = "RTd2F";
  P[VT_FdRT1k25].name = "FdRT1k25";
  P[VT_F2dRT].name = "F2dRT";
  P[VT_C_m].name = "C_m";
  P[VT_dC_m].name = "dC_m";
  P[VT_k_cachoff].name = "k_cachoff";
  P[VT_k_mk1].name = "k_mk1";
  P[VT_k_mk].name = "k_mk";
  P[VT_k_mNa].name = "k_mNa";
  P[VT_k_kNa].name = "k_kNa";
  P[VT_k_dsoff].name = "k_dsoff";
  P[VT_k_mCaDS].name = "k_mCaDS";
  P[VT_k_decay].name = "k_decay";
  P[VT_R_decay].name = "R_decay";
  P[VT_G_to].name = "G_to";
  P[VT_G_pNa].name = "G_pNa";
  P[VT_k_mCaCyt].name = "k_mCaCyt";
  P[VT_G_bNa].name = "G_bNa";
  P[VT_G_Kr1].name = "G_Kr1";
  P[VT_G_Kr2].name = "G_Kr2";
  P[VT_K_Kr].name = "K_Kr";
  P[VT_G_Ks].name = "G_Ks";
  P[VT_I_NaKmax].name = "I_NaKmax";
  P[VT_G_Na].name = "G_Na";
  P[VT_G_k1].name = "G_k1";
  P[VT_G_bCa].name = "G_bCa";
  P[VT_k_NaCa].name = "k_NaCa";
  P[VT_d_NaCa].name = "d_NaCa";
  P[VT_Ca_o].name = "Ca_o";
  P[VT_Na_o].name = "Na_o";
  P[VT_NNNO].name = "NNNO";
  P[VT_Na_ok3].name = "Na_ok3";
  P[VT_Fractl_CaL].name = "Fractl_CaL";
  P[VT_Fractl_NaCA].name = "Fractl_NaCA";
  P[VT_K_b].name = "K_b";
  P[VT_diffCa].name = "diffCa";
  P[VT_g].name = "g";
  P[VT_n_NaCa].name = "n_NaCa";
  P[VT_k_cyCa].name = "k_cyCa";
  P[VT_k_xcs].name = "k_xcs";
  P[VT_k_srCa].name = "k_srCa";
  P[VT_radius].name = "radius";
  P[VT_length].name = "length";
  P[VT_Vol].name = "Vol";
  P[VT_V_cell].name = "V_cell";
  P[VT_V_ecs].name = "V_ecs";
  P[VT_V_i].name = "V_i";
  P[VT_V_up].name = "V_up";
  P[VT_V_rel].name = "V_rel";
  P[VT_V_SRup].name = "V_SRup";
  P[VT_V_SRrel].name = "V_SRrel";
  P[VT_V_ds].name = "V_ds";
  P[VT_k_1dV_iF].name = "k_1dV_iF";
  P[VT_k_1dV_i2F].name = "k_1dV_i2F";
  P[VT_k_1dV_i2zF].name = "k_1dV_i2zF";
  P[VT_k_VupdVrel].name = "k_VupdVrel";
  P[VT_k_Vspez].name = "k_Vspez";
  P[VT_k_V_idV_SRup].name = "k_V_idV_SRup";
  P[VT_k_kckxdk_s].name = "k_kckxdk_s";
  P[VT_k_kckx].name = "k_kckx";
  P[VT_a_troponin].name = "a_troponin";
  P[VT_b_troponin].name = "b_troponin";
  P[VT_Troponin].name = "Troponin";
  P[VT_Calmod].name = "Calmod";
  P[VT_k_cont1].name = "k_cont1";
  P[VT_k_cont2].name = "k_cont2";
  P[VT_k_cont3].name = "k_cont3";
  P[VT_k_cont4].name = "k_cont4";
  P[VT_KCalTrop].name = "KCalTrop";
  P[VT_CBden].name = "CBden";
  P[VT_Trop_IT].name = "Trop_IT";
  P[VT_Trop_SL].name = "Trop_SL";
  P[VT_SR_SL].name = "SR_SL";
  P[VT_SR_IT].name = "SR_IT";
  P[VT_SACSL].name = "SACSL";
  P[VT_SACSL2].name = "SACSL2";
  P[VT_SACIT].name = "SACIT";
  P[VT_SL].name = "SL";
  P[VT_SLHST].name = "SLHST";
  P[VT_ITHST].name = "ITHST";
  P[VT_G_Castretch].name = "G_Castretch";
  P[VT_G_Nastretch].name = "G_Nastretch";
  P[VT_G_Kstretch].name = "G_Kstretch";
  P[VT_G_NSstretch].name = "G_NSstretch";
  P[VT_G_ANstretch].name = "G_ANstretch";
  P[VT_E_ANstretch].name = "E_ANstretch";
  P[VT_E_ANstretch1k5].name = "E_ANstretch1k5";
  P[VT_E_NSstretch].name = "E_NSstretch";
  P[VT_G_fibro].name = "G_fibro"; // fibro is not used
  P[VT_C_fibro].name = "C_fibro"; // fibro is not used
  P[VT_G_fibro_stretch].name = "G_fibro_stretch"; // fibro is not used
  P[VT_C_fibro_stretch].name = "C_fibro_stretch"; // fibro is not used
  P[VT_CellE].name = "CellE"; // fibro is not used
  P[VT_init_Basis_f].name = "init_Basis_f";
  P[VT_init_Basis_f2].name = "init_Basis_f2";
  P[VT_init_Basis_f2ds].name = "init_Basis_f2ds";
  P[VT_init_Basis_x_r1].name = "init_Basis_x_r1";
  P[VT_init_Basis_x_r2].name = "init_Basis_x_r2";
  P[VT_init_Basis_x_s].name = "init_Basis_x_s";
  P[VT_init_Basis_s].name = "init_Basis_s";
  P[VT_init_Basis_f_activator].name = "init_Basis_f_activator";
  P[VT_init_Basis_f_product].name = "init_Basis_f_product";
  P[VT_init_Basis_K_o].name = "init_Basis_K_o";
  P[VT_init_Basis_Ca_i].name = "init_Basis_Ca_i";
  P[VT_init_Basis_Ca_calmod].name = "init_Basis_Ca_calmod";
  P[VT_init_Basis_Ca_troponin].name = "init_Basis_Ca_troponin";
  P[VT_init_Basis_Ca_ds].name = "init_Basis_Ca_ds";
  P[VT_init_Basis_Ca_up].name = "init_Basis_Ca_up";
  P[VT_init_Basis_Ca_rel].name = "init_Basis_Ca_rel";
  P[VT_init_m].name = "init_m";
  P[VT_init_h].name = "init_h";
  P[VT_init_d].name = "init_d";
  P[VT_init_r].name = "init_r";
  P[VT_init_Na_i].name = "init_Na_i";
  P[VT_init_K_i].name = "init_K_i";
  P[VT_init_Stretch_Complex_f_LightChain].name = "init_Stretch_Complex_f_LightChain";
  P[VT_init_Stretch_Complex_f_CrossBridge].name = "init_Stretch_Complex_f_CrossBridge";
  P[VT_init_Vm].name = "init_Vm";
  P[VT_Amp].name = "Amp";

  P[VT_RTdF].readFromFile = false;
  P[VT_FdRT].readFromFile = false;
  P[VT_RTd2F].readFromFile = false;
  P[VT_FdRT1k25].readFromFile = false;
  P[VT_F2dRT].readFromFile = false;
  P[VT_dC_m].readFromFile = false;
  P[VT_K_Kr].readFromFile = false;
  P[VT_NNNO].readFromFile = false;
  P[VT_Na_ok3].readFromFile = false;
  P[VT_Vol].readFromFile = false;
  P[VT_V_cell].readFromFile = false;
  P[VT_V_ecs].readFromFile = false;
  P[VT_V_i].readFromFile = false;
  P[VT_V_up].readFromFile = false;
  P[VT_V_rel].readFromFile = false;
  P[VT_V_SRup].readFromFile = false;
  P[VT_V_SRrel].readFromFile = false;
  P[VT_V_ds].readFromFile = false;
  P[VT_k_1dV_iF].readFromFile = false;
  P[VT_k_1dV_i2F].readFromFile = false;
  P[VT_k_1dV_i2zF].readFromFile = false;
  P[VT_k_VupdVrel].readFromFile = false;
  P[VT_k_Vspez].readFromFile = false;
  P[VT_k_V_idV_SRup].readFromFile = false;
  P[VT_k_kckxdk_s].readFromFile = false;
  P[VT_k_kckx].readFromFile = false;
  P[VT_KCalTrop].readFromFile = false;
  P[VT_SACSL2].readFromFile = false;
  P[VT_E_ANstretch1k5].readFromFile = false;

  switch (emt) {
    case EMT_NobleSingleCell:
    case EMT_NobleStretch:
    case EMT_NobleStretchComplex:
    case EMT_NobleStretchSimple:
    case EMT_NobleCaSingleCell:
    case EMT_NobleCaStretch:
    case EMT_NobleCaStretchComplex:
    case EMT_NobleCaStretchSimple:
      break;
    default:
      throw kaBaseException("NobleEtAlParameters::Wrong ElphyModelType!");
      break;
  }

  ParameterLoader EPL(initFile, emt);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
  InitTable(tinc);
} // NobleEtAlParameters::Init

void NobleEtAlParameters::Calculate() {
#if KADEBUG
  cerr << "NobleEtAlParameters::Calculate\n";
#endif // if KADEBUG
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
  P[VT_RTdF].value = P[VT_R].value * (P[VT_Tx].value) / (P[VT_F].value);
  P[VT_FdRT].value = 1.0 / (P[VT_RTdF].value);
  P[VT_RTd2F].value = P[VT_RTdF].value * .5;
  P[VT_FdRT1k25].value = 1.25 * (P[VT_FdRT].value);
  P[VT_F2dRT].value = 1.0 / (P[VT_RTd2F].value);
  P[VT_dC_m].value = .001 / (P[VT_C_m].value);
  P[VT_K_Kr].value = P[VT_G_Kr2].value / (P[VT_G_Kr1].value);
  P[VT_NNNO].value = P[VT_Na_o].value * (P[VT_Na_o].value) * (P[VT_Na_o].value);
  P[VT_Na_ok3].value = .03 * (P[VT_Na_o].value);
  P[VT_Vol].value = M_PI * (P[VT_radius].value) * (P[VT_radius].value) * (P[VT_length].value);
  P[VT_V_cell].value = P[VT_Vol].value * 1e9;
  P[VT_V_ecs].value = .4 * (P[VT_V_cell].value);
  P[VT_V_i].value = .49 * (P[VT_V_cell].value);
  P[VT_V_up].value = P[VT_V_cell].value * .01;
  P[VT_V_rel].value = P[VT_V_cell].value * .1;
  P[VT_V_SRup].value = P[VT_V_cell].value * .01;
  P[VT_V_SRrel].value = P[VT_V_cell].value * .1;
  P[VT_V_ds].value = .1 * (P[VT_V_i].value);
  P[VT_k_1dV_iF].value = 1.0 / (P[VT_V_i].value * (P[VT_F].value));
  P[VT_k_1dV_i2F].value = P[VT_k_1dV_iF].value * .5;
  P[VT_k_1dV_i2zF].value = P[VT_k_1dV_iF].value * 5.0;
  P[VT_k_VupdVrel].value = P[VT_V_up].value / (P[VT_V_rel].value);
  P[VT_k_Vspez].value = P[VT_V_SRrel].value / (P[VT_V_i].value);
  P[VT_k_V_idV_SRup].value = P[VT_V_i].value / (P[VT_V_SRup].value);
  P[VT_k_kckxdk_s].value = P[VT_k_cyCa].value * (P[VT_k_xcs].value) / (P[VT_k_srCa].value);
  P[VT_k_kckx].value = P[VT_k_cyCa].value * (P[VT_k_xcs].value);
  P[VT_KCalTrop].value =
      P[VT_k_cont1].value / (P[VT_Calmod].value * (P[VT_Calmod].value) * (P[VT_Troponin].value));
  P[VT_SACSL2].value = 2.0 * (P[VT_SACSL].value);
  P[VT_E_ANstretch1k5].value = P[VT_E_ANstretch].value * 1.5;
} // NobleEtAlParameters::Calculate

void NobleEtAlParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr<<"NobleEtAlParameters:InitTable\n";
#endif // if KADEBUG
  tinc *= -1.0; // negation for exptau calculation
  double a, b;
  exp50 = exp(50.0 * (P[VT_FdRT].value));
  exp50_2 = exp(50.0 * (P[VT_F2dRT].value));
  for (double V = -RangeTabhalf + .0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int) (DivisionTab * (RangeTabhalf + V) + .5);
    a = (fabs(V + 41.0) < .00001 ? 2000.0 : 200.0 * (V + 41.0) / (1.0 - exp(-(V + 41.0) * .1)));
    b = a + 8000.0 * exp(-.056 * (V + 66.0));
    m_inf[Vi] = a / b;
    exptau_m[Vi] = exp(tinc * b);
    a = 20.0 * exp(-.125 * (V + 75.0));
    b = a + 2000.0 / (1.0 + 320.0 * exp(-.1 * (V + 75.0)));
    h_inf[Vi] = a / b;
    exptau_h[Vi] = exp(tinc * b);

    if (fabs(V + 19.0) < .0001) {
      a = 360.0;
      b = a + 360.0;
      d_inf[Vi] = a / b;
      exptau_d[Vi] = exp(tinc * b);
    } else {
      a = 90.0 * (V + 19.0) / (1.0 - exp(-(V + 19.0) * .25));
      b = a + 36.0 * (V + 19.0) / (exp((V + 19.0) * .1) - 1.0);
      d_inf[Vi] = a / b;
      exptau_d[Vi] = exp(tinc * b);
    }

    a = (fabs(V + 34.0) < .0001 ? 7.5 : 1.875 * (V + 34.0) / (exp((V + 34.0) * .25) - 1.0));
    b = a + 3.6 / (1.0 + exp(-(V + 34.0) * .25));
    f_inf[Vi] = a / b;
    exptau_f[Vi] = exp(tinc * b);

    a = 50.0 / (1.0 + exp(-(V - 5.0) * .11111111));
    b = a + .05 * exp(-(V - 20.0) * .066666667);
    x_r1_inf[Vi] = a / b;
    exptau_x_r1[Vi] = exp(tinc * b);
    double temp = (V + 30.0) * .033333333;
    b = a + .4 * exp(-temp * temp * temp);
    x_r2_inf[Vi] = a / b;
    exptau_x_r2[Vi] = exp(tinc * b);

    a = 14.0 / (1 + exp(-(V - 40.0) * .11111111));
    b = a + exp(-(V * .022222222));
    x_s_inf[Vi] = a / b;
    exptau_x_s[Vi] = exp(tinc * b);

    a = .033 * exp(-V * .058823529);
    b = a + 33.0 / (1.0 + exp(-.125 * (V + 10.0)));
    s_inf[Vi] = a / b;
    exptau_s[Vi] = exp(tinc * b);

    array_r[Vi] = 1.0 / (1.0 + exp(-(V + 4.0) * .2));

    KI_Kr[Vi] = (P[VT_G_Kr1].value) / (1.0 + exp((V + 9.0) * .044642857));
    KIpNa[Vi] = (P[VT_G_pNa].value) / (1.0 + exp(-(V + 52.0) * .125));
    KINaCaxg[Vi] =
        (P[VT_k_NaCa].value) * exp((P[VT_g].value) * V * (P[VT_FdRT].value)) * .0000068999998 *
        (P[VT_Ca_o].value);
    KINaCaxg2[Vi] = exp(-V * (P[VT_FdRT].value)) * (P[VT_NNNO].value) / (P[VT_Ca_o].value);
    KCaLCahinten[Vi] = (P[VT_Ca_o].value) * exp(-(V - 50.0) * (P[VT_F2dRT].value)) / exp50_2;
    KCaLKhinten[Vi] = exp(-(V - 50.0) * (P[VT_FdRT].value)) / exp50;
    KCaLNahinten[Vi] = (P[VT_Na_o].value) * exp(-(V - 50.0) * (P[VT_FdRT].value)) / exp50;
    KCaLCa[Vi] =
        .4 * (V - 50.0) * (P[VT_FdRT].value) / (1.0 - exp(-(V - 50.0) * (P[VT_F2dRT].value))) *
        exp50_2;
    KCaLx[Vi] =
        .002 * exp50 * .1 * (V - 50.0) * (P[VT_FdRT].value) / (1.0 - KCaLKhinten[Vi] * exp50);
  }
} // NobleEtAlParameters::InitTable
