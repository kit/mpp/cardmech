/**@file IyerMazhariWinslowKrParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowKrParameters.h>

IyerMazhariWinslowKrParameters::IyerMazhariWinslowKrParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void IyerMazhariWinslowKrParameters::PrintParameters() {
  cout << "#IyerMazhariWinslowKrParameter:\n";

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void IyerMazhariWinslowKrParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "IyerMazhariWinslowKrParameters:init " << initFile << endl;
#endif // if KADEBUG

  P[VT_Tx].name = "Tx";
  P[VT_Acap].name = "Acap";
  P[VT_Vmyo].name = "Vmyo";

  P[VT_Ko].name = "Ko";
  P[VT_Ki].name = "Ki";

  P[VT_C1Kr].name = "C1Kr";
  P[VT_C2Kr].name = "C2Kr";
  P[VT_C3Kr].name = "C3Kr";
  P[VT_OKr].name = "OKr";
  P[VT_IKr].name = "IKr";

  P[VT_GKr].name = "GKr";

  P[VT_Kra0a].name = "Kra0a";
  P[VT_Kra0b].name = "Kra0b";
  P[VT_Krb0a].name = "Krb0a";
  P[VT_Krb0b].name = "Krb0b";
  P[VT_Kra1a].name = "Kra1a";
  P[VT_Kra1b].name = "Kra1b";
  P[VT_Krb1a].name = "Krb1a";
  P[VT_Krb1b].name = "Krb1b";
  P[VT_Kraia].name = "Kraia";
  P[VT_Kraib].name = "Kraib";
  P[VT_Krbia].name = "Krbia";
  P[VT_Krbib].name = "Krbib";
  P[VT_Krai3a].name = "Krai3a";
  P[VT_Krai3b].name = "Krai3b";
  P[VT_Krkf].name = "Krkf";
  P[VT_Krkb].name = "Krkb";
  P[VT_Amp].name = "Amp";

  ParameterLoader EPL(initFile, EMT_IyerMazhariWinslowKr);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
} // IyerMazhariWinslowKrParameters::Init

void IyerMazhariWinslowKrParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
}
