/**@file IyerMazhariWinslowKrParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef IYER_MAZHARI_WINSLOW_KR_PARAMETERS
#define IYER_MAZHARI_WINSLOW_KR_PARAMETERS

#include <ParameterLoader.h>

namespace NS_IyerMazhariWinslowKrParameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_Acap,
    VT_Vmyo,

    VT_Ko,
    VT_Ki,

    VT_C1Kr,
    VT_C2Kr,
    VT_C3Kr,
    VT_OKr,
    VT_IKr,

    VT_GKr,

    VT_Kra0a,
    VT_Kra0b,
    VT_Krb0a,
    VT_Krb0b,
    VT_Kra1a,
    VT_Kra1b,
    VT_Krb1a,
    VT_Krb1b,
    VT_Kraia,
    VT_Kraib,
    VT_Krbia,
    VT_Krbib,
    VT_Krai3a,
    VT_Krai3b,
    VT_Krkf,
    VT_Krkb,
    VT_Amp,
    vtLast
  };
} // namespace NS_IyerMazhariWinslowKrParameters

using namespace NS_IyerMazhariWinslowKrParameters;

class IyerMazhariWinslowKrParameters : public vbNewElphyParameters {
public:
  IyerMazhariWinslowKrParameters(const char *);

  ~IyerMazhariWinslowKrParameters() {}

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&Krkb-&Tx+1)*sizeof(T);};
  // virtual inline T* GetBase(void){return Tx;};
  // virtual int GetNumParameters() { return 27; };
  void Init(const char *);

  void InitTable() {}

  void Calculate();
};

#endif // ifndef IYER_MAZHARI_WINSLOW_KR_PARAMETERS
