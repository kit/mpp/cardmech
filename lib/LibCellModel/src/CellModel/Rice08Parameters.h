/**@file Rice08Parameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef RICE08PARAMETERS_H
#define RICE08PARAMETERS_H

#include <ParameterLoader.h>

namespace NS_Rice08Parameters {
  enum varType {
    VT_SLmax = vtFirst,
    VT_SLmin,
    VT_len_thin,
    VT_len_thick,
    VT_len_hbare,
    VT_Qkon,
    VT_Qkoff,
    VT_Qkn_p,
    VT_Qkp_n,
    VT_kon,
    VT_koffL,
    VT_koffH,
    VT_perm50,
    VT_nperm,
    VT_kn_p,
    VT_kp_n,
    VT_koffmod,
    VT_TmpC,
    VT_fapp,
    VT_gapp,
    VT_hf,
    VT_hb,
    VT_gxb,
    VT_gslmod,
    VT_hfmdc,
    VT_hbmdc,
    VT_sigmap,
    VT_sigman,
    VT_xbmodsp,
    VT_Qfapp,
    VT_Qgapp,
    VT_Qhf,
    VT_Qhb,
    VT_Qgxb,
    VT_x_0,
    VT_xPsi,
    VT_kxb,
    VT_SL_c,
    VT_SLrest,
    VT_PCon_t,
    VT_PExp_t,
    VT_PCon_c,
    VT_PExp_c,
    VT_massf,
    VT_visc,
    VT_KSE,
    VT_SEon,
    VT_Trop_conc,
    VT_tau1,
    VT_tau2,
    VT_start_time,
    VT_Ca_amplitude,
    VT_Ca_diastolic,
    VT_konT,
    VT_koffLT,
    VT_koffHT,
    VT_fappT,
    VT_SSXBprer,
    VT_beta,
    VT_SSXBpostr,
    VT_Fnordv,
    VT_SL_init,
    VT_TRPNCaL_init,
    VT_TRPNCaH_init,
    VT_xXBpostr_init,
    VT_xXBprer_init,
    VT_XBpostr_init,
    VT_XBprer_init,
    VT_N_NoXB_init,
    VT_P_NoXB_init,
    VT_N_init,
    VT_intf_init,
    VT_Tension_init,
    VT_Fmax,
    vtLast
  };
} // namespace NS_Rice08Parameters

using namespace NS_Rice08Parameters;

class Rice08Parameters : public vbNewForceParameters {
public:
  Rice08Parameters(const char *);

  ~Rice08Parameters() {}

  void Init(const char *);

  void Calculate();

  void PrintParameters();
};

#endif // ifndef RICE08PARAMETERS_H
