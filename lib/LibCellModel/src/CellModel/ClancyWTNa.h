/*! \file ClancyWTNa.h
   \brief Markov model of cardiac sodium channel
   Clancy and Rudy
   Circulation. 2002 Mar 12;105(10):1208-13.

   \author fs, CVRTI - University of Utah, USA
 */


#ifndef CLANCYWTNA_H
#define CLANCYWTNA_H

#include <ElphyModelBasis.h>
#include <ClancyWTNaParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_ClancyWTNaParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pC->P[NS_ClancyWTNaParameters::a].value
#endif // ifdef HETERO


class ClancyWTNa : public vbElphyModel<ML_CalcType> {
public:
  ClancyWTNaParameters *pC;
  ML_CalcType IC3, IC2, IF, IM1, IM2;
  ML_CalcType C1, C2, C3, O;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  ClancyWTNa(ClancyWTNaParameters *);

  ~ClancyWTNa() {}

  virtual inline int GetSize(void) {
    return sizeof(ClancyWTNa) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(ClancyWTNaParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType *GetBase(void) { return &IC3; }

  virtual inline ML_CalcType Volume() { return 0.; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return -.080; }

  virtual inline ML_CalcType GetCai() { return 0.0; }

  virtual inline ML_CalcType GetCao() { return 0.0; }

  virtual inline ML_CalcType GetNai() { return CELLMODEL_PARAMVALUE(VT_Nai); }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Nao); }

  virtual inline ML_CalcType GetKi() { return 0.0; }

  virtual inline ML_CalcType GetKo() { return 0.0; }

  virtual inline void SetCai(ML_CalcType val) {}

  virtual void Init();

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char) (adVm < .15e-6 ? 3 : (adVm < .3e-6 ? 2 : 1));
  }

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);
}; // class ClancyWTNa


#endif // ifndef CLANCYWTNA_H
