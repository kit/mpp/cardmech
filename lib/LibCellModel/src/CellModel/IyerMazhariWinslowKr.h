/*! \file IyerMazhariWinslowKr.h
   \brief Implementation of I_Kr of electrophysiological model for describing human ventricular myocytes
   modified/corrected from Iyer et al., Biophys J 2004, Mazhari et al, Circ Res 2001

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef IYER_MAZHARI_WINSLOW_KR_H
#define IYER_MAZHARI_WINSLOW_KR_H

#include <IyerMazhariWinslowKrParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_IyerMazhariWinslowKrParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pIMW->P[NS_IyerMazhariWinslowKrParameters::a].value
#endif // ifdef HETERO

class IyerMazhariWinslowKr : public vbElphyModel<ML_CalcType> {
public:
  double Ki;

  double C1Kr;
  double C2Kr;
  double C3Kr;
  double OKr;
  double IKr;

  double Qb;

  IyerMazhariWinslowKrParameters *pIMW;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  IyerMazhariWinslowKr(IyerMazhariWinslowKrParameters *);

  ~IyerMazhariWinslowKr() {}

  virtual void Init();

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vmyo); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.; }

  virtual inline ML_CalcType GetVm() { return -.09066; }

  virtual inline ML_CalcType GetCai() { return 0; }

  virtual inline ML_CalcType GetCao() { return 0; }

  virtual inline ML_CalcType GetNai() { return 0; }

  virtual inline ML_CalcType GetNao() { return 0; }

  virtual inline ML_CalcType GetKi() { return Ki; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_Ko); }

  virtual inline int GetSize(void) {
    return sizeof(IyerMazhariWinslowKr) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(IyerMazhariWinslowKrParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return (ML_CalcType *) &Ki; }

  virtual inline void SetCai(ML_CalcType val) {}

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);

  virtual int GetNumStatus() { return 6; }

  virtual void GetStatus(double *) const;

  virtual void SetStatus(const double *);
}; // class IyerMazhariWinslowKr


#endif // ifndef IYER_MAZHARI_WINSLOW_KR_H
