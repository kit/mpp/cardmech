/*      File: FentonKarma.h
   automatically created by ExtractParameterClass.pl - done by dw (22.03.2007)
   Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
   send comments to dw@ibt.uka.de       */

#include <FentonKarma.h>

FentonKarma::FentonKarma(FentonKarmaParameters *pp) {
  pCmP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pCmP, NS_FentonKarmaParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

FentonKarma::~FentonKarma() {
  cerr << "SpeedCompare for tend=50: 3.61 | 3.43 | 3.97\n";
}

#ifdef HETERO

inline bool FentonKarma::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool FentonKarma::AddHeteroValue(string desc, double val) {
  throw kaBaseException("not possible!\n");
}

#endif // ifdef HETERO

inline int FentonKarma::GetSize(void) {
  return sizeof(FentonKarma) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(FentonKarmaParameters *)
#ifdef HETERO
    -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
      ;
}

inline unsigned char FentonKarma::getSpeed(ML_CalcType adVm) {
  return (unsigned char) (adVm < 1e-6 ? 2 : 1);
}

void FentonKarma::Init() {
#if KADEBUG
  cerr << "FentonKarma::Init" << endl;
#endif // if KADEBUG

  // Init values
  u = CELLMODEL_PARAMVALUE(VT_Init_u);
  v = CELLMODEL_PARAMVALUE(VT_Init_v);
  w = CELLMODEL_PARAMVALUE(VT_Init_w);
}

ML_CalcType
FentonKarma::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
                  int euler = 2) {
  // Constants
  tinc *= 1000.0; // convert tinc from seconds to milliseconds

  const ML_CalcType J_stim = i_external * CELLMODEL_PARAMVALUE(VT_dCm_V_fi_V_O);

  const ML_CalcType p = (u < CELLMODEL_PARAMVALUE(VT_u_c) ? 0.0 : 1.0);
  const ML_CalcType q = (u < CELLMODEL_PARAMVALUE(VT_u_v) ? 0.0 : 1.0);

  const ML_CalcType J_fi = -(v * p * (u - CELLMODEL_PARAMVALUE(VT_u_c)) * ((1.0 - u) * CELLMODEL_PARAMVALUE(VT_dtau_d)));
  const ML_CalcType J_so = ((u * (1.0 - p)) * CELLMODEL_PARAMVALUE(VT_dtau_O)) + (p * CELLMODEL_PARAMVALUE(VT_dtau_r));
  const ML_CalcType J_si = ((-(w) * (1.0 + tanh(CELLMODEL_PARAMVALUE(VT_k) * (u - CELLMODEL_PARAMVALUE(VT_u_csi))))) * CELLMODEL_PARAMVALUE(VT_d2tau_si));

  ML_CalcType tau_v_minus = (q * CELLMODEL_PARAMVALUE(VT_tau_v1_minus) + (1.0 - q) * CELLMODEL_PARAMVALUE(VT_tau_v2_minus));
  v += ((1.0 - p) * ((1.0 - v) / tau_v_minus) - ((p * v) * CELLMODEL_PARAMVALUE(VT_dtau_v_plus))) * tinc;
  w += ((1.0 - p) * ((1.0 - w) * CELLMODEL_PARAMVALUE(VT_dtau_w_minus)) - (p * w) * CELLMODEL_PARAMVALUE(VT_dtau_w_plus)) * tinc;
  const ML_CalcType total_flux = J_stim - (J_fi + J_so + J_si);

  u += total_flux * tinc;

  return .001 * total_flux * CELLMODEL_PARAMVALUE(VT_V_fi_V_O);
}

void FentonKarma::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << u << ' ' << v << ' ' << w << ' ';
}

void FentonKarma::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
}

void FentonKarma::GetParameterNames(vector<string> &getpara) {
  const int numpara = 3;
  const string ParaNames[numpara] = {"u", "v", "w"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void FentonKarma::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
}
