/*      File: PuglisiBersParameters.cpp
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */


#include <PuglisiBersParameters.h>
PuglisiBersParameters::PuglisiBersParameters(const char *initFile, ML_CalcType tinc) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

PuglisiBersParameters::~PuglisiBersParameters() {
  // Destruktor
}

void PuglisiBersParameters::PrintParameters() {
  // print the parameter to the stdout
  cout<<"PuglisiBersParameters:"<<endl;
  for (int i = vtFirst; i < vtLast; i++)
    cout << P[i].name << "\t = " << P[i].value << endl;
}

void PuglisiBersParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "PuglisiBersParameters::Init('" << initFile << "')\n";
#endif // if KADEBUG

  // Initialization of the Parameters ...
  P[VT_Tx].name          =  "T";
  P[VT_Na_i].name        =        "Init_Na_i";
  P[VT_Na_o].name        =        "Init_Na_o";
  P[VT_Na_b].name        =        "Init_Na_b";
  P[VT_K_i].name         = "Init_K_i";
  P[VT_K_o].name         = "Init_K_o";
  P[VT_K_b].name         = "Init_K_b";
  P[VT_Ca_i].name        =        "Init_Ca_i";
  P[VT_Ca_o].name        =        "Init_Ca_o";
  P[VT_Ca_b].name        =        "Init_Ca_b";
  P[VT_Cl_o].name        =        "Init_Cl_o";
  P[VT_Cl_i].name        =        "Init_Cl_i";
  P[VT_V].name           =   "Init_V";
  P[VT_m].name           =   "Init_m";
  P[VT_h].name           =   "Init_h";
  P[VT_j].name           =   "Init_j";
  P[VT_d].name           =   "Init_d";
  P[VT_f].name           =   "Init_f";
  P[VT_Xs].name          =  "Init_Xs";
  P[VT_Xr].name          =  "Init_Xr";
  P[VT_b].name           =   "Init_b";
  P[VT_g].name           =   "Init_g";
  P[VT_Xtof].name        =        "Init_Xtof";
  P[VT_Ytof].name        =        "Init_Ytof";
  P[VT_Xtos].name        =        "Init_Xtos";
  P[VT_Yto1s].name       =       "Init_Yto1s";
  P[VT_Yto2s].name       =       "Init_Yto2s";
  P[VT_t_rel].name       =       "trel";
  P[VT_Ca_int_2ms].name  =  "Ca_int_2ms";
  P[VT_Ca_int_Jrel].name = "Ca_int_Jrel";
  P[VT_Ca_JSR].name      =      "Init_Ca_JSR";
  P[VT_Ca_NSR].name      =      "Init_Ca_NSR";
  P[VT_TRPN].name        =        "Init_TRPN";
  P[VT_CMDN].name        =        "Init_CMDN";
  P[VT_CSQN].name        =        "Init_CSQN";
  P[VT_Ca_iontold].name  =  "Init_caiont";
  P[VT_dCa_iont].name    =    "Init_dCa_iont";
  P[VT_Vtot].name        =        "Vtot";
  P[VT_Cap].name         = "Cap";
  P[VT_fr_NSR].name      =      "fr_NSR";
  P[VT_fr_JSR].name      =      "fr_JSR";
  P[VT_fr_MYO].name      =      "fr_MYO";
  P[VT_fr_MITO].name     =     "fr_MITO";
  P[VT_fr_SR].name       =       "fr_SR";
  P[VT_fr_CLEFT].name    =    "fr_CLEFT";
  P[VT_Vol_NSR].name     =     "Vol_NSR";
  P[VT_Vol_JSR].name     =     "Vol_JSR";
  P[VT_Vol_MYO].name     =     "Vol_MYO";
  P[VT_Vol_MITO].name    =    "Vol_MITO";
  P[VT_Vol_SR].name      =      "Vol_SR";
  P[VT_g_Na].name        =        "g_Na";
  P[VT_k_mCa].name       =       "KmCa";
  P[VT_P_Ca].name        =        "P_Ca";
  P[VT_gamma_Cai].name   =   "gamma_Cai";
  P[VT_gamma_Cao].name   =   "gamma_Cao";
  P[VT_P_Na].name        =        "P_Na";
  P[VT_gamma_Nai].name   =   "gamma_Nai";
  P[VT_gamma_Nao].name   =   "gamma_Nao";
  P[VT_P_K].name         = "P_K";
  P[VT_gamma_Ki].name    =    "gamma_Ki";
  P[VT_gamma_Ko].name    =    "gamma_Ko";
  P[VT_g_CaT].name       =       "g_CaT";
  P[VT_E_CaT].name       =       "E_CaT";
  P[VT_prNaK].name       =       "prNaK";
  P[VT_gbar_K1].name     =     "gbar_K1";
  P[VT_gbar_Kr].name     =     "gbar_Kr";
  P[VT_gbar_Ks].name     =     "gbar_Ks";
  P[VT_g_Kp].name        =        "g_Kp";
  P[VT_kNaCa].name       =       "kNaCa";
  P[VT_KmNaex].name      =      "KmNaex";
  P[VT_KmCaex].name      =      "KmCaex";
  P[VT_eta].name         = "eta";
  P[VT_ksat].name        =        "ksat";
  P[VT_I_NaKmax].name    =    "I_NaKmax";
  P[VT_k_mNai].name      =      "k_mNai";
  P[VT_k_mKo].name       =       "k_mKo";
  P[VT_I_pCamax].name    =    "I_pCamax";
  P[VT_k_mpCa].name      =      "k_mpCa";
  P[VT_g_Cab].name       =       "g_Cab";
  P[VT_g_Nab].name       =       "g_Nab";
  P[VT_g_tof].name       =       "g_tof";
  P[VT_g_tos].name       =       "g_tos";
  P[VT_PnsK].name        =        "PnsK";
  P[VT_PnsNa].name       =       "PnsNa";
  P[VT_k_mnsCa].name     =     "k_mnsCa";
  P[VT_g_ClCa].name      =      "g_ClCa";
  P[VT_k_mClCa].name     =     "k_mClCa";
  P[VT_t_tr].name        =        "tau_tr";
  P[VT_k_mup].name       =       "k_mup";
  P[VT_I_upmax].name     =     "I_upmax";
  P[VT_Ca_NSRmax].name   =   "Ca_NSRmax";
  P[VT_t_on].name        =        "tau_on";
  P[VT_t_off].name       =       "tau_off";
  P[VT_CSQN_th].name     =     "CSQN_th";
  P[VT_g_maxrel].name    =    "g_maxrel";
  P[VT_CSQN_max].name    =    "CSQN_max";
  P[VT_k_mCSQN].name     =     "k_mCSQN";
  P[VT_CMDN_max].name    =    "CMDN_max";
  P[VT_TRPN_max].name    =    "TRPN_max";
  P[VT_k_mCMDN].name     =     "k_mCMDN";
  P[VT_k_mTRPN].name     =     "k_mTRPN";
  P[VT_g_K1].name        =        "g_K1";
  P[VT_g_Kr].name        =        "g_Kr";
  P[VT_F].name           =   "F";
  P[VT_R].name           =   "R";
  P[VT_FdRT].name        =        "FdRT";
  P[VT_RTdF].name        =        "RTdF";
  P[VT_d2F].name         = "d2F";
  P[VT_RTd2F].name       =       "RTd2F";
  P[VT_V_myo].name       =       "V_myo";
  P[VT_V_mito].name      =      "V_mito";
  P[VT_V_SR].name        =        "V_SR";
  P[VT_V_NSR].name       =       "V_NSR";
  P[VT_V_JSR].name       =       "V_JSR";
  P[VT_V_cleft].name     =     "V_cleft";
  P[VT_VJdVN].name       =       "VJdVN";
  P[VT_CapdVcF].name     =     "CapdVcF";
  P[VT_CapdVmF].name     =     "CapdVmF";
  P[VT_Amp].name         =         "Amp";

  P[VT_g_K1].readFromFile    = false;
  P[VT_g_Kr].readFromFile    = false;
  P[VT_F].readFromFile       = false;
  P[VT_R].readFromFile       = false;
  P[VT_RTdF].readFromFile    = false;
  P[VT_FdRT].readFromFile    = false;
  P[VT_d2F].readFromFile     = false;
  P[VT_RTd2F].readFromFile   = false;
  P[VT_V_myo].readFromFile   = false;
  P[VT_V_mito].readFromFile  = false;
  P[VT_V_SR].readFromFile    = false;
  P[VT_V_NSR].readFromFile   = false;
  P[VT_V_JSR].readFromFile   = false;
  P[VT_V_cleft].readFromFile = false;
  P[VT_VJdVN].readFromFile   = false;
  P[VT_CapdVcF].readFromFile = false;
  P[VT_CapdVmF].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_PuglisiBers);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  // End Initialization of the Parameters ...

  Calculate();
  InitTable(tinc);
} // PuglisiBersParameters::Init

void PuglisiBersParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "PuglisiBersParameters::Calculate()"<<endl;
#endif // if KADEBUG
  P[VT_g_K1].value    = P[VT_gbar_K1].value*sqrt(P[VT_K_o].value/5.4);
  P[VT_g_Kr].value    = P[VT_gbar_Kr].value*sqrt(P[VT_K_o].value/5.4);
  P[VT_F].value       = 96485;
  P[VT_R].value       = 8314;
  P[VT_RTdF].value    = P[VT_R].value*P[VT_Tx].value/(P[VT_F].value);
  P[VT_FdRT].value    = 1./(P[VT_RTdF].value);
  P[VT_d2F].value     = 1.0/(2.0*(P[VT_F].value));
  P[VT_RTd2F].value   = (P[VT_RTdF].value)*.5;
  P[VT_V_myo].value   = (P[VT_Vtot].value*P[VT_fr_MYO].value);
  P[VT_V_mito].value  = (P[VT_Vtot].value*P[VT_fr_MITO].value);
  P[VT_V_SR].value    = (P[VT_Vtot].value*P[VT_fr_SR].value);
  P[VT_V_NSR].value   = (P[VT_Vtot].value*P[VT_fr_NSR].value);
  P[VT_V_JSR].value   = (P[VT_Vtot].value*P[VT_fr_JSR].value);
  P[VT_V_cleft].value = (P[VT_Vtot].value*P[VT_fr_CLEFT].value);
  P[VT_VJdVN].value   = P[VT_V_JSR].value/(P[VT_V_NSR].value);
  P[VT_CapdVcF].value = P[VT_Cap].value/(P[VT_V_cleft].value*(P[VT_F].value));
  P[VT_CapdVmF].value = P[VT_Cap].value/(P[VT_V_myo].value)/(P[VT_F].value);
}

void PuglisiBersParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr << "PuglisiBersParameters::InitTable()\n";
#endif // if KADEBUG
  tinc *= -1000.0;  // sec -> ms, negation for exp(-dt/tau)
  ML_CalcType a, b;
  for (double V = -RangeTabhalf+.0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int)(DivisionTab*(RangeTabhalf+V)+.5);

    /* Na */
    if (V >= -40.0) {
      a            = .0;
      b            = 1/(.13*(1.0+exp((V+10.66)/ -11.1)));
      h_inf[Vi]    = 0.; // = a/b
      exptau_h[Vi] = exp(tinc * b);

      // a_=.0;
      b            = .3*exp(-2.535e-7*V)/(1.0+exp(-.1*(V+32.0)));
      j_inf[Vi]    = 0.;
      exptau_j[Vi] = exp(tinc * b);
    } else   {
      a            = .135*exp((V+80.0)/ -6.8);
      b            = a + (3.56*exp(.079*V)+3.1e5*exp(.35*V));
      h_inf[Vi]    = a / b;
      exptau_h[Vi] = exp(tinc * b);

      a            = (-1.27140e5*exp(.2444*V)-3.474e-5*exp(-.04391*V))*((V+37.78)/(1.0+exp(.311*(V+79.23))));
      b            = a + (0.1212*exp(-.01052*V)/(1.0+exp(-.1378*(V+40.14))));
      j_inf[Vi]    = a / b;
      exptau_j[Vi] = exp(tinc * b);
    }
    a            = .32*(V+47.13)/(1.0-exp(-.1*(V+47.13)));
    b            = a + .08*exp(-V/11.0);
    m_inf[Vi]    = a / b;
    exptau_m[Vi] = exp(tinc * b);  // = exp( -dt / tau)

    /* L-type Ca */
    d_inf[Vi]    = 1./(1.+exp(-(V+0.01)/6.24));
    exptau_d[Vi] = exp(tinc / (d_inf[Vi]*(1.-exp(-(V+0.01)/6.24))/(0.035*(V+0.01))));
    exptau_f[Vi] = exp(tinc / (1/(0.0197*exp(-pow(0.0337*(V+0.01), 2))+0.02)));
    f_inf[Vi]    = 1/(1+exp((V+35.06)/8.6))+0.6/(1+exp((50-V)/20));

    /* T-type Ca */
    exptau_b[Vi]  = exp(tinc / (0.1 + 5.4/(1+exp((V+100.)/33.))));
    b_inf[Vi]     = 1./(1+exp(-(V+48.)/6.1));
    exptau_g[Vi]  = exp(tinc / (8. + 32./(1+exp((V+65.)/5.))));
    g_inf[Vi]     = 1./(1+exp((V+66.)/6.6));
    Xr_inf[Vi]    = 1./(1.+exp(-(V+21.1)/8.1));
    exptau_Xr[Vi] = exp(tinc * (0.00138*(V+4.2)/(1.-exp(-0.123*(V+4.2)))+0.00061*(V+28.9)/(exp(0.145*(V+28.9))-1.)));
    R_Xr[Vi]      = 1./(1.+exp((V+33.)/22.4));
    exptau_Xs[Vi] = exp(tinc * (7.19e-5*(V+10.)/(1.-exp(-0.148*(V+10.)))+1.31e-4*(V+10.)/(exp(0.0687*(V+10.))-1.)));
    Xs_inf[Vi]    = 1/(1+exp(-(V-0.1)/13.5));

    /* Transient outward current: Itof */
    exptau_Xtof[Vi] = exp(tinc / (3.5*exp(-pow((V+3)/30, 2.))+1.5));
    Xtof_inf[Vi]    = 1./(1.+exp((V+3.)/ -15.));
    exptau_Ytof[Vi] = exp(tinc / (20./(1.+exp(((V+33.5)/10.)))+20.));
    Ytof_inf[Vi]    = 1./(1.+exp((V+33.5)/10.));

    /* Transient outward current: Itos */
    exptau_Xtos[Vi]  = exp(tinc / (9./(1.+exp((V+3.)/15.))+0.5));
    Xtos_inf[Vi]     = 1./(1.+exp((V+3.)/ -15.));
    exptau_Yto1s[Vi] = exp(tinc / ((3000./(1.+exp((V+60.)/10.)))+20.));
    Yto1s_inf[Vi]    = 1./(1.+exp((V+33.5)/10.));
    exptau_Yto2s[Vi] = exp(tinc / ((2800./(1.+exp((V+60.)/10.)))+220.));
    Yto2s_inf[Vi]    = 1./(1.+exp((V+33.5)/10.));
    Kp[Vi]           = P[VT_g_Kp].value/(1.+exp((7.488-V)/5.98));

    // const double KgC=P[VT_gamma_Cai].value/(P[VT_gamma_Cao].value);
    const double VFdRT = V*(P[VT_FdRT].value);
    expV[Vi]     = exp(VFdRT);
    expV_2[Vi]   = exp(VFdRT*2.0);
    expVm[Vi]    = exp(-VFdRT);
    expV_mk1[Vi] = exp(-.1*VFdRT);
  }
} // PuglisiBersParameters::InitTable
