/*      File: LuoRudy.h
    automatically created by ExtractParameterClass.pl - done by dw (19.09.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#ifndef LUORUDY
#define LUORUDY

#include <LuoRudyParameters.h>

#define HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_LuoRudyParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pP->P[NS_LuoRudyParameters::a].value
#endif // ifdef HETERO

class LuoRudy : public vbElphyModel<ML_CalcType> {
public:
  LuoRudyParameters *pP;
  ML_CalcType Ca_i, Ca_o;
  ML_CalcType Na_i, K_o;
  double K_i, Na_o;
  ML_CalcType m, h, j, d, f, b, g, Xr, Xs1, Xs2, ato, ito;
  ML_CalcType Ca_JSR, Ca_NSR, dCa_iont, Ca_iontold;
  ML_CalcType t_CICR, t_jsrol;
  unsigned char grelbarjsrol, flip;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  LuoRudy(LuoRudyParameters *pp);

  ~LuoRudy();

  virtual bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType Volume() { return (CELLMODEL_PARAMVALUE(VT_Vol)) * 12.5; }

  virtual inline ML_CalcType GetAmplitude() {
    return 18. / 1e-3 * (CELLMODEL_PARAMVALUE(VT_A_Cap));
  } // 18 A/F (LR) ~ 2.7 nA (per cell)

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_init_V_m); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return Ca_o; }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return Na_o; }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &Ca_i; }

  virtual inline ML_CalcType GetKo() { return K_o; }

  virtual inline ML_CalcType GetTCa() { return CELLMODEL_PARAMVALUE(VT_TRPN_max) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mTRPN))); }

  virtual inline bool OutIsTCa() { return true; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char) (adVm < .1e-6 ? 5 : (adVm < .2e-6 ? 4 : (adVm < .4e-6 ? 3 : (adVm < .8e-6
                                                                                        ? 2 : 1))));
  }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline void SetCao(ML_CalcType val) { Ca_o = val; }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetNao(ML_CalcType val) { Na_o = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual inline void SetKo(ML_CalcType val) { K_o = val; }

  virtual void Init();

  virtual ML_CalcType
  Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch, int euler);

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class LuoRudy

class LuoRudyCa : public LuoRudy {
public:
  LuoRudyCa(LuoRudyParameters *pp) : LuoRudy(pp) {}

  virtual inline ML_CalcType GetTCa() { return 0.0; }

  virtual inline bool OutIsTCa() { return false; }
};

#endif // ifndef LUORUDY
