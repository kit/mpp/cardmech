/* File: MahajanEtAl.cpp
        automatically created by CellML2Elphymodel.pl
        Institute of Biomedical Engineering, Universität Karlsruhe (TH) */

#include <cstdio>
#include <cmath>
#include <MahajanEtAl.h>

MahajanEtAl::MahajanEtAl(MahajanEtAlParameters *pp) {
  ptTeaP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(ptTeaP, NS_MahajanEtAlParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

MahajanEtAl::~MahajanEtAl() {}

#ifdef HETERO

inline bool MahajanEtAl::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool MahajanEtAl::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int MahajanEtAl::GetSize(void) {
  return (&trops - &Ca_i + 1) * sizeof(ML_CalcType);
}

inline unsigned char MahajanEtAl::getSpeed(ML_CalcType adVm) {
  return (unsigned char) 5;
}

void MahajanEtAl::Init() {
  xm = (CELLMODEL_PARAMVALUE(VT_xm_init));
  xh = (CELLMODEL_PARAMVALUE(VT_xh_init));
  xj = (CELLMODEL_PARAMVALUE(VT_xj_init));
  Ca_dyad = (CELLMODEL_PARAMVALUE(VT_Ca_dyad_init));
  c1 = (CELLMODEL_PARAMVALUE(VT_c1_init));
  c2 = (CELLMODEL_PARAMVALUE(VT_c2_init));
  xi1ca = (CELLMODEL_PARAMVALUE(VT_xi1ca_init));
  xi1ba = (CELLMODEL_PARAMVALUE(VT_xi1ba_init));
  xi2ca = (CELLMODEL_PARAMVALUE(VT_xi2ca_init));
  xi2ba = (CELLMODEL_PARAMVALUE(VT_xi2ba_init));
  xr = (CELLMODEL_PARAMVALUE(VT_xr_init));
  Ca_i = (CELLMODEL_PARAMVALUE(VT_Ca_i_init));
  xs1 = (CELLMODEL_PARAMVALUE(VT_xs1_init));
  xs2 = (CELLMODEL_PARAMVALUE(VT_xs2_init));
  xtos = (CELLMODEL_PARAMVALUE(VT_xtos_init));
  ytos = (CELLMODEL_PARAMVALUE(VT_ytos_init));
  xtof = (CELLMODEL_PARAMVALUE(VT_xtof_init));
  ytof = (CELLMODEL_PARAMVALUE(VT_ytof_init));
  Na_i = (CELLMODEL_PARAMVALUE(VT_Na_i_init));
  Ca_submem = (CELLMODEL_PARAMVALUE(VT_Ca_submem_init));
  Ca_NSR = (CELLMODEL_PARAMVALUE(VT_Ca_NSR_init));
  Ca_JSR = (CELLMODEL_PARAMVALUE(VT_Ca_JSR_init));
  xir = (CELLMODEL_PARAMVALUE(VT_xir_init));
  tropi = (CELLMODEL_PARAMVALUE(VT_tropi_init));
  trops = (CELLMODEL_PARAMVALUE(VT_trops_init));
} // MahajanEtAl::Init

ML_CalcType
MahajanEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                  int euler) {
  ML_CalcType svolt = V * 1000;
  ML_CalcType HT = tinc * 1000;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + svolt) + .5);

  // calculating algebraic part
  const ML_CalcType dtaukr = 1.0 / (ptTeaP->taukr[Vi]);

  // const ML_CalcType taukr = 1.00000/(ptTeaP->xkrv1[Vi]+ptTeaP->xkrv2[Vi]);
  // const ML_CalcType xs2ss = ptTeaP->xs1ss[Vi];
  // const ML_CalcType tauxs2 =  ptTeaP->tauxs2[Vi];
  // const ML_CalcType tauxs2 =  4.00000*ptTeaP->tauxs1[Vi];
  const ML_CalcType xtos_inf = ptTeaP->xtos_inf[Vi];

  // const ML_CalcType xtos_inf = 1.00000/(1.00000+(exp(ptTeaP->rt1[Vi])));
  // const ML_CalcType txs = ptTeaP->txs[Vi];
  // const ML_CalcType txs = 9.00000/(1.00000+(exp((- ptTeaP->rt1[Vi]))))+0.500000;
  // const ML_CalcType xtof_inf = xtos_inf;
  // const ML_CalcType txf =  ptTeaP->txf[Vi];
  // const ML_CalcType txf =  3.50000*(exp(ptTeaP->rt4[Vi]))+1.50000;
  const ML_CalcType alpha = ptTeaP->alpha[Vi];

  // const ML_CalcType alpha = ptTeaP->poinf[Vi]/(CELLMODEL_PARAMVALUE(VT_taupo));
  const ML_CalcType beta = ptTeaP->beta[Vi];

  // const ML_CalcType beta = (1.00000 - ptTeaP->poinf[Vi])/(CELLMODEL_PARAMVALUE(VT_taupo));
  const ML_CalcType fca =
      1.00000 / (1.00000 + ((1 / Ca_dyad) * (1 / Ca_dyad) * (1 / Ca_dyad) * CELLMODEL_PARAMVALUE(VT_cat3)));

  // const ML_CalcType fca = 1.00000/(1.00000+(pow(((CELLMODEL_PARAMVALUE(VT_cat))/Ca_dyad), 3.00000)));
  ML_CalcType tau_ca =
      (CELLMODEL_PARAMVALUE(VT_tca)) / (1.00000 + ((Ca_dyad) * (Ca_dyad) * (Ca_dyad) * (Ca_dyad) * CELLMODEL_PARAMVALUE(VT_cptm4))) +
      0.100000;

  // const ML_CalcType tau_ca = (CELLMODEL_PARAMVALUE(VT_tca))/(1.00000+(pow((Ca_dyad/(CELLMODEL_PARAMVALUE(VT_cpt))), 4.00000)))+0.100000;
  const ML_CalcType dtauca = 1.0 / ((ptTeaP->recov[Vi] - tau_ca) * ptTeaP->Pr[Vi] + tau_ca);
  const ML_CalcType k6 = (fca * ptTeaP->Ps[Vi]) * dtauca;
  const ML_CalcType k5 = (1.00000 - ptTeaP->Ps[Vi]) * dtauca;
  const ML_CalcType dtauba = 1 / (ptTeaP->tauba[Vi]);

  // const ML_CalcType tauba =  (ptTeaP->recov[Vi] - 450.000)*ptTeaP->Pr[Vi]+450.000;
  const ML_CalcType k6t = (ptTeaP->Ps[Vi]) * dtauba;
  const ML_CalcType k5t = (1.00000 - ptTeaP->Ps[Vi]) * dtauba;
  const ML_CalcType k3 = ptTeaP->k3[Vi];

  // const ML_CalcType k3 = (1.00000 - ptTeaP->poi[Vi])/(CELLMODEL_PARAMVALUE(VT_tau3));
  const ML_CalcType k1 = 0.0241680 * fca;
  const ML_CalcType k4 = (((((k3 * alpha) / beta) * k1) / (CELLMODEL_PARAMVALUE(VT_k2))) * k5) / k6;
  const ML_CalcType k3t = k3;
  const ML_CalcType k4t = (((((k3t * alpha) / beta) * (CELLMODEL_PARAMVALUE(VT_k1t))) / (CELLMODEL_PARAMVALUE(VT_k2t))) * k5t) / k6t;
  const ML_CalcType po = (((((1.00000 - xi1ca) - xi2ca) - xi1ba) - xi2ba) - c1) - c2;
  const ML_CalcType s1 = 0.0182688 * fca;
  const ML_CalcType s2 = s1 * CELLMODEL_PARAMVALUE(VT_r1r2k2) / k1;

  // const ML_CalcType s2 = ( (( s1*(CELLMODEL_PARAMVALUE(VT_r1)))/(CELLMODEL_PARAMVALUE(VT_r2)))*(CELLMODEL_PARAMVALUE(VT_k2)))/k1;
  const ML_CalcType ytos_inf = ptTeaP->ytos_inf[Vi];

  // const ML_CalcType ytos_inf = 1.00000/(1.00000+(exp(ptTeaP->rt2[Vi])));
  // const ML_CalcType tys = ptTeaP->tys[Vi];
  // const ML_CalcType tys = 3000.00/(1.00000+(exp(ptTeaP->rt3[Vi])))+30.0000;
  // const ML_CalcType ytof_inf = ytos_inf;
  // const ML_CalcType tyf = ptTeaP->tyf[Vi];
  // const ML_CalcType tyf = 20.0000/(1.00000+(exp(ptTeaP->rt5[Vi])))+20.0000;
  const ML_CalcType kmem = (CELLMODEL_PARAMVALUE(VT_kmem));
  const ML_CalcType cup2 = (CELLMODEL_PARAMVALUE(VT_cup)) * (CELLMODEL_PARAMVALUE(VT_cup));
  const ML_CalcType jup = ((CELLMODEL_PARAMVALUE(VT_vup)) * Ca_i * Ca_i) / (Ca_i * Ca_i + cup2);

  // const ML_CalcType jup = ( (CELLMODEL_PARAMVALUE(VT_vup))*Ca_i*Ca_i)/( Ca_i*Ca_i+ (CELLMODEL_PARAMVALUE(VT_cup))*(CELLMODEL_PARAMVALUE(VT_cup)));
  const ML_CalcType kj2 = (CELLMODEL_PARAMVALUE(VT_kj)) * (CELLMODEL_PARAMVALUE(VT_kj));
  const ML_CalcType jleak =
      (((CELLMODEL_PARAMVALUE(VT_gleak)) * Ca_NSR * Ca_NSR) / (Ca_NSR * Ca_NSR + (kj2))) * (Ca_NSR * 16.6670 - Ca_i);

  // const ML_CalcType jleak =  (( (CELLMODEL_PARAMVALUE(VT_gleak))*Ca_NSR*Ca_NSR)/( Ca_NSR*Ca_NSR+ (CELLMODEL_PARAMVALUE(VT_kj))*(CELLMODEL_PARAMVALUE(VT_kj))))*(
  // Ca_NSR*16.6670 - Ca_i);
  const ML_CalcType xkcal = (CELLMODEL_PARAMVALUE(VT_xkcal));
  const ML_CalcType bpxi = ((CELLMODEL_PARAMVALUE(VT_bcal)) * (xkcal)) / (((xkcal) + Ca_i) * ((xkcal) + Ca_i));

  // const ML_CalcType bpxi = ( (CELLMODEL_PARAMVALUE(VT_bcal))*(CELLMODEL_PARAMVALUE(VT_xkcal)))/( ((CELLMODEL_PARAMVALUE(VT_xkcal))+Ca_i)*((CELLMODEL_PARAMVALUE(VT_xkcal))+Ca_i));
  const ML_CalcType srkd = (CELLMODEL_PARAMVALUE(VT_srkd));
  const ML_CalcType spxi = ((CELLMODEL_PARAMVALUE(VT_srmax)) * (srkd)) / (((srkd) + Ca_i) * ((srkd) + Ca_i));

  // const ML_CalcType spxi = ( (CELLMODEL_PARAMVALUE(VT_srmax))*(CELLMODEL_PARAMVALUE(VT_srkd)))/( ((CELLMODEL_PARAMVALUE(VT_srkd))+Ca_i)*((CELLMODEL_PARAMVALUE(VT_srkd))+Ca_i));
  const ML_CalcType mempxi = ((CELLMODEL_PARAMVALUE(VT_bmem)) * kmem) / ((kmem + Ca_i) * (kmem + Ca_i));
  const ML_CalcType ksar = (CELLMODEL_PARAMVALUE(VT_ksar));
  const ML_CalcType sarpxi = ((CELLMODEL_PARAMVALUE(VT_bsar)) * (ksar)) / (((ksar) + Ca_i) * ((ksar) + Ca_i));

  // const ML_CalcType sarpxi = ( (CELLMODEL_PARAMVALUE(VT_bsar))*(CELLMODEL_PARAMVALUE(VT_ksar)))/( ((CELLMODEL_PARAMVALUE(VT_ksar))+Ca_i)*((CELLMODEL_PARAMVALUE(VT_ksar))+Ca_i));
  const ML_CalcType dciib = 1.00000 / (1.00000 + bpxi + spxi + mempxi + sarpxi);
  const ML_CalcType xkon = (CELLMODEL_PARAMVALUE(VT_xkon));
  const ML_CalcType btrop = (CELLMODEL_PARAMVALUE(VT_btrop));
  const ML_CalcType xkoff = (CELLMODEL_PARAMVALUE(VT_xkoff));
  const ML_CalcType xbi = (xkon) * Ca_i * ((btrop) - tropi) - (xkoff) * tropi;
  const ML_CalcType jd = (Ca_submem - Ca_i) / (CELLMODEL_PARAMVALUE(VT_taud));
  const ML_CalcType xbs = (xkon) * Ca_submem * ((btrop) - trops) - (xkoff) * trops;
  const ML_CalcType dCa_JSR = (-xir + jup) - jleak;
  const ML_CalcType cstar = (CELLMODEL_PARAMVALUE(VT_cstar));
  const ML_CalcType Qr0 = (Ca_JSR > 50.0000 && Ca_JSR < (cstar) ?
                           (Ca_JSR - 50.0000) / 1.00000 : Ca_JSR >= (cstar) ?
                                                          (CELLMODEL_PARAMVALUE(VT_av)) * Ca_JSR + (CELLMODEL_PARAMVALUE(VT_bv))
                                                                            : 0.00000);
  const ML_CalcType Qr = (Ca_NSR * Qr0) / (cstar);
  const ML_CalcType csm = Ca_submem / 1000.00;
  const ML_CalcType FonRT = (CELLMODEL_PARAMVALUE(VT_FonRT));
  const ML_CalcType F = (CELLMODEL_PARAMVALUE(VT_F));
  const ML_CalcType pca = (CELLMODEL_PARAMVALUE(VT_pca));
  const ML_CalcType Ca_o = (CELLMODEL_PARAMVALUE(VT_Ca_o));
  const ML_CalcType rxa =
      ((fabs(svolt * 2.0 * (FonRT))) <
       0.00100000 ? (4.00000 * (pca) * (F) * (FonRT) *
                     (csm * (exp(svolt * 2.0 * (FonRT))) - 0.341000 * (Ca_o))) /
                    (2.00000 * (FonRT)) : (4.00000 * (pca) * svolt * (F) * (FonRT) *
                                           (csm * (exp(svolt * 2.0 * (FonRT))) -
                                            0.341000 * (Ca_o))) /
                                          ((exp(svolt * 2.0 * (FonRT))) - 1.00000));

  // const ML_CalcType rxa =  ((fabs(svolt*2.0*(CELLMODEL_PARAMVALUE(VT_FonRT))))<0.00100000 ? (
  // 4.00000*(CELLMODEL_PARAMVALUE(VT_pca))*(CELLMODEL_PARAMVALUE(VT_F))*(CELLMODEL_PARAMVALUE(VT_FonRT))*( csm*(exp(svolt*2.0*(CELLMODEL_PARAMVALUE(VT_FonRT)))) -  0.341000*(CELLMODEL_PARAMVALUE(VT_Ca_o))))/(
  // 2.00000*(CELLMODEL_PARAMVALUE(VT_FonRT))) :( 4.00000*(CELLMODEL_PARAMVALUE(VT_pca))*svolt*(CELLMODEL_PARAMVALUE(VT_F))*(CELLMODEL_PARAMVALUE(VT_FonRT))*( csm*(exp(svolt*2.0*(CELLMODEL_PARAMVALUE(VT_FonRT)))) -
  //  0.341000*(CELLMODEL_PARAMVALUE(VT_Ca_o))))/((exp(svolt*2.0*(CELLMODEL_PARAMVALUE(VT_FonRT)))) - 1.00000));
  // const ML_CalcType rxa =  ((fabs(ptTeaP->za[Vi]))<0.00100000 ? ( 4.00000*(CELLMODEL_PARAMVALUE(VT_pca))*(CELLMODEL_PARAMVALUE(VT_F))*(CELLMODEL_PARAMVALUE(VT_FonRT))*(
  // csm*(exp(ptTeaP->za[Vi])) -  0.341000*(CELLMODEL_PARAMVALUE(VT_Ca_o))))/( 2.00000*(CELLMODEL_PARAMVALUE(VT_FonRT))) :(
  // 4.00000*(CELLMODEL_PARAMVALUE(VT_pca))*svolt*(CELLMODEL_PARAMVALUE(VT_F))*(CELLMODEL_PARAMVALUE(VT_FonRT))*( csm*(exp(ptTeaP->za[Vi])) -
  //  0.341000*(CELLMODEL_PARAMVALUE(VT_Ca_o))))/((exp(ptTeaP->za[Vi])) - 1.00000));
  const ML_CalcType spark_rate = ((CELLMODEL_PARAMVALUE(VT_gryr)) / 1.00000) * po * (fabs(rxa)) * ptTeaP->sparkV[Vi];
  const ML_CalcType xirp = (((po * Qr * (fabs(rxa)) * ptTeaP->comp[Vi])));

  // const ML_CalcType xirp = ( (( po*Qr*(fabs(rxa))*(CELLMODEL_PARAMVALUE(VT_gbarsr)))/1.00000)*(exp(( -
  // (CELLMODEL_PARAMVALUE(VT_ax))*(svolt+30.0000)))))/(1.00000+(exp(( - (CELLMODEL_PARAMVALUE(VT_ax))*(svolt+30.0000)))));
  const ML_CalcType xicap = po * (CELLMODEL_PARAMVALUE(VT_gdyad)) * (fabs(rxa));
  const ML_CalcType xiryr = xirp + xicap;
  const ML_CalcType jca = (CELLMODEL_PARAMVALUE(VT_gca)) * po * rxa;
  const ML_CalcType aloss =
      1.00000 / (1.00000 + ((1 / Ca_submem) * 1 / Ca_submem) * 1 / Ca_submem * CELLMODEL_PARAMVALUE(VT_xkdna3));

  // const ML_CalcType aloss = 1.00000/(1.00000+(pow(((CELLMODEL_PARAMVALUE(VT_xkdna))/Ca_submem), 3.00000)));
  const ML_CalcType Na_i3 = Na_i * Na_i * Na_i;
  const ML_CalcType zw3 =
      (Na_i3) * (Ca_o) * (exp((svolt * 0.350000 * (FonRT)))) - csm * ptTeaP->comp2[Vi];

  // const ML_CalcType zw3 =  (pow(Na_i, 3.00000))*(CELLMODEL_PARAMVALUE(VT_Ca_o))*(exp(( svolt*0.350000*(CELLMODEL_PARAMVALUE(VT_FonRT))))) -
  //  (pow((CELLMODEL_PARAMVALUE(VT_Na_o)), 3.00000))*csm*(exp(( svolt*(0.350000 - 1.00000)*(CELLMODEL_PARAMVALUE(VT_FonRT)))));
  const ML_CalcType xmnao3 = (CELLMODEL_PARAMVALUE(VT_xmnao)) * (CELLMODEL_PARAMVALUE(VT_xmnao)) * (CELLMODEL_PARAMVALUE(VT_xmnao));
  const ML_CalcType yz1 = (CELLMODEL_PARAMVALUE(VT_xmcao)) * (Na_i3) + ((xmnao3)) * csm;

  // const ML_CalcType yz1 =  (CELLMODEL_PARAMVALUE(VT_xmcao))*(pow(Na_i, 3.00000))+ (pow((CELLMODEL_PARAMVALUE(VT_xmnao)), 3.00000))*csm;
  const ML_CalcType yz2 = CELLMODEL_PARAMVALUE(VT_xmnai3) * (Ca_o) * (1.00000 + csm / (CELLMODEL_PARAMVALUE(VT_xmcai)));

  // const ML_CalcType yz2 =  (pow((CELLMODEL_PARAMVALUE(VT_xmnai)), 3.00000))*(CELLMODEL_PARAMVALUE(VT_Ca_o))*(1.00000+csm/(CELLMODEL_PARAMVALUE(VT_xmcai)));
  const ML_CalcType Na_o = (CELLMODEL_PARAMVALUE(VT_Na_o));
  const ML_CalcType Na_o3 = (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o));
  const ML_CalcType yz3 = (CELLMODEL_PARAMVALUE(VT_xmcai)) * ((Na_o3)) * (1.00000 + ((Na_i3) / CELLMODEL_PARAMVALUE(VT_xmnai3)));

  // const ML_CalcType yz3 =  (CELLMODEL_PARAMVALUE(VT_xmcai))*(pow((CELLMODEL_PARAMVALUE(VT_Na_o)), 3.00000))*(1.00000+(pow((Na_i/(CELLMODEL_PARAMVALUE(VT_xmnai))), 3.00000)));
  const ML_CalcType yz4 = (Na_i3) * (Ca_o) + ((Na_o3)) * csm;

  // const ML_CalcType yz4 =  (pow(Na_i, 3.00000))*(CELLMODEL_PARAMVALUE(VT_Ca_o))+ (pow((CELLMODEL_PARAMVALUE(VT_Na_o)), 3.00000))*csm;
  const ML_CalcType zw8 = yz1 + yz2 + yz3 + yz4;
  const ML_CalcType jNaCa = ((CELLMODEL_PARAMVALUE(VT_gNaCa)) * aloss * zw3) / (ptTeaP->zw4[Vi] * zw8);
  const ML_CalcType bpxs =
      ((CELLMODEL_PARAMVALUE(VT_bcal)) * (xkcal)) / (((xkcal) + Ca_submem) * ((xkcal) + Ca_submem));

  // const ML_CalcType bpxs = ( (CELLMODEL_PARAMVALUE(VT_bcal))*(CELLMODEL_PARAMVALUE(VT_xkcal)))/( ((CELLMODEL_PARAMVALUE(VT_xkcal))+Ca_submem)*((CELLMODEL_PARAMVALUE(VT_xkcal))+Ca_submem));
  const ML_CalcType spxs = ((CELLMODEL_PARAMVALUE(VT_srmax)) * (srkd)) / (((srkd) + Ca_submem) * ((srkd) + Ca_submem));

  // const ML_CalcType spxs = ( (CELLMODEL_PARAMVALUE(VT_srmax))*(CELLMODEL_PARAMVALUE(VT_srkd)))/( ((CELLMODEL_PARAMVALUE(VT_srkd))+Ca_submem)*((CELLMODEL_PARAMVALUE(VT_srkd))+Ca_submem));
  const ML_CalcType mempxs =
      ((CELLMODEL_PARAMVALUE(VT_bmem)) * (kmem)) / (((kmem) + Ca_submem) * ((kmem) + Ca_submem));

  // const ML_CalcType mempxs = ( (CELLMODEL_PARAMVALUE(VT_bmem))*(CELLMODEL_PARAMVALUE(VT_kmem)))/( ((CELLMODEL_PARAMVALUE(VT_kmem))+Ca_submem)*((CELLMODEL_PARAMVALUE(VT_kmem))+Ca_submem));
  const ML_CalcType sarpxs =
      ((CELLMODEL_PARAMVALUE(VT_bsar)) * (ksar)) / (((ksar) + Ca_submem) * ((ksar) + Ca_submem));

  // const ML_CalcType sarpxs = ( (CELLMODEL_PARAMVALUE(VT_bsar))*(CELLMODEL_PARAMVALUE(VT_ksar)))/( ((CELLMODEL_PARAMVALUE(VT_ksar))+Ca_submem)*((CELLMODEL_PARAMVALUE(VT_ksar))+Ca_submem));
  const ML_CalcType dcsib = 1.00000 / (1.00000 + bpxs + spxs + mempxs + sarpxs);
  const ML_CalcType K_o = (CELLMODEL_PARAMVALUE(VT_K_o));
  const ML_CalcType xiNaK =
      ((((CELLMODEL_PARAMVALUE(VT_gNaK)) * ptTeaP->fNaK[Vi] * Na_i) / (Na_i + (CELLMODEL_PARAMVALUE(VT_xkmnai)))) * (K_o)) /
      ((K_o) + (CELLMODEL_PARAMVALUE(VT_xkmko)));

  // const ML_CalcType xiNaK = ( ((
  // (CELLMODEL_PARAMVALUE(VT_gNaK))*ptTeaP->fNaK[Vi]*Na_i)/(Na_i+(CELLMODEL_PARAMVALUE(VT_xkmnai))))*(CELLMODEL_PARAMVALUE(VT_K_o)))/((CELLMODEL_PARAMVALUE(VT_K_o))+(CELLMODEL_PARAMVALUE(VT_xkmko)));
  const ML_CalcType xiNaCa = (CELLMODEL_PARAMVALUE(VT_wca)) * jNaCa;
  const ML_CalcType ena = (1.00000 / (FonRT)) * (log(((Na_o) / Na_i)));

  // const ML_CalcType ena =  (1.00000/(CELLMODEL_PARAMVALUE(VT_FonRT)))*(log(((CELLMODEL_PARAMVALUE(VT_Na_o))/Na_i)));
  const ML_CalcType xina = (CELLMODEL_PARAMVALUE(VT_gna)) * xh * xj * xm * xm * xm * (svolt - ena);

  // const ML_CalcType xkin = ptTeaP->xkin[Vi];
  // const ML_CalcType xkin = (ptTeaP->aki[Vi])/(ptTeaP->aki[Vi]+ptTeaP->bki[Vi]);
  const ML_CalcType ek = (CELLMODEL_PARAMVALUE(VT_ek));
  const ML_CalcType xik1 = (CELLMODEL_PARAMVALUE(VT_gkix)) * sqrt((K_o) / 5.40000) * ptTeaP->xkin[Vi] * (svolt - (ek));

  // const ML_CalcType xik1 =  (CELLMODEL_PARAMVALUE(VT_gkix))* pow(((CELLMODEL_PARAMVALUE(VT_K_o))/5.40000), 1.0 / 2)*xkin*(svolt - (CELLMODEL_PARAMVALUE(VT_ek)));
  // const ML_CalcType rs_inf = ptTeaP->rs_inf[Vi];
  // const ML_CalcType rs_inf = 1.00000/(1.00000+(exp(ptTeaP->rt2[Vi])));
  const ML_CalcType xitos =
      (CELLMODEL_PARAMVALUE(VT_gtos)) * xtos * (ytos + 0.500000 * ptTeaP->rs_inf[Vi]) * (svolt - (ek));
  const ML_CalcType xitof = (CELLMODEL_PARAMVALUE(VT_gtof)) * xtof * ytof * (svolt - (ek));
  const ML_CalcType xito = xitos + xitof;
  const ML_CalcType xica = 2.00000 * (CELLMODEL_PARAMVALUE(VT_wca)) * jca;
  const ML_CalcType xikr =
      (CELLMODEL_PARAMVALUE(VT_gkr)) * sqrt((K_o) / 5.40000) * xr * ptTeaP->rg[Vi] * (svolt - (ek));

  // const ML_CalcType xikr =  (CELLMODEL_PARAMVALUE(VT_gkr))* pow(((CELLMODEL_PARAMVALUE(VT_K_o))/5.40000), 1.0 / 2)*xr*ptTeaP->rg[Vi]*(svolt - (CELLMODEL_PARAMVALUE(VT_ek)));
  const ML_CalcType prNaK = (CELLMODEL_PARAMVALUE(VT_prNaK));
  const ML_CalcType eks =
      (1.00000 / (FonRT)) * (log((((K_o) + (prNaK) * (Na_o)) / ((CELLMODEL_PARAMVALUE(VT_K_i)) + (prNaK) * Na_i))));

  // const ML_CalcType eks =  (1.00000/(CELLMODEL_PARAMVALUE(VT_FonRT)))*(log((((CELLMODEL_PARAMVALUE(VT_K_o))+ (CELLMODEL_PARAMVALUE(VT_prNaK))*(CELLMODEL_PARAMVALUE(VT_Na_o)))/((CELLMODEL_PARAMVALUE(VT_K_i))+
  // (CELLMODEL_PARAMVALUE(VT_prNaK))*Na_i))));
  const ML_CalcType gksx =
      1.00000 + 0.800000 / (1.00000 + ((0.500000 / Ca_i) * (0.500000 / Ca_i) * (0.500000 / Ca_i)));

  // const ML_CalcType gksx = 1.00000+0.800000/(1.00000+(pow((0.500000/Ca_i), 3.00000)));
  const ML_CalcType xiks = (CELLMODEL_PARAMVALUE(VT_gks)) * gksx * xs1 * xs2 * (svolt - eks);

  // calculating rates part
  Ca_JSR += HT * ((Ca_NSR - Ca_JSR) / (CELLMODEL_PARAMVALUE(VT_taua)));
  xh += HT * (ptTeaP->ah[Vi] * (1.00000 - xh) - ptTeaP->bh[Vi] * xh);
  xj += HT * (ptTeaP->aj[Vi] * (1.00000 - xj) - ptTeaP->bj[Vi] * xj);
  xm += HT * (ptTeaP->am[Vi] * (1.00000 - xm) - ptTeaP->bm[Vi] * xm);
  checkGatingVariable(xm);
  xs1 += HT * ((ptTeaP->xs1ss[Vi] - xs1) / ptTeaP->tauxs1[Vi]);
  xr += HT * ((ptTeaP->xkrinf[Vi] - xr) * dtaukr);
  xs2 += HT * ((ptTeaP->xs1ss[Vi] - xs2) / (ptTeaP->tauxs2[Vi]));
  xtos += HT * ((xtos_inf - xtos) / (ptTeaP->txs[Vi]));
  xtof += HT * ((xtos_inf - xtof) / (ptTeaP->txf[Vi]));

  //    c2 += HT*( ( beta*c1+ k5*xi2ca+ k5t*xi2ba) -  (k6+k6t+alpha)*c2);
  //    xi2ca += HT*( ( k3*xi1ca+ k6*c2) -  (k5+k4)*xi2ca);
  //    xi2ba += HT*( ( k3t*xi1ba+ k6t*c2) -  (k5t+k4t)*xi2ba);
  //    c1 += HT*( ( alpha*c2+ (CELLMODEL_PARAMVALUE(VT_k2))*xi1ca+ (CELLMODEL_PARAMVALUE(VT_k2t))*xi1ba+ (CELLMODEL_PARAMVALUE(VT_r2))*po) -
  //  (beta+(CELLMODEL_PARAMVALUE(VT_r1))+(CELLMODEL_PARAMVALUE(VT_k1t))+k1)*c1);
  //    xi1ca += HT*( ( k1*c1+ k4*xi2ca+ s1*po) -  (k3+(CELLMODEL_PARAMVALUE(VT_k2))+s2)*xi1ca);
  //    xi1ba += HT*( ( (CELLMODEL_PARAMVALUE(VT_k1t))*c1+ k4t*xi2ba+ (CELLMODEL_PARAMVALUE(VT_s1t))*po) -  (k3t+(CELLMODEL_PARAMVALUE(VT_k2t))+(CELLMODEL_PARAMVALUE(VT_s2t)))*xi1ba);
  ytos += HT * ((ytos_inf - ytos) / (ptTeaP->tys[Vi]));
  ytof += HT * ((ytos_inf - ytof) / (ptTeaP->tyf[Vi]));
  Ca_i += HT * (dciib * (((jd - jup) + jleak) - xbi));
  tropi += HT * (xbi);
  trops += HT * (xbs);
  Ca_NSR += HT * (dCa_JSR);
  xir +=
      HT * (spark_rate * Qr - (xir * (1.00000 - ((CELLMODEL_PARAMVALUE(VT_taur)) * dCa_JSR) / Ca_NSR)) / (CELLMODEL_PARAMVALUE(VT_taur)));
  Ca_dyad += HT * (xiryr - (Ca_dyad - Ca_submem) / (CELLMODEL_PARAMVALUE(VT_taups)));
  Ca_submem += HT * (dcsib * (50.0000 * (((xir - jd) - jca) + jNaCa) - xbs));
  Na_i += HT * (-(xina + 3.00000 * xiNaK + 3.00000 * xiNaCa) / ((CELLMODEL_PARAMVALUE(VT_wca)) * 1000.00));


#ifdef RUNGE_KUTTA

  int NDERIVS   = 4;                                    // number of derivatives/accuracy of Markov chain
  ML_CalcType h = HT/(NDERIVS);

  float m1, m2, n1, n2, o1, o2, u1, u2, v1, v2, w1, w2; // derivatives for each variable at beginning (1) and end (2) of
                                                        // time step
  //    float l1, l2, j1, j2, j3;
  int i;
  for (i = 0; i < NDERIVS; i++) {
    //  l1=( xiryr - (Ca_dyad - Ca_submem)/(CELLMODEL_PARAMVALUE(VT_taups)));
    //  l2=( (xiryr) - ((Ca_dyad+ h/2*(l1)) - (Ca_submem+ h/2*(j2)))/(CELLMODEL_PARAMVALUE(VT_taups)));

    //  j1=(  dcsib*( 50.0000*(((xir - jd) - jca)+jNaCa) - xbs));
    //  j3=(  spark_rate*Qr - ( xir*(1.00000 - ( (CELLMODEL_PARAMVALUE(VT_taur))*dCa_JSR)/Ca_NSR))/(CELLMODEL_PARAMVALUE(VT_taur)));
    //  j2=(  dcsib*( 50.0000*((((xir+ h/2*j3) - jd) - jca)+jNaCa) - xbs));

    m1 = ( (k1*c1+ k4*xi2ca+ s1*po) -  (k3+(CELLMODEL_PARAMVALUE(VT_k2))+s2)*xi1ca);
    m2 =     ( (k1*(c1+ h/2*o2)+ k4*(xi2ca+h/2*v2)+ s1*po) -  (k3+(CELLMODEL_PARAMVALUE(VT_k2))+s2)*(xi1ca+h/2*m1));

    n1 = ( ( (CELLMODEL_PARAMVALUE(VT_k1t))*c1+ k4t*xi2ba+ (CELLMODEL_PARAMVALUE(VT_s1t))*po) -  (k3t+(CELLMODEL_PARAMVALUE(VT_k2t))+(CELLMODEL_PARAMVALUE(VT_s2t)))*xi1ba);
    n2 =
      ( ( (CELLMODEL_PARAMVALUE(VT_k1t))*(c1+ h/2*o2)+ k4t*(xi2ba+h/2*w2)+ (CELLMODEL_PARAMVALUE(VT_s1t))*po) -  (k3t+(CELLMODEL_PARAMVALUE(VT_k2t))+(CELLMODEL_PARAMVALUE(VT_s2t)))*
        (xi1ba+ h/2*n1));

    o1 = ( (alpha*c2+ (CELLMODEL_PARAMVALUE(VT_k2))*xi1ca+ (CELLMODEL_PARAMVALUE(VT_k2t))*xi1ba+ (CELLMODEL_PARAMVALUE(VT_r2))*po) -  (beta+(CELLMODEL_PARAMVALUE(VT_r1))+(CELLMODEL_PARAMVALUE(VT_k1t))+k1)*c1);
    o2 =
      ( (alpha*(c2+ h/2*u2)+ (CELLMODEL_PARAMVALUE(VT_k2))*(xi1ca+ h/2*m2)+ (CELLMODEL_PARAMVALUE(VT_k2t))*(xi1ba+ h/2*n2)+ (CELLMODEL_PARAMVALUE(VT_r2))*po) -
        (beta+(CELLMODEL_PARAMVALUE(VT_r1))+(CELLMODEL_PARAMVALUE(VT_k1t))+k1)*(c1+ h/2*o1));

    u1 = ( (beta*c1+ k5*xi2ca+ k5t*xi2ba) -  (k6+k6t+alpha)*c2);
    u2 =     ( (beta*(c1+ h/2*o2)+ k5*(xi2ca+ h/2*v2)+ k5t*(xi2ba+ h/2*w2)) -  (k6+k6t+alpha)*(c2+ h/2*u1));

    v1 = ( (k3*xi1ca+ k6*c2) -  (k5+k4)*xi2ca);
    v2 = ( (k3*(xi1ca+ h/2*m2)+ k6*(c2+ h/2*u2)) -  (k5+k4)*(xi2ca+ h/2*v1));

    w1 =     ( (k3t*xi1ba+ k6t*c2) -  (k5t+k4t)*xi2ba);
    w2 =     ( (k3t*(xi1ba+ h/2*n2)+ k6t*(c2+ h/2*u2)) -  (k5t+k4t)*(xi2ba+ h/2*w2));

    //  Ca_dyad += h*l2;
    //  Ca_submem += h*j2;
    xi1ca += h*m2;
    xi1ba += h*n2;
    c1    += h*o2;
    c2    += h*u2;
    xi2ca += h*v2;
    xi2ba += h*w2;
  }

#else // ifdef RUNGE_KUTTA

  //    Ca_dyad += HT*( xiryr - (Ca_dyad - Ca_submem)/(CELLMODEL_PARAMVALUE(VT_taups)));
  //    Ca_submem += HT*(  dcsib*( 50.0000*(((xir - jd) - jca)+jNaCa) - xbs));
  xi1ca += HT * ((k1 * c1 + k4 * xi2ca + s1 * po) - (k3 + (CELLMODEL_PARAMVALUE(VT_k2)) + s2) * xi1ca);
  xi1ba += HT * (((CELLMODEL_PARAMVALUE(VT_k1t)) * c1 + k4t * xi2ba + (CELLMODEL_PARAMVALUE(VT_s1t)) * po) -
                 (k3t + (CELLMODEL_PARAMVALUE(VT_k2t)) + (CELLMODEL_PARAMVALUE(VT_s2t))) * xi1ba);
  c1 += HT * ((alpha * c2 + (CELLMODEL_PARAMVALUE(VT_k2)) * xi1ca + (CELLMODEL_PARAMVALUE(VT_k2t)) * xi1ba + (CELLMODEL_PARAMVALUE(VT_r2)) * po) -
              (beta + (CELLMODEL_PARAMVALUE(VT_r1)) + (CELLMODEL_PARAMVALUE(VT_k1t)) + k1) * c1);
  c2 += HT * ((beta * c1 + k5 * xi2ca + k5t * xi2ba) - (k6 + k6t + alpha) * c2);
  xi2ca += HT * ((k3 * xi1ca + k6 * c2) - (k5 + k4) * xi2ca);
  xi2ba += HT * ((k3t * xi1ba + k6t * c2) - (k5t + k4t) * xi2ba);
#endif // ifdef RUNGE_KUTTA

  // convert i_external from nA/cell to nA/nF; C_m = 3.1 e-1 nF
  // i_external = i_external/(0.31);

  const ML_CalcType I_tot =
      -i_external + xina + xik1 + xikr + xiks + xitof + xitos + xiNaCa + xica + xiNaK;

  // cerr<< i_external << endl;
  return tinc * (-I_tot);
} // MahajanEtAl::Calc

void MahajanEtAl::Print(ostream &tempstr, double tArg, ML_CalcType V) {
  tempstr << tArg << ' ' << V << ' ' << xm << ' ' << xh << ' ' << xj << ' ' << Ca_dyad << ' ' << c1
          << ' ' << c2 << ' ' << xi1ca << ' ' << xi1ba <<
          ' ' << xi2ca
          << ' ' << xi2ba << ' ' << xr << ' ' << Ca_i << ' ' << xs1 << ' ' << xs2 << ' ' << xtos
          << ' ' << ytos << ' ' << xtof << ' ' << ytof
          << ' ' << Na_i << ' ' << Ca_submem << ' ' << Ca_NSR << ' ' << Ca_JSR << ' ' << xir << ' '
          << tropi << ' ' << trops;
}

void MahajanEtAl::LongPrint(ostream &tempstr, double tArg, ML_CalcType V) {
  Print(tempstr, tArg, V);
  const ML_CalcType svolt = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + svolt) + .5);

  // const ML_CalcType taukr = ptTeaP->taukr[Vi];
  // const ML_CalcType taukr = 1.00000/(ptTeaP->xkrv1[Vi]+ptTeaP->xkrv2[Vi]);
  // const ML_CalcType xs2ss = ptTeaP->xs1ss[Vi];
  // const ML_CalcType tauxs2 =  ptTeaP->tauxs2[Vi];
  // const ML_CalcType tauxs2 =  4.00000*ptTeaP->tauxs1[Vi];
  // const ML_CalcType xtos_inf = ptTeaP->xtos_inf[Vi];
  // const ML_CalcType xtos_inf = 1.00000/(1.00000+(exp(ptTeaP->rt1[Vi])));
  // const ML_CalcType txs = ptTeaP->txs[Vi];
  // const ML_CalcType txs = 9.00000/(1.00000+(exp((- ptTeaP->rt1[Vi]))))+0.500000;
  // const ML_CalcType xtof_inf = ptTeaP->xtos_inf[Vi];
  // const ML_CalcType txf =  ptTeaP->txf[Vi];
  // const ML_CalcType txf =  3.50000*(exp(ptTeaP->rt4[Vi]))+1.50000;
  const ML_CalcType alpha = ptTeaP->alpha[Vi];

  // const ML_CalcType alpha = ptTeaP->poinf[Vi]/(CELLMODEL_PARAMVALUE(VT_taupo));
  const ML_CalcType beta = ptTeaP->beta[Vi];

  // const ML_CalcType beta = (1.00000 - ptTeaP->poinf[Vi])/(CELLMODEL_PARAMVALUE(VT_taupo));
  const ML_CalcType fca =
      1.00000 / (1.00000 + ((1 / Ca_dyad) * (1 / Ca_dyad) * (1 / Ca_dyad) * CELLMODEL_PARAMVALUE(VT_cat3)));

  // const ML_CalcType fca = 1.00000/(1.00000+(pow(((CELLMODEL_PARAMVALUE(VT_cat))/Ca_dyad), 3.00000)));
  const ML_CalcType cpt = (CELLMODEL_PARAMVALUE(VT_cpt));
  ML_CalcType tau_ca =
      (CELLMODEL_PARAMVALUE(VT_tca)) / (1.00000 + ((Ca_dyad) * (Ca_dyad) * (Ca_dyad) * (Ca_dyad) * CELLMODEL_PARAMVALUE(VT_cptm4))) +
      0.100000;

  // const ML_CalcType tau_ca = (CELLMODEL_PARAMVALUE(VT_tca))/(1.00000+(pow((Ca_dyad/(CELLMODEL_PARAMVALUE(VT_cpt))), 4.00000)))+0.100000;
  const ML_CalcType dtauca = 1.0 / ((ptTeaP->recov[Vi] - tau_ca) * ptTeaP->Pr[Vi] + tau_ca);
  const ML_CalcType k6 = (fca * ptTeaP->Ps[Vi]) * dtauca;
  const ML_CalcType k5 = (1.00000 - ptTeaP->Ps[Vi]) * dtauca;
  const ML_CalcType dtauba = 1 / (ptTeaP->tauba[Vi]);

  // const ML_CalcType tauba =  (ptTeaP->recov[Vi] - 450.000)*ptTeaP->Pr[Vi]+450.000;
  const ML_CalcType k6t = ptTeaP->Ps[Vi] * dtauba;
  const ML_CalcType k5t = (1.00000 - ptTeaP->Ps[Vi]) * dtauba;
  const ML_CalcType k3 = ptTeaP->k3[Vi];

  // const ML_CalcType k3 = (1.00000 - ptTeaP->poi[Vi])/(CELLMODEL_PARAMVALUE(VT_tau3));
  const ML_CalcType k1 = 0.0241680 * fca;
  const ML_CalcType k4 = (((((k3 * alpha) / beta) * k1) / (CELLMODEL_PARAMVALUE(VT_k2))) * k5) / k6;
  const ML_CalcType k3t = k3;
  const ML_CalcType k4t = (((((k3t * alpha) / beta) * (CELLMODEL_PARAMVALUE(VT_k1t))) / (CELLMODEL_PARAMVALUE(VT_k2t))) * k5t) / k6t;
  const ML_CalcType po = (((((1.00000 - xi1ca) - xi2ca) - xi1ba) - xi2ba) - c1) - c2;
  const ML_CalcType s1 = 0.0182688 * fca;
  const ML_CalcType s2 = s1 * CELLMODEL_PARAMVALUE(VT_r1r2k2) / k1;

  // const ML_CalcType s2 = ( (( s1*(CELLMODEL_PARAMVALUE(VT_r1)))/(CELLMODEL_PARAMVALUE(VT_r2)))*(CELLMODEL_PARAMVALUE(VT_k2)))/k1;
  // const ML_CalcType ytos_inf = ptTeaP->ytos_inf[Vi];
  // const ML_CalcType ytos_inf = 1.00000/(1.00000+(exp(ptTeaP->rt2[Vi])));
  // const ML_CalcType tys = ptTeaP->tys[Vi];
  // const ML_CalcType tys = 3000.00/(1.00000+(exp(ptTeaP->rt3[Vi])))+30.0000;
  // const ML_CalcType ytof_inf = ptTeaP->ytos_inf[Vi];
  // const ML_CalcType tyf = ptTeaP->tyf[Vi];
  // const ML_CalcType tyf = 20.0000/(1.00000+(exp(ptTeaP->rt5[Vi])))+20.0000;
  const ML_CalcType kmem = (CELLMODEL_PARAMVALUE(VT_kmem));
  const ML_CalcType cup2 = (CELLMODEL_PARAMVALUE(VT_cup)) * (CELLMODEL_PARAMVALUE(VT_cup));
  const ML_CalcType jup = ((CELLMODEL_PARAMVALUE(VT_vup)) * Ca_i * Ca_i) / (Ca_i * Ca_i + (cup2));

  // const ML_CalcType jup = ( (CELLMODEL_PARAMVALUE(VT_vup))*Ca_i*Ca_i)/( Ca_i*Ca_i+ (CELLMODEL_PARAMVALUE(VT_cup))*(CELLMODEL_PARAMVALUE(VT_cup)));
  const ML_CalcType kj2 = (CELLMODEL_PARAMVALUE(VT_kj)) * (CELLMODEL_PARAMVALUE(VT_kj));
  const ML_CalcType jleak =
      (((CELLMODEL_PARAMVALUE(VT_gleak)) * Ca_NSR * Ca_NSR) / (Ca_NSR * Ca_NSR + (kj2))) * (Ca_NSR * 16.6670 - Ca_i);

  // const ML_CalcType jleak =  (( (CELLMODEL_PARAMVALUE(VT_gleak))*Ca_NSR*Ca_NSR)/( Ca_NSR*Ca_NSR+ (CELLMODEL_PARAMVALUE(VT_kj))*(CELLMODEL_PARAMVALUE(VT_kj))))*(
  // Ca_NSR*16.6670 - Ca_i);
  const ML_CalcType xkcal = (CELLMODEL_PARAMVALUE(VT_xkcal));
  const ML_CalcType bpxi = ((CELLMODEL_PARAMVALUE(VT_bcal)) * (xkcal)) / (((xkcal) + Ca_i) * ((xkcal) + Ca_i));

  // const ML_CalcType bpxi = ( (CELLMODEL_PARAMVALUE(VT_bcal))*(CELLMODEL_PARAMVALUE(VT_xkcal)))/( ((CELLMODEL_PARAMVALUE(VT_xkcal))+Ca_i)*((CELLMODEL_PARAMVALUE(VT_xkcal))+Ca_i));
  const ML_CalcType srkd = (CELLMODEL_PARAMVALUE(VT_srkd));
  const ML_CalcType spxi = ((CELLMODEL_PARAMVALUE(VT_srmax)) * (srkd)) / (((srkd) + Ca_i) * ((srkd) + Ca_i));

  // const ML_CalcType spxi = ( (CELLMODEL_PARAMVALUE(VT_srmax))*(CELLMODEL_PARAMVALUE(VT_srkd)))/( ((CELLMODEL_PARAMVALUE(VT_srkd))+Ca_i)*((CELLMODEL_PARAMVALUE(VT_srkd))+Ca_i));
  const ML_CalcType mempxi = ((CELLMODEL_PARAMVALUE(VT_bmem)) * (kmem)) / (((kmem) + Ca_i) * ((kmem) + Ca_i));
  const ML_CalcType ksar = (CELLMODEL_PARAMVALUE(VT_ksar));
  const ML_CalcType sarpxi = ((CELLMODEL_PARAMVALUE(VT_bsar)) * (ksar)) / (((ksar) + Ca_i) * ((ksar) + Ca_i));

  // const ML_CalcType sarpxi = ( (CELLMODEL_PARAMVALUE(VT_bsar))*(CELLMODEL_PARAMVALUE(VT_ksar)))/( ((CELLMODEL_PARAMVALUE(VT_ksar))+Ca_i)*((CELLMODEL_PARAMVALUE(VT_ksar))+Ca_i));
  const ML_CalcType dciib = 1.00000 / (1.00000 + bpxi + spxi + mempxi + sarpxi);
  const ML_CalcType xkon = (CELLMODEL_PARAMVALUE(VT_xkon));
  const ML_CalcType btrop = (CELLMODEL_PARAMVALUE(VT_btrop));
  const ML_CalcType xkoff = (CELLMODEL_PARAMVALUE(VT_xkoff));
  const ML_CalcType xbi = (xkon) * Ca_i * ((btrop) - tropi) - (xkoff) * tropi;
  const ML_CalcType jd = (Ca_submem - Ca_i) / (CELLMODEL_PARAMVALUE(VT_taud));
  const ML_CalcType xbs = (xkon) * Ca_submem * ((btrop) - trops) - (xkoff) * trops;
  const ML_CalcType dCa_JSR = (-xir + jup) - jleak;
  const ML_CalcType cstar = (CELLMODEL_PARAMVALUE(VT_cstar));
  const ML_CalcType Qr0 = (Ca_JSR > 50.0000 && Ca_JSR < (cstar) ?
                           (Ca_JSR - 50.0000) / 1.00000 : Ca_JSR >= (cstar) ?
                                                          (CELLMODEL_PARAMVALUE(VT_av)) * Ca_JSR + (CELLMODEL_PARAMVALUE(VT_bv))
                                                                            : 0.00000);
  const ML_CalcType Qr = (Ca_NSR * Qr0) / (cstar);
  const ML_CalcType csm = Ca_submem / 1000.00;
  const ML_CalcType FonRT = (CELLMODEL_PARAMVALUE(VT_FonRT));
  const ML_CalcType F = (CELLMODEL_PARAMVALUE(VT_F));
  const ML_CalcType pca = (CELLMODEL_PARAMVALUE(VT_pca));
  const ML_CalcType Ca_o = (CELLMODEL_PARAMVALUE(VT_Ca_o));
  const ML_CalcType rxa =
      ((fabs(svolt * 2.0 * (FonRT))) <
       0.00100000 ? (4.00000 * (pca) * (F) * (FonRT) *
                     (csm * (exp(svolt * 2.0 * (FonRT))) - 0.341000 * (Ca_o))) /
                    (2.00000 * (FonRT)) : (4.00000 * (pca) * svolt * (F) * (FonRT) *
                                           (csm * (exp(svolt * 2.0 * (FonRT))) -
                                            0.341000 * (Ca_o))) /
                                          ((exp(svolt * 2.0 * (FonRT))) - 1.00000));

  // const ML_CalcType rxa =  ((fabs(svolt*2.0*(CELLMODEL_PARAMVALUE(VT_FonRT))))<0.00100000 ? (
  // 4.00000*(CELLMODEL_PARAMVALUE(VT_pca))*(CELLMODEL_PARAMVALUE(VT_F))*(CELLMODEL_PARAMVALUE(VT_FonRT))*( csm*(exp(svolt*2.0*(CELLMODEL_PARAMVALUE(VT_FonRT)))) -  0.341000*(CELLMODEL_PARAMVALUE(VT_Ca_o))))/(
  // 2.00000*(CELLMODEL_PARAMVALUE(VT_FonRT))) :( 4.00000*(CELLMODEL_PARAMVALUE(VT_pca))*svolt*(CELLMODEL_PARAMVALUE(VT_F))*(CELLMODEL_PARAMVALUE(VT_FonRT))*( csm*(exp(svolt*2.0*(CELLMODEL_PARAMVALUE(VT_FonRT)))) -
  //  0.341000*(CELLMODEL_PARAMVALUE(VT_Ca_o))))/((exp(svolt*2.0*(CELLMODEL_PARAMVALUE(VT_FonRT)))) - 1.00000));
  // const ML_CalcType rxa = ((fabs(ptTeaP->za[Vi]))<0.00100000 ? ( 4.00000*(CELLMODEL_PARAMVALUE(VT_pca))*(CELLMODEL_PARAMVALUE(VT_F))*(CELLMODEL_PARAMVALUE(VT_FonRT))*(
  // csm*(exp(ptTeaP->za[Vi])) -  0.341000*(CELLMODEL_PARAMVALUE(VT_Ca_o))))/( 2.00000*(CELLMODEL_PARAMVALUE(VT_FonRT))) : (
  // 4.00000*(CELLMODEL_PARAMVALUE(VT_pca))*svolt*(CELLMODEL_PARAMVALUE(VT_F))*(CELLMODEL_PARAMVALUE(VT_FonRT))*( csm*(exp(ptTeaP->za[Vi])) -
  //  0.341000*(CELLMODEL_PARAMVALUE(VT_Ca_o))))/((exp(ptTeaP->za[Vi])) - 1.00000));
  const ML_CalcType spark_rate = ((CELLMODEL_PARAMVALUE(VT_gryr)) / 1.00000) * po * (fabs(rxa)) * ptTeaP->sparkV[Vi];
  const ML_CalcType xirp = (((po * Qr * (fabs(rxa)) * ptTeaP->comp[Vi])));

  // const ML_CalcType xirp = ( (( po*Qr*(fabs(rxa))*(CELLMODEL_PARAMVALUE(VT_gbarsr)))/1.00000)*(exp(( -
  // (CELLMODEL_PARAMVALUE(VT_ax))*(svolt+30.0000)))))/(1.00000+(exp(( - (CELLMODEL_PARAMVALUE(VT_ax))*(svolt+30.0000)))));
  const ML_CalcType xicap = po * (CELLMODEL_PARAMVALUE(VT_gdyad)) * (fabs(rxa));
  const ML_CalcType xiryr = xirp + xicap;
  const ML_CalcType jca = (CELLMODEL_PARAMVALUE(VT_gca)) * po * rxa;
  const ML_CalcType aloss =
      1.00000 / (1.00000 + ((1 / Ca_submem) * 1 / Ca_submem) * 1 / Ca_submem * CELLMODEL_PARAMVALUE(VT_xkdna3));

  // const ML_CalcType aloss = 1.00000/(1.00000+(pow(((CELLMODEL_PARAMVALUE(VT_xkdna))/Ca_submem), 3.00000)));
  const ML_CalcType Na_i3 = Na_i * Na_i * Na_i;
  const ML_CalcType zw3 =
      (Na_i3) * (Ca_o) * (exp((svolt * 0.350000 * (FonRT)))) - csm * ptTeaP->comp2[Vi];

  // const ML_CalcType zw3 =  (pow(Na_i, 3.00000))*(CELLMODEL_PARAMVALUE(VT_Ca_o))*(exp(( svolt*0.350000*(CELLMODEL_PARAMVALUE(VT_FonRT))))) -
  //  (pow((CELLMODEL_PARAMVALUE(VT_Na_o)), 3.00000))*csm*(exp(( svolt*(0.350000 - 1.00000)*(CELLMODEL_PARAMVALUE(VT_FonRT)))));
  const ML_CalcType xmnao3 = (CELLMODEL_PARAMVALUE(VT_xmnao)) * (CELLMODEL_PARAMVALUE(VT_xmnao)) * (CELLMODEL_PARAMVALUE(VT_xmnao));
  const ML_CalcType yz1 = (CELLMODEL_PARAMVALUE(VT_xmcao)) * (Na_i3) + ((xmnao3)) * csm;

  // const ML_CalcType yz1 =  (CELLMODEL_PARAMVALUE(VT_xmcao))*(pow(Na_i, 3.00000))+ (pow((CELLMODEL_PARAMVALUE(VT_xmnao)), 3.00000))*csm;
  const ML_CalcType yz2 = CELLMODEL_PARAMVALUE(VT_xmnai3) * (Ca_o) * (1.00000 + csm / (CELLMODEL_PARAMVALUE(VT_xmcai)));

  // const ML_CalcType yz2 =  (pow((CELLMODEL_PARAMVALUE(VT_xmnai)), 3.00000))*(CELLMODEL_PARAMVALUE(VT_Ca_o))*(1.00000+csm/(CELLMODEL_PARAMVALUE(VT_xmcai)));
  const ML_CalcType Na_o = (CELLMODEL_PARAMVALUE(VT_Na_o));
  const ML_CalcType Na_o3 = (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o));
  const ML_CalcType yz3 = (CELLMODEL_PARAMVALUE(VT_xmcai)) * ((Na_o3)) * (1.00000 + ((Na_i3) / CELLMODEL_PARAMVALUE(VT_xmnai3)));

  // const ML_CalcType yz3 =  (CELLMODEL_PARAMVALUE(VT_xmcai))*(pow((CELLMODEL_PARAMVALUE(VT_Na_o)), 3.00000))*(1.00000+(pow((Na_i/(CELLMODEL_PARAMVALUE(VT_xmnai))), 3.00000)));
  const ML_CalcType yz4 = (Na_i3) * (Ca_o) + ((Na_o3)) * csm;

  // const ML_CalcType yz4 =  (pow(Na_i, 3.00000))*(CELLMODEL_PARAMVALUE(VT_Ca_o))+ (pow((CELLMODEL_PARAMVALUE(VT_Na_o)), 3.00000))*csm;
  const ML_CalcType zw8 = yz1 + yz2 + yz3 + yz4;
  const ML_CalcType jNaCa = ((CELLMODEL_PARAMVALUE(VT_gNaCa)) * aloss * zw3) / (ptTeaP->zw4[Vi] * zw8);
  const ML_CalcType bpxs =
      ((CELLMODEL_PARAMVALUE(VT_bcal)) * (xkcal)) / (((xkcal) + Ca_submem) * ((xkcal) + Ca_submem));

  // const ML_CalcType bpxs = ( (CELLMODEL_PARAMVALUE(VT_bcal))*(CELLMODEL_PARAMVALUE(VT_xkcal)))/( ((CELLMODEL_PARAMVALUE(VT_xkcal))+Ca_submem)*((CELLMODEL_PARAMVALUE(VT_xkcal))+Ca_submem));
  const ML_CalcType spxs = ((CELLMODEL_PARAMVALUE(VT_srmax)) * (srkd)) / (((srkd) + Ca_submem) * ((srkd) + Ca_submem));

  // const ML_CalcType spxs = ( (CELLMODEL_PARAMVALUE(VT_srmax))*(CELLMODEL_PARAMVALUE(VT_srkd)))/( ((CELLMODEL_PARAMVALUE(VT_srkd))+Ca_submem)*((CELLMODEL_PARAMVALUE(VT_srkd))+Ca_submem));
  const ML_CalcType mempxs =
      ((CELLMODEL_PARAMVALUE(VT_bmem)) * (kmem)) / (((kmem) + Ca_submem) * ((kmem) + Ca_submem));

  // const ML_CalcType mempxs = ( (CELLMODEL_PARAMVALUE(VT_bmem))*(CELLMODEL_PARAMVALUE(VT_kmem)))/( ((CELLMODEL_PARAMVALUE(VT_kmem))+Ca_submem)*((CELLMODEL_PARAMVALUE(VT_kmem))+Ca_submem));
  const ML_CalcType sarpxs =
      ((CELLMODEL_PARAMVALUE(VT_bsar)) * (ksar)) / (((ksar) + Ca_submem) * ((ksar) + Ca_submem));

  // const ML_CalcType sarpxs = ( (CELLMODEL_PARAMVALUE(VT_bsar))*(CELLMODEL_PARAMVALUE(VT_ksar)))/( ((CELLMODEL_PARAMVALUE(VT_ksar))+Ca_submem)*((CELLMODEL_PARAMVALUE(VT_ksar))+Ca_submem));
  const ML_CalcType dcsib = 1.00000 / (1.00000 + bpxs + spxs + mempxs + sarpxs);
  const ML_CalcType K_o = (CELLMODEL_PARAMVALUE(VT_K_o));
  const ML_CalcType xiNaK =
      ((((CELLMODEL_PARAMVALUE(VT_gNaK)) * ptTeaP->fNaK[Vi] * Na_i) / (Na_i + (CELLMODEL_PARAMVALUE(VT_xkmnai)))) * (K_o)) /
      ((K_o) + (CELLMODEL_PARAMVALUE(VT_xkmko)));

  // const ML_CalcType xiNaK = ( ((
  // (CELLMODEL_PARAMVALUE(VT_gNaK))*ptTeaP->fNaK[Vi]*Na_i)/(Na_i+(CELLMODEL_PARAMVALUE(VT_xkmnai))))*(CELLMODEL_PARAMVALUE(VT_K_o)))/((CELLMODEL_PARAMVALUE(VT_K_o))+(CELLMODEL_PARAMVALUE(VT_xkmko)));
  const ML_CalcType xiNaCa = (CELLMODEL_PARAMVALUE(VT_wca)) * jNaCa;
  const ML_CalcType ena = (1.00000 / (FonRT)) * (log(((Na_o) / Na_i)));

  // const ML_CalcType ena =  (1.00000/(CELLMODEL_PARAMVALUE(VT_FonRT)))*(log(((CELLMODEL_PARAMVALUE(VT_Na_o))/Na_i)));
  const ML_CalcType xina = (CELLMODEL_PARAMVALUE(VT_gna)) * xh * xj * xm * xm * xm * (svolt - ena);

  // const ML_CalcType xkin = ptTeaP->xkin[Vi];
  // const ML_CalcType xkin = ptTeaP->aki[Vi]/(ptTeaP->aki[Vi]+ptTeaP->bki[Vi]);
  const ML_CalcType ek = (CELLMODEL_PARAMVALUE(VT_ek));
  const ML_CalcType xik1 = (CELLMODEL_PARAMVALUE(VT_gkix)) * sqrt((K_o) / 5.40000) * ptTeaP->xkin[Vi] * (svolt - (ek));

  // const ML_CalcType xik1 =  (CELLMODEL_PARAMVALUE(VT_gkix))* pow(((CELLMODEL_PARAMVALUE(VT_K_o))/5.40000), 1.0 / 2)*xkin*(svolt - (CELLMODEL_PARAMVALUE(VT_ek)));
  // const ML_CalcType rs_inf = ptTeaP->rs_inf[Vi];
  // const ML_CalcType rs_inf = 1.00000/(1.00000+(exp(ptTeaP->rt2[Vi])));
  const ML_CalcType xitos =
      (CELLMODEL_PARAMVALUE(VT_gtos)) * xtos * (ytos + 0.500000 * ptTeaP->rs_inf[Vi]) * (svolt - (ek));
  const ML_CalcType xitof = (CELLMODEL_PARAMVALUE(VT_gtof)) * xtof * ytof * (svolt - (ek));
  const ML_CalcType xito = xitos + xitof;
  const ML_CalcType xica = 2.00000 * (CELLMODEL_PARAMVALUE(VT_wca)) * jca;
  const ML_CalcType xikr =
      (CELLMODEL_PARAMVALUE(VT_gkr)) * sqrt((K_o) / 5.40000) * xr * ptTeaP->rg[Vi] * (svolt - (ek));

  // const ML_CalcType xikr =  (CELLMODEL_PARAMVALUE(VT_gkr))* pow(((CELLMODEL_PARAMVALUE(VT_K_o))/5.40000), 1.0 / 2)*xr*ptTeaP->rg[Vi]*(svolt - (CELLMODEL_PARAMVALUE(VT_ek)));
  const ML_CalcType prNaK = (CELLMODEL_PARAMVALUE(VT_prNaK));
  const ML_CalcType eks =
      (1.00000 / (FonRT)) * (log((((K_o) + (prNaK) * (Na_o)) / ((CELLMODEL_PARAMVALUE(VT_K_i)) + (prNaK) * Na_i))));

  // const ML_CalcType eks =  (1.00000/(CELLMODEL_PARAMVALUE(VT_FonRT)))*(log((((CELLMODEL_PARAMVALUE(VT_K_o))+ (CELLMODEL_PARAMVALUE(VT_prNaK))*(CELLMODEL_PARAMVALUE(VT_Na_o)))/((CELLMODEL_PARAMVALUE(VT_K_i))+
  // (CELLMODEL_PARAMVALUE(VT_prNaK))*Na_i))));
  const ML_CalcType gksx =
      1.00000 + 0.800000 / (1.00000 + ((0.500000 / Ca_i) * (0.500000 / Ca_i) * (0.500000 / Ca_i)));

  // const ML_CalcType gksx = 1.00000+0.800000/(1.00000+(pow((0.500000/Ca_i), 3.00000)));
  const ML_CalcType xiks = (CELLMODEL_PARAMVALUE(VT_gks)) * gksx * xs1 * xs2 * (svolt - eks);


  const ML_CalcType I_mem = xina + xik1 + xikr + xiks + xitof + xitos + xiNaCa + xica + xiNaK;
  tempstr << ' ' << xina << ' ' << xik1 << ' ' << xikr << ' ' << xiks << ' ' << xitof << ' '
          << xitos << ' ' << xiNaCa << ' ' << xica <<
          ' '
          << xiNaK << ' ' << I_mem << ' ';
} // MahajanEtAl::LongPrint

void MahajanEtAl::GetParameterNames(vector<string> &getpara) {
  const int numpara = 25;
  const string ParaNames[numpara] =
      {"xm", "xh", "xj", "Ca_dyad", "c1", "c2", "xi1ca", "xi1ba",
       "xi2ca",
       "xi2ba", "xr",
       "Ca_i", "xs1",
       "xs2", "xtos",
       "ytos", "xtof", "ytof", "Na_i", "Ca_submem", "Ca_NSR", "Ca_JSR", "xir",
       "tropi",
       "trops"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void MahajanEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 10;
  const string ParaNames[numpara] =
      {"I_Na", "I_K1", "I_Kr", "I_Ks", "I_tof", "I_tos", "I_NaCa", "I_Ca", "I_NaK", "I_mem"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
