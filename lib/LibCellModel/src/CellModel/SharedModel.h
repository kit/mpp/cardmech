/**
 * SharedModel
 *  \author Manuel Ifland
 *  \date 15.03.2007
 *
 * Cellmodel to use shared currents (SharedObjects)
 */

#ifndef SharedModel_H
#define SharedModel_H
#ifdef osMac_Vec

# include <SharedModelParameters.h>

# undef HETERO
# undef v

# ifdef HETERO
#  define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_SharedModelParameters::a)
# else // ifdef HETERO
#  define CELLMODEL_PARAMVALUE(a) pIP->P[NS_SharedModelParameters::a].value
# endif // ifdef HETERO

// template<class T> class SharedModelBasis;

class SharedModel : public vbElphyModel<ML_CalcType>{
 private:
  int Vi;

 public:
  SharedModelParameters *pIP;

  // HandleManager<T> *HM;

  // memory to copy during set
  ML_CalcType Ca_i;
  ML_CalcType Array[ArraySize];
  ML_CalcType E_to, E_Na, E_K, E_Ks, E_Ca;
  ML_CalcType cICa, cICab, cINaCa, cINa, cINab, cIKb, cINaK, cIKr, cIKs, cIK1, cIto, cIpCa, cICaK, cIKatp;
  ML_CalcType iStim;

  // memory to copy during set

  ML_CalcType dV;


  int indexINa;
  int indexIto;
  int indexIKr;
  int indexIKs;
  int indexINab;
  int indexIKb;
  int indexIK1;
  int indexICa;
  int indexICab;
  int indexINaCa;
  int indexINaK;
  int indexIpCa;
  int indexIKatp;


  int numCurrents;

  // string *FN;
  // string *EV;

  //            string NaHandling, CaHandling, KHandling;
  SO_ionicCurrent<ML_CalcType> **shd;

  SO_NaConc_Handling<ML_CalcType> *NA;
  SO_CaConc_Handling<ML_CalcType> *CA;
  SO_KConc_Handling<ML_CalcType> *KA;

  SharedModel(SharedModelParameters *pp);
  ~SharedModel();
  virtual void CalcEQs(void);
  virtual void Init();

  // virtual inline int GetSize(void) {return
  // sizeof(SharedModel<T>)-sizeof(vbElphyModel<T>)-sizeof(SharedModelParameters<T> *);};
  virtual inline int GetSize(void) { return (&iStim-&Ca_i+1)*sizeof(ML_CalcType); }

  virtual inline ML_CalcType* GetBase(void) { return &Ca_i; }

  virtual inline ML_CalcType SurfaceToVolumeRatio() { return 0.0; }

  virtual inline ML_CalcType Volume() { return 2.064403e-13; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime(void) { return 0.001; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_init_Vm); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return 0.0; }

  virtual inline ML_CalcType GetNai() { return 0.0; }

  virtual inline ML_CalcType GetNao() { return 0.0; }

  virtual inline ML_CalcType GetKi() { return 0.0; }

  virtual inline ML_CalcType GetKo() { return 0.0; }
  virtual inline void        Set(vbElphyModel<ML_CalcType>& from);

  virtual inline SO_ionicCurrent<ML_CalcType>* GetSO_ionicCurrent(int index);
  virtual inline void                          SetCai(ML_CalcType val);
  virtual void                                 SetTinc(ML_CalcType tinc);
  virtual void                                 Print(ostream& tempstr, double t, ML_CalcType V);
  virtual void                                 LongPrint(ostream& tempstr, double t, ML_CalcType V);
  virtual void                                 GetParameterNames(vector<string>& getpara);
  virtual void                                 GetLongParameterNames(vector<string>& getpara);
  virtual inline unsigned char                 getSpeed(ML_CalcType adVm);
  virtual ML_CalcType                          Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                                                    ML_CalcType stretch = 1., int euler = 1);
}; // class SharedModel

#endif // ifdef osMac_Vec
#endif // ifndef SharedModel_H
