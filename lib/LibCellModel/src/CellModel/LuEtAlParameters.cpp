/*! \file LuEtAl.h
   \brief Implementation of electrophysiological model for describing hERG channels
   from Lu et al., J Phys 2001

   \author fs, CVRTI - University of Utah, USA
 */

#include <LuEtAlParameters.h>

LuEtAlParameters::LuEtAlParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void LuEtAlParameters::PrintParameters() {
  cout << "LuEtAlParameters:" << endl;
  for (int i = vtFirst; i < vtLast; i++)
    cout << P[i].name << " = " << P[i].value << endl;
}

void LuEtAlParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "LuEtAlParameters:init " << initFile << endl;
#endif // if KADEBUG

  // Initialization of the Parameters ...
  P[VT_Tx].name = "Tx";
  P[VT_Acap].name = "Acap";
  P[VT_Vmyo].name = "Vmyo";
  P[VT_Ko].name = "Ko";
  P[VT_Ki].name = "Ki";
  P[VT_C0Kr].name = "C0Kr";
  P[VT_C1Kr].name = "C1Kr";
  P[VT_C2Kr].name = "C2Kr";
  P[VT_OKr].name = "OKr";
  P[VT_IKr].name = "IKr";
  P[VT_G_C0Kr].name = "G_C0Kr";
  P[VT_G_C1Kr].name = "G_C1Kr";
  P[VT_G_C2Kr].name = "G_C2Kr";
  P[VT_G_OKr].name = "G_OKr";
  P[VT_G_IKr].name = "G_IKr";
  P[VT_Kra0a].name = "Kra0a";
  P[VT_Kra0z].name = "Kra0z";
  P[VT_Krb0a].name = "Krb0a";
  P[VT_Krb0z].name = "Krb0z";
  P[VT_Krfa].name = "Krfa";
  P[VT_Krfz].name = "Krfz";
  P[VT_Krba].name = "Krba";
  P[VT_Krbz].name = "Krbz";
  P[VT_Kra1a].name = "Kra1a";
  P[VT_Kra1z].name = "Kra1z";
  P[VT_Krb1a].name = "Krb1a";
  P[VT_Krb1z].name = "Krb1z";
  P[VT_Kraia].name = "Kraia";
  P[VT_Kraiz].name = "Kraiz";
  P[VT_Krbia].name = "Krbia";
  P[VT_Krbiz].name = "Krbiz";
  P[VT_Kracia].name = "Kracia";
  P[VT_Kraciz].name = "Kraciz";
  P[VT_Krbcia].name = "Krbcia";
  P[VT_Krbciz].name = "Krbciz";
  P[VT_Amp].name = "Amp";

  ParameterLoader EPL(initFile, EMT_LuEtAl);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
} // LuEtAlParameters::Init
