/*      File: DiFrancescoNoble.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
        Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
        send comments to dw@ibt.uka.de  */

#ifndef DIFRANCESCONOBLE
#define DIFRANCESCONOBLE

#include <DiFrancescoNobleParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_DiFrancescoNobleParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pDFNP->P[NS_DiFrancescoNobleParameters::a].value
#endif // ifdef HETERO

class DiFrancescoNoble : public vbElphyModel<ML_CalcType> {
public:
  DiFrancescoNobleParameters *pDFNP;
  ML_CalcType Ca_i, K_o;
  ML_CalcType Ca_up, Ca_rel;
  ML_CalcType y, x, h, f, f2, r, p;
  ML_CalcType Na_i, K_i;
  ML_CalcType m, d;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  DiFrancescoNoble(DiFrancescoNobleParameters *pp);

  ~DiFrancescoNoble();

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vol); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp);  /*500.0;*/ }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_Vm); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return K_o; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &Ca_i; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline void SetKo(ML_CalcType val) { K_o = val; }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual inline void
  SetNaiKiconc(ML_CalcType tinc, const ML_CalcType I_mNa, const ML_CalcType I_mK);

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                           int euler);

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class DiFrancescoNoble
#endif // ifndef DIFRANCESCONOBLE
