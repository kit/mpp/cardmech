/**@file Rice3.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <Rice3.h>

Rice3::Rice3(Rice3Parameters *pp) {
  r3p = pp;
  Init();
}

void Rice3::Init() {
  P1 = CELLMODEL_PARAMVALUE(VT_P1a);
  P0 = CELLMODEL_PARAMVALUE(VT_P0a);
  N1 = CELLMODEL_PARAMVALUE(VT_N1a);
  TCa = CELLMODEL_PARAMVALUE(VT_TCaa);
}

ML_CalcType Rice3::Calc(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType &Ca,
                        int euler = 1) {
  TCa -= tinc * (-CELLMODEL_PARAMVALUE(VT_k_on) * Ca * (1 - TCa) + CELLMODEL_PARAMVALUE(VT_k_off) * TCa);
  Ca -= tinc * (CELLMODEL_PARAMVALUE(VT_k_on) * Ca * (1 - TCa) - CELLMODEL_PARAMVALUE(VT_k_off) * TCa) * CELLMODEL_PARAMVALUE(VT_TCaMax);
  double SL_norm = 3.33333 * (stretch - .85);
  double k_1 = CELLMODEL_PARAMVALUE(VT_k_m1) * pow((TCa * (1.0 + CELLMODEL_PARAMVALUE(VT_K_Ca) / (1.5 - SL_norm))), 7.0 + 3.0 * SL_norm);
  return ForceEulerNEuler(euler, tinc / euler, k_1, stretch);
}

ML_CalcType
Rice3::CalcTrop(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType TnCa,
                int euler = 1) {
  TCa = TnCa / (CELLMODEL_PARAMVALUE(VT_TCaMax));
  double SL_norm = 3.33333 * (stretch - .85);
  double k_1 = CELLMODEL_PARAMVALUE(VT_k_m1) * pow((TCa * (1.0 + CELLMODEL_PARAMVALUE(VT_K_Ca) / (1.5 - SL_norm))), 7.0 + 3.0 * SL_norm);
  return ForceEulerNEuler(euler, tinc / euler, k_1, stretch);
}

inline ML_CalcType
Rice3::ForceEulerNEuler(int r, ML_CalcType tinc, ML_CalcType k_1, ML_CalcType stretch) {
  while (r--) {
    double kP0mN0 = CELLMODEL_PARAMVALUE(VT_k_m1) * P0 - k_1 * (1.0 - P0 - P1 - N1);
    double kP1mN1 = CELLMODEL_PARAMVALUE(VT_k_m1) * P1 - k_1 * N1;
    double kP0mP1 = CELLMODEL_PARAMVALUE(VT_f) * P0 - CELLMODEL_PARAMVALUE(VT_g) * P1;

    P0 += tinc * (-kP0mN0 - kP0mP1);
    P1 += tinc * (kP0mP1 - kP1mN1);
    N1 += tinc * (kP1mN1 - CELLMODEL_PARAMVALUE(VT_g) * N1);
  }

  // double TC=CELLMODEL_PARAMVALUE(VT_k_on)*10/(CELLMODEL_PARAMVALUE(VT_k_on)*10+CELLMODEL_PARAMVALUE(VT_k_off));
  // double k=CELLMODEL_PARAMVALUE(VT_k_m1)*pow((TC*(1.0+CELLMODEL_PARAMVALUE(VT_K_Ca))), 8.5);
  return Overlap(stretch, r3p->getOverlapID(), r3p->getOverlapParameters()) * (P1 + N1) *
         CELLMODEL_PARAMVALUE(VT_dFmax);

  // CELLMODEL_PARAMVALUE(VT_dFmax));
}

void Rice3::steadyState(ML_CalcType Ca, ML_CalcType stretch) {
  double TC = CELLMODEL_PARAMVALUE(VT_k_on) * Ca / (CELLMODEL_PARAMVALUE(VT_k_on) * Ca + CELLMODEL_PARAMVALUE(VT_k_off));
  double SL_norm = 3.33333 * (stretch - .85);
  double k_1 = CELLMODEL_PARAMVALUE(VT_k_m1) * pow((TC * (1.0 + CELLMODEL_PARAMVALUE(VT_K_Ca) / (1.5 - SL_norm))), 7.0 + 3.0 * SL_norm);

  cout << Ca << ' ' <<
       ((Overlap(stretch, r3p->getOverlapID(),
                 r3p->getOverlapParameters()) * CELLMODEL_PARAMVALUE(VT_f) * k_1 * (CELLMODEL_PARAMVALUE(VT_g) + k_1 + CELLMODEL_PARAMVALUE(VT_k_m1))) /
        (CELLMODEL_PARAMVALUE(VT_f) * CELLMODEL_PARAMVALUE(VT_g) * CELLMODEL_PARAMVALUE(VT_k_m1) + CELLMODEL_PARAMVALUE(VT_g) * CELLMODEL_PARAMVALUE(VT_g) * CELLMODEL_PARAMVALUE(VT_k_m1) +
         CELLMODEL_PARAMVALUE(VT_g) * CELLMODEL_PARAMVALUE(VT_k_m1) * CELLMODEL_PARAMVALUE(VT_k_m1) + CELLMODEL_PARAMVALUE(VT_f) * CELLMODEL_PARAMVALUE(VT_g) * k_1 + CELLMODEL_PARAMVALUE(VT_g) *
                                                                       CELLMODEL_PARAMVALUE(VT_g) * k_1 +
         CELLMODEL_PARAMVALUE(VT_f) * k_1 * CELLMODEL_PARAMVALUE(VT_k_m1) + 2 * CELLMODEL_PARAMVALUE(VT_g) * k_1 * CELLMODEL_PARAMVALUE(VT_k_m1) + CELLMODEL_PARAMVALUE(VT_f) * k_1 * k_1 +
         CELLMODEL_PARAMVALUE(VT_g) * k_1 * k_1)) * CELLMODEL_PARAMVALUE(VT_dFmax) << ' ' << TC <<
       ' ' << CELLMODEL_PARAMVALUE(VT_f) << ' ' << CELLMODEL_PARAMVALUE(VT_g) << ' ' << k_1 << ' ' << CELLMODEL_PARAMVALUE(VT_k_m1) << ' ' << CELLMODEL_PARAMVALUE(VT_k_on) * Ca
       << ' ' << CELLMODEL_PARAMVALUE(VT_k_off) << ' ' <<
       (CELLMODEL_PARAMVALUE(VT_f) * k_1 * (CELLMODEL_PARAMVALUE(VT_g) + k_1)) /
       (CELLMODEL_PARAMVALUE(VT_f) * k_1 * CELLMODEL_PARAMVALUE(VT_k_m1) + CELLMODEL_PARAMVALUE(VT_g) * (CELLMODEL_PARAMVALUE(VT_f) * k_1 + CELLMODEL_PARAMVALUE(VT_g) * (CELLMODEL_PARAMVALUE(VT_k_m1) + k_1) +
                                                CELLMODEL_PARAMVALUE(VT_k_m1) * (CELLMODEL_PARAMVALUE(VT_f) + CELLMODEL_PARAMVALUE(VT_k_m1) + k_1)) +
        (CELLMODEL_PARAMVALUE(VT_f) * k_1 + CELLMODEL_PARAMVALUE(VT_g) * (CELLMODEL_PARAMVALUE(VT_k_m1) + k_1)) * k_1) << ' '
       << (CELLMODEL_PARAMVALUE(VT_f) * k_1 * CELLMODEL_PARAMVALUE(VT_k_m1)) /
          (CELLMODEL_PARAMVALUE(VT_f) * k_1 * CELLMODEL_PARAMVALUE(VT_k_m1) + CELLMODEL_PARAMVALUE(VT_g) * (CELLMODEL_PARAMVALUE(VT_f) * k_1 + CELLMODEL_PARAMVALUE(VT_g) * (CELLMODEL_PARAMVALUE(VT_k_m1) + k_1) +
                                                   CELLMODEL_PARAMVALUE(VT_k_m1) * (CELLMODEL_PARAMVALUE(VT_f) + CELLMODEL_PARAMVALUE(VT_k_m1) + k_1)) +
           (CELLMODEL_PARAMVALUE(VT_f) * k_1 + CELLMODEL_PARAMVALUE(VT_g) * (CELLMODEL_PARAMVALUE(VT_k_m1) + k_1)) * k_1) << endl;
}

void Rice3::Print(ostream &tempstr) {
  tempstr << 1.0 - N1 - P0 - P1 << ' ' << N1 << ' '
          << P0 << ' '
          << P1 << ' ' << 1.0 - TCa << ' ' << TCa << ' ';
}

void Rice3::GetParameterNames(vector<string> &getpara) {
  const int numpara = 6;
  const string ParaNames[numpara] = {"N0", "N1", "P0", "P1", "T", "TCa"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
