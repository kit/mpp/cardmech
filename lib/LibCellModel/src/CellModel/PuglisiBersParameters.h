/*      File: PuglisiBersParameters.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

/*! \file PuglisiBers.h
   \brief Implementation of electrophysiological model for describing rabbit ventricular myocytes
   of Puglisi et al., AJP 2001
   Implementation is based on work by ?

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef PUGLISIBERSPARAMETERS_H
#define PUGLISIBERSPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_PuglisiBersParameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_Na_i,
    VT_Na_o,
    VT_Na_b,
    VT_K_i,
    VT_K_o,
    VT_K_b,
    VT_Ca_i,
    VT_Ca_o,
    VT_Ca_b,
    VT_Cl_o,
    VT_Cl_i,
    VT_V,
    VT_m,
    VT_h,
    VT_j,
    VT_d,
    VT_f,
    VT_Xs,
    VT_Xr,
    VT_b,
    VT_g,
    VT_Xtof,
    VT_Ytof,
    VT_Xtos,
    VT_Yto1s,
    VT_Yto2s,
    VT_t_rel,
    VT_Ca_int_2ms,
    VT_Ca_int_Jrel,
    VT_Ca_JSR,
    VT_Ca_NSR,
    VT_TRPN,
    VT_CMDN,
    VT_CSQN,
    VT_Ca_iontold,
    VT_dCa_iont,
    VT_Vtot,
    VT_Cap,
    VT_fr_NSR,
    VT_fr_JSR,
    VT_fr_MYO,
    VT_fr_MITO,
    VT_fr_SR,
    VT_fr_CLEFT,
    VT_Vol_NSR,
    VT_Vol_JSR,
    VT_Vol_MYO,
    VT_Vol_MITO,
    VT_Vol_SR,
    VT_g_Na,
    VT_k_mCa,
    VT_P_Ca,
    VT_gamma_Cai,
    VT_gamma_Cao,
    VT_P_Na,
    VT_gamma_Nai,
    VT_gamma_Nao,
    VT_P_K,
    VT_gamma_Ki,
    VT_gamma_Ko,
    VT_g_CaT,
    VT_E_CaT,
    VT_prNaK,
    VT_gbar_K1,
    VT_gbar_Kr,
    VT_gbar_Ks,
    VT_g_Kp,
    VT_kNaCa,
    VT_KmNaex,
    VT_KmCaex,
    VT_eta,
    VT_ksat,
    VT_I_NaKmax,
    VT_k_mNai,
    VT_k_mKo,
    VT_I_pCamax,
    VT_k_mpCa,
    VT_g_Cab,
    VT_g_Nab,
    VT_g_tof,
    VT_g_tos,
    VT_PnsK,
    VT_PnsNa,
    VT_k_mnsCa,
    VT_g_ClCa,
    VT_k_mClCa,
    VT_t_tr,
    VT_k_mup,
    VT_I_upmax,
    VT_Ca_NSRmax,
    VT_t_on,
    VT_t_off,
    VT_CSQN_th,
    VT_g_maxrel,
    VT_CSQN_max,
    VT_k_mCSQN,
    VT_CMDN_max,
    VT_TRPN_max,
    VT_k_mCMDN,
    VT_k_mTRPN,
    VT_g_K1,
    VT_g_Kr,
    VT_F,
    VT_R,
    VT_FdRT,
    VT_RTdF,
    VT_d2F,
    VT_RTd2F,
    VT_V_myo,
    VT_V_mito,
    VT_V_SR,
    VT_V_NSR,
    VT_V_JSR,
    VT_V_cleft,
    VT_VJdVN,
    VT_CapdVcF,
    VT_CapdVmF,
    VT_Amp,
    vtLast
  };
} // namespace NS_PuglisiBersParameters

using namespace NS_PuglisiBersParameters;

class PuglisiBersParameters : public vbNewElphyParameters {
public:
  PuglisiBersParameters(const char *, ML_CalcType);

  ~PuglisiBersParameters();

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType);

  void Init(const char *, ML_CalcType);

  ML_CalcType m_inf[RTDT];
  ML_CalcType exptau_m[RTDT];
  ML_CalcType h_inf[RTDT];
  ML_CalcType exptau_h[RTDT];
  ML_CalcType j_inf[RTDT];
  ML_CalcType exptau_j[RTDT];
  ML_CalcType d_inf[RTDT];
  ML_CalcType exptau_d[RTDT];
  ML_CalcType f_inf[RTDT];
  ML_CalcType exptau_f[RTDT];
  ML_CalcType b_inf[RTDT];
  ML_CalcType exptau_b[RTDT];
  ML_CalcType g_inf[RTDT];
  ML_CalcType exptau_g[RTDT];
  ML_CalcType Xr_inf[RTDT];
  ML_CalcType exptau_Xr[RTDT];
  ML_CalcType R_Xr[RTDT];
  ML_CalcType Xs_inf[RTDT];
  ML_CalcType exptau_Xs[RTDT];
  ML_CalcType exptau_Xtof[RTDT];
  ML_CalcType Xtof_inf[RTDT];
  ML_CalcType exptau_Ytof[RTDT];
  ML_CalcType Ytof_inf[RTDT];
  ML_CalcType exptau_Xtos[RTDT];
  ML_CalcType Xtos_inf[RTDT];
  ML_CalcType exptau_Yto1s[RTDT];
  ML_CalcType Yto1s_inf[RTDT];
  ML_CalcType exptau_Yto2s[RTDT];
  ML_CalcType Yto2s_inf[RTDT];
  ML_CalcType Kp[RTDT];
  ML_CalcType expV[RTDT];
  ML_CalcType expV_2[RTDT];
  ML_CalcType expVm[RTDT];
  ML_CalcType expV_mk1[RTDT];
}; // class PuglisiBersParameters
#endif // ifndef PUGLISIBERSPARAMETERS_H
