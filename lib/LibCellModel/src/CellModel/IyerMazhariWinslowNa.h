/*! \file IyerMazhariWinslowNa.h
   \brief Markov model of sodium channel of human ventricular myocytes
   Iyer et al., Biophys J 2004

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef IYER_MAZHARI_WINSLOW_NA_H
#define IYER_MAZHARI_WINSLOW_NA_H

#include <IyerMazhariWinslowNaParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_IyerMazhariWinslowNaParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pIMW->P[NS_IyerMazhariWinslowNaParameters::a].value
#endif // ifdef HETERO

class IyerMazhariWinslowNa : public vbElphyModel<ML_CalcType> {
public:
  double C0Na;
  double C1Na;
  double C2Na;
  double C3Na;
  double C4Na;
  double O1Na;
  double O2Na;
  double CI0Na;
  double CI1Na;
  double CI2Na;
  double CI3Na;
  double CI4Na;
  double INa;

  IyerMazhariWinslowNaParameters *pIMW;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  IyerMazhariWinslowNa(IyerMazhariWinslowNaParameters *);

  ~IyerMazhariWinslowNa() {}

  virtual void Init();

  virtual inline ML_CalcType Volume() { return 0; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return -.09066; }

  virtual inline ML_CalcType GetCai() { return 0; }

  virtual inline ML_CalcType GetCao() { return 0; }

  virtual inline ML_CalcType GetNai() { return CELLMODEL_PARAMVALUE(VT_Nai); }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Nao); }

  virtual inline ML_CalcType GetKi() { return 0; }

  virtual inline ML_CalcType GetKo() { return 0; }

  virtual inline int GetSize(void) {
    return sizeof(IyerMazhariWinslowNa) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(IyerMazhariWinslowNaParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return (ML_CalcType *) &C0Na; }

  virtual inline void SetCai(ML_CalcType val) {}

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char) (adVm < .15e-6 ? 3 : (adVm < .3e-6 ? 2 : 1));
  }

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);
}; // class IyerMazhariWinslowNa


#endif // ifndef IYER_MAZHARI_WINSLOW_NA_H
