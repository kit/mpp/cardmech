/*! \file LuEtAl.h
   \brief Implementation of electrophysiological model for describing hERG channels
   from Lu et al., J Phys 2001

   \author fs, CVRTI - University of Utah, USA
 */

#include <LuEtAl.h>

LuEtAl::LuEtAl(LuEtAlParameters *pIMWArg) {
  pIMW = pIMWArg;
#ifdef HETERO
  PS = new ParameterSwitch(pIMW, NS_LuEtAlParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

#ifdef HETERO

inline bool LuEtAl::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool LuEtAl::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int LuEtAl::GetSize(void) {
  return sizeof(LuEtAl) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(LuEtAlParameters *)
#ifdef HETERO
    -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
      ;
}

void LuEtAl::Init() {
  Ki = CELLMODEL_PARAMVALUE(VT_Ki);
  C0Kr = CELLMODEL_PARAMVALUE(VT_C0Kr);
  C1Kr = CELLMODEL_PARAMVALUE(VT_C1Kr);
  C2Kr = CELLMODEL_PARAMVALUE(VT_C2Kr);
  OKr = CELLMODEL_PARAMVALUE(VT_OKr);
  IKr = CELLMODEL_PARAMVALUE(VT_IKr);
  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);
  Qb = pow(3.3, (Tx - 310.) / 10.);  // Q10 from Iyer et al.
}

void LuEtAl::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' '  // 1
          << V << ' '
          << Ki << ' '
          << C0Kr << ' '
          << C1Kr << ' ' // 5
          << C2Kr << ' '
          << OKr << ' '
          << IKr << ' ';
}

void LuEtAl::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);

  // const double Acap=CELLMODEL_PARAMVALUE(VT_Acap);
  // const double Vmyo=CELLMODEL_PARAMVALUE(VT_Vmyo);
  const double V_int = V * 1000.0;
  const double Ko = CELLMODEL_PARAMVALUE(VT_Ko);
  const double RTdF = ElphyModelConstants::R * Tx / ElphyModelConstants::F;
  const double EK = RTdF * log(Ko / Ki); // -92 mV for resting 1 Hz
  const double f = sqrt(Ko * .25);
  const double I_Kr =
      (CELLMODEL_PARAMVALUE(VT_G_C0Kr) * C0Kr + CELLMODEL_PARAMVALUE(VT_G_C1Kr) * C1Kr + CELLMODEL_PARAMVALUE(VT_G_C2Kr) * C2Kr + CELLMODEL_PARAMVALUE(VT_G_OKr) * OKr +
       CELLMODEL_PARAMVALUE(VT_G_IKr) * IKr) * f *
      (V_int - EK);

  //    cerr << EK << endl;
  tempstr << ' ' << I_Kr;  // 9
}

void LuEtAl::GetParameterNames(vector<string> &getpara) {
  const char *ParaNames[] = {"Ki", "C0Kr", "C1Kr", "C2Kr", "OKr", "IKr"};

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void LuEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const char *ParaNames[] = {"I_Kr"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType
LuEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim = .0, ML_CalcType stretch = 1.,
             int euler = 1) {
  tinc *= 1000.0;
  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);
  const double V_RTdF = V / (ElphyModelConstants::R * Tx / ElphyModelConstants::F / 1000.);
  const double Kra0 = CELLMODEL_PARAMVALUE(VT_Kra0a) * exp(CELLMODEL_PARAMVALUE(VT_Kra0z) * V_RTdF);
  const double Krb0 = CELLMODEL_PARAMVALUE(VT_Krb0a) * exp(-CELLMODEL_PARAMVALUE(VT_Krb0z) * V_RTdF);
  const double Krf = CELLMODEL_PARAMVALUE(VT_Krfa) * exp(CELLMODEL_PARAMVALUE(VT_Krfz) * V_RTdF);
  const double Krb = CELLMODEL_PARAMVALUE(VT_Krba) * exp(-CELLMODEL_PARAMVALUE(VT_Krbz) * V_RTdF);
  const double Kra1 = CELLMODEL_PARAMVALUE(VT_Kra1a) * exp(CELLMODEL_PARAMVALUE(VT_Kra1z) * V_RTdF);
  const double Krb1 = CELLMODEL_PARAMVALUE(VT_Krb1a) * exp(-CELLMODEL_PARAMVALUE(VT_Krb1z) * V_RTdF);
  const double Krai = abs(CELLMODEL_PARAMVALUE(VT_Kraia)) * exp(CELLMODEL_PARAMVALUE(VT_Kraiz) * V_RTdF);
  const double Krbi = abs(CELLMODEL_PARAMVALUE(VT_Krbia)) * exp(-CELLMODEL_PARAMVALUE(VT_Krbiz) * V_RTdF);
  const double Kraci = CELLMODEL_PARAMVALUE(VT_Kracia) * exp(CELLMODEL_PARAMVALUE(VT_Kraciz) * V_RTdF);
  const double Krbci = CELLMODEL_PARAMVALUE(VT_Krbcia) * exp(-CELLMODEL_PARAMVALUE(VT_Krbciz) * V_RTdF);
  const double dC0Kr = (-Kra0 * C0Kr + Krb0 * C1Kr);
  const double dC1Kr = (-(Krb0 + Krf) * C1Kr + Kra0 * C0Kr + Krb * C2Kr);
  const double dC2Kr = (-(Kra1 + Krb + Kraci) * C2Kr + Krf * C1Kr + Krb1 * OKr + Krbci * IKr);

  // const double dOKr= (-(Krb1+Krai)     *OKr +Kra1* C2Kr+Krbi*IKr);
  const double dIKr = (-(Krbci + Krbi) * IKr + Kraci * C2Kr + Krai * OKr);
  C0Kr += tinc * dC0Kr * Qb;
  if (C0Kr > 1.)
    C0Kr = 1.;
  else if (C0Kr < 0)
    C0Kr = 0.;
  C1Kr += tinc * dC1Kr * Qb;
  if (C1Kr > 1.)
    C1Kr = 1.;
  else if (C1Kr < 0)
    C1Kr = 0.;
  C2Kr += tinc * dC2Kr * Qb;

  // OKr+=tinc*dOKr*Qb;
  IKr += tinc * dIKr * Qb;
  OKr = 1. - C0Kr - C1Kr - C2Kr - IKr;
  if (OKr > 1.)
    OKr = 1.;
  else if (OKr < 0)
    OKr = 0.;
  assert(C0Kr >= 0. && C0Kr <= 1.);
  assert(C1Kr >= 0. && C1Kr <= 1.);
  assert(C2Kr >= 0. && C2Kr <= 1.);
  assert(OKr >= 0. && OKr <= 1.);
  assert(IKr >= 0. && IKr <= 1.);
  return 0.;
} // LuEtAl::Calc

void LuEtAl::GetStatus(double *p) const {
  p[0] = Ki;
  p[1] = C0Kr;
  p[2] = C1Kr;
  p[3] = C2Kr;
  p[4] = OKr;
  p[5] = IKr;
}

void LuEtAl::SetStatus(const double *p) {
  Ki = p[0];
  C0Kr = p[1];
  C1Kr = p[2];
  C2Kr = p[3];
  OKr = p[4];
  IKr = p[5];
}
