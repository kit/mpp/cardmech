/**@file BondarenkoCa.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <BondarenkoCa.h>

BondarenkoCa::BondarenkoCa(BondarenkoCaParameters *pp) {
  pB = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pB, NS_BondarenkoParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

#ifdef HETERO

inline bool BondarenkoCa::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool BondarenkoCa::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

void BondarenkoCa::Init() {
  C1 = CELLMODEL_PARAMVALUE(VT_C1);
  C2 = CELLMODEL_PARAMVALUE(VT_C2);
  C3 = CELLMODEL_PARAMVALUE(VT_C3);
  C4 = CELLMODEL_PARAMVALUE(VT_C4);
  I1 = CELLMODEL_PARAMVALUE(VT_I1);
  I2 = CELLMODEL_PARAMVALUE(VT_I2);
  I3 = CELLMODEL_PARAMVALUE(VT_I3);
  O = CELLMODEL_PARAMVALUE(VT_O);
}

void BondarenkoCa::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' ';
  tempstr << C1 << ' ';  // 3
  tempstr << C2 << ' ';
  tempstr << C3 << ' ';  // 5
  tempstr << C4 << ' ';
  tempstr << I1 << ' ';
  tempstr << I2 << ' ';
  tempstr << I3 << ' ';
  tempstr << O << ' ';  // 10
  //    tempstr << C1+C2+C3+C4+I1+I2+I3+O <<' ';
}

void BondarenkoCa::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);

  ML_CalcType i_CaL = CELLMODEL_PARAMVALUE(VT_g_CaL) * (V - CELLMODEL_PARAMVALUE(VT_E_CaL)) * O;
  tempstr << i_CaL << ' ';  // 11
}

ML_CalcType BondarenkoCa::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                               ML_CalcType stretch = 1.,
                               int euler = 1) {
  // cout << tinc << " " << V << " " << i_external << " " << stretch << endl;
  /*    LongPrint(cout, tinc, V);
     cout << endl;
     pB->PrintParameters();
     cout << endl;
   */
  const ML_CalcType VinMV = V * 1000;
  const ML_CalcType tinMs = tinc * 1000;

  const ML_CalcType af1 = CELLMODEL_PARAMVALUE(VT_a1) * exp((VinMV + CELLMODEL_PARAMVALUE(VT_a2)) / CELLMODEL_PARAMVALUE(VT_a3));
  const ML_CalcType af2 = CELLMODEL_PARAMVALUE(VT_a4) * exp(-(VinMV + CELLMODEL_PARAMVALUE(VT_a5)) * (VinMV + CELLMODEL_PARAMVALUE(VT_a5)) / CELLMODEL_PARAMVALUE(VT_a6));
  const ML_CalcType af3 = CELLMODEL_PARAMVALUE(VT_a7) * exp(-(VinMV + CELLMODEL_PARAMVALUE(VT_a8)) * (VinMV + CELLMODEL_PARAMVALUE(VT_a8)) / CELLMODEL_PARAMVALUE(VT_a9));
  const ML_CalcType af4 = CELLMODEL_PARAMVALUE(VT_a10) * exp((VinMV + CELLMODEL_PARAMVALUE(VT_a11)) / CELLMODEL_PARAMVALUE(VT_a12));
  const ML_CalcType a = af1 * (1. + af2 - af3) / (1. + af4);
  const ML_CalcType b = CELLMODEL_PARAMVALUE(VT_b1) * exp(-(VinMV + CELLMODEL_PARAMVALUE(VT_b2)) / CELLMODEL_PARAMVALUE(VT_b3));
  const ML_CalcType g = CELLMODEL_PARAMVALUE(VT_K_pcmax) * CELLMODEL_PARAMVALUE(VT_Ca_ss) / (CELLMODEL_PARAMVALUE(VT_K_pchalf) + CELLMODEL_PARAMVALUE(VT_Ca_ss));
  const ML_CalcType K_pcf =
      CELLMODEL_PARAMVALUE(VT_K_pcf1) * (1. - exp(-(VinMV + CELLMODEL_PARAMVALUE(VT_K_pcf2)) * (VinMV + CELLMODEL_PARAMVALUE(VT_K_pcf2)) / CELLMODEL_PARAMVALUE(VT_K_pcf3)));

  //    cout << "b1 " << *pB->b1 << " b2 " <<  *pB->b2<< " b3 " << *pB->b3 << endl;
  //    cerr << "a " << a << " b " << b << " g " << g << " K " <<
  //    K_pcf << endl;

  const ML_CalcType C4O = a * C4 - 4. * b * O;
  const ML_CalcType I1O = CELLMODEL_PARAMVALUE(VT_K_pcb) * I1 - g * O;
  const ML_CalcType I2O = .001 * (a * I2 - K_pcf * O);
  const ML_CalcType C1C2 = 4. * a * C1 - b * C2;
  const ML_CalcType C2C3 = 3. * a * C2 - 2. * b * C3;
  const ML_CalcType C3C4 = 2. * a * C3 - 3. * b * C4;
  const ML_CalcType I1C4 = 0.01 * (4. * CELLMODEL_PARAMVALUE(VT_K_pcb) * b * I1 - a * g * C4);
  const ML_CalcType I2C4 = 0.002 * (4. * b * I2 - K_pcf * C4);
  const ML_CalcType I3C4 = 4. * b * CELLMODEL_PARAMVALUE(VT_K_pcb) * I3 - g * K_pcf * C4;
  const ML_CalcType I1I3 = 0.001 * (K_pcf * I1 - a * I3);
  const ML_CalcType I2I3 = g * I2 - CELLMODEL_PARAMVALUE(VT_K_pcb) * I3;


  O += tinMs * (C4O + I1O + I2O);
  C1 = 1. - (O + C2 + C3 + C4 + I1 + I2 + I3);
  C2 += tinMs * (C1C2 - C2C3);
  C3 += tinMs * (C2C3 - C3C4);
  C4 += tinMs * (I1C4 + I2C4 + I3C4 + C3C4 - C4O);
  I1 += tinMs * (-I1C4 - I1I3 - I1O);
  I2 += tinMs * (-I2C4 - I2I3 - I2O);
  I3 += tinMs * (I1I3 + I2I3 - I3C4);

  C1 = 1. - (O + C2 + C3 + C4 + I1 + I2 + I3);

  return tinc * (CELLMODEL_PARAMVALUE(VT_dC_m) * CELLMODEL_PARAMVALUE(VT_g_CaL) * (V - CELLMODEL_PARAMVALUE(VT_E_CaL)) * O - i_external);
} // BondarenkoCa::Calc
