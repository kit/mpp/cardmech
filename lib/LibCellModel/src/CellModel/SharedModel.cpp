/**@file SharedModel.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifdef osMac_Vec

#define osMac

// #define VERBOSEOUTPUT

#include <SharedModel.h>

/*!
 *  \brief Constructor
 */
SharedModel::SharedModel(SharedModelParameters *pp) {
  pIP         = pp;
  numCurrents = pIP->HM->numCurrents;
  cerr<<"using "<<numCurrents<<" ionic currents and "<<pIP->HM->numConcentrations<<
    " concentrations for calculation ...\n";
  shd = new SO_ionicCurrent<ML_CalcType>*[numCurrents];
#if KADEBUG
  cerr<<"loading array of string and *SO_ionicCurrent<ML_CalcType> with " << numCurrents << " fileNames ...\n";
  for (int x = 0; x < numCurrents; x++)
    cerr<<"x="<<x<<", handle="<<pp->handle[x]<<endl;
#endif // if KADEBUG
  Init();
}

/*!
 *  \brief Destructor
 */
SharedModel::~SharedModel() {
#if KADEBUG
  cerr<<"deleting shd[0 .." << numCurrents-1 << "] and FN + EV\n";
#endif // if KADEBUG
  delete NA;
  delete CA;
  delete KA;

  for (int x = 0; x < numCurrents; x++)
    delete shd[x];

  if (shd)
    delete[] shd;
}

void SharedModel::CalcEQs() {
  /**********************************************************************
  * Equilibrium Potentials
  **********************************************************************/
  /*!< Eto = *pCmP->RTdF * log ( ( *pCmP->PNato * *pCmP->Nao + *pCmP->Ko ) / ( *pCmP->PNato * Nai + Ki ) ); // (20)
     PNato = 0.043 */
  E_to = Array[8]*log((0.043*Array[3]+Array[5])/(0.043*Array[2]+Array[4]));

  /*!< ENa = *pCmP->RTdF * log( *pCmP->Nao / Nai ); */
  E_Na = Array[8]*log(Array[3]/Array[2]);

  /*!< EK = *pCmP->RTdF * log( *pCmP->Ko / Ki ); */
  E_K = Array[8]*log(Array[5]/Array[4]);

  /*!< EKs = *pCmP->RTdF * log( ( *pCmP->PNaKs * *pCmP->Nao + *pCmP->Ko ) / ( *pCmP->PNaKs * Nai + Ki ) ); // (16) */
  E_Ks = Array[8]*log((CELLMODEL_PARAMVALUE(VT_pNaKs) *Array[3]+Array[5])/(CELLMODEL_PARAMVALUE(VT_pNaKs) * Array[2] + Array[4]));

  /*!< ECa = *pCmP->RTdF * log( *pCmP->Cao / Cai ) / 2; */
  E_Ca = 0.5*Array[8]*log(Array[7]/Array[6]);
}

void SharedModel::Init() {
#if KADEBUG
  cerr << "initializing Class: SharedModel ... " << endl;
#endif // if KADEBUG
  Array[0] = CELLMODEL_PARAMVALUE(VT_init_Vm)*1000; // Vm    -.09086
  // Array[1]=.00005; //tinc / tinc_1000
  Array[1]  = 0.01;
  Array[2]  = CELLMODEL_PARAMVALUE(VT_init_Nai); // Na_i
  Array[3]  = CELLMODEL_PARAMVALUE(VT_init_Nao); // Na_o
  Array[4]  = CELLMODEL_PARAMVALUE(VT_init_Ki); // K_i  125.55896
  Array[5]  = CELLMODEL_PARAMVALUE(VT_init_Ko); // K_o  bisher 4.0
  Array[6]  = CELLMODEL_PARAMVALUE(VT_init_Cai); // Ca_i
  Array[7]  = CELLMODEL_PARAMVALUE(VT_init_Cao); // Ca_o
  Array[8]  = CELLMODEL_PARAMVALUE(VT_RTdF); // RTdF
  Array[9]  = CELLMODEL_PARAMVALUE(VT_init_IKv14Na); // IKv14Na
  Array[10] = CELLMODEL_PARAMVALUE(VT_init_CaSS);  // CaSS
  Array[11] = CELLMODEL_PARAMVALUE(VT_init_ICaK);  // ICa,K
  Array[12] = CELLMODEL_PARAMVALUE(VT_C_m);
  Array[13] = CELLMODEL_PARAMVALUE(VT_CmdF);
  Array[14] = CELLMODEL_PARAMVALUE(VT_F);

  /* cout << "C_m" << CELLMODEL_PARAMVALUE(VT_C_m) << endl;
     cout << "Array[13] = " << Array[13] << endl;
     cout << "CmdF" << pIP->CmdF << endl;
     cout << "F" << CELLMODEL_PARAMVALUE(VT_F) << endl;
     cout << "Array[14] = " << Array[14] << endl;
     cout << "Array[0] = " << Array[0] << endl; */


  /**********************************************************************
  * Equilibrium Potentials
  **********************************************************************/
  CalcEQs();

  /*
     cout << "Initial equilibrium potentials: " << endl;

     cout << "Eto = " << E_to << endl;

     cout << "E_Na = " << E_Na << endl;

     cout << "E_K = " << E_K << endl;

     cout << "E_Ks = " << E_Ks << endl;

     cout << "E_Ca = " << E_Ca << endl;
   */

  cICaK = CELLMODEL_PARAMVALUE(VT_init_ICaK2);  // 3.1106544E-16;

  // removed for Kurata
  // iStim=GetAmplitude();

  indexINa   = -1;
  indexIto   = -1;
  indexIKr   = -1;
  indexIKs   = -1;
  indexINab  = -1;
  indexIKb   = -1;
  indexIK1   = -1;
  indexICa   = -1;
  indexICab  = -1;
  indexINaCa = -1;
  indexINaK  = -1;
  indexIpCa  = -1;
  indexIKatp = -1;

  cINa   = 0;
  cIto   = 0;
  cIKr   = 0;
  cIKs   = 0;
  cINab  = 0;
  cIKb   = 0;
  cIK1   = 0;
  cICa   = 0;
  cICab  = 0;
  cINaCa = 0;
  cINaK  = 0;
  cIpCa  = 0;
  cIKatp = 0;

  for (int x = 0; x < numCurrents; x++) {
    shd[x] = new SO_ionicCurrent<ML_CalcType>(pIP->handle[x], pIP->pEP[x], &Array[0], &Vi);
    if ((pIP->pEP[x])->ReInitRequired) {
      (pIP->pEP[x])->InitTableWithArrayStart(&Array[0]);
    }

    switch (shd[x]->GetEquilibriumType()) {
      case eqE_Na:
        shd[x]->SetEquilibrium(&E_Na);
        break;
      case eqE_to:
        shd[x]->SetEquilibrium(&E_to);
        break;
      case eqE_K:
        shd[x]->SetEquilibrium(&E_K);
        break;
      case eqE_Ks:
        shd[x]->SetEquilibrium(&E_Ks);
        break;
      case eqE_Ca:
        shd[x]->SetEquilibrium(&E_Ca);
        break;
      default:
        throw kaBaseException("no valid equilibrium type defined ...\n");
    }

    c_Type ct = (c_Type)LoadSymbol(pIP->handle[x], "CurrentType");

    switch (ct() ) {
      case INa:
        indexINa = x;
        break;
      case Ito:
        indexIto = x;
        break;
      case IKr:
        indexIKr = x;
        break;
      case IKs:
        indexIKs = x;
        break;
      case INab:
        indexINab = x;
        break;
      case IKb:
        indexIKb = x;
        break;
      case IK1:
        indexIK1 = x;
        break;
      case ICa:
        indexICa = x;
        break;
      case ICab:
        indexICab = x;
        break;
      case INaCa:
        indexINaCa = x;
        break;
      case INaK:
        indexINaK = x;
        break;
      case IpCa:
        indexIpCa = x;
        break;
      default:
        throw kaBaseException("unknown index %i=%i\n", x, ct());
        break;
    } // switch

    /*if (ct()==INa){
            indexINa=x;
       }
       else if (ct()==Ito){
            indexIto=x;
       }
       else if (ct()==IKr){
            indexIKr=x;
       }
       else if (ct()==IKs){
            indexIKs=x;
       }
       else if (ct()==INab){
            indexINab=x;
       }
       else if (ct()==IKb){
            indexIKb=x;
       }
       else if (ct()==IK1){
            indexIK1=x;
       }
       else if (ct()==ICa){
            indexICa=x;
       }
       else if (ct()==ICab){
            indexICab=x;
       }
       else if (ct()==INaCa){
            indexINaCa=x;
       }
       else if (ct()==INaK){
            indexINaK=x;
       }
       else if (ct()==IpCa){
            indexIpCa=x;
       }
       else
            throw kaBaseException("unknown index %i=%i\n",x,ct());*/

#if KADEBUG
    cerr<<"x="<<x<<": currentType: "<<ct()<<endl;
#endif // if KADEBUG
  }
#if KADEBUG
  cerr<<"initialization of ionic currents finished\n";
#endif // if KADEBUG
  if (pIP->HM->indexNaConcentration)
    NA =
      new SO_NaConc_Handling<ML_CalcType>(pIP->handle[pIP->HM->indexNaConcentration],
                                          pIP->pEP[pIP->HM->indexNaConcentration], &Array[0], &Vi, &dV, &cINa, &cINab,
                                          &cINaCa, &cINaK);
  else
    NA = NULL;
  if (pIP->HM->indexKConcentration)
    KA =
      new SO_KConc_Handling<ML_CalcType>(pIP->handle[pIP->HM->indexKConcentration],
                                         pIP->pEP[pIP->HM->indexKConcentration], &Array[0], &Vi, &dV, &cIKr, &cIKs,
                                         &cIto,
                                         &cIK1, &cINaK, &cICaK, &cIKb, &cIKatp, &iStim);
  else
    KA = NULL;
  if (pIP->HM->indexCaConcentration)
    CA =
      new SO_CaConc_Handling<ML_CalcType>(pIP->handle[pIP->HM->indexCaConcentration],
                                          pIP->pEP[pIP->HM->indexCaConcentration], &Array[0], &Vi, &dV, &cICa, &cICab,
                                          &cINaCa, &cIpCa);
  else
    CA = NULL;
#if KADEBUG
  cerr<<"initialization of concentration handlings finished\n";
#endif // if KADEBUG
} // SharedModel::Init

inline void SharedModel::Set(vbElphyModel<ML_CalcType>& from) {
  // cerr<<"SharedModel.h: copying "<<from.GetSize()<<" Bytes starting from "<<from.GetBase()<<endl;
  memcpy(GetBase(), from.GetBase(), from.GetSize());
  for (int x = 0; x < numCurrents; x++) {
    SO_ionicCurrent<ML_CalcType> *mySIC = GetSO_ionicCurrent(x);
    SO_ionicCurrent<ML_CalcType> *exSIC = from.GetSO_ionicCurrent(x);

    //                          cerr<<"x="<<x<<"\tmySIC="<<mySIC<<"\texSIC="<<exSIC<<"\n";
    //                          cerr<<mySIC->GetEquilibriumType()<<"\t"<<exSIC->GetEquilibriumType()<<"\n";
    mySIC->Set(exSIC);
  }
}

inline SO_ionicCurrent<ML_CalcType>* SharedModel::GetSO_ionicCurrent(int index) {
  // cerr<<"index="<<index<<endl;
  return shd[index];
}

inline void SharedModel::SetCai(ML_CalcType val) {
  // #if KADEBUG
  cerr<<"setting Cai to " << val << endl;

  // #endif
  Array[6] = val;
}

/*!< Function used to set the new value of tinc in all shared currents that need it, e.g. for table initialisation with
   tinc */
void SharedModel::SetTinc(ML_CalcType tinc) {
  Array[1] = tinc * 1000;
#if KADEBUG
  cout << "SharedModel::SetTinc: Informing all shared currents of a global tinc ( " << Array[1] << " )..." << endl;
#endif // if KADEBUG
  for (int x = 0; x < numCurrents; x++) {
    shd[x]->InitWithNewTinc(Array[1]);
  }
}

void SharedModel::Print(ostream& tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' ' << Array[6] << ' '  << Array[2] << ' ' << Array[4] << ' ';
}

void SharedModel::LongPrint(ostream& tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);

  tempstr << cICa << ' ' << cIKr << ' ' << cIKs << ' ' << cIto << ' ' << cINa << ' ' << cIK1 << ' ' << cINab << ' ' <<
    cICab << ' '
          << cIKb << ' ' << cINaK << ' ' << cINaCa << ' ' << cIpCa << ' ';

  for (int i = 0; i < numCurrents; i++)
    shd[i]->LongPrint(tempstr);
  KA->LongPrint(tempstr);
  NA->LongPrint(tempstr);
  CA->LongPrint(tempstr);
}

void SharedModel::GetParameterNames(vector<string>& getpara) {
  const int numpara               = 3;
  const string ParaNames[numpara] = { "Cai", "Nai", "Ki" };

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void SharedModel::GetLongParameterNames(vector<string>& getpara) {
  GetParameterNames(getpara);
  const int numpara               = 12;
  const string ParaNames[numpara] =
  { "iCaL", "iKr", "iKs", "ito", "iNa", "iK1", "ibNa", "ibCa", "ibK", "iNaK", "iNaCa", "ipCa" };
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);

  // for (int i=0; i<num; i++)
  //    shd[i]->GPN();
  for (int i = 0; i < numCurrents; i++)
    shd[i]->GetLongParameterNames(getpara);
  KA->GetLongParameterNames(getpara);
  NA->GetLongParameterNames(getpara);
  CA->GetLongParameterNames(getpara);
}

/* void SharedModel::Print(ostream &tempstr, double t, ML_CalcType V) {
        tempstr<<t<<' '<<V<<' '
        <<cINa<<' '<<cIto<<' '<<cIKr<<' '<<cIKs<<' '
        <<cINab<<' '<<cIK1<<' '<<cICa<<' '<<cICab<<' '
        <<cINaCa<<' '<<cINaK<<' '<<cIpCa<<' '<<cICaK<<' '
        ;
   }

   void SharedModel::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
        Print(tempstr, t, V);
        tempstr
                <<Array[2]<<' '<<Array[3]<<' '<<Array[4]<<' '<<Array[5]<<' '
                <<Array[6]<<' '<<Array[7]<<' ';

        for (int i=0; i<numCurrents; i++)
                shd[i]->LongPrint(tempstr);
        KA->LongPrint(tempstr);
        NA->LongPrint(tempstr);
        CA->LongPrint(tempstr);
   }

   void SharedModel::GetParameterNames(vector<string> &getpara){
        const int numpara=12;
        const string ParaNames[numpara] =
           {"INa","Ito","IKr","IKs","INab","IK1","ICa","ICab","INaCa","INaK","IpCa","ICaK"};
        for (int i=0; i<numpara; i++)
                getpara.push_back(ParaNames[i]);
   }

   void SharedModel::GetLongParameterNames(vector<string> &getpara){
        GetParameterNames(getpara);
        const int numpara=9;
        const string ParaNames[numpara] = {"Nai","Nao","Ki","Ko","Cai","Cao"};
        for (int i=0; i<numpara; i++)
                getpara.push_back(ParaNames[i]);
        //for (int i=0; i<num; i++)
        //      shd[i]->GPN();
        for (int i=0; i<numCurrents; i++)
                shd[i]->GetLongParameterNames(getpara);
        KA->GetLongParameterNames(getpara);
        NA->GetLongParameterNames(getpara);
        CA->GetLongParameterNames(getpara);
   }*/

inline unsigned char SharedModel::getSpeed(ML_CalcType adVm) {
  return (unsigned char)(adVm < .15e-6 ? 3 : (adVm < .3e-6 ? 2 : 1));
}

ML_CalcType SharedModel::Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch, int euler) {
  const ML_CalcType V_int = V*1000.0;

  Array[0] = V_int;
  Array[1] = tinc*1000;  // in case of TenTusscher2 it should be 0.02 (-tinc 0.00002)

  iStim = i_external;
  Ca_i  = Array[6];

  Vi = (int)(DivisionTab*(RangeTabhalf+V_int)+.5);

  CalcEQs();

  if (indexINa > -1)
    cINa = shd[indexINa]->CalcNext();
  if (indexIto > -1)
    cIto = shd[indexIto]->CalcNext();
  if (indexIKr > -1)
    cIKr = shd[indexIKr]->CalcNext();
  if (indexIKs > -1)
    cIKs = shd[indexIKs]->CalcNext();
  if (indexINab > -1)
    cINab = shd[indexINab]->CalcNext();
  if (indexIKb > -1)
    cIKb = shd[indexIKb]->CalcNext();
  if (indexIK1 > -1)
    cIK1 = shd[indexIK1]->CalcNext();
  if (indexICa > -1)
    cICa = shd[indexICa]->CalcNext();
  if (indexICab > -1)
    cICab = shd[indexICab]->CalcNext();
  if (indexINaCa > -1)
    cINaCa = shd[indexINaCa]->CalcNext();
  if (indexINaK > -1)
    cINaK = shd[indexINaK]->CalcNext();
  if (indexIpCa > -1)
    cIpCa = shd[indexIpCa]->CalcNext();

  cICaK = Array[11];

  const ML_CalcType sumI = cINa + cIto + cIKr + cIKs + cINab + cIKb + cIK1 + cICa + cICab + cINaCa + cINaK + cIpCa;

#ifdef VERBOSEOUTPUT
  cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$ Calc $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" << endl;

  cout << "Equation Potentials: " << endl;
  cout << "Eto = " << E_to << endl;
  cout << "E_Na = " << E_Na << endl;
  cout << "E_K = " << E_K << endl;
  cout << "E_Ks = " << E_Ks << endl;
  cout << "E_Ca = " << E_Ca << endl;

  cout << "Currents: " << endl;

  // << setprecision(10)
  cout << "cINa = " << cINa << endl;
  cout << "cIto = " << cIto << endl;
  cout << "cIKr = " << cIKr << endl;
  cout << "cIKs = " << cIKs << endl;
  cout << "cINab = " << cINab << endl;
  cout << "cIK1 = " << cIK1 << endl;
  cout << "cICa (cICaL) = " << cICa << endl;
  cout << "cICab = " << cICab << endl;
  cout << "cINaCa = " << cINaCa << endl;
  cout << "cINaK = " << cINaK << endl;
  cout << "cIpCa = " << cIpCa << endl;
  cout << "cIpK (cIKb) = " << cIKb << endl;
  cout << "i_stim = " << iStim << endl;
  cout << "sumI = " << sumI << endl;
  cout << "tinc = " << Array[1] << endl;
  cout << "------------------------------------------------------------------------V = " << V_int << endl;

  cout << "Concentrations: " << endl;
  cout << "Nai = " << Array[2] << endl;
  cout << "Nao = " << Array[3] << endl;
  cout << "Ki = " << Array[4] << endl;
  cout << "Ko = " << Array[5] << endl;
  cout << "Cai = " << Array[6] << endl;
  cout << "Cao = " << Array[7] << endl;
#endif // ifdef VERBOSEOUTPUT

  /*!< multiply with 0.001 not needed here because tinc is used, not tinc*1000 */
  dV = (iStim - sumI) * tinc;

  if (CA)
    CA->CalcNext();
  if (NA)
    NA->CalcNext();
  if (KA)
    KA->CalcNext();

  return dV;
} // SharedModel::Calc

#endif // ifdef osMac_Vec
