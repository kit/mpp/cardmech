/*! \file Saucerman.cpp
   \cell model of rat modelling adrenergic influence

   \version 0.0.0

   \date Created Carola Otto (12.03.2009)\n
       template (00.00.00)\n
       man Gunnar Seemann (27.02.03)\n
       Last Modified Eike Wuelfers (03.02.10)

   \author Carola Otto\n
         Institute of Biomedical Engineering\n
         Universitaet Karlsruhe (TH)\n
         http://www.ibt.uni-karlsruhe.de\n
         Copyright 2000-2009 - All rights reserved.

   // \sa  \ref Saucerman
 */

#include <Saucerman.h>

// Prototypes of functions called by IDA
static int resrob(realtype t, N_Vector w, N_Vector wd, N_Vector rr, void *rdata);

static void PrintFinalStats(void *ida_mem);

static int check_flag(void *flagvalue, char *funcname, int opt);

static realtype jsrol_time = NAN;

// Allocate solver memory and initialize solver settings
void Saucerman::alloc() {
  ida_mem = NULL;
  w = wd = id = NULL;
  constraints = NULL;
  neq = 49;     // number of equations to be solved
  abstol = 1.0e-6; // scalar value for the solution's absolute tolerance
  reltol = 1.0e-5; // scalar value for the solution's relative tolerance
  hmax = 0.0001; // maximal time step of solver integration
  mxsteps = 10000;  // maximal number of steps until an error flag of the solver is set
  tout1 = 0.00001;

  // Allocate N-vectors.
  w = N_VNew_Serial(neq);

  // if(check_flag((void *)w, "N_VNew_Serial", 0)) exit(1);
  wd = N_VNew_Serial(neq);

  // if(check_flag((void *)wd, "N_VNew_Serial", 0)) exit(1);
  id = N_VNew_Serial(neq);

  // if(check_flag((void *)id, "N_VNew_Serial", 0)) exit(1);
  // constraints = N_VNew_Serial(neq);
  // if(check_flag((void *)constraints, "N_VNew_Serial", 0)) exit(1);

  for (int i = 0; i < 49; i++) {
    NV_Ith_S(w, i) = (realtype) y0[i];
  }

  for (int i = 0; i < 49; i++) {
    NV_Ith_S(wd, i) = 0;
  }

  // Initialize vector with constraint definitions
  /*for(int i=0; i<49; i++)
     {NV_Ith_S(constraints,i)=(realtype)1.0;}
     NV_Ith_S(constraints,48)=0.0;*/

  for (int i = 0; i < 49; i++) {
    NV_Ith_S(id, i) = m0[i];
  }

  // Instantiate an IDA solver object
  ida_mem = IDACreate();

  // if(check_flag((void *)ida_mem, "IDACreate", 0)) exit(1);

  // Provide required problem and solution specifications, allocate internal memory, initialize IDA
  flag = IDAInit(ida_mem, resrob, t, w, wd);
  flag = IDASStolerances(ida_mem, reltol, abstol);

  // if(check_flag(&flag, "IDAMalloc", 1)) exit(1);

  // Specify algebraic / differential components in the w vector
  flag = IDASetId(ida_mem, id);

  // if(check_flag(&flag, "IDASetId", 1)) exit(1);
  // Specify the user data block and attach it to the main IDA memory block
  IDASetUserData(ida_mem, rdata);

  // Specify maximum number of steps to be taken by the solver in its attempt to reach the next output time
  IDASetMaxNumSteps(ida_mem, mxsteps);

  // Set maximum absolute value of the step size
  IDASetMaxStep(ida_mem, hmax);

  // Define inequality constraints for each component of the solution vector w
  // IDASetConstraints(ida_mem, constraints);

  // Call IDADense and set up the linear solver.
  flag = IDADense(ida_mem, neq);

  // if(check_flag(&flag, "IDADense", 1)) exit(1);

  // Calculate consistent initial conditions (correct initial values of w and wd at time t0)
  // tout1: first value at which a solution will be requested, to determine direction of integration and rough scale in
  // the independent variable t
  flag = IDACalcIC(ida_mem, IDA_YA_YDP_INIT, tout1);

  // if(check_flag(&flag, "IDACalcIC", 1)) exit(1);
} // Saucerman::alloc

Saucerman::Saucerman(SaucermanParameters *pp) {
  ptTeaP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(ptTeaP, NS_SaucermanParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

Saucerman::~Saucerman() {
  // Free w, wd, id and constraints vector
  N_VDestroy_Serial(id);

  // N_VDestroy_Serial(constraints);
  N_VDestroy_Serial(w);
  N_VDestroy_Serial(wd);

  // Free integrator memory
  IDAFree(&ida_mem);
}

#ifdef HETERO

inline bool Saucerman::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool Saucerman::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int Saucerman::GetSize(void) {
  return sizeof(Saucerman) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(SaucermanParameters *)
         #ifdef HETERO
         - sizeof(ParameterSwitch *)
#endif // ifdef HETERO
      ;
}

inline unsigned char Saucerman::getSpeed(ML_CalcType adVm) {
  return (unsigned char) 5;
}

void Saucerman::Init() {
#if KADEBUG
  cerr << "#initializing Class: Saucerman ... " << endl;
#endif // if KADEBUG

  // Initialize variables
  m = (CELLMODEL_PARAMVALUE(VT_m_init));
  h = (CELLMODEL_PARAMVALUE(VT_h_init));
  j = (CELLMODEL_PARAMVALUE(VT_j_init));
  xr1 = (CELLMODEL_PARAMVALUE(VT_xr1_init));
  xs = (CELLMODEL_PARAMVALUE(VT_xs_init));
  r = (CELLMODEL_PARAMVALUE(VT_r_init));
  s = (CELLMODEL_PARAMVALUE(VT_s_init));
  d = (CELLMODEL_PARAMVALUE(VT_d_init));
  f = (CELLMODEL_PARAMVALUE(VT_f_init));
  fCa = (CELLMODEL_PARAMVALUE(VT_fCa_init));
  Ca_i = (CELLMODEL_PARAMVALUE(VT_Cai_init));
  CaSR = (CELLMODEL_PARAMVALUE(VT_CaSR_init));
  Na_i = (CELLMODEL_PARAMVALUE(VT_Nai_init));
  K_i = ((CELLMODEL_PARAMVALUE(VT_Ki_init)));
  I_Ksp = (CELLMODEL_PARAMVALUE(VT_I_Ksp_init)); // needed to calculate the phosphorylated part of I_Ks
  Ca_jsr = (CELLMODEL_PARAMVALUE(VT_Ca_jsr_init));
  Ca_nsr = (CELLMODEL_PARAMVALUE(VT_Ca_nsr_init));
  L = (CELLMODEL_PARAMVALUE(VT_L_init));
  R = (CELLMODEL_PARAMVALUE(VT_R_init));
  Gs = (CELLMODEL_PARAMVALUE(VT_Gs_init));
  b1ARd = (CELLMODEL_PARAMVALUE(VT_b1ARd_init));
  b1ARtot = (CELLMODEL_PARAMVALUE(VT_b1ARtot_init));
  b1ARp = (CELLMODEL_PARAMVALUE(VT_b1ARp_init));
  Gsagtptot = (CELLMODEL_PARAMVALUE(VT_Gsagtptot_init));
  Gsagdp = (CELLMODEL_PARAMVALUE(VT_Gsagdp_init));
  Gsbg = (CELLMODEL_PARAMVALUE(VT_Gsbg_init));
  Gsa_gtp = (CELLMODEL_PARAMVALUE(VT_Gsa_gtp_init));
  AC = (CELLMODEL_PARAMVALUE(VT_AC_init));
  Fsk = (CELLMODEL_PARAMVALUE(VT_Fsk_init));
  PDE = (CELLMODEL_PARAMVALUE(VT_PDE_init));
  cAMP = (CELLMODEL_PARAMVALUE(VT_cAMP_init));
  IBMX = (CELLMODEL_PARAMVALUE(VT_IBMX_init));
  PKACI = (CELLMODEL_PARAMVALUE(VT_PKACI_init));
  PKACII = (CELLMODEL_PARAMVALUE(VT_PKACII_init));
  cAMPtot = (CELLMODEL_PARAMVALUE(VT_cAMPtot_init));
  PLBs = (CELLMODEL_PARAMVALUE(VT_PLBs_init));
  PP1 = (CELLMODEL_PARAMVALUE(VT_PP1_init));
  Inhib1ptot = (CELLMODEL_PARAMVALUE(VT_Inhib1ptot_init));
  Inhib1p = (CELLMODEL_PARAMVALUE(VT_Inhib1p_init));
  LCCap = (CELLMODEL_PARAMVALUE(VT_LCCap_init));
  LCCbp = (CELLMODEL_PARAMVALUE(VT_LCCbp_init));
  RyRp = (CELLMODEL_PARAMVALUE(VT_RyRp_init));
  TnIp = (CELLMODEL_PARAMVALUE(VT_TnIp_init));
  Iks = (CELLMODEL_PARAMVALUE(VT_Iks_init));
  Yotiao = (CELLMODEL_PARAMVALUE(VT_Yotiao_init));
  Iksp = (CELLMODEL_PARAMVALUE(VT_Iksp_init));
  trel = (CELLMODEL_PARAMVALUE(VT_trel_init));
  t = (CELLMODEL_PARAMVALUE(VT_t_init));
  xs05 = (CELLMODEL_PARAMVALUE(VT_xs05_init));
  Vm_old = (CELLMODEL_PARAMVALUE(VT_Vm_init)) * 1000;


  // Initialize constant parameters as user data

  sdata.p[0] = 50.5;                  // S2_time
  sdata.p[1] = (CELLMODEL_PARAMVALUE(VT_Ltotmax));       // Ltotmax   [uM] ** apply agonist concentration here **
  sdata.p[2] = 0.028;                 // sumb1AR   [uM]
  sdata.p[3] = (CELLMODEL_PARAMVALUE(VT_Gstot));         // Gstot     [uM]
  sdata.p[4] = (CELLMODEL_PARAMVALUE(VT_Kl));            // Kl        [uM]
  sdata.p[5] = (CELLMODEL_PARAMVALUE(VT_Kr));            // Kr        [uM]
  sdata.p[6] = (CELLMODEL_PARAMVALUE(VT_Kc));            // Kc        [uM]
  sdata.p[7] = (CELLMODEL_PARAMVALUE(VT_k_barkp));       // k_barkp   [1/sec]
  sdata.p[8] = (CELLMODEL_PARAMVALUE(VT_k_barkm));       // k_barkm   [1/sec]
  sdata.p[9] = (CELLMODEL_PARAMVALUE(VT_k_pkap));        // k_pkap    [1/sec/uM]
  sdata.p[10] = (CELLMODEL_PARAMVALUE(VT_k_pkam));  // k_pkam    [1/sec]
  sdata.p[11] = (CELLMODEL_PARAMVALUE(VT_k_gact));        // k_gact    [1/sec]
  sdata.p[12] = (CELLMODEL_PARAMVALUE(VT_k_hyd));         // k_hyd     [1/sec]
  sdata.p[13] = (CELLMODEL_PARAMVALUE(VT_k_reassoc));  // k_reassoc [1/sec/uM]
  // cAMP module
  sdata.p[14] = (CELLMODEL_PARAMVALUE(VT_AC_tot));        // AC_tot    [uM]
  sdata.p[15] = (CELLMODEL_PARAMVALUE(VT_ATP));           // ATP       [uM]
  sdata.p[16] = (CELLMODEL_PARAMVALUE(VT_PDE3tot));       // PDE3tot   [uM]
  sdata.p[17] = (CELLMODEL_PARAMVALUE(VT_PDE4tot));       // PDE4tot   [uM]
  sdata.p[18] = (CELLMODEL_PARAMVALUE(VT_IBMXtot));       // IBMXtot   [uM]
  // sdata.p[19] = (CELLMODEL_PARAMVALUE(VT_Fsk));    // Fsktot    [uM] [10 uM when used]
  sdata.p[19] = 0.0;                   // Fsktot    [uM] [10 uM when used]
  sdata.p[20] = (CELLMODEL_PARAMVALUE(VT_k_ac_basal));    // k_ac_basal[1/sec]
  sdata.p[21] = (CELLMODEL_PARAMVALUE(VT_k_ac_gsa));      // k_ac_gsa  [1/sec]
  sdata.p[22] = (CELLMODEL_PARAMVALUE(VT_k_ac_fsk));      // k_ac_fsk  [1/sec]
  sdata.p[23] = (CELLMODEL_PARAMVALUE(VT_Km_basal));  // Km_basal  [uM]
  sdata.p[24] = (CELLMODEL_PARAMVALUE(VT_Km_gsa));        // Km_gsa    [uM]
  sdata.p[25] = (CELLMODEL_PARAMVALUE(VT_Km_fsk));        // Km_fsk    [uM]
  sdata.p[26] = (CELLMODEL_PARAMVALUE(VT_Kgsa));          // Kgsa      [uM]
  sdata.p[27] = (CELLMODEL_PARAMVALUE(VT_Kfsk));          // Kfsk      [uM]
  sdata.p[28] = (CELLMODEL_PARAMVALUE(VT_k_pde3));        // k_pde3    [1/sec]
  sdata.p[29] = (CELLMODEL_PARAMVALUE(VT_Km_pde3));       // Km_pde3   [uM]
  sdata.p[30] = (CELLMODEL_PARAMVALUE(VT_k_pde4));        // k_pde4    [1/sec]
  sdata.p[31] = (CELLMODEL_PARAMVALUE(VT_Km_pde4));       // Km_pde4   [uM]
  sdata.p[32] = (CELLMODEL_PARAMVALUE(VT_Ki_ibmx));       // Ki_ibmx   [uM]
  // PKA module
  sdata.p[33] = (CELLMODEL_PARAMVALUE(VT_PKAItot));       // PKAItot   [uM]
  sdata.p[34] = (CELLMODEL_PARAMVALUE(VT_PKAIItot));      // PKAIItot  [uM]
  sdata.p[35] = (CELLMODEL_PARAMVALUE(VT_PKItot));        // PKItot    [uM]
  sdata.p[36] = (CELLMODEL_PARAMVALUE(VT_Ka));            // Ka        [uM]
  sdata.p[37] = (CELLMODEL_PARAMVALUE(VT_Kb));            // Kb        [uM]
  sdata.p[38] = (CELLMODEL_PARAMVALUE(VT_Kd));            // Kd        [uM]
  sdata.p[39] = (CELLMODEL_PARAMVALUE(VT_Ki_pki));  // Ki_pki    [uM]
  // PLB module
  sdata.p[40] = (CELLMODEL_PARAMVALUE(VT_epsilon));       // epsilon   [none]
  sdata.p[41] = (CELLMODEL_PARAMVALUE(VT_PLBtot));        // PLBtot    [uM]
  sdata.p[42] = (CELLMODEL_PARAMVALUE(VT_PP1tot));        // PP1tot    [uM]
  sdata.p[43] = (CELLMODEL_PARAMVALUE(VT_Inhib1tot));     // Inhib1tot [uM]
  sdata.p[44] = (CELLMODEL_PARAMVALUE(VT_k_pka_plb));     // k_pka_plb     [1/sec]
  sdata.p[45] = (CELLMODEL_PARAMVALUE(VT_Km_pka_plb));    // Km_pka_plb    [uM]
  sdata.p[46] = (CELLMODEL_PARAMVALUE(VT_k_pp1_plb));     // k_pp1_plb     [1/sec]
  sdata.p[47] = (CELLMODEL_PARAMVALUE(VT_Km_pp1_plb));    // Km_pp1_plb    [uM]
  sdata.p[48] = (CELLMODEL_PARAMVALUE(VT_k_pka_i1));      // k_pka_i1      [1/sec]
  sdata.p[49] = (CELLMODEL_PARAMVALUE(VT_Km_pka_i1));     // Km_pka_i1     [uM]
  sdata.p[50] = (CELLMODEL_PARAMVALUE(VT_Vmax_pp2a_i1));  // Vmax_pp2a_i1  [uM/sec]
  sdata.p[51] = (CELLMODEL_PARAMVALUE(VT_Km_pp2a_i1));    // Km_pp2a_i1    [uM]
  sdata.p[52] = (CELLMODEL_PARAMVALUE(VT_Ki_inhib1));     // Ki_inhib1     [uM]
  // LCC module
  sdata.p[53] = (CELLMODEL_PARAMVALUE(VT_LCCtot));        // LCCtot        [uM]
  sdata.p[54] = (CELLMODEL_PARAMVALUE(VT_PKAIIlcctot));   // PKAIIlcctot   [uM]
  sdata.p[55] = (CELLMODEL_PARAMVALUE(VT_PP1lcctot));     // PP1lcctot     [uM]
  sdata.p[56] = (CELLMODEL_PARAMVALUE(VT_PP2Alcctot));    // PP2Alcctot    [uM]
  sdata.p[57] = (CELLMODEL_PARAMVALUE(VT_k_pka_lcc));     // k_pka_lcc     [1/sec]
  sdata.p[58] = (CELLMODEL_PARAMVALUE(VT_Km_pka_lcc));    // Km_pka_lcc    [uM]
  sdata.p[59] = (CELLMODEL_PARAMVALUE(VT_k_pp1_lcc));     // k_pp1_lcc     [1/sec]
  sdata.p[60] = (CELLMODEL_PARAMVALUE(VT_Km_pp1_lcc));    // Km_pp1_lcc    [uM]
  sdata.p[61] = (CELLMODEL_PARAMVALUE(VT_k_pp2a_lcc));    // k_pp2a_lcc    [1/sec]
  sdata.p[62] = (CELLMODEL_PARAMVALUE(VT_Km_pp2a_lcc));   // Km_pp2a_lcc   [uM]
  // RyR module
  sdata.p[63] = (CELLMODEL_PARAMVALUE(VT_RyRtot));        // RyRtot        [uM]
  sdata.p[64] = (CELLMODEL_PARAMVALUE(VT_PKAIIryrtot));   // PKAIIryrtot   [uM]
  sdata.p[65] = (CELLMODEL_PARAMVALUE(VT_PP1ryr));        // PP1ryr        [uM]
  sdata.p[66] = (CELLMODEL_PARAMVALUE(VT_PP2Aryr));       // PP2Aryr       [uM]
  sdata.p[67] = (CELLMODEL_PARAMVALUE(VT_kcat_pka_ryr));  // kcat_pka_ryr  [1/sec]
  sdata.p[68] = (CELLMODEL_PARAMVALUE(VT_Km_pka_ryr));    // Km_pka_ryr    [uM]
  sdata.p[69] = (CELLMODEL_PARAMVALUE(VT_kcat_pp1_ryr));  // kcat_pp1_ryr  [1/sec]
  sdata.p[70] = (CELLMODEL_PARAMVALUE(VT_Km_pp1_ryr));    // Km_pp1_ryr    [uM]
  sdata.p[71] = (CELLMODEL_PARAMVALUE(VT_kcat_pp2a_ryr)); // kcat_pp2a_ryr [1/sec]
  sdata.p[72] = (CELLMODEL_PARAMVALUE(VT_Km_pp2a_ryr));   // Km_pp2a_ryr   [uM]
  // TnI module
  sdata.p[73] = (CELLMODEL_PARAMVALUE(VT_TnItot));        // TnItot        [uM]
  sdata.p[74] = (CELLMODEL_PARAMVALUE(VT_PP2Atni));       // PP2Atni       [uM]
  sdata.p[75] = (CELLMODEL_PARAMVALUE(VT_kcat_pka_tni));  // kcat_pka_tni  [1/sec]
  sdata.p[76] = (CELLMODEL_PARAMVALUE(VT_Km_pka_tni));    // Km_pka_tni    [uM]
  sdata.p[77] = (CELLMODEL_PARAMVALUE(VT_kcat_pp2a_tni)); // kcat_pp2a_tni [1/sec]
  sdata.p[78] = (CELLMODEL_PARAMVALUE(VT_Km_pp2a_tni));   // Km_pp2a_tni   [uM]
  // Iks module
  sdata.p[79] = (CELLMODEL_PARAMVALUE(VT_Iks_tot));       // Iks_tot       [uM]
  sdata.p[80] = (CELLMODEL_PARAMVALUE(VT_Yotiao_tot));    // Yotiao_tot    [uM]
  sdata.p[81] = (CELLMODEL_PARAMVALUE(VT_K_yotiao));  // K_yotiao      [uM] ** apply G589D mutation here **
  sdata.p[82] = (CELLMODEL_PARAMVALUE(VT_PKAII_ikstot));  // PKAII_ikstot  [uM]
  sdata.p[83] = (CELLMODEL_PARAMVALUE(VT_PP1_ikstot));    // PP1_ikstot    [uM]
  sdata.p[84] = (CELLMODEL_PARAMVALUE(VT_k_pka_iks));     // k_pka_iks     [1/sec]
  sdata.p[85] = (CELLMODEL_PARAMVALUE(VT_Km_pka_iks));    // Km_pka_iks    [uM]
  sdata.p[86] = (CELLMODEL_PARAMVALUE(VT_k_pp1_iks));     // k_pp1_iks     [1/sec]
  sdata.p[87] = (CELLMODEL_PARAMVALUE(VT_Km_pp1_iks));    // Km_pp1_iks    [uM]
  //// ---- EC Coupling model parameters ------
  // universal parameters
  sdata.p[88] = (CELLMODEL_PARAMVALUE(VT_Vmyo));          // Vmyo  [uL]
  sdata.p[89] = (CELLMODEL_PARAMVALUE(VT_Vnsr));          // Vnsr  [uL]
  sdata.p[90] = (CELLMODEL_PARAMVALUE(VT_Vjsr));          // Vjsr  [uL]
  sdata.p[91] = (CELLMODEL_PARAMVALUE(VT_ACap));          // ACap  [cm^2] with C = 1 uF/cm^2
  sdata.p[92] = (CELLMODEL_PARAMVALUE(VT_Tx));            // Temp  [K]
  // extracellular concentrations
  sdata.p[93] = (CELLMODEL_PARAMVALUE(VT_Na_o));          // Extracellular Na  [mM]
  sdata.p[94] = (CELLMODEL_PARAMVALUE(VT_K_o));           // Extracellular K   [mM]
  sdata.p[95] = (CELLMODEL_PARAMVALUE(VT_Ca_o));          // Extracellular Ca  [mM]
  // current conductances
  sdata.p[96] = (CELLMODEL_PARAMVALUE(VT_g_Na));         // G_Na      [mS/uF]
  sdata.p[97] = (CELLMODEL_PARAMVALUE(VT_g_to));         // G_to      [mS/uF]
  sdata.p[98] = (CELLMODEL_PARAMVALUE(VT_g_Kr));         // G_kro     [mS/uF]
  sdata.p[99] = (CELLMODEL_PARAMVALUE(VT_g_K1));         // G_kibar   [mS/uF]
  sdata.p[100] = (CELLMODEL_PARAMVALUE(VT_g_pK));         // G_kp      [mS/uF]
  sdata.p[101] = (CELLMODEL_PARAMVALUE(VT_g_Ks));         // Gkso       [mS/uF]
  // I_Ca parameters
  sdata.p[102] = (CELLMODEL_PARAMVALUE(VT_Nifi));         // Nifidipine[uM]
  sdata.p[103] = (CELLMODEL_PARAMVALUE(VT_IC50_nif));     // IC50_nif  [uM]
  sdata.p[104] = (CELLMODEL_PARAMVALUE(VT_g_CaNaL));  // pNa       [cm/sec]
  sdata.p[105] = (CELLMODEL_PARAMVALUE(VT_g_CaL));  // pCa       [cm/sec]
  sdata.p[106] = (CELLMODEL_PARAMVALUE(VT_g_CaKL));  // pK        [cm/sec]
  sdata.p[107] = (CELLMODEL_PARAMVALUE(VT_KmCa));  // KmCa      [mM]
  sdata.p[108] = 0;

  // pumps and background currents
  sdata.p[109] = (CELLMODEL_PARAMVALUE(VT_kNaCa)); // k_NaCa    [uA/uF]
  sdata.p[110] = (CELLMODEL_PARAMVALUE(VT_KmNa));  // Km_Na     [mM]
  sdata.p[111] = (CELLMODEL_PARAMVALUE(VT_Km_Ca)); // Km_Ca     [mM]
  sdata.p[112] = (CELLMODEL_PARAMVALUE(VT_ksat));  // k_sat     [none]
  sdata.p[113] = (CELLMODEL_PARAMVALUE(VT_n));     // eta       [none]
  sdata.p[114] = (CELLMODEL_PARAMVALUE(VT_knak));  // ibarnak   [uA/uF]
  sdata.p[115] = (CELLMODEL_PARAMVALUE(VT_KmNai)); // Km_Nai    [mM]
  sdata.p[116] = (CELLMODEL_PARAMVALUE(VT_KmK));   // Km_Ko     [mM]
  sdata.p[117] = (CELLMODEL_PARAMVALUE(VT_g_pCa)); // ibarpca   [uA/uF]
  sdata.p[118] = (CELLMODEL_PARAMVALUE(VT_KpCa));  // Km_pca    [mM]
  sdata.p[119] = (CELLMODEL_PARAMVALUE(VT_g_bCa)); // G_Cab     [uA/uF]
  sdata.p[120] = (CELLMODEL_PARAMVALUE(VT_g_bNa));  // G_Nab     [uA/uF]
  sdata.p[121] = (CELLMODEL_PARAMVALUE(VT_Pns));  // Pns       [cm/sec]
  sdata.p[122] = (CELLMODEL_PARAMVALUE(VT_Kmns));  // Km_ns     [mM]
  // Calcium handling parameters
  sdata.p[123] = (CELLMODEL_PARAMVALUE(VT_I_upbar));  // I_upbar   [mM/sec]
  sdata.p[124] = (CELLMODEL_PARAMVALUE(VT_KmUp));      // Km_up     [mM]
  sdata.p[125] = (CELLMODEL_PARAMVALUE(VT_nsrbar));    // nsrbar    [mM]
  sdata.p[126] = 2e-3;              // tauon     [sec]
  sdata.p[127] = 2e-3;              // tauoff    [sec]
  sdata.p[128] = (CELLMODEL_PARAMVALUE(VT_gmaxrel));   // gmaxrel   [mM/sec]
  sdata.p[129] = (CELLMODEL_PARAMVALUE(VT_gmaxrelol)); // gmaxrelol [mM/sec]
  sdata.p[130] = 0.8e-3;  // Km_rel    [mM]
  sdata.p[131] = (CELLMODEL_PARAMVALUE(VT_CSQNth));    // CSQNth    [mM]
  sdata.p[132] = (CELLMODEL_PARAMVALUE(VT_CSQNbar));   // CSQNbar   [mM]
  sdata.p[133] = (CELLMODEL_PARAMVALUE(VT_KmCsqn));  // Km_csqn   [mM]
  sdata.p[134] = (CELLMODEL_PARAMVALUE(VT_tau_tr));    // tau_tr    [sec]
  sdata.p[135] = (CELLMODEL_PARAMVALUE(VT_TRPNbar));   // TRPNbar   [mM]
  sdata.p[136] = (CELLMODEL_PARAMVALUE(VT_CMDNbar));   // CMDNbar   [mM]
  sdata.p[137] = (CELLMODEL_PARAMVALUE(VT_INDObar));   // INDObar   [mM]
  sdata.p[138] = (CELLMODEL_PARAMVALUE(VT_Km_trpn));   // Km_trpn   [mM]
  sdata.p[139] = (CELLMODEL_PARAMVALUE(VT_Km_cmdn));   // Km_cmdn   [mM]
  sdata.p[140] = (CELLMODEL_PARAMVALUE(VT_Km_indo));   // Km_indo   [mM]
  // Simulation parameters
  sdata.p[141] = 1.0;  // rate [Hz]   ** change pacing rate here if own stimulation is applied**
  sdata.p[142] = -80;  // Vhold
  sdata.p[143] = -80;  // Vtest
  sdata.p[144] = 0;  // later I_app
  sdata.p[145] = 0;  // clamp voltage
  sdata.p[146] = 0; // clamp voltage set?
  sdata.p[147] = 1;  // first calc loop?

  // Give solver access to user data/ parameters via rdata
  rdata = &sdata;

  // Initialize mass matrix (1=ODE, 0=DAE)
  for (int i = 0; i < 49; i++) {
    m0[i] = 1.0;
  }
  m0[1] = 0;
  m0[2] = 0;
  m0[3] = 0;
  m0[10] = 0;
  m0[11] = 0;
  m0[12] = 0;
  m0[13] = 0;
  m0[14] = 0;
  m0[16] = 0;
  m0[17] = 0;
  m0[18] = 0;
  m0[21] = 0;
  m0[22] = 0;
  m0[27] = 0;
  m0[28] = 0;


  // Initialize y with variable values
  y0[0] = trel;

  // beta-AR module
  y0[1] = L;
  y0[2] = R;
  y0[3] = Gs;
  y0[4] = b1ARtot;
  y0[5] = b1ARd;
  y0[6] = b1ARp;
  y0[7] = Gsagtptot;
  y0[8] = Gsagdp;
  y0[9] = Gsbg;

  // cAMP module
  y0[10] = Gsa_gtp;
  y0[11] = Fsk;
  y0[12] = AC;
  y0[13] = PDE;
  y0[14] = IBMX;
  y0[15] = cAMPtot;

  // PKA module
  y0[16] = cAMP;
  y0[17] = PKACI;
  y0[18] = PKACII;

  // Target Protein Complexes
  y0[19] = PLBs;
  y0[20] = Inhib1ptot;
  y0[21] = Inhib1p;
  y0[22] = PP1;
  y0[23] = LCCap;
  y0[24] = LCCbp;
  y0[25] = RyRp;
  y0[26] = TnIp;
  y0[27] = Iks;
  y0[28] = Yotiao;
  y0[29] = Iksp;

  // Gates
  y0[30] = m;
  y0[31] = h;
  y0[32] = j;
  y0[33] = d;
  y0[34] = f;
  y0[35] = 0;
  y0[36] = 0;
  y0[37] = 0;
  y0[38] = r;
  y0[39] = s;
  y0[40] = xr1;
  y0[41] = xs;
  y0[42] = 0;

  // Ion concentrations
  y0[43] = Ca_nsr;
  y0[44] = Ca_jsr;
  y0[45] = Na_i;
  y0[46] = K_i;
  y0[47] = Ca_i;
  y0[48] = Vm_old;  // in mV

  alloc();
} // Saucerman::Init

ML_CalcType Saucerman::Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                            int euler) {
  ML_CalcType svolt = V * 1000;  // membrane voltage in mV

  // const int Vi=(int)(DivisionTab*(RangeTabhalf+svolt)+.5); //array position

  sdata.p[144] = (realtype) i_external;

  // this block is only required if user data from outside shpuld be applied (e.g. V during voltage clamping)
  y0[0] = trel;
  y0[1] = L;
  y0[2] = R;
  y0[3] = Gs;
  y0[4] = b1ARtot;
  y0[5] = b1ARd;
  y0[6] = b1ARp;
  y0[7] = Gsagtptot;
  y0[8] = Gsagdp;
  y0[9] = Gsbg;
  y0[10] = Gsa_gtp;
  y0[11] = Fsk;
  y0[12] = AC;
  y0[13] = PDE;
  y0[14] = IBMX;
  y0[15] = cAMPtot;
  y0[16] = cAMP;
  y0[17] = PKACI;
  y0[18] = PKACII;
  y0[19] = PLBs;
  y0[20] = Inhib1ptot;
  y0[21] = Inhib1p;
  y0[22] = PP1;
  y0[23] = LCCap;
  y0[24] = LCCbp;
  y0[25] = RyRp;
  y0[26] = TnIp;
  y0[27] = Iks;
  y0[28] = Yotiao;
  y0[29] = Iksp;
  y0[30] = m;
  y0[31] = h;
  y0[32] = j;
  y0[33] = d;
  y0[34] = f;
  y0[35] = 0.0;
  y0[36] = 0.0;
  y0[37] = 0.0;
  y0[38] = r;
  y0[39] = s;
  y0[40] = xr1;
  y0[41] = xs;
  y0[42] = 0.0;
  y0[43] = Ca_nsr;
  y0[44] = Ca_jsr;
  y0[45] = Na_i;
  y0[46] = K_i;
  y0[47] = Ca_i;
  if ((sdata.p[147] == 1) && (y0[48] != svolt)) {
    sdata.p[146] = 1;  // clamped voltage!
  }
  y0[48] = svolt;

  for (int i = 0; i < 49; i++) {
    NV_Ith_S(w, i) = y0[i];
  }

  sdata.p[147] = 0;  // first loop passed

  // Define time of next solver output
  tout = t + tinc;

  sdata.p[145] = svolt;

  // Integrate the DAE over an interval in t
  flag = IDASolve(ida_mem, tout, &t, w, wd,
                  IDA_NORMAL);  // IDA_NORMAL: solver may pass the user specified tout time
  // and returns an interpolated value
  // if(check_flag(&flag, "IDASolve", 1)) exit(1);
  // PrintFinalStats(ida_mem);

  // Return solution vector to variables for printing
  ML_CalcType Vm = (ML_CalcType) NV_Ith_S(w, 48);

  if (sdata.p[146] == 1) {
    ML_CalcType Vm = svolt;
  }

  m = (ML_CalcType) NV_Ith_S(w, 30);
  h = (ML_CalcType) NV_Ith_S(w, 31);
  j = (ML_CalcType) NV_Ith_S(w, 32);
  d = (ML_CalcType) NV_Ith_S(w, 33);
  f = (ML_CalcType) NV_Ith_S(w, 34);
  r = (ML_CalcType) NV_Ith_S(w, 38);
  s = (ML_CalcType) NV_Ith_S(w, 39);
  xr1 = (ML_CalcType) NV_Ith_S(w, 40);
  xs = (ML_CalcType) NV_Ith_S(w, 41);
  Ca_i = (ML_CalcType) NV_Ith_S(w, 47);
  Na_i = (ML_CalcType) NV_Ith_S(w, 45);
  K_i = (ML_CalcType) NV_Ith_S(w, 46);
  Ca_jsr = (ML_CalcType) NV_Ith_S(w, 44);
  Ca_nsr = (ML_CalcType) NV_Ith_S(w, 43);
  trel = (ML_CalcType) NV_Ith_S(w, 0);

  // Intracellular Signaling Cascade
  L = (ML_CalcType) NV_Ith_S(w, 1);
  R = (ML_CalcType) NV_Ith_S(w, 2);
  Gs = (ML_CalcType) NV_Ith_S(w, 3);
  b1ARtot = (ML_CalcType) NV_Ith_S(w, 4);
  b1ARd = (ML_CalcType) NV_Ith_S(w, 5);
  b1ARp = (ML_CalcType) NV_Ith_S(w, 6);
  Gsagtptot = (ML_CalcType) NV_Ith_S(w, 7);
  Gsagdp = (ML_CalcType) NV_Ith_S(w, 8);
  Gsbg = (ML_CalcType) NV_Ith_S(w, 9);
  Gsa_gtp = (ML_CalcType) NV_Ith_S(w, 10);
  Fsk = (ML_CalcType) NV_Ith_S(w, 11);
  AC = (ML_CalcType) NV_Ith_S(w, 12);
  PDE = (ML_CalcType) NV_Ith_S(w, 13);
  IBMX = (ML_CalcType) NV_Ith_S(w, 14);
  cAMPtot = (ML_CalcType) NV_Ith_S(w, 15);
  cAMP = (ML_CalcType) NV_Ith_S(w, 16);
  PKACI = (ML_CalcType) NV_Ith_S(w, 17);
  PKACII = (ML_CalcType) NV_Ith_S(w, 18);
  PLBs = (ML_CalcType) NV_Ith_S(w, 19);
  Inhib1ptot = (ML_CalcType) NV_Ith_S(w, 20);
  Inhib1p = (ML_CalcType) NV_Ith_S(w, 21);
  PP1 = (ML_CalcType) NV_Ith_S(w, 22);
  LCCap = (ML_CalcType) NV_Ith_S(w, 23);
  LCCbp = (ML_CalcType) NV_Ith_S(w, 24);
  RyRp = (ML_CalcType) NV_Ith_S(w, 25);
  TnIp = (ML_CalcType) NV_Ith_S(w, 26);
  Iks = (ML_CalcType) NV_Ith_S(w, 27);
  Yotiao = (ML_CalcType) NV_Ith_S(w, 28);
  Iksp = (ML_CalcType) NV_Ith_S(w, 29);

  // required to be able to return Vm to Elphy
  ML_CalcType dVm = Vm_old - Vm;
  Vm_old = Vm;
  return -dVm / 1000;
} // Saucerman::Calc

// Function to be solved by IDA
static int resrob(realtype t, N_Vector w, N_Vector wd, N_Vector rr, void *rdata) {
  /* Note: When looping over the components of an N_Vector v, it is
   * more efficient to first obtain the component array via
   * v_data = NV_DATA_S(v) and then access v_data[i] within the
   * loop than it is to use NV_Ith_S(v,i) within the loop.*/

  // Initialize an array with variable values (gates, concentrations, voltage, ..., see top)
  realtype y[49] = {0.0};
  realtype *w_data = NV_DATA_S(w);

  for (int kappa = 0; kappa < 49; kappa++) {
    y[kappa] = w_data[kappa];
  }

  // Give user data to an array
  UserData *sdata;
  sdata = static_cast<UserData *>(rdata);
  realtype *p = &(sdata->p[0]);

  const realtype Vm_non_clamp = y[48];

  if (p[146] == 1) {
    y[48] = p[145];
  }

  // rr / rval is the vector / array that has to be optimized to ZERO
  realtype *rval;
  rval = NV_DATA_S(rr);

  // array for the differential values of w
  realtype *ypval;
  ypval = NV_DATA_S(wd);

  // Introduce array for equation value allocation
  realtype ydot[49] = {0.0};


  // SIGNALING MODEL

  //// b-AR module
  realtype LR = y[1] * y[2] / p[4];
  realtype LRG = LR * y[3] / p[5];
  realtype RG = y[2] * y[3] / p[6];
  realtype BARKDESENS = p[7] * (LR + LRG);
  realtype BARKRESENS = p[8] * y[5];
  realtype PKADESENS = p[9] * y[17] * y[4];
  realtype PKARESENS = p[10] * y[6];
  realtype GACT = p[11] * (RG + LRG);
  realtype HYD = p[12] * y[7];
  realtype REASSOC = p[13] * y[8] * y[9];
  ydot[1] = p[1] - LR - LRG - y[1];
  ydot[2] = y[4] - LR - LRG - RG - y[2];
  ydot[3] = p[3] - LRG - RG - y[3];
  ydot[4] = (BARKRESENS - BARKDESENS) + (PKARESENS - PKADESENS);
  ydot[5] = BARKDESENS - BARKRESENS;
  ydot[6] = PKADESENS - PKARESENS;
  ydot[7] = GACT - HYD;
  ydot[8] = HYD - REASSOC;
  ydot[9] = GACT - REASSOC;

  // end b-AR module

  //// cAMP module
  realtype Gsa_gtp_AC = y[10] * y[12] / p[26];
  realtype Fsk_AC = y[11] * y[12] / p[27];
  realtype AC_ACT_BASAL = p[20] * y[12] * p[15] / (p[23] + p[15]);
  realtype AC_ACT_GSA = p[21] * Gsa_gtp_AC * p[15] / (p[24] + p[15]);
  realtype AC_ACT_FSK = p[22] * Fsk_AC * p[15] / (p[25] + p[15]);
  realtype PDE3_ACT = p[28] * y[13] * y[16] / (p[29] + y[16]);
  realtype PDE4_ACT = p[30] * y[13] * y[16] / (p[31] + y[16]);
  realtype PDE_IBMX = y[13] * y[14] / p[32];
  ydot[10] = y[7] - Gsa_gtp_AC - y[10];
  ydot[11] = p[19] - Fsk_AC - y[11];
  ydot[12] = p[14] - Gsa_gtp_AC -
             y[12]; // note: assumes Fsk = 0.  Change Gsa_gtp_AC to Fsk_AC for Forskolin.
  ydot[13] = p[17] - PDE_IBMX - y[13];
  ydot[14] = p[18] - PDE_IBMX - y[14];
  ydot[15] = AC_ACT_BASAL + AC_ACT_GSA + AC_ACT_FSK - PDE3_ACT - PDE4_ACT;

  // end cAMP module

  //// PKA module
  realtype PKI = p[35] * p[39] / (p[39] + y[17] + y[18]);
  realtype A2RC_I = (y[17] / p[38]) * y[17] * (1 + PKI / p[39]);
  realtype A2R_I = y[17] * (1 + PKI / p[39]);
  realtype A2RC_II = (y[18] / p[38]) * y[18] * (1 + PKI / p[39]);
  realtype A2R_II = y[18] * (1 + PKI / p[39]);
  realtype ARC_I = (p[36] / y[16]) * A2RC_I;
  realtype ARC_II = (p[36] / y[16]) * A2RC_II;
  ydot[16] = y[15] - (ARC_I + 2 * A2RC_I + 2 * A2R_I) - (ARC_II + 2 * A2RC_II + 2 * A2R_II) - y[16];
  realtype PKAtemp = p[36] * p[37] / p[38] + p[36] * y[16] / p[38] + y[16] * y[16] / p[38];
  ydot[17] =
      2 * p[33] * y[16] * y[16] - y[17] * (1 + PKI / p[39]) * (PKAtemp * y[17] + y[16] * y[16]);
  ydot[18] =
      2 * p[34] * y[16] * y[16] - y[18] * (1 + PKI / p[39]) * (PKAtemp * y[18] + y[16] * y[16]);

  // end PKA module

  //// PLB module
  realtype PLB = p[41] - y[19];
  realtype PLB_PHOSPH = p[44] * y[17] * PLB / (p[45] + PLB);
  realtype PLB_DEPHOSPH = p[46] * y[22] * y[19] / (p[47] + y[19]);
  ydot[19] = PLB_PHOSPH - PLB_DEPHOSPH;

  realtype Inhib1 = p[43] - y[20];
  realtype Inhib1p_PP1 = y[21] * y[22] / p[52];
  realtype Inhib1_PHOSPH = p[48] * y[17] * Inhib1 / (p[49] + Inhib1);
  realtype Inhib1_DEPHOSPH = p[50] * y[20] / (p[51] + y[20]);
  ydot[20] = Inhib1_PHOSPH - Inhib1_DEPHOSPH;
  ydot[21] = y[20] - Inhib1p_PP1 - y[21];
  ydot[22] = p[42] - Inhib1p_PP1 - y[22];

  // realtype fracPLBp = y[19]/p[41];
  realtype fracPLB = PLB / p[41];
  realtype fracPLBo = 0.9926;

  // end PLB module

  //// LCC module
  realtype PKAClcc = (p[54] / p[34]) * y[18];
  realtype LCCa = p[53] - y[23];
  realtype LCCa_PHOSPH = p[40] * p[57] * PKAClcc * LCCa / (p[58] + p[40] * LCCa);
  realtype LCCa_DEPHOSPH = p[40] * p[61] * p[56] * y[23] / (p[62] + p[40] * y[23]);
  ydot[23] = LCCa_PHOSPH - LCCa_DEPHOSPH;
  realtype fracLCCap = y[23] / p[53];
  realtype fracLCCapo = 0.028;

  realtype LCCb = p[53] - y[24];
  realtype LCCb_PHOSPH = p[40] * p[57] * PKAClcc * LCCb / (p[58] + p[40] * LCCb);
  realtype LCCb_DEPHOSPH = p[40] * p[59] * p[55] * y[24] / (p[60] + p[40] * y[24]);
  ydot[24] = LCCb_PHOSPH - LCCb_DEPHOSPH;
  realtype fracLCCbp = y[24] / p[53];
  realtype fracLCCbpo = 0.0328;

  // end LCC module

  //// RyR module
  realtype PKACryr = (p[64] / p[34]) * y[18];
  realtype RyR = p[63] - y[25];
  realtype RyRPHOSPH = p[40] * p[67] * PKACryr * RyR / (p[68] + p[40] * RyR);
  realtype RyRDEPHOSPH1 = p[40] * p[69] * p[65] * y[25] / (p[70] + p[40] * y[25]);
  realtype RyRDEPHOSPH2A = p[40] * p[71] * p[66] * y[25] / (p[72] + p[40] * y[25]);
  ydot[25] = RyRPHOSPH - RyRDEPHOSPH1 - RyRDEPHOSPH2A;
  realtype fracRyRp = y[25] / p[63];
  realtype fracRyRpo = 0.0244;

  // end RyR module

  //// TnI module
  realtype TnI = p[73] - y[26];
  realtype TnIPHOSPH = p[75] * y[17] * TnI / (p[76] + TnI);
  realtype TnIDEPHOSPH = p[77] * p[74] * y[26] / (p[78] + y[26]);
  ydot[26] = TnIPHOSPH - TnIDEPHOSPH;
  realtype fracTnIp = y[26] / p[73];
  realtype fracTnIpo = 0.0031;

  // end TnI module

  //// Iks module
  realtype IksYot = y[27] * y[28] / p[81]; // [uM]
  ydot[27] = p[79] - IksYot - y[27];   // [uM]
  ydot[28] = p[80] - IksYot - y[28];   // [uM]

  realtype PKACiks = (IksYot / p[79]) * (p[82] / p[34]) * y[18];
  realtype PP1iks = (IksYot / p[79]) * p[83];
  realtype Iks = p[79] - y[29];
  realtype IKS_PHOSPH = p[40] * p[84] * PKACiks * Iks / (p[85] + p[40] * Iks);
  realtype IKS_DEPHOSPH = p[40] * p[86] * PP1iks * y[29] / (p[87] + p[40] * y[29]);
  ydot[29] = IKS_PHOSPH - IKS_DEPHOSPH;
  realtype fracIksp = y[29] / p[79];
  realtype fracIkspo = 1.8e-3 / p[79];

  // end Iks module
  // -------- END SIGNALING MODEL ---------


  //// -------- EC COUPLING MODEL -----------

  //// Constants
  realtype R = 8314;                                                   // R     [J/kmol*K]
  realtype Frdy = 96485;                                                  // Frdy     [C/mol]
  realtype FoRT = Frdy / (R * p[92]);
  realtype zna = 1;                                                      // Na valence
  realtype zk = 1;                                                      // K valence
  realtype zca = 2;                                                      // Ca valence
  // Nernst Potentials
  realtype ena =
      (1 / (FoRT * zna)) * log(p[93] / y[45]);                       // should be 70.54 mV
  realtype ek =
      (1 / (FoRT * zk)) * log(p[94] / y[46]);                        // should be -87.94 mV
  realtype eca = (1 / (FoRT * zca)) * log(p[95] / y[47]);                       // should be 120 mV
  realtype prnak = 0.01833;
  realtype eks =
      (1 / FoRT) * log((p[94] + prnak * p[93]) / (y[46] + prnak * y[45])); // should be -77.54
  realtype ecl = -40.0;

  //// I_Na: Fast Na Current
  realtype am = 0.32 * (y[48] + 47.13) / (1 - exp(-0.1 * (y[48] + 47.13)));
  realtype bm = 0.08 * exp(-y[48] / 11);
  realtype ah, bh, aj, bj;
  if (y[48] >= -40) {
    ah = 0;
    aj = 0;
    bh = 1 / (0.13 * (1 + exp(-(y[48] + 10.66) / 11.1)));
    bj = 0.3 * exp(-2.535e-7 * y[48]) / (1 + exp(-0.1 * (y[48] + 32)));
  } else {
    ah = 0.135 * exp((80 + y[48]) / -6.8);
    bh = 3.56 * exp(0.079 * y[48]) + 3.1e5 * exp(0.35 * y[48]);
    aj = (-1.2714e5 * exp(0.2444 * y[48]) - 3.474e-5 * exp(-0.04391 * y[48])) * (y[48] + 37.78) /
         (1 + exp(0.311 * (y[48] + 79.23)));
    bj = 0.1212 * exp(-0.01052 * y[48]) / (1 + exp(-0.1378 * (y[48] + 40.14)));
  }

  ydot[30] = 1e3 * (am * (1 - y[30]) - bm * y[30]);
  ydot[31] = 1e3 * (ah * (1 - y[31]) - bh * y[31]);
  ydot[32] = 1e3 * (aj * (1 - y[32]) - bj * y[32]);
  realtype I_Na = p[96] * y[30] * y[30] * y[30] * y[31] * y[32] * (y[48] - ena);

  //// I_Ca: L-type Calcium Current
  realtype dss = 1 / (1 + exp(-(y[48] + 0.01) / 6.24));
  realtype taud = dss * (1 - exp(-(y[48] + 0.01) / 6.24)) / (0.035 * (y[48] + 0.01));
  realtype fss = 1 / (1 + exp((y[48] + 35.06) / 8.6)) + 0.6 / (1 + exp((50 - y[48]) / 20));
  realtype tauf = 1 / (0.0197 * exp(-(0.0337 * (y[48] + 0.01)) * (0.0337 * (y[48] + 0.01))) + 0.02);
  ydot[33] = 1e3 * (dss - y[33]) / taud;
  ydot[34] = 1e3 * (fss - y[34]) / tauf;

  realtype ibarca =
      p[105] * 4 * (y[48] * Frdy * FoRT) * (y[47] * exp(2 * y[48] * FoRT) - 0.341 * p[95]) /
      (exp(2 * y[48] * FoRT) - 1);
  realtype ibark =
      p[106] * (y[48] * Frdy * FoRT) * (0.75 * y[46] * exp(y[48] * FoRT) - 0.75 * p[94]) /
      (exp(y[48] * FoRT) - 1);
  realtype ibarna =
      p[104] * (y[48] * Frdy * FoRT) * (0.75 * y[45] * exp(y[48] * FoRT) - 0.75 * p[93]) /
      (exp(y[48] * FoRT) - 1);
  realtype fCa = 1 / (1 + (y[47] / p[107]) * (y[47] / p[107]));
  realtype favail = 1 * (0.05 * fracLCCbp / fracLCCbpo + 0.95); // PHOSPHOREGULATION
  realtype fpo = 1 * (0.03 * fracLCCap / fracLCCapo + 0.97); // PHOSPHOREGULATION
  realtype rel = p[103] / (p[103] + p[102]);             // effect of nifedipine (ICa blocker)
  realtype I_Ca = rel * ibarca * favail * fpo * y[33] * y[34] * fCa;
  realtype I_CaK = rel * ibark * favail * fpo * y[33] * y[34] * fCa;
  realtype I_CaNa = rel * ibarna * favail * fpo * y[33] * y[34] * fCa;

  // realtype I_Catot = I_Ca+I_CaK+I_CaNa;

  //// I_to: Transient Outward K Current
  realtype axto = 0.04561 * exp(0.03577 * y[48]);
  realtype bxto = 0.0989 * exp(-0.06237 * y[48]);
  ydot[38] = 1e3 * (axto * (1 - y[38]) - bxto * y[38]);
  realtype ayto = 0.005415 * exp(-(y[48] + 33.5) / 5) / (1 + 0.051335 * exp(-(y[48] + 33.5) / 5));
  realtype byto = 0.005415 * exp((y[48] + 33.5) / 5) / (1 + 0.05133 * exp((y[48] + 33.5) / 5));
  ydot[39] = 1e3 * (ayto * (1 - y[39]) - byto * y[39]);
  realtype I_to = p[97] * y[38] * y[39] * (y[48] - ek); // [uA/uF]

  //// I_kr: Rapidly Activating K Current
  realtype gkr = p[98] * sqrt(p[94] / 5.4);
  realtype xrss = 1 / (1 + exp(-(y[48] + 50) / 7.5));
  realtype tauxr = 1 / (1.38e-3 * (y[48] + 7) / (1 - exp(-0.123 * (y[48] + 7))) +
                        6.1e-4 * (y[48] + 10) / (exp(0.145 * (y[48] + 10)) - 1));
  ydot[40] = 1e3 * (xrss - y[40]) / tauxr;
  realtype rkr = 1 / (1 + exp((y[48] + 33) / 22.4));
  realtype I_kr = gkr * y[40] * rkr * (y[48] - ek);

  //// I_ks: Slowly Activating K Current
  realtype pcaks = -log10(y[47]) + 3.0;
  realtype gkso = p[101] * (0.04 + 0.133 / (1 + exp((-7.2 + pcaks) / 0.6)));
  realtype gks = gkso * (0.2 * fracIksp / fracIkspo + 0.8); // PHOSPHOREGULATION
  realtype xs05 = 1.5 - (1.5 * fracIksp / fracIkspo - 1.5);  // PHOSPHOREGULATION
  realtype xsss = 1 / (1 + exp(-(y[48] - xs05) / 16.7));
  realtype tauxs = 1 / (7.19e-5 * (y[48] + 30) / (1 - exp(-0.148 * (y[48] + 30))) +
                        1.31e-4 * (y[48] + 30) / (exp(0.0687 * (y[48] + 30)) - 1));
  ydot[41] = 1e3 * (xsss - y[41]) / tauxs;
  realtype I_ks = gks * y[41] * y[41] * (y[48] - eks);

  //// I_ki: Time-Independent K Current
  realtype aki = 1.02 / (1 + exp(0.2385 * (y[48] - ek - 59.215)));
  realtype bki =
      (0.49124 * exp(0.08032 * (y[48] + 5.476 - ek)) + exp(0.06175 * (y[48] - ek - 594.31))) /
      (1 + exp(-0.5143 * (y[48] - ek + 4.753)));
  realtype kiss = aki / (aki + bki);
  realtype I_ki = p[99] * sqrt(p[94] / 5.4) * kiss * (y[48] - ek);

  //// I_kp: Plateau K Current
  realtype kp = 1 / (1 + exp((7.488 - y[48]) / 5.98));
  realtype I_kp = p[100] * kp * (y[48] - ek);

  //// I_ncx: Na/Ca Exchanger Current
  realtype s4 = exp(p[113] * y[48] * FoRT) * y[45] * y[45] * y[45] * p[95];
  realtype s5 = exp((p[113] - 1) * y[48] * FoRT) * p[93] * p[93] * p[93] * y[47];
  realtype I_ncx = p[109] / (p[110] * p[110] * p[110] + p[93] * p[93] * p[93]) / (p[111] + p[95]) /
                   (1 + p[112] * exp((p[113] - 1) * y[48] * FoRT)) * (s4 - s5);

  //// I_nak: Na/K Pump Current
  realtype sigma = (exp(p[93] / 67.3) - 1) / 7;
  realtype fnak = 1 / (1 + 0.1245 * exp(-0.1 * y[48] * FoRT) + 0.0365 * sigma * exp(-y[48] * FoRT));
  realtype I_nak =
      p[114] * fnak / (1 + (p[115] / y[45]) * sqrt(p[115] / y[45])) * (p[94] / (p[94] + p[116]));

  //// I_ns: Nonspecific Calcium-activated Current
  realtype ibarnsk =
      p[121] * (y[48] * Frdy * FoRT) * (0.75 * y[46] * exp(y[48] * FoRT) - 0.75 * p[94]) /
      (exp(y[48] * FoRT) - 1);
  realtype ibarnsna =
      p[121] * (y[48] * Frdy * FoRT) * (0.75 * y[45] * exp(y[48] * FoRT) - 0.75 * p[93]) /
      (exp(y[48] * FoRT) - 1);
  realtype I_nsK = ibarnsk / (1 + (p[122] / y[47]) * (p[122] / y[47]) * (p[122] / y[47]));
  realtype I_nsNa = ibarnsna / (1 + (p[122] / y[47]) * (p[122] / y[47]) * (p[122] / y[47]));

  // realtype I_ns = I_nsK + I_nsNa;

  //// I_pca: Sarcolemmal Ca Pump Current
  realtype I_pca = p[117] * y[47] / (p[118] + y[47]);

  //// I_cab: Ca Background Current
  realtype I_cab = p[119] * (y[48] - eca);

  //// I_nab: Na Background Current
  realtype I_nab = p[120] * (y[48] - ena);

  //// I_ClCa: Calcium-activated Chloride Current
  realtype G_Cl = 0.010;
  realtype I_ClCa = G_Cl / (1 + 0.1e-3 / y[47]) * (y[48] - ecl);

  //// Total Membrane Currents
  realtype I_Na_tot = I_Na + I_nab + 3 * I_ncx + 3 * I_nak + I_CaNa + I_nsNa; // [uA/uF]
  realtype I_K_tot = I_to + I_kr + I_ks + I_ki + I_kp - 2 * I_nak + I_CaK + I_nsK; // [uA/uF]
  realtype I_Ca_tot = I_Ca + I_cab + I_pca - 2 * I_ncx;                 // [uA/uF]

  //// Calcium Induced Calcium Release (CICR): Faber/Rudy style
  realtype Kmrel = 5 * (2.0 / 3.0 * (1 - fracRyRp) / (1 - fracRyRpo) + 1.0 / 3.0); // [uA/uF]
  realtype trel = y[0] - 4e-3;
  realtype ryron = 1 / (1 + exp(-trel / 0.5e-3));
  realtype ryroff = 1 - 1 / (1 + exp(-trel / 0.5e-3));
  realtype grel = p[128] / (1 + exp((I_Ca_tot + Kmrel) / 0.9)) * ryron * ryroff;
  realtype I_relcicr = grel * (y[44] - y[47]); // [mM/sec]

  //// Calcium overload release

  realtype bcsqn = p[132] * y[44] / (p[133] + y[44]);
  if ((bcsqn > p[131]) && (((t - jsrol_time) > 0.5) || (std::isnan(jsrol_time))))
    jsrol_time = t; // jsrol_time = t;
  realtype I_relol = 0;

  realtype t_ol = t - jsrol_time;
  if ((t_ol >= 0) && (t_ol <= 100e-3)) {
    realtype grelol = p[129] * (1 - exp(-t_ol / 0.5e-3)) * exp(-t_ol / 0.5e-3);
    I_relol = grelol * (y[44] - y[47]); // [mM/sec]
  }


  realtype I_rel = I_relcicr + I_relol;

  //// Other SR fluxes and concentrations
  realtype Bjsr = 1 / (1 + p[132] * p[133] / ((p[133] + y[44]) * (p[133] + y[44])));
  realtype Km_up = p[124] * (3.0 / 4.0 * fracPLB / fracPLBo + 1.0 / 4.0); // PHOSPHOREGULATION

  // Shannon Iup
  realtype I_up = p[123] * (((y[47] / Km_up) * (y[47] / Km_up)) - (y[43] / 3) * (y[43] / 3)) /
                  (1 + ((y[47] / Km_up) * (y[47] / Km_up)) + (y[43] / 3) * (y[43] / 3));

  // realtype I_up = p[123]*(((y[47]/Km_up)*(y[47]/ p[124]))-(y[43]/3)*(y[43]/3))/(1+((y[47]/Km_up)*(y[47]/
  // p[124]))+(y[43]/3)*(y[43]/3));
  realtype I_leak = p[123] * y[43] / p[125];   //   [mM/sec]
  realtype I_tr = (y[43] - y[44]) / p[134];  //   [mM/sec]
  ydot[43] = I_up - I_leak - I_tr * p[90] / p[89]; //   [mM/sec]
  ydot[44] = Bjsr * (I_tr - I_rel);            //   [mM/sec]
  // realtype SRcontent = 1e3*((y[44]+y[44]/Bjsr)*p[90]/p[88]+y[43]*p[89]/p[88]);    // [umol/L cytosol]

  // Cytoplasmic Calcium Buffering
  realtype kmtrpn = p[138] * (1.45 - 0.45 * (1 - fracTnIp) / (1 - fracTnIpo));  // PHOSPHOREGULATION
  realtype btrpn = p[135] * kmtrpn / ((kmtrpn + y[47]) * (kmtrpn + y[47]));
  realtype bcmdn = p[136] * p[139] / ((p[139] + y[47]) * (p[139] + y[47]));
  realtype bindo = p[137] * p[140] / ((p[140] + y[47]) * (p[140] + y[47]));
  realtype Bmyo = 1 / (1 + bcmdn + btrpn + bindo);

  //// Ion Concentrations and Membrane Potential
  ydot[45] = -1e3 * I_Na_tot * p[91] / (p[88] * zna *
                                        Frdy);                                                        // [mM/sec]
  ydot[46] = -1e3 * I_K_tot * p[91] / (p[88] * zk *
                                       Frdy);                                                          // [mM/sec]
  ydot[47] = -Bmyo *
             (1e3 * I_Ca_tot * p[91] / (p[88] * zca * Frdy) + ((I_up - I_leak) * p[89] / p[88]) -
              (I_rel * p[90] / p[88])); // [mM/sec]

  // Internal Stimulation:
  /*realtype rate = p[141];

     realtype a=(t+0.9);
     a=a-(1/rate);
     for (int i=1; a>=0; i++)
          a=a-(1/rate);
     a=a+(1/rate);

     realtype I_app = 0.0;

     //if (((t+0.9)%(1/rate)) <= 0.005)
     if (a<=0.005)
     {I_app = 10.0;}*/

  // Transmembrane Voltage
  ydot[48] = -1e3 * (I_Ca_tot + I_K_tot + I_Na_tot + I_ClCa - p[144]);

  // for internal stimulation
  // ydot[48] = -1e3*(I_Ca_tot+I_K_tot+I_Na_tot+I_ClCa-I_app);

  // CICR timing: y(0) tracks the last time when Vdot > 30 mV/msec or Ca overload
  if (ydot[48] > 30e3)
    ydot[0] = 1 - 2e4 * y[0];
  else
    ydot[0] = 1;

  if (p[146] == 1) {
    y[48] = Vm_non_clamp;
    ydot[48] = 0;
  }

  // Bring dy/dt to the right side of the equation to have "0" on the left side (DAE: dy/dt=0); ydot represents the
  // equation (see above)
  rval[0] = ydot[0] - ypval[0];
  rval[1] = ydot[1];
  rval[2] = ydot[2];
  rval[3] = ydot[3];
  rval[4] = ydot[4] - ypval[4];
  rval[5] = ydot[5] - ypval[5];
  rval[6] = ydot[6] - ypval[6];
  rval[7] = ydot[7] - ypval[7];
  rval[8] = ydot[8] - ypval[8];
  rval[9] = ydot[9] - ypval[9];
  rval[10] = ydot[10];
  rval[11] = ydot[11];
  rval[12] = ydot[12];
  rval[13] = ydot[13];
  rval[14] = ydot[14];
  rval[15] = ydot[15] - ypval[15];
  rval[16] = ydot[16];
  rval[17] = ydot[17];
  rval[18] = ydot[18];
  rval[19] = ydot[19] - ypval[19];
  rval[20] = ydot[20] - ypval[20];
  rval[21] = ydot[21];
  rval[22] = ydot[22];
  rval[23] = ydot[23] - ypval[23];
  rval[24] = ydot[24] - ypval[24];
  rval[25] = ydot[25] - ypval[25];
  rval[26] = ydot[26] - ypval[26];
  rval[27] = ydot[27];
  rval[28] = ydot[28];
  rval[29] = ydot[29] - ypval[29];
  rval[30] = ydot[30] - ypval[30];
  rval[31] = ydot[31] - ypval[31];
  rval[32] = ydot[32] - ypval[32];
  rval[33] = ydot[33] - ypval[33];
  rval[34] = ydot[34] - ypval[34];
  rval[35] = ydot[35] - ypval[35];
  rval[36] = ydot[36] - ypval[36];
  rval[37] = ydot[37] - ypval[37];
  rval[38] = ydot[38] - ypval[38];
  rval[39] = ydot[39] - ypval[39];
  rval[40] = ydot[40] - ypval[40];
  rval[41] = ydot[41] - ypval[41];
  rval[42] = ydot[42] - ypval[42];
  rval[43] = ydot[43] - ypval[43];
  rval[44] = ydot[44] - ypval[44];
  rval[45] = ydot[45] - ypval[45];
  rval[46] = ydot[46] - ypval[46];
  rval[47] = ydot[47] - ypval[47];
  rval[48] = ydot[48] - ypval[48];

  return 0;
} // resrob

// function to check if the memory could be allocated successfully
static int check_flag(void *flagvalue, char *funcname, int opt) {
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if ((opt == 0) && (flagvalue == NULL)) {
    fprintf(stderr,
            "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
            funcname);
    return 1;
  } else if (opt == 1) {
    /* Check if flag < 0 */
    errflag = (int *) flagvalue;
    if (*errflag < 0) {
      fprintf(stderr,
              "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
              funcname, *errflag);
      return 1;
    }
  } else if ((opt == 2) && (flagvalue == NULL)) {
    /* Check if function returned NULL pointer - no memory allocated */
    fprintf(stderr,
            "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
            funcname);
    return 1;
  }

  return 0;
} // check_flag

// function to get information about the solver behavior and success
static void PrintFinalStats(void *mem) {
  int retval;
  long int nst, nni, nje, nre, nreLS, netf, ncfn, nge;

  retval = IDAGetNumSteps(mem, &nst);

  // check_flag(&retval, "IDAGetNumSteps", 1);
  retval = IDAGetNumResEvals(mem, &nre);

  // check_flag(&retval, "IDAGetNumResEvals", 1);
  retval = IDADlsGetNumJacEvals(mem, &nje);

  // check_flag(&retval, "IDADenseGetNumJacEvals", 1);
  retval = IDAGetNumNonlinSolvIters(mem, &nni);

  // check_flag(&retval, "IDAGetNumNonlinSolvIters", 1);
  retval = IDAGetNumErrTestFails(mem, &netf);

  // check_flag(&retval, "IDAGetNumErrTestFails", 1);
  retval = IDAGetNumNonlinSolvConvFails(mem, &ncfn);

  // check_flag(&retval, "IDAGetNumNonlinSolvConvFails", 1);
  retval = IDADlsGetNumResEvals(mem, &nreLS);

  // check_flag(&retval, "IDADenseGetNumResEvals", 1);
  retval = IDAGetNumGEvals(mem, &nge);

  // check_flag(&retval, "IDAGetNumGEvals", 1);

  printf("\nFinal Run Statistics: \n\n");
  printf("Number of steps                    = %ld\n", nst);
  printf("Number of residual evaluations     = %ld\n", nre + nreLS);
  printf("Number of Jacobian evaluations     = %ld\n", nje);
  printf("Number of nonlinear iterations     = %ld\n", nni);
  printf("Number of error test failures      = %ld\n", netf);
  printf("Number of nonlinear conv. failures = %ld\n", ncfn);
  printf("Number of root fn. evaluations     = %ld\n", nge);
} // PrintFinalStats

void Saucerman::Print(ostream &tempstr, double tArg, ML_CalcType V) {
  tempstr << tArg << ' ' << V << ' ' << m << ' ' << h << ' ' << j << ' ' << d << ' ' << f << ' '
          << xr1 << ' ' << xs << ' '
          << r << ' ' << s << ' ' << Ca_i << ' ' << Na_i << ' ' << K_i << ' ' << Ca_jsr << ' '
          << Ca_nsr;
}

void Saucerman::LongPrint(ostream &tempstr, double tArg, ML_CalcType V) {
  Print(tempstr, tArg, V);
  const ML_CalcType svolt = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + svolt) + .5);

  // Recalculate factors that represent phosphorylation

  // PLB module
  const ML_CalcType PLB = (CELLMODEL_PARAMVALUE(VT_PLBtot)) - PLBs;
  const ML_CalcType fracPLB = PLB / (CELLMODEL_PARAMVALUE(VT_PLBtot));
  const ML_CalcType fracPLBo = 0.9926;

  // LCC module
  const ML_CalcType fracLCCap = LCCap / (CELLMODEL_PARAMVALUE(VT_LCCtot));
  const ML_CalcType fracLCCapo = 0.028;

  const ML_CalcType fracLCCbp = LCCbp / (CELLMODEL_PARAMVALUE(VT_LCCtot));
  const ML_CalcType fracLCCbpo = 0.0328;

  // RyR module
  const ML_CalcType fracRyRp = RyRp / (CELLMODEL_PARAMVALUE(VT_RyRtot));
  const ML_CalcType fracRyRpo = 0.0244;

  // TnI module (not required for current calculation)
  // const  ML_CalcType fracTnIp=TnIp/(CELLMODEL_PARAMVALUE(VT_TnItot));
  // const  ML_CalcType fracTnIpo=0.0031;

  // Iks module
  const ML_CalcType fracIksp = Iksp / (CELLMODEL_PARAMVALUE(VT_Iks_tot));
  const ML_CalcType fracIkspo = 0.0018 / (CELLMODEL_PARAMVALUE(VT_Iks_tot));


  // Needed to compute currents
  // Nernst Equations
  const ML_CalcType EK = (CELLMODEL_PARAMVALUE(VT_RTONF)) * (log(((CELLMODEL_PARAMVALUE(VT_K_o)) / K_i)));
  const ML_CalcType ENa = (CELLMODEL_PARAMVALUE(VT_RTONF)) * (log(((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_i)));
  const ML_CalcType EKs = (CELLMODEL_PARAMVALUE(VT_RTONF)) * (log((CELLMODEL_PARAMVALUE(VT_KopKNaNao)) / (K_i + (CELLMODEL_PARAMVALUE(VT_pKNa)) * Na_i)));
  const ML_CalcType ECa = 0.5 * (CELLMODEL_PARAMVALUE(VT_RTONF)) * (log(((CELLMODEL_PARAMVALUE(VT_Ca_o)) / Ca_i)));
  const ML_CalcType ECl = -40.0;


  // Currents
  // I_to: Transient outward K current
  const ML_CalcType I_to = (CELLMODEL_PARAMVALUE(VT_g_to)) * r * s * (svolt - EK);  // r=xto and s=yto

  // I_kr: Rapidly acitivating K current
  const ML_CalcType rkr = 1. / (1 + exp((svolt + 33) / 22.4));
  const ML_CalcType I_Kr = (CELLMODEL_PARAMVALUE(VT_g_Kr)) * sqrt((CELLMODEL_PARAMVALUE(VT_K_o)) / 5.4) * rkr * xr1 * (svolt - EK);

  // I_ks: Slowly activating K current
  const ML_CalcType pcaks = -log10(Ca_i) + 3.0;  // PKA on Ks dependent on Ca_i
  const ML_CalcType gKs0 = (CELLMODEL_PARAMVALUE(VT_g_Ks)) * (0.04 + 0.133 / (1 + exp((-7.2 + pcaks) / 0.6)));
  const ML_CalcType gKs = gKs0 * (0.2 * fracIksp / fracIkspo + 0.8); // Phosphoregulation
  xs05 = 1.5 - (1.5 * fracIksp / fracIkspo - 1.5);
  const ML_CalcType I_Ks = gKs * xs * xs * (svolt - EKs);  // xs dependent on stimulation frequentcy


  // I_K1: Time independent K current
  const ML_CalcType AK1 = 1.02 / (1. + exp(0.2385 * (svolt - EK - 59.215)));
  const ML_CalcType BK1 =
      (.49124 * exp(0.08032 * (svolt - EK + 5.476)) + exp(0.06175 * (svolt - EK - 594.31))) /
      (1. + exp(-0.5143 * (svolt - EK + 4.753)));
  const ML_CalcType rec_iK1 = AK1 / (AK1 + BK1);
  const ML_CalcType I_K1 = (CELLMODEL_PARAMVALUE(VT_g_K1)) * sqrt((CELLMODEL_PARAMVALUE(VT_K_o)) / 5.4) * rec_iK1 * (svolt - EK);

  // I_pCa: Sarcolemmal Ca pump current
  const ML_CalcType I_pCa = (CELLMODEL_PARAMVALUE(VT_g_pCa)) * Ca_i / ((CELLMODEL_PARAMVALUE(VT_KpCa)) + Ca_i);

  // I_pK: Plateau K current
  const ML_CalcType I_pK = (CELLMODEL_PARAMVALUE(VT_g_pK)) * ptTeaP->rec_ipK[Vi] * (svolt - EK);

  // I_bNa: Na background current
  const ML_CalcType I_bNa = (CELLMODEL_PARAMVALUE(VT_g_bNa)) * (svolt - ENa);

  // I_bCa: Ca background current
  const ML_CalcType I_bCa = (CELLMODEL_PARAMVALUE(VT_g_bCa)) * (svolt - ECa);

  // Compute scalar currents
  // Fast Na currrent
  const ML_CalcType I_Na = (CELLMODEL_PARAMVALUE(VT_g_Na)) * m * m * m * h * j * (svolt - ENa);

  // L-type calcium current
  const ML_CalcType fpo = 1. * (0.03 * fracLCCap / fracLCCapo + 0.97);
  const ML_CalcType favail = 1. * (0.05 * fracLCCbp / fracLCCbpo + 0.95);
  const ML_CalcType fCa = 1 / (1 + (Ca_i / (CELLMODEL_PARAMVALUE(VT_KmCa))) * (Ca_i / (CELLMODEL_PARAMVALUE(VT_KmCa))));

  // without nifedipine effect
  const ML_CalcType I_CaL =
      d * f * fCa * fpo * favail * (ptTeaP->CaL_P1[Vi] * Ca_i + ptTeaP->CaL_P2[Vi]); // includes
  // Phosphoregulation
  const ML_CalcType I_CaKL =
      d * f * fCa * fpo * favail * (ptTeaP->CaKL_P1[Vi] * K_i + ptTeaP->CaKL_P2[Vi]);
  const ML_CalcType I_CaNaL =
      d * f * fCa * fpo * favail * (ptTeaP->CaNaL_P1[Vi] * Na_i + ptTeaP->CaNaL_P2[Vi]);
  const ML_CalcType I_CaL_tot = I_CaL + I_CaKL + I_CaNaL;

  // I_NaCa: Na/Ca exchanger current
  const ML_CalcType I_NaCa = (ptTeaP->NaCa_P1[Vi]) * (Na_i * Na_i * Na_i * (ptTeaP->NaCa_P2[Vi]) -
                                                      Ca_i * (ptTeaP->NaCa_P3[Vi]));

  // I_NaK: Na/K pump current
  const ML_CalcType I_NaK =
      (ptTeaP->NaK_P1[Vi]) / (1 + ((CELLMODEL_PARAMVALUE(VT_KmNai)) / Na_i) * sqrt(((CELLMODEL_PARAMVALUE(VT_KmNai)) / Na_i)));

  // I_ns: Nonspecific calcium-activated currents
  const ML_CalcType ibarnsk = (CELLMODEL_PARAMVALUE(VT_Pns)) * (ptTeaP->nsK_P2[Vi]) *
                              (0.75 * K_i * exp((ptTeaP->nsK_P1[Vi])) - 0.75 * (CELLMODEL_PARAMVALUE(VT_K_o))) /
                              (exp((ptTeaP->nsK_P1[Vi])) - 1);
  const ML_CalcType I_nsK =
      ibarnsk / (1 + ((CELLMODEL_PARAMVALUE(VT_Kmns)) / Ca_i) * ((CELLMODEL_PARAMVALUE(VT_Kmns)) / Ca_i) * ((CELLMODEL_PARAMVALUE(VT_Kmns)) / Ca_i));
  const ML_CalcType ibarnsna = (CELLMODEL_PARAMVALUE(VT_Pns)) * (ptTeaP->nsNa_P2[Vi]) *
                               (0.75 * Na_i * exp((ptTeaP->nsNa_P1[Vi])) - 0.75 * (CELLMODEL_PARAMVALUE(VT_Na_o))) /
                               (exp((ptTeaP->nsNa_P1[Vi])) - 1);
  const ML_CalcType I_nsNa =
      ibarnsna / (1 + ((CELLMODEL_PARAMVALUE(VT_Kmns)) / Ca_i) * ((CELLMODEL_PARAMVALUE(VT_Kmns)) / Ca_i) * ((CELLMODEL_PARAMVALUE(VT_Kmns)) / Ca_i));
  const ML_CalcType I_ns = I_nsK + I_nsNa;

  // I_ClCa: Calcium-activated chloride current
  const ML_CalcType I_ClCa = 0.010 / (1 + 0.1e-3 / Ca_i) * (svolt - ECl);

  // Total Membrane Currents:
  const ML_CalcType I_Na_tot = I_Na + I_bNa + 3 * I_NaCa + 3 * I_NaK + I_CaNaL + I_nsNa;
  const ML_CalcType I_K_tot = I_to + I_Kr + I_Ks + I_K1 + I_pK - 2 * I_NaK + I_CaKL + I_nsK;
  const ML_CalcType I_Ca_tot = I_CaL + I_bCa + I_pCa - 2 * I_NaCa;

  // Calcium Induced Calcium Release (CICR): Faber/ Rudy style
  const ML_CalcType Kmrel = 5 * (2.0 / 3.0 * (1 - fracRyRp) / (1 - fracRyRpo) + 1.0 / 3.0);
  const ML_CalcType t_rel = trel - 0.004;
  const ML_CalcType ryron = 1 / (1 + exp(-t_rel / 0.0005));
  const ML_CalcType ryroff = 1 - 1 / (1 + exp(-t_rel / 0.0005));
  const ML_CalcType grel = (CELLMODEL_PARAMVALUE(VT_gmaxrel)) / (1 + exp((I_Ca_tot + Kmrel) / 0.9)) * ryron * ryroff;
  const ML_CalcType I_relcicr = grel * (Ca_jsr - Ca_i);

  const ML_CalcType bcsqn = (CELLMODEL_PARAMVALUE(VT_CSQNbar)) * Ca_jsr / ((CELLMODEL_PARAMVALUE(VT_Km_csqn)) + Ca_jsr);

  if ((bcsqn > (CELLMODEL_PARAMVALUE(VT_CSQNth))) && (((t - jsrol_time) > 0.5) || std::isnan(jsrol_time)))
    jsrol_time = t;
  const ML_CalcType t_ol = t - jsrol_time;
  ML_CalcType I_relol = 0;

  if ((t_ol >= 0) && (t_ol <= 0.1)) {
    const ML_CalcType grelol = (CELLMODEL_PARAMVALUE(VT_gmaxrelol)) * (1 - exp(-t_ol / 0.5e-3)) * exp(-t_ol / 0.5e-3);
    I_relol = grelol * (Ca_jsr - Ca_i);
  }


  const ML_CalcType I_rel = I_relcicr + I_relol;

  const ML_CalcType Km_up = (CELLMODEL_PARAMVALUE(VT_KmUp)) * (0.75 * fracPLB / fracPLBo + 0.25);  // PHOSPHOREGULATION

  // Shannon I_up
  const ML_CalcType I_up =
      (CELLMODEL_PARAMVALUE(VT_I_upbar)) * ((Ca_i / Km_up) * (Ca_i / Km_up) - (Ca_nsr * Ca_nsr / 9.0)) /
      (1 + ((Ca_i / Km_up) * (Ca_i / Km_up)) + (Ca_nsr * Ca_nsr / 9.0));

  // const  ML_CalcType
  // I_up=(CELLMODEL_PARAMVALUE(VT_I_upbar))*((Ca_i/Km_up)*(Ca_i/(CELLMODEL_PARAMVALUE(VT_KmUp)))-(Ca_nsr*Ca_nsr/9.0))/(1+((Ca_i/Km_up)*(Ca_i/(CELLMODEL_PARAMVALUE(VT_KmUp))))+(Ca_nsr*Ca_nsr/9.0));

  const ML_CalcType I_leak = (CELLMODEL_PARAMVALUE(VT_I_upbar)) * Ca_nsr / (CELLMODEL_PARAMVALUE(VT_nsrbar));
  const ML_CalcType I_tr = (Ca_nsr - Ca_jsr) / (CELLMODEL_PARAMVALUE(VT_tau_tr));

  const ML_CalcType I_mem = (I_Ca_tot + I_K_tot + I_Na_tot + I_ClCa);
  tempstr << ' ' << I_to << ' ' << I_Kr << ' ' << I_Ks << ' ' << I_K1 << ' ' << I_pK << ' ' << I_pCa
          << ' ' << I_bCa << ' ' << I_bNa << ' ' << I_Na << ' ' <<
          I_CaL_tot << ' ' << I_NaCa
          << ' ' << I_NaK << ' ' << I_nsK << ' ' << I_nsNa << ' ' << I_ns << ' ' << I_rel << ' '
          << I_up << ' ' << I_leak << ' ' << I_tr << ' ' <<
          I_K_tot << ' ' << I_Na_tot << ' ' << I_Ca_tot << ' ' << I_mem << ' ' << I_ClCa << ' ';
} // Saucerman::LongPrint

void Saucerman::GetParameterNames(vector<string> &getpara) {
  const int numpara = 14;
  const string ParaNames[numpara] =
      {"m", "h", "j", "d", "f", "xr1", "xs", "r", "s", "Ca_i", "Na_i", "K_i", "Ca_jsr", "Ca_nsr"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void Saucerman::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 24;
  const string ParaNames[numpara] =
      {"I_to", "I_Kr", "I_Ks", "I_K1", "I_pK", "I_pCa", "I_bCa",
       "I_bNa", "I_Na", "I_CaL_tot", "I_NaCa",
       "I_NaK", "I_nsK", "I_nsNa", "I_ns", "I_rel",
       "I_up", "I_leak", "I_tr", "I_K_tot", "I_Na_tot", "I_Ca_tot", "I_mem",
       "I_ClCa"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
