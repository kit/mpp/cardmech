/*! \file Zhang.cpp
   \brief Implementation of Zhang et al sinus node model
   AJP 2000, JCE 2002 (implementation partial), JCE 2003

   \author unknown, Universitaet Karlsruhe (TH)
 */


#include <Zhang.h>

Zhang::Zhang(ZhangParameters *pp) {
  pZP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pZP, NS_ZhangParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

Zhang::~Zhang() {}

inline bool Zhang::AddHeteroValue(string desc, double val) {
#ifdef HETERO
  Parameter EP(desc, val);
  return PS->addDynamicParameter(EP);

#else // ifdef HETERO
  throw kaBaseException("function needs to be compiled with HETERO");
#endif // ifdef HETERO
}

inline int Zhang::GetSize() {
  return sizeof(Zhang) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(ZhangParameters *)
#ifdef HETERO
    -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
      ;
}

void Zhang::Init() {
#if KADEBUG
  cerr << "#initializing Class: Zhang ..."<<endl;
#endif // if KADEBUG
  h1 = CELLMODEL_PARAMVALUE(VT_init_h1);
  h2 = CELLMODEL_PARAMVALUE(VT_init_h2);
  m = CELLMODEL_PARAMVALUE(VT_init_m);
  dL = CELLMODEL_PARAMVALUE(VT_init_dL);
  fL = CELLMODEL_PARAMVALUE(VT_init_fL);
  dT = CELLMODEL_PARAMVALUE(VT_init_dT);
  fT = CELLMODEL_PARAMVALUE(VT_init_fT);
  q = CELLMODEL_PARAMVALUE(VT_init_q);
  r = CELLMODEL_PARAMVALUE(VT_init_r);
  paf = CELLMODEL_PARAMVALUE(VT_init_paf);
  pas = CELLMODEL_PARAMVALUE(VT_init_pas);
  pi_m = CELLMODEL_PARAMVALUE(VT_init_pi_m);
  xs = CELLMODEL_PARAMVALUE(VT_init_xs);
  y = CELLMODEL_PARAMVALUE(VT_init_y);
  Cai = CELLMODEL_PARAMVALUE(VT_init_Cai);

#ifdef ZHANG03
  j = CELLMODEL_PARAMVALUE(VT_j);
  k = CELLMODEL_PARAMVALUE(VT_k);
#endif // ifdef ZHANG03
}

void Zhang::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << h1 << ' ' << h2 << ' ' << m << ' '
          << dL << ' ' << fL << ' ' << dT << ' ' << fT << ' '
          << q << ' ' << r << ' ' << paf << ' ' << pas << ' ' << pi_m << ' '
          << xs << ' ' // 15
          << y << ' '
          << Cai << ' '
#ifdef ZHANG03
    <<j<<' '
    <<k<<' '
#endif // ifdef ZHANG03
      ;
}

void Zhang::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double h = (1 - pZP->FNa[Vi]) * h1 + pZP->FNa[Vi] * h2;
  const double iNa = CELLMODEL_PARAMVALUE(VT_gNa) * m * m * m * h * (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_F)) / (CELLMODEL_PARAMVALUE(VT_RTdF)) *
                     (exp((V_int - (CELLMODEL_PARAMVALUE(VT_ENa))) / (CELLMODEL_PARAMVALUE(VT_RTdF))) - 1.0) *
                     (V_int ? V_int / (exp(V_int / (CELLMODEL_PARAMVALUE(VT_RTdF))) - 1.0) : 1. / (CELLMODEL_PARAMVALUE(VT_RTdF)));
  double iCaL = (CELLMODEL_PARAMVALUE(VT_gCaL)) * (fL * dL + pZP->CCaL[Vi]) * (V_int - (CELLMODEL_PARAMVALUE(VT_ECaL)));

#ifdef ZHANG03
  const double b = CELLMODEL_PARAMVALUE(VT_bmax)*CELLMODEL_PARAMVALUE(VT_ACh)/(CELLMODEL_PARAMVALUE(VT_ACh)+CELLMODEL_PARAMVALUE(VT_kCaACh));
  iCaL *= (1.-b);
#endif // ifdef ZHANG03

  const double iCaT = CELLMODEL_PARAMVALUE(VT_gCaT) * (fT * dT * (V_int - (CELLMODEL_PARAMVALUE(VT_ECaT))));
  const double ito = CELLMODEL_PARAMVALUE(VT_gto) * q * r * (V_int - (CELLMODEL_PARAMVALUE(VT_EK)));
  const double isus = CELLMODEL_PARAMVALUE(VT_gsus) * r * (V_int - (CELLMODEL_PARAMVALUE(VT_EK)));
  const double pa = 0.6 * paf + 0.4 * pas;
  const double iKr = CELLMODEL_PARAMVALUE(VT_gKr) * pa * pi_m * (V_int - (CELLMODEL_PARAMVALUE(VT_EK)));
  const double iKs = CELLMODEL_PARAMVALUE(VT_gKs) * xs * xs * (V_int - (CELLMODEL_PARAMVALUE(VT_EKs)));
  const double ifNa = CELLMODEL_PARAMVALUE(VT_gfNa) * y * (V_int - (CELLMODEL_PARAMVALUE(VT_ENa)));
  const double ifK = CELLMODEL_PARAMVALUE(VT_gfK) * y * (V_int - (CELLMODEL_PARAMVALUE(VT_EK)));
  const double ibNa = CELLMODEL_PARAMVALUE(VT_gbNa) * (V_int - (CELLMODEL_PARAMVALUE(VT_ENa)));
  const double ECa = CELLMODEL_PARAMVALUE(VT_RTdF) * 0.5 * log(CELLMODEL_PARAMVALUE(VT_Cao) / Cai);
  const double ibCa = CELLMODEL_PARAMVALUE(VT_gbCa) * (V_int - ECa);
  const double ibK = CELLMODEL_PARAMVALUE(VT_gbK) * (V_int - (CELLMODEL_PARAMVALUE(VT_EK)));
  const double iNaCa = CELLMODEL_PARAMVALUE(VT_kNaCa) *
                       (CELLMODEL_PARAMVALUE(VT_Nai) * (CELLMODEL_PARAMVALUE(VT_Nai)) * (CELLMODEL_PARAMVALUE(VT_Nai)) * (CELLMODEL_PARAMVALUE(VT_Cao)) *
                        exp(0.03743 * V_int * (CELLMODEL_PARAMVALUE(VT_yNaCa))) -
                        (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao)) *
                        Cai * exp(0.03743 * V_int * (CELLMODEL_PARAMVALUE(VT_yNaCa) - 1.0))) /
                       (1.0 + (CELLMODEL_PARAMVALUE(VT_dNaCa)) *
                              ((CELLMODEL_PARAMVALUE(VT_Nai)) * (CELLMODEL_PARAMVALUE(VT_Nai)) * (CELLMODEL_PARAMVALUE(VT_Nai)) * (CELLMODEL_PARAMVALUE(VT_Cao)) +
                               (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao)) * Cai));
  const double ip = CELLMODEL_PARAMVALUE(VT_ipmax) * (CELLMODEL_PARAMVALUE(VT_Nai) / (CELLMODEL_PARAMVALUE(VT_KmNa) + (CELLMODEL_PARAMVALUE(VT_Nai)))) *
                    (CELLMODEL_PARAMVALUE(VT_Nai) / (CELLMODEL_PARAMVALUE(VT_KmNa) + (CELLMODEL_PARAMVALUE(VT_Nai)))) *
                    (CELLMODEL_PARAMVALUE(VT_Nai) / (CELLMODEL_PARAMVALUE(VT_KmNa) + (CELLMODEL_PARAMVALUE(VT_Nai)))) *
                    (CELLMODEL_PARAMVALUE(VT_Ko) / (CELLMODEL_PARAMVALUE(VT_KmK) + (CELLMODEL_PARAMVALUE(VT_Ko)))) * (CELLMODEL_PARAMVALUE(VT_Ko) / (CELLMODEL_PARAMVALUE(VT_KmK) + (CELLMODEL_PARAMVALUE(VT_Ko)))) *
                    1.6 /
                    (1.5 + exp(-(V_int + 60.0) / 40.0));

  double imem =
      iNa + iCaL + iCaT + ito + isus + iKr + iKs + ifNa + ifK + ibNa + ibCa + ibK + iNaCa + ip;

#ifdef ZHANG03
  const double KAChn = pow(CELLMODEL_PARAMVALUE(VT_ACh), CELLMODEL_PARAMVALUE(VT_nKACh));
  const double iACh  = CELLMODEL_PARAMVALUE(VT_gKACh)*j*k*KAChn/
    (KAChn+
     pow(CELLMODEL_PARAMVALUE(VT_kKACh),
         CELLMODEL_PARAMVALUE(VT_nKACh)))*CELLMODEL_PARAMVALUE(VT_Ko)/(CELLMODEL_PARAMVALUE(VT_Ko)+10.)*(V_int-CELLMODEL_PARAMVALUE(VT_EK))/
    (1.+exp((V_int-CELLMODEL_PARAMVALUE(VT_EK)-140.)*CELLMODEL_PARAMVALUE(VT_F)/(2.5*CELLMODEL_PARAMVALUE(VT_R)*CELLMODEL_PARAMVALUE(VT_Tx))));
  imem += iACh;
#endif // ifdef ZHANG03


  Print(tempstr, t, V);
  tempstr << iNa << ' '  // 26
          << iCaL << ' '
          << iCaT << ' '
          << ito << ' '
          << isus << ' ' // 30
          << iKr << ' '
          << iKs << ' '
          << ifNa << ' '
          << ifK << ' '
          << ibNa << ' ' // 35
          << ibCa << ' '
          << ibK << ' '
          << iNaCa << ' '
          << ip << ' '
          #ifdef ZHANG03
          <<iACh<<' ';
          #endif // ifdef ZHANG03
          << imem << ' ';
} // Zhang::LongPrint

void Zhang::GetParameterNames(vector<string> &getpara) {
  const string ParaNames[] =
      {"h1", "h2", "m", "dL", "fL", "dT", "fT", "q", "r", "paf", "pas", "pi", "xs", "y", "Cai"
#ifdef ZHANG03
          , "j",     "k"
#endif // ifdef ZHANG03
      };

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void Zhang::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const string ParaNames[] =
      {"I_Na", "I_CaL", "I_CaT", "I_to", "I_sus", "I_Kr", "I_Ks", "I_fNa", "I_fK", "I_bNa", "I_bCa",
       "I_bK", "I_NaCa",
       "I_p"
#ifdef ZHANG03
          ,       "I_ACh"
#endif // ifdef ZHANG03
          , "I_mem"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType
Zhang::Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch, int euler) {
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);

  const ML_CalcType m_inf = pZP->m_inf[Vi];

  m = (m_inf - (m_inf - m) * pZP->exptau_m[Vi]);
  const ML_CalcType h1_inf = pZP->h1_inf[Vi];
  h1 = (h1_inf - (h1_inf - h1) * pZP->exptau_h1[Vi]);
  const ML_CalcType h2_inf = pZP->h2_inf[Vi];
  h2 = (h2_inf - (h2_inf - h2) * pZP->exptau_h2[Vi]);
  const ML_CalcType dL_inf = pZP->dL_inf[Vi];
  dL = (dL_inf - (dL_inf - dL) * pZP->exptau_dL[Vi]);
  const ML_CalcType fL_inf = pZP->fL_inf[Vi];
  fL = (fL_inf - (fL_inf - fL) * pZP->exptau_fL[Vi]);
  const ML_CalcType dT_inf = pZP->dT_inf[Vi];
  dT = (dT_inf - (dT_inf - dT) * pZP->exptau_dT[Vi]);
  const ML_CalcType fT_inf = pZP->fT_inf[Vi];
  fT = (fT_inf - (fT_inf - fT) * pZP->exptau_fT[Vi]);
  const ML_CalcType q_inf = pZP->q_inf[Vi];
  q = (q_inf - (q_inf - q) * pZP->exptau_q[Vi]);
  const ML_CalcType r_inf = pZP->r_inf[Vi];
  r = (r_inf - (r_inf - r) * pZP->exptau_r[Vi]);
  const ML_CalcType paf_inf = pZP->paf_inf[Vi];
  paf = (paf_inf - (paf_inf - paf) * pZP->exptau_paf[Vi]);
  const ML_CalcType pas_inf = pZP->pas_inf[Vi];
  pas = (pas_inf - (pas_inf - pas) * pZP->exptau_pas[Vi]);
  pi_m += tinc * ((pZP->pi_inf[Vi] - pi_m) / (CELLMODEL_PARAMVALUE(VT_tau_pi)));
  const ML_CalcType xs_inf = pZP->xs_inf[Vi];
  xs = (xs_inf - (xs_inf - xs) * pZP->exptau_xs[Vi]);
  const ML_CalcType y_inf = pZP->y_inf[Vi];
  y = (y_inf - (y_inf - y) * pZP->exptau_y[Vi]);

  const double h = (1.0 - pZP->FNa[Vi]) * h1 + pZP->FNa[Vi] * h2;
  const double iNa = CELLMODEL_PARAMVALUE(VT_gNa) * m * m * m * h * (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_F)) / (CELLMODEL_PARAMVALUE(VT_RTdF)) *
                     (exp((V_int - (CELLMODEL_PARAMVALUE(VT_ENa))) / (CELLMODEL_PARAMVALUE(VT_RTdF))) - 1.0) *
                     (V_int ? V_int / (exp(V_int / (CELLMODEL_PARAMVALUE(VT_RTdF))) - 1.0) : 1. / (CELLMODEL_PARAMVALUE(VT_RTdF)));

  double iCaL = CELLMODEL_PARAMVALUE(VT_gCaL) * (fL * dL + pZP->CCaL[Vi]) * (V_int - CELLMODEL_PARAMVALUE(VT_ECaL));
#ifdef ZHANG03
  const double b = CELLMODEL_PARAMVALUE(VT_bmax)*CELLMODEL_PARAMVALUE(VT_ACh)/(CELLMODEL_PARAMVALUE(VT_ACh)+CELLMODEL_PARAMVALUE(VT_kCaACh));
  iCaL *= (1.-b);
#endif // ifdef ZHANG03

  const double iCaT = CELLMODEL_PARAMVALUE(VT_gCaT) * (fT * dT * (V_int - CELLMODEL_PARAMVALUE(VT_ECaT)));

  const double ito = CELLMODEL_PARAMVALUE(VT_gto) * q * r * (V_int - (CELLMODEL_PARAMVALUE(VT_EK)));
  const double isus = CELLMODEL_PARAMVALUE(VT_gsus) * r * (V_int - (CELLMODEL_PARAMVALUE(VT_EK)));

  const double pa = 0.6 * paf + 0.4 * pas;
  const double iKr = CELLMODEL_PARAMVALUE(VT_gKr) * pa * pi_m * (V_int - (CELLMODEL_PARAMVALUE(VT_EK)));

  const double iKs = CELLMODEL_PARAMVALUE(VT_gKs) * xs * xs * (V_int - (CELLMODEL_PARAMVALUE(VT_EKs)));

  const double ifNa = CELLMODEL_PARAMVALUE(VT_gfNa) * y * (V_int - CELLMODEL_PARAMVALUE(VT_ENa));
  const double ifK = CELLMODEL_PARAMVALUE(VT_gfK) * y * (V_int - CELLMODEL_PARAMVALUE(VT_EK));

  const double ibNa = CELLMODEL_PARAMVALUE(VT_gbNa) * (V_int - (CELLMODEL_PARAMVALUE(VT_ENa)));
  const double ECa = CELLMODEL_PARAMVALUE(VT_RTdF) * 0.5 * log(CELLMODEL_PARAMVALUE(VT_Cao) / Cai);
  const double ibCa = CELLMODEL_PARAMVALUE(VT_gbCa) * (V_int - ECa);
  const double ibK = CELLMODEL_PARAMVALUE(VT_gbK) * (V_int - (CELLMODEL_PARAMVALUE(VT_EK)));

  const double iNaCa = CELLMODEL_PARAMVALUE(VT_kNaCa) *
                       (CELLMODEL_PARAMVALUE(VT_Nai) * CELLMODEL_PARAMVALUE(VT_Nai) * CELLMODEL_PARAMVALUE(VT_Nai) * (CELLMODEL_PARAMVALUE(VT_Cao)) *
                        exp(0.03743 * V_int * (CELLMODEL_PARAMVALUE(VT_yNaCa))) -
                        (CELLMODEL_PARAMVALUE(VT_Nao)) * CELLMODEL_PARAMVALUE(VT_Nao) * CELLMODEL_PARAMVALUE(VT_Nao) * Cai *
                        exp(0.03743 * V_int * (CELLMODEL_PARAMVALUE(VT_yNaCa) - 1.0))) /
                       (1.0 + CELLMODEL_PARAMVALUE(VT_dNaCa) * (CELLMODEL_PARAMVALUE(VT_Nai) * CELLMODEL_PARAMVALUE(VT_Nai) * CELLMODEL_PARAMVALUE(VT_Nai) * CELLMODEL_PARAMVALUE(VT_Cao) +
                                             CELLMODEL_PARAMVALUE(VT_Nao) * CELLMODEL_PARAMVALUE(VT_Nao) * CELLMODEL_PARAMVALUE(VT_Nao) * Cai));

  const double ip = pZP->Cip[Vi];

#ifdef ZHANG03
  j += tinc*(73.1*(1-j)-pZP->beta_j[Vi]*j);
  k += tinc*(3.7*(1-k)-pZP->beta_k[Vi]*k);

  const double KAChn = pow(CELLMODEL_PARAMVALUE(VT_ACh), CELLMODEL_PARAMVALUE(VT_nKACh));
  const double iACh  = CELLMODEL_PARAMVALUE(VT_gKACh)*j*k*KAChn/
    (KAChn+
     pow(CELLMODEL_PARAMVALUE(VT_kKACh),
         CELLMODEL_PARAMVALUE(VT_nKACh)))*CELLMODEL_PARAMVALUE(VT_Ko)/(CELLMODEL_PARAMVALUE(VT_Ko)+10.)*(V_int-CELLMODEL_PARAMVALUE(VT_EK))/
    (1.+exp((V_int-CELLMODEL_PARAMVALUE(VT_EK)-140.)*CELLMODEL_PARAMVALUE(VT_F)/(2.5*CELLMODEL_PARAMVALUE(VT_R)*CELLMODEL_PARAMVALUE(VT_Tx))));
#endif // ifdef ZHANG03


  return tinc * (-1.0 / (CELLMODEL_PARAMVALUE(VT_Cm)) *
                 (iNa + iCaL + iCaT + ito + isus + iKr + iKs + ifNa + ifK + ibNa + ibCa + ibK +
                  iNaCa + ip
                  #ifdef ZHANG03
                  +iACh
                  #endif // ifdef ZHANG03
                  - i_external)) * 0.001;
} // Zhang::Calc
