/*      File: DiFrancescoNobleParameters.cpp
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */


#include <DiFrancescoNobleParameters.h>

DiFrancescoNobleParameters::DiFrancescoNobleParameters(const char *initFile, ML_CalcType tinc) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

DiFrancescoNobleParameters::~DiFrancescoNobleParameters() {
  // Destruktor
}

void DiFrancescoNobleParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "DiFrancescoNobleParameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void DiFrancescoNobleParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "Loading the DiFrancescoNoble parameter from " << initFile << " ...\n";
#endif // if KADEBUG


  // Initialization of the Parameters ...
  P[VT_R].name = "R";
  P[VT_Tx].name = "T";
  P[VT_F].name = "F";
  P[VT_RTdF].name = "RTdF";
  P[VT_FdRT].name = "FdRT";
  P[VT_F2dRT].name = "F2dRT";
  P[VT_RTd2F].name = "RTd2F";
  P[VT_C_m].name = "C_m";
  P[VT_dC_m].name = "dC_m";
  P[VT_k_mK].name = "k_mK";
  P[VT_k_mNa].name = "k_mNa";
  P[VT_k_mf2].name = "k_mf2";
  P[VT_k_NaCa].name = "k_NaCa";
  P[VT_d_NaCa].name = "d_NaCa";
  P[VT_K_mf].name = "K_mf";
  P[VT_K_m1].name = "K_m1";
  P[VT_K_mto].name = "K_mto";
  P[VT_K_act4].name = "K_act4";
  P[VT_K_b].name = "K_b";
  P[VT_k_mCa].name = "k_mCa";
  P[VT_Ca_upmax].name = "Ca_upmax";
  P[VT_I_NaKmax].name = "I_NaKmax";
  P[VT_I_Kmax].name = "I_Kmax";
  P[VT_I_Kmaxd].name = "I_Kmaxd";
  P[VT_n_NaCa].name = "n_NaCa";
  P[VT_n_NaCam2].name = "n_NaCam2";
  P[VT_ndn_NaCa].name = "ndn_NaCa";
  P[VT_tdn].name = "tdn";
  P[VT_P_std].name = "P_std";
  P[VT_g_fK].name = "g_fK";
  P[VT_g_fNa].name = "g_fNa";
  P[VT_g_k1].name = "g_k1";
  P[VT_g_bNa].name = "g_bNa";
  P[VT_g_bCa].name = "g_bCa";
  P[VT_g_bK].name = "g_bK";
  P[VT_g_Na].name = "g_Na";
  P[VT_g_to].name = "g_to";
  P[VT_t_up].name = "t_up";
  P[VT_t_rep].name = "t_rep";
  P[VT_t_rel].name = "t_rel";
  P[VT_b_up].name = "b_up";
  P[VT_Na_o].name = "Na_o";
  P[VT_NNNO].name = "NNNO";
  P[VT_Ca_o].name = "Ca_o";
  P[VT_P_si].name = "P_si";
  P[VT_gamm].name = "gamm";
  P[VT_V_ecs].name = "V_ecs";
  P[VT_radius].name = "radius";
  P[VT_lenght].name = "lenght";
  P[VT_Vol].name = "Vol";
  P[VT_Vcell].name = "Vcell";
  P[VT_V_e].name = "V_e";
  P[VT_dVeF].name = "dVeF";
  P[VT_V_i].name = "V_i";
  P[VT_dViF].name = "dViF";
  P[VT_dF2Vi].name = "dF2Vi";
  P[VT_F2VidCa_up].name = "F2VidCa_up";
  P[VT_V_up].name = "V_up";
  P[VT_dF2Vup].name = "dF2Vup";
  P[VT_V_rel].name = "V_rel";
  P[VT_dF2Vrel].name = "dF2Vrel";
  P[VT_F2Vreldtrep].name = "F2Vreldtrep";
  P[VT_F2Vreldtrel].name = "F2Vreldtrel";
  P[VT_Vr].name = "Vr";
  P[VT_SL0].name = "SL0";
  P[VT_SL].name = "SL";
  P[VT_G_Stretch].name = "G_Stretch";
  P[VT_roh].name = "roh";
  P[VT_alfa].name = "alfa";
  P[VT_K].name = "K";
  P[VT_A].name = "A";
  P[VT_stretchfactor].name = "stretchfactor";
  P[VT_y].name = "Init_y";
  P[VT_x].name = "Init_x";
  P[VT_h].name = "Init_h";
  P[VT_f].name = "Init_f";
  P[VT_f2].name = "Init_f2";
  P[VT_r].name = "Init_r";
  P[VT_p].name = "Init_p";
  P[VT_K_o].name = "Init_K_o";
  P[VT_Ca_i].name = "Init_Ca_i";
  P[VT_Ca_up].name = "Init_Ca_up";
  P[VT_Ca_rel].name = "Init_Ca_rel";
  P[VT_m].name = "Init_m";
  P[VT_d].name = "Init_d";
  P[VT_Na_i].name = "Init_Na_i";
  P[VT_K_i].name = "Init_K_i";
  P[VT_Vm].name = "Init_Vm";
  P[VT_Amp].name = "Amp";

  P[VT_RTdF].readFromFile = false;
  P[VT_FdRT].readFromFile = false;
  P[VT_F2dRT].readFromFile = false;
  P[VT_RTd2F].readFromFile = false;
  P[VT_dC_m].readFromFile = false;
  P[VT_I_Kmaxd].readFromFile = false;
  P[VT_n_NaCam2].readFromFile = false;
  P[VT_ndn_NaCa].readFromFile = false;
  P[VT_tdn].readFromFile = false;
  P[VT_NNNO].readFromFile = false;
  P[VT_Vol].readFromFile = false;
  P[VT_Vcell].readFromFile = false;
  P[VT_V_e].readFromFile = false;
  P[VT_dVeF].readFromFile = false;
  P[VT_V_i].readFromFile = false;
  P[VT_dViF].readFromFile = false;
  P[VT_dF2Vi].readFromFile = false;
  P[VT_F2VidCa_up].readFromFile = false;
  P[VT_V_up].readFromFile = false;
  P[VT_dF2Vup].readFromFile = false;
  P[VT_V_rel].readFromFile = false;
  P[VT_dF2Vrel].readFromFile = false;
  P[VT_F2Vreldtrep].readFromFile = false;
  P[VT_F2Vreldtrel].readFromFile = false;
  P[VT_stretchfactor].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_DiFrancescoNoble);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  // End Initialization of the Parameters ...

  Calculate();
  InitTable(tinc);
} // DiFrancescoNobleParameters::Init

void DiFrancescoNobleParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "DiFrancescoNobleParameters - Calculate ..."<<endl;
#endif // if KADEBUG
  P[VT_RTdF].value = P[VT_R].value * (P[VT_Tx].value) / (P[VT_F].value);
  P[VT_FdRT].value = 1.0 / (P[VT_RTdF].value);
  P[VT_F2dRT].value = 2.0 * (P[VT_FdRT].value);
  P[VT_RTd2F].value = 1.0 / (P[VT_F2dRT].value);
  P[VT_dC_m].value = .001 / (P[VT_C_m].value);
  P[VT_I_Kmaxd].value = P[VT_I_Kmax].value / (140.0);
  P[VT_n_NaCam2].value = P[VT_n_NaCa].value - 2.0;
  P[VT_ndn_NaCa].value = P[VT_n_NaCa].value / (P[VT_n_NaCam2].value);
  P[VT_tdn].value = 2.0 / (P[VT_n_NaCam2].value);
  P[VT_NNNO].value = P[VT_Na_o].value * (P[VT_Na_o].value) * (P[VT_Na_o].value);
  P[VT_Vol].value = M_PI * (P[VT_radius].value) * (P[VT_radius].value) * (P[VT_lenght].value);
  P[VT_Vcell].value = 1e9 * (P[VT_Vol].value);
  P[VT_V_e].value = P[VT_V_ecs].value * (P[VT_Vcell].value);
  P[VT_dVeF].value = 1.0 / (P[VT_V_e].value * (P[VT_F].value));
  P[VT_V_i].value = (1.0 - (P[VT_V_ecs].value)) * (P[VT_Vcell].value);
  P[VT_dViF].value = 1.0 / (P[VT_V_i].value * (P[VT_F].value));
  P[VT_dF2Vi].value = P[VT_dViF].value * .5;
  P[VT_F2VidCa_up].value = 1.0 / (P[VT_dF2Vi].value * (P[VT_t_up].value) * (P[VT_Ca_upmax].value));
  P[VT_V_up].value = .05 * (P[VT_V_i].value);
  P[VT_dF2Vup].value = 1.0 / (2.0 * (P[VT_V_up].value) * (P[VT_F].value));
  P[VT_V_rel].value = .02 * (P[VT_V_i].value);
  P[VT_dF2Vrel].value = 1.0 / (2.0 * (P[VT_V_rel].value) * (P[VT_F].value));
  P[VT_F2Vreldtrep].value = 1.0 / (P[VT_t_rep].value * (P[VT_dF2Vrel].value));
  P[VT_F2Vreldtrel].value = 1.0 / (P[VT_t_rel].value * (P[VT_dF2Vrel].value));
  P[VT_stretchfactor].value = P[VT_G_Stretch].value * (P[VT_roh].value) * (P[VT_A].value) /
                              (1.0 + (P[VT_K].value) * exp(-(P[VT_alfa].value) *
                                                           (P[VT_SL].value - (P[VT_SL0].value))));
  exp50 = exp(50.0 * (P[VT_FdRT].value));
  exp50_2 = exp(100.0 * (P[VT_FdRT].value));
} // DiFrancescoNobleParameters::Calculate

void DiFrancescoNobleParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr << "DiFrancescoNobleBasis - InitTable ...\n";
#endif // if KADEBUG
  double a, b;
  tinc *= -1;  // neg. for exptau calculation
  for (double V = -RangeTabhalf + 0.0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int) (DivisionTab * (RangeTabhalf + V) + .5);
    a = .05 * exp((V + 42.0) * -.067);
    b = a + (V + 42.0) / (1.0 - exp((V + 42.0) * -.2));
    y_inf[Vi] = a / b; // = alpha/(alpha+beta)
    exptau_y[Vi] = exp(tinc * b);  // = exp(-dt / tau), tau = 1/(alpha+beta)

    a = .5 * exp((V + 50.0) * .0826) / (1.0 + exp((V + 50.0) * .057));
    b = a + 1.3 * exp(-(V + 20.0) * .06) / (1.0 + exp(-(V + 20.0) * .04));
    x_inf[Vi] = a / b;
    exptau_x[Vi] = exp(tinc * b);

    a = .033 * exp(-V * .058823529);
    b = a + 33.0 / (1.0 + exp(-(V + 10.0) * .125));
    r_inf[Vi] = a / b;
    exptau_r[Vi] = exp(tinc * b);

    a = 200.0 * (V + 41.0) / (1.0 - exp(-(V + 41.0) * .1));
    b = a + 8000.0 * exp(-(V + 66.0) * .056);
    m_inf[Vi] = a / b;
    exptau_m[Vi] = exp(tinc * b);

    a = 20.0 * exp(-(V + 75.0) * .125);
    b = a + 2000.0 / (1.0 + 320.0 * exp(-(V + 75.0) * .1));
    h_inf[Vi] = a / b;
    exptau_h[Vi] = exp(tinc * b);

    a = 30.0 * (V + 19.0) / (1.0 - exp(-(V + 19.0) * .25));
    b = a + 12.0 * (V + 19.0) / (exp((V + 19.0) * .1) - 1.0);
    d_inf[Vi] = a / b;
    exptau_d[Vi] = exp(tinc * b);

    a = 6.25 * (V + 34.0) / (exp((V + 34.0) * .25) - 1.0);
    b = a + 50.0 / (1.0 + exp(-(V + 34.0) * .25));
    f_inf[Vi] = a / b;
    exptau_f[Vi] = exp(tinc * b);

    a *= 0.1;
    b *= 0.1;

    // p_inf[Vi] = a / b; // == f_inf
    exptau_p[Vi] = exp(tinc * b);

    double V50RF = (V - 50.0) * (P[VT_FdRT].value);
    dexpV50[Vi] = 1.0 / exp(V50RF);
    double dexpV50_2 = 1.0 / exp(V50RF * 2.0);
    expmV[Vi] = exp(-V * (P[VT_FdRT].value));
    VdV[Vi] = V50RF / (1.0 - 1.0 * dexpV50[Vi]);
    VdV2[Vi] = V50RF / (1.0 - 1.0 * dexpV50_2);
    Vp10dexp[Vi] =
        (P[VT_g_to].value) * .28 * (V + 10.0) / (1.0 - exp(-.2 * (V + 10.0))) * exp(.02 * V);
    expm4V[Vi] = exp(-.04 * V);
    double STOCHEX = ((P[VT_n_NaCam2].value)) * V * (P[VT_FdRT].value);
    expstoch[Vi] = (P[VT_k_NaCa].value) * exp((P[VT_gamm].value) * STOCHEX);
    expmstoch[Vi] = exp(-STOCHEX);
    Nadexp[Vi] = P[VT_Na_o].value * dexpV50[Vi];
    Cadexp[Vi] = P[VT_Ca_o].value * dexpV50_2;
  }
} // DiFrancescoNobleParameters::InitTable
