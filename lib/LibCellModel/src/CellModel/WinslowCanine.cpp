/**@file WinslowCanine.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <WinslowCanine.h>

WinslowCanine::WinslowCanine(WinslowCanineParameters *pp) {
#if KADEBUG
  cerr << "#Konstruktor Class: WinslowCanine\n";
#endif // if KADEBUG
  WCp = pp;
#ifdef HETERO
  PS = new ParameterSwitch(WCp, NS_WinslowCanineParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

void WinslowCanine::Init() {
#if KADEBUG
  cerr << "#initializing Class: WinslowCanine ... " << endl;
#endif // if KADEBUG
  mNa = CELLMODEL_PARAMVALUE(VT_init_mNa);
  hNa = CELLMODEL_PARAMVALUE(VT_init_hNa);
  jNa = CELLMODEL_PARAMVALUE(VT_init_jNa);
  Na_i = CELLMODEL_PARAMVALUE(VT_init_Na_i);
  K_i = CELLMODEL_PARAMVALUE(VT_init_K_i);
  Ca_i = CELLMODEL_PARAMVALUE(VT_init_Ca_i);
  Ca_NSR = CELLMODEL_PARAMVALUE(VT_init_Ca_NSR);
  Ca_SS = CELLMODEL_PARAMVALUE(VT_init_Ca_SS);
  Ca_JSR = CELLMODEL_PARAMVALUE(VT_init_Ca_JSR);
  C1_RyR = CELLMODEL_PARAMVALUE(VT_init_C1_RyR);
  O1_RyR = CELLMODEL_PARAMVALUE(VT_init_O1_RyR);
  O2_RyR = CELLMODEL_PARAMVALUE(VT_init_O2_RyR);
  C2_RyR = CELLMODEL_PARAMVALUE(VT_init_C2_RyR);
  xKr = CELLMODEL_PARAMVALUE(VT_init_xKr);
  xKs = CELLMODEL_PARAMVALUE(VT_init_xKs);
  C0 = CELLMODEL_PARAMVALUE(VT_init_C0);
  C1 = CELLMODEL_PARAMVALUE(VT_init_C1);
  C2 = CELLMODEL_PARAMVALUE(VT_init_C2);
  C3 = CELLMODEL_PARAMVALUE(VT_init_C3);
  C4 = CELLMODEL_PARAMVALUE(VT_init_C4);
  Open = CELLMODEL_PARAMVALUE(VT_init_Open);
  CCa0 = CELLMODEL_PARAMVALUE(VT_init_CCa0);
  CCa1 = CELLMODEL_PARAMVALUE(VT_init_CCa1);
  CCa2 = CELLMODEL_PARAMVALUE(VT_init_CCa2);
  CCa3 = CELLMODEL_PARAMVALUE(VT_init_CCa3);
  CCa4 = CELLMODEL_PARAMVALUE(VT_init_CCa4);
  yCa = CELLMODEL_PARAMVALUE(VT_init_yCa);
  LTRPNCa = CELLMODEL_PARAMVALUE(VT_init_LTRPNCa);
  HTRPNCa = CELLMODEL_PARAMVALUE(VT_init_HTRPNCa);
  C0Kv43 = CELLMODEL_PARAMVALUE(VT_init_C0Kv43);
  C1Kv43 = CELLMODEL_PARAMVALUE(VT_init_C1Kv43);
  C2Kv43 = CELLMODEL_PARAMVALUE(VT_init_C2Kv43);
  C3Kv43 = CELLMODEL_PARAMVALUE(VT_init_C3Kv43);
  OKv43 = CELLMODEL_PARAMVALUE(VT_init_OKv43);
  CI0Kv43 = CELLMODEL_PARAMVALUE(VT_init_CI0Kv43);
  CI1Kv43 = CELLMODEL_PARAMVALUE(VT_init_CI1Kv43);
  CI2Kv43 = CELLMODEL_PARAMVALUE(VT_init_CI2Kv43);
  CI3Kv43 = CELLMODEL_PARAMVALUE(VT_init_CI3Kv43);
  OIKv43 = CELLMODEL_PARAMVALUE(VT_init_OIKv43);
  C0Kv14 = CELLMODEL_PARAMVALUE(VT_init_C0Kv14);
  C1Kv14 = CELLMODEL_PARAMVALUE(VT_init_C1Kv14);
  C2Kv14 = CELLMODEL_PARAMVALUE(VT_init_C2Kv14);
  C3Kv14 = CELLMODEL_PARAMVALUE(VT_init_C3Kv14);
  OKv14 = CELLMODEL_PARAMVALUE(VT_init_OKv14);
  CI0Kv14 = CELLMODEL_PARAMVALUE(VT_init_CI0Kv14);
  CI1Kv14 = CELLMODEL_PARAMVALUE(VT_init_CI1Kv14);
  CI2Kv14 = CELLMODEL_PARAMVALUE(VT_init_CI2Kv14);
  CI3Kv14 = CELLMODEL_PARAMVALUE(VT_init_CI3Kv14);
  OIKv14 = CELLMODEL_PARAMVALUE(VT_init_OIKv14);
} // WinslowCanine::Init

ML_CalcType WinslowCanine::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                                ML_CalcType stretch = .1,
                                int euler = 1) {
  tinc *= 1000.0;
  ML_CalcType V_int = V * 1000.0;
  int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);

  double VmE_K = V_int - CELLMODEL_PARAMVALUE(VT_RTdF) * log(CELLMODEL_PARAMVALUE(VT_K_o) / K_i);

  double I_Na = (CELLMODEL_PARAMVALUE(VT_GNa) * mNa * mNa * mNa * hNa * jNa + CELLMODEL_PARAMVALUE(VT_GNab)) *
                (V_int - CELLMODEL_PARAMVALUE(VT_RTdF) * log(CELLMODEL_PARAMVALUE(VT_Na_o) / Na_i));  // I_Na+I_Nab

  double I_K =
      (WCp->RV[Vi] * xKr + CELLMODEL_PARAMVALUE(VT_KI_K1) / (2.0 + exp(VmE_K * CELLMODEL_PARAMVALUE(VT_FdRTm1k5))) + WCp->KpV[Vi]) *
      VmE_K;  // I_Kr+I_K1+I_Kp

  double I_Ks = CELLMODEL_PARAMVALUE(VT_GKs) * xKs * xKs *
                (V_int - CELLMODEL_PARAMVALUE(VT_RTdF) * log((CELLMODEL_PARAMVALUE(VT_K_o) + CELLMODEL_PARAMVALUE(VT_Na_oK)) / (K_i + .01833 * Na_i)));

  double I_Kv14_K = WCp->KI_Kv14_K[Vi] * (K_i * WCp->expVFdRT[Vi] - CELLMODEL_PARAMVALUE(VT_K_o));
  double I_Kv14_Na = WCp->KI_Kv14_Na[Vi] * (Na_i * WCp->expVFdRT[Vi] - CELLMODEL_PARAMVALUE(VT_Na_o));
  double I_Kv14 = (I_Kv14_K + I_Kv14_Na) * OKv14;

  double I_Kv43 = CELLMODEL_PARAMVALUE(VT_GKv43) * OKv43 * VmE_K;
  double I_to = I_Kv43 + I_Kv14;

  double I_Camax = WCp->KI_Camax[Vi];
  double OpenyCa = Open * yCa;
  double I_Ca = I_Camax * OpenyCa;

  double PKprime = 1.0 / (1.0 + ((.0 < I_Camax ? .0 : I_Camax) * CELLMODEL_PARAMVALUE(VT_dICahalf)));
  double I_CaK = WCp->KI_Ca[Vi] * PKprime * OpenyCa * (K_i * WCp->expVFdRT[Vi] - CELLMODEL_PARAMVALUE(VT_K_o));

  double base = CELLMODEL_PARAMVALUE(VT_KmNai) / Na_i;
  double I_NaK = WCp->KI_NaK[Vi] / (1.0 + base * sqrt(base));

  double I_NaCa = WCp->KI_NaCa[Vi] *
                  (Na_i * Na_i * Na_i * CELLMODEL_PARAMVALUE(VT_Ca_o) - WCp->expm1VFdRT[Vi] * CELLMODEL_PARAMVALUE(VT_NNNO) * Ca_i);

  double I_Cab = CELLMODEL_PARAMVALUE(VT_GCab) * (V_int - CELLMODEL_PARAMVALUE(VT_RTd2F) * log(CELLMODEL_PARAMVALUE(VT_Ca_o) / (double) Ca_i));
  double I_pCa = CELLMODEL_PARAMVALUE(VT_IpCamax) * Ca_i / (CELLMODEL_PARAMVALUE(VT_KmpCa) + Ca_i);

  mNa = (V >= -90.0 ? mNa + (WCp->alpha_m[Vi] - WCp->beta_m[Vi] * mNa) * tinc : WCp->alpha_m[Vi] /
                                                                                (WCp->beta_m[Vi]));

  // TODO: Gates (ew)
  hNa = WCp->h_inf[Vi] - (WCp->h_inf[Vi] - hNa) * WCp->exptau_h[Vi];
  jNa = WCp->j_inf[Vi] - (WCp->j_inf[Vi] - jNa) * WCp->exptau_j[Vi];
  xKr = WCp->xKr_inf[Vi] - (WCp->xKr_inf[Vi] - xKr) * WCp->exptau_xKr[Vi];
  xKs = WCp->xKs_inf[Vi] - (WCp->xKs_inf[Vi] - xKs) * WCp->exptau_xKs[Vi];

  double fb = pow(Ca_i * CELLMODEL_PARAMVALUE(VT_dKfb), CELLMODEL_PARAMVALUE(VT_Nfb));
  double rb = Ca_NSR * CELLMODEL_PARAMVALUE(VT_dKrb);  // eigentlich: pow(Ca_NSR*dKrb,Nrb);

  double J_up = (CELLMODEL_PARAMVALUE(VT_vmaxf) * fb - CELLMODEL_PARAMVALUE(VT_vmaxr) * rb) / (1.0 + fb + rb); //! !*KSR;
  double J_rel = CELLMODEL_PARAMVALUE(VT_v1) * (O1_RyR + O2_RyR) * (Ca_JSR - Ca_SS);
  double J_tr = (Ca_NSR - Ca_JSR) * CELLMODEL_PARAMVALUE(VT_dtautr);
  double J_xfer = (Ca_SS - Ca_i) * CELLMODEL_PARAMVALUE(VT_dtauxfer);

  double dLTRPNCa = CELLMODEL_PARAMVALUE(VT_kltrpn_plus) * Ca_i * (1.0 - LTRPNCa) - CELLMODEL_PARAMVALUE(VT_kltrpn_minus) * LTRPNCa;
  double dHTRPNCa = CELLMODEL_PARAMVALUE(VT_khtrpn_plus) * Ca_i * (1.0 - HTRPNCa) - CELLMODEL_PARAMVALUE(VT_khtrpn_minus) * HTRPNCa;
  double J_trpn = CELLMODEL_PARAMVALUE(VT_LTRPNtot) * dLTRPNCa + CELLMODEL_PARAMVALUE(VT_HTRPNtot) * dHTRPNCa;
  LTRPNCa += tinc * dLTRPNCa;
  HTRPNCa += tinc * dHTRPNCa;

  double beta_SS = 1.0 /
                   (1.0 + CELLMODEL_PARAMVALUE(VT_K1bSS) / ((Ca_SS + CELLMODEL_PARAMVALUE(VT_KmCMDN)) * (Ca_SS + CELLMODEL_PARAMVALUE(VT_KmCMDN))) +
                    CELLMODEL_PARAMVALUE(VT_K2bSS) /
                    ((Ca_SS + CELLMODEL_PARAMVALUE(VT_KmEGTA)) * (Ca_SS + CELLMODEL_PARAMVALUE(VT_KmEGTA))));

  double beta_JSR = 1.0 / (1.0 + CELLMODEL_PARAMVALUE(VT_KbJSR) / ((Ca_JSR + CELLMODEL_PARAMVALUE(VT_KmCSQN)) * (Ca_JSR + CELLMODEL_PARAMVALUE(VT_KmCSQN))));

  double beta_i = 1.0 /
                  (1.0 + CELLMODEL_PARAMVALUE(VT_K1bSS) / ((Ca_i + CELLMODEL_PARAMVALUE(VT_KmCMDN)) * (Ca_i + CELLMODEL_PARAMVALUE(VT_KmCMDN))) +
                   CELLMODEL_PARAMVALUE(VT_K2bSS) / ((Ca_i + CELLMODEL_PARAMVALUE(VT_KmEGTA)) * (Ca_i + CELLMODEL_PARAMVALUE(VT_KmEGTA))));

  // Na_i-=(I_Na+3.0*(I_NaCa+I_NaK))*AcapdVmyo*tinc; //Auskommentiert: festes Na_i

  K_i -= (I_K + I_Ks + I_CaK - 2.0 * I_NaK + I_to) * CELLMODEL_PARAMVALUE(VT_AcapdVmyo) * tinc;

  Ca_i +=
      beta_i * (J_xfer - J_up - J_trpn - (I_Cab - 2.0 * I_NaCa + I_pCa) * CELLMODEL_PARAMVALUE(VT_Acapd2Vmyo)) * tinc;

  Ca_SS +=
      beta_SS * ((J_rel * CELLMODEL_PARAMVALUE(VT_VJSRdVSS) - J_xfer * CELLMODEL_PARAMVALUE(VT_VmyodVSS)) - I_Ca * CELLMODEL_PARAMVALUE(VT_AcapDVSS)) * tinc;

  Ca_JSR += beta_JSR * (J_tr - J_rel) * tinc;

  Ca_NSR += (J_up * CELLMODEL_PARAMVALUE(VT_VmyodVNSR) - J_tr * CELLMODEL_PARAMVALUE(VT_VJSRdVNSR)) * tinc;

  base = Ca_SS * 1000.0;
  double temp = base * base * base;
  base *= temp;
  double dO2_RyR = CELLMODEL_PARAMVALUE(VT_kbplus) * temp * O1_RyR - CELLMODEL_PARAMVALUE(VT_kbminus) * O2_RyR;
  double dC1_RyR = -CELLMODEL_PARAMVALUE(VT_kaplus) * base * C1_RyR + CELLMODEL_PARAMVALUE(VT_kaminus) * O1_RyR;
  double dC2_RyR = CELLMODEL_PARAMVALUE(VT_kcplus) * O1_RyR - CELLMODEL_PARAMVALUE(VT_kcminus) * C2_RyR;
  C1_RyR += dC1_RyR * tinc;
  O1_RyR -= (dC1_RyR + dO2_RyR + dC2_RyR) * tinc;
  C2_RyR += dC2_RyR * tinc;
  O2_RyR += dO2_RyR * tinc;

  double C3_to_C4 = WCp->alphaarray[Vi];
  double C2_to_C3 = C3_to_C4 + C3_to_C4;
  double C1_to_C2 = C2_to_C3 + C3_to_C4;
  double C0_to_C1 = C1_to_C2 + C3_to_C4;

  double CCa3_to_CCa4 = CELLMODEL_PARAMVALUE(VT_aL) * C3_to_C4;
  double CCa2_to_CCa3 = CCa3_to_CCa4 + CCa3_to_CCa4;
  double CCa1_to_CCa2 = CCa2_to_CCa3 + CCa3_to_CCa4;
  double CCa0_to_CCa1 = CCa1_to_CCa2 + CCa3_to_CCa4;

  double C1_to_C0 = WCp->bettaarray[Vi];
  double C2_to_C1 = C1_to_C0 + C1_to_C0;
  double C3_to_C2 = C2_to_C1 + C1_to_C0;
  double C4_to_C3 = C3_to_C2 + C1_to_C0;

  double CCa1_to_CCa0 = C1_to_C0 * CELLMODEL_PARAMVALUE(VT_dbL);
  double CCa2_to_CCa1 = CCa1_to_CCa0 + CCa1_to_CCa0;
  double CCa3_to_CCa2 = CCa2_to_CCa1 + CCa1_to_CCa0;
  double CCa4_to_CCa3 = CCa3_to_CCa2 + CCa1_to_CCa0;

  double C0_to_CCa0 = 0.09233 * Ca_SS;
  double C1_to_CCa1 = CELLMODEL_PARAMVALUE(VT_aL) * C0_to_CCa0;
  double C2_to_CCa2 = CELLMODEL_PARAMVALUE(VT_aL) * C1_to_CCa1;
  double C3_to_CCa3 = CELLMODEL_PARAMVALUE(VT_aL) * C2_to_CCa2;
  double C4_to_CCa4 = CELLMODEL_PARAMVALUE(VT_aL) * C3_to_CCa3;

  double CCa1_to_C1 = CELLMODEL_PARAMVALUE(VT_omega) * CELLMODEL_PARAMVALUE(VT_dbL);
  double CCa2_to_C2 = CCa1_to_C1 * CELLMODEL_PARAMVALUE(VT_dbL);
  double CCa3_to_C3 = CCa2_to_C2 * CELLMODEL_PARAMVALUE(VT_dbL);
  double CCa4_to_C4 = CCa3_to_C3 * CELLMODEL_PARAMVALUE(VT_dbL);

  C0 += (C1_to_C0 * C1 + CELLMODEL_PARAMVALUE(VT_omega) * CCa0 - (C0_to_C1 + C0_to_CCa0) * C0) * tinc;
  C1 += (C0_to_C1 * C0 + C2_to_C1 * C2 + CCa1_to_C1 * CCa1 -
         (C1_to_C0 + C1_to_C2 + C1_to_CCa1) * C1) * tinc;
  C2 += (C1_to_C2 * C1 + C3_to_C2 * C3 + CCa2_to_C2 * CCa2 -
         (C2_to_C1 + C2_to_C3 + C2_to_CCa2) * C2) * tinc;
  C3 += (C2_to_C3 * C2 + C4_to_C3 * C4 + CCa3_to_C3 * CCa3 -
         (C3_to_C2 + C3_to_C4 + C3_to_CCa3) * C3) * tinc;
  C4 += (C3_to_C4 * C3 + CELLMODEL_PARAMVALUE(VT_gL) * Open + CCa4_to_C4 * CCa4 -
         (C4_to_C3 + CELLMODEL_PARAMVALUE(VT_fL) + C4_to_CCa4) * C4) * tinc;

  Open += (CELLMODEL_PARAMVALUE(VT_fL) * C4 - CELLMODEL_PARAMVALUE(VT_gL) * Open) * tinc;

  CCa0 += (CCa1_to_CCa0 * CCa1 + C0_to_CCa0 * C0 - (CCa0_to_CCa1 + CELLMODEL_PARAMVALUE(VT_omega)) * CCa0) * tinc;
  CCa1 += (CCa0_to_CCa1 * CCa0 + CCa2_to_CCa1 * CCa2 + C1_to_CCa1 * C1 -
           (CCa1_to_CCa0 + CCa1_to_CCa2 + CCa1_to_C1) * CCa1) * tinc;
  CCa2 += (CCa1_to_CCa2 * CCa1 + CCa3_to_CCa2 * CCa3 + C2_to_CCa2 * C2 -
           (CCa2_to_CCa1 + CCa2_to_CCa3 + CCa2_to_C2) * CCa2) * tinc;
  CCa3 += (CCa2_to_CCa3 * CCa2 + CCa4_to_CCa3 * CCa4 + C3_to_CCa3 * C3 -
           (CCa3_to_CCa2 + CCa3_to_CCa4 + CCa3_to_C3) * CCa3) * tinc;
  CCa4 += (CCa3_to_CCa4 * CCa3 + C4_to_CCa4 * C4 - (CCa4_to_CCa3 + CCa4_to_C4) * CCa4) * tinc;
  yCa = WCp->yCa_inf[Vi] - (WCp->yCa_inf[Vi] - yCa) * WCp->exptau_yCa[Vi];

  double C3Kv43_to_OKv43 = WCp->alpha_act43array[Vi];
  double C2Kv43_to_C3Kv43 = C3Kv43_to_OKv43 + C3Kv43_to_OKv43;
  double C1Kv43_to_C2Kv43 = C2Kv43_to_C3Kv43 + C3Kv43_to_OKv43;
  double C0Kv43_to_C1Kv43 = C1Kv43_to_C2Kv43 + C3Kv43_to_OKv43;

  double CI0Kv43_to_CI1Kv43 = C0Kv43_to_C1Kv43 * CELLMODEL_PARAMVALUE(VT_b1Kv43);
  double CI1Kv43_to_CI2Kv43 = C1Kv43_to_C2Kv43 * CELLMODEL_PARAMVALUE(VT_kb2Kv43);
  double CI2Kv43_to_CI3Kv43 = C2Kv43_to_C3Kv43 * CELLMODEL_PARAMVALUE(VT_kb3Kv43);
  double CI3Kv43_to_OIKv43 = C3Kv43_to_OKv43 * CELLMODEL_PARAMVALUE(VT_kb4Kv43);

  double C1Kv43_to_C0Kv43 = WCp->beta_act43array[Vi];
  double C2Kv43_to_C1Kv43 = C1Kv43_to_C0Kv43 + C1Kv43_to_C0Kv43;
  double C3Kv43_to_C2Kv43 = C2Kv43_to_C1Kv43 + C1Kv43_to_C0Kv43;
  double OKv43_to_C3Kv43 = C3Kv43_to_C2Kv43 + C1Kv43_to_C0Kv43;

  double CI1Kv43_to_CI0Kv43 = C1Kv43_to_C0Kv43 * CELLMODEL_PARAMVALUE(VT_df1Kv43);
  double CI2Kv43_to_CI1Kv43 = C2Kv43_to_C1Kv43 * CELLMODEL_PARAMVALUE(VT_kf1Kv43);
  double CI3Kv43_to_CI2Kv43 = C3Kv43_to_C2Kv43 * CELLMODEL_PARAMVALUE(VT_kf2Kv43);
  double OIKv43_to_CI3Kv43 = OKv43_to_C3Kv43 * CELLMODEL_PARAMVALUE(VT_kf3Kv43);

  double C0Kv43_to_CI0Kv43 = WCp->beta_inact43array[Vi];
  double C1Kv43_to_CI1Kv43 = CELLMODEL_PARAMVALUE(VT_f1Kv43) * C0Kv43_to_CI0Kv43;
  double C2Kv43_to_CI2Kv43 = CELLMODEL_PARAMVALUE(VT_f2Kv43) * C0Kv43_to_CI0Kv43;
  double C3Kv43_to_CI3Kv43 = CELLMODEL_PARAMVALUE(VT_f3Kv43) * C0Kv43_to_CI0Kv43;
  double OKv43_to_OIKv43 = CELLMODEL_PARAMVALUE(VT_f4Kv43) * C0Kv43_to_CI0Kv43;

  double CI0Kv43_to_C0Kv43 = WCp->alpha_inact43array[Vi];
  double CI1Kv43_to_C1Kv43 = CI0Kv43_to_C0Kv43 * CELLMODEL_PARAMVALUE(VT_db1Kv43);
  double CI2Kv43_to_C2Kv43 = CI0Kv43_to_C0Kv43 * CELLMODEL_PARAMVALUE(VT_db2Kv43);
  double CI3Kv43_to_C3Kv43 = CI0Kv43_to_C0Kv43 * CELLMODEL_PARAMVALUE(VT_db3Kv43);
  double OIKv43_to_OKv43 = CI0Kv43_to_C0Kv43 * CELLMODEL_PARAMVALUE(VT_db4Kv43);

  C0Kv43 += (C1Kv43_to_C0Kv43 * C1Kv43 + CI0Kv43_to_C0Kv43 * CI0Kv43 -
             (C0Kv43_to_C1Kv43 + C0Kv43_to_CI0Kv43) * C0Kv43) * tinc;

  C1Kv43 +=
      (C2Kv43_to_C1Kv43 * C2Kv43 + CI1Kv43_to_C1Kv43 * CI1Kv43 + C0Kv43_to_C1Kv43 * C0Kv43 -
       (C1Kv43_to_C2Kv43 + C1Kv43_to_C0Kv43 + C1Kv43_to_CI1Kv43) * C1Kv43) * tinc;

  C2Kv43 +=
      (C3Kv43_to_C2Kv43 * C3Kv43 + CI2Kv43_to_C2Kv43 * CI2Kv43 + C1Kv43_to_C2Kv43 * C1Kv43 -
       (C2Kv43_to_C3Kv43 + C2Kv43_to_C1Kv43 + C2Kv43_to_CI2Kv43) * C2Kv43) * tinc;

  C3Kv43 +=
      (OKv43_to_C3Kv43 * OKv43 + CI3Kv43_to_C3Kv43 * CI3Kv43 + C2Kv43_to_C3Kv43 * C2Kv43 -
       (C3Kv43_to_OKv43 + C3Kv43_to_C2Kv43 + C3Kv43_to_CI3Kv43) * C3Kv43) * tinc;

  OKv43 += (C3Kv43_to_OKv43 * C3Kv43 + OIKv43_to_OKv43 * OIKv43 -
            (OKv43_to_C3Kv43 + OKv43_to_OIKv43) * OKv43) * tinc;

  CI0Kv43 += (C0Kv43_to_CI0Kv43 * C0Kv43 + CI1Kv43_to_CI0Kv43 * CI1Kv43 -
              (CI0Kv43_to_C0Kv43 + CI0Kv43_to_CI1Kv43) * CI0Kv43) * tinc;

  CI1Kv43 +=
      (CI2Kv43_to_CI1Kv43 * CI2Kv43 + C1Kv43_to_CI1Kv43 * C1Kv43 + CI0Kv43_to_CI1Kv43 * CI0Kv43 -
       (CI1Kv43_to_CI2Kv43 + CI1Kv43_to_C1Kv43 + CI1Kv43_to_CI0Kv43) * CI1Kv43) * tinc;

  CI2Kv43 +=
      (CI3Kv43_to_CI2Kv43 * CI3Kv43 + C2Kv43_to_CI2Kv43 * C2Kv43 + CI1Kv43_to_CI2Kv43 * CI1Kv43 -
       (CI2Kv43_to_CI3Kv43 + CI2Kv43_to_C2Kv43 + CI2Kv43_to_CI1Kv43) * CI2Kv43) * tinc;

  CI3Kv43 +=
      (OIKv43_to_CI3Kv43 * OIKv43 + C3Kv43_to_CI3Kv43 * C3Kv43 + CI2Kv43_to_CI3Kv43 * CI2Kv43 -
       (CI3Kv43_to_OIKv43 + CI3Kv43_to_C3Kv43 + CI3Kv43_to_CI2Kv43) * CI3Kv43) * tinc;

  OIKv43 += (OKv43_to_OIKv43 * OKv43 + CI3Kv43_to_OIKv43 * CI3Kv43 -
             (OIKv43_to_OKv43 + OIKv43_to_CI3Kv43) * OIKv43) * tinc;

  double C3Kv14_to_OKv14 = WCp->alpha_act14array[Vi];
  double C2Kv14_to_C3Kv14 = C3Kv14_to_OKv14 + C3Kv14_to_OKv14;
  double C1Kv14_to_C2Kv14 = C2Kv14_to_C3Kv14 + C3Kv14_to_OKv14;
  double C0Kv14_to_C1Kv14 = C1Kv14_to_C2Kv14 + C3Kv14_to_OKv14;

  double CI0Kv14_to_CI1Kv14 = C0Kv14_to_C1Kv14 * CELLMODEL_PARAMVALUE(VT_b1Kv14);
  double CI1Kv14_to_CI2Kv14 = C1Kv14_to_C2Kv14 * CELLMODEL_PARAMVALUE(VT_kb2Kv14);
  double CI2Kv14_to_CI3Kv14 = C2Kv14_to_C3Kv14 * CELLMODEL_PARAMVALUE(VT_kb3Kv14);
  double CI3Kv14_to_OIKv14 = C3Kv14_to_OKv14 * CELLMODEL_PARAMVALUE(VT_kb4Kv14);

  double C1Kv14_to_C0Kv14 = WCp->beta_act14array[Vi];
  double C2Kv14_to_C1Kv14 = C1Kv14_to_C0Kv14 + C1Kv14_to_C0Kv14;
  double C3Kv14_to_C2Kv14 = C2Kv14_to_C1Kv14 + C1Kv14_to_C0Kv14;
  double OKv14_to_C3Kv14 = C3Kv14_to_C2Kv14 + C1Kv14_to_C0Kv14;

  double CI1Kv14_to_CI0Kv14 = C1Kv14_to_C0Kv14 * CELLMODEL_PARAMVALUE(VT_df1Kv14);
  double CI2Kv14_to_CI1Kv14 = C2Kv14_to_C1Kv14 * CELLMODEL_PARAMVALUE(VT_kf1Kv14);
  double CI3Kv14_to_CI2Kv14 = C3Kv14_to_C2Kv14 * CELLMODEL_PARAMVALUE(VT_kf2Kv14);
  double OIKv14_to_CI3Kv14 = OKv14_to_C3Kv14 * CELLMODEL_PARAMVALUE(VT_kf3Kv14);

  C0Kv14 += (C1Kv14_to_C0Kv14 * C1Kv14 + CELLMODEL_PARAMVALUE(VT_CI0Kv14_to_C0Kv14) * CI0Kv14 -
             (C0Kv14_to_C1Kv14 + CELLMODEL_PARAMVALUE(VT_C0Kv14_to_CI0Kv14)) * C0Kv14) *
            tinc;

  C1Kv14 +=
      (C2Kv14_to_C1Kv14 * C2Kv14 + CELLMODEL_PARAMVALUE(VT_CI1Kv14_to_C1Kv14) * CI1Kv14 + C0Kv14_to_C1Kv14 * C0Kv14 -
       (C1Kv14_to_C2Kv14 + C1Kv14_to_C0Kv14 + CELLMODEL_PARAMVALUE(VT_C1Kv14_to_CI1Kv14)) * C1Kv14) * tinc;

  C2Kv14 +=
      (C3Kv14_to_C2Kv14 * C3Kv14 + CELLMODEL_PARAMVALUE(VT_CI2Kv14_to_C2Kv14) * CI2Kv14 + C1Kv14_to_C2Kv14 * C1Kv14 -
       (C2Kv14_to_C3Kv14 + C2Kv14_to_C1Kv14 + CELLMODEL_PARAMVALUE(VT_C2Kv14_to_CI2Kv14)) * C2Kv14) * tinc;

  C3Kv14 +=
      (OKv14_to_C3Kv14 * OKv14 + CELLMODEL_PARAMVALUE(VT_CI3Kv14_to_C3Kv14) * CI3Kv14 + C2Kv14_to_C3Kv14 * C2Kv14 -
       (C3Kv14_to_OKv14 + C3Kv14_to_C2Kv14 + CELLMODEL_PARAMVALUE(VT_C3Kv14_to_CI3Kv14)) * C3Kv14) * tinc;

  OKv14 += (C3Kv14_to_OKv14 * C3Kv14 + CELLMODEL_PARAMVALUE(VT_OIKv14_to_OKv14) * OIKv14 -
            (OKv14_to_C3Kv14 + CELLMODEL_PARAMVALUE(VT_OKv14_to_OIKv14)) * OKv14) * tinc;

  CI0Kv14 +=
      (CELLMODEL_PARAMVALUE(VT_C0Kv14_to_CI0Kv14) * C0Kv14 + CI1Kv14_to_CI0Kv14 * CI1Kv14 -
       (CELLMODEL_PARAMVALUE(VT_CI0Kv14_to_C0Kv14) + CI0Kv14_to_CI1Kv14) * CI0Kv14) *
      tinc;

  CI1Kv14 +=
      (CI2Kv14_to_CI1Kv14 * CI2Kv14 + CELLMODEL_PARAMVALUE(VT_C1Kv14_to_CI1Kv14) * C1Kv14 +
       CI0Kv14_to_CI1Kv14 * CI0Kv14 -
       (CI1Kv14_to_CI2Kv14 + CELLMODEL_PARAMVALUE(VT_CI1Kv14_to_C1Kv14) + CI1Kv14_to_CI0Kv14) * CI1Kv14) * tinc;

  CI2Kv14 +=
      (CI3Kv14_to_CI2Kv14 * CI3Kv14 + CELLMODEL_PARAMVALUE(VT_C2Kv14_to_CI2Kv14) * C2Kv14 +
       CI1Kv14_to_CI2Kv14 * CI1Kv14 -
       (CI2Kv14_to_CI3Kv14 + CELLMODEL_PARAMVALUE(VT_CI2Kv14_to_C2Kv14) + CI2Kv14_to_CI1Kv14) * CI2Kv14) * tinc;

  CI3Kv14 +=
      (OIKv14_to_CI3Kv14 * OIKv14 + CELLMODEL_PARAMVALUE(VT_C3Kv14_to_CI3Kv14) * C3Kv14 +
       CI2Kv14_to_CI3Kv14 * CI2Kv14 -
       (CI3Kv14_to_OIKv14 + CELLMODEL_PARAMVALUE(VT_CI3Kv14_to_C3Kv14) + CI3Kv14_to_CI2Kv14) * CI3Kv14) * tinc;

  OIKv14 += (CELLMODEL_PARAMVALUE(VT_OKv14_to_OIKv14) * OKv14 + CI3Kv14_to_OIKv14 * CI3Kv14 -
             (CELLMODEL_PARAMVALUE(VT_OIKv14_to_OKv14) + OIKv14_to_CI3Kv14) * OIKv14) *
            tinc;

  return -(I_Na + I_Ca + I_CaK + I_K + I_Ks + I_NaCa + I_NaK + I_to + I_pCa + I_Cab - i_external) *
         tinc * .001;
} // WinslowCanine::Calc

void WinslowCanine::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << mNa << ' ' << hNa << ' ' << jNa << ' '
          << Na_i << ' ' << K_i << ' '
          << Ca_i << ' ' << Ca_NSR << ' '
          << Ca_SS << ' ' << Ca_JSR << ' '
          << C1_RyR << ' ' << C2_RyR << ' '
          << O1_RyR << ' ' << O2_RyR << ' '
          << xKr << ' ' << xKs << ' '
          << C0 << ' ' << C1 << ' '
          << C2 << ' ' << C3 << ' '
          << C4 << ' ' << Open << ' '
          << CCa0 << ' ' << CCa1 << ' '
          << CCa2 << ' ' << CCa3 << ' '
          << CCa4 << ' ' << yCa << ' '
          << HTRPNCa << ' ' << LTRPNCa << ' '
          << C0Kv43 << ' ' << C1Kv43 << ' '
          << C2Kv43 << ' ' << C3Kv43 << ' '
          << OKv43 << ' ' << CI0Kv43 << ' '
          << CI1Kv43 << ' ' << CI2Kv43 << ' '
          << CI3Kv43 << ' ' << OIKv43 << ' '
          << C0Kv14 << ' ' << C1Kv14 << ' '
          << C2Kv14 << ' ' << C3Kv14 << ' '
          << OKv14 << ' ' << CI0Kv14 << ' '
          << CI1Kv14 << ' ' << CI2Kv14 << ' '
          << CI3Kv14 << ' ' << OIKv14 << ' ';
} // WinslowCanine::Print

void WinslowCanine::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
}

void WinslowCanine::GetParameterNames(vector<string> &getpara) {
  const int numpara = 1;
  const string ParaNames[numpara] = {"To be done..."};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void WinslowCanine::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 1;
  const string ParaNames[numpara] = {"To be done..."};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
