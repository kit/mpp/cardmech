/*! \file SaucermanTusscherParameters.cpp
   \human cell model of ten Tusscher including Saucermans intracellular model adrenergic signaling

   \version 0.0.0

   \date Created Carola Otto (21.02.2009)\n
   template (00.00.00)\n
   man Gunnar Seemann (27.02.03)\n
   Last Modified Eike Wuelfers (03.02.10)

   \author Carola Otto\n
   Institute of Biomedical Engineering\n
   Universitaet Karlsruhe (TH)\n
   http://www.ibt.uni-karlsruhe.de\n
   Copyright 2000-2009 - All rights reserved.

   // \sa  \ref SaucermanTusscher
 */


#include "SaucermanTusscherParameters.h"

SaucermanTusscherParameters::SaucermanTusscherParameters(const char *initFile) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile);
}

SaucermanTusscherParameters::~SaucermanTusscherParameters() {
  // Destruktor
}

void SaucermanTusscherParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "SaucermanTusscherParameters:" << endl;
  for (int i = vtFirst; i < vtLast; i++)
    cout << P[i].name << "\t = " << P[i].value << endl;
}

void SaucermanTusscherParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "#Loading the SaucermanTusscher parameter from " << initFile << " ...\n";
#endif // if KADEBUG

  // Initialization of the Parameters ...
  P[VT_R].name = "R";
  P[VT_Tx].name = "Tx";
  P[VT_F].name = "F";
  P[VT_m_init].name = "init_m";
  P[VT_h_init].name = "init_h";
  P[VT_j_init].name = "init_j";
  P[VT_xr1_init].name = "init_Xr1";
  P[VT_xr2_init].name = "init_Xr2";
  P[VT_xs_init].name = "init_Xs";
  P[VT_r_init].name = "init_r";
  P[VT_s_init].name = "init_s";
  P[VT_d_init].name = "init_d";
  P[VT_f_init].name = "init_f";
  P[VT_f2_init].name = "init_f2";
  P[VT_fCa_init].name = "init_fCa";
  P[VT_Rq_init].name = "init_Rq";
  P[VT_O_init].name = "init_O";
  P[VT_K_o].name = "K_o";
  P[VT_Ca_o].name = "Ca_o";
  P[VT_Na_o].name = "Na_o";
  P[VT_Vc].name = "Vc";
  P[VT_Vsr].name = "Vsr";
  P[VT_Vss].name = "Vss";
  P[VT_Vrel].name = "Vrel";
  P[VT_ks1].name = "ks1";
  P[VT_ks2].name = "ks2";
  P[VT_k3].name = "k3";
  P[VT_k4].name = "k4";
  P[VT_EC].name = "EC";
  P[VT_max_sr].name = "max_sr";
  P[VT_min_sr].name = "min_sr";
  P[VT_Vxfer].name = "Vxfer";
  P[VT_Bufc].name = "Bufc";
  P[VT_Kbufc].name = "Kbufc";
  P[VT_Bufsr].name = "Bufsr";
  P[VT_Kbufsr].name = "Kbufsr";
  P[VT_Bufss].name = "Bufss";
  P[VT_Kbufss].name = "Kbufss";
  P[VT_taug].name = "taug";
  P[VT_Vmaxup].name = "Vmaxup";
  P[VT_Kup].name = "Kup";
  P[VT_C].name = "C";
  P[VT_g_Kr].name = "g_Kr";
  P[VT_pKNa].name = "pKNa";
  P[VT_g_Ks].name = "g_Ks";
  P[VT_g_K1].name = "g_K1";
  P[VT_g_to].name = "g_to";
  P[VT_g_Na].name = "g_Na";
  P[VT_g_bNa].name = "g_bNa";
  P[VT_KmK].name = "KmK";
  P[VT_KmNa].name = "KmNa";
  P[VT_knak].name = "knak";
  P[VT_g_CaL].name = "g_CaL";
  P[VT_g_bCa].name = "g_bCa";
  P[VT_kNaCa].name = "kNaCa";
  P[VT_KmNai].name = "KmNai";
  P[VT_KmCa].name = "KmCa";
  P[VT_ksat].name = "ksat";
  P[VT_n].name = "n";
  P[VT_g_pCa].name = "g_pCa";
  P[VT_KpCa].name = "KpCa";
  P[VT_g_pK].name = "g_pK";
  P[VT_V_init].name = "init_V_m";
  P[VT_Cai_init].name = "init_Cai";
  P[VT_CaSR_init].name = "init_CaSR";
  P[VT_CaSS_init].name = "init_CaSS";
  P[VT_Nai_init].name = "init_Nai";
  P[VT_Ki_init].name = "init_Ki";
  P[VT_lenght].name = "lenght";
  P[VT_radius].name = "radius";
  P[VT_pi].name = "pi";
  P[VT_CaiMin].name = "CaiMin";
  P[VT_CaiMax].name = "CaiMax";
  P[VT_s_inf_vHalf].name = "s_inf_vHalf";
  P[VT_tau_s_f1].name = "tau_s_f1";
  P[VT_tau_s_slope1].name = "tau_s_slope1";
  P[VT_tau_s_vHalf1].name = "tau_s_vHalf1";
  P[VT_tau_s_enable].name = "tau_s_enable";
  P[VT_tau_s_add].name = "tau_s_add";
  P[VT_vol].name = "vol";
  P[VT_vi].name = "vi";
  P[VT_inverseviF].name = "inverseviF";
  P[VT_inverseviF2].name = "inverseviF2";
  P[VT_inversevssF2].name = "inversevssF2";
  P[VT_volforCall].name = "volforCall";
  P[VT_INVERSECAPACITANCE].name = "INVERSECAPACITANCE";
  P[VT_VcdVsr].name = "VcdVsr";
  P[VT_Kupsquare].name = "Kupsquare";
  P[VT_BufcPKbufc].name = "BufcPKbufc";
  P[VT_Kbufcsquare].name = "Kbufcsquare";
  P[VT_Kbufc2].name = "Kbufc2";
  P[VT_BufsrPKbufsr].name = "BufsrPKbufsr";
  P[VT_Kbufsrsquare].name = "Kbufsrsquare";
  P[VT_Kbufsr2].name = "Kbufsr2";
  P[VT_KopKNaNao].name = "KopKNaNao";
  P[VT_KmNai3].name = "KmNai3";
  P[VT_Nao3].name = "Nao3";
  P[VT_StepCai].name = "StepCai";
  P[VT_inverseRTONF].name = "inverseRTONF";
  P[VT_RTONF].name = "RTONF";
  P[VT_InitTableDone].name = "InitTableDone";
  P[VT_tincforTab].name = "tincforTab";
  P[VT_Ltotmax].name = "Ltotmax";
  P[VT_Gstot].name = "Gstot";
  P[VT_Kl].name = "Kl";
  P[VT_Kr].name = "Kr";
  P[VT_Kc].name = "Kc";
  P[VT_k_barkp].name = "k_barkp";
  P[VT_k_barkm].name = "k_barkm";
  P[VT_k_pkap].name = "k_pkap";
  P[VT_k_pkam].name = "k_pkam";
  P[VT_k_gact].name = "k_gact";
  P[VT_k_hyd].name = "k_hyd";
  P[VT_k_reassoc].name = "k_reassoc";
  P[VT_AC_tot].name = "AC_tot";
  P[VT_ATP].name = "ATP";
  P[VT_PDE3tot].name = "PDE3tot";
  P[VT_PDE4tot].name = "PDE4tot";
  P[VT_IBMXtot].name = "IBMXtot";
  P[VT_Fsktot].name = "Fsktot";
  P[VT_k_ac_basal].name = "k_ac_basal";
  P[VT_k_ac_gsa].name = "k_ac_gsa";
  P[VT_k_ac_fsk].name = "k_ac_fsk";
  P[VT_Km_basal].name = "Km_basal";
  P[VT_Km_gsa].name = "Km_gsa";
  P[VT_Km_fsk].name = "Km_fsk";
  P[VT_Kgsa].name = "Kgsa";
  P[VT_Kfsk].name = "Kfsk";
  P[VT_k_pde3].name = "k_pde3";
  P[VT_Km_pde3].name = "Km_pde3";
  P[VT_k_pde4].name = "k_pde4";
  P[VT_Km_pde4].name = "Km_pde4";
  P[VT_Ki_ibmx].name = "Ki_ibmx";
  P[VT_PKAItot].name = "PKAItot";
  P[VT_PKAIItot].name = "PKAIItot";
  P[VT_PKItot].name = "PKItot";
  P[VT_Ka].name = "Ka";
  P[VT_Kb].name = "Kb";
  P[VT_Kd].name = "Kd";
  P[VT_Ki_pki].name = "Ki_pki";
  P[VT_epsilon].name = "epsilon";
  P[VT_PLBtot].name = "PLBtot";
  P[VT_PP1tot].name = "PP1tot";
  P[VT_Inhib1tot].name = "Inhib1tot";
  P[VT_k_pka_plb].name = "k_pka_plb";
  P[VT_Km_pka_plb].name = "Km_pka_plb";
  P[VT_k_pp1_plb].name = "k_pp1_plb";
  P[VT_Km_pp1_plb].name = "Km_pp1_plb";
  P[VT_k_pka_i1].name = "k_pka_i1";
  P[VT_Km_pka_i1].name = "Km_pka_i1";
  P[VT_Vmax_pp2a_i1].name = "Vmax_pp2a_i1";
  P[VT_Km_pp2a_i1].name = "Km_pp2a_i1";
  P[VT_Ki_inhib1].name = "Ki_inhib1";
  P[VT_LCCtot].name = "LCCtot";
  P[VT_PKAIIlcctot].name = "PKAIIlcctot";
  P[VT_PP1lcctot].name = "PP1lcctot";
  P[VT_PP2Alcctot].name = "PP2Alcctot";
  P[VT_k_pka_lcc].name = "k_pka_lcc";
  P[VT_Km_pka_lcc].name = "Km_pka_lcc";
  P[VT_k_pp1_lcc].name = "k_pp1_lcc";
  P[VT_Km_pp1_lcc].name = "Km_pp1_lcc";
  P[VT_k_pp2a_lcc].name = "k_pp2a_lcc";
  P[VT_Km_pp2a_lcc].name = "Km_pp2a_lcc";
  P[VT_RyRtot].name = "RyRtot";
  P[VT_PKAIIryrtot].name = "PKAIIryrtot";
  P[VT_PP1ryr].name = "PP1ryr";
  P[VT_PP2Aryr].name = "PP2Aryr";
  P[VT_kcat_pka_ryr].name = "kcat_pka_ryr";
  P[VT_Km_pka_ryr].name = "Km_pka_ryr";
  P[VT_kcat_pp1_ryr].name = "kcat_pp1_ryr";
  P[VT_Km_pp1_ryr].name = "Km_pp1_ryr";
  P[VT_kcat_pp2a_ryr].name = "kcat_pp2a_ryr";
  P[VT_Km_pp2a_ryr].name = "Km_pp2a_ryr";
  P[VT_TnItot].name = "TnItot";
  P[VT_PP2Atni].name = "PP2Atni";
  P[VT_kcat_pka_tni].name = "kcat_pka_tni";
  P[VT_Km_pka_tni].name = "Km_pka_tni";
  P[VT_kcat_pp2a_tni].name = "kcat_pp2a_tni";
  P[VT_Km_pp2a_tni].name = "Km_pp2a_tni";
  P[VT_Iks_tot].name = "Iks_tot";
  P[VT_Yotiao_tot].name = "Yotiao_tot";
  P[VT_K_yotiao].name = "K_yotiao";
  P[VT_PKAII_ikstot].name = "PKAII_ikstot";
  P[VT_PP1_ikstot].name = "PP1_ikstot";
  P[VT_k_pka_iks].name = "k_pka_iks";
  P[VT_Km_pka_iks].name = "Km_pka_iks";
  P[VT_k_pp1_iks].name = "k_pp1_iks";
  P[VT_Km_pp1_iks].name = "Km_pp1_iks";
  P[VT_L_init].name = "init_L";
  P[VT_R_init].name = "init_R";
  P[VT_Gs_init].name = "init_Gs";
  P[VT_b1ARd_init].name = "init_b1ARd";
  P[VT_b1ARtot_init].name = "init_b1ARtot";
  P[VT_b1ARp_init].name = "init_b1ARp";
  P[VT_Gsagtptot_init].name = "init_Gsagtptot";
  P[VT_Gsagdp_init].name = "init_Gsagdp";
  P[VT_Gsbg_init].name = "init_Gsbg";
  P[VT_Gsa_gtp_init].name = "init_Gsa_gtp";
  P[VT_AC_init].name = "init_AC";
  P[VT_Fsk_init].name = "init_Fsk";
  P[VT_PDE_init].name = "init_PDE";
  P[VT_IBMX_init].name = "init_IBMX";
  P[VT_cAMPtot_init].name = "init_cAMPtot";
  P[VT_cAMP_init].name = "init_cAMP";
  P[VT_PKACI_init].name = "init_PKACI";
  P[VT_PKACII_init].name = "init_PKACII";
  P[VT_PLBs_init].name = "init_PLBs";
  P[VT_Inhib1ptot_init].name = "init_Inhib1ptot";
  P[VT_Inhib1p_init].name = "init_Inhib1p";
  P[VT_PP1_init].name = "init_PP1";
  P[VT_LCCap_init].name = "init_LCCap";
  P[VT_LCCbp_init].name = "init_LCCbp";
  P[VT_RyRp_init].name = "init_RyRp";
  P[VT_TnIp_init].name = "init_TnIp";
  P[VT_Iks_init].name = "init_Iks";
  P[VT_Yotiao_init].name = "init_Yotiao";
  P[VT_Iksp_init].name = "init_Iksp";
  P[VT_t_init].name = "init_t";
  P[VT_xs05_init].name = "init_xs05";
  P[VT_trel_init].name = "init_trel";
  P[VT_p_PLB].name = "p_PLB";
  P[VT_fac_KmNa].name = "fac_KmNa";
  P[VT_fac_knak].name = "fac_knak";
  P[VT_Amp].name = "Amp";

  // Solver Specifications
  P[VT_abstol].name = "abstol";
  P[VT_reltol].name = "reltol";
  P[VT_hmax].name = "hmax";
  P[VT_mxsteps].name = "mxsteps";

#ifdef ACTIVATE_IKATP_CHANNEL
  P[VT_nicholsarea].name = "nicholsarea";
  P[VT_gkatp].name       = "gkatp";
  P[VT_Mgi].name         = "Mgi";
  P[VT_atpi].name        = "atpi";
  P[VT_adpi].name        = "adpi";
  P[VT_Km_factor].name   = "Km_factor";
# ifdef ISCHEMIA
  P[VT_IschemiaStart].name       = "IschemiaStart";
  P[VT_IschemiaStage1].name      = "IschemiaStage1";
  P[VT_IschemiaStage2].name      = "IschemiaStage2";
  P[VT_ZoneFactor].name          = "ZoneFactor";
  P[VT_DiffusionFactor].name     = "DiffusionFactor";
  P[VT_RestoreIschemia].name     = "RestoreIschemia";
  P[VT_Ko_ZoneFactor_Begin].name = "Ko_ZoneFactorVariance_Begin";
  P[VT_Ko_ZoneFactor_End].name   = "Ko_ZoneFactorVariance_End";

  // P[VT_dVmNa_ZoneFactor_Begin].name = "dVmNa_ZoneFactorVariance_Begin";
  // P[VT_dVmNa_ZoneFactor_End].name = "dVmNa_ZoneFactorVariance_End";
  P[VT_fpH_ZoneFactor_Begin].name = "fpH_ZoneFactorVariance_Begin";
  P[VT_fpH_ZoneFactor_End].name   = "fpH_ZoneFactorVariance_End";
  P[VT_pO_ZoneFactor_Begin].name  = "pO_ZoneFactorVariance_Begin";
  P[VT_pO_ZoneFactor_End].name    = "pO_ZoneFactorVariance_End";
  P[VT_K_o_stage1].name           = "K_o_stage1";
  P[VT_K_o_stage2].name           = "K_o_stage2";
  P[VT_gCaL_stage1].name          = "gCaL_stage1";
  P[VT_gCaL_stage2].name          = "gCaL_stage2";
  P[VT_gNa_stage1].name           = "gNa_stage1";
  P[VT_gNa_stage2].name           = "gNa_stage2";
  P[VT_Mgi_stage1].name           = "Mgi_stage1";
  P[VT_Mgi_stage2].name           = "Mgi_stage2";
  P[VT_ATP_stage1].name           = "ATP_stage1";
  P[VT_ATP_stage2].name           = "ATP_stage2";
  P[VT_ADP_stage1].name           = "ADP_stage1";
  P[VT_ADP_stage2].name           = "ADP_stage2";
  P[VT_ADP_stage2].name           = "ADP_stage2";
  P[VT_dVmNa_stage0].name         = "dVmNa_stage0";
  P[VT_dVmNa_stage1].name         = "dVmNa_stage1";
  P[VT_dVmNa_stage2].name         = "dVmNa_stage2";
# endif // ifdef ISCHEMIA
#endif // ifdef ACTIVATE_IKATP_CHANNEL

  P[VT_RTONF].readFromFile = false;
  P[VT_inverseRTONF].readFromFile = false;
  P[VT_vol].readFromFile = false;
  P[VT_vi].readFromFile = false;
  P[VT_vi].readFromFile = false;
  P[VT_inverseviF].readFromFile = false;
  P[VT_inverseviF2].readFromFile = false;
  P[VT_inversevssF2].readFromFile = false;
  P[VT_volforCall].readFromFile = false;
  P[VT_INVERSECAPACITANCE].readFromFile = false;
  P[VT_VcdVsr].readFromFile = false;
  P[VT_Kupsquare].readFromFile = false;
  P[VT_BufcPKbufc].readFromFile = false;
  P[VT_Kbufcsquare].readFromFile = false;
  P[VT_Kbufc2].readFromFile = false;
  P[VT_BufsrPKbufsr].readFromFile = false;
  P[VT_Kbufsrsquare].readFromFile = false;
  P[VT_Kbufsr2].readFromFile = false;
  P[VT_KopKNaNao].readFromFile = false;
  P[VT_KmNai3].readFromFile = false;
  P[VT_Nao3].readFromFile = false;
  P[VT_StepCai].readFromFile = false;
  P[VT_tincforTab].readFromFile = false;
  P[VT_InitTableDone].readFromFile = false;
  P[VT_tincforTab].readFromFile = false;
  P[VT_pi].readFromFile = false;

#ifdef ACTIVATE_IKATP_CHANNEL

  // Ischemia
  P[VT_f_T].readFromFile        = false;
  P[VT_gammaconst].readFromFile = false;
#endif // ifdef ACTIVATE_IKATP_CHANNEL

#ifdef ISCHEMIA
  P[VT_Ko_ZoneFactor_Begin].readFromFile = false;
  P[VT_Ko_ZoneFactor_End].readFromFile   = false;

  // P[VT_dVmNa_ZoneFactor_Begin].readFromFile=false;
  // P[VT_dVmNa_ZoneFactor_End].readFromFile=false;
  P[VT_fpH_ZoneFactor_Begin].readFromFile = false;
  P[VT_fpH_ZoneFactor_End].readFromFile   = false;
  P[VT_pO_ZoneFactor_Begin].readFromFile  = false;
  P[VT_pO_ZoneFactor_End].readFromFile    = false;
  P[VT_K_o_stage1].readFromFile           = false;
  P[VT_K_o_stage2].readFromFile           = false;
  P[VT_gCaL_stage1].readFromFile          = false;
  P[VT_gCaL_stage2].readFromFile          = false;
  P[VT_gNa_stage1].readFromFile           = false;
  P[VT_gNa_stage2].readFromFile           = false;
  P[VT_Mgi_stage1].readFromFile           = false;
  P[VT_Mgi_stage2].readFromFile           = false;
  P[VT_ATP_stage1].readFromFile           = false;
  P[VT_ATP_stage2].readFromFile           = false;
  P[VT_ADP_stage1].readFromFile           = false;
  P[VT_ADP_stage2].readFromFile           = false;
  P[VT_dVmNa_stage0].readFromFile         = false;
  P[VT_dVmNa_stage1].readFromFile         = false;
  P[VT_dVmNa_stage2].readFromFile         = false;
#endif // ifdef ISCHEMIA
  ParameterLoader EPL(initFile, EMT_SaucermanTusscher);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  // End Initialization of the Parameters ...

  Calculate();
  InitTable();
} // SaucermanTusscherParameters::Init

void SaucermanTusscherParameters::Calculate() {
#if KADEBUG
  cerr << "#SaucermanTusscherParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_pi].value = M_PI;
  P[VT_RTONF].value = (P[VT_R].value) * (P[VT_Tx].value) / (P[VT_F].value);
  P[VT_inverseRTONF].value = 1 / (P[VT_RTONF].value);
  P[VT_vol].value =
      (P[VT_pi].value) * (P[VT_lenght].value) * (P[VT_radius].value) * (P[VT_radius].value);
  P[VT_vi].value = 0.49 * (P[VT_vol].value);
  P[VT_vi].value = 0.016404;
  P[VT_inverseviF].value = 1 / ((P[VT_vi].value) * (P[VT_F].value));
  P[VT_inverseviF2].value = 1. / (2 * (P[VT_vi].value) * (P[VT_F].value));
  P[VT_inversevssF2].value = 1 / (2 * ((P[VT_vi].value) / 300.) * (P[VT_F].value));
  P[VT_volforCall].value = P[VT_vol].value / 1000000000;
  P[VT_INVERSECAPACITANCE].value = 1. / (P[VT_C].value);
  P[VT_VcdVsr].value = (P[VT_Vc].value) / (P[VT_Vsr].value);
  P[VT_Kupsquare].value = (P[VT_Kup].value) * (P[VT_Kup].value);
  P[VT_BufcPKbufc].value = (P[VT_Bufc].value) + (P[VT_Kbufc].value);
  P[VT_Kbufcsquare].value = (P[VT_Kbufc].value) * (P[VT_Kbufc].value);
  P[VT_Kbufc2].value = 2 * (P[VT_Kbufc].value);
  P[VT_BufsrPKbufsr].value = (P[VT_Bufsr].value) + (P[VT_Kbufsr].value);
  P[VT_Kbufsrsquare].value = (P[VT_Kbufsr].value) * (P[VT_Kbufsr].value);
  P[VT_Kbufsr2].value = 2 * (P[VT_Kbufsr].value);
  P[VT_KopKNaNao].value = (P[VT_K_o].value) + (P[VT_pKNa].value) * (P[VT_Na_o].value);
  P[VT_KmNai3].value = (P[VT_KmNai].value) * (P[VT_KmNai].value) * (P[VT_KmNai].value);
  P[VT_Nao3].value = (P[VT_Na_o].value) * (P[VT_Na_o].value) * (P[VT_Na_o].value);
  P[VT_StepCai].value = (P[VT_CaiMax].value - P[VT_CaiMin].value) / RTDT;

#ifdef ACTIVATE_IKATP_CHANNEL

  // temperature effect of I_Katp channel
  /// (E16)
  P[VT_f_T].value        = pow(1.3, ( (P[VT_Tx].value - 273.15) - 36) / 10); // All simulations correspond to a
                                                                             // temperature of 37°C
  P[VT_gammaconst].value = (P[VT_gkatp].value / P[VT_nicholsarea].value) * P[VT_INVERSECAPACITANCE].value;
#endif // ifdef ACTIVATE_IKATP_CHANNEL
#ifdef ISCHEMIA

  // the following values should be transferred to the *.ev file - if necessary ...
  P[VT_Ko_ZoneFactor_Begin].value = 0;
  P[VT_Ko_ZoneFactor_End].value   = 1;

  // P[VT_dVmNa_ZoneFactor_Begin].value=0.5;
  // P[VT_dVmNa_ZoneFactor_End].value=1;
  P[VT_fpH_ZoneFactor_Begin].value = 0.5;
  P[VT_fpH_ZoneFactor_End].value   = 1;
  P[VT_pO_ZoneFactor_Begin].value  = 0;
  P[VT_pO_ZoneFactor_End].value    = 0.1;
  P[VT_K_o_stage1].value           = 8.7;
  P[VT_K_o_stage2].value           = 12.5;
  P[VT_gCaL_stage1].value          = P[VT_g_CaL].value * 0.875;
  P[VT_gCaL_stage2].value          = P[VT_g_CaL].value * 0.75;
  P[VT_gNa_stage1].value           = P[VT_g_Na].value * 0.875;
  P[VT_gNa_stage2].value           = P[VT_g_Na].value * 0.75;
  P[VT_Mgi_stage1].value           = 3;
  P[VT_Mgi_stage2].value           = 6;
  P[VT_ATP_stage1].value           = 5.7;
  P[VT_ATP_stage2].value           = 4.6;
  P[VT_ADP_stage1].value           = 57;
  P[VT_ADP_stage2].value           = 99;

  P[VT_dVmNa_stage0].value = 0;
  P[VT_dVmNa_stage1].value = 1.7;
  P[VT_dVmNa_stage2].value = 3.4;

  // see shaw97a p 270 - added by dw
#endif // ifdef ISCHEMIA

  // if (PrintParameterMode == PrintParameterModeOn) PrintParameters();
} // SaucermanTusscherParameters::Calculate

void SaucermanTusscherParameters::InitTableWithtinc(ML_CalcType _tinc) {
  P[VT_tincforTab].value = _tinc;
  InitTable();
  P[VT_InitTableDone].value = 1;
}

void SaucermanTusscherParameters::InitTable() {
#if KADEBUG
  cerr << "#SaucermanTusscherParameters - InitTable ..." << endl;
#endif // if KADEBUG
  for (double V = -RangeTabhalf + .0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int) (DivisionTab * (RangeTabhalf + V) + .5);
    const ML_CalcType rec_iNaK =
        (1. / (1. + 0.1245 * exp(-0.1 * V * (P[VT_inverseRTONF].value)) +
               0.0353 * exp(-V * (P[VT_inverseRTONF].value))));
    NaK_P1[Vi] = rec_iNaK;
    rec_ipK[Vi] = P[VT_g_pK].value / (1. + exp((25. - V) / 5.98));  // new_g
    const double a_m = 1. / (1. + exp((-60. - V) / 5.));
    const double b_m = 0.1 / (1. + exp((V + 35.) / 5.)) + 0.10 / (1. + exp((V - 50.) / 200.));
    tau_m[Vi] = a_m * b_m;
    m_inf[Vi] = 1. / ((1. + exp((-56.86 - V) / 9.03)) * (1. + exp((-56.86 - V) / 9.03)));
    if (V >= -40.) {
      const ML_CalcType AH_1 = 0.;
      const ML_CalcType BH_1 = (0.77 / (0.13 * (1. + exp(-(V + 10.66) / 11.1))));
      tau_h[Vi] = 1.0 / (AH_1 + BH_1);
    } else {
      const ML_CalcType AH_2 = (0.057 * exp(-(V + 80.) / 6.8));
      const ML_CalcType BH_2 = (2.7 * exp(0.079 * V) + (3.1e5) * exp(0.3485 * V));
      tau_h[Vi] = 1.0 / (AH_2 + BH_2);
    }

    h_inf[Vi] = 1. / ((1. + exp((V + 71.55) / 7.43)) * (1. + exp((V + 71.55) / 7.43)));
    if (V >= -40.) {
      const ML_CalcType AJ_1 = 0.;
      const ML_CalcType BJ_1 = (0.6 * exp((0.057) * V) / (1. + exp(-0.1 * (V + 32.))));
      tau_j[Vi] = 1.0 / (AJ_1 + BJ_1);
    } else {
      const ML_CalcType AJ_2 = (((-2.5428e4) * exp(0.2444 * V) - (6.948e-6) *
                                                                 exp(-0.04391 * V)) * (V + 37.78) /
                                (1. + exp(0.311 * (V + 79.23))));
      const ML_CalcType BJ_2 = (0.02424 * exp(-0.01052 * V) / (1. + exp(-0.1378 * (V + 40.14))));
      tau_j[Vi] = 1.0 / (AJ_2 + BJ_2);
    }
    j_inf[Vi] = h_inf[Vi];
    Xr1_inf[Vi] = 1. / (1. + exp((-26. - V) / 7.));
    const ML_CalcType a_Xr1 = 450. / (1. + exp((-45. - V) / 10.));
    const ML_CalcType b_Xr1 = 6. / (1. + exp((V - (-30.)) / 11.5));
    tau_Xr1[Vi] = a_Xr1 * b_Xr1;
    Xr2_inf[Vi] = 1. / (1. + exp((V - (-88.)) / 24.));
    const ML_CalcType a_Xr2 = 3. / (1. + exp((-60. - V) / 20.));
    const ML_CalcType b_Xr2 = 1.12 / (1. + exp((V - 60.) / 20.));
    tau_Xr2[Vi] = a_Xr2 * b_Xr2;
    Xs_inf[Vi] = 1. / (1. + exp((-5. - V) / 14.));
    tau_Xs[Vi] = (1400. / (sqrt(1. + exp((5. - V) / 6)))) * (1. / (1. + exp((V - 35.) / 15.))) + 80;
    r_inf[Vi] = 1. / (1. + exp((20 - V) / 6.));                        // 56
    s_inf[Vi] =
        1. / (1. + exp((V + (P[VT_s_inf_vHalf].value)) / 5.)); // =1./(1.+exp((V+20)/5.));    //58
    tau_r[Vi] = 9.5 * exp(-(V + 40.) * (V + 40.) / 1800.) + 0.8;           // 57
    tau_s[Vi] = P[VT_tau_s_f1].value *
                exp(-(V + (P[VT_tau_s_vHalf1].value)) * (V + (P[VT_tau_s_vHalf1].value)) /
                    P[VT_tau_s_slope1].value) +
                P[VT_tau_s_enable].value * 5. / (1. + exp((V - 20.) / 5.)) + P[VT_tau_s_add].value;
    d_inf[Vi] = 1. / (1. + exp((-8. - V) / 7.5)); // 41 n7
    const ML_CalcType a_d = 1.4 / (1. + exp((-35 - V) / 13)) + 0.25;
    const ML_CalcType b_d = 1.4 / (1. + exp((V + 5) / 5));
    const ML_CalcType c_d = 1. / (1. + exp((50 - V) / 20));
    tau_d[Vi] = a_d * b_d + c_d;
    f_inf[Vi] = 1. / (1. + exp((V + 20) / 7));  // 46 n12
    const ML_CalcType V27square = (V + 27) * (V + 27);                   // new
    const ML_CalcType exsquare = V27square / (15 * 15);               // new
    const ML_CalcType a_f = 1102.5 * exp(-exsquare);           // new n13
    const ML_CalcType b_f = 200. / (1. + exp((13 - V) / 10.));       // new n14
    const ML_CalcType g_f = (180. / (1. + exp((V + 30) / 10.))) + 20.; // new n15
    tau_f[Vi] = a_f + b_f + g_f;                                      // new n16
    f2_inf[Vi] = (.67 / (1. + exp((V + 35.) / 7.))) + .33;                   // new n17
    const ML_CalcType a_f2 = 600. * exp(-(V + 25) * (V + 25) / 170.);        // new n18
    const ML_CalcType b_f2 = 31. / (1. + exp((25 - V) / 10.));             // new n19
    const ML_CalcType c_f2 = 16. / (1. + exp((V + 30) / 10.));             // new n20
    tau_f2[Vi] = a_f2 + b_f2 + c_f2;                                   // new n21
    // const ML_CalcType
    // NaCaP1=(P[VT_kNaCa].value)*(1./((P[VT_KmNai3].value)+(P[VT_Nao3].value)))*(1./((P[VT_KmCa].value)+(P[VT_Ca_o].value)))*(1./(1.+(P[VT_ksat].value)*exp(((P[VT_n].value)-1.)*V*(P[VT_inverseRTONF].value))));
    // const ML_CalcType NaCaP2=exp((P[VT_n].value)*V*(P[VT_inverseRTONF].value))*(P[VT_Ca_o].value);
    // const ML_CalcType NaCaP3=exp(((P[VT_n].value)-1)*V*(P[VT_inverseRTONF].value))*(P[VT_Nao3].value)*2.5;
    NaCa_P1[Vi] =
        ((P[VT_kNaCa].value) * (1. / ((P[VT_KmNai3].value) + (P[VT_Nao3].value))) *
         (1. / ((P[VT_KmCa].value) + (P[VT_Ca_o].value))) *
         (1. / (1. + (P[VT_ksat].value) *
                     exp(((P[VT_n].value) - 1.) * V * (P[VT_inverseRTONF].value))))) *
        (exp((P[VT_n].value) * V * (P[VT_inverseRTONF].value)) *
         (P[VT_Ca_o].value));  // (exp(n*vv/RTONF)*Nai*Nai*NaiP[VT_C].valueao);
    NaCa_P2[Vi] =
        ((P[VT_kNaCa].value) * (1. / ((P[VT_KmNai3].value) + (P[VT_Nao3].value))) *
         (1. / ((P[VT_KmCa].value) + (P[VT_Ca_o].value))) *
         (1. / (1. + (P[VT_ksat].value) *
                     exp(((P[VT_n].value) - 1.) * V * (P[VT_inverseRTONF].value))))) *
        (exp(((P[VT_n].value) - 1) * V / (P[VT_RTONF].value)) * (P[VT_Na_o].value) *
         (P[VT_Na_o].value) * (P[VT_Na_o].value) * 2.5);  //
    // *7.5);//*2.5);
    CaL_P1[Vi] = 4 * (V - 15.) * (P[VT_F].value) / (P[VT_RTONF].value) *
                 (0.25 * exp(2 * (V - 15.) / (P[VT_RTONF].value))) /
                 (exp(2 * (V - 15.) / (P[VT_RTONF].value)) - 1.);
    CaL_P2[Vi] =
        4 * (V - 15.) * ((P[VT_F].value) / (P[VT_RTONF].value)) * (1 * (P[VT_Ca_o].value)) /
        (exp(2 * (V - 15.) / (P[VT_RTONF].value)) - 1.);
    if ((P[VT_tincforTab].value) == 0) {
      P[VT_tincforTab].value = 0.00001;
    }

    // cout << "P[VT_tincforTab].value = " << P[VT_tincforTab].value << endl;
    const ML_CalcType HT = (P[VT_tincforTab].value) * 1000;
    exptau_m[Vi] = exp(-HT / tau_m[Vi]);
    exptau_h[Vi] = exp(-HT / tau_h[Vi]);
    exptau_j[Vi] = exp(-HT / tau_j[Vi]);
    exptau_Xr1[Vi] = exp(-HT / tau_Xr1[Vi]);
    exptau_Xr2[Vi] = exp(-HT / tau_Xr2[Vi]);
    exptau_Xs[Vi] = exp(-HT / tau_Xs[Vi]);
    exptau_s[Vi] = exp(-HT / tau_s[Vi]);
    exptau_r[Vi] = exp(-HT / tau_r[Vi]);
    exptau_d[Vi] = exp(-HT / tau_d[Vi]);
    exptau_f[Vi] = exp(-HT / tau_f[Vi]);
    exptau_f2[Vi] = exp(-HT / tau_f2[Vi]);
    const double Cai = Vi * (P[VT_StepCai].value);
    ECA[Vi] = ((P[VT_RTONF].value) * 0.5) * (log(((P[VT_Ca_o].value) / Cai)));

#ifdef ACTIVATE_IKATP_CHANNEL

    /* Stuff used by the ATP dependent potassium channel I_Katp */

    // intracellular Na+ ions
    /// (E15)
    KhNa[Vi] = 25.9 * exp(-0.35 * P[VT_inverseRTONF].value * V);
#endif // ifdef ACTIVATE_IKATP_CHANNEL
  }
} // SaucermanTusscherParameters::InitTable
