/*! \file PiperEtAl.cpp
   \brief Implementation of Markov model of IKr
   (based on Piper, PNAS, 2003, S0..S2 are not complete)

   \author fs, CVRTI - University of Utah, USA
 */

#include <PiperEtAl.h>

PiperEtAl::PiperEtAl(PiperEtAlParameters *pp) {
  pPEA = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pPEA, NS_PiperEtAlParameters::vtLast);
#endif // ifdef HETERO
  Init();
  cerr << "new Piper ...\n";
}

PiperEtAl::~PiperEtAl() {}

#ifdef HETERO

inline bool PiperEtAl::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool PiperEtAl::AddHeteroValue(string desc, double val) {
  throw kaBaseException("not possible!\n");
}

#endif // ifdef HETERO

inline int PiperEtAl::GetSize(void) {
  return sizeof(PiperEtAl) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(PiperEtAlParameters *)
#ifdef HETERO
    -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
      ;
}

void PiperEtAl::Init() {
  S0000 = CELLMODEL_PARAMVALUE(VT_S0000);
  S0001 = CELLMODEL_PARAMVALUE(VT_S0001);
  S0011 = CELLMODEL_PARAMVALUE(VT_S0011);
  S0111 = CELLMODEL_PARAMVALUE(VT_S0111);
  S1111 = CELLMODEL_PARAMVALUE(VT_S1111);
  S1112 = CELLMODEL_PARAMVALUE(VT_S1112);
  S1122 = CELLMODEL_PARAMVALUE(VT_S1122);
  S1222 = CELLMODEL_PARAMVALUE(VT_S1222);
  S2222 = CELLMODEL_PARAMVALUE(VT_S2222);
  C1 = CELLMODEL_PARAMVALUE(VT_C1);
  C2 = CELLMODEL_PARAMVALUE(VT_C2);
  O1 = CELLMODEL_PARAMVALUE(VT_O1);
  O2 = CELLMODEL_PARAMVALUE(VT_O2);
  I0 = CELLMODEL_PARAMVALUE(VT_I0);
  I1 = CELLMODEL_PARAMVALUE(VT_I1);
  I2 = CELLMODEL_PARAMVALUE(VT_I2);
}

void PiperEtAl::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' ';
  tempstr << S0000 << ' ';
  tempstr << S0001 << ' ';
  tempstr << S0011 << ' ';  // 5
  tempstr << S0111 << ' ';
  tempstr << S1111 << ' ';
  tempstr << S1112 << ' ';
  tempstr << S1122 << ' ';
  tempstr << S1222 << ' ';  // 10
  tempstr << S2222 << ' ';
  tempstr << C1 << ' ';  // 12
  tempstr << C2 << ' ';
  tempstr << O1 << ' ';
  tempstr << O2 << ' ';
  tempstr << I0 << ' ';  // 16
  tempstr << I1 << ' ';
  tempstr << I2 << ' ';
}

void PiperEtAl::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  ML_CalcType i_HERG = CELLMODEL_PARAMVALUE(VT_g_HERG) * (V - CELLMODEL_PARAMVALUE(VT_E_HERG)) * (O1 + O2);
  tempstr << i_HERG << ' ';  // 19
  tempstr
      << S0000 + S0001 + S0011 + S0111 + S1111 + S1112 + S1122 + S1222 + S2222 + C1 + C2 + O1 + O2 +
         I0 + I1 + I2 << ' ';
}

void PiperEtAl::GetParameterNames(vector<string> &getpara) {
  const int numpara = 16;
  const string ParaNames[numpara] =
      {"S0000", "S0001", "S0011", "S0111", "S1111", "S1112", "S1122", "S1222", "S2222", "C1", "C2",
       "O1", "O2", "I0", "I1",
       "I2"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void PiperEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 2;
  const string ParaNames[numpara] = {"I_HERG", "Not open states"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

inline unsigned char PiperEtAl::getSpeed(ML_CalcType adVm) {
  return (unsigned char) (adVm < .15e-6 ? 3 : (adVm < .3e-6 ? 2 : 1));
}

ML_CalcType
PiperEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
                int euler = 1) {
  const ML_CalcType a_S0S1 = CELLMODEL_PARAMVALUE(VT_S0S1A0) * exp(CELLMODEL_PARAMVALUE(VT_S0S1ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_S0S1 = CELLMODEL_PARAMVALUE(VT_S0S1B0) * exp(-CELLMODEL_PARAMVALUE(VT_S0S1ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType a_S1S2 = CELLMODEL_PARAMVALUE(VT_S1S2A0) * exp(CELLMODEL_PARAMVALUE(VT_S1S2ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_S1S2 = CELLMODEL_PARAMVALUE(VT_S1S2B0) * exp(-CELLMODEL_PARAMVALUE(VT_S1S2ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType a_S2C1 = CELLMODEL_PARAMVALUE(VT_S2C1A0) * exp(CELLMODEL_PARAMVALUE(VT_S2C1ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_S2C1 = CELLMODEL_PARAMVALUE(VT_S2C1B0) * exp(-CELLMODEL_PARAMVALUE(VT_S2C1ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType a_C1C2 = CELLMODEL_PARAMVALUE(VT_C1C2A0) * exp(CELLMODEL_PARAMVALUE(VT_C1C2ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_C1C2 = CELLMODEL_PARAMVALUE(VT_C1C2B0) * exp(-CELLMODEL_PARAMVALUE(VT_C1C2ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType a_C2O1 = CELLMODEL_PARAMVALUE(VT_C2O1A0) * exp(CELLMODEL_PARAMVALUE(VT_C2O1ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_C2O1 = CELLMODEL_PARAMVALUE(VT_C2O1B0) * exp(-CELLMODEL_PARAMVALUE(VT_C2O1ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType a_O1O2 = CELLMODEL_PARAMVALUE(VT_O1O2A0) * exp(CELLMODEL_PARAMVALUE(VT_O1O2ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_O1O2 = CELLMODEL_PARAMVALUE(VT_O1O2B0) * exp(-CELLMODEL_PARAMVALUE(VT_O1O2ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType a_C2I0 = CELLMODEL_PARAMVALUE(VT_C2I0A0) * exp(CELLMODEL_PARAMVALUE(VT_C2I0ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_C2I0 = CELLMODEL_PARAMVALUE(VT_C2I0B0) * exp(-CELLMODEL_PARAMVALUE(VT_C2I0ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType a_O1I1 = CELLMODEL_PARAMVALUE(VT_O1I1A0) * exp(CELLMODEL_PARAMVALUE(VT_O1I1ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_O1I1 = CELLMODEL_PARAMVALUE(VT_O1I1B0) * exp(-CELLMODEL_PARAMVALUE(VT_O1I1ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType a_O2I2 = CELLMODEL_PARAMVALUE(VT_O2I2A0) * exp(CELLMODEL_PARAMVALUE(VT_O2I2ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_O2I2 = CELLMODEL_PARAMVALUE(VT_O2I2B0) * exp(-CELLMODEL_PARAMVALUE(VT_O2I2ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType a_I0I1 = CELLMODEL_PARAMVALUE(VT_I0I1A0) * exp(CELLMODEL_PARAMVALUE(VT_I0I1ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_I0I1 = CELLMODEL_PARAMVALUE(VT_I0I1B0) * exp(-CELLMODEL_PARAMVALUE(VT_I0I1ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType a_I1I2 = CELLMODEL_PARAMVALUE(VT_I1I2A0) * exp(CELLMODEL_PARAMVALUE(VT_I1I2ZA) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType b_I1I2 = CELLMODEL_PARAMVALUE(VT_I1I2B0) * exp(-CELLMODEL_PARAMVALUE(VT_I1I2ZB) * V * CELLMODEL_PARAMVALUE(VT_FdRT));
  const ML_CalcType S0000S0001 = 4. * a_S0S1 * S0000 - b_S0S1 * S0001;
  const ML_CalcType c0 = CELLMODEL_PARAMVALUE(VT_S0S1C);
  const ML_CalcType S0001S0011 = 3. * c0 * a_S0S1 * S0001 - 2. / c0 * b_S0S1 * S0011;
  const ML_CalcType c02 = c0 * c0;
  const ML_CalcType S0011S0111 = 2. * c02 * a_S0S1 * S0011 - 3. / c02 * b_S0S1 * S0111;
  const ML_CalcType c03 = c02 * c0;
  const ML_CalcType S0111S1111 = c03 * a_S0S1 * S0111 - 4. / c03 * b_S0S1 * S1111;
  const ML_CalcType S1111S1112 = 4. * a_S1S2 * S1111 - b_S1S2 * S1112;
  const ML_CalcType c1 = CELLMODEL_PARAMVALUE(VT_S1S2C);
  const ML_CalcType S1112S1122 = 3. * c1 * a_S1S2 * S1112 - 2. / c1 * b_S1S2 * S1122;
  const ML_CalcType c12 = c1 * c1;
  const ML_CalcType S1122S1222 = 2. * c12 * a_S1S2 * S1122 - 3. / c12 * b_S1S2 * S1222;
  const ML_CalcType c13 = c12 * c1;
  const ML_CalcType S1222S2222 = c13 * a_S1S2 * S1222 - 4. / c13 * b_S1S2 * S2222;
  const ML_CalcType S2222C1 = a_S2C1 * S2222 - b_S2C1 * C1;
  const ML_CalcType C1C2 = a_C1C2 * C1 - b_C1C2 * C2;
  const ML_CalcType C2O1 = a_C2O1 * C2 - b_C2O1 * O1;
  const ML_CalcType O1O2 = a_O1O2 * O1 - b_O1O2 * O2;
  const ML_CalcType C2I0 = a_C2I0 * C2 - b_C2I0 * I0;
  const ML_CalcType O1I1 = a_O1I1 * O1 - b_O1I1 * I1;
  const ML_CalcType O2I2 = a_O2I2 * O2 - b_O2I2 * I2;
  const ML_CalcType I0I1 = a_I0I1 * I0 - b_I0I1 * I1;
  const ML_CalcType I1I2 = a_I1I2 * I1 - b_I1I2 * I2;

  S0001 -= tinc * (S0001S0011 - S0000S0001);
  S0011 -= tinc * (S0011S0111 - S0001S0011);
  S0111 -= tinc * (S0111S1111 - S0011S0111);
  S1111 -= tinc * (S1111S1112 - S0111S1111);
  S1112 -= tinc * (S1112S1122 - S1111S1112);
  S1122 -= tinc * (S1122S1222 - S1112S1122);
  S1222 -= tinc * (S1222S2222 - S1122S1222);
  S2222 -= tinc * (S2222C1 - S1222S2222);
  C1 -= tinc * (C1C2 - S2222C1);
  C2 -= tinc * (C2O1 + C2I0 - C1C2);
  O1 -= tinc * (O1O2 + O1I1 - C2O1);
  O2 -= tinc * (O2I2 - O1O2);
  I0 -= tinc * (I0I1 - C2I0);
  I1 -= tinc * (I1I2 - I0I1 - O1I1);
  I2 -= tinc * (-O2I2 - I1I2);
  S0000 = 1. -
          (S0001 + S0011 + S0111 + S1111 + S1112 + S1122 + S1222 + S2222 + C1 + C2 + O1 + O2 + I0 +
           I1 + I2);
  return tinc * (CELLMODEL_PARAMVALUE(VT_dC_m) * CELLMODEL_PARAMVALUE(VT_g_HERG) * (V - CELLMODEL_PARAMVALUE(VT_E_HERG)) * (O1 + O2) - i_external);
} // PiperEtAl::Calc
