/*      File: CourtemancheEtAl.cpp
    automatically created by ExtractParameterClass.pl - done by dw (27.02.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de

    28.10.2009 - mwk - adaption to dialysis environment

 */

#include <CourtemancheEtAl.h>

Courtemanche::Courtemanche(CourtemancheParameters *pp) {
  pCmP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pCmP, NS_CourtemancheParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

Courtemanche::~Courtemanche() {}

#ifdef HETERO

inline bool Courtemanche::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool Courtemanche::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int Courtemanche::GetSize(void) {
  // return ( &fT - &Ca_i + 1 ) * sizeof( ML_CalcType );
  return sizeof(Courtemanche) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(CourtemancheParameters *)
         #ifdef HETERO
         - sizeof(ParameterSwitch *)
#endif // ifdef HETERO
      ;
}

inline unsigned char Courtemanche::getSpeed(ML_CalcType adVm) {
  return (unsigned char) (adVm < 1e-6 ? 2 : 1);
}

void Courtemanche::Init() {
#if KADEBUG
  cerr << "Courtemanche::Init" << endl;
#endif // if KADEBUG
  Na_i = CELLMODEL_PARAMVALUE(VT_Init_Na_i);
  K_i = CELLMODEL_PARAMVALUE(VT_Init_K_i);
  m = CELLMODEL_PARAMVALUE(VT_Init_m);
  oa = CELLMODEL_PARAMVALUE(VT_Init_oa);
  ua = CELLMODEL_PARAMVALUE(VT_Init_ua);
  d = CELLMODEL_PARAMVALUE(VT_Init_d);
  f_Ca = CELLMODEL_PARAMVALUE(VT_Init_f_Ca);
  Ca_i = CELLMODEL_PARAMVALUE(VT_Init_Ca_i);
  Ca_up = CELLMODEL_PARAMVALUE(VT_Init_Ca_up);
  Ca_rel = CELLMODEL_PARAMVALUE(VT_Init_Ca_rel);
  h = CELLMODEL_PARAMVALUE(VT_Init_h);
  j = CELLMODEL_PARAMVALUE(VT_Init_j);
  oi = CELLMODEL_PARAMVALUE(VT_Init_oi);
  ui = CELLMODEL_PARAMVALUE(VT_Init_ui);
  Xr = CELLMODEL_PARAMVALUE(VT_Init_Xr);
  Xs = CELLMODEL_PARAMVALUE(VT_Init_Xs);
  f = CELLMODEL_PARAMVALUE(VT_Init_f);
  u = CELLMODEL_PARAMVALUE(VT_Init_u);
  v = CELLMODEL_PARAMVALUE(VT_Init_v);
  w = CELLMODEL_PARAMVALUE(VT_Init_w);
  y = CELLMODEL_PARAMVALUE(VT_Init_y);
  dT = CELLMODEL_PARAMVALUE(VT_Init_dT);
  fT = CELLMODEL_PARAMVALUE(VT_Init_fT);
} // Courtemanche::Init

ML_CalcType Courtemanche::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                               ML_CalcType stretch = 1.,
                               int euler = 2) {
  tinc *= 1000.0;
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double dNa_i = 1.0 / Na_i;
  const double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) * dNa_i));
  const double VmE_K = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_K_o)) / K_i));
  const double VmE_Ca = V_int - ((CELLMODEL_PARAMVALUE(VT_RTd2F)) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / (double) Ca_i));
  const double m_m = pCmP->m_m[Vi];
  m = m_m + (m - m_m) * pCmP->exptau_m[Vi];
  checkGatingVariable(m);
  const double m_h = pCmP->m_h[Vi];
  h = m_h + (h - m_h) * pCmP->exptau_h[Vi];
  const double m_j = pCmP->m_j[Vi];
  j = m_j + (j - m_j) * pCmP->exptau_j[Vi];
  const double m_oa = pCmP->m_oa[Vi];
  oa = m_oa + (oa - m_oa) * pCmP->exptau_oa[Vi];
  const double m_oi = pCmP->m_oi[Vi];
  oi = m_oi + (oi - m_oi) * pCmP->exptau_oi[Vi];
  const double m_ua = pCmP->m_ua[Vi];
  ua = m_ua + (ua - m_ua) * pCmP->exptau_ua[Vi];
  const double m_ui = pCmP->m_ui[Vi];
  ui = m_ui + (ui - m_ui) * pCmP->exptau_ui[Vi];
  const double m_Xr = pCmP->m_Xr[Vi];
  Xr = m_Xr + (Xr - m_Xr) * pCmP->exptau_Xr[Vi];
  const double m_Xs = pCmP->m_Xs[Vi];
  Xs = m_Xs + (Xs - m_Xs) * pCmP->exptau_Xs[Vi];
  const double m_d = pCmP->m_d[Vi];
  d = m_d + (d - m_d) * pCmP->exptau_d[Vi];
  const double m_f = pCmP->m_f[Vi];
  f = m_f + (f - m_f) * pCmP->exptau_f[Vi];
  f_Ca += (1.0 / (1.0 + Ca_i * 2857.1429) - f_Ca) * 0.5 * tinc;
  const double m_dT = pCmP->m_dT[Vi];
  dT = m_dT + (dT - m_dT) * pCmP->exptau_dT[Vi];
  const double m_fT = pCmP->m_fT[Vi];
  fT = m_fT + (fT - m_fT) * pCmP->exptau_fT[Vi];
  const double m_w = pCmP->m_w[Vi];
  w = m_w + (w - m_w) * pCmP->exptau_w[Vi];
  const double m_y = pCmP->m_y[Vi];
  y = m_y + (y - m_y) * pCmP->exptau_y[Vi];
  const double I_Na = (CELLMODEL_PARAMVALUE(VT_g_Na)) * m * m * m * h * j * VmE_Na;
  const double I_to = (CELLMODEL_PARAMVALUE(VT_g_to)) * oa * oa * oa * oi * VmE_K;
  const double I_sus = (CELLMODEL_PARAMVALUE(VT_g_sus)) * oa * oa * oa * VmE_K;
  const double I_Kur = pCmP->g_Kur[Vi] * ua * ua * ua * ui * VmE_K;

  // const double I_Kr = pCmP->CKr[Vi]*Xr*VmE_K;
  const double I_Kr =
      pCmP->CKr[Vi] * Xr * VmE_K * sqrt(CELLMODEL_PARAMVALUE(VT_K_o) / 5.4);  // [mwk] adaption to dialysis
  const double I_Ks = (CELLMODEL_PARAMVALUE(VT_g_Ks)) * Xs * Xs * VmE_K;
  const double I_CaL = (CELLMODEL_PARAMVALUE(VT_g_CaL)) * d * f * f_Ca * (V_int - (CELLMODEL_PARAMVALUE(VT_E_rL)));

  //    const double I_CaT= (CELLMODEL_PARAMVALUE(VT_g_CaT))*fT*dT*(V_int-(CELLMODEL_PARAMVALUE(VT_E_CaT)));
  // const double I_K1 = pCmP->CK1[Vi]*VmE_K;
  const double I_K1 = pCmP->CK1[Vi] * VmE_K * sqrt(CELLMODEL_PARAMVALUE(VT_K_o) / 5.4);  // [mwk] adaption to dialysis
  const double I_bNa = (CELLMODEL_PARAMVALUE(VT_g_bNa)) * VmE_Na;
  const double I_bK = (CELLMODEL_PARAMVALUE(VT_g_bK)) * VmE_K;
  const double I_bCa = (CELLMODEL_PARAMVALUE(VT_g_bCa)) * VmE_Ca;
  const double temp = (CELLMODEL_PARAMVALUE(VT_k_mNai)) * dNa_i;
  const double I_NaK = pCmP->CNaK[Vi] / (1.0 + temp * sqrt(temp));
  const double I_pCa = (CELLMODEL_PARAMVALUE(VT_I_pCamax)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mpCa)));
  const double I_NaCa =
      pCmP->CNaCa[Vi] * ((Na_i * Na_i * Na_i * (CELLMODEL_PARAMVALUE(VT_Ca_o))) - pCmP->expVm[Vi] * Ca_i);
  const double I_fNa = (CELLMODEL_PARAMVALUE(VT_g_fNa)) * y * (VmE_Na);
  const double I_fK = (CELLMODEL_PARAMVALUE(VT_g_fK)) * y * (VmE_K);
  const double I_rel = (CELLMODEL_PARAMVALUE(VT_k_rel)) * u * u * v * w * (Ca_rel - Ca_i);
  const double I_up = (CELLMODEL_PARAMVALUE(VT_I_upmax)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_up)));
  const double I_upleak = (CELLMODEL_PARAMVALUE(VT_kupleak)) * Ca_up;
  const double I_tr = (Ca_up - Ca_rel) * (CELLMODEL_PARAMVALUE(VT_dt_tr));

#ifdef SAC_KUIJPERS
  const double I_sacNa = pCmP->Csac1[Vi] * (Na_i-CELLMODEL_PARAMVALUE(VT_Na_o)*exp(-CELLMODEL_PARAMVALUE(VT_FdRT)*V_int));
  const double I_sacK  = pCmP->Csac1[Vi] * (K_i-CELLMODEL_PARAMVALUE(VT_K_o)*exp(-CELLMODEL_PARAMVALUE(VT_FdRT)*V_int));
  const double I_sacCa = pCmP->Csac2[Vi] * (Ca_i-CELLMODEL_PARAMVALUE(VT_Ca_o)*exp(-2.0*CELLMODEL_PARAMVALUE(VT_FdRT)*V_int));
  const double I_sac   = I_sacNa + I_sacK + I_sacCa;
#endif // ifdef SAC_KUIJPERS

  Ca_up += tinc * (I_up - I_tr * (CELLMODEL_PARAMVALUE(VT_VreldVup)) - I_upleak);
  Ca_rel += tinc * ((I_tr - I_rel) / (1.0 + (CELLMODEL_PARAMVALUE(VT_csqnkm)) /
                                            (Ca_rel * Ca_rel + Ca_rel * (CELLMODEL_PARAMVALUE(VT_kmcsqnm2)) +
                                             (CELLMODEL_PARAMVALUE(VT_kkmcsqn)))));
  Na_i += tinc * (-3.0 * I_NaK - 3.0 * I_NaCa - I_bNa - I_Na - I_fNa
#ifdef SAC_KUIJPERS
      - I_sacNa
#endif // ifdef SAC_KUIJPERS
  ) * (CELLMODEL_PARAMVALUE(VT_CmdFvi));
  K_i += tinc * (2.0 * I_NaK - I_K1 - I_to - I_sus - I_Kur - I_Kr - I_Ks - I_bK - I_fK
#ifdef SAC_KUIJPERS
      - I_sacK
#endif // ifdef SAC_KUIJPERS
  ) * (CELLMODEL_PARAMVALUE(VT_CmdFvi));

  // Adjustements according to (Oosterom & Jacquemet, CinC 2009): anchoring concentrations to steady-state values
  Na_i = CELLMODEL_PARAMVALUE(VT_alpha1m) * Na_i + CELLMODEL_PARAMVALUE(VT_alphaNai);
  K_i = CELLMODEL_PARAMVALUE(VT_alpha1m) * K_i + CELLMODEL_PARAMVALUE(VT_alphaKi);

  Ca_i += tinc * ((2.0 * I_NaCa - I_pCa - I_CaL - I_bCa
#ifdef SAC_KUIJPERS
                      - I_sacCa
#endif // ifdef SAC_KUIJPERS
                  ) * .5 * (CELLMODEL_PARAMVALUE(VT_CmdFvi)) + (I_upleak - I_up) * (CELLMODEL_PARAMVALUE(VT_VupdVi)) +
                  I_rel * (CELLMODEL_PARAMVALUE(VT_VreldVi))) /
          (1.0 + (CELLMODEL_PARAMVALUE(VT_trpnkm)) / (Ca_i * Ca_i + Ca_i * (CELLMODEL_PARAMVALUE(VT_kmtrpnm2)) + (CELLMODEL_PARAMVALUE(VT_kkmtrpn))) +
           (CELLMODEL_PARAMVALUE(VT_cmdnkm)) /
           (Ca_i * Ca_i + Ca_i * (CELLMODEL_PARAMVALUE(VT_kmcmdnm2)) + (CELLMODEL_PARAMVALUE(VT_kkmcmdn))));
  const double Fn = exp(
      -((CELLMODEL_PARAMVALUE(VT_Vrel)) * I_rel - (0.5 * I_CaL - 0.2 * I_NaCa) * (CELLMODEL_PARAMVALUE(VT_Cmd2F))) * 731.5289);
  const double m_u = 1.0 / (1.0 + Fn * pCmP->exp250);
  u += (m_u - u) * 0.125 * tinc;
  const double m_v = 1.0 - 1.0 / (1.0 + Fn * pCmP->exp50);
  const double t_v = 1.91 + 2.09 * m_u;
  v += (m_v - v) * tinc / t_v;
  double I_tot =
      I_Na + I_CaL + I_pCa + I_K1 + I_to + I_sus + I_Kur + I_Kr + I_Ks + I_bNa + I_bK + I_bCa +
      I_NaK + I_NaCa + I_fK + I_fNa
      #ifdef SAC_KUIJPERS
      + I_sac
      #endif // ifdef SAC_KUIJPERS
      - i_external;
  return -.001 * I_tot *
         tinc; // *.001 to revert back from ms to s (conversion was done at the beginning of this function)
} // Courtemanche::Calc

void Courtemanche::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << Ca_i << ' ' << Ca_up << ' ' << Ca_rel << ' '
          << h << ' ' << j << ' '
          << oi << ' ' << ui << ' '
          << Xr << ' ' << Xs << ' '
          << f << ' ' << u << ' ' << v << ' '
          << w << ' ' << Na_i << ' ' << K_i << ' '
          << m << ' ' << oa << ' ' << ua << ' '
          << d << ' ' << f_Ca << ' '
          << y << ' ' << dT << ' ' << fT << ' ';
}

void Courtemanche::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double dNa_i = 1.0 / Na_i;
  const double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) * dNa_i));
  const double VmE_K = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_K_o)) / K_i));
  const double VmE_Ca = V_int - ((CELLMODEL_PARAMVALUE(VT_RTd2F)) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / (double) Ca_i));
  const double I_Na = (CELLMODEL_PARAMVALUE(VT_g_Na)) * m * m * m * h * j * VmE_Na;
  const double I_to = (CELLMODEL_PARAMVALUE(VT_g_to)) * oa * oa * oa * oi * VmE_K;
  const double I_sus = (CELLMODEL_PARAMVALUE(VT_g_sus)) * oa * oa * oa * VmE_K;
  const double I_Kur = pCmP->g_Kur[Vi] * ua * ua * ua * ui * VmE_K;
  const double I_Kr = pCmP->CKr[Vi] * Xr * VmE_K;
  const double I_Ks = (CELLMODEL_PARAMVALUE(VT_g_Ks)) * Xs * Xs * VmE_K;
  const double I_CaL = (CELLMODEL_PARAMVALUE(VT_g_CaL)) * d * f * f_Ca * (V_int - (CELLMODEL_PARAMVALUE(VT_E_rL)));
  const double I_CaT = (CELLMODEL_PARAMVALUE(VT_g_CaT)) * fT * dT * (V_int - (CELLMODEL_PARAMVALUE(VT_E_CaT)));
  const double I_K1 = pCmP->CK1[Vi] * VmE_K;
  const double I_bNa = (CELLMODEL_PARAMVALUE(VT_g_bNa)) * VmE_Na;
  const double I_bK = (CELLMODEL_PARAMVALUE(VT_g_bK)) * VmE_K;
  const double I_bCa = (CELLMODEL_PARAMVALUE(VT_g_bCa)) * VmE_Ca;
  const double temp = (CELLMODEL_PARAMVALUE(VT_k_mNai)) * dNa_i;
  const double I_NaK = pCmP->CNaK[Vi] / (1.0 + temp * sqrt(temp));
  const double I_pCa = (CELLMODEL_PARAMVALUE(VT_I_pCamax)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mpCa)));
  const double I_NaCa =
      pCmP->CNaCa[Vi] * ((Na_i * Na_i * Na_i * (CELLMODEL_PARAMVALUE(VT_Ca_o))) - pCmP->expVm[Vi] * Ca_i);
  const double I_fNa = (CELLMODEL_PARAMVALUE(VT_g_fNa)) * y * (VmE_Na);
  const double I_fK = (CELLMODEL_PARAMVALUE(VT_g_fK)) * y * (VmE_K);
  const double I_rel = (CELLMODEL_PARAMVALUE(VT_k_rel)) * u * u * v * w * (Ca_rel - Ca_i);
  const double I_up = (CELLMODEL_PARAMVALUE(VT_I_upmax)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_up)));
  const double I_upleak = (CELLMODEL_PARAMVALUE(VT_kupleak)) * Ca_up;
  const double I_tr = (Ca_up - Ca_rel) * (CELLMODEL_PARAMVALUE(VT_dt_tr));
#ifdef SAC_KUIJPERS
  const double I_sacNa = pCmP->Csac1[Vi] * (Na_i-CELLMODEL_PARAMVALUE(VT_Na_o)*exp(-CELLMODEL_PARAMVALUE(VT_FdRT)*V_int));
  const double I_sacK  = pCmP->Csac1[Vi] * (K_i-CELLMODEL_PARAMVALUE(VT_K_o)*exp(-CELLMODEL_PARAMVALUE(VT_FdRT)*V_int));
  const double I_sacCa = pCmP->Csac2[Vi] * (Ca_i-CELLMODEL_PARAMVALUE(VT_Ca_o)*exp(-2.0*CELLMODEL_PARAMVALUE(VT_FdRT)*V_int));
  const double I_sac   = I_sacNa + I_sacK + I_sacCa;
#endif // ifdef SAC_KUIJPERS
  const double I_mem =
      I_Na + I_CaL + I_pCa + I_K1 + I_to + I_sus + I_Kur + I_Kr + I_Ks + I_bNa + I_bK + I_bCa +
      I_NaK + I_NaCa + I_fK
      #ifdef SAC_KUIJPERS
      +I_sac
      #endif // ifdef SAC_KUIJPERS
      + I_fNa;
  tempstr << I_Na << ' ' << I_bNa << ' '
          << I_K1 << ' ' << I_to << ' '
          << I_Kur << ' ' << I_Kr << ' '
          << I_Ks << ' ' << I_CaL << ' '
          << I_NaK << ' ' << I_NaCa << ' '
          << I_bCa << ' ' << I_pCa << ' '
          << I_rel << ' ' << I_tr << ' '
          << I_up << ' ' << I_upleak << ' '
          << I_fNa << ' ' << I_fK << ' '
          << I_CaT << ' ' << I_bK << ' '
          << I_sus << ' '
          #ifdef SAC_KUIJPERS
          <<I_sacNa <<' '<<I_sacK <<' '<<I_sacCa <<' '
          #endif // ifdef SAC_KUIJPERS
          << I_mem << ' ';
} // Courtemanche::LongPrint

void Courtemanche::GetParameterNames(vector<string> &getpara) {
  const string ParaNames[] =
      {"Ca_i", "Ca_up", "Ca_rel", "h", "j", "o_i", "u_i", "Xr", "Xs",
       "f",
       "u",
       "v", "w",
       "Na_i",
       "K_i",
       "m",
       "o_a",
       "u_a",
       "d", "f_Ca", "y", "dT", "fT"};

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void Courtemanche::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const string ParaNames[] =
      {"I_Na", "I_bNa", "I_K1", "I_to", "I_Kur", "I_Kr",
       "I_Ks", "I_CaL",
       "I_NaK", "I_NaCa", "I_bCa", "I_pCa",
       "I_rel", "I_tr", "I_up", "I_upleak", "I_fNa", "I_fK",
       "I_CaT", "I_bK",
       "I_sus"
#ifdef SAC_KUIJPERS
          ,        "I_sacNa",        "I_sacK",         "I_sacCa"
#endif // ifdef SAC_KUIJPERS
          , "I_mem"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}
