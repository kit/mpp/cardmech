/**@file PetersonParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef PETERSON_PARAMETERS
#define PETERSON_PARAMETERS

#include <ParameterLoader.h>

namespace NS_PetersonParameters {
  enum varType {
    VT_P1a = vtFirst,
    VT_P0a,
    VT_N1a,
    VT_p_k_on,
    VT_p_k_on_s,
    VT_p_k_off,
    VT_p_k_off_s,
    VT_p_f,
    VT_p_g,
    VT_p_g_s,

    VT_Fmax,
    VT_dFmax,
    vtLast
  };
}

using namespace NS_PetersonParameters;

class PetersonParameters : public vbNewForceParameters {
public:
  PetersonParameters(const char *);

  ~PetersonParameters() {}

  // virtual inline int GetSize(void){return (&p_g_s-&P1a)*sizeof(T);};
  // virtual inline T* GetBase(void){return P1a;};
  // virtual int GetNumParameters() { return 10; };
  void Init(const char *);

  void Calculate();

  void PrintParameters();
};

#endif // ifndef PETERSON_PARAMETERS
