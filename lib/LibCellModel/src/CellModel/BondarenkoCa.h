/*! \file BondarenkoCa.h
   \brief Markov model of cardiac calcium channels
   Bondarenko, Szigeti, Bett, Kim, and Rasmusson
   Am J Physiol Heart Circ Physiol. 2004 May 13

   \author fs, CVRTI - University of Utah, USA
 */


#ifndef BONDARENKOCA_H
#define BONDARENKOCA_H

#include <ElphyModelBasis.h>
#include <BondarenkoCaParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_BondarenkoCaParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pB->P[NS_BondarenkoCaParameters::a].value
#endif // ifdef HETERO


class BondarenkoCa : public vbElphyModel<ML_CalcType> {
public:
  BondarenkoCaParameters *pB;
  ML_CalcType C1, C2, C3, C4, I1, I2, I3, O;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  BondarenkoCa(BondarenkoCaParameters *);

  //  BondarenkoCa() {};
  ~BondarenkoCa() {}

  virtual inline int GetSize(void) {
    return sizeof(BondarenkoCa) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(BondarenkoCaParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType *GetBase(void) { return &C1; }

  virtual inline ML_CalcType Volume() { return 0.; }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetVm() { return -.080; }

  virtual inline ML_CalcType GetCai() { return 0.0; }

  virtual inline ML_CalcType GetCao() { return 0.0; }

  virtual inline ML_CalcType GetNai() { return 0.0; }

  virtual inline ML_CalcType GetNao() { return 0.0; }

  virtual inline ML_CalcType GetKi() { return 0.0; }

  virtual inline ML_CalcType GetKo() { return 0.0; }

  virtual inline void SetCai(ML_CalcType) {}

  virtual void Init();

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &getpara) {
    const int numpara = 7;
    const string ParaNames[numpara] = {"m", "h", "j", "d", "f", "x1", "Ca_i"};

    for (int i = 0; i < numpara; i++)
      getpara.push_back(ParaNames[i]);
  }

  virtual void GetLongParameterNames(vector<string> &getpara) {
    GetParameterNames(getpara);
    const int numpara = 4;
    const string ParaNames[numpara] = {"I_s", "I_K1", "I_x1", "I_Na"};
    for (int i = 0; i < numpara; i++)
      getpara.push_back(ParaNames[i]);
  }

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char) (adVm < .15e-6 ? 3 : (adVm < .3e-6 ? 2 : 1));
  }

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);
}; // class BondarenkoCa

#endif // ifndef BONDARENKOCA_H
