/* File: ShannonEtAlParameters.h
    Robin Moss - UHZ Freiburg/Bad Krozingen
    Based on https://models.physiomeproject.org/exposure/aeec124feedddcd9139685b2ff1131ae
    xml can also be found in the originals folder*/

#ifndef SHANNONETALPARAMETERS_H
#define SHANNONETALPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_ShannonEtAlParameters {
  enum varType {
    VT_T = vtFirst,
    VT_R,
    VT_F,
    VT_Cm,
    VT_cell_length,
    VT_cell_radius,
    VT_stim_offset,
    VT_stim_period,
    VT_stim_duration,
    VT_stim_amplitude,
    VT_Na_o,
    VT_Ca_o,
    VT_K_o,
    VT_K_i,
    VT_Cl_o,
    VT_Cl_i,
    VT_Mg_i,
    VT_g_INa,
    VT_Fx_Na_SL,
    VT_Fx_Na_jct,
    VT_g_INaBK,
    VT_Fx_NaBk_SL,
    VT_Fx_NaBk_jct,
    VT_g_NaK,
    VT_Fx_NaK_jct,
    VT_Fx_NaK_SL,
    VT_Km_Nai,
    VT_Km_Ko,
    VT_H_NaK,
    VT_g_Kr,
    VT_g_Ks,
    VT_pKNa,
    VT_Fx_Ks_jct,
    VT_Fx_Ks_SL,
    VT_g_Kp,
    VT_g_tos,
    VT_g_tof,
    VT_g_Ki,
    VT_g_Cl,
    VT_Fx_Cl_jct,
    VT_Fx_Cl_SL,
    VT_Kd_ClCa,
    VT_g_Clbk,
    VT_g_CaL,
    VT_PCa,
    VT_PNa,
    VT_PK,
    VT_Fx_ICaL_jct,
    VT_Fx_ICaL_SL,
    VT_gamma_Cai,
    VT_gamma_Cao,
    VT_gamma_Nai,
    VT_gamma_Nao,
    VT_gamma_Ki,
    VT_gamma_Ko,
    VT_Q10_CaL,
    VT_g_NCX,
    VT_Fx_NCX_jct,
    VT_Fx_NCX_SL,
    VT_Q10_NCX,
    VT_K_mNai,
    VT_K_mCao,
    VT_K_mNao,
    VT_K_mCai,
    VT_Kd_act,
    VT_ksat,
    VT_eta,
    VT_HNa,
    VT_g_Cap,
    VT_Fx_SLCaP_jct,
    VT_Fx_SLCaP_SL,
    VT_Q10_SLCaP,
    VT_HCap,
    VT_Km,
    VT_g_CaBK,
    VT_Fx_CaBk_jct,
    VT_Fx_CaBk_SL,
    VT_Max_SR,
    VT_Min_SR,
    VT_EC50_SR,
    VT_ks,
    VT_koCa,
    VT_kom,
    VT_kiCa,
    VT_kim,
    VT_HSR,
    VT_KSRleak,
    VT_KSRPump,
    VT_Q10_SRCaP,
    VT_Kmf,
    VT_Kmr,
    VT_HSRPump,
    VT_Bmax_SL,
    VT_Bmax_jct,
    VT_kon,
    VT_koff,
    VT_Bmax_SLB_SL,
    VT_Bmax_SLB_jct,
    VT_Bmax_SLHigh_SL,
    VT_Bmax_SLHigh_jct,
    VT_Bmax_Calsequestrin,
    VT_kon_SL,
    VT_kon_Calsequestrin,
    VT_koff_SLB,
    VT_koff_SLHigh,
    VT_koff_Calsequestrin,
    VT_Bmax_TroponinC,
    VT_Bmax_TroponinC_Ca_Mg_Ca,
    VT_Bmax_TroponinC_Ca_Mg_Mg,
    VT_Bmax_Calmodulin,
    VT_Bmax_Myosin_Ca,
    VT_Bmax_Myosin_Mg,
    VT_Bmax_SRB,
    VT_kon_TroponinC,
    VT_kon_TroponinC_Ca_Mg_Ca,
    VT_kon_TroponinC_Ca_Mg_Mg,
    VT_kon_Calmodulin,
    VT_kon_Myosin_Ca,
    VT_kon_Myosin_Mg,
    VT_kon_SRB,
    VT_koff_TroponinC,
    VT_koff_TroponinC_Ca_Mg_Ca,
    VT_koff_TroponinC_Ca_Mg_Mg,
    VT_koff_Calmodulin,
    VT_koff_Myosin_Ca,
    VT_koff_Myosin_Mg,
    VT_koff_SRB,
    VT_V_init,
    VT_m_init,
    VT_h_init,
    VT_j_init,
    VT_Xr_init,
    VT_Xs_init,
    VT_Xtos_init,
    VT_Ytos_init,
    VT_Xtof_init,
    VT_Ytof_init,
    VT_Rtos_init,
    VT_d_init,
    VT_f_init,
    VT_fCaB_SL_init,
    VT_fCaB_jct_init,
    VT_R_init,
    VT_I_init,
    VT_O_init,
    VT_Na_SL_buf_init,
    VT_Na_jct_buf_init,
    VT_Na_i_init,
    VT_Na_SL_init,
    VT_Na_jct_init,
    VT_Ca_SLB_SL_init,
    VT_Ca_SLB_jct_init,
    VT_Ca_SLHigh_SL_init,
    VT_Ca_SLHigh_jct_init,
    VT_Ca_Calsequestrin_init,
    VT_Ca_SR_init,
    VT_Ca_SL_init,
    VT_Ca_jct_init,
    VT_Ca_i_init,
    VT_Ca_TroponinC_init,
    VT_Ca_TroponinC_Ca_Mg_init,
    VT_Mg_TroponinC_Ca_Mg_init,
    VT_Ca_Calmodulin_init,
    VT_Ca_Myosin_init,
    VT_Mg_Myosin_init,
    VT_Ca_SRB_init,
    VT_RTdF,
    VT_FdRT,
    VT_sigma,
    VT_E_K,
    VT_Q_CaL,
    VT_Q_NCX,
    VT_Q_SLCaP,
    VT_Q_SRCaP,
    VT_Vol_Cell,
    VT_Vol_SR,
    VT_Vol_SL,
    VT_Vol_jct,
    VT_Vol_myo,
    vtLast
  };
} // namespace NS_ShannonEtAlParameters

using namespace NS_ShannonEtAlParameters;

class ShannonEtAlParameters : public vbNewElphyParameters {
public:
  ShannonEtAlParameters(const char *, ML_CalcType);

  ~ShannonEtAlParameters();

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType);

  void Init(const char *, ML_CalcType);

  ML_CalcType ah[RTDT];
  ML_CalcType bh[RTDT];
  ML_CalcType aj[RTDT];
  ML_CalcType bj[RTDT];
  ML_CalcType am[RTDT];
  ML_CalcType bm[RTDT];
  ML_CalcType f_NaK[RTDT];
  ML_CalcType Xrtau[RTDT];
  ML_CalcType Xrinf[RTDT];
  ML_CalcType rr[RTDT];
  ML_CalcType Xstau[RTDT];
  ML_CalcType Xsinf[RTDT];
  ML_CalcType Xtosinf[RTDT];
  ML_CalcType Xtostau[RTDT];
  ML_CalcType Ytosinf[RTDT];
  ML_CalcType Ytostau[RTDT];
  ML_CalcType Rtosinf[RTDT];
  ML_CalcType Rtostau[RTDT];
  ML_CalcType Xtoftau[RTDT];
  ML_CalcType Ytoftau[RTDT];
  ML_CalcType k1inf[RTDT];
  ML_CalcType dinf[RTDT];
  ML_CalcType dtau[RTDT];
  ML_CalcType finf[RTDT];
  ML_CalcType ftau[RTDT];
}; // class ShannonEtAlParameters

#endif // ifndef SHANNONETALPARAMETERS_H
