/**@file Rice08.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef RICE08
#define RICE08

#include <Rice08Parameters.h>

#undef CELLMODEL_PARAMVALUE
#define CELLMODEL_PARAMVALUE(a) r08p->P[NS_Rice08Parameters::a].value

class Rice08 : public vbForceModel<ML_CalcType> {
public:
  Rice08Parameters *r08p;
  ML_CalcType SL;
  ML_CalcType TRPNCaL;
  ML_CalcType TRPNCaH;
  ML_CalcType xXBpostr;
  ML_CalcType xXBprer;
  ML_CalcType XBpostr;
  ML_CalcType XBprer;
  ML_CalcType N_NoXB;
  ML_CalcType P_NoXB;
  ML_CalcType N;
  ML_CalcType intf;
  ML_CalcType Tension;


  Rice08(Rice08Parameters *);

  ~Rice08() {}

  virtual inline ML_CalcType *GetBase(void) { return &SL; }

  virtual inline int GetSize(void) {
    return sizeof(Rice08) - sizeof(vbForceModel<ML_CalcType>) - sizeof(Rice08Parameters *);
  }

  virtual inline bool InIsTCa(void) { return false; }

  void Init();

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType &, int);

  virtual void Print(ostream &);

  virtual void GetParameterNames(vector<string> &);
}; // class Rice08
#endif // ifndef RICE08
