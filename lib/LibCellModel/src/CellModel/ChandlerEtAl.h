/*      File: ChandlerEtAl.h
    Created by al128 (23.01.2013)
    Institute of Biomedical Engineering, KIT
 */

#ifndef CHANDLER
#define CHANDLER

#include <ChandlerParameters.h>

#define vCh(a) pCmP->P[NS_ChandlerParameters::a].value

class Chandler : public vbElphyModel<ML_CalcType> {
public:
  ChandlerParameters *pCmP;
  ML_CalcType Ca_i;
  ML_CalcType Ca_up, Ca_rel;
  ML_CalcType h, j, oi, ui, Xr, Xrhet, Xs, u, v, w;
  ML_CalcType Na_i, K_i;
  ML_CalcType m, oa, ua, d, f, f_Ca, dT, fT, a;

  // periph can't be bool type for alignment reasons
  ML_CalcType periph;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  Chandler(ChandlerParameters *pp);

  ~Chandler();

  virtual inline ML_CalcType Volume() { return 2.89529179 * 10e-13; }

  virtual inline ML_CalcType GetAmplitude() { return vCh(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.0; }

  virtual inline ML_CalcType GetVm() { return vCh(VT_Init_Vm); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return vCh(VT_Ca_o); }

  virtual inline ML_CalcType GetNao() { return vCh(VT_Na_o); }

  virtual inline ML_CalcType GetKo() { return vCh(VT_K_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &Ca_i; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                           int euler);

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class Chandler
#endif // ifndef CHANDLER
