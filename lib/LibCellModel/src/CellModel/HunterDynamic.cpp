/**@file HunterDynamic.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <HunterDynamic.h>

HunterDynamic::HunterDynamic(HunterDynamicParameters *pp) {
  hdp = pp;
  Init();
}

void HunterDynamic::Init() {
  TCa = CELLMODEL_PARAMVALUE(VT_TCaa);
  z = CELLMODEL_PARAMVALUE(VT_z0);
}

ML_CalcType
HunterDynamic::Calc(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType &Ca,
                    int euler = 1) {
  return ForceEulerNEuler(euler, tinc / euler, stretch, Ca);
}

inline ML_CalcType
HunterDynamic::ForceEulerNEuler(int r, ML_CalcType tinc, ML_CalcType stretch, ML_CalcType &Ca) {
  stretch -= 1.0;

  double force = CELLMODEL_PARAMVALUE(VT_Fref) * (1 + CELLMODEL_PARAMVALUE(VT_beta0) * stretch) * z;
  double n = CELLMODEL_PARAMVALUE(VT_nref) * (1 + CELLMODEL_PARAMVALUE(VT_beta1) * stretch);
  double pC50 = CELLMODEL_PARAMVALUE(VT_pC50ref) * (1 + CELLMODEL_PARAMVALUE(VT_beta2) * stretch);
  double C50 = pow(10., 6. - pC50);

  ML_CalcType kTCamT =
      CELLMODEL_PARAMVALUE(VT_f) * Ca * (CELLMODEL_PARAMVALUE(VT_Ca_bmax) - TCa) - CELLMODEL_PARAMVALUE(VT_g) * (1 - force * CELLMODEL_PARAMVALUE(VT_dgamma)) * TCa;
  ML_CalcType mZ = CELLMODEL_PARAMVALUE(VT_alpha) * (pow((TCa / C50), n) * (1 - z) - z);

  z += tinc * mZ;
  TCa += tinc * kTCamT;
  Ca -= tinc * kTCamT * CELLMODEL_PARAMVALUE(VT_Ca_bmax);
  return force;
}

void HunterDynamic::steadyState(ML_CalcType Ca, ML_CalcType stretch) {
  stretch -= 1.0;
  ML_CalcType n = CELLMODEL_PARAMVALUE(VT_nref) * (1 + CELLMODEL_PARAMVALUE(VT_beta1) * stretch);
  ML_CalcType pC50 = CELLMODEL_PARAMVALUE(VT_pC50ref) * (1 + CELLMODEL_PARAMVALUE(VT_beta2) * stretch);
  ML_CalcType C50 = pow(10., 6. - pC50);
  ML_CalcType z_ss = pow(Ca, n) / (pow(Ca, n) + pow(C50, n));
  ML_CalcType force = CELLMODEL_PARAMVALUE(VT_Fref) * (1 + CELLMODEL_PARAMVALUE(VT_beta0) * stretch) * z_ss;

  cout << Ca << ' ' << force << endl;
}

void HunterDynamic::Print(ostream &tempstr) {
  tempstr << TCa << ' ' << z << ' ';
}

void HunterDynamic::GetParameterNames(vector<string> &getpara) {
  getpara.push_back("TCa");
  getpara.push_back("z");
}
