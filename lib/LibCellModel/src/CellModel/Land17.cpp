// created by Robin Moss

// based on the available Matlab code from cemrg.co.uk
// 7.6.17

#include <Land17.h>


Land17::Land17(Land17Parameters *pp) {
  L17p = pp;
  Init();
}

void Land17::Init() {
#if KADEBUG
  std::cerr << "#initializing Class: Land17 ... " << endl;
#endif  // if KADEBUG
  XS = (CELLMODEL_PARAMVALUE(VT_XS_init));
  XW = (CELLMODEL_PARAMVALUE(VT_XW_init));
  CaTRPN = (CELLMODEL_PARAMVALUE(VT_CaTRPN_init));
  B = (CELLMODEL_PARAMVALUE(VT_B_init));
  ZETAS = (CELLMODEL_PARAMVALUE(VT_ZETAS_init));
  ZETAW = (CELLMODEL_PARAMVALUE(VT_ZETAW_init));
  Cd = (CELLMODEL_PARAMVALUE(VT_Cd_init));
}

ML_CalcType Land17::Calc(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType &Ca_i,
                         int euler = 1) {
  double HT = tinc * 1000;    // sec to msec
  double VEL = velocity / 1000; // 1/s to 1/ms

  XS = std::max(0.0, XS);
  XW = std::max(0.0, XW);
  CaTRPN = std::max(0.0, CaTRPN);


  // -------------------------------------------------------------------------------
  // XB model
  double lambda = std::min(1.2, stretch);
  double Lfac = std::max(0.0, 1 + CELLMODEL_PARAMVALUE(VT_beta_0) * (lambda + std::min(0.87, lambda) - 1.87));

  // unattached available xb = all - tm blocked - already prepowerstroke - already post-poststroke - no overlap
  double XU = (1 - B) - XW - XS;

  double xb_ws = (CELLMODEL_PARAMVALUE(VT_k_ws)) * XW;
  double xb_uw = (CELLMODEL_PARAMVALUE(VT_k_uw)) * XU;
  double xb_wu = (CELLMODEL_PARAMVALUE(VT_k_wu)) * XW;
  double xb_su = (CELLMODEL_PARAMVALUE(VT_k_su)) * XS;

  double gamma_rate = (CELLMODEL_PARAMVALUE(VT_gamma)) * std::max(((ZETAS > 0) ? 1 : 0) * ZETAS,
                                               ((ZETAS < -1) ? 1 : 0) * (-ZETAS - 1));

  double xb_su_gamma = gamma_rate * XS;

  double gamma_rate_w = (CELLMODEL_PARAMVALUE(VT_gamma_wu)) * abs(ZETAW);  // weak xbs don't like being strained
  double xb_wu_gamma = gamma_rate_w * XW;


  double ca50 = (CELLMODEL_PARAMVALUE(VT_ca50)) + (CELLMODEL_PARAMVALUE(VT_beta_1)) * std::min(0.2, lambda - 1);


  double XSSS = (CELLMODEL_PARAMVALUE(VT_dr)) * 0.5;
  double XWSS = (1 - (CELLMODEL_PARAMVALUE(VT_dr))) * (CELLMODEL_PARAMVALUE(VT_wfrac)) * 0.5;
  double ktm_block =
      (CELLMODEL_PARAMVALUE(VT_ktm_unblock)) * pow((CELLMODEL_PARAMVALUE(VT_perm50)), (CELLMODEL_PARAMVALUE(VT_nperm))) * 0.5 / (0.5 - XSSS - XWSS);


  // -------------------------------------------------------------------------------
  // Passive model

  double C = lambda - 1;


  double eta;
  if ((C - Cd) < 0) {
    eta = (CELLMODEL_PARAMVALUE(VT_eta_s));
  } else {
    eta = (CELLMODEL_PARAMVALUE(VT_eta_l));
  }


  /// Calculating rates

  // -------------------------------------------------------------------------------
  // XB model
  XS += HT * (xb_ws - xb_su - xb_su_gamma);
  XW += HT * (xb_uw - xb_wu - xb_ws - xb_wu_gamma);
  CaTRPN += HT * (CELLMODEL_PARAMVALUE(VT_koff) *
                  (pow((Ca_i / ca50), (CELLMODEL_PARAMVALUE(VT_TRPN_n))) * (1 - CaTRPN) - CaTRPN));  // untouched
  B += HT * (ktm_block * std::min(100.0, pow(CaTRPN, -((CELLMODEL_PARAMVALUE(VT_nperm)) / 2)))
             * XU - CELLMODEL_PARAMVALUE(VT_ktm_unblock) * pow(CaTRPN, CELLMODEL_PARAMVALUE(VT_nperm) / 2) * B);


  // -------------------------------------------------------------------------------
  // Velocity dependence -- assumes distortion resets on W->S
  ZETAS += HT * (CELLMODEL_PARAMVALUE(VT_A) * VEL - CELLMODEL_PARAMVALUE(VT_cds) * ZETAS);  // - gamma_rate * ZETAS;
  ZETAW += HT * (CELLMODEL_PARAMVALUE(VT_A) * VEL - CELLMODEL_PARAMVALUE(VT_cdw) * ZETAW);  // - gamma_rate_w * ZETAW;


  // -------------------------------------------------------------------------------
  // Passive model
  double dCd_dt = CELLMODEL_PARAMVALUE(VT_par_k) * (C - Cd) / eta;
  Cd += HT * dCd_dt; // F2 = Fd
  // FIXME should we subtract dCaTRPN from Ca_i?
  Ca_i += 0; // -CaTRPN;


  // -------------------------------------------------------------------------------
  // Active and Total Force

  double Fd = eta * dCd_dt;
  double F1 = exp((CELLMODEL_PARAMVALUE(VT_b)) * C) - 1;
  Tp = CELLMODEL_PARAMVALUE(VT_a) * (F1 + Fd);

  Ta = Lfac * ((CELLMODEL_PARAMVALUE(VT_Tref)) / (CELLMODEL_PARAMVALUE(VT_dr))) * ((ZETAS + 1) * XS + (ZETAW) * XW);
  // leave passive_contribution on 0, otherwise there is a slight offset on T
  double T = Ta + (CELLMODEL_PARAMVALUE(VT_passive_contribution)) * Tp;
  /*
  cout << "XS: " << XS <<
  " XW: " << XW <<
  " CaTRPN: "<< CaTRPN <<
  " B: " << B <<
  " Zeta_S:" << ZETAS <<
  " Zeta_W:" << ZETAW <<
  " Calcium: " << Ca_i <<
  " vel: " << VEL <<
  " ca50: " << ca50 << endl;*/

  // T is returned in kPa
  return T;
}  // Land17::Calc

void Land17::Print(ostream &tempstr) {
  tempstr << ' ' << XS << ' ' << XW << ' ' << CaTRPN
          << ' ' << B << ' ' << ZETAS << ' ' << ZETAW << ' ' << Cd << ' ' << Ta << ' ' << Tp;
}

void Land17::GetParameterNames(vector<string> &getpara) {
  const int numpara = 9;
  const string ParaNames[numpara] = {"XS", "XW", "CaTRPN", "B", "ZETAS", "ZETAW", "Cd", "Ta", "Tp"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
