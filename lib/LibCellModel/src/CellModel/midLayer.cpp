/*
 *  midLayer.cpp
 *  CellModelSO
 *
 *  Created by Daniel Weiss on 27.01.05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#ifdef osMac_Vec

#include <midLayer.h>

template class SO_Object<ML_CalcType>;
template class SO_ionicCurrent<ML_CalcType>;
template class SO_CaConc_Handling<ML_CalcType>;
template class SO_NaConc_Handling<ML_CalcType>;
template class SO_KConc_Handling<ML_CalcType>;

SharedObjectType::SharedObjectType(string fileName) {
#if KADEBUG
  cerr<<"SharedObjectType(): loading shLib from "<<fileName.c_str()<<endl;
#endif // if KADEBUG
  handle              = LoadLibrary((char*)fileName.c_str());
  GetSharedObjectType = (so_Type)LoadSymbol(handle, "SOType");
}

#endif // ifdef osMac_Vec
