/*
 *  Hund.cpp
 *
 *
 *  Created by Christian Rombach on 04.08.08.
 *  Last modified Christian Rombach (15.09.09)
 *  Institute of Biomedical Engineering\n
 *  Universitaet Karlsruhe (TH)\n
 *  http://www.ibt.uni-karlsruhe.de\n
 *
 */

#include "Hund.h"

Hund::Hund(HundParameters *pp) {
  ptHP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(ptHP, NS_HundParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

Hund::~Hund() {}

#ifdef HETERO

inline bool Hund::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  //    cerr<<"add value in "<<PS<<endl;
  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool Hund::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int Hund::GetSize(void) {
  return sizeof(Hund) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(HundParameters *)
         #ifdef HETERO
         - sizeof(ParameterSwitch *)
#endif // ifdef HETERO
      ;
}

inline unsigned char Hund::getSpeed(ML_CalcType adVm) {
  return (unsigned char) 5;
}

void Hund::Init() {
#if KADEBUG
  cerr << "#initializing Class: Hund ... " << endl;
#endif // if KADEBUG
  Na_i = (CELLMODEL_PARAMVALUE(VT_Na_i_init));
  K_i = (CELLMODEL_PARAMVALUE(VT_K_i_init));
  Cl_i = (CELLMODEL_PARAMVALUE(VT_Cl_i_init));
  Ca_ss = (CELLMODEL_PARAMVALUE(VT_Ca_ss_init));
  Ca_nsr = (CELLMODEL_PARAMVALUE(VT_Ca_nsr_init));
  Ca_jsr = (CELLMODEL_PARAMVALUE(VT_Ca_jsr_init));
  Ca_i = (CELLMODEL_PARAMVALUE(VT_Ca_i_init));
  H = (CELLMODEL_PARAMVALUE(VT_H_init));
  m = (CELLMODEL_PARAMVALUE(VT_m_init));
  J = (CELLMODEL_PARAMVALUE(VT_J_init));
  d = (CELLMODEL_PARAMVALUE(VT_d_init));
  dp = (CELLMODEL_PARAMVALUE(VT_dp_init));
  f = (CELLMODEL_PARAMVALUE(VT_f_init));
  f2 = (CELLMODEL_PARAMVALUE(VT_f2_init));
  fca = (CELLMODEL_PARAMVALUE(VT_fca_init));
  fca2 = (CELLMODEL_PARAMVALUE(VT_fca2_init));
  xs1 = (CELLMODEL_PARAMVALUE(VT_xs1_init));
  xs2 = (CELLMODEL_PARAMVALUE(VT_xs2_init));
  xr = (CELLMODEL_PARAMVALUE(VT_xr_init));
  ydv = (CELLMODEL_PARAMVALUE(VT_ydv_init));
  ydv2 = (CELLMODEL_PARAMVALUE(VT_ydv2_init));
  zdv = (CELLMODEL_PARAMVALUE(VT_zdv_init));
  AA = (CELLMODEL_PARAMVALUE(VT_AA_init));
  mL = (CELLMODEL_PARAMVALUE(VT_mL_init));
  hL = (CELLMODEL_PARAMVALUE(VT_hL_init));
  ro = (CELLMODEL_PARAMVALUE(VT_ro_init));
  ri = (CELLMODEL_PARAMVALUE(VT_ri_init));
  CaMKtrap = (CELLMODEL_PARAMVALUE(VT_CaMKtrap_init));
} // Hund::Init

ML_CalcType
Hund::Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch, int euler) {
  ML_CalcType svolt = V * 1000;  // membrane voltage in mV
  ML_CalcType HT = tinc * 1000; // timestep in ms
  const int Vi = (int) (DivisionTab * (RangeTabhalf + svolt) + .5); // array position

  const ML_CalcType R = ptHP->r[Vi];
  const ML_CalcType RV = ptHP->rv[Vi];

  // needed to compute currents
  const ML_CalcType ENa = log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_i) / (CELLMODEL_PARAMVALUE(VT_FONRT));
  const ML_CalcType EK = log((CELLMODEL_PARAMVALUE(VT_K_o)) / K_i) / (CELLMODEL_PARAMVALUE(VT_FONRT));
  const ML_CalcType EKs =
      (log(((CELLMODEL_PARAMVALUE(VT_K_o)) + (CELLMODEL_PARAMVALUE(VT_prnak)) * (CELLMODEL_PARAMVALUE(VT_Na_o))) / (K_i + (CELLMODEL_PARAMVALUE(VT_prnak)) * Na_i))) /
      (CELLMODEL_PARAMVALUE(VT_FONRT));
  const ML_CalcType ECl = -log((CELLMODEL_PARAMVALUE(VT_Cl_o)) / Cl_i) / (CELLMODEL_PARAMVALUE(VT_FONRT));
  const ML_CalcType gNa = (CELLMODEL_PARAMVALUE(VT_GNa)) * m * m * m * H * J;
  const ML_CalcType ibarca =
      ((CELLMODEL_PARAMVALUE(VT_pca)) * 4. * (svolt - 15.) * (CELLMODEL_PARAMVALUE(VT_F)) * (CELLMODEL_PARAMVALUE(VT_FONRT)) *
       ((CELLMODEL_PARAMVALUE(VT_gacai)) * Ca_ss * exp(2. * (svolt - 15.) * (CELLMODEL_PARAMVALUE(VT_FONRT))) -
        (CELLMODEL_PARAMVALUE(VT_gacao)) * (CELLMODEL_PARAMVALUE(VT_Ca_o)))) /
      (exp(2. * (svolt - 15.) * (CELLMODEL_PARAMVALUE(VT_FONRT))) - 1.);
  const ML_CalcType ak1 = 1.02 / (1. + exp(0.2385 * ((svolt - EK) - 59.215)));
  const ML_CalcType bk1 = (0.49124 * exp(0.08032 * ((svolt - EK) + 5.476)) +
                           1. * exp(0.06175 * ((svolt - EK) - 594.31))) /
                          (1. + exp(-0.5143 * ((svolt - EK) + 4.753)));
  const ML_CalcType gkr = 0.0138542 * sqrt((CELLMODEL_PARAMVALUE(VT_K_o)) / 5.4);
  const ML_CalcType gks = 0.0248975 * (1. + 0.6 / (1. + pow((3.8e-5) / Ca_i, 1.4)));
  const ML_CalcType fnak = 1. /
                           (1. + 0.1245 * exp(-0.1 * svolt * (CELLMODEL_PARAMVALUE(VT_FONRT))) +
                            0.0365 * (CELLMODEL_PARAMVALUE(VT_sigma)) * exp(-svolt * (CELLMODEL_PARAMVALUE(VT_FONRT))));
  const ML_CalcType ca_i_NaCa = 1.5 * Ca_i;
  const ML_CalcType allo = 1. / (1. + ((CELLMODEL_PARAMVALUE(VT_KmCa)) / ca_i_NaCa) * ((CELLMODEL_PARAMVALUE(VT_KmCa)) / ca_i_NaCa));
  const ML_CalcType num =
      (Na_i * Na_i * Na_i * (CELLMODEL_PARAMVALUE(VT_Ca_o)) * exp((CELLMODEL_PARAMVALUE(VT_eta)) * svolt * (CELLMODEL_PARAMVALUE(VT_FONRT))) -
       (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) * ca_i_NaCa *
       exp(((CELLMODEL_PARAMVALUE(VT_eta)) - 1.) * svolt * (CELLMODEL_PARAMVALUE(VT_FONRT))));
  const ML_CalcType denom1 = (1. + (CELLMODEL_PARAMVALUE(VT_ksat)) * exp(((CELLMODEL_PARAMVALUE(VT_eta)) - 1.) * svolt * (CELLMODEL_PARAMVALUE(VT_FONRT))));
  const ML_CalcType denom2 =
      ((CELLMODEL_PARAMVALUE(VT_KmCao)) * Na_i * Na_i * Na_i +
       (CELLMODEL_PARAMVALUE(VT_KmNao)) * (CELLMODEL_PARAMVALUE(VT_KmNao)) * (CELLMODEL_PARAMVALUE(VT_KmNao)) * ca_i_NaCa + (CELLMODEL_PARAMVALUE(VT_KmNai)) * (CELLMODEL_PARAMVALUE(VT_KmNai)) *
                                                                   (CELLMODEL_PARAMVALUE(VT_KmNai)) * (CELLMODEL_PARAMVALUE(VT_Ca_o)) *
                                                                   (1. +
                                                                    ca_i_NaCa / (CELLMODEL_PARAMVALUE(VT_KmCai))));
  const ML_CalcType denom3 =
      ((CELLMODEL_PARAMVALUE(VT_KmCai)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) *
       (1. + (Na_i / (CELLMODEL_PARAMVALUE(VT_KmNai))) * (Na_i / (CELLMODEL_PARAMVALUE(VT_KmNai))) * (Na_i / (CELLMODEL_PARAMVALUE(VT_KmNai)))) +
       Na_i * Na_i * Na_i * (CELLMODEL_PARAMVALUE(VT_Ca_o)) + (CELLMODEL_PARAMVALUE(VT_Na_o)) *
                                           (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) * ca_i_NaCa);
  const ML_CalcType Ito2_max =
      ((CELLMODEL_PARAMVALUE(VT_PCl)) * svolt * (CELLMODEL_PARAMVALUE(VT_F)) * (CELLMODEL_PARAMVALUE(VT_FONRT)) *
       (Cl_i - (CELLMODEL_PARAMVALUE(VT_Cl_o)) * exp(svolt * (CELLMODEL_PARAMVALUE(VT_FONRT))))) /
      (1. - exp(svolt * (CELLMODEL_PARAMVALUE(VT_FONRT))));

  // compute skalar currents
  const ML_CalcType INa = gNa * (svolt - ENa);
  ML_CalcType ICaL = d * f * f2 * fca * fca2 * ibarca;

  if ((CELLMODEL_PARAMVALUE(VT_tissue)) == 0)
    ICaL = pow(d, dp) * f * f2 * fca * fca2 * ibarca;

  // const ML_CalcType ICaL=pow(d,dp)*f*f2*fca*fca2*ibarca;
  const ML_CalcType IK1 = ((0.5 * sqrt((CELLMODEL_PARAMVALUE(VT_K_o)) / 5.4) * ak1) / (ak1 + bk1)) * (svolt - EK);
  const ML_CalcType IKr = gkr * xr * R * (svolt - EK);
  const ML_CalcType IKs = gks * xs1 * xs2 * (svolt - EKs);
  const ML_CalcType Ito = (CELLMODEL_PARAMVALUE(VT_gitodv)) * ydv * ydv * ydv * ydv2 * zdv * RV * (svolt - EK);
  const ML_CalcType INaK =
      ((((CELLMODEL_PARAMVALUE(VT_ibarnak)) * fnak * 1.) / (1. + (((CELLMODEL_PARAMVALUE(VT_kmnai)) / Na_i) * ((CELLMODEL_PARAMVALUE(VT_kmnai)) / Na_i)))) *
       (CELLMODEL_PARAMVALUE(VT_K_o))) /
      ((CELLMODEL_PARAMVALUE(VT_K_o)) + (CELLMODEL_PARAMVALUE(VT_kmko)));
  const ML_CalcType INaCa = ((CELLMODEL_PARAMVALUE(VT_NCXmax)) * allo * num) / (denom1 * (denom2 + denom3));
  const ML_CalcType IKp = (0.00276 * (svolt - EK)) / (1. + exp((7.488 - svolt) / 5.98));
  const ML_CalcType IpCa = ((CELLMODEL_PARAMVALUE(VT_ibarpca)) * Ca_i) / ((CELLMODEL_PARAMVALUE(VT_kmpca)) + Ca_i);
  const ML_CalcType ICab =
      ((1.995084e-7) * 4. * svolt * (CELLMODEL_PARAMVALUE(VT_F)) * (CELLMODEL_PARAMVALUE(VT_FONRT)) *
       (Ca_i * exp(2. * svolt * (CELLMODEL_PARAMVALUE(VT_FONRT))) - 0.341 * (CELLMODEL_PARAMVALUE(VT_Ca_o)))) /
      (exp(2 * svolt * (CELLMODEL_PARAMVALUE(VT_FONRT))) - 1.);
  const ML_CalcType Ito2 = Ito2_max * AA;
  const ML_CalcType IClb = (CELLMODEL_PARAMVALUE(VT_GClb)) * (svolt - ECl);
  const ML_CalcType INal = (CELLMODEL_PARAMVALUE(VT_GNaL)) * mL * mL * mL * hL * (svolt - ENa);
  const ML_CalcType caiont = ((ICaL + ICab + IpCa) - 2. * INaCa);
  const ML_CalcType naiont = (INa + 3. * INaCa + 3. * INaK + INal);
  const ML_CalcType kiont = (((IKr + IKs + IK1 + IKp) - 2. * INaK) + Ito - 0.5 * i_external);
  const ML_CalcType clont = (IClb + Ito2 - 0.5 * i_external);
  const ML_CalcType I_tot = (naiont + kiont + caiont + clont);

  // update concentrations
  const ML_CalcType fcass = (0.3 / (1. - ICaL / 0.05) + 0.55 / (1. + Ca_ss / 0.003) + 0.15);
  const ML_CalcType fca2ss = 1. / (1. - ICaL / 0.01);
  const ML_CalcType CaMKbound = ((CELLMODEL_PARAMVALUE(VT_CaMK0)) * (1 - CaMKtrap)) / (1. + (CELLMODEL_PARAMVALUE(VT_Km)) / Ca_ss);
  const ML_CalcType CaMKactive = (CaMKbound + CaMKtrap);
  const ML_CalcType taufca = ((10. * CaMKactive) / (0.15 + CaMKactive) + 1. / (1. + Ca_ss / 0.003) +
                              0.5);
  const ML_CalcType taufca2 = (300. / (1. + exp((-ICaL - 0.175) / 0.04)) + 125.);
  const ML_CalcType AAss = 1. / (1. + (CELLMODEL_PARAMVALUE(VT_Kmto2)) / Ca_ss);
  ML_CalcType vg = 1.;
  if (CELLMODEL_PARAMVALUE(VT_tissue) == 0)
    vg = 1. / (1. + exp((ibarca + 13.) / 5.));

  // const  ML_CalcType vg=1./(1.+exp((ibarca+13.)/5.));
  const ML_CalcType Grel = 3000. * vg;
  const ML_CalcType cafac = 1. / (1. + exp((ICaL + 0.05) / 0.015));
  const ML_CalcType dro_inf =
      (pow(Ca_jsr, 1.9)) / (pow(Ca_jsr, 1.9) + pow(((49.28 * Ca_ss) / (Ca_ss + 0.0028)), 1.9));
  const ML_CalcType dtau_rel = ((CELLMODEL_PARAMVALUE(VT_dtau_rel_max)) * CaMKactive) / ((CELLMODEL_PARAMVALUE(VT_KmCaMK)) + CaMKactive);
  const ML_CalcType ross = dro_inf / ((1. / ICaL) * (1. / ICaL) + 1.);
  const ML_CalcType riss = 1. / (1. + exp(((Ca_ss - (4e-4)) + 0.002 * cafac) / (2.5e-5)));
  const ML_CalcType tauri = (3. + dtau_rel + (350. - dtau_rel) / (1. + exp(((Ca_ss - 0.003) +
                                                                            0.003 * cafac) /
                                                                           (2e-4))));
  const ML_CalcType irelcicr = Grel * ro * ri * (Ca_jsr - Ca_ss);
  const ML_CalcType dCaMKtrap = (0.05 * CaMKactive * (CaMKactive - CaMKtrap) - (6.8e-4) * CaMKtrap);
  CaMKtrap += dCaMKtrap * HT;
  const ML_CalcType dKmPLB = ((CELLMODEL_PARAMVALUE(VT_dKmPLBmax)) * CaMKactive) / ((CELLMODEL_PARAMVALUE(VT_KmCaMK)) + CaMKactive);
  const ML_CalcType dJup = ((CELLMODEL_PARAMVALUE(VT_dJupmax)) * CaMKactive) / ((CELLMODEL_PARAMVALUE(VT_KmCaMK)) + CaMKactive);
  const ML_CalcType iup = ((dJup + 1.) * (CELLMODEL_PARAMVALUE(VT_iupmax)) * Ca_i) / ((Ca_i + (CELLMODEL_PARAMVALUE(VT_Kmup))) - dKmPLB);
  const ML_CalcType ileak = ((CELLMODEL_PARAMVALUE(VT_iupmax)) * Ca_nsr) / (CELLMODEL_PARAMVALUE(VT_nsrmax));
  const ML_CalcType idiff = (Ca_ss - Ca_i) / 0.2;
  const ML_CalcType itr = (Ca_nsr - Ca_jsr) / 120.;
  const ML_CalcType CTNaCl =
      ((CELLMODEL_PARAMVALUE(VT_CTNaClmax)) * (ENa - ECl) * (ENa - ECl) * (ENa - ECl) * (ENa - ECl)) /
      ((ENa - ECl) * (ENa - ECl) * (ENa - ECl) * (ENa - ECl) +
       87.8251 * 87.8251 * 87.8251 * 87.8251);
  const ML_CalcType dNa_i = ((-naiont * (CELLMODEL_PARAMVALUE(VT_AF))) / (CELLMODEL_PARAMVALUE(VT_vmyo)) + CTNaCl);
  Na_i += dNa_i * HT;
  const ML_CalcType CTKCl = ((CELLMODEL_PARAMVALUE(VT_CTKClmax)) * (EK - ECl)) / ((EK - ECl) + 87.8251);
  const ML_CalcType dK_i = ((-kiont * (CELLMODEL_PARAMVALUE(VT_AF))) / (CELLMODEL_PARAMVALUE(VT_vmyo)) + CTKCl);
  K_i += dK_i * HT;
  const ML_CalcType dCl_i = ((clont * (CELLMODEL_PARAMVALUE(VT_AF))) / (CELLMODEL_PARAMVALUE(VT_vmyo)) + CTNaCl + CTKCl);
  Cl_i += dCl_i * HT;
  const ML_CalcType bcsqn = 1. / (1. + ((CELLMODEL_PARAMVALUE(VT_kmcsqn)) * (CELLMODEL_PARAMVALUE(VT_csqnbar))) /
                                       ((Ca_jsr + (CELLMODEL_PARAMVALUE(VT_kmcsqn))) * (Ca_jsr + (CELLMODEL_PARAMVALUE(VT_kmcsqn)))));
  const ML_CalcType bmyo = 1. /
                           (1. + ((CELLMODEL_PARAMVALUE(VT_cbar)) * (CELLMODEL_PARAMVALUE(VT_kmc))) /
                                 ((Ca_i + (CELLMODEL_PARAMVALUE(VT_kmc))) * (Ca_i + (CELLMODEL_PARAMVALUE(VT_kmc)))) +
                            ((CELLMODEL_PARAMVALUE(VT_kmt)) * (CELLMODEL_PARAMVALUE(VT_tbar))) /
                            ((Ca_i + (CELLMODEL_PARAMVALUE(VT_kmt))) * (Ca_i + (CELLMODEL_PARAMVALUE(VT_kmt)))));
  const ML_CalcType bss = 1. /
                          (1. + ((CELLMODEL_PARAMVALUE(VT_BSRmax)) * (CELLMODEL_PARAMVALUE(VT_KmBSR))) /
                                (((CELLMODEL_PARAMVALUE(VT_KmBSR)) + Ca_ss) * ((CELLMODEL_PARAMVALUE(VT_KmBSR)) + Ca_ss)) +
                           ((CELLMODEL_PARAMVALUE(VT_BSLmax)) * (CELLMODEL_PARAMVALUE(VT_KmBSL))) /
                           (((CELLMODEL_PARAMVALUE(VT_KmBSL)) + Ca_ss) * ((CELLMODEL_PARAMVALUE(VT_KmBSL)) + Ca_ss)));
  const ML_CalcType dCa_i = bmyo *
                            (((-((ICab + IpCa) - 2 * INaCa) * (CELLMODEL_PARAMVALUE(VT_AF))) / ((CELLMODEL_PARAMVALUE(VT_vmyo)) * 2)) +
                             (((ileak - iup) * (CELLMODEL_PARAMVALUE(VT_vnsr))) / (CELLMODEL_PARAMVALUE(VT_vmyo))) +
                             ((idiff * (CELLMODEL_PARAMVALUE(VT_vss))) / (CELLMODEL_PARAMVALUE(VT_vmyo))));
  Ca_i += dCa_i * HT;
  const ML_CalcType dCa_ss = bss * ((-ICaL * (CELLMODEL_PARAMVALUE(VT_AF))) / ((CELLMODEL_PARAMVALUE(VT_vss)) * 2) +
                                    (irelcicr * (CELLMODEL_PARAMVALUE(VT_vjsr))) / (CELLMODEL_PARAMVALUE(VT_vss)) - idiff);
  Ca_ss += dCa_ss * HT;
  const ML_CalcType dCa_nsr = ((iup - (itr * (CELLMODEL_PARAMVALUE(VT_vjsr))) / (CELLMODEL_PARAMVALUE(VT_vnsr))) - ileak);
  Ca_nsr += dCa_nsr * HT;
  const ML_CalcType dCa_jsr = bcsqn * (itr - irelcicr);
  Ca_jsr += dCa_jsr * HT;

  // update gates
  const ML_CalcType HSS = ptHP->hss[Vi];
  H = HSS + (H - HSS) * ptHP->eth[Vi];

  const ML_CalcType MSS = ptHP->mss[Vi];
  m = MSS + (m - MSS) * ptHP->etm[Vi];

  const ML_CalcType JSS = ptHP->jss[Vi];
  J = JSS + (J - JSS) * ptHP->etj[Vi];

  const ML_CalcType DSS = ptHP->dss[Vi];
  d = DSS + (d - DSS) * ptHP->etd[Vi];

  const ML_CalcType DPSS = ptHP->dpss[Vi];
  dp = DPSS + (dp - DPSS) * ptHP->et10;

  const ML_CalcType FSS = ptHP->fss[Vi];
  f = FSS + (f - FSS) * ptHP->etf[Vi];

  const ML_CalcType F2SS = ptHP->f2ss[Vi];
  f2 = F2SS + (f2 - F2SS) * ptHP->etf2[Vi];

  fca = fcass - (fcass - fca) * exp(-HT / taufca);
  fca2 = fca2ss - (fca2ss - fca2) * exp(-HT / taufca2);

  const ML_CalcType XRSS = ptHP->xrss[Vi];
  xr = XRSS + (xr - XRSS) * ptHP->etxr[Vi];

  const ML_CalcType XSS = ptHP->xss[Vi];
  xs1 = XSS + (xs1 - XSS) * ptHP->etxs[Vi];
  xs2 = XSS + (xs2 - XSS) * ptHP->etxs2[Vi];

  const ML_CalcType YSS = ptHP->yss[Vi];
  ydv = YSS + (ydv - YSS) * ptHP->ety[Vi];

  const ML_CalcType Y2SS = ptHP->y2ss[Vi];
  ydv2 = Y2SS + (ydv2 - Y2SS) * ptHP->ety2[Vi];

  const ML_CalcType ZSS = ptHP->zss[Vi];
  zdv = ZSS + (zdv - ZSS) * ptHP->etz[Vi];

  AA = AAss - (AAss - AA) * ptHP->et1;

  const ML_CalcType MLSS = ptHP->mLss[Vi];
  mL = MLSS + (mL - MLSS) * ptHP->etmL[Vi];

  const ML_CalcType HLSS = ptHP->hLss[Vi];
  hL = HLSS + (hL - HLSS) * ptHP->et600;

  ro = ross - (ross - ro) * ptHP->et3;

  ri = riss - (riss - ri) * exp(-HT / tauri);

  return tinc * (-I_tot);
} // Hund::Calc

void Hund::Print(ostream &tempstr, double tArg, ML_CalcType V) {
  tempstr << tArg << ' ' << V << ' '
          << Na_i << ' ' << K_i << ' ' << Cl_i << ' ' << Ca_ss << ' ' << Ca_nsr << ' ' << Ca_jsr
          << ' ' << Ca_i << ' '
          << H << ' ' << m << ' ' << J << ' ' << d << ' ' << dp << ' ' << f << ' ' << f2 << ' '
          << fca << ' ' << fca2 << ' '
          << xs1 << ' ' << xs2 << ' ' << xr << ' ' << ydv << ' ' << ydv2 << ' ' << zdv << ' '
          << AA << ' ' << mL << ' ' << hL << ' ' << ro << ' ' << ri << ' ';
}

void Hund::LongPrint(ostream &tempstr, double tArg, ML_CalcType V) {
  Print(tempstr, tArg, V);
  const ML_CalcType svolt = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + svolt) + .5);

  const ML_CalcType ENa = (log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_i) / (CELLMODEL_PARAMVALUE(VT_FONRT)));
  const ML_CalcType EK = (log((CELLMODEL_PARAMVALUE(VT_K_o)) / K_i) / (CELLMODEL_PARAMVALUE(VT_FONRT)));
  const ML_CalcType EKs =
      (log(((CELLMODEL_PARAMVALUE(VT_K_o)) + ((CELLMODEL_PARAMVALUE(VT_prnak)) * (CELLMODEL_PARAMVALUE(VT_Na_o)))) / (K_i + ((CELLMODEL_PARAMVALUE(VT_prnak)) * Na_i))) /
       (CELLMODEL_PARAMVALUE(VT_FONRT)));
  const ML_CalcType ECl = (((-1.) * log((CELLMODEL_PARAMVALUE(VT_Cl_o)) / Cl_i)) / (CELLMODEL_PARAMVALUE(VT_FONRT)));
  const ML_CalcType gNa = ((((((CELLMODEL_PARAMVALUE(VT_GNa)) * m) * m) * m) * H) * J);
  const ML_CalcType INa = (gNa * (svolt - ENa));
  const ML_CalcType gks = ((.0248975) * (1 + (.6 / (1 + pow(((3.8e-5) / Ca_i), 1.4)))));
  const ML_CalcType IKs = (((gks * xs1) * xs2) * (svolt - EKs));
  const ML_CalcType ak1 = ((1.02) / (1 + exp((.2385) * ((svolt - EK) - (59.215)))));
  const ML_CalcType bk1 =
      ((((.49124) * exp((.08032) * ((svolt - EK) + (5.476)))) +
        ((1.) * exp((.06175) * ((svolt - EK) - (594.31))))) /
       (1. + exp(((-1.) * (.5143)) * ((svolt - EK) + (4.753)))));
  const ML_CalcType gkr = ((.0138542) * sqrt((CELLMODEL_PARAMVALUE(VT_K_o)) / (5.4)));
  const ML_CalcType IKr = (((gkr * xr) * (ptHP->r[Vi])) * (svolt - EK));
  const ML_CalcType IK1 = (((((.5) * sqrt((CELLMODEL_PARAMVALUE(VT_K_o)) / (5.4))) * ak1) / (ak1 + bk1)) *
                           (svolt - EK));
  const ML_CalcType Ito = (
      (((((CELLMODEL_PARAMVALUE(VT_gitodv)) * (ydv * ydv * ydv)) * ydv2) * zdv) * (ptHP->rv[Vi])) * (svolt - EK));
  const ML_CalcType fnak =
      (1. / ((1 + (.1245 * exp((((-1.) * .1) * svolt) * (CELLMODEL_PARAMVALUE(VT_FONRT))))) +
             ((.0365 * (CELLMODEL_PARAMVALUE(VT_sigma))) * exp(((-1) * svolt) * (CELLMODEL_PARAMVALUE(VT_FONRT))))));
  const ML_CalcType ca_i_NaCa = (1.5 * Ca_i);
  const ML_CalcType allo = (1. / (1 + (((CELLMODEL_PARAMVALUE(VT_KmCa)) / ca_i_NaCa) * ((CELLMODEL_PARAMVALUE(VT_KmCa)) / ca_i_NaCa))));
  const ML_CalcType num =
      ((((Na_i * Na_i * Na_i) * (CELLMODEL_PARAMVALUE(VT_Ca_o))) * exp(((CELLMODEL_PARAMVALUE(VT_eta)) * svolt) * (CELLMODEL_PARAMVALUE(VT_FONRT)))) -
       ((((CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o))) * ca_i_NaCa) *
        exp((((CELLMODEL_PARAMVALUE(VT_eta)) - 1.) * svolt) * (CELLMODEL_PARAMVALUE(VT_FONRT)))));
  const ML_CalcType denom1 = (1. +
                              ((CELLMODEL_PARAMVALUE(VT_ksat)) * exp((((CELLMODEL_PARAMVALUE(VT_eta)) - 1.) * svolt) * (CELLMODEL_PARAMVALUE(VT_FONRT)))));
  const ML_CalcType denom2 =
      ((((CELLMODEL_PARAMVALUE(VT_KmCao)) * (Na_i * Na_i * Na_i)) +
        (((CELLMODEL_PARAMVALUE(VT_KmNao)) * (CELLMODEL_PARAMVALUE(VT_KmNao)) * (CELLMODEL_PARAMVALUE(VT_KmNao))) * ca_i_NaCa)) +
       ((((CELLMODEL_PARAMVALUE(VT_KmNai)) * (CELLMODEL_PARAMVALUE(VT_KmNai)) * (CELLMODEL_PARAMVALUE(VT_KmNai))) * (CELLMODEL_PARAMVALUE(VT_Ca_o))) *
        (1. + (ca_i_NaCa / (CELLMODEL_PARAMVALUE(VT_KmCai))))));
  const ML_CalcType denom3 =
      (((((CELLMODEL_PARAMVALUE(VT_KmCai)) * ((CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)))) *
         (1 + ((Na_i / (CELLMODEL_PARAMVALUE(VT_KmNai))) * (Na_i / (CELLMODEL_PARAMVALUE(VT_KmNai))) * (Na_i / (CELLMODEL_PARAMVALUE(VT_KmNai)))))) +
        ((Na_i * Na_i * Na_i) * (CELLMODEL_PARAMVALUE(VT_Ca_o)))) +
       (((CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o)) * (CELLMODEL_PARAMVALUE(VT_Na_o))) * ca_i_NaCa));
  const ML_CalcType INaK =
      ((((((CELLMODEL_PARAMVALUE(VT_ibarnak)) * fnak) * 1.) /
         (1. + (((CELLMODEL_PARAMVALUE(VT_kmnai)) / Na_i) * (CELLMODEL_PARAMVALUE(VT_kmnai)) / Na_i)))) * (CELLMODEL_PARAMVALUE(VT_K_o))) /
      ((CELLMODEL_PARAMVALUE(VT_K_o)) + (CELLMODEL_PARAMVALUE(VT_kmko)));
  const ML_CalcType INaCa = ((((CELLMODEL_PARAMVALUE(VT_NCXmax)) * allo) * num) / (denom1 * (denom2 + denom3)));
  const ML_CalcType IKp = (((.00276) * (svolt - EK)) / (1 + exp(((7.488) - svolt) / (5.98))));
  const ML_CalcType IpCa = (((CELLMODEL_PARAMVALUE(VT_ibarpca)) * Ca_i) / ((CELLMODEL_PARAMVALUE(VT_kmpca)) + Ca_i));
  const ML_CalcType ICab =
      (((((((1.995084e-7) * 4.) * svolt) * (CELLMODEL_PARAMVALUE(VT_F))) * (CELLMODEL_PARAMVALUE(VT_FONRT))) *
        ((Ca_i * exp((2. * svolt) * (CELLMODEL_PARAMVALUE(VT_FONRT)))) - (.341 * (CELLMODEL_PARAMVALUE(VT_Ca_o))))) /
       (exp((2. * svolt) * (CELLMODEL_PARAMVALUE(VT_FONRT))) - 1.));
  const ML_CalcType IClb = ((CELLMODEL_PARAMVALUE(VT_GClb)) * (svolt - ECl));

  tempstr << ' ' << INa << ' ' << IK1 << ' ' << IKr << ' ' << IKs << ' ' << Ito << ' ' << INaK
          << ' ' << INaCa << ' ' << IKp
          << ' ' << IpCa << ' ' << ICab << ' ' << IClb;
} // Hund::LongPrint

void Hund::GetParameterNames(vector<string> &getpara) {
  const int numpara = 27;
  const string ParaNames[numpara] = {"Na_i", "K_i", "Cl_i", "Ca_ss", "Ca_nsr", "Ca_jsr", "Ca_i",
                                     "H", "m", "J", "d", "dp", "f", "f2", "fca", "fca2",
                                     "xs1", "xs2", "xr", "ydv", "ydv2", "zdv",
                                     "AA", "mL", "hL", "ro", "ri"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void Hund::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 11;
  const string ParaNames[numpara] =
      {"INa", "IK1", "IKr", "IKs", "Ito", "INaK", "INaCa", "IKp", "IpCa", "ICab", "IClb"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
