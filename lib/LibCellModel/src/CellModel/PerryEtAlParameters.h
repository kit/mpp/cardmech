/*      File: PerryEtAlParameters.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
        Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
        send comments to dw@ibt.uka.de  */

/*! \file PerryEtAl.h
   \brief Implementation of electrophysiological model for describing HERG channels
   from Lu et al., J Phys 2001 and RPR binding studies of M. Perry

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef PERRYETALPARAMETERS_H
#define PERRYETALPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_PerryEtAlParameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_Acap,
    VT_Vmyo,
    VT_Ko,
    VT_Ki,
    VT_RPR,
    VT_C0Kr,
    VT_C1Kr,
    VT_C2Kr,
    VT_OKr,
    VT_IKr,
    VT_C0Kr2,
    VT_C1Kr2,
    VT_C2Kr2,
    VT_OKr2,
    VT_IKr2,
    VT_GKr,
    VT_Kra0a,
    VT_Kra0z,
    VT_Krb0a,
    VT_Krb0z,
    VT_Krfa,
    VT_Krfz,
    VT_Krba,
    VT_Krbz,
    VT_Kra1a,
    VT_Kra1z,
    VT_Krb1a,
    VT_Krb1z,
    VT_Kraia,
    VT_Kraiz,
    VT_Krbia,
    VT_Krbiz,
    VT_Kracia,
    VT_Kraciz,
    VT_Krbcia,
    VT_Krbciz,
    VT_Kra1a2,
    VT_Krb1a2,
    VT_Kraia2,
    VT_Kraiz2,
    VT_Kra12,
    VT_Krb12,
    VT_Krexp12,
    VT_Amp,
    vtLast
  };
} // namespace NS_PerryEtAlParameters

using namespace NS_PerryEtAlParameters;

class PerryEtAlParameters : public vbNewElphyParameters {
public:
  PerryEtAlParameters(const char *);

  ~PerryEtAlParameters();

  void PrintParameters();

  void Calculate();

  void InitTable();

  void Init(const char *);
};

#endif // ifndef PERRYETALPARAMETERS_H
