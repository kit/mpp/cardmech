/**@file IyerMazhariWinslowKsParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef IYER_MAZHARI_WINSLOW_KS_PARAMETERS
#define IYER_MAZHARI_WINSLOW_KS_PARAMETERS

#include <ParameterLoader.h>

namespace NS_IyerMazhariWinslowKsParameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_Acap,
    VT_Vmyo,

    VT_Ko,
    VT_Ki,

    VT_C0Ks,
    VT_C1Ks,
    VT_O1Ks,
    VT_O2Ks,

    VT_GKs,

    VT_Ksa,
    VT_Ksb,
    VT_Ksg,
    VT_Ksd,
    VT_Kse,
    VT_Kso,
    VT_Amp,
    vtLast
  };
} // namespace NS_IyerMazhariWinslowKsParameters

using namespace NS_IyerMazhariWinslowKsParameters;

class IyerMazhariWinslowKsParameters : public vbNewElphyParameters {
public:
  IyerMazhariWinslowKsParameters(const char *);

  ~IyerMazhariWinslowKsParameters() {}

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&Kso-&Tx+1)*sizeof(T);};
  // virtual inline T* GetBase(void){return Tx;};
  // virtual int GetNumParameters() { return 16; };
  void Init(const char *);

  void InitTable() {}

  void Calculate();
};

#endif // ifndef IYER_MAZHARI_WINSLOW_KS_PARAMETERS
