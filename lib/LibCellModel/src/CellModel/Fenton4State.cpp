/*      File: Fenton4State.cpp   */

/*  Version 1.1 (2008-06-25) by fmw */

/*      Version 1.1 (2008-06-25)

 * Included 3 new ev-file parameters: Vol, Amp, and StimTime
 * LongPrint() now outputs J_fi, J_si and J_so
 */

#include <Fenton4State.h>

Fenton4State::Fenton4State(Fenton4StateParameters *pp) {
  pCmP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pCmP, NS_Fenton4StateParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

Fenton4State::~Fenton4State() {}

#ifdef HETERO

inline bool Fenton4State::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  PS->addDynamicParameter(EP);
  return true;
}

#else // ifdef HETERO

inline bool Fenton4State::AddHeteroValue(string desc, double val) {
  throw kaBaseException("not possible!\n");
}

#endif // ifdef HETERO

inline int Fenton4State::GetSize(void) {
  return (&J_si_tinc - &u + 1) * sizeof(ML_CalcType);
  /*return sizeof(Fenton4State)-sizeof(vbElphyModel<ML_CalcType>)-sizeof(Fenton4StateParameters *)-sizeof(bool)
   #ifdef HETERO
      -sizeof(ParameterSwitch *)
   #endif
     ;*/
}

inline unsigned char Fenton4State::getSpeed(ML_CalcType adVm) {
  return (unsigned char) (adVm < 1e-6 ? 2 : 1);
}

void Fenton4State::Init() {
#if KADEBUG
  cerr << "Fenton4State::Init neu" << endl;
#endif // if KADEBUG

  // Init values
  u = CELLMODEL_PARAMVALUE(VT_Init_u);
  v = CELLMODEL_PARAMVALUE(VT_Init_v);
  w = CELLMODEL_PARAMVALUE(VT_Init_w);
  s = CELLMODEL_PARAMVALUE(VT_Init_s);
}

ML_CalcType Fenton4State::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                               ML_CalcType stretch = 1.,
                               int euler = 2) {
  tinc *= 1000.0; // convert tinc from seconds to milliseconds

  // Array index for pre-calculated tables
  unsigned int ui = (int) ((u - uTabMin) * uTabStepSizeInv + 0.5f);

  // Stimulation current
  ML_CalcType J_stim_tinc = i_external * CELLMODEL_PARAMVALUE(VT_Cm_Vm_u_factor_inv_tinc); // / (0.018 * 85.7) * tinc;

  J_fi_tinc = v * pCmP->J_fi_expr_tinc[ui];
  J_so_tinc = pCmP->J_so_tinc[ui];
  J_si_tinc = (u < CELLMODEL_PARAMVALUE(VT_u_p)) ? 0.0 : w * s * CELLMODEL_PARAMVALUE(VT_minus_tau_si_inv_tinc);

  //    cout << "\tu: \t" << u << "\t:v \t" << v << "\tw: \t" << w << "\ts: \t" << s  << "\tJ_fi: \t" << J_fi <<
  // "\tJ_so: \t" << J_so << "\tJ_si: \t" << J_si <<endl;

  ML_CalcType total_flux_tinc = J_stim_tinc - (J_fi_tinc + J_so_tinc + J_si_tinc);

  v += u < CELLMODEL_PARAMVALUE(VT_u_m) ? (pCmP->v_inf[ui] - v) * pCmP->tau_v_minus_inv_tinc[ui] : v *
                                                                                CELLMODEL_PARAMVALUE(VT_minus_tau_v_plus_inv_tinc);
  w += u < CELLMODEL_PARAMVALUE(VT_u_p) ? (pCmP->w_inf[ui] - w) * pCmP->tau_w_minus_inv_tinc[ui] : w *
                                                                                CELLMODEL_PARAMVALUE(VT_minus_tau_w_plus_inv_tinc);

  s += ((pCmP->s_expr_tanh[ui] - s) * pCmP->tau_s_inv_tinc[ui]);
  u += total_flux_tinc;

  return 0.001 * total_flux_tinc * CELLMODEL_PARAMVALUE(VT_Vm_u_factor);
}

void Fenton4State::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << u << ' ' << v << ' ' << w << ' ' << s << ' ';
}

void Fenton4State::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << u << ' ' << v << ' ' << w << ' ' << s << ' '
          << J_fi_tinc * CELLMODEL_PARAMVALUE(VT_Vm_u_factor) / CELLMODEL_PARAMVALUE(VT_tinc) << ' ' <<
          J_si_tinc * CELLMODEL_PARAMVALUE(VT_Vm_u_factor) / CELLMODEL_PARAMVALUE(VT_tinc) << ' '
          << J_so_tinc * CELLMODEL_PARAMVALUE(VT_Vm_u_factor) / CELLMODEL_PARAMVALUE(VT_tinc) << ' ';
}

void Fenton4State::GetParameterNames(vector<string> &getpara) {
  const int numpara = 4;
  const string ParaNames[numpara] = {"u", "v", "w", "s"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void Fenton4State::GetLongParameterNames(vector<string> &getpara) {
  const int numpara = 7;
  const string ParaNames[numpara] = {"u", "v", "w", "s", "J_fi", "J_si", "J_so"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
