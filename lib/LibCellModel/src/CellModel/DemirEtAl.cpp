/**@file DemirEtAl.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <DemirEtAl.h>

DemirEtAl::DemirEtAl(DemirEtAlParameters *pp) {
  pDeaP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pDeaP, NS_DemirEtAlParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

#ifdef HETERO

inline bool DemirEtAl::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool DemirEtAl::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

void DemirEtAl::Init() {
#if KADEBUG
  cerr << "initializing Class: DemirEtAl ... " << endl;
#endif // if KADEBUG
  h_1 = CELLMODEL_PARAMVALUE(VT_Init_h_1);
  h_2 = CELLMODEL_PARAMVALUE(VT_Init_h_2);
  d_L = CELLMODEL_PARAMVALUE(VT_Init_d_L);
  f_L = CELLMODEL_PARAMVALUE(VT_Init_f_L);
  d_T = CELLMODEL_PARAMVALUE(VT_Init_d_T);
  f_T = CELLMODEL_PARAMVALUE(VT_Init_f_T);
  p_a = CELLMODEL_PARAMVALUE(VT_Init_p_a);
  p_i = CELLMODEL_PARAMVALUE(VT_Init_p_i);
  y = CELLMODEL_PARAMVALUE(VT_Init_y);

  m = CELLMODEL_PARAMVALUE(VT_Init_m);

  Na_i = CELLMODEL_PARAMVALUE(VT_Init_Na_i);
  Na_c = CELLMODEL_PARAMVALUE(VT_Init_Na_c);
  K_i = CELLMODEL_PARAMVALUE(VT_Init_K_i);
  K_c = CELLMODEL_PARAMVALUE(VT_Init_K_c);
  Ca_i = CELLMODEL_PARAMVALUE(VT_Init_Ca_i);
  Ca_c = CELLMODEL_PARAMVALUE(VT_Init_Ca_c);

  P_C = CELLMODEL_PARAMVALUE(VT_Init_P_C);
  P_TC = CELLMODEL_PARAMVALUE(VT_Init_P_TC);
  P_TMGC = CELLMODEL_PARAMVALUE(VT_Init_P_TMGC);
  P_TMGM = CELLMODEL_PARAMVALUE(VT_Init_P_TMGM);
  P_Calse = CELLMODEL_PARAMVALUE(VT_Init_P_Calse);

  Ca_up = CELLMODEL_PARAMVALUE(VT_Init_Ca_up);
  Ca_rel = CELLMODEL_PARAMVALUE(VT_Init_Ca_rel);
  F1 = CELLMODEL_PARAMVALUE(VT_Init_F1);
  F2 = CELLMODEL_PARAMVALUE(VT_Init_F2);
  F3 = CELLMODEL_PARAMVALUE(VT_Init_F3);
} // DemirEtAl::Init

ML_CalcType
DemirEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
                int euler = 1) {
  ML_CalcType V_int = V * 1000.0;
  int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);

  double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(Na_c / Na_i));

  m = pDeaP->m_[Vi] - (pDeaP->m_[Vi] - m) * pDeaP->et_m[Vi];

  h_1 = pDeaP->h_1_[Vi] - (pDeaP->h_1_[Vi] - h_1) * pDeaP->et_h_1[Vi];
  h_2 = pDeaP->h_2_[Vi] - (pDeaP->h_2_[Vi] - h_2) * pDeaP->et_h_2[Vi];

  d_L = pDeaP->d_L_[Vi] - (pDeaP->d_L_[Vi] - d_L) * pDeaP->et_d_L[Vi];
  f_L = pDeaP->f_L_[Vi] - (pDeaP->f_L_[Vi] - f_L) * pDeaP->et_f_L[Vi];
  d_T = pDeaP->d_T_[Vi] - (pDeaP->d_T_[Vi] - d_T) * pDeaP->et_d_T[Vi];
  f_T = pDeaP->f_T_[Vi] - (pDeaP->f_T_[Vi] - f_T) * pDeaP->et_f_T[Vi];

  p_a = pDeaP->p_a_[Vi] - (pDeaP->p_a_[Vi] - p_a) * pDeaP->et_p_a_1[Vi];
  p_i += (pDeaP->a_p_i[Vi] * (1 - p_i) - pDeaP->b_p_i[Vi] * p_i) * tinc;

  y = pDeaP->y_[Vi] - (pDeaP->y_[Vi] - y) * pDeaP->et_y_1[Vi];

  double P_C__ = 1.29e5 * Ca_i * (1.0 - P_C) - 307.0 * P_C;
  P_C += P_C__ * tinc;
  double P_TC__ = 5.05e4 * Ca_i * (1.0 - P_TC) - 252.0 * P_TC;
  P_TC += P_TC__ * tinc;
  double P_TMGC__ = 1.29e5 * Ca_i * (1.0 - P_TMGC - P_TMGM) - 4.25 * P_TMGC;
  P_TMGC += P_TMGC__ * tinc;
  double P_TMGM__ = 1290.0 * (CELLMODEL_PARAMVALUE(VT_Mg_i)) * (1.0 - P_TMGC - P_TMGM) - 429.0 * P_TMGM;
  P_TMGM += P_TMGM__ * tinc;
  double P_B = .09 * P_C__ + .031 * P_TC__ + .062 * P_TMGC__;

  double I_up = ((CELLMODEL_PARAMVALUE(VT_a_up)) * Ca_i - (CELLMODEL_PARAMVALUE(VT_b_up)) * Ca_up * (CELLMODEL_PARAMVALUE(VT_K_1))) /
                (Ca_i + Ca_up * (CELLMODEL_PARAMVALUE(VT_K_1)) + (CELLMODEL_PARAMVALUE(VT_kmkpk)));
  double I_rel = F2 / (F2 + .25);
  I_rel *= (CELLMODEL_PARAMVALUE(VT_a_rel)) * I_rel * Ca_rel;

  double I_tr = (Ca_up - Ca_rel) * (CELLMODEL_PARAMVALUE(VT_V_up2F));

  double P_Calse__ = 770.0 * Ca_rel * (1.0 - P_Calse) - 641.0 * P_Calse;
  P_Calse += P_Calse__ * tinc;

  double r_Ca4 = Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_rel)));
  r_Ca4 *= r_Ca4;
  r_Ca4 *= r_Ca4;
  double r_act = 240.0 * (pDeaP->expVm40[Vi] + r_Ca4);
  double r_inact = 40.0 + 400.0 * r_Ca4;

  F1 += (.96 * F3 - r_act * F1) * tinc;
  F2 += (r_act * F1 - r_inact * F2) * tinc;
  F3 += (r_inact * F2 - .96 * F3) * tinc;

  double I_Na = pDeaP->C_Na[Vi] * m * m * m * h_1 * h_2 * Na_c * (exp(VmE_Na * (CELLMODEL_PARAMVALUE(VT_FdRT))) - 1.0);

  double I_CaL = (CELLMODEL_PARAMVALUE(VT_g_CaL)) * (d_L * f_L + .095 * pDeaP->d_L_[Vi]) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_CaL)));
  double I_CaT = (CELLMODEL_PARAMVALUE(VT_g_CaT)) * d_T * f_T * (V_int - (CELLMODEL_PARAMVALUE(VT_E_CaT)));
  double I_Ca = I_CaL + I_CaT;

  double I_K = ((CELLMODEL_PARAMVALUE(VT_g_K)) * p_a * p_i + (CELLMODEL_PARAMVALUE(VT_g_BK))) *
               (V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_c / K_i)));  // I_K+I_BK

  double I_BNa = (CELLMODEL_PARAMVALUE(VT_g_BNa)) * VmE_Na;
  double I_BCa = (CELLMODEL_PARAMVALUE(VT_g_BCa)) * (V_int - (CELLMODEL_PARAMVALUE(VT_RTd2F)) * log(Ca_c / (double) Ca_i));

  double yy = y * y;
  double I_fNa = (CELLMODEL_PARAMVALUE(VT_g_fNa)) * yy * (V_int - 75.0);
  double I_fK = (CELLMODEL_PARAMVALUE(VT_g_fK)) * yy * (V_int + 85.0);

  double NaIdNaI3 = Na_i / (Na_i + (CELLMODEL_PARAMVALUE(VT_k_mNa)));
  NaIdNaI3 *= NaIdNaI3 * NaIdNaI3;
  double KcdKc2 = K_c / (K_c + (CELLMODEL_PARAMVALUE(VT_k_mK)));
  KcdKc2 *= KcdKc2;
  double I_NaK = NaIdNaI3 * KcdKc2 * pDeaP->C_NaK[Vi];

  double I_CaP = (CELLMODEL_PARAMVALUE(VT_I_CaP_)) * Ca_i / (Ca_i + .0004);

  double NNNICO = Na_i * Na_i * Na_i * Ca_c;
  double NNNOCI = Na_c * Na_c * Na_c * Ca_i;
  double I_NaCa = pDeaP->C_NaCa[Vi] * (NNNICO - NNNOCI * pDeaP->dexpV[Vi]) /
                  (1.0 + (CELLMODEL_PARAMVALUE(VT_d_NaCa)) * (NNNOCI + NNNICO));

  Ca_up += ((I_up - I_tr) * (CELLMODEL_PARAMVALUE(VT_dV_up2F))) * tinc;
  Ca_rel += ((I_tr - I_rel) * (CELLMODEL_PARAMVALUE(VT_dV_rel2F)) - 11.48 * P_Calse__) * tinc;

  double I_Nages = I_Na + 3.0 * (I_NaCa + I_NaK) + I_BNa + I_fNa;
  double I_Kges = -I_NaK - I_NaK + I_K + I_fK;
  double I_Cages = I_Ca - I_NaCa - I_NaCa + I_CaP + I_BCa;

  Na_i -= I_Nages * (CELLMODEL_PARAMVALUE(VT_dV_iF)) * tinc;
  K_i -= I_Kges * (CELLMODEL_PARAMVALUE(VT_dV_iF)) * tinc;
  Ca_i += ((I_rel - I_up - I_Cages) * (CELLMODEL_PARAMVALUE(VT_dV_Ca2F)) - P_B) * tinc;

  Na_c += (((CELLMODEL_PARAMVALUE(VT_Na_b)) - Na_c) * (CELLMODEL_PARAMVALUE(VT_dt_p)) + I_Nages * (CELLMODEL_PARAMVALUE(VT_dV_cF))) * tinc;
  K_c += (((CELLMODEL_PARAMVALUE(VT_K_b)) - K_c) * (CELLMODEL_PARAMVALUE(VT_dt_p)) + I_Kges * (CELLMODEL_PARAMVALUE(VT_dV_cF))) * tinc;
  Ca_c += (((CELLMODEL_PARAMVALUE(VT_Ca_b)) - Ca_c) * (CELLMODEL_PARAMVALUE(VT_dt_p)) + I_Cages * (CELLMODEL_PARAMVALUE(VT_dV_cF)) * .5) * tinc;

  return -(I_Nages + I_Kges + I_Cages - i_external) * (CELLMODEL_PARAMVALUE(VT_dC_m)) * tinc;
} // DemirEtAl::Calc

void DemirEtAl::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << h_1 << ' ' << h_2 << ' '
          << d_L << ' ' << f_L << ' '
          << d_T << ' ' << f_T << ' '
          << p_a << ' ' << p_i << ' '
          << y << ' ' << m << ' '
          << Na_i << ' ' << Na_c << ' '
          << K_i << ' ' << K_c << ' '
          << Ca_i << ' ' << Ca_c << ' '
          << P_C << ' ' << P_TC << ' ' << P_TMGC << ' ' << P_TMGM << ' ' << P_Calse << ' '
          << Ca_up << ' ' << Ca_rel << ' '
          << F1 << ' ' << F2 << ' ' << F3 << ' ';
}

void DemirEtAl::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  ML_CalcType V_int = V * 1000.0;
  int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  double VmE_Na = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(Na_c / Na_i));
  double NaIdNaI3 = Na_i / (Na_i + (CELLMODEL_PARAMVALUE(VT_k_mNa)));
  NaIdNaI3 *= NaIdNaI3 * NaIdNaI3;
  double KcdKc2 = K_c / (K_c + (CELLMODEL_PARAMVALUE(VT_k_mK)));
  KcdKc2 *= KcdKc2;
  double NNNICO = Na_i * Na_i * Na_i * Ca_c;
  double NNNOCI = Na_c * Na_c * Na_c * Ca_i;

  tempstr << ((CELLMODEL_PARAMVALUE(VT_a_up)) * Ca_i - (CELLMODEL_PARAMVALUE(VT_b_up)) * Ca_up * (CELLMODEL_PARAMVALUE(VT_K_1))) /
             (Ca_i + Ca_up * (CELLMODEL_PARAMVALUE(VT_K_1)) + (CELLMODEL_PARAMVALUE(VT_kmkpk))) << ' '  // I_up
          << F2 / (F2 + .25) * F2 / (F2 + .25) * (CELLMODEL_PARAMVALUE(VT_a_rel)) * Ca_rel << ' ' // I_rel
          << (Ca_up - Ca_rel) * (CELLMODEL_PARAMVALUE(VT_V_up2F)) << ' ' // I_tr
          << pDeaP->C_Na[Vi] * m * m * m * h_1 * h_2 * Na_c * (exp(VmE_Na * (CELLMODEL_PARAMVALUE(VT_FdRT))) - 1.0)
          << ' ' // I_Na
          << (CELLMODEL_PARAMVALUE(VT_g_CaL)) * (d_L * f_L + .095 * pDeaP->d_L_[Vi]) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_CaL)))
          << ' ' // I_CaL
          << (CELLMODEL_PARAMVALUE(VT_g_CaT)) * d_T * f_T * (V_int - (CELLMODEL_PARAMVALUE(VT_E_CaT))) << ' ' // I_CaT
          << (CELLMODEL_PARAMVALUE(VT_g_K)) * p_a * p_i * (V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_c / K_i))) << ' ' // I_K
          << (CELLMODEL_PARAMVALUE(VT_g_BK)) * (V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_c / K_i))) << ' ' // I_BK
          << (CELLMODEL_PARAMVALUE(VT_g_BNa)) * VmE_Na << ' ' // I_BNa
          << (CELLMODEL_PARAMVALUE(VT_g_BCa)) * (V_int - (CELLMODEL_PARAMVALUE(VT_RTd2F)) * log(Ca_c / (double) Ca_i)) << ' ' // I_BCa
          << (CELLMODEL_PARAMVALUE(VT_g_fNa)) * y * y * (V_int - 75.0) << ' ' // I_fNa
          << (CELLMODEL_PARAMVALUE(VT_g_fK)) * y * y * (V_int + 85.0) << ' ' // I_fK
          << NaIdNaI3 * KcdKc2 * pDeaP->C_NaK[Vi] << ' ' // I_NaK
          << (CELLMODEL_PARAMVALUE(VT_I_CaP_)) * Ca_i / (Ca_i + .0004) << ' ' // I_CaP
          << pDeaP->C_NaCa[Vi] * (NNNICO - NNNOCI * pDeaP->dexpV[Vi]) /
             (1.0 + (CELLMODEL_PARAMVALUE(VT_d_NaCa)) * (NNNOCI + NNNICO)) << ' '; // I_NaCa
} // DemirEtAl::LongPrint

void DemirEtAl::GetParameterNames(vector<string> &getpara) {
  const int numpara = 26;
  const string ParaNames[numpara] =
      {"h_1", "h_2", "d_L", "f_L", "d_T", "f_T", "p_a", "p_i",
       "y",
       "m", "Na_i", "Na_c", "K_i", "K_c",
       "Ca_i",
       "Ca_c", "P_C", "P_TC", "P_TMGC", "P_TMGM", "P_Calse", "Ca_up", "Ca_rel",
       "F1",
       "F2", "F3"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void DemirEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 15;
  const string ParaNames[numpara] = {"I_up", "I_rel", "I_tr", "I_Na", "I_CaL", "I_CaT", "I_K",
                                     "I_BK",
                                     "I_BNa", "I_BCa", "I_fNa", "I_fK", "I_NaK", "I_CaP", "I_NaCa"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
