/*      File: LuoRudyParameters.cpp
    automatically created by ExtractParameterClass.pl - done by dw (19.09.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#include <LuoRudyParameters.h>

LuoRudyParameters::LuoRudyParameters(const char *initFile, ElphyModelType emt, ML_CalcType tinc) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile, emt, tinc);
}

LuoRudyParameters::~LuoRudyParameters() {
  // Destruktor
}

void LuoRudyParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "LuoRudyParameters:" << endl;
  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void LuoRudyParameters::Init(const char *initFile, ElphyModelType emt, ML_CalcType tinc) {
#if KADEBUG
  cerr << "LuoRudyParameters " << initFile << "\n";
#endif // if KADEBUG

  // Initialization of the Parameters ...
  P[VT_R].name = "R";
  P[VT_Tx].name = "T";
  P[VT_F].name = "F";
  P[VT_RTdF].name = "RTdF";
  P[VT_FdRT].name = "FdRT";
  P[VT_d2F].name = "d2F";
  P[VT_RTd2F].name = "RTd2F";
  P[VT_C_m].name = "C_m";
  P[VT_dC_m].name = "dC_m";
  P[VT_TRPN_max].name = "TRPN_max";
  P[VT_CMDN_max].name = "CMDN_max";
  P[VT_taudiff].name = "taudiff";
  P[VT_dtaudiff].name = "dtaudiff";
  P[VT_k_mTRPN].name = "k_mTRPN";
  P[VT_k_mCMDN].name = "k_mCMDN";
  P[VT_k_mCSQN].name = "k_mCSQN";
  P[VT_CSQN_th].name = "CSQN_th";
  P[VT_k_mCa].name = "k_mCa";
  P[VT_eta].name = "eta";
  P[VT_k_mNai].name = "k_mNai";
  P[VT_kk_mNai].name = "kk_mNai";
  P[VT_k_mKo].name = "k_mKo";
  P[VT_k_mnsCa].name = "k_mnsCa";
  P[VT_kkk_mnsCa].name = "kkk_mnsCa";
  P[VT_k_mpCa].name = "k_mpCa";
  P[VT_k_mup].name = "k_mup";
  P[VT_I_NaKmax].name = "I_NaKmax";
  P[VT_I_pCamax].name = "I_pCamax";
  P[VT_I_upmax].name = "I_upmax";
  P[VT_g_Na].name = "g_Na";
  P[VT_g_Kp].name = "g_Kp";
  P[VT_g_Cab].name = "g_Cab";
  P[VT_g_CaT].name = "g_CaT";
  P[VT_g_Nab].name = "g_Nab";
  P[VT_g_to].name = "g_to";
  P[VT_g_K1].name = "g_K1";
  P[VT_g_maxrel].name = "g_maxrel";
  P[VT_g_KNa].name = "g_KNa";
  P[VT_n_KNa].name = "n_KNa";
  P[VT_kdk_Na].name = "kdk_Na";
  P[VT_t_on].name = "t_on";
  P[VT_dt_on].name = "dt_on";
  P[VT_t_off].name = "t_off";
  P[VT_dt_off].name = "dt_off";
  P[VT_t_tr].name = "t_tr";
  P[VT_dt_tr].name = "dt_tr";
  P[VT_P_Ca].name = "P_Ca";
  P[VT_P_Na].name = "P_Na";
  P[VT_P_K].name = "P_K";
  P[VT_P_NaK].name = "P_NaK";
  P[VT_P_nsCa].name = "P_nsCa";
  P[VT_gamm_Cai].name = "gamm_Cai";
  P[VT_gamm_Cao].name = "gamm_Cao";
  P[VT_gamm_Nai].name = "gamm_Nai";
  P[VT_gamm_Nao].name = "gamm_Nao";
  P[VT_gamm_Ki].name = "gamm_Ki";
  P[VT_gamm_Ko].name = "gamm_Ko";
  P[VT_KgC].name = "KgC";
  P[VT_K_b].name = "K_b";
  P[VT_Ca_b].name = "Ca_b";
  P[VT_Ca_NSRmax].name = "Ca_NSRmax";
  P[VT_IupdCa].name = "IupdCa";
  P[VT_Na_b].name = "Na_b";
  P[VT_natp].name = "natp";
  P[VT_nicholsarea].name = "nicholsarea";
  P[VT_atpi].name = "atpi";
  P[VT_hatp].name = "hatp";
  P[VT_katp].name = "katp";
  P[VT_I_Katpmax].name = "I_Katpmax";
  P[VT_radius].name = "radius";
  P[VT_lenght].name = "lenght";
  P[VT_Vol].name = "Vol";
  P[VT_V_cell].name = "V_cell";
  P[VT_A_Geo].name = "A_Geo";
  P[VT_R_CG].name = "R_CG";
  P[VT_A_Cap].name = "A_Cap";
  P[VT_V_myo].name = "V_myo";
  P[VT_dV_myo].name = "dV_myo";
  P[VT_AdVmF].name = "AdVmF";
  P[VT_V_mito].name = "V_mito";
  P[VT_V_SR].name = "V_SR";
  P[VT_V_NSR].name = "V_NSR";
  P[VT_V_JSR].name = "V_JSR";
  P[VT_VJdVN].name = "VJdVN";
  P[VT_V_cleft].name = "V_cleft";
  P[VT_AdVcF].name = "AdVcF";
  P[VT_AdVcFd2].name = "AdVcFd2";
  P[VT_Ca_i].name = "Ca_i";
  P[VT_Ca_o].name = "Ca_o";
  P[VT_Na_i].name = "Na_i";
  P[VT_Na_o].name = "Na_o";
  P[VT_K_i].name = "K_i";
  P[VT_K_o].name = "K_o";
  P[VT_Ca_JSR].name = "Ca_JSR";
  P[VT_Ca_NSR].name = "Ca_NSR";
  P[VT_dCa_iont].name = "dCa_iont";
  P[VT_t_CICR].name = "t_CICR";
  P[VT_t_jsrol].name = "t_jsrol";
  P[VT_m].name = "m";
  P[VT_h].name = "h";
  P[VT_j].name = "j";
  P[VT_d].name = "d";
  P[VT_f].name = "f";
  P[VT_b].name = "b";
  P[VT_g].name = "g";
  P[VT_Xr].name = "Xr";
  P[VT_Xs1].name = "Xs1";
  P[VT_Xs2].name = "Xs2";
  P[VT_ato].name = "ato";
  P[VT_ito].name = "ito";
  P[VT_grelbarjsrol].name = "grelbarjsrol";
  P[VT_flip].name = "flip";
  P[VT_init_V_m].name = "init_V_m";
  P[VT_KgN].name = "KgN";
  P[VT_CSQN_max].name = "CSQN_max";

  P[VT_RTdF].readFromFile = false;
  P[VT_FdRT].readFromFile = false;
  P[VT_d2F].readFromFile = false;
  P[VT_RTd2F].readFromFile = false;
  P[VT_dtaudiff].readFromFile = false;
  P[VT_dC_m].readFromFile = false;
  P[VT_CSQN_th].readFromFile = false;
  P[VT_kk_mNai].readFromFile = false;
  P[VT_kkk_mnsCa].readFromFile = false;
  P[VT_dt_on].readFromFile = false;
  P[VT_dt_off].readFromFile = false;
  P[VT_dt_tr].readFromFile = false;
  P[VT_KgC].readFromFile = false;
  P[VT_IupdCa].readFromFile = false;
  P[VT_I_Katpmax].readFromFile = false;
  P[VT_Vol].readFromFile = false;
  P[VT_V_cell].readFromFile = false;
  P[VT_A_Geo].readFromFile = false;
  P[VT_A_Cap].readFromFile = false;
  P[VT_V_myo].readFromFile = false;
  P[VT_dV_myo].readFromFile = false;
  P[VT_AdVmF].readFromFile = false;
  P[VT_V_mito].readFromFile = false;
  P[VT_V_SR].readFromFile = false;
  P[VT_V_NSR].readFromFile = false;
  P[VT_V_JSR].readFromFile = false;
  P[VT_VJdVN].readFromFile = false;
  P[VT_V_cleft].readFromFile = false;
  P[VT_AdVcF].readFromFile = false;
  P[VT_AdVcFd2].readFromFile = false;
  P[VT_KgN].readFromFile = false;

  ParameterLoader EPL(initFile, emt);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  // End Initialization of the Parameters ...

  Calculate();
  InitTable(tinc);
} // LuoRudyParameters::Init

void LuoRudyParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "LuoRudyParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_RTdF].value = (P[VT_R].value) * ((P[VT_Tx].value) / (P[VT_F].value));
  P[VT_FdRT].value = 1.0 / (P[VT_RTdF].value);
  P[VT_d2F].value = 1.0 / (2.0 * (P[VT_F].value));
  P[VT_RTd2F].value = (P[VT_RTdF].value) * .5;
  P[VT_dtaudiff].value = 1.0 / (P[VT_taudiff].value);
  P[VT_dC_m].value = .001 / (P[VT_C_m].value);
  P[VT_CSQN_th].value = .875 * P[VT_CSQN_max].value;
  P[VT_kk_mNai].value = P[VT_k_mNai].value * (P[VT_k_mNai].value);
  P[VT_kkk_mnsCa].value = P[VT_k_mnsCa].value * (P[VT_k_mnsCa].value) * (P[VT_k_mnsCa].value);
  P[VT_dt_on].value = 1.0 / (P[VT_t_on].value);
  P[VT_dt_off].value = 1.0 / (P[VT_t_off].value);
  P[VT_dt_tr].value = 1.0 / (P[VT_t_tr].value);
  P[VT_KgC].value = P[VT_gamm_Cai].value / (P[VT_gamm_Cao].value);
  P[VT_KgN].value = P[VT_gamm_Nai].value / (P[VT_gamm_Nao].value);
  P[VT_IupdCa].value = P[VT_I_upmax].value / (P[VT_Ca_NSRmax].value);
  P[VT_I_Katpmax].value = .000195 / (P[VT_nicholsarea].value) /
                          (1 + pow((P[VT_atpi].value / (P[VT_katp].value)), P[VT_hatp].value));
  P[VT_Vol].value =
      1000.0 * M_PI * (P[VT_radius].value) * (P[VT_radius].value) * (P[VT_lenght].value); // m^3
  P[VT_V_cell].value = P[VT_Vol].value * 1e9;
  P[VT_A_Geo].value =
      2.0 * M_PI * (P[VT_radius].value) * (P[VT_radius].value + (P[VT_lenght].value)) * 1e6;
  P[VT_A_Cap].value = P[VT_R_CG].value * (P[VT_A_Geo].value);
  P[VT_V_myo].value = P[VT_V_cell].value * .68;
  P[VT_dV_myo].value = 1.0 / (P[VT_V_myo].value);
  P[VT_AdVmF].value = P[VT_A_Cap].value * (P[VT_dV_myo].value) / (P[VT_F].value);
  P[VT_V_mito].value = P[VT_V_cell].value * .26;
  P[VT_V_SR].value = P[VT_V_cell].value * .06;
  P[VT_V_NSR].value = P[VT_V_cell].value * .0552;
  P[VT_V_JSR].value = P[VT_V_cell].value * .0048;
  P[VT_VJdVN].value = P[VT_V_JSR].value / (P[VT_V_NSR].value);
  P[VT_V_cleft].value = P[VT_V_cell].value * .13636364;
  P[VT_AdVcF].value = P[VT_A_Cap].value / (P[VT_V_cleft].value * (P[VT_F].value));
  P[VT_AdVcFd2].value = P[VT_AdVcF].value * .5;
} // LuoRudyParameters::Calculate

void LuoRudyParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr << "LuoRudyParameters::InitTable()\n";
#endif // if KADEBUG
  ML_CalcType a, b;
  tinc *= -1000.;  // sec -> ms; neg. for exp(-dt/tau);
  for (double V = -RangeTabhalf + .0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int) (DivisionTab * (RangeTabhalf + V) + .5);
    if (V >= -40.0) {
      // a=.0;
      b = 1 / (.13 * (1.0 + exp((V + 10.66) / -11.1)));
      h_inf[Vi] = 0;  // a / b;
      et_h[Vi] = exp(tinc * b);

      b = .3 * exp(-.0000002535 * V) / (1.0 + exp(-.1 * (V + 32.0)));
      j_inf[Vi] = 0;
      et_j[Vi] = exp(tinc * b);
    } else {
      a = .135 * exp((V + 80.0) / -6.8);
      b = a + 3.56 * exp(.079 * V) + 310000 * exp(.35 * V);
      h_inf[Vi] = a / b;
      et_h[Vi] = exp(tinc * b);

      a = (-127140 * exp(.2444 * V) - .00003474 * exp(-.04391 * V)) *
          ((V + 37.78) / (1.0 + exp(.311 * (V + 79.23))));
      b = a + .1212 * exp(-.01052 * V) / (1.0 + exp(-.1378 * (V + 40.14)));
      j_inf[Vi] = a / b;
      et_j[Vi] = exp(tinc * b);
    }
    a = .32 * (V + 47.13) / (1.0 - exp(-.1 * (V + 47.13)));
    b = a + .08 * exp(-V / 11.0);
    m_inf[Vi] = a / b;
    et_m[Vi] = exp(tinc * b);

    a = (.035 * (V + 10.0)) / (1.0 - exp(-(V + 10.0) / 6.24));
    b = a + ((1.0 + exp(-(V + 10.0) / 6.24)) - 1.0) * a;
    d_inf[Vi] = a / b;
    et_d[Vi] = exp(tinc * b);

    a = (1.0 / (1.0 + exp((V + 32.0) * .125)) + .6 / (1.0 + exp(.05 * (50.0 - V)))) *
        (.0197 * exp(-(.0337 * (V + 10)) * (.0337 * (V + 10.0))) + .02);
    b = a + (1.0 - (1.0 / (1.0 + exp((V + 32.0) * .125)) + .6 / (1.0 + exp(.05 * (50.0 - V))))) *
            (.0197 * exp(-(.0337 * (V + 10.0)) * (.0337 * (V + 10.0))) + .02);
    f_inf[Vi] = a / b;
    et_f[Vi] = exp(tinc * b);

    a = 1.0 / (1.0 + exp(-(V + 14.0) / 10.8)) / (3.7 + 6.1 / (1.0 + exp((V + 25.0) / 4.5)));
    b = a +
        (1.0 - 1.0 / (1.0 + exp(-(V + 14.0) / 10.8))) / (3.7 + 6.1 / (1 + exp((V + 25.0) / 4.5)));
    b_inf[Vi] = a / b;
    et_b[Vi] = exp(tinc * b);

    if (V > 0.0) {
      a = 1.0 / (1.0 + exp((V + 60.0) / 5.6)) / 12.0;
      b = a + (1.0 - 1.0 / (1.0 + exp((V + 60.0) / 5.6))) / 12.0;
      g_inf[Vi] = a / b;
      et_g[Vi] = exp(tinc * b);
    } else {
      a = 1.0 / (1.0 + exp((V + 60.0) / 5.6)) / (-.875 * V + 12.0);
      b = a + (1.0 - 1.0 / (1.0 + exp((V + 60.0) / 5.6))) / (-.875 * V + 12.0);
      g_inf[Vi] = a / b;
      et_g[Vi] = exp(tinc * b);
    }
    a = 1.0 / (1.0 + exp(-(V + 21.5) / 7.5)) *
        (.00138 * (V + 14.2) / (1.0 - exp(-.123 * (V + 14.2))) +
         .00061 * (V + 38.9) / (exp(.145 * (V + 38.9)) - 1.0));
    b = a + (1.0 - 1.0 / (1.0 + exp(-(V + 21.5) / 7.5))) *
            (.00138 * (V + 14.2) / (1.0 - exp(-.123 * (V + 14.2))) +
             .00061 * (V + 38.9) / (exp(.145 * (V + 38.9)) - 1.0));
    Xr_inf[Vi] = a / b;
    et_Xr[Vi] = exp(tinc * b);

    a = 1.0 / (1.0 + exp(-(V - 1.5) / 16.7)) *
        ((7.19e-5 * (V + 30.0) / (1.0 - exp(-.148 * (V + 30.0))) +
          1.31e-4 * (V + 30.0) / (exp(.0687 * (V + 30.0)) - 1.0)));
    b = a + (1.0 - 1.0 / (1.0 + exp(-(V - 1.5) / 16.7))) *
            (7.19e-5 * (V + 30.0) / (1.0 - exp(-.148 * (V + 30.0))) +
             1.31e-4 * (V + 30.0) / (exp(.0687 * (V + 30.0)) - 1.0));
    Xs1_inf[Vi] = a / b;
    et_Xs1[Vi] = exp(tinc * b);

    // a=a*.25;
    // b=a+b*.25;
    Xs2_inf[Vi] = Xs1_inf[Vi];
    et_Xs2[Vi] = exp(tinc * b * .25);

    double m_ato = 45.16 * exp(0.03577 * V) / (45.16 * exp(.03577 * V) + 98.9 * exp(-.06237 * V));
    double t_ato = 250.0 / (45.16 * exp(.03577 * V) + 98.9 * exp(-.06237 * V));
    a = m_ato / t_ato;
    b = a + (1 - m_ato) / t_ato;
    ato_inf[Vi] = a / b;
    et_ato[Vi] = exp(tinc * b);

    double m_ito = 1.0 /
                   (1.0 +
                    ((1.9 * exp((V + 13.5) / 11.3) / (1 + .067083 * exp((V + 13.5) / 11.3))) /
                     (1.9 * exp(-(V + 13.5) / 11.3) / (1.0 + .051335 * exp(-(V + 13.5) / 11.3)))));
    double t_ito = 250.0 /
                   ((1.9 * exp(-(V + 13.5) / 11.3) / (1.0 + .051335 * exp(-(V + 13.5) / 11.3))) +
                    (1.9 * exp((V + 13.5) / 11.3) / (1.0 + .067083 * exp((V + 13.5) / 11.3))));
    a = m_ito / t_ito;
    b = a + (1 - m_ito) / t_ito;
    ito_inf[Vi] = a / b;
    et_ito[Vi] = exp(tinc * b);

    Kp[Vi] = P[VT_g_Kp].value / (1.0 + exp((7.488 - V) / 5.98));
    Rinf[Vi] = .02614 / (1.0 + exp((V + 9.0) / 22.4));
    double VFdRT = V * (P[VT_FdRT].value);
    expV[Vi] = exp(VFdRT);
    expV_2[Vi] = P[VT_KgC].value * exp(VFdRT * 2.0);
    expVm[Vi] = .0052142856 * exp(-VFdRT);
    expV_mk1[Vi] = exp(-.1 * VFdRT);
    C_Ca[Vi] = (P[VT_gamm_Cao].value) * (P[VT_P_Ca].value) * 4.0 * VFdRT * (P[VT_F].value) /
               (expV_2[Vi] / (P[VT_KgC].value) - 1.0);
    C_NaK[Vi] = (P[VT_gamm_Nao].value) * VFdRT * (P[VT_F].value) / (expV[Vi] - 1.0);
    etaV[Vi] = .0001 * exp(((P[VT_eta].value) - 1.0) * VFdRT);
  }
} // LuoRudyParameters::InitTable
