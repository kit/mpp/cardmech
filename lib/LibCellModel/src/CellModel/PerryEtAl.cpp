/*      File: PerryEtAl.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
        Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
        send comments to dw@ibt.uka.de  */

#include <PerryEtAl.h>

PerryEtAl::PerryEtAl(PerryEtAlParameters *pIMWArg) {
  pIMW = pIMWArg;
#ifdef HETERO
  PS = new ParameterSwitch(pIMW, NS_PerryEtAlParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

PerryEtAl::~PerryEtAl() {}

#ifdef HETERO

inline bool PerryEtAl::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool PerryEtAl::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int PerryEtAl::GetSize(void) {
  return (&Qb - &Ki + 1) * sizeof(ML_CalcType)
#ifdef HETERO
    -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
      ;
}

void PerryEtAl::Init() {
  Ki = CELLMODEL_PARAMVALUE(VT_Ki);
  C0Kr = CELLMODEL_PARAMVALUE(VT_C0Kr);
  C1Kr = CELLMODEL_PARAMVALUE(VT_C1Kr);
  C2Kr = CELLMODEL_PARAMVALUE(VT_C2Kr);
  OKr = CELLMODEL_PARAMVALUE(VT_OKr);
  IKr = CELLMODEL_PARAMVALUE(VT_IKr);
  C0Kr2 = CELLMODEL_PARAMVALUE(VT_C0Kr2);
  C1Kr2 = CELLMODEL_PARAMVALUE(VT_C1Kr2);
  C2Kr2 = CELLMODEL_PARAMVALUE(VT_C2Kr2);
  OKr2 = CELLMODEL_PARAMVALUE(VT_OKr2);
  IKr2 = CELLMODEL_PARAMVALUE(VT_IKr2);

  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);
  Qb = pow(3.3, (Tx - 310.) / 10.);  // Q10 from Iyer et al.
}

void PerryEtAl::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' '
          << V << ' '
          << Ki << ' '
          << C0Kr << ' '
          << C1Kr << ' '
          << C2Kr << ' '
          << OKr << ' '
          << IKr << ' '
          << C0Kr2 << ' '
          << C1Kr2 << ' '
          << C2Kr2 << ' '
          << OKr2 << ' '
          << IKr2 << ' ';
}

void PerryEtAl::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);

  //   const double Acap=CELLMODEL_PARAMVALUE(VT_Acap);
  //   const double Vmyo=CELLMODEL_PARAMVALUE(VT_Vmyo);
  const double V_int = V * 1000.0;
  const double Ko = CELLMODEL_PARAMVALUE(VT_Ko);
  const double RTdF = ElphyModelConstants::R * Tx / ElphyModelConstants::F;
  const double EK = RTdF * log(Ko / Ki);
  const double f = sqrt(Ko * .25);
  const double I_Kr = CELLMODEL_PARAMVALUE(VT_GKr) * f * (OKr + OKr2) * (V_int - EK);
  tempstr << ' ' << I_Kr;
}

void PerryEtAl::GetParameterNames(vector<string> &getpara) {
  const char *ParaNames[] = {"Ki", "C0Kr", "C1Kr", "C2Kr", "OKr", "IKr", "C0Kr2", "C1Kr2", "C2Kr2",
                             "OKr2", "IKr2"};

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void PerryEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const char *ParaNames[] = {"I_Kr"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType
PerryEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim = .0, ML_CalcType stretch = 1.,
                int euler = 1) {
  tinc *= 1000.0;
  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);
  const double V_RTdF = V / (ElphyModelConstants::R * Tx / ElphyModelConstants::F / 1000.);

  const double Kra0 = CELLMODEL_PARAMVALUE(VT_Kra0a) * exp(CELLMODEL_PARAMVALUE(VT_Kra0z) * V_RTdF);
  const double Krb0 = CELLMODEL_PARAMVALUE(VT_Krb0a) * exp(-CELLMODEL_PARAMVALUE(VT_Krb0z) * V_RTdF);
  const double Krf = CELLMODEL_PARAMVALUE(VT_Krfa) * exp(CELLMODEL_PARAMVALUE(VT_Krfz) * V_RTdF);
  const double Krb = CELLMODEL_PARAMVALUE(VT_Krba) * exp(-CELLMODEL_PARAMVALUE(VT_Krbz) * V_RTdF);
  const double Kra1 = CELLMODEL_PARAMVALUE(VT_Kra1a) * exp(CELLMODEL_PARAMVALUE(VT_Kra1z) * V_RTdF);
  const double Krb1 = CELLMODEL_PARAMVALUE(VT_Krb1a) * exp(-CELLMODEL_PARAMVALUE(VT_Krb1z) * V_RTdF);
  const double Krai = CELLMODEL_PARAMVALUE(VT_Kraia) * exp(CELLMODEL_PARAMVALUE(VT_Kraiz) * V_RTdF);
  const double Krbi = CELLMODEL_PARAMVALUE(VT_Krbia) * exp(-CELLMODEL_PARAMVALUE(VT_Krbiz) * V_RTdF);
  const double Kraci = CELLMODEL_PARAMVALUE(VT_Kracia) * exp(CELLMODEL_PARAMVALUE(VT_Kraciz) * V_RTdF);
  const double Krbci = CELLMODEL_PARAMVALUE(VT_Krbcia) * exp(-CELLMODEL_PARAMVALUE(VT_Krbciz) * V_RTdF);
  const double Kra12 = pow(CELLMODEL_PARAMVALUE(VT_Kra12) * CELLMODEL_PARAMVALUE(VT_RPR), CELLMODEL_PARAMVALUE(VT_Krexp12));
  const double Krb12 = pow(CELLMODEL_PARAMVALUE(VT_Krb12), CELLMODEL_PARAMVALUE(VT_Krexp12));

  const double Kra1_2 = CELLMODEL_PARAMVALUE(VT_Kra1a2) * exp(CELLMODEL_PARAMVALUE(VT_Kra1z) * V_RTdF);
  const double Krb1_2 = CELLMODEL_PARAMVALUE(VT_Krb1a2) * exp(-CELLMODEL_PARAMVALUE(VT_Krb1z) * V_RTdF);
  const double Krai_2 = CELLMODEL_PARAMVALUE(VT_Kraia2) * exp(CELLMODEL_PARAMVALUE(VT_Kraiz2) * V_RTdF);


  const double dC0Kr = -Kra0 * C0Kr + Krb0 * C1Kr;
  const double dC1Kr = -(Krb0 + Krf) * C1Kr + Kra0 * C0Kr + Krb * C2Kr;
  const double dC2Kr = -(Kra1 + Krb + Kraci) * C2Kr + Krf * C1Kr + Krb1 * OKr + Krbci * IKr;
  const double dOKr = -(Krb1 + Krai + Kra12) * OKr + Kra1 * C2Kr + Krbi * IKr + Krb12 * OKr2;
  const double dIKr = -(Krbci + Krbi + Kra12) * IKr + Kraci * C2Kr + Krai * OKr + Krb12 * IKr2;

  const double dC0Kr2 = -Kra0 * C0Kr2 + Krb0 * C1Kr2;
  const double dC1Kr2 = -(Krb0 + Krf) * C1Kr2 + Kra0 * C0Kr2 + Krb * C2Kr2;
  const double dC2Kr2 =
      -(Kra1_2 + Krb + Kraci) * C2Kr2 + Krf * C1Kr2 + Krb1_2 * OKr2 + Krbci * IKr2;

  //    const double dOKr2= -(Krb1_2+Krai_2+Krb12)*OKr2 +Kra1_2 *C2Kr2+Krbi  *IKr2 +Kra12*OKr;
  const double dIKr2 = -(Krbci + Krbi + Krb12) * IKr2 + Kraci * C2Kr2 + Krai_2 * OKr2 + Kra12 * IKr;

  C0Kr += tinc * dC0Kr * Qb;
  if (C0Kr > 1.)
    C0Kr = 1.;
  else if (C0Kr < 0)
    C0Kr = 0.;
  C1Kr += tinc * dC1Kr * Qb;
  if (C1Kr > 1.)
    C1Kr = 1.;
  else if (C1Kr < 0)
    C1Kr = 0.;
  C2Kr += tinc * dC2Kr * Qb;
  if (C2Kr > 1.)
    C2Kr = 1.;
  else if (C2Kr < 0)
    C2Kr = 0.;
  IKr += tinc * dIKr * Qb;
  if (IKr > 1.)
    IKr = 1.;
  else if (IKr < 0)
    IKr = 0.;
  OKr += tinc * dOKr * Qb;
  if (OKr > 1.)
    OKr = 1.;
  else if (OKr < 0)
    OKr = 0.;

  C0Kr2 += tinc * dC0Kr2 * Qb;
  if (C0Kr2 > 1.)
    C0Kr2 = 1.;
  else if (C0Kr2 < 0)
    C0Kr2 = 0.;
  C1Kr2 += tinc * dC1Kr2 * Qb;
  if (C1Kr2 > 1.)
    C1Kr2 = 1.;
  else if (C1Kr2 < 0)
    C1Kr2 = 0.;
  C2Kr2 += tinc * dC2Kr2 * Qb;
  if (C2Kr2 > 1.)
    C2Kr2 = 1.;
  else if (C2Kr2 < 0)
    C2Kr2 = 0.;
  IKr2 += tinc * dIKr2 * Qb;
  if (IKr2 > 1.)
    IKr2 = 1.;
  else if (IKr2 < 0)
    IKr2 = 0.;
  OKr2 = 1. - C0Kr - C1Kr - C2Kr - OKr - IKr - C0Kr2 - C1Kr2 - C2Kr2 - IKr2;

  //    OKr2+=tinc*dOKr2*Qb;
  if (OKr2 > 1.)
    OKr2 = 1.;
  else if (OKr2 < 0)
    OKr2 = 0.;

  return 0.;
} // PerryEtAl::Calc

int PerryEtAl::GetNumStatus() {
  return 11;
}

void PerryEtAl::GetStatus(double *p) const {
  p[0] = Ki;
  p[1] = C0Kr;
  p[2] = C1Kr;
  p[3] = C2Kr;
  p[4] = OKr;
  p[5] = IKr;
  p[6] = C0Kr2;
  p[7] = C1Kr2;
  p[8] = C2Kr2;
  p[9] = OKr2;
  p[10] = IKr2;
}

void PerryEtAl::SetStatus(const double *p) {
  Ki = p[0];
  C0Kr = p[1];
  C1Kr = p[2];
  C2Kr = p[3];
  OKr = p[4];
  IKr = p[5];
  C0Kr2 = p[6];
  C1Kr2 = p[7];
  C2Kr2 = p[8];
  OKr2 = p[9];
  IKr2 = p[10];
}
