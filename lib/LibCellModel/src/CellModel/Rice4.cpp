/**@file Rice4.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <Rice4.h>

Rice4::Rice4(Rice4Parameters *pp) {
  r4p = pp;
  Init();
}

void Rice4::Init() {
  P0 = CELLMODEL_PARAMVALUE(VT_P0a);
  P1 = CELLMODEL_PARAMVALUE(VT_P1a);
  N1 = CELLMODEL_PARAMVALUE(VT_N1a);
  P2 = CELLMODEL_PARAMVALUE(VT_P2a);
  P3 = CELLMODEL_PARAMVALUE(VT_P3a);
  N0 = 1 - N1 - P0 - P1 - P2 - P3;
  TCa = CELLMODEL_PARAMVALUE(VT_TCaa);
}

ML_CalcType
Rice4::CalcTrop(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType TnCa,
                int euler = 1) {
  TCa = TnCa / (CELLMODEL_PARAMVALUE(VT_TCaMax));
  double SL_norm = (stretch - .85) * 3.333333333;
  double k_1 = pow((TCa * (1.0 + CELLMODEL_PARAMVALUE(VT_K_Ca) / (1.5 - SL_norm))), 5.0 + 3.0 * SL_norm);
  double g_sl = CELLMODEL_PARAMVALUE(VT_g_stern) * (1.0 + pow((double) (1 - SL_norm), 1.6));

  return ForceEulerNEuler(euler, tinc / euler, k_1, g_sl, stretch);
}

inline ML_CalcType
Rice4::Calc(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType &Ca,
            int euler = 1) {
  TCa -= tinc * (-Ca * CELLMODEL_PARAMVALUE(VT_k_on) * (1.0 - TCa) + CELLMODEL_PARAMVALUE(VT_k_off) * TCa);
  Ca -= tinc * (Ca * CELLMODEL_PARAMVALUE(VT_k_on) * (1.0 - TCa) - CELLMODEL_PARAMVALUE(VT_k_off) * TCa) * CELLMODEL_PARAMVALUE(VT_TCaMax);

  double SL_norm = (stretch - .85) * 3.333333333;
  double k_1 = pow((TCa * (1.0 + CELLMODEL_PARAMVALUE(VT_K_Ca) / (1.5 - SL_norm))), 5.0 + 3.0 * SL_norm);
  double g_sl = CELLMODEL_PARAMVALUE(VT_g_stern) * (1.0 + pow((double) (1 - SL_norm), 1.6));

  return ForceEulerNEuler(euler, tinc / euler, k_1, g_sl, stretch);
}

inline ML_CalcType
Rice4::ForceEulerNEuler(int r, ML_CalcType tinc, ML_CalcType k_1, ML_CalcType g_sl,
                        ML_CalcType stretch) {
  while (r--) {
    double g_10_sl = CELLMODEL_PARAMVALUE(VT_g_10) * g_sl;

    double kP0mN0 = CELLMODEL_PARAMVALUE(VT_k_m1) * P0 - k_1 * (1.0 - P0 - P1 - N1 - P2 - P3);
    double kP0mP1 = CELLMODEL_PARAMVALUE(VT_f_01) * P0 - g_10_sl * P1;
    double kP1mN1 = CELLMODEL_PARAMVALUE(VT_k_m1) * P1 - k_1 * N1;
    double kP1mP2 = CELLMODEL_PARAMVALUE(VT_f_12) * P1 - CELLMODEL_PARAMVALUE(VT_g_21) * g_sl * P2;
    double kP2mP3 = CELLMODEL_PARAMVALUE(VT_f_23) * P2 - CELLMODEL_PARAMVALUE(VT_g_32) * g_sl * P3;

    P0 += tinc * (-kP0mN0 - kP0mP1);
    P1 += tinc * (kP0mP1 - kP1mN1 - kP1mP2);
    N1 += tinc * (kP1mN1 - g_10_sl * N1);
    P2 += tinc * (kP1mP2 - kP2mP3);
    P3 += tinc * (kP2mP3);
  }
  return Overlap(stretch, r4p->getOverlapID(), r4p->getOverlapParameters()) *
         (P1 + N1 + P2 + P2 + 3.0 * P3) * CELLMODEL_PARAMVALUE(VT_dFmax);
}

void Rice4::steadyState(ML_CalcType Ca, ML_CalcType stretch) {
  ML_CalcType SL_norm = (stretch - .85) * 3.333333333;
  ML_CalcType f_01 = CELLMODEL_PARAMVALUE(VT_mfakt1) * CELLMODEL_PARAMVALUE(VT_f);
  ML_CalcType f_12 = CELLMODEL_PARAMVALUE(VT_mfakt2) * CELLMODEL_PARAMVALUE(VT_f);
  ML_CalcType f_23 = CELLMODEL_PARAMVALUE(VT_mfakt3) * CELLMODEL_PARAMVALUE(VT_f);
  ML_CalcType g_10stern =
      CELLMODEL_PARAMVALUE(VT_g_10) * CELLMODEL_PARAMVALUE(VT_g_stern) * (1 + pow((ML_CalcType) (1 - SL_norm), (ML_CalcType) 1.6));
  ML_CalcType g_21stern =
      CELLMODEL_PARAMVALUE(VT_g_21) * CELLMODEL_PARAMVALUE(VT_g_stern) * (1 + pow((ML_CalcType) (1 - SL_norm), (ML_CalcType) 1.6));
  ML_CalcType g_32stern =
      CELLMODEL_PARAMVALUE(VT_g_32) * CELLMODEL_PARAMVALUE(VT_g_stern) * (1 + pow((ML_CalcType) (1 - SL_norm), (ML_CalcType) 1.6));

  ML_CalcType k_1 = CELLMODEL_PARAMVALUE(VT_k_m1) * pow(ML_CalcType(
                                         CELLMODEL_PARAMVALUE(VT_k_on) * Ca * (1 + CELLMODEL_PARAMVALUE(VT_K_Ca) / (1.5 - SL_norm)) / (CELLMODEL_PARAMVALUE(VT_k_on) * Ca + CELLMODEL_PARAMVALUE(
                                             VT_k_off))),
                                     ML_CalcType(5 + 10 * (stretch - .85)));

  // cerr<<"k_1 "<<k_1<<" g_32stern "<<g_32stern<<" f_23 "<< f_23<<" SL_norm "<<SL_norm<<" f_01 "<< f_01<<endl;

  cout << Ca << ' ' <<
       Overlap(stretch, r4p->getOverlapID(),
               r4p->getOverlapParameters()) * CELLMODEL_PARAMVALUE(VT_dFmax) *
       (f_01 * g_21stern * g_32stern * k_1 * (CELLMODEL_PARAMVALUE(VT_k_m1) + g_10stern + k_1) +
        2 * f_01 * f_12 * g_32stern * k_1 * (g_10stern + k_1) + 3 * f_01 * f_12 * f_23 *
                                                                k_1 * (g_10stern + k_1)) /
       (g_10stern * g_21stern * g_32stern * (k_1 + CELLMODEL_PARAMVALUE(VT_k_m1)) * (g_10stern + k_1 + CELLMODEL_PARAMVALUE(VT_k_m1)) +
        f_01 * (g_10stern + k_1) *
        (f_12 * (f_23 + g_32stern) * k_1 + g_21stern * g_32stern * (k_1 + CELLMODEL_PARAMVALUE(VT_k_m1)))) << ' '
       << CELLMODEL_PARAMVALUE(VT_k_on) * Ca / (CELLMODEL_PARAMVALUE(VT_k_on) * Ca + CELLMODEL_PARAMVALUE(VT_k_off)) <<
       endl;
}

void Rice4::Print(ostream &tempstr) {
  tempstr << (1 - N1 - P0 - P1 - P2 - P3) << ' ' << N1 << ' '
          << P0 << ' ' << P1 << ' '
          << P2 << ' ' << P3 << ' '
          << (1 - TCa) << ' ' << TCa << ' ';
}

void Rice4::GetParameterNames(vector<string> &getpara) {
  const int numpara = 8;
  const string ParaNames[numpara] = {"N0", "N1", "P0", "P1", "P2", "P3", "T", "TCa"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
