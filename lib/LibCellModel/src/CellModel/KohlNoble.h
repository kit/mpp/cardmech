/*! \file KohlNoble.h
   \brief Implementation of Kohl-Noble model of fibroblast

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef KOHL_NOBLE_H
#define KOHL_NOBLE_H

#include <KohlNobleParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_KohlNobleParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pKNP->P[NS_KohlNobleParameters::a].value
#endif // ifdef HETERO


class KohlNoble : public vbElphyModel<ML_CalcType> {
public:
  KohlNobleParameters *pKNP;

  ML_CalcType dummy;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  KohlNoble(KohlNobleParameters *);

  KohlNoble() {}

  ~KohlNoble() {}

  virtual inline bool AddHeteroValue(string, double);

  virtual inline int GetSize(void) {
    return sizeof(KohlNoble) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(KohlNobleParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return &dummy; }

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vol); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.0; }

  virtual inline ML_CalcType GetVm() { return -.020; }

  virtual inline ML_CalcType GetCai() { return 0.0; }

  virtual inline ML_CalcType GetCao() { return 0.0; }

  virtual inline ML_CalcType GetNai() { return 0.0; }

  virtual inline ML_CalcType GetNao() { return 0.0; }

  virtual inline ML_CalcType GetKi() { return 0.0; }

  virtual inline ML_CalcType GetKo() { return 0.0; }

  virtual inline void SetCai(ML_CalcType val) {}

  virtual void Init() {}

  virtual void Print(ostream &, double, ML_CalcType);


  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);

  virtual int GetNumStatus() { return 0; }

  virtual void GetStatus(double *p) const {}

  virtual void SetStatus(const double *p) {}
}; // class KohlNoble

#endif // ifndef KOHL_NOBLE_H
