/**@file IyerMazhariWinslowCa.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowCa.h>

IyerMazhariWinslowCa::IyerMazhariWinslowCa(IyerMazhariWinslowCaParameters *pIMWArg) {
  pIMW = pIMWArg;
#ifdef HETERO
  PS = new ParameterSwitch(pIMW, NS_IyerMazhariWinslowCaParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

void IyerMazhariWinslowCa::Init() {
  C0L = CELLMODEL_PARAMVALUE(VT_C0L);
  C1L = CELLMODEL_PARAMVALUE(VT_C1L);
  C2L = CELLMODEL_PARAMVALUE(VT_C2L);
  C3L = CELLMODEL_PARAMVALUE(VT_C3L);
  C4L = CELLMODEL_PARAMVALUE(VT_C4L);
  OL = CELLMODEL_PARAMVALUE(VT_OL);
  CCa0L = CELLMODEL_PARAMVALUE(VT_CCa0L);
  CCa1L = CELLMODEL_PARAMVALUE(VT_CCa1L);
  CCa2L = CELLMODEL_PARAMVALUE(VT_CCa2L);
  CCa3L = CELLMODEL_PARAMVALUE(VT_CCa3L);
  CCa4L = CELLMODEL_PARAMVALUE(VT_CCa4L);
  y = CELLMODEL_PARAMVALUE(VT_y);
#ifdef TIMOTHY
  ytimothy = CELLMODEL_PARAMVALUE(VT_ytimothy);
#endif // ifdef TIMOTHY
#ifdef TIMOTHY2
  C0Ltimothy = CELLMODEL_PARAMVALUE(VT_C0Ltimothy);
  C1Ltimothy = CELLMODEL_PARAMVALUE(VT_C1Ltimothy);
  C2Ltimothy = CELLMODEL_PARAMVALUE(VT_C2Ltimothy);
  C3Ltimothy = CELLMODEL_PARAMVALUE(VT_C3Ltimothy);
  C4Ltimothy = CELLMODEL_PARAMVALUE(VT_C4Ltimothy);
  OLtimothy = CELLMODEL_PARAMVALUE(VT_OLtimothy);
  CCa0Ltimothy = CELLMODEL_PARAMVALUE(VT_CCa0Ltimothy);
  CCa1Ltimothy = CELLMODEL_PARAMVALUE(VT_CCa1Ltimothy);
  CCa2Ltimothy = CELLMODEL_PARAMVALUE(VT_CCa2Ltimothy);
  CCa3Ltimothy = CELLMODEL_PARAMVALUE(VT_CCa3Ltimothy);
  CCa4Ltimothy = CELLMODEL_PARAMVALUE(VT_CCa4Ltimothy);
  ytimothy = CELLMODEL_PARAMVALUE(VT_ytimothy);
#endif // ifdef TIMOTHY2
} // IyerMazhariWinslowCa::Init

void IyerMazhariWinslowCa::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' '
          << V << ' '
          << C0L << ' '
          << C1L << ' '
          << C2L << ' '
          << C3L << ' '
          << C4L << ' '
          << OL << ' '
          << CCa0L << ' '
          << CCa1L << ' '
          << CCa2L << ' '
          << CCa3L << ' '
          << CCa4L << ' '
          << y << ' '
          #ifdef TIMOTHY
          << ytimothy << ' '
          #endif // ifdef TIMOTHY
          #ifdef TIMOTHY2
          << C0Ltimothy << ' '
          << C1Ltimothy << ' '
          << C2Ltimothy << ' '
          << C3Ltimothy << ' '
          << C4Ltimothy << ' '
          << OLtimothy << ' '
          << CCa0Ltimothy << ' '
          << CCa1Ltimothy << ' '
          << CCa2Ltimothy << ' '
          << CCa3Ltimothy << ' '
          << CCa4Ltimothy << ' '
          << ytimothy << ' '
#endif // ifdef TIMOTHY2
      ;
} // IyerMazhariWinslowCa::Print

void IyerMazhariWinslowCa::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);

  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);

  if (V == 0.)
    V = 1e-8;

  const double F = ElphyModelConstants::F * 1000;
  const double RTdF = ElphyModelConstants::R * Tx / F;
  const double FdRT = 1. / RTdF;
  const double VFdRT = V * FdRT;


  const double exp2VFdRT = exp(2. * VFdRT);
  const double ICas =
      CELLMODEL_PARAMVALUE(VT_PCa) * 4. * VFdRT * F * (.001 * exp2VFdRT - 0.341 * CELLMODEL_PARAMVALUE(VT_Cao)) / (exp2VFdRT - 1.);

#ifdef TIMOTHY
  const double I_Ca = ICas*((1.-TIMOTHY)*y+TIMOTHY*ytimothy)*OL;
#else // ifdef TIMOTHY
# ifdef TIMOTHY2
  const double I_Ca = ICas * ((1. - TIMOTHY2) * y * OL + TIMOTHY2 * ytimothy * OLtimothy);
# else // ifdef TIMOTHY2
  const double I_Ca = ICas*y*OL;
# endif // ifdef TIMOTHY2
#endif // ifdef TIMOTHY
  tempstr << I_Ca << ' ';
} // IyerMazhariWinslowCa::LongPrint

void IyerMazhariWinslowCa::GetParameterNames(vector<string> &getpara) {
  const string ParaNames[] =
      {"C0L", "C1L", "C2L", "C3L", "C4L",
       "OL",
       "CCa0L", "CCa1L", "CCa2L",
       "CCa3L", "CCa4L", "y"
#ifdef TIMOTHY
          ,               "ytimothy"
#endif // ifdef TIMOTHY
#ifdef TIMOTHY2
          , "C0Ltimothy", "C1Ltimothy", "C2Ltimothy", "C3Ltimothy",
       "C4Ltimothy",
       "OLtimothy",
       "CCa0Ltimothy", "CCa1Ltimothy", "CCa2Ltimothy", "CCa3Ltimothy", "CCa4Ltimothy",
       "ytimothy"
#endif // ifdef TIMOTHY2
      };

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void IyerMazhariWinslowCa::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const string ParaNames[] = {"I_Ca"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType IyerMazhariWinslowCa::Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim = .0,
                                       ML_CalcType stretch = 1.,
                                       int euler = 1) {
  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);

  tinc *= 1000.0;

  if (V == 0.)
    V = 1e-8;
  const double V_int = V * 1000.0;

  const double CaLa = 1.997 * exp(0.012 * (V_int - 35));
  const double CaLb = 0.0882 * exp(-0.065 * (V_int - 22));
  const double CaLas = CaLa * CELLMODEL_PARAMVALUE(VT_CaLa);
  const double CaLbs = CaLb / (CELLMODEL_PARAMVALUE(VT_CaLb));
  const double CaLg = .0554 * CELLMODEL_PARAMVALUE(VT_Cai); // Assuming Cai==CaSS
  const double CaLo = CELLMODEL_PARAMVALUE(VT_CaLo);

  const double a1 = CELLMODEL_PARAMVALUE(VT_CaLa), b1 = CELLMODEL_PARAMVALUE(VT_CaLb);
  const double a2 = a1 * a1, b2 = b1 * b1;
  const double a3 = a2 * a1, b3 = b2 * b1;
  const double a4 = a3 * a1, b4 = b3 * b1;

  const double dC1L =
      -(3. * CaLa + CaLb + CaLg * a1) * C1L + 4. * CaLa * C0L + 2. * CaLb * C2L + CaLo / b1 * CCa1L;
  const double dC2L =
      -(2. * CaLa + 2. * CaLb + CaLg * a2) * C2L + 3. * CaLa * C1L + 3. * CaLb * C3L +
      CaLo / b2 * CCa2L;
  const double dC3L =
      -(CaLa + 3. * CaLb + CaLg * a3) * C3L + 2. * CaLa * C2L + 4. * CaLb * C4L + CaLo / b3 * CCa3L;
  const double dC4L = -(CELLMODEL_PARAMVALUE(VT_CaLf) + 4. * CaLb + CaLg * a4) * C4L + CaLa * C3L + CELLMODEL_PARAMVALUE(VT_CaLg) * OL +
                      CaLo / b4 * CCa4L;
  const double dOL = -CELLMODEL_PARAMVALUE(VT_CaLg) * OL + CELLMODEL_PARAMVALUE(VT_CaLf) * C4L;

  const double dCCa0L = -(4. * CaLas + CaLo) * CCa0L + CaLbs * CCa1L + CaLg * C0L;
  const double dCCa1L =
      -(3. * CaLas + CaLbs + CaLo / b1) * CCa1L + 4. * CaLas * CCa0L + 2. * CaLbs * CCa2L +
      CaLg * a1 * C1L;
  const double dCCa2L =
      -(2. * CaLas + 2. * CaLbs + CaLo / b2) * CCa2L + 3. * CaLas * CCa1L + 3. * CaLbs * CCa3L +
      CaLg * a2 * C2L;
  const double dCCa3L =
      -(CaLas + 3. * CaLbs + CaLo / b3) * CCa3L + 2. * CaLas * CCa2L + 4. * CaLbs * CCa4L +
      CaLg * a3 * C3L;
  const double dCCa4L = -(4. * CaLbs + CaLo / b4) * CCa4L + CaLas * CCa3L + +CaLg * a4 * C4L;

  C0L = 1. - C1L - C2L - C3L - C4L - OL - CCa0L - CCa1L - CCa2L - CCa3L - CCa4L;
  C1L += tinc * dC1L;
  C2L += tinc * dC2L;
  C3L += tinc * dC3L;
  C4L += tinc * dC4L;
  OL += tinc * dOL;
  CCa0L += tinc * dCCa0L;
  CCa1L += tinc * dCCa1L;
  CCa2L += tinc * dCCa2L;
  CCa3L += tinc * dCCa3L;
  CCa4L += tinc * dCCa4L;

  if (C0L < 0.)
    C0L = 0.;
  else if (C0L > 1.)
    C0L = 1.;
  if (C1L < 0.)
    C1L = 0.;
  else if (C1L > 1.)
    C1L = 1.;
  if (C2L < 0.)
    C2L = 0.;
  else if (C2L > 1.)
    C2L = 1.;
  if (C3L < 0.)
    C3L = 0.;
  else if (C3L > 1.)
    C3L = 1.;
  if (C4L < 0.)
    C4L = 0.;
  else if (C4L > 1.)
    C4L = 1.;
  if (CCa0L < 0.)
    CCa0L = 0.;
  else if (CCa0L > 1.)
    CCa0L = 1.;
  if (CCa1L < 0.)
    CCa1L = 0.;
  else if (CCa1L > 1.)
    CCa1L = 1.;
  if (CCa2L < 0.)
    CCa2L = 0.;
  else if (CCa2L > 1.)
    CCa2L = 1.;
  if (CCa3L < 0.)
    CCa3L = 0.;
  else if (CCa3L > 1.)
    CCa3L = 1.;
  if (CCa4L < 0.)
    CCa4L = 0.;
  else if (CCa4L > 1.)
    CCa4L = 1.;


#ifdef TIMOTHY2
  {
    const double CaLa = 1.997 * exp(0.012 * (V_int - 35 - CELLMODEL_PARAMVALUE(VT_CaLShift) * 1000));
    const double CaLb = 0.0882 * exp(-0.065 * (V_int - 22 - CELLMODEL_PARAMVALUE(VT_CaLShift) * 1000));
    const double CaLas = CaLa * CELLMODEL_PARAMVALUE(VT_CaLa);
    const double CaLbs = CaLb / (CELLMODEL_PARAMVALUE(VT_CaLb));
    const double CaLg = .0554 * CELLMODEL_PARAMVALUE(VT_Cai); // Assuming Cai==CaSS
    const double CaLo = CELLMODEL_PARAMVALUE(VT_CaLo);

    const double dC1L = -(3. * CaLa + CaLb + CaLg * a1) * C1Ltimothy + 4. * CaLa * C0Ltimothy +
                        2. * CaLb * C2Ltimothy + CaLo / b1 *
                                                 CCa1Ltimothy;
    const double dC2L = -(2. * CaLa + 2. * CaLb + CaLg * a2) * C2Ltimothy + 3. * CaLa * C1Ltimothy +
                        3. * CaLb * C3Ltimothy + CaLo / b2 *
                                                 CCa2Ltimothy;
    const double dC3L = -(CaLa + 3. * CaLb + CaLg * a3) * C3Ltimothy + 2. * CaLa * C2Ltimothy +
                        4. * CaLb * C4Ltimothy + CaLo / b3 *
                                                 CCa3Ltimothy;
    const double dC4L = -(CELLMODEL_PARAMVALUE(VT_CaLf) + 4. * CaLb + CaLg * a4) * C4Ltimothy + CaLa * C3Ltimothy +
                        CELLMODEL_PARAMVALUE(VT_CaLg) * OLtimothy + CaLo / b4 *
                                                 CCa4Ltimothy;
    const double dOL = -CELLMODEL_PARAMVALUE(VT_CaLg) * OLtimothy + CELLMODEL_PARAMVALUE(VT_CaLf) * C4Ltimothy;

    const double dCCa0L = -(4. * CaLas + CaLo) * CCa0Ltimothy + CaLbs * CCa1Ltimothy +
                          CaLg * C0Ltimothy;
    const double dCCa1L =
        -(3. * CaLas + CaLbs + CaLo / b1) * CCa1Ltimothy + 4. * CaLas * CCa0Ltimothy +
        2. * CaLbs * CCa2Ltimothy +
        CaLg * a1 * C1Ltimothy;
    const double dCCa2L =
        -(2. * CaLas + 2. * CaLbs + CaLo / b2) * CCa2Ltimothy + 3. * CaLas * CCa1Ltimothy +
        3. * CaLbs * CCa3Ltimothy +
        CaLg * a2 * C2Ltimothy;
    const double dCCa3L =
        -(CaLas + 3. * CaLbs + CaLo / b3) * CCa3Ltimothy + 2. * CaLas * CCa2Ltimothy +
        4. * CaLbs * CCa4Ltimothy + CaLg *
                                    a3 * C3Ltimothy;
    const double dCCa4L =
        -(4. * CaLbs + CaLo / b4) * CCa4Ltimothy + CaLas * CCa3Ltimothy + +CaLg * a4 *
                                                                          C4Ltimothy;

    C1Ltimothy += tinc * dC1L;
    C2Ltimothy += tinc * dC2L;
    C3Ltimothy += tinc * dC3L;
    C4Ltimothy += tinc * dC4L;
    OLtimothy += tinc * dOL;
    CCa0Ltimothy += tinc * dCCa0L;
    CCa1Ltimothy += tinc * dCCa1L;
    CCa2Ltimothy += tinc * dCCa2L;
    CCa3Ltimothy += tinc * dCCa3L;
    CCa4Ltimothy += tinc * dCCa4L;
    C0Ltimothy = 1. - C1Ltimothy - C2Ltimothy - C3Ltimothy - C4Ltimothy - OLtimothy - CCa0Ltimothy -
                 CCa1Ltimothy - CCa2Ltimothy -
                 CCa3Ltimothy - CCa4Ltimothy;

    if (C0Ltimothy < 0.)
      C0Ltimothy = 0.;
    else if (C0Ltimothy > 1.)
      C0Ltimothy = 1.;
    if (C1Ltimothy < 0.)
      C1Ltimothy = 0.;
    else if (C1Ltimothy > 1.)
      C1Ltimothy = 1.;
    if (C2Ltimothy < 0.)
      C2Ltimothy = 0.;
    else if (C2Ltimothy > 1.)
      C2Ltimothy = 1.;
    if (C3Ltimothy < 0.)
      C3Ltimothy = 0.;
    else if (C3Ltimothy > 1.)
      C3Ltimothy = 1.;
    if (C4Ltimothy < 0.)
      C4Ltimothy = 0.;
    else if (C4Ltimothy > 1.)
      C4Ltimothy = 1.;
    if (CCa0Ltimothy < 0.)
      CCa0Ltimothy = 0.;
    else if (CCa0Ltimothy > 1.)
      CCa0Ltimothy = 1.;
    if (CCa1Ltimothy < 0.)
      CCa1Ltimothy = 0.;
    else if (CCa1Ltimothy > 1.)
      CCa1Ltimothy = 1.;
    if (CCa2Ltimothy < 0.)
      CCa2Ltimothy = 0.;
    else if (CCa2Ltimothy > 1.)
      CCa2Ltimothy = 1.;
    if (CCa3Ltimothy < 0.)
      CCa3Ltimothy = 0.;
    else if (CCa3Ltimothy > 1.)
      CCa3Ltimothy = 1.;
    if (CCa4Ltimothy < 0.)
      CCa4Ltimothy = 0.;
    else if (CCa4Ltimothy > 1.)
      CCa4Ltimothy = 1.;
  }
#endif // ifdef TIMOTHY2

  const double Vs = V + CELLMODEL_PARAMVALUE(VT_ShiftVm); // surface charge effect is reported only for inactivation
  const double Vs_int = (Vs ? Vs * 1000.0 : 1.e-8);

  //    double ty=1./(.00653/(.5+exp(-Vs_int/7.1))+.00512*exp(-Vs_int/39.8)); // rev 1.0

  double ty =
      1. / (.003363 / (.5 + exp(-V_int / 5.5389)) + .00779 * exp(-V_int / 49.51));  // rev 1.03

#ifdef TIMOTHY
  const double yinf = .985/(1.+exp((Vs_int+30)/ (7)))  // fit to Splawski 2004
    +.1/(1.+exp((-Vs_int+39)/ (20)))
    -0.021;
#else // ifdef TIMOTHY
# ifdef TIMOTHY2
  const double yinf = .9 / (1. + exp((V_int + 28.5) / 7.8)) + .1;
# else // ifdef TIMOTHY2
  const double yinf = .82/(1.+exp((Vs_int+28.5)/7.8))+.18; // original Winslow
# endif // ifdef TIMOTHY2
#endif // ifdef TIMOTHY

  assert(yinf >= 0. && yinf <= 1.);
  y += tinc * (yinf - y) / ty;
  assert(y >= 0. && y <= 1.);

#ifdef TIMOTHY
  const double yinftimothy =
    CELLMODEL_PARAMVALUE(VT_y1)/(1.+exp((Vs_int+CELLMODEL_PARAMVALUE(VT_y2))/ (CELLMODEL_PARAMVALUE(VT_y3))))
    +CELLMODEL_PARAMVALUE(VT_y4)/(1.+exp((-Vs_int+CELLMODEL_PARAMVALUE(VT_y5))/ (CELLMODEL_PARAMVALUE(VT_y6))))
    +CELLMODEL_PARAMVALUE(VT_y7);
  assert(yinftimothy >= 0. && yinftimothy <= 1.);

  ytimothy += tinc*(yinftimothy-ytimothy)/ty;
  assert(ytimothy >= 0. && ytimothy <= 1.);
#endif // ifdef TIMOTHY

#ifdef TIMOTHY2
  const double yinftimothy =
      CELLMODEL_PARAMVALUE(VT_y1) / (1. + exp((Vs_int + CELLMODEL_PARAMVALUE(VT_y2)) / (CELLMODEL_PARAMVALUE(VT_y3)))) + 1 - CELLMODEL_PARAMVALUE(VT_y1);
  assert(yinftimothy >= 0. && yinftimothy <= 1.);
  ty *= CELLMODEL_PARAMVALUE(VT_tytimothyscale);
  ytimothy += tinc * (yinftimothy - ytimothy) / ty;
  assert(ytimothy >= 0. && ytimothy <= 1.);
#endif // ifdef TIMOTHY2

  return 0.;
} // IyerMazhariWinslowCa::Calc
