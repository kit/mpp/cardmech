/**@file HodgkinHuxley.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <kaMachineOS.h>

namespace HodginHuxleyNS {
  const float G_L = 0.3;
  const float G_K_max = 36;
  const float G_Na_max = 120;
  const float C_m = 1;

  const float V_r = -65;
  const float V_L = V_r + 10.613;
  const float V_K = V_r - 12;
  const float V_Na = V_r + 115;
  const float V_m__ = 0;
  const float V_m = V_r + 50;
}

template<class T>
class HodginHuxley {
public:
  T V;
  T m, n, h;

  HodginHuxley() {}

  void Init() {
    using namespace HodginHuxleyNS;
    V = V_r;
    m = 0.0529347;
    n = 0.317711;
    h = 0.596385;
  }

  void Calc(double tinc, T i_external = 0) {
    using namespace HodginHuxleyNS;
    T V__ = V - V_r;

    T a_m = (25 == V__ ? 1 : .1 * (25 - V__) / (exp(.1 * (25 - V__)) - 1));
    T b_m = 4 * exp(-V__ / 18);
    T a_h = .07 / exp(V__ / 20);
    T b_h = 1 / (exp((30 - V__) / 10) + 1);
    T a_n = (10 == V__ ? 1 : .01 * (10 - V__) / (exp((10 - V__) / 10) - 1));
    T b_n = 0.125 / exp(V__ / 80);

    m += tinc * (a_m * (1 - m) - b_m * m);
    h += tinc * (a_h * (1 - h) - b_h * h);
    n += tinc * (a_n * (1 - n) - b_n * n);

    T G_Na = G_Na_max * m * m * m * h;
    T G_K = G_K_max * n * n * n * n;

    V -= tinc / C_m * ((V - V_Na) * G_Na + (V - V_K) * G_K + (V - V_L) * G_L - i_external);
  }

  void Print(T t) {
    using namespace HodginHuxleyNS;
    T G_Na = G_Na_max * m * m * m * h;
    T G_K = G_K_max * n * n * n * n;

    cout << t << ' ' << V;
    cout << ' ' << m << ' ' << h << ' ' << n;
    cout << ' ' << G_Na << ' ' << G_K << ' ' << G_L;
    cout << ' ' << (V - V_Na) * G_Na << ' ' << (V - V_K) * G_K << ' ' << (V - V_L) * G_L;
    cout << '\n';
  }
}; // class HodginHuxley

int main(int argc, char *argv[]) {
  float t, tend = 25, tinc = .0002, tverg = 5, amplitude = 25, toutbegin = 0;
  float text = tend, textlen = 1, textdiv = 1;
  int tout = 0, toutinc = 10;

  for (int ac = 1; ac < argc; ac++) {
    if (!strcmp("-tend", argv[ac]) && (ac + 1 < argc)) {
      tend = atof(argv[++ac]);
    } else if (!strcmp("-tinc", argv[ac]) && (ac + 1 < argc)) {
      tinc = atof(argv[++ac]);
    } else if (!strcmp("-toutbegin", argv[ac]) && (ac + 1 < argc)) {
      toutbegin = atof(argv[++ac]);
    } else if (!strcmp("-toutinc", argv[ac]) && (ac + 1 < argc)) {
      toutinc = atoi(argv[++ac]);
    } else if (!strcmp("-text", argv[ac]) && (ac + 1 < argc)) {
      text = atof(argv[++ac]);
    } else if (!strcmp("-textlen", argv[ac]) && (ac + 1 < argc)) {
      textlen = atof(argv[++ac]);
    } else if (!strcmp("-textdiv", argv[ac]) && (ac + 1 < argc)) {
      textdiv = atof(argv[++ac]);
    } else if (!strcmp("-amplitude", argv[ac]) && (ac + 1 < argc)) {
      amplitude = atof(argv[++ac]);
    } else {
      fprintf(stderr, "Error: Unknown or incomplete option %s\n", argv[ac]);
      exit(-1);
    }
  }
  HodginHuxley<float> HH;
  HH.Init();

  for (t = 0; t < tend; t += tinc) {
    float i_external = 0.0;
    if (t >= tverg) {
      i_external = amplitude;
      if (t >= tverg + textlen) {
        text /= textdiv;
        tverg += text;
      }
    }
    HH.Calc(tinc, i_external);

    if (t >= toutbegin) {
      if (tout == 0)
        HH.Print(t);
      tout++;
      if (tout >= toutinc)
        tout = 0;
    }
  }
} // main
