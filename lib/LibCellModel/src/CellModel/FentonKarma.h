/*      File: FentonKarma.h
    automatically created by ExtractParameterClass.pl - done by dw (22.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

/*! FentonKarma
 *  \brief FentonKarma Cell Model
 *  \author Manuel Ifland
 *  \version 1.1
 *  \date 15.03.2007
 *
 *  This is the FentonKarma Cell Model.
 */

#ifndef FENTONKARMA
#define FENTONKARMA

#include <FentonKarmaParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_FentonKarmaParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pCmP->P[NS_FentonKarmaParameters::a].value
#endif // ifdef HETERO

class FentonKarma : public vbElphyModel<ML_CalcType> {
public:
  FentonKarmaParameters *pCmP;
  ML_CalcType u, v, w;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  FentonKarma(FentonKarmaParameters *pp);

  ~FentonKarma();

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType Volume() { return 2.89529179 * 10e-13; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp);  /*0.15;*/ }

  virtual inline ML_CalcType GetStimTime() { return 0.002; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_Vm_init); }

  virtual inline ML_CalcType GetCai() { return 0; }

  virtual inline ML_CalcType GetCao() { return 0; }

  virtual inline ML_CalcType GetNai() { return 0; }

  virtual inline ML_CalcType GetNao() { return 0; }

  virtual inline ML_CalcType GetKi() { return 0; }

  virtual inline ML_CalcType GetKo() { return 0; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &u; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                           int euler);

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class FentonKarma
#endif // ifndef FENTONKARMA
