/**@file Rice5.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <Rice5.h>

Rice5::Rice5(Rice5Parameters *pp) {
  r5p = pp;
  Init();
}

void Rice5::Init() {
  P0 = CELLMODEL_PARAMVALUE(VT_P0a);
  P1 = CELLMODEL_PARAMVALUE(VT_P1a);
  N1 = CELLMODEL_PARAMVALUE(VT_N1a);
  N0 = 1 - N1 - P0 - P1 - P2 - P3;
  P2 = CELLMODEL_PARAMVALUE(VT_P2a);
  P3 = CELLMODEL_PARAMVALUE(VT_P3a);

  TCa = CELLMODEL_PARAMVALUE(VT_TCaa);
}

ML_CalcType
Rice5::CalcTrop(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType TnCa,
                int euler = 1) {
  TCa = TnCa /
        (CELLMODEL_PARAMVALUE(VT_TCaMax));  // Here the feedback mechanism (that the force influences the affinity of troponin for
  // calcium) is lost !!!
  return ForceEulerNEuler(euler, tinc / euler, stretch);
}

inline ML_CalcType
Rice5::Calc(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType &Ca,
            int euler = 1) {
  const double Force = ForceEulerNEuler(euler, tinc / euler, stretch);

  TCa -=
      tinc * (-Ca * CELLMODEL_PARAMVALUE(VT_k_on) * (1.0 - TCa) + CELLMODEL_PARAMVALUE(VT_k_off) * (1.0 - CELLMODEL_PARAMVALUE(VT_gewForce) * Force) * TCa);
  Ca -=
      tinc * (Ca * CELLMODEL_PARAMVALUE(VT_k_on) * (1.0 - TCa) - CELLMODEL_PARAMVALUE(VT_k_off) * (1.0 - CELLMODEL_PARAMVALUE(VT_gewForce) * Force) * TCa) *
      CELLMODEL_PARAMVALUE(VT_TCaMax);
  return Force;
}

inline ML_CalcType Rice5::ForceEulerNEuler(int r, ML_CalcType tinc, ML_CalcType stretch) {
  const double SL_norm = (stretch - .85) * 3.333333333;
  const double k_1 = CELLMODEL_PARAMVALUE(VT_k_m1) * pow((TCa * (1.0 + CELLMODEL_PARAMVALUE(VT_K_Ca) / (1.8 - SL_norm))),
                                      CELLMODEL_PARAMVALUE(VT_N_1) + CELLMODEL_PARAMVALUE(VT_N_2) * SL_norm);
  const double g_sl = CELLMODEL_PARAMVALUE(VT_g_stern) * (1.0 + pow((double) (1.0 - SL_norm), 1.6));

  while (r--) {
    const double kP0mN0 = CELLMODEL_PARAMVALUE(VT_k_m1) * P0 - k_1 * (1.0 - P0 - P1 - N1 - P2 - P3);
    const double kP0mP1 = CELLMODEL_PARAMVALUE(VT_f_01) * P0 - CELLMODEL_PARAMVALUE(VT_g_10) * g_sl * P1;
    const double kP1mN1 = CELLMODEL_PARAMVALUE(VT_k_m1) * P1 - k_1 * N1;
    const double kP1mP2 = CELLMODEL_PARAMVALUE(VT_f_12) * P1 - CELLMODEL_PARAMVALUE(VT_g_21) * g_sl * P2;
    const double kP2mP3 = CELLMODEL_PARAMVALUE(VT_f_23) * P2 - CELLMODEL_PARAMVALUE(VT_g_32) * g_sl * P3;

    P0 += tinc * (-kP0mN0 - kP0mP1);
    P1 += tinc * (kP0mP1 - kP1mN1 - kP1mP2);
    N1 += tinc * (kP1mN1 - CELLMODEL_PARAMVALUE(VT_g_10) * g_sl * N1);
    P2 += tinc * (kP1mP2 - kP2mP3);
    P3 += tinc * (kP2mP3);
  }
  return Overlap(stretch, r5p->getOverlapID(), r5p->getOverlapParameters()) *
         (P1 + N1 + P2 + P2 + 3.0 * P3) * CELLMODEL_PARAMVALUE(VT_dFmax);
}

void Rice5::Print(ostream &tempstr) {
  tempstr << (1 - N1 - P0 - P1 - P2 - P3) << ' '
          << N1 << ' ' << P0 << ' ' << P1 << ' '
          << P2 << ' ' << P3 << ' ' << (1 - TCa) << ' '
          << TCa << ' ';
}

void Rice5::GetParameterNames(vector<string> &getpara) {
  const int numpara = 8;
  const string ParaNames[numpara] = {"N0", "N1", "P0", "P1", "P2", "P3", "T", "TCa"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
