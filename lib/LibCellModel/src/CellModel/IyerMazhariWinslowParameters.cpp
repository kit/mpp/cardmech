/**@file IyerMazhariWinslowParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowParameters.h>

IyerMazhariWinslowParameters::IyerMazhariWinslowParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void IyerMazhariWinslowParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "IyerMazhariWinslowParameters:init " << initFile << endl;
#endif // if KADEBUG

  // Initialization of parameter names
  P[VT_Tx].name = "Tx";
  P[VT_Acap].name = "Acap";
  P[VT_Vmyo].name = "Vmyo";
  P[VT_VJSR].name = "VJSR";
  P[VT_VNSR].name = "VNSR";
  P[VT_VSS].name = "VSS";
  P[VT_CSC].name = "CSC";

  P[VT_Nao].name = "Nao";
  P[VT_Ko].name = "Ko";
  P[VT_Cao].name = "Cao";
  P[VT_Nai].name = "Nai";
  P[VT_Ki].name = "Ki";
  P[VT_Cai].name = "Cai";
  P[VT_CaNSR].name = "CaNSR";
  P[VT_CaSS].name = "CaSS";
  P[VT_CaJSR].name = "CaJSR";
  P[VT_PC1].name = "PC1";
  P[VT_PO1].name = "PO1";
  P[VT_PC2].name = "PC2";
  P[VT_PO2].name = "PO2";
  P[VT_C0L].name = "C0L";
  P[VT_C1L].name = "C1L";
  P[VT_C2L].name = "C2L";
  P[VT_C3L].name = "C3L";
  P[VT_C4L].name = "C4L";
  P[VT_OL].name = "OL";
  P[VT_CCa0L].name = "CCa0L";
  P[VT_CCa1L].name = "CCa1L";
  P[VT_CCa2L].name = "CCa2L";
  P[VT_CCa3L].name = "CCa3L";
  P[VT_CCa4L].name = "CCa4L";
  P[VT_y].name = "y";
#ifdef TIMOTHY
  P[VT_ytimothy].name = "ytimothy";
#endif // ifdef TIMOTHY
#ifdef TIMOTHY2
  P[VT_CaLShift].name = "CaLShift";
  P[VT_C0Ltimothy].name = "C0Ltimothy";
  P[VT_C1Ltimothy].name = "C1Ltimothy";
  P[VT_C2Ltimothy].name = "C2Ltimothy";
  P[VT_C3Ltimothy].name = "C3Ltimothy";
  P[VT_C4Ltimothy].name = "C4Ltimothy";
  P[VT_OLtimothy].name = "OLtimothy";
  P[VT_CCa0Ltimothy].name = "CCa0Ltimothy";
  P[VT_CCa1Ltimothy].name = "CCa1Ltimothy";
  P[VT_CCa2Ltimothy].name = "CCa2Ltimothy";
  P[VT_CCa3Ltimothy].name = "CCa3Ltimothy";
  P[VT_CCa4Ltimothy].name = "CCa4Ltimothy";
  P[VT_ytimothy].name = "ytimothy";
  P[VT_y1].name = "y1";
  P[VT_y2].name = "y2";
  P[VT_y3].name = "y3";
  P[VT_tytimothyscale].name = "tytimothyscale";
  P[VT_PCatimothy].name = "PCatimothy";
#endif // ifdef TIMOTHY2

  P[VT_HTRPNCa].name = "HTRPNCa";
  P[VT_LTRPNCa].name = "LTRPNCa";

  P[VT_C0Kvf].name = "C0Kvf";
  P[VT_C1Kvf].name = "C1Kvf";
  P[VT_C2Kvf].name = "C2Kvf";
  P[VT_C3Kvf].name = "C3Kvf";
  P[VT_OKvf].name = "OKvf";
  P[VT_CI0Kvf].name = "CI0Kvf";
  P[VT_CI1Kvf].name = "CI1Kvf";
  P[VT_CI2Kvf].name = "CI2Kvf";
  P[VT_CI3Kvf].name = "CI3Kvf";
  P[VT_OIKvf].name = "OIKvf";

  P[VT_C0Kvs].name = "C0Kvs";
  P[VT_C1Kvs].name = "C1Kvs";
  P[VT_C2Kvs].name = "C2Kvs";
  P[VT_C3Kvs].name = "C3Kvs";
  P[VT_OKvs].name = "OKvs";
  P[VT_CI0Kvs].name = "CI0Kvs";
  P[VT_CI1Kvs].name = "CI1Kvs";
  P[VT_CI2Kvs].name = "CI2Kvs";
  P[VT_CI3Kvs].name = "CI3Kvs";
  P[VT_OIKvs].name = "OIKvs";

  P[VT_C1Kr].name = "C1Kr";
  P[VT_C2Kr].name = "C2Kr";
  P[VT_C3Kr].name = "C3Kr";
  P[VT_OKr].name = "OKr";
  P[VT_IKr].name = "IKr";

  P[VT_C0Ks].name = "C0Ks";
  P[VT_C1Ks].name = "C1Ks";
  P[VT_O1Ks].name = "O1Ks";
  P[VT_O2Ks].name = "O2Ks";

  P[VT_C0Na].name = "C0Na";
  P[VT_C1Na].name = "C1Na";
  P[VT_C2Na].name = "C2Na";
  P[VT_C3Na].name = "C3Na";
  P[VT_C4Na].name = "C4Na";
  P[VT_O1Na].name = "O1Na";
  P[VT_O2Na].name = "O2Na";
  P[VT_CI0Na].name = "CI0Na";
  P[VT_CI1Na].name = "CI1Na";
  P[VT_CI2Na].name = "CI2Na";
  P[VT_CI3Na].name = "CI3Na";
  P[VT_CI4Na].name = "CI4Na";
  P[VT_INa].name = "INa";

  P[VT_GNa].name = "GNa";
  P[VT_GNab].name = "GNab";
  P[VT_GKr].name = "GKr";
  P[VT_GKs].name = "GKs";
  P[VT_GKv43].name = "GKv43";
  P[VT_PKv14].name = "PKv14";
  P[VT_GK1].name = "GK1";

  P[VT_Naa1].name = "Naa1";
  P[VT_Naa2].name = "Naa2";
  P[VT_Naa3].name = "Naa3";
  P[VT_Nab1].name = "Nab1";
  P[VT_Nab2].name = "Nab2";
  P[VT_Nab3].name = "Nab3";
  P[VT_Nag1].name = "Nag1";
  P[VT_Nag2].name = "Nag2";
  P[VT_Nag3].name = "Nag3";
  P[VT_Nad1].name = "Nad1";
  P[VT_Nad2].name = "Nad2";
  P[VT_Nad3].name = "Nad3";
  P[VT_NaOn1].name = "NaOn1";
  P[VT_NaOn2].name = "NaOn2";
  P[VT_NaOn3].name = "NaOn3";
  P[VT_NaOf1].name = "NaOf1";
  P[VT_NaOf2].name = "NaOf2";
  P[VT_NaOf3].name = "NaOf3";
  P[VT_Nagg1].name = "Nagg1";
  P[VT_Nagg2].name = "Nagg2";
  P[VT_Nagg3].name = "Nagg3";
  P[VT_Nadd1].name = "Nadd1";
  P[VT_Nadd2].name = "Nadd2";
  P[VT_Nadd3].name = "Nadd3";
  P[VT_Nae1].name = "Nae1";
  P[VT_Nae2].name = "Nae2";
  P[VT_Nae3].name = "Nae3";
  P[VT_NaO1].name = "NaO1";
  P[VT_NaO2].name = "NaO2";
  P[VT_NaO3].name = "NaO3";
  P[VT_Naeta1].name = "Naeta1";
  P[VT_Naeta2].name = "Naeta2";
  P[VT_Naeta3].name = "Naeta3";
  P[VT_Nanu1].name = "Nanu1";
  P[VT_Nanu2].name = "Nanu2";
  P[VT_Nanu3].name = "Nanu3";
  P[VT_NaCn1].name = "NaCn1";
  P[VT_NaCn2].name = "NaCn2";
  P[VT_NaCn3].name = "NaCn3";
  P[VT_NaCf1].name = "NaCf1";
  P[VT_NaCf2].name = "NaCf2";
  P[VT_NaCf3].name = "NaCf3";
  P[VT_NaScalinga].name = "NaScalinga";
  P[VT_NaQ].name = "NaQ";

  P[VT_Kra0].name = "Kra0";
  P[VT_Krb0].name = "Krb0";
  P[VT_Kra1].name = "Kra1";
  P[VT_Krb1].name = "Krb1";
  P[VT_Krai].name = "Krai";
  P[VT_Krbi].name = "Krbi";
  P[VT_Krai3].name = "Krai3";
  P[VT_Krkf].name = "Krkf";
  P[VT_Krkb].name = "Krkb";

  P[VT_Ksa].name = "Ksa";
  P[VT_Ksb].name = "Ksb";
  P[VT_Ksg].name = "Ksg";
  P[VT_Ksd].name = "Ksd";
  P[VT_Kse].name = "Kse";
  P[VT_Kso].name = "Kso";

  P[VT_Kv43aa].name = "Kv43aa";
  P[VT_Kv43ba].name = "Kv43ba";
  P[VT_Kv43ai].name = "Kv43ai";
  P[VT_Kv43bi].name = "Kv43bi";
  P[VT_Kv43f1].name = "Kv43f1";
  P[VT_Kv43f2].name = "Kv43f2";
  P[VT_Kv43f3].name = "Kv43f3";
  P[VT_Kv43f4].name = "Kv43f4";
  P[VT_Kv43b1].name = "Kv43b1";
  P[VT_Kv43b2].name = "Kv43b2";
  P[VT_Kv43b3].name = "Kv43b3";
  P[VT_Kv43b4].name = "Kv43b4";

  P[VT_Kv14aa].name = "Kv14aa";
  P[VT_Kv14ba].name = "Kv14ba";
  P[VT_Kv14ai].name = "Kv14ai";
  P[VT_Kv14bi].name = "Kv14bi";
  P[VT_Kv14f1].name = "Kv14f1";
  P[VT_Kv14f2].name = "Kv14f2";
  P[VT_Kv14f3].name = "Kv14f3";
  P[VT_Kv14f4].name = "Kv14f4";
  P[VT_Kv14b1].name = "Kv14b1";
  P[VT_Kv14b2].name = "Kv14b2";
  P[VT_Kv14b3].name = "Kv14b3";
  P[VT_Kv14b4].name = "Kv14b4";

  P[VT_P1K1].name = "P1K1";
  P[VT_P2K1].name = "P2K1";

  P[VT_KNaCa].name = "KNaCa";
  P[VT_KmNa].name = "KmNa";
  P[VT_KmCa].name = "KmCa";
  P[VT_ksat].name = "ksat";
  P[VT_NaCaeta].name = "NaCaeta";
  P[VT_kNaK].name = "kNaK";
  P[VT_KmNai].name = "KmNai";
  P[VT_KmKo].name = "KmKo";

  P[VT_IpCa].name = "IpCa";
  P[VT_KmpCa].name = "KmpCa";
  P[VT_GCab].name = "GCab";

  P[VT_CaLf].name = "CaLf";
  P[VT_CaLg].name = "CaLg";
  P[VT_CaLa].name = "CaLa";
  P[VT_CaLb].name = "CaLb";
  P[VT_CaLo].name = "CaLo";
  P[VT_PScale].name = "PScale";
  P[VT_PCa].name = "PCa";
  P[VT_PK].name = "PK";
  P[VT_ICahalf].name = "ICahalf";

  P[VT_SRKap].name = "SRKap";
  P[VT_SRKam].name = "SRKam";
  P[VT_SRKbp].name = "SRKbp";
  P[VT_SRKbm].name = "SRKbm";
  P[VT_SRKcp].name = "SRKcp";
  P[VT_SRKcm].name = "SRKcm";
  P[VT_SRn].name = "SRn";
  P[VT_SRm].name = "SRm";
  P[VT_SRv1].name = "SRv1";
  P[VT_SRKfb].name = "SRKfb";
  P[VT_SRNfb].name = "SRNfb";
  P[VT_SRKrb].name = "SRKrb";
  P[VT_SRNrb].name = "SRNrb";
  P[VT_SRvmaxf].name = "SRvmaxf";
  P[VT_SRvmaxr].name = "SRvmaxr";
  P[VT_SRKSR].name = "SRKSR";

  P[VT_ttr].name = "ttr";
  P[VT_txfer].name = "txfer";
  P[VT_HTRPNtot].name = "HTRPNtot";
  P[VT_LTRPNtot].name = "LTRPNtot";
  P[VT_KHTRPNp].name = "KHTRPNp";
  P[VT_KHTRPNm].name = "KHTRPNm";
  P[VT_KLTRPNp].name = "KLTRPNp";
  P[VT_KLTRPNm].name = "KLTRPNm";
  P[VT_KmCMDN].name = "KmCMDN";
  P[VT_KmCSQN].name = "KmCSQN";
  P[VT_KmEGTA].name = "KmEGTA";
  P[VT_EGTAtot].name = "EGTAtot";
  P[VT_CMDNtot].name = "CMDNtot";
  P[VT_CSQNtot].name = "CSQNtot";
  P[VT_Amp].name = "Amp";

  ParameterLoader EPL(initFile, EMT_IyerMazhariWinslow);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
} // IyerMazhariWinslowParameters::Init

void IyerMazhariWinslowParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "IyerMazhariWinslowParameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void IyerMazhariWinslowParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
}
