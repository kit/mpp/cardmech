/*      File: PuglisiBers.cpp
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
        Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
        send comments to dw@ibt.uka.de  */

// typical parameters for ElphyModelTest
// tinc 1e-6
// textlen 5e-3
// textbegin 0.02

#include <PuglisiBers.h>

PuglisiBers::PuglisiBers(PuglisiBersParameters *pp) {
  pP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pP, NS_PuglisiBersParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

PuglisiBers::~PuglisiBers() {}

#ifdef HETERO

inline bool PuglisiBers::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool PuglisiBers::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int PuglisiBers::GetSize(void) {
  return (&Ca_int_Jrel - &Na_i + 1) * sizeof(ML_CalcType);
}

void PuglisiBers::Init() {
#if KADEBUG
  cerr << "PuglisiBers::Init()\n";
#endif // if KADEBUG
  Na_i = CELLMODEL_PARAMVALUE(VT_Na_i);
  Na_o = CELLMODEL_PARAMVALUE(VT_Na_o);
  Nab = CELLMODEL_PARAMVALUE(VT_Na_b);
  K_o = CELLMODEL_PARAMVALUE(VT_K_o);
  K_b = CELLMODEL_PARAMVALUE(VT_K_b);
  K_i = CELLMODEL_PARAMVALUE(VT_K_i);
  Ca_o = CELLMODEL_PARAMVALUE(VT_Ca_o);
  Ca_i = CELLMODEL_PARAMVALUE(VT_Ca_i);
  Cl_o = CELLMODEL_PARAMVALUE(VT_Cl_o);
  Cl_i = CELLMODEL_PARAMVALUE(VT_Cl_i);
  m = CELLMODEL_PARAMVALUE(VT_m);
  h = CELLMODEL_PARAMVALUE(VT_h);
  j = CELLMODEL_PARAMVALUE(VT_j);
  d = CELLMODEL_PARAMVALUE(VT_d);
  f = CELLMODEL_PARAMVALUE(VT_f);
  Xr = CELLMODEL_PARAMVALUE(VT_Xr);
  Xs = CELLMODEL_PARAMVALUE(VT_Xs);
  b = CELLMODEL_PARAMVALUE(VT_b);
  g = CELLMODEL_PARAMVALUE(VT_g);
  Xtof = CELLMODEL_PARAMVALUE(VT_Xtof);
  Ytof = CELLMODEL_PARAMVALUE(VT_Ytof);
  Xtos = CELLMODEL_PARAMVALUE(VT_Xtos);
  Yto1s = CELLMODEL_PARAMVALUE(VT_Yto1s);
  Yto2s = CELLMODEL_PARAMVALUE(VT_Yto2s);
  Ca_JSR = CELLMODEL_PARAMVALUE(VT_Ca_JSR);
  Ca_NSR = CELLMODEL_PARAMVALUE(VT_Ca_NSR);
  Ca_iontold = CELLMODEL_PARAMVALUE(VT_Ca_iontold);
  dCa_iont = CELLMODEL_PARAMVALUE(VT_dCa_iont);
  t_rel = CELLMODEL_PARAMVALUE(VT_t_rel);
  Ca_int_2ms = CELLMODEL_PARAMVALUE(VT_Ca_int_2ms);
  Ca_int_Jrel = CELLMODEL_PARAMVALUE(VT_Ca_int_Jrel);
} // PuglisiBers::Init

ML_CalcType
PuglisiBers::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
                  int euler = 1) {
  tinc *= 1000.0;
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double dCai = 1.0 / (double) Ca_i;

  // const double dNai=1.0/(double)Na_i;
  const double VmE_K = V_int - CELLMODEL_PARAMVALUE(VT_RTdF) * log(K_o / K_i);
  const double VmE_Ks = V_int - CELLMODEL_PARAMVALUE(VT_RTdF) * log((K_o + 0.01833 * Na_o) / (K_i + 0.01833 * Na_i));
  const double VmE_Na = V_int - CELLMODEL_PARAMVALUE(VT_RTdF) * log(Na_o / Na_i);

  // const double VmE_Ca=V_int-CELLMODEL_PARAMVALUE(VT_RTd2F)*log(Ca_o/Ca_i);
  const double VmE_Cl = V_int + CELLMODEL_PARAMVALUE(VT_RTdF) * log(Cl_o / Cl_i);
  const double V_myo = CELLMODEL_PARAMVALUE(VT_V_myo);
  const double V_JSR = CELLMODEL_PARAMVALUE(VT_V_JSR);
  const double V_NSR = CELLMODEL_PARAMVALUE(VT_V_NSR);

  // m+=tinc*(pP->a_m[Vi]*(1.-m)-pP->b_m[Vi]*m);
  const double m_inf = pP->m_inf[Vi];
  m = m_inf + (m - m_inf) * pP->exptau_m[Vi];

  // h+=tinc*(pP->a_h[Vi]*(1.-h)-pP->b_h[Vi]*h);
  const double h_inf = pP->h_inf[Vi];
  h = h_inf + (h - h_inf) * pP->exptau_h[Vi];

  // j+=tinc*(pP->a_j[Vi]*(1.-j)-pP->b_j[Vi]*j);
  const double j_inf = pP->j_inf[Vi];
  j = j_inf + (j - j_inf) * pP->exptau_j[Vi];
  const double I_Na = CELLMODEL_PARAMVALUE(VT_g_Na) * m * m * m * h * j * VmE_Na; // I_Na
  const double I_Nab = CELLMODEL_PARAMVALUE(VT_g_Nab) * VmE_Na; // I_Nab
  const double d_inf = pP->d_inf[Vi];
  d = d_inf - (d_inf - d) * pP->exptau_d[Vi];

  // f+=tinc*(pP->f_inf[Vi]-f)/pP->tau_f[Vi];
  const double f_inf = pP->f_inf[Vi];
  f = f_inf - (f_inf - f) * pP->exptau_f[Vi];
  const double F = CELLMODEL_PARAMVALUE(VT_F);
  const double FdRT = CELLMODEL_PARAMVALUE(VT_FdRT);
  const double expV_2 = pP->expV_2[Vi];
  const double expV = pP->expV[Vi];
  double GHKCa, GHKNa, GHKK;
  if (fabs(V_int) > .1) {
    GHKCa = 4 * F * FdRT * V_int * (CELLMODEL_PARAMVALUE(VT_gamma_Cai) * Ca_i * expV_2 - CELLMODEL_PARAMVALUE(VT_gamma_Cao) * Ca_o) /
            expm1(V_int * 2 * FdRT);
    GHKNa = F * FdRT * V_int * (CELLMODEL_PARAMVALUE(VT_gamma_Nai) * Na_i * expV - CELLMODEL_PARAMVALUE(VT_gamma_Nao) * Na_o) /
            expm1(V_int * FdRT);
    GHKK = F * FdRT * V_int * (CELLMODEL_PARAMVALUE(VT_gamma_Ki) * K_i * expV - CELLMODEL_PARAMVALUE(VT_gamma_Ko) * K_o) /
           expm1(V_int * FdRT);
  } else {
    GHKCa = 4 * F * FdRT * (CELLMODEL_PARAMVALUE(VT_gamma_Cai) * Ca_i * expV_2 - CELLMODEL_PARAMVALUE(VT_gamma_Cao) * Ca_o) * .5;
    GHKNa = F * FdRT * (CELLMODEL_PARAMVALUE(VT_gamma_Nai) * Na_i * expV - CELLMODEL_PARAMVALUE(VT_gamma_Nao) * Na_o);
    GHKK = F * FdRT * (CELLMODEL_PARAMVALUE(VT_gamma_Ki) * K_i * expV - CELLMODEL_PARAMVALUE(VT_gamma_Ko) * K_o);
  }
  const double fCa = CELLMODEL_PARAMVALUE(VT_k_mCa) / (CELLMODEL_PARAMVALUE(VT_k_mCa) + Ca_i);
  const double I_Ca = d * f * fCa * CELLMODEL_PARAMVALUE(VT_P_Ca) * GHKCa;
  const double I_CaNa = d * f * fCa * CELLMODEL_PARAMVALUE(VT_P_Na) * GHKNa;
  const double I_CaK = d * f * fCa * CELLMODEL_PARAMVALUE(VT_P_K) * GHKK;
  const double I_CaL = I_Ca + I_CaNa + I_CaK;
  const double b_inf = pP->b_inf[Vi];
  b = b_inf - (b_inf - b) * pP->exptau_b[Vi];
  const double g_inf = pP->g_inf[Vi];
  g = g_inf - (g_inf - g) * pP->exptau_g[Vi];
  const double I_CaT = CELLMODEL_PARAMVALUE(VT_g_CaT) * b * g * (V_int - CELLMODEL_PARAMVALUE(VT_E_CaT));  // I_CaT
  const double I_Cab = CELLMODEL_PARAMVALUE(VT_g_Cab) * (V_int - CELLMODEL_PARAMVALUE(VT_RTd2F) * log(Ca_o * dCai));  // I_Cab
  // Xr+=tinc*(pP->Xr_inf[Vi]-Xr)/pP->tau_Xr[Vi];
  const double Xr_inf = pP->Xr_inf[Vi];
  Xr = Xr_inf - (Xr_inf - Xr) * pP->exptau_Xr[Vi];
  const double I_Kr = CELLMODEL_PARAMVALUE(VT_g_Kr) * Xr * pP->R_Xr[Vi] * VmE_K;

  // Xs+=tinc*(pP->Xs_inf[Vi]-Xs)/pP->tau_Xs[Vi];
  const double Xs_inf = pP->Xs_inf[Vi];
  Xs = Xs_inf - (Xs_inf - Xs) * pP->exptau_Xs[Vi];
  const double pCa = -log10(Ca_i) + 3;
  const double g_Ks = CELLMODEL_PARAMVALUE(VT_gbar_Ks) * (0.057 + (0.19 / (1 + exp((-7.2 + pCa) / 0.6))));
  const double I_Ks = g_Ks * Xs * VmE_Ks;
  const double aki = 1.02 / (1 + exp(0.2385 * (VmE_K - 59.215)));
  const double bki = (0.49124 * exp(0.08032 * (VmE_K + 5.476)) + exp(0.06175 * (VmE_K - 594.31))) /
                     (1 + exp(-0.5143 * (VmE_K + 4.753)));
  const double I_K1 = CELLMODEL_PARAMVALUE(VT_g_K1) * VmE_K * aki / (aki + bki);
  const double I_Kp = pP->Kp[Vi] * VmE_K;

  // const double
  // I_KNa=(*(pP->g_KNa))/(1.0+pow(((*(pP->kdk_Na))*dNai),2.8))*(.8-.65/(1.0+exp((V+125.0)*.0666666666666)))*VmE_K;
  //    const double I_Katp=(*pP->I_Katpmax)*(pow((K_o*.25), (*pP->natp)))*VmE_K;
  // Xtof+=tinc*(pP->Xtof_inf[Vi]-Xtof)/pP->tau_Xtof[Vi];
  const double Xtof_inf = pP->Xtof_inf[Vi];
  Xtof = Xtof_inf - (Xtof_inf - Xtof) * pP->exptau_Xtof[Vi];

  // Ytof+=tinc*(pP->Ytof_inf[Vi]-Ytof)/pP->tau_Ytof[Vi];
  const double Ytof_inf = pP->Ytof_inf[Vi];
  Ytof = Ytof_inf - (Ytof_inf - Ytof) * pP->exptau_Ytof[Vi];
  const double I_tof = CELLMODEL_PARAMVALUE(VT_g_tof) * Xtof * Ytof * VmE_K;

  // Xtos+=tinc*(pP->Xtos_inf[Vi]-Xtos)/pP->tau_Xtos[Vi];
  // Yto1s+=tinc*(pP->Yto1s_inf[Vi]-Yto1s)/pP->tau_Yto1s[Vi];
  // Yto2s+=tinc*(pP->Yto2s_inf[Vi]-Yto2s)/pP->tau_Yto2s[Vi];
  const double Xtos_inf = pP->Xtos_inf[Vi];
  Xtos = Xtos_inf - (Xtos_inf - Xtos) * pP->exptau_Xtos[Vi];
  const double Yto1s_inf = pP->Yto1s_inf[Vi];
  Yto1s = Yto1s_inf - (Yto1s_inf - Yto1s) * pP->exptau_Yto1s[Vi];
  const double Yto2s_inf = pP->Yto2s_inf[Vi];
  Yto2s = Yto2s_inf - (Yto2s_inf - Yto2s) * pP->exptau_Yto2s[Vi];
  const double I_tos = CELLMODEL_PARAMVALUE(VT_g_tos) * Xtos * (Yto1s + .5 * Yto2s) * VmE_K;
  const double NNNO = Na_o * Na_o * Na_o;
  const double NNNI = Na_i * Na_i * Na_i;
  const double K1 = 1. / (CELLMODEL_PARAMVALUE(VT_KmNaex) * CELLMODEL_PARAMVALUE(VT_KmNaex) * CELLMODEL_PARAMVALUE(VT_KmNaex) + NNNO);
  const double K2 = 1. / (CELLMODEL_PARAMVALUE(VT_KmCaex) + Ca_o);
  const double K3 = 1. / (1. + CELLMODEL_PARAMVALUE(VT_ksat) * exp((CELLMODEL_PARAMVALUE(VT_eta) - 1) * V_int * FdRT));
  const double I_NaCa = CELLMODEL_PARAMVALUE(VT_kNaCa) * K1 * K2 * K3 *
                        (exp(CELLMODEL_PARAMVALUE(VT_eta) * V_int * FdRT) * NNNI * Ca_o -
                         exp((CELLMODEL_PARAMVALUE(VT_eta) - 1) * V_int * FdRT) * NNNO * Ca_i);
  const double tauNaK = (1. / 7.) * (exp(Na_o / 67.3) - 1);
  const double fNaK = 1. / (1. + 0.1245 * pP->expV_mk1[Vi] + 0.0365 * tauNaK * pP->expVm[Vi]);
  const double I_NaK = CELLMODEL_PARAMVALUE(VT_I_NaKmax) * fNaK * 1. / (1. + pow(CELLMODEL_PARAMVALUE(VT_k_mNai) / Na_i, 1.5)) *
                       (K_o / (K_o + CELLMODEL_PARAMVALUE(VT_k_mKo)));
  const double I_nsK = CELLMODEL_PARAMVALUE(VT_PnsK) * GHKK;
  const double I_nsNa = CELLMODEL_PARAMVALUE(VT_PnsNa) * GHKNa;
  const double fcans = 1. / (1. + pow(CELLMODEL_PARAMVALUE(VT_k_mnsCa) / Ca_i, 3.));
  const double I_ns = (I_nsK + I_nsNa) * fcans;
  const double JCanot = -0.5 * (I_CaL / fCa) * 6.15e-5 * 0.009;
  const double I_ClCa =
      CELLMODEL_PARAMVALUE(VT_g_ClCa) / (1. + CELLMODEL_PARAMVALUE(VT_k_mClCa) / JCanot * CELLMODEL_PARAMVALUE(VT_k_mClCa) / JCanot) * VmE_Cl;
  const double I_pCa = CELLMODEL_PARAMVALUE(VT_I_pCamax) * Ca_i / (Ca_i + CELLMODEL_PARAMVALUE(VT_k_mpCa));

  // const double Na_iont=I_Na+I_Nab+3.0*(I_NaK+I_NaCa);
  // const double K_iont=I_Kr+I_Ks+I_K1+I_Kp-2.0*I_NaK+I_tof+I_tos;
  const double Ca_iont = I_CaL + I_pCa - 2. * I_NaCa + I_CaT + I_Cab;

  // const double Cl_iont=I_ClCa;
  const double JCa = -0.5 * Ca_iont * 6.15e-5;
  const double I_ion =
      I_Na + I_K1 + I_tos + I_tof + I_Kr + I_Ks + I_Kp + I_CaL + I_CaT + I_Nab + I_Cab + I_NaCa +
      I_ns + I_NaK + I_pCa + I_ClCa -
      i_external;

  //    Na_i-=tinc*Na_iont*(CELLMODEL_PARAMVALUE(VT_CapdVmF));
  //    K_i-=tinc*K_iont*(CELLMODEL_PARAMVALUE(VT_CapdVmF));
  //    Cl_i-=tinc*Cl_iont*(CELLMODEL_PARAMVALUE(VT_CapdVmF));
  double ryron = 1. - exp(-(t_rel - 2.) / (CELLMODEL_PARAMVALUE(VT_t_on)));
  ryron = ryron * (ryron > 0);
  double ryroff = exp(-(t_rel - 2.) / (CELLMODEL_PARAMVALUE(VT_t_off)));
  ryroff = ryroff * (ryroff > 0);
  double grel = (t_rel >= 2) * 60 * (Ca_int_2ms - 0.18e-3) / ((Ca_int_2ms - 0.18e-3) + 0.8e-3);
  grel = (grel > 0) * grel;
  const double J_rel = (grel * ryron * ryroff + 3.8e-3) * (Ca_JSR - Ca_i);

  //    const double J_leak=CELLMODEL_PARAMVALUE(VT_I_upmax)/(CELLMODEL_PARAMVALUE(VT_Ca_NSRmax))*Ca_NSR;
  const double J_leak = 0;
  const double J_up = CELLMODEL_PARAMVALUE(VT_I_upmax) * Ca_i / (Ca_i + CELLMODEL_PARAMVALUE(VT_k_mup));
  const double J_tr = (Ca_NSR - Ca_JSR) / (CELLMODEL_PARAMVALUE(VT_t_tr));
  const double J_ion = Ca_iont * CELLMODEL_PARAMVALUE(VT_Cap) / (V_myo * (-2) * F);
  const double CSQN =
      CELLMODEL_PARAMVALUE(VT_CSQN_max) * CELLMODEL_PARAMVALUE(VT_k_mCSQN) / (Ca_JSR + CELLMODEL_PARAMVALUE(VT_k_mCSQN)) / (Ca_JSR + CELLMODEL_PARAMVALUE(VT_k_mCSQN));
  const double Bjsr = 1. / (1. + CSQN);
  const double btrpn =
      CELLMODEL_PARAMVALUE(VT_TRPN_max) * CELLMODEL_PARAMVALUE(VT_k_mTRPN) / (CELLMODEL_PARAMVALUE(VT_k_mTRPN) + Ca_i) / (CELLMODEL_PARAMVALUE(VT_k_mTRPN) + Ca_i);
  const double bcmdn =
      CELLMODEL_PARAMVALUE(VT_CMDN_max) * CELLMODEL_PARAMVALUE(VT_k_mCMDN) / (CELLMODEL_PARAMVALUE(VT_k_mCMDN) + Ca_i) / (CELLMODEL_PARAMVALUE(VT_k_mCMDN) + Ca_i);
  const double Bmyo = 1. / (1. + bcmdn + btrpn);
  Ca_NSR += tinc * (J_up - J_leak - J_tr);
  Ca_JSR += tinc * Bjsr * (J_tr / V_JSR * V_NSR - J_rel);
  Ca_i +=
      tinc * Bmyo * (J_ion - J_up * V_NSR / V_myo + J_leak * V_NSR / V_myo + J_rel * V_JSR / V_myo);

  //    Na_o+=tinc*(((CELLMODEL_PARAMVALUE(VT_Na_b))-Na_o)*.001+Na_iont*(CELLMODEL_PARAMVALUE(VT_CapdVcF)));
  //    K_o+=tinc*(((CELLMODEL_PARAMVALUE(VT_K_b))-K_o)*.001+K_iont*(CELLMODEL_PARAMVALUE(VT_CapdVcF)));
  //    Ca_o+=tinc*(((CELLMODEL_PARAMVALUE(VT_Ca_b))-Ca_o)*0.001+Ca_iont*CELLMODEL_PARAMVALUE(VT_CapdVcF)*.5);
  //    Cl_o+=tinc*(((*pP->Cl_b)-Cl_o)*.001+Cl_iont*(CELLMODEL_PARAMVALUE(VT_CapdVcF)));
  //    cerr << Na_iont << " " << K_iont << " " << Ca_iont << " " << Cl_iont << " " << i_external << endl;
  const double Vd = -tinc * I_ion;

  //    cerr << Vd << endl;
  if (Vd > 0.03)
    t_rel += tinc;
  else if (t_rel > 100)
    t_rel = 0;
  else if (t_rel > 0)
    t_rel += tinc;
  if ((t_rel <= 2) && t_rel)
    Ca_int_2ms += tinc * JCa;
  else if (t_rel > 40)
    Ca_int_2ms += tinc * -1e4 * Ca_int_2ms;
  if (Ca_int_2ms < 0)
    Ca_int_2ms = 0;
  return Vd / 1000;
} // PuglisiBers::Calc

void PuglisiBers::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << m << ' ' << h << ' ' << j << ' '
          << d << ' ' << f << ' '
          << Xr << ' ' << Xs << ' '
          << b << ' ' << g << ' '
          << Xtof << ' ' << Ytof << ' '
          << Xtos << ' ' << Yto1s << ' ' << Yto2s << ' '
          << Ca_i << ' ' << Ca_o << ' '
          << Cl_i << ' ' << Cl_o << ' '
          << Na_i << ' ' << Na_o << ' '
          << K_i << ' ' << K_o << ' '
          << Ca_JSR << ' ' << Ca_NSR << ' '
          << t_rel << ' ' << Ca_int_2ms << ' ';
}

void PuglisiBers::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);

  // const double dCai=1.0/(double)Ca_i;
  // const double dNai=1.0/(double)Na_i;
  const double VmE_K = V_int - CELLMODEL_PARAMVALUE(VT_RTdF) * log(K_o / K_i);
  const double VmE_Ks = V_int - CELLMODEL_PARAMVALUE(VT_RTdF) * log((K_o + 0.01833 * Na_o) / (K_i + 0.01833 * Na_i));
  const double VmE_Na = V_int - CELLMODEL_PARAMVALUE(VT_RTdF) * log(Na_o / Na_i);
  const double VmE_Ca = V_int - CELLMODEL_PARAMVALUE(VT_RTd2F) * log(Ca_o / Ca_i);
  const double VmE_Cl = V_int + CELLMODEL_PARAMVALUE(VT_RTdF) * log(Cl_o / Cl_i);
  const double F = CELLMODEL_PARAMVALUE(VT_F);
  const double FdRT = CELLMODEL_PARAMVALUE(VT_FdRT);
  const double expV_2 = pP->expV_2[Vi];
  const double expV = pP->expV[Vi];
  double GHKCa, GHKNa, GHKK;
  if (fabs(V_int) > 0.1) {
    GHKCa = 4 * F * FdRT * V_int * (CELLMODEL_PARAMVALUE(VT_gamma_Cai) * Ca_i * expV_2 - CELLMODEL_PARAMVALUE(VT_gamma_Cao) * Ca_o) /
            expm1(V_int * 2. * FdRT);
    GHKNa = F * FdRT * V_int * (CELLMODEL_PARAMVALUE(VT_gamma_Nai) * Na_i * expV - CELLMODEL_PARAMVALUE(VT_gamma_Nao) * Na_o) /
            expm1(V_int * FdRT);
    GHKK = F * FdRT * V_int * (CELLMODEL_PARAMVALUE(VT_gamma_Ki) * K_i * expV - CELLMODEL_PARAMVALUE(VT_gamma_Ko) * K_o) /
           expm1(V_int * FdRT);
  } else {
    GHKCa = 4 * F * FdRT * (CELLMODEL_PARAMVALUE(VT_gamma_Cai) * Ca_i * expV_2 - CELLMODEL_PARAMVALUE(VT_gamma_Cao) * Ca_o) * .5;
    GHKNa = F * FdRT * (CELLMODEL_PARAMVALUE(VT_gamma_Nai) * Na_i * expV - CELLMODEL_PARAMVALUE(VT_gamma_Nao) * Na_o);
    GHKK = F * FdRT * (CELLMODEL_PARAMVALUE(VT_gamma_Ki) * K_i * expV - CELLMODEL_PARAMVALUE(VT_gamma_Ko) * K_o);
  }
  const double fCa = CELLMODEL_PARAMVALUE(VT_k_mCa) / (CELLMODEL_PARAMVALUE(VT_k_mCa) + Ca_i);
  const double I_Na = CELLMODEL_PARAMVALUE(VT_g_Na) * m * m * m * h * j * VmE_Na;
  const double I_Nab = CELLMODEL_PARAMVALUE(VT_g_Nab) * VmE_Na;
  const double I_Ca = d * f * fCa * CELLMODEL_PARAMVALUE(VT_P_Ca) * GHKCa;
  const double I_CaNa = d * f * fCa * CELLMODEL_PARAMVALUE(VT_P_Na) * GHKNa;
  const double I_CaK = d * f * fCa * CELLMODEL_PARAMVALUE(VT_P_K) * GHKK;
  const double I_CaL = I_Ca + I_CaNa + I_CaK;
  const double I_CaT = CELLMODEL_PARAMVALUE(VT_g_CaT) * b * g * (V_int - CELLMODEL_PARAMVALUE(VT_E_CaT));
  const double I_Cab = CELLMODEL_PARAMVALUE(VT_g_Cab) * VmE_Ca;
  const double I_Kr = CELLMODEL_PARAMVALUE(VT_g_Kr) * Xr * pP->R_Xr[Vi] * VmE_K;
  const double pCa = -log10(Ca_i) + 3;
  const double g_Ks = CELLMODEL_PARAMVALUE(VT_gbar_Ks) * (0.057 + (0.19 / (1 + exp((-7.2 + pCa) / 0.6))));
  const double I_Ks = g_Ks * Xs * VmE_Ks;
  const double aki = 1.02 / (1 + exp(0.2385 * (VmE_K - 59.215)));
  const double bki = (0.49124 * exp(0.08032 * (VmE_K + 5.476)) + exp(0.06175 * (VmE_K - 594.31))) /
                     (1 + exp(-0.5143 * (VmE_K + 4.753)));
  const double I_K1 = CELLMODEL_PARAMVALUE(VT_g_K1) * VmE_K * aki / (aki + bki);
  const double I_Kp = pP->Kp[Vi] * VmE_K;
  const double I_tof = CELLMODEL_PARAMVALUE(VT_g_tof) * Xtof * Ytof * VmE_K;
  const double I_tos = CELLMODEL_PARAMVALUE(VT_g_tos) * Xtos * (Yto1s + .5 * Yto2s) * VmE_K;
  const double NNNO = Na_o * Na_o * Na_o;
  const double NNNI = Na_i * Na_i * Na_i;
  const double K1 = 1 / (CELLMODEL_PARAMVALUE(VT_KmNaex) * CELLMODEL_PARAMVALUE(VT_KmNaex) * CELLMODEL_PARAMVALUE(VT_KmNaex) + NNNO);
  const double K2 = 1 / (CELLMODEL_PARAMVALUE(VT_KmCaex) + Ca_o);
  const double K3 = 1 / (1 + CELLMODEL_PARAMVALUE(VT_ksat) * exp((CELLMODEL_PARAMVALUE(VT_eta) - 1) * V_int * FdRT));
  const double I_NaCa = CELLMODEL_PARAMVALUE(VT_kNaCa) * K1 * K2 * K3 *
                        (exp(CELLMODEL_PARAMVALUE(VT_eta) * V_int * FdRT) * NNNI * Ca_o -
                         exp((CELLMODEL_PARAMVALUE(VT_eta) - 1) * V_int * FdRT) * NNNO * Ca_i);
  const double I_nsK = CELLMODEL_PARAMVALUE(VT_PnsK) * GHKK;
  const double I_nsNa = CELLMODEL_PARAMVALUE(VT_PnsNa) * GHKNa;
  const double fcans = 1. / (1. + pow(CELLMODEL_PARAMVALUE(VT_k_mnsCa) / Ca_i, 3.));
  const double I_ns = (I_nsK + I_nsNa) * fcans;
  const double tauNaK = (1. / 7.) * (exp(Na_o / 67.3) - 1);
  const double fNaK = 1. / (1. + 0.1245 * pP->expV_mk1[Vi] + 0.0365 * tauNaK * pP->expVm[Vi]);
  const double I_NaK = CELLMODEL_PARAMVALUE(VT_I_NaKmax) * fNaK * 1. / (1. + pow(CELLMODEL_PARAMVALUE(VT_k_mNai) / Na_i, 1.5)) *
                       (K_o / (K_o + CELLMODEL_PARAMVALUE(VT_k_mKo)));
  const double I_pCa = CELLMODEL_PARAMVALUE(VT_I_pCamax) * Ca_i / (Ca_i + CELLMODEL_PARAMVALUE(VT_k_mpCa));
  const double JCanot = -0.5 * (I_CaL / fCa) * 6.15e-5 * 0.009;
  const double I_ClCa =
      CELLMODEL_PARAMVALUE(VT_g_ClCa) / (1. + CELLMODEL_PARAMVALUE(VT_k_mClCa) / JCanot * CELLMODEL_PARAMVALUE(VT_k_mClCa) / JCanot) * VmE_Cl;
  const double I_mem =
      I_Na + I_K1 + I_tos + I_tof + I_Kr + I_Ks + I_Kp + I_CaL + I_CaT + I_Nab + I_Cab + I_NaCa +
      I_ns + I_NaK + I_pCa + I_ClCa;
  tempstr << I_Na << ' ' << I_Nab << ' ' << I_Ca << ' ' << I_CaNa << ' ' << I_CaK << ' ' << I_CaT
          << ' ' << I_Cab << ' '
          << I_Kr << ' ' << I_Ks << ' ' << I_K1 << ' ' << I_Kp << ' ' << I_tof << ' ' << I_tos
          << ' ' << I_NaCa << ' '
          << I_ns << ' ' << I_NaK << ' ' << I_pCa << ' '
          << I_ClCa << ' ' // I_ClCa
          << (Ca_NSR - Ca_JSR) * (CELLMODEL_PARAMVALUE(VT_t_tr)) << ' ' // I_tr
          << CELLMODEL_PARAMVALUE(VT_I_upmax) / (CELLMODEL_PARAMVALUE(VT_Ca_NSRmax)) * Ca_NSR << ' ' // I_leak
          << CELLMODEL_PARAMVALUE(VT_I_upmax) * Ca_i / (Ca_i + CELLMODEL_PARAMVALUE(VT_k_mup)) << ' ' // I_up
          << I_mem << ' '; // I_mem
} // PuglisiBers::LongPrint

void PuglisiBers::GetParameterNames(vector<string> &getpara) {
  const string ParaNames[] = {"m", "h", "j",
                              "d", "f", "Xr", "Xs", "b", "g", "Xtof", "Ytof", "Xtos",
                              "Yto1s",
                              "Yto2s",
                              "Ca_i", "Ca_o", "Cl_i", "Cl_o", "Na_i", "Na_o", "K_i", "K_o",
                              "Ca_JSR",
                              "Ca_NSR",
                              "t_rel", "Ca_int_2ms"};

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void PuglisiBers::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const string ParaNames[] =
      {"I_Na", "I_Nab", "I_Ca", "I_CaNa", "I_CaK", "I_CaT", "I_Cab", "I_Kr",
       "I_Ks",
       "I_K1", "I_Kp",
       "I_tof", "I_tos", "I_NaCa", "I_ns", "I_NaK", "I_pCa", "I_ClCa", "I_tr",
       "I_leak",
       "I_up", "I_mem"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}
