/**@file Iyer.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef IYER_H
#define IYER_H
#ifdef osMac_Vec

# include <IyerParameters.h>
# include <HandleManager.h>

# ifdef HETERO
#  define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_IyerParameters::a)
# else // ifdef HETERO
#  define CELLMODEL_PARAMVALUE(a) pIP->P[NS_IyerParameters::a].value
# endif // ifdef HETERO

// template<class T> class IyerBasis;

class Iyer : public vbElphyModel<ML_CalcType>{
 private:
  int Vi;

 public:
  IyerParameters *pIP;

  // HandleManager<ML_CalcType> *HM;

# ifdef HETERO
  ParameterSwitch *PS;
# endif // ifdef HETERO

  // memory to copy during set
  ML_CalcType Ca_i;
  ML_CalcType Array[ArraySize];
  ML_CalcType E_to, E_Na, E_K, E_Ks, E_Ca;
  ML_CalcType cICa, cICab, cINaCa, cINa, cINab, cIKb, cINaK, cIKr, cIKs, cIK1, cIto, cIpCa, cICaK;
  ML_CalcType iStim;

  // memory to copy during set

  ML_CalcType dV;

  int indexINa;
  int indexIto;
  int indexIKr;
  int indexIKs;
  int indexINab;
  int indexIK1;
  int indexICa;
  int indexICab;
  int indexINaCa;
  int indexINaK;
  int indexIpCa;

  // int num;
  // string *FN;
  // string *EV;

  // string NaHandling, CaHandling, KHandling;
  SO_ionicCurrent<ML_CalcType> **shd;

  SO_NaConc_Handling<ML_CalcType> *NA;
  SO_CaConc_Handling<ML_CalcType> *CA;
  SO_KConc_Handling<ML_CalcType> *KA;

  Iyer(IyerParameters *pp);
  ~Iyer();
  virtual void        Init();
  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline int GetSize(void) {
    return sizeof(Iyer)-sizeof(vbElphyModel<ML_CalcType>)-sizeof(IyerParameters*)
# ifdef HETERO
           -sizeof(ParameterSwitch*)
# endif // ifdef HETERO
    ;
  }

  // virtual inline int GetSize(void) {return (&iStim-&Ca_i+1)*sizeof(T);};
  virtual inline ML_CalcType* GetBase(void) { return &Ca_i; }

  virtual inline ML_CalcType SurfaceToVolumeRatio() { return 0.0; }

  virtual inline ML_CalcType Volume() { return 2.064403e-13; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime(void) { return .0005; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_init_Vm); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return 0.0; }

  virtual inline ML_CalcType GetNai() { return 0.0; }

  virtual inline ML_CalcType GetNao() { return 0.0; }

  virtual inline ML_CalcType GetKi() { return 0.0; }

  virtual inline ML_CalcType GetKo() { return 0.0; }

  virtual inline void Set(vbElphyModel<ML_CalcType>& from) {
    // cerr<<"Iyer.h: copying "<<from.GetSize()<<" Bytes starting from "<<from.GetBase()<<endl;
    memcpy(GetBase(), from.GetBase(), from.GetSize());
    for (int x = 0; x < pIP->HM->numCurrents; x++) {
      SO_ionicCurrent<ML_CalcType> *mySIC = GetSO_ionicCurrent(x);
      SO_ionicCurrent<ML_CalcType> *exSIC = from.GetSO_ionicCurrent(x);

      // cerr<<"x="<<x<<"\tmySIC="<<mySIC<<"\texSIC="<<exSIC<<"\n";
      // cerr<<mySIC->GetEquilibriumType()<<"\t"<<exSIC->GetEquilibriumType()<<"\n";
      mySIC->Set(exSIC);
    }
  }

  virtual inline SO_ionicCurrent<ML_CalcType>* GetSO_ionicCurrent(int index) {
    // cerr<<"index="<<index<<endl;
    return shd[index];
  }

  virtual inline void SetCai(ML_CalcType val) {
    // #if KADEBUG
    cerr<<"setting Cai to " << val << endl;

    // #endif
    Array[6] = val;
  }

  virtual void Print(ostream& tempstr, double t, ML_CalcType V) {
    tempstr<<t<<' '<<V<<' '
           <<cINa<<' '<<cIto<<' '<<cIKr<<' '<<cIKs<<' '
           <<cINab<<' '<<cIK1<<' '<<cICa<<' '<<cICab<<' '
           <<cINaCa<<' '<<cINaK<<' '<<cIpCa<<' '<<cICaK<<' '
    ;
  }

  virtual void LongPrint(ostream& tempstr, double t, ML_CalcType V) {
    Print(tempstr, t, V);
    tempstr
      <<Array[2]<<' '<<Array[3]<<' '<<Array[4]<<' '<<Array[5]<<' '
      <<Array[6]<<' '<<Array[7]<<' '<<Array[9]<<' '<<Array[10]<<' '
      <<Array[11]<<' '
    ;
    for (int i = 0; i < pIP->HM->numCurrents; i++)
      shd[i]->LongPrint(tempstr);
    KA->LongPrint(tempstr);
    NA->LongPrint(tempstr);
    CA->LongPrint(tempstr);
  }

  virtual void GetParameterNames(vector<string>& getpara) {
    const int numpara               = 12;
    const string ParaNames[numpara] =
    { "INa", "Ito", "IKr", "IKs", "INab", "IK1", "ICa", "ICab", "INaCa", "INaK", "IpCa", "ICaK" };

    for (int i = 0; i < numpara; i++)
      getpara.push_back(ParaNames[i]);
  }

  virtual void GetLongParameterNames(vector<string>& getpara) {
    GetParameterNames(getpara);
    const int numpara               = 9;
    const string ParaNames[numpara] = { "Nai", "Nao", "Ki", "Ko", "Cai", "Cao", "IKv14Na", "ICaSS", "ICaK" };
    for (int i = 0; i < numpara; i++)
      getpara.push_back(ParaNames[i]);

    // for (int i=0; i<num; i++)
    //  shd[i]->GPN();
    for (int i = 0; i < pIP->HM->numCurrents; i++)
      shd[i]->GetLongParameterNames(getpara);
    KA->GetLongParameterNames(getpara);
    NA->GetLongParameterNames(getpara);
    CA->GetLongParameterNames(getpara);
  }

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char)(adVm < .15e-6 ? 3 : (adVm < .3e-6 ? 2 : 1));
  }
  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
                           int euler = 1);
}; // class Iyer


#endif // ifdef osMac_Vec
#endif // ifndef IYER_H
