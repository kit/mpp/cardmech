/**@file FabbriParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef FabbriParameters_H
#define FabbriParameters_H


#include <ParameterLoader.h>

namespace NS_FabbriParameters {
  enum varType {
    VT_Vari_Nai = vtFirst,
    VT_R,
    VT_T,
    VT_F,
    VT_C_m,
    VT_clamp_mode,
    VT_t_holding,
    VT_t_test,
    VT_V_test,
    VT_V_holding,
    VT_Iva_3_uM,
    VT_Cs_5_mM,
    VT_ACh,
    VT_Iso_1_uM,
    VT_BAPTA_10_mM,
    VT_Nao,
    VT_Init_Ki,
    VT_Ko,
    VT_Cao,
    VT_g_fNa,
    VT_g_fK,
    VT_Km_f,
    VT_alpha_f,
    VT_blockade_f,
    VT_y_shift_f,
    VT_Km_Kp,
    VT_Km_Nap,
    VT_i_NaK_max,
    VT_K_NaCa,
    VT_Qci,
    VT_Qn,
    VT_Qco,
    VT_K3ni,
    VT_Kci,
    VT_K1ni,
    VT_K2ni,
    VT_Kcni,
    VT_K3no,
    VT_K1no,
    VT_K2no,
    VT_Kco,
    VT_blockade_NaCa,
    VT_g_na,
    VT_delta_m,
    VT_P_CaL,
    VT_k_dl,
    VT_V_dl,
    VT_k_fl,
    VT_V_fl,
    VT_shift_fL,
    VT_alpha_fCa,
    VT_Km_fCa,
    VT_P_CaT,
    VT_offset_fT,
    VT_ks,
    VT_MaxSR,
    VT_MinSR,
    VT_EC50_SR,
    VT_HSR,
    VT_koCa,
    VT_kiCa,
    VT_kim,
    VT_kom,
    VT_tau_dif_Ca,
    VT_tau_tr,
    VT_P_up_basal,
    VT_K_up,
    VT_slope_up,
    VT_TC_tot,
    VT_TMC_tot,
    VT_CM_tot,
    VT_CQ_tot,
    VT_kf_TC,
    VT_kf_TMM,
    VT_kf_TMC,
    VT_kf_CM,
    VT_kf_CQ,
    VT_kb_TC,
    VT_kb_TMC,
    VT_kb_TMM,
    VT_kb_CM,
    VT_kb_CQ,
    VT_Init_Mgi,
    VT_T_Ca,
    VT_V_jsr_part,
    VT_V_i_part,
    VT_V_nsr_part,
    VT_R_cell,
    VT_L_cell,
    VT_L_sub,
    VT_g_Kur,
    VT_g_to,
    VT_g_Kr,
    VT_g_Ks,
    VT_shift,
    VT_g_KACh,
    VT_ACh_on,
    VT_b_up,
    VT_E_K,
    VT_ACh_shift,
    VT_Iso_shift_If,
    VT_Iso_increase_INaK,
    VT_k34,
    VT_Iso_increase_ICaL,
    VT_ACh_block,
    VT_Iso_shift_IKs,
    VT_alpha_a,
    VT_RTd2F,
    VT_Init_Vm,
    VT_Init_Ca_sub,
    VT_Init_Nai,
    VT_Init_y,
    VT_Init_m,
    VT_Init_h,
    VT_Init_dL,
    VT_Init_fL,
    VT_Init_fCa,
    VT_Init_dT,
    VT_Init_fT,
    VT_Init_R_Ca_rel,
    VT_Init_O_Ca_rel,
    VT_Init_I_Ca_rel,
    VT_Init_RI_Ca_rel,
    VT_Init_Ca_jsr,
    VT_Init_Ca_nsr,
    VT_Init_Cai,
    VT_Init_fTMM,
    VT_Init_fCMi,
    VT_Init_fCMs,
    VT_Init_fTC,
    VT_Init_fTMC,
    VT_Init_fCQ,
    VT_Init_r_Kur,
    VT_Init_s,
    VT_Init_q,
    VT_Init_r_to,
    VT_Init_paS,
    VT_Init_paF,
    VT_Init_piy,
    VT_Init_n,
    VT_Init_a,
    VT_g_f,
    VT_g_Na_L,
    VT_g_K1,
    VT_Init_pi,
    VT_V_cell,
    VT_V_sub,
    VT_V_jsr,
    VT_V_i,
    VT_V_nsr,
    vtLast
  };
} // namespace NS_FabbriParameters

using namespace NS_FabbriParameters;

class FabbriParameters : public vbNewElphyParameters {
public:
  FabbriParameters(const char *, ML_CalcType);

  ~FabbriParameters();

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType);

  void Init(const char *, ML_CalcType);

  ML_CalcType m_y[RTDT];
  ML_CalcType exptau_y[RTDT];
  ML_CalcType m_m[RTDT];
  ML_CalcType exptau_m[RTDT];
  ML_CalcType m_h[RTDT];
  ML_CalcType exptau_h[RTDT];
  ML_CalcType m_dL[RTDT];
  ML_CalcType exptau_dL[RTDT];
  ML_CalcType m_fL[RTDT];
  ML_CalcType exptau_fL[RTDT];
  ML_CalcType m_dT[RTDT];
  ML_CalcType exptau_dT[RTDT];
  ML_CalcType m_fT[RTDT];
  ML_CalcType exptau_fT[RTDT];
  ML_CalcType m_r_Kur[RTDT];
  ML_CalcType exptau_r_Kur[RTDT];
  ML_CalcType m_s[RTDT];
  ML_CalcType exptau_s[RTDT];
  ML_CalcType m_q[RTDT];
  ML_CalcType exptau_q[RTDT];
  ML_CalcType m_r_to[RTDT];
  ML_CalcType exptau_r_to[RTDT];
  ML_CalcType m_pa[RTDT];
  ML_CalcType exptau_paF[RTDT];
  ML_CalcType exptau_paS[RTDT];
  ML_CalcType m_pi[RTDT];
  ML_CalcType exptau_pi[RTDT];
  ML_CalcType m_n[RTDT];
  ML_CalcType exptau_n[RTDT];
  ML_CalcType m_a[RTDT];
  ML_CalcType exptau_a[RTDT];
  ML_CalcType ICs_on_Icontrol[RTDT];
  ML_CalcType doo[RTDT];
  ML_CalcType k21[RTDT];
  ML_CalcType k41[RTDT];
  ML_CalcType k23[RTDT];
  ML_CalcType k32[RTDT];
}; // class FabbriParameters
#endif // ifndef FabbriParameters_H
