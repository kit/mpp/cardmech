/*      File: LuoRudy.cpp
   automatically created by ExtractParameterClass.pl - done by dw (19.09.2007)
   Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
   send comments to dw@ibt.uka.de       */

#include <LuoRudy.h>

LuoRudy::LuoRudy(LuoRudyParameters *pp) {
  pP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pP, NS_LuoRudyParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

LuoRudy::~LuoRudy() {}

#ifdef HETERO

bool LuoRudy::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool LuoRudy::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

int LuoRudy::GetSize(void) {
  return sizeof(LuoRudy) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(LuoRudyParameters *)
         #ifdef HETERO
         - sizeof(ParameterSwitch *)
#endif // ifdef HETERO
      ;
}

void LuoRudy::Init() {
#if KADEBUG
  cerr << "initializing Class: LuoRudy ... " << endl;
#endif // if KADEBUG
  Ca_i = CELLMODEL_PARAMVALUE(VT_Ca_i);
  Ca_o = CELLMODEL_PARAMVALUE(VT_Ca_o);
  Na_i = CELLMODEL_PARAMVALUE(VT_Na_i);
  Na_o = CELLMODEL_PARAMVALUE(VT_Na_o);
  K_i = CELLMODEL_PARAMVALUE(VT_K_i);
  K_o = CELLMODEL_PARAMVALUE(VT_K_o);
  Ca_JSR = CELLMODEL_PARAMVALUE(VT_Ca_JSR);
  Ca_NSR = CELLMODEL_PARAMVALUE(VT_Ca_NSR);
  dCa_iont = CELLMODEL_PARAMVALUE(VT_dCa_iont);
  t_CICR = CELLMODEL_PARAMVALUE(VT_t_CICR);
  t_jsrol = CELLMODEL_PARAMVALUE(VT_t_jsrol);
  m = CELLMODEL_PARAMVALUE(VT_m);
  h = CELLMODEL_PARAMVALUE(VT_h);
  j = CELLMODEL_PARAMVALUE(VT_j);
  d = CELLMODEL_PARAMVALUE(VT_d);
  f = CELLMODEL_PARAMVALUE(VT_f);
  b = CELLMODEL_PARAMVALUE(VT_b);
  g = CELLMODEL_PARAMVALUE(VT_g);
  Xr = CELLMODEL_PARAMVALUE(VT_Xr);
  Xs1 = CELLMODEL_PARAMVALUE(VT_Xs1);
  Xs2 = CELLMODEL_PARAMVALUE(VT_Xs2);
  ato = CELLMODEL_PARAMVALUE(VT_ato);
  ito = CELLMODEL_PARAMVALUE(VT_ito);
  grelbarjsrol = (unsigned char) CELLMODEL_PARAMVALUE(VT_grelbarjsrol);
  flip = (unsigned char) CELLMODEL_PARAMVALUE(VT_flip);
} // LuoRudy::Init

ML_CalcType
LuoRudy::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
              int euler = 1) {
  tinc *= 1000.0;
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double dCai = 1.0 / (double) Ca_i;
  const double dNai = 1.0 / (double) Na_i;
  const double VmE_K = V_int - (CELLMODEL_PARAMVALUE(VT_RTdF) * log(K_o / K_i));

  const double m_inf = pP->m_inf[Vi];
  m = m_inf - (m_inf - m) * pP->et_m[Vi];

  // h+=tinc*(pP->a_h[Vi]-pP->b_h[Vi]*h);
  const ML_CalcType h_inf = pP->h_inf[Vi];
  h = h_inf + (h - h_inf) * pP->et_h[Vi];

  // j+=tinc*(pP->a_j[Vi]-pP->b_j[Vi]*j);
  const ML_CalcType j_inf = pP->j_inf[Vi];
  j = j_inf + (j - j_inf) * pP->et_j[Vi];

  // d+=tinc*(pP->a_d[Vi]-pP->b_d[Vi]*d);
  const ML_CalcType d_inf = pP->d_inf[Vi];
  d = d_inf + (d - d_inf) * pP->et_d[Vi];

  // f+=tinc*(pP->a_f[Vi]-pP->b_f[Vi]*f);
  const ML_CalcType f_inf = pP->f_inf[Vi];
  f = f_inf + (f - f_inf) * pP->et_f[Vi];

  // b+=tinc*(pP->a_b[Vi]-pP->b_b[Vi]*b);
  const ML_CalcType b_inf = pP->b_inf[Vi];
  b = b_inf + (b - b_inf) * pP->et_b[Vi];

  // g+=tinc*(pP->a_g[Vi]-pP->b_g[Vi]*g);
  const ML_CalcType g_inf = pP->g_inf[Vi];
  g = g_inf + (g - g_inf) * pP->et_g[Vi];

  // Xr+=tinc*(pP->a_Xr[Vi]-pP->b_Xr[Vi]*Xr);
  const ML_CalcType Xr_inf = pP->Xr_inf[Vi];
  Xr = Xr_inf + (Xr - Xr_inf) * pP->et_Xr[Vi];

  // Xs1+=tinc*(pP->a_Xs1[Vi]-pP->b_Xs1[Vi]*Xs1);
  const ML_CalcType Xs1_inf = pP->Xs1_inf[Vi];
  Xs1 = Xs1_inf + (Xs1 - Xs1_inf) * pP->et_Xs1[Vi];

  // Xs2+=tinc*(pP->a_Xs2[Vi]-pP->b_Xs2[Vi]*Xs2);
  const ML_CalcType Xs2_inf = pP->Xs2_inf[Vi];
  Xs2 = Xs2_inf + (Xs2 - Xs2_inf) * pP->et_Xs2[Vi];

  // ato+=tinc*(pP->a_ato[Vi]-pP->b_ato[Vi]*ato);
  const ML_CalcType ato_inf = pP->ato_inf[Vi];
  ato = ato_inf + (ato - ato_inf) * pP->et_ato[Vi];

  // ito+=tinc*(pP->a_ito[Vi]-pP->b_ito[Vi]*ito);
  const ML_CalcType ito_inf = pP->ito_inf[Vi];
  ito = ito_inf + (ito - ito_inf) * pP->et_ito[Vi];

  const double I_Na = ((CELLMODEL_PARAMVALUE(VT_g_Na)) * m * m * m * h * j + (CELLMODEL_PARAMVALUE(VT_g_Nab))) *
                      (V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(Na_o * dNai))); // I_Na+I_Nab
  const double dffCa = d * f * (CELLMODEL_PARAMVALUE(VT_k_mCa)) / ((CELLMODEL_PARAMVALUE(VT_k_mCa)) + Ca_i);
  const double I_Ca = dffCa * pP->C_Ca[Vi] * (Ca_i * pP->expV_2[Vi] - Ca_o);
  const double I_Namax = pP->C_NaK[Vi] * (CELLMODEL_PARAMVALUE(VT_KgN) * Na_i * pP->expV[Vi] - Na_o);
  const double I_CaNa = dffCa * (CELLMODEL_PARAMVALUE(VT_P_Na)) * I_Namax;
  const double I_Kmax = pP->C_NaK[Vi] * (CELLMODEL_PARAMVALUE(VT_KgN) * K_i * pP->expV[Vi] - K_o);
  const double I_CaK = dffCa * (CELLMODEL_PARAMVALUE(VT_P_K)) * I_Kmax;
  const double I_CaT = ((CELLMODEL_PARAMVALUE(VT_g_CaT)) * b * b * g + (CELLMODEL_PARAMVALUE(VT_g_Cab))) *
                       (V_int - ((CELLMODEL_PARAMVALUE(VT_RTd2F)) * log(Ca_o * dCai))); // I_CaT+I_Ca
  const double g_Kall = sqrt(K_o * .18518519) * VmE_K;
  const double I_Kr = g_Kall * Xr * pP->Rinf[Vi];
  const double I_Ks = .433 *
                      (1.0 + .6 /
                             (1 +
                              pow((.000038 * dCai), 1.4))) * Xs1 * Xs2 * (V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) *
                                                                                   log((K_o +
                                                                                        (CELLMODEL_PARAMVALUE(VT_P_NaK)) *
                                                                                        Na_o) /
                                                                                       (K_i +
                                                                                        (CELLMODEL_PARAMVALUE(VT_P_NaK)) *
                                                                                        Na_i))));
  const double aki = 1.02 / (1 + exp(0.2385 * (VmE_K - 59.215)));
  const double bki = (0.49124 * exp(0.08032 * (VmE_K + 5.476)) + exp(0.06175 * (VmE_K - 594.31))) /
                     (1 + exp(-0.5143 * (VmE_K + 4.753)));
  const double I_K1 = (CELLMODEL_PARAMVALUE(VT_g_K1)) * g_Kall * aki / (aki + bki);
  const double I_Kp = pP->Kp[Vi] * VmE_K;

  // const double
  // I_KNa=(*(pP->g_KNa))/(1.0+pow(((*(pP->kdk_Na))*dNai),2.8))*(.8-.65/(1.0+exp((V+125.0)*.0666666666666)))*VmE_K;
  const double I_KNa = 0;
  const double I_Katp = (CELLMODEL_PARAMVALUE(VT_I_Katpmax)) * (pow((K_o * .25), (CELLMODEL_PARAMVALUE(VT_natp)))) * VmE_K;

  //    const double I_Katp=0;
  const double I_to = (CELLMODEL_PARAMVALUE(VT_g_to)) * ato * ato * ato * ito *
                      (V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((.082 * Na_o + K_o) / (.082 * Na_i + K_i))));
  const double NNNOCI = Na_o * Na_o * Na_o * Ca_i;
  const double NNNICO = Na_i * Na_i * Na_i * Ca_o;
  const double I_NaCa = 2.5 * pP->etaV[Vi] * ((pP->expV[Vi] * NNNICO - NNNOCI) / (1.0 +
                                                                                  pP->etaV[Vi] *
                                                                                  (pP->expV[Vi] *
                                                                                   NNNICO +
                                                                                   NNNOCI)));
  const double I_NaK = (CELLMODEL_PARAMVALUE(VT_I_NaKmax)) / (1.0 + .1245 * pP->expV_mk1[Vi] +
                                           (exp(Na_o * 0.014858841) - 1.0) * pP->expVm[Vi]) /
                       (1.0 + CELLMODEL_PARAMVALUE(VT_kk_mNai) * dNai * dNai) * K_o / (K_o + CELLMODEL_PARAMVALUE(VT_k_mKo));

  // const double nstemp=(CELLMODEL_PARAMVALUE(VT_P_nsCa))/(1+(*(pP->kkk_mnsCa))*dCai*dCai*dCai);
  // const double I_nsK=I_Kmax*nstemp;
  // const double I_nsNa=I_Namax*nstemp;
  const double I_nsK = 0;
  const double I_nsNa = 0;
  const double I_pCa = (CELLMODEL_PARAMVALUE(VT_I_pCamax)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mpCa)));
  const double Na_iont = I_Na + I_CaNa + I_nsNa + 3.0 * (I_NaK + I_NaCa);
  const double K_iont =
      I_Kr + I_Ks + I_K1 + I_Kp + I_CaK + I_nsK - 2.0 * I_NaK + I_to + I_KNa + I_Katp;
  const double Ca_iont = I_Ca + I_pCa - 2.0 * I_NaCa + I_CaT;
  const double i_ext = i_external * 1e-3 / (CELLMODEL_PARAMVALUE(VT_A_Cap)); // conversion from nA/cell to A/F
  const double I_ion = Na_iont + K_iont + Ca_iont - i_ext;
  Na_i -= tinc * Na_iont * (CELLMODEL_PARAMVALUE(VT_AdVmF));
  K_i -= tinc * K_iont * (CELLMODEL_PARAMVALUE(VT_AdVmF));
  const double I_tr = (Ca_NSR - Ca_JSR) * (CELLMODEL_PARAMVALUE(VT_dt_tr));
  const double I_leak = (CELLMODEL_PARAMVALUE(VT_IupdCa)) * Ca_NSR;
  const double I_up = (CELLMODEL_PARAMVALUE(VT_I_upmax)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mup)));
  Ca_NSR += tinc * (I_up - I_leak - I_tr * (CELLMODEL_PARAMVALUE(VT_VJdVN)));
  const double dCa_iontnew = (Ca_iont - Ca_iontold) / tinc;
  if ((i_external > 10.0) && (flip == 3))
    flip = 2;
  if ((V_int > -35.0) && (dCa_iontnew < dCa_iont) && (flip == 2)) {
    flip = 1;
    t_CICR = 0.0;
  }
  if ((t_CICR > 100.0) && (flip == 1))
    flip = 3;
  const double on = 1.0 / (1.0 + exp(-t_CICR - t_CICR + 8.0));
  const double magrel = 1.0 / (1.0 + exp((Ca_iont + 5.0) * 1.1111111));
  const double I_relcicr = (CELLMODEL_PARAMVALUE(VT_g_maxrel)) * on * (1.0 - on) * magrel * (Ca_JSR - Ca_i);
  t_CICR += tinc;
  const double I_reljsrol =
      int(grelbarjsrol) * (1.0 - exp(-t_jsrol * (CELLMODEL_PARAMVALUE(VT_dt_on)))) * exp(-t_jsrol * (CELLMODEL_PARAMVALUE(VT_dt_off))) *
      (Ca_JSR - Ca_i);
  t_jsrol += tinc;
  const double I_relges = I_relcicr + I_reljsrol;
  const double CSQN = (CELLMODEL_PARAMVALUE(VT_CSQN_max)) * Ca_JSR / (Ca_JSR + (CELLMODEL_PARAMVALUE(VT_k_mCSQN)));
  const double dCa_JSR = tinc * (I_tr - I_relges);
  const double C_CSQN = (CELLMODEL_PARAMVALUE(VT_CSQN_max)) - CSQN - dCa_JSR - Ca_JSR + (CELLMODEL_PARAMVALUE(VT_k_mCSQN));
  Ca_JSR = (sqrt(C_CSQN * C_CSQN + 4 * (CELLMODEL_PARAMVALUE(VT_k_mCSQN)) * (CSQN + dCa_JSR + Ca_JSR)) - C_CSQN) * .5;
  const double dCa_i = -tinc *
                       (Ca_iont * (CELLMODEL_PARAMVALUE(VT_A_Cap)) * (CELLMODEL_PARAMVALUE(VT_d2F)) + (I_up - I_leak) * (CELLMODEL_PARAMVALUE(VT_V_NSR)) -
                        I_relges * (CELLMODEL_PARAMVALUE(VT_V_JSR))) *
                       (CELLMODEL_PARAMVALUE(VT_dV_myo));
  const double Ca_tot =
      ((CELLMODEL_PARAMVALUE(VT_TRPN_max)) / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mTRPN))) + (CELLMODEL_PARAMVALUE(VT_CMDN_max)) / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mCMDN)))) *
      Ca_i + dCa_i +
      Ca_i;
  const double bmyo =
      (CELLMODEL_PARAMVALUE(VT_CMDN_max)) + (CELLMODEL_PARAMVALUE(VT_TRPN_max)) - Ca_tot + (CELLMODEL_PARAMVALUE(VT_k_mTRPN)) + (CELLMODEL_PARAMVALUE(VT_k_mCMDN));
  const double cmyo =
      ((CELLMODEL_PARAMVALUE(VT_k_mCMDN)) * (CELLMODEL_PARAMVALUE(VT_k_mTRPN))) - (Ca_tot * ((CELLMODEL_PARAMVALUE(VT_k_mTRPN)) + (CELLMODEL_PARAMVALUE(VT_k_mCMDN)))) +
      ((CELLMODEL_PARAMVALUE(VT_TRPN_max)) * (CELLMODEL_PARAMVALUE(VT_k_mCMDN))) + ((CELLMODEL_PARAMVALUE(VT_CMDN_max)) * (CELLMODEL_PARAMVALUE(VT_k_mTRPN)));
  const double emyo = bmyo * bmyo - 3.0 * cmyo;
  Ca_i =
      (2.0 * sqrt(emyo) *
       cos(acos((9.0 * bmyo * cmyo - 2.0 * bmyo * bmyo * bmyo -
                 27.0 * -(CELLMODEL_PARAMVALUE(VT_k_mTRPN)) * (CELLMODEL_PARAMVALUE(VT_k_mCMDN)) * Ca_tot) / (2.0 * emyo * sqrt(emyo))) *
           d1d3) - (bmyo)) * d1d3;
  Na_o += tinc * (((CELLMODEL_PARAMVALUE(VT_Na_b)) - Na_o) * (CELLMODEL_PARAMVALUE(VT_dtaudiff)) + Na_iont * (CELLMODEL_PARAMVALUE(VT_AdVcF)));
  K_o += tinc * (((CELLMODEL_PARAMVALUE(VT_K_b)) - K_o) * (CELLMODEL_PARAMVALUE(VT_dtaudiff)) + K_iont * (CELLMODEL_PARAMVALUE(VT_AdVcF)));
  Ca_o += tinc * (((CELLMODEL_PARAMVALUE(VT_Ca_b)) - Ca_o) * (CELLMODEL_PARAMVALUE(VT_dtaudiff)) + Ca_iont * (CELLMODEL_PARAMVALUE(VT_AdVcFd2)));
  if ((CSQN >= (CELLMODEL_PARAMVALUE(VT_CSQN_th))) && (t_jsrol > 50.0)) {
    grelbarjsrol = 4;
    t_jsrol = 0.0;
  }
  Ca_iontold = Ca_iont;
  dCa_iont = dCa_iontnew;
  return -tinc * I_ion * (CELLMODEL_PARAMVALUE(VT_dC_m));
} // LuoRudy::Calc

void LuoRudy::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' '
          << m << ' ' << h << ' ' << j << ' '
          << d << ' ' << f << ' '
          << b << ' ' << g << ' '
          << Xr << ' ' << Xs1 << ' ' << Xs2 << ' '
          << ato << ' ' << ito << ' '
          << Ca_i << ' ' << Ca_o << ' '
          << Na_i << ' ' << Na_o << ' '
          << K_i << ' ' << K_o << ' '
          << Ca_JSR << ' ' << Ca_NSR << ' ';
}

void LuoRudy::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double dCai = 1.0 / (double) Ca_i;
  const double dNai = 1.0 / (double) Na_i;
  const double VmE_K = V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(K_o / K_i));
  const double dffCa = d * f * (CELLMODEL_PARAMVALUE(VT_k_mCa)) / ((CELLMODEL_PARAMVALUE(VT_k_mCa)) + Ca_i);
  const double I_Namax = pP->C_NaK[Vi] * ((CELLMODEL_PARAMVALUE(VT_KgN)) * Na_i * pP->expV[Vi] - Na_o);
  const double I_Kmax = pP->C_NaK[Vi] * ((CELLMODEL_PARAMVALUE(VT_KgN)) * K_i * pP->expV[Vi] - K_o);
  const double g_Kall = sqrt(K_o * .18518519) * VmE_K;
  const double NNNOCI = Na_o * Na_o * Na_o * Ca_i;
  const double NNNICO = Na_i * Na_i * Na_i * Ca_o;
  const double nstemp = (CELLMODEL_PARAMVALUE(VT_P_nsCa)) / (1.0 + (CELLMODEL_PARAMVALUE(VT_kkk_mnsCa)) * dCai * dCai * dCai);
  const double aki = 1.02 / (1 + exp(0.2385 * (VmE_K - 59.215)));
  const double bki = (0.49124 * exp(0.08032 * (VmE_K + 5.476)) + exp(0.06175 * (VmE_K - 594.31))) /
                     (1 + exp(-0.5143 * (VmE_K + 4.753)));
  tempstr << CELLMODEL_PARAMVALUE(VT_g_Na) * m * m * m * h * j * (V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(Na_o * dNai)))
          << ' '  // I_Na
          << CELLMODEL_PARAMVALUE(VT_g_Nab) * (V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log(Na_o * dNai))) << ' ' // I_Nab
          << dffCa * pP->C_Ca[Vi] * (Ca_i * pP->expV_2[Vi] - Ca_o) << ' ' // I_Ca
          << dffCa * (CELLMODEL_PARAMVALUE(VT_P_Na)) * I_Namax << ' ' // I_CaNa
          << CELLMODEL_PARAMVALUE(VT_g_CaT) * b * b * g * (V_int - ((CELLMODEL_PARAMVALUE(VT_RTd2F)) * log(Ca_o * dCai))) << ' ' // I_CaT
          << CELLMODEL_PARAMVALUE(VT_g_Cab) * (V_int - ((CELLMODEL_PARAMVALUE(VT_RTd2F)) * log(Ca_o * dCai))) << ' ' // I_Cab
          << g_Kall * Xr * pP->Rinf[Vi] << ' ' // I_Kr
          << .433 *
             (1.0 + .6 /
                    (1 +
                     pow((double) (.000038 * dCai),
                         1.4))) * Xs1 * Xs2 * (V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) *
                                                        log((K_o + (CELLMODEL_PARAMVALUE(VT_P_NaK)) * Na_o) /
                                                            (K_i + (CELLMODEL_PARAMVALUE(VT_P_NaK)) * Na_i))))
          << ' ' // I_Ks
          << (CELLMODEL_PARAMVALUE(VT_g_K1)) * g_Kall * aki / (aki + bki) << ' ' // I_K1
          << pP->Kp[Vi] * VmE_K << ' ' // I_Kp
          << (CELLMODEL_PARAMVALUE(VT_g_KNa)) /
             (1.0 + pow((double) ((CELLMODEL_PARAMVALUE(VT_kdk_Na)) * dNai), 2.8)) *
             (.8 - .65 / (1.0 + exp((V + 125.0) * .0666666666666))) * VmE_K << ' ' // I_KNa
          << (CELLMODEL_PARAMVALUE(VT_I_Katpmax)) * (pow((double) (K_o * .25), (double) (CELLMODEL_PARAMVALUE(VT_natp)))) * VmE_K
          << ' ' // I_Katp
          << (CELLMODEL_PARAMVALUE(VT_g_to)) * ato * ato * ato * ito *
             (V_int - ((CELLMODEL_PARAMVALUE(VT_RTdF)) * log((.082 * Na_o + K_o) / (.082 * Na_i + K_i))))
          << ' ' // I_to
          << 2.5 * pP->etaV[Vi] * ((pP->expV[Vi] * NNNICO - NNNOCI) /
                                   (1.0 + pP->etaV[Vi] * (pP->expV[Vi] * NNNICO + NNNOCI)))
          << ' ' // I_NaCa
          << (CELLMODEL_PARAMVALUE(VT_I_NaKmax)) /
             (1.0 + .1245 * pP->expV_mk1[Vi] + (exp(Na_o * 0.014858841) - 1.0) * pP->expVm[Vi]) /
             (1.0 + (CELLMODEL_PARAMVALUE(VT_kk_mNai)) * dNai * dNai) * K_o / (K_o + (CELLMODEL_PARAMVALUE(VT_k_mKo))) << ' ' // I_NaK
          << I_Kmax * nstemp << ' ' // I_nsK
          << I_Namax * nstemp << ' ' // I_nsNa
          << (CELLMODEL_PARAMVALUE(VT_I_pCamax)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mpCa))) << ' ' // I_pCa
          << (Ca_NSR - Ca_JSR) * (CELLMODEL_PARAMVALUE(VT_dt_tr)) << ' ' // I_tr
          << (CELLMODEL_PARAMVALUE(VT_IupdCa)) * Ca_NSR << ' ' // I_leak
          << (CELLMODEL_PARAMVALUE(VT_I_upmax)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mup))) << ' '; // I_up
} // LuoRudy::LongPrint

void LuoRudy::GetParameterNames(vector<string> &getpara) {
  const int numpara = 20;
  const string ParaNames[numpara] =
      {"m", "h", "j", "d", "f", "b", "g", "Xr",
       "Xs1",
       "Xs2",
       "ato",
       "ito", "Ca_i", "Ca_o", "Na_i",
       "Na_o", "K_i", "K_o", "Ca_JSR", "Ca_NSR"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void LuoRudy::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 21;
  const string ParaNames[numpara] =
      {"I_Na", "I_bNa", "I_Ca", "I_CaNa", "I_CaT", "I_bCa",
       "I_Kr",
       "I_Ks",
       "I_K1", "I_pK", "I_KNa",
       "I_Katp", "I_to", "I_NaCa", "I_NaK", "I_nsK", "I_nsNa",
       "I_pCa",
       "I_tr",
       "I_leak", "I_up"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
