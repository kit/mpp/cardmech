/*      File: PerryEtAl.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
        Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
        send comments to dw@ibt.uka.de  */

#ifndef PERRYETAL
#define PERRYETAL

#include <PerryEtAlParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_PerryEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pIMW->P[NS_PerryEtAlParameters::a].value
#endif // ifdef HETERO

class PerryEtAl : public vbElphyModel<ML_CalcType> {
public:
  double Ki;
  double C0Kr;
  double C1Kr;
  double C2Kr;
  double OKr;
  double IKr;
  double C0Kr2;
  double C1Kr2;
  double C2Kr2;
  double OKr2;
  double IKr2;
  double Qb;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO
  PerryEtAlParameters *pIMW;

  PerryEtAl(PerryEtAlParameters *pIMWArg);

  ~PerryEtAl();

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual void Init();

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vmyo); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp);  /*0.;*/ }

  virtual inline ML_CalcType GetStimTime() { return 0.; }

  virtual inline ML_CalcType GetVm() { return -.09066; }

  virtual inline ML_CalcType GetCai() { return 0; }

  virtual inline ML_CalcType GetCao() { return 0; }

  virtual inline ML_CalcType GetNai() { return 0; }

  virtual inline ML_CalcType GetNao() { return 0; }

  virtual inline ML_CalcType GetKi() { return Ki; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_Ko); }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return (ML_CalcType *) &Ki; }

  virtual inline void SetCai(ML_CalcType val) {}

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);

  virtual ML_CalcType
  Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim, ML_CalcType stretch, int euler);

  virtual int GetNumStatus();

  virtual void GetStatus(double *p) const;

  virtual void SetStatus(const double *p);
}; // class PerryEtAl
#endif // ifndef PERRYETAL
