/* File: ShannonEtAl.cpp
    Robin Moss - UHZ Freiburg/Bad Krozingen
    Based on https://models.physiomeproject.org/exposure/aeec124feedddcd9139685b2ff1131ae
    xml can also be found in the originals folder*/

#include <cstdio>
#include <cmath>
#include "ShannonEtAl.h"

ShannonEtAl::ShannonEtAl(ShannonEtAlParameters *pp) {
  pSP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pSP, NS_ShannonEtAlParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

ShannonEtAl::~ShannonEtAl() {}

#ifdef HETERO

inline bool ShannonEtAl::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool ShannonEtAl::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int ShannonEtAl::GetSize(void) {
  return sizeof(ShannonEtAl) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(ShannonEtAlParameters *)
         #ifdef HETERO
         - sizeof(ParameterSwitch *)
#endif // ifdef HETERO
      ;
}

inline unsigned char ShannonEtAl::getSpeed(ML_CalcType adVm) {
  return (unsigned char) 5;
}

void ShannonEtAl::Init() {
  m = (CELLMODEL_PARAMVALUE(VT_m_init));
  h = (CELLMODEL_PARAMVALUE(VT_h_init));
  j = (CELLMODEL_PARAMVALUE(VT_j_init));
  Xr = (CELLMODEL_PARAMVALUE(VT_Xr_init));
  Xs = (CELLMODEL_PARAMVALUE(VT_Xs_init));
  Xtos = (CELLMODEL_PARAMVALUE(VT_Xtos_init));
  Ytos = (CELLMODEL_PARAMVALUE(VT_Ytos_init));
  Rtos = (CELLMODEL_PARAMVALUE(VT_Rtos_init));
  Xtof = (CELLMODEL_PARAMVALUE(VT_Xtof_init));
  Ytof = (CELLMODEL_PARAMVALUE(VT_Ytof_init));
  d = (CELLMODEL_PARAMVALUE(VT_d_init));
  f = (CELLMODEL_PARAMVALUE(VT_f_init));
  fCaB_jct = (CELLMODEL_PARAMVALUE(VT_fCaB_jct_init));
  fCaB_SL = (CELLMODEL_PARAMVALUE(VT_fCaB_SL_init));
  R = (CELLMODEL_PARAMVALUE(VT_R_init));
  O = (CELLMODEL_PARAMVALUE(VT_O_init));
  I = (CELLMODEL_PARAMVALUE(VT_I_init));
  Na_jct_buf = (CELLMODEL_PARAMVALUE(VT_Na_jct_buf_init));
  Na_SL_buf = (CELLMODEL_PARAMVALUE(VT_Na_SL_buf_init));
  Na_jct = (CELLMODEL_PARAMVALUE(VT_Na_jct_init));
  Na_SL = (CELLMODEL_PARAMVALUE(VT_Na_SL_init));
  Na_i = (CELLMODEL_PARAMVALUE(VT_Na_i_init));
  Ca_Calsequestrin = (CELLMODEL_PARAMVALUE(VT_Ca_Calsequestrin_init));
  Ca_SLB_SL = (CELLMODEL_PARAMVALUE(VT_Ca_SLB_SL_init));
  Ca_SLB_jct = (CELLMODEL_PARAMVALUE(VT_Ca_SLB_jct_init));
  Ca_SLHigh_SL = (CELLMODEL_PARAMVALUE(VT_Ca_SLHigh_SL_init));
  Ca_SLHigh_jct = (CELLMODEL_PARAMVALUE(VT_Ca_SLHigh_jct_init));
  Ca_SR = (CELLMODEL_PARAMVALUE(VT_Ca_SR_init));
  Ca_jct = (CELLMODEL_PARAMVALUE(VT_Ca_jct_init));
  Ca_SL = (CELLMODEL_PARAMVALUE(VT_Ca_SL_init));
  Ca_i = (CELLMODEL_PARAMVALUE(VT_Ca_i_init));
  Ca_TroponinC = (CELLMODEL_PARAMVALUE(VT_Ca_TroponinC_init));
  Ca_TroponinC_Ca_Mg = (CELLMODEL_PARAMVALUE(VT_Ca_TroponinC_Ca_Mg_init));
  Mg_TroponinC_Ca_Mg = (CELLMODEL_PARAMVALUE(VT_Mg_TroponinC_Ca_Mg_init));
  Ca_Calmodulin = (CELLMODEL_PARAMVALUE(VT_Ca_Calmodulin_init));
  Ca_Myosin = (CELLMODEL_PARAMVALUE(VT_Ca_Myosin_init));
  Mg_Myosin = (CELLMODEL_PARAMVALUE(VT_Mg_Myosin_init));
  Ca_SRB = (CELLMODEL_PARAMVALUE(VT_Ca_SRB_init));
} // ShannonEtAl::Init

ML_CalcType
ShannonEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                  int euler) {
  ML_CalcType svolt = V * 1000;
  ML_CalcType HT = tinc * 1000;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + svolt) + .5);


  // Nernst potentials
  const ML_CalcType E_Na_jct = (((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T))) / (CELLMODEL_PARAMVALUE(VT_F))) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_jct);
  const ML_CalcType E_Na_SL = (((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T))) / (CELLMODEL_PARAMVALUE(VT_F))) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_SL);
  const ML_CalcType E_Ca_jct =
      (((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T))) / (2.00000 * (CELLMODEL_PARAMVALUE(VT_F)))) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / Ca_jct);
  const ML_CalcType E_Ca_SL =
      (((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T))) / (2.00000 * (CELLMODEL_PARAMVALUE(VT_F)))) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / Ca_SL);
  const ML_CalcType E_K = (CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_K_o)) / (CELLMODEL_PARAMVALUE(VT_K_i)));
  const ML_CalcType E_Cl = (CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Cl_i)) / (CELLMODEL_PARAMVALUE(VT_Cl_o)));
  const ML_CalcType E_Ks = (((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T))) / (CELLMODEL_PARAMVALUE(VT_F))) *
                           log(((CELLMODEL_PARAMVALUE(VT_K_o)) + (CELLMODEL_PARAMVALUE(VT_pKNa)) * (CELLMODEL_PARAMVALUE(VT_Na_o))) /
                               ((CELLMODEL_PARAMVALUE(VT_K_i)) + (CELLMODEL_PARAMVALUE(VT_pKNa)) * Na_i));

  // I_Na
  const ML_CalcType openProb = m * m * m * h * j;
  const ML_CalcType i_Na_SL = (CELLMODEL_PARAMVALUE(VT_Fx_Na_SL)) * (CELLMODEL_PARAMVALUE(VT_g_INa)) * openProb * (svolt - E_Na_SL);
  const ML_CalcType i_Na_jct = (CELLMODEL_PARAMVALUE(VT_Fx_Na_jct)) * (CELLMODEL_PARAMVALUE(VT_g_INa)) * openProb * (svolt - E_Na_jct);
  const ML_CalcType I_Na = (i_Na_jct + i_Na_SL);


  // I_NaBk
  const ML_CalcType i_NaBk_SL = (CELLMODEL_PARAMVALUE(VT_Fx_NaBk_SL)) * (CELLMODEL_PARAMVALUE(VT_g_INaBK)) * (svolt - E_Na_SL);
  const ML_CalcType i_NaBk_jct = (CELLMODEL_PARAMVALUE(VT_Fx_NaBk_jct)) * (CELLMODEL_PARAMVALUE(VT_g_INaBK)) * (svolt - E_Na_jct);
  const ML_CalcType I_NaBk = i_NaBk_jct + i_NaBk_SL;


  // INa_K
  const ML_CalcType i_NaK_SL =
      ((((CELLMODEL_PARAMVALUE(VT_Fx_NaK_SL)) * (CELLMODEL_PARAMVALUE(VT_g_NaK)) * pSP->f_NaK[Vi]) /
        (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Km_Nai)) / Na_SL, (CELLMODEL_PARAMVALUE(VT_H_NaK))))) * (CELLMODEL_PARAMVALUE(VT_K_o))) /
      ((CELLMODEL_PARAMVALUE(VT_K_o)) + (CELLMODEL_PARAMVALUE(VT_Km_Ko)));
  const ML_CalcType i_NaK_jct =
      ((((CELLMODEL_PARAMVALUE(VT_Fx_NaK_jct)) * (CELLMODEL_PARAMVALUE(VT_g_NaK)) * pSP->f_NaK[Vi]) /
        (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Km_Nai)) / Na_jct, (CELLMODEL_PARAMVALUE(VT_H_NaK))))) * (CELLMODEL_PARAMVALUE(VT_K_o))) /
      ((CELLMODEL_PARAMVALUE(VT_K_o)) + (CELLMODEL_PARAMVALUE(VT_Km_Ko)));
  const ML_CalcType I_NaK = i_NaK_jct + i_NaK_SL;


  // I_Kr
  const ML_CalcType I_Kr =
      (CELLMODEL_PARAMVALUE(VT_g_Kr)) * sqrt((CELLMODEL_PARAMVALUE(VT_K_o)) / 5.4) * Xr * pSP->rr[Vi] * (svolt - E_K);


  // I_Ks
  const ML_CalcType pCa_jct = (-1 * log10(Ca_jct / 1)) + 3;
  const ML_CalcType pCa_SL = (-1 * log10(Ca_SL / 1)) + 3;
  const ML_CalcType G_Ks_jct = (CELLMODEL_PARAMVALUE(VT_g_Ks)) * (0.057 + 0.19 / (1 + exp((-7.2 + pCa_jct) / 0.6)));
  const ML_CalcType G_Ks_SL = (CELLMODEL_PARAMVALUE(VT_g_Ks)) * (0.057 + 0.19 / (1 + exp((-7.2 + pCa_SL) / 0.6)));
  const ML_CalcType i_Ks_jct = (CELLMODEL_PARAMVALUE(VT_Fx_Ks_jct)) * G_Ks_jct * Xs * Xs * (svolt - E_Ks);
  const ML_CalcType i_Ks_SL = (CELLMODEL_PARAMVALUE(VT_Fx_Ks_SL)) * G_Ks_SL * Xs * Xs * (svolt - E_Ks);
  const ML_CalcType I_Ks = i_Ks_jct + i_Ks_SL;


  // I_Kp
  const ML_CalcType I_Kp = ((CELLMODEL_PARAMVALUE(VT_g_Kp)) * (V - E_K)) / (1 + exp(7.488 - (svolt / 5.98)));


  // I_tos
  const ML_CalcType I_tos = (CELLMODEL_PARAMVALUE(VT_g_tos)) * Xtos * (Ytos + 0.5 * Rtos) * (svolt - E_K);


  // I_tof
  const ML_CalcType I_tof = (CELLMODEL_PARAMVALUE(VT_g_tof)) * Xtof * Ytof * (V - E_K);


  // I_K1
  const ML_CalcType I_Ki =
      (CELLMODEL_PARAMVALUE(VT_g_Ki)) * sqrt((CELLMODEL_PARAMVALUE(VT_K_o)) / 5.4) * pSP->k1inf[Vi] * (svolt - (CELLMODEL_PARAMVALUE(VT_E_K)));


  // I_Cl_Ca
  const ML_CalcType I_Cl_Ca = (CELLMODEL_PARAMVALUE(VT_g_Cl)) * (svolt - E_Cl) *
                              ((CELLMODEL_PARAMVALUE(VT_Fx_Cl_jct)) / (1.00000 + (CELLMODEL_PARAMVALUE(VT_Kd_ClCa)) / Ca_jct) +
                               (CELLMODEL_PARAMVALUE(VT_Fx_Cl_SL)) / (1.00000 + (CELLMODEL_PARAMVALUE(VT_Kd_ClCa)) / Ca_SL));


  // I_CLb
  const ML_CalcType I_Clb = (CELLMODEL_PARAMVALUE(VT_g_Clbk)) * (svolt - E_Cl);


  // I_CaL
  const ML_CalcType fCa_jct = 1 - fCaB_jct;
  const ML_CalcType fCa_SL = 1 - fCaB_SL;

  const ML_CalcType temp =
      ((CELLMODEL_PARAMVALUE(VT_g_CaL)) * d * f * (CELLMODEL_PARAMVALUE(VT_Q_CaL)) * svolt * pow((CELLMODEL_PARAMVALUE(VT_F)), 2.00000)) /
      ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)));

  const ML_CalcType i_CaL_Ca_jct =
      (temp * fCa_jct * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_jct)) * (CELLMODEL_PARAMVALUE(VT_PCa)) * 4.00000 *
       ((CELLMODEL_PARAMVALUE(VT_gamma_Cai)) * Ca_jct * exp((2.00000 * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
        (CELLMODEL_PARAMVALUE(VT_gamma_Cao)) *
        (CELLMODEL_PARAMVALUE(VT_Ca_o)))) /
      (exp((2.00000 * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) - 1.00000);
  const ML_CalcType i_CaL_Na_jct =
      (temp * fCa_jct * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_jct)) * (CELLMODEL_PARAMVALUE(VT_PNa)) *
       ((CELLMODEL_PARAMVALUE(VT_gamma_Nai)) * Na_jct * exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
        (CELLMODEL_PARAMVALUE(VT_gamma_Nao)) * (CELLMODEL_PARAMVALUE(VT_Na_o)))) /
      (exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) - 1.00000);

  const ML_CalcType i_CaL_Ca_SL =
      (temp * fCa_SL * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_SL)) * (CELLMODEL_PARAMVALUE(VT_PCa)) * 4.00000 *
       ((CELLMODEL_PARAMVALUE(VT_gamma_Cai)) * Ca_SL * exp((2.00000 * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
        (CELLMODEL_PARAMVALUE(VT_gamma_Cao)) *
        (CELLMODEL_PARAMVALUE(VT_Ca_o)))) /
      (exp((2.00000 * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) - 1.00000);
  const ML_CalcType i_CaL_Na_SL =
      (temp * fCa_SL * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_SL)) * (CELLMODEL_PARAMVALUE(VT_PNa)) *
       ((CELLMODEL_PARAMVALUE(VT_gamma_Nai)) * Na_SL * exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
        (CELLMODEL_PARAMVALUE(VT_gamma_Nao)) * (CELLMODEL_PARAMVALUE(VT_Na_o)))) /
      (exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) - 1.00000);

  const ML_CalcType i_CaL_K =
      (temp * (fCa_SL * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_SL)) + fCa_jct * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_jct))) * (CELLMODEL_PARAMVALUE(VT_PK)) *
       ((CELLMODEL_PARAMVALUE(VT_gamma_Ki)) * (CELLMODEL_PARAMVALUE(VT_K_i)) * exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
        (CELLMODEL_PARAMVALUE(VT_gamma_Ko)) * (CELLMODEL_PARAMVALUE(VT_K_o)))) /
      (exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) - 1.00000);
  const ML_CalcType I_CaL = i_CaL_Ca_SL + i_CaL_Ca_jct + i_CaL_Na_SL + i_CaL_Na_jct + i_CaL_K;


  // I_NaCa
  const ML_CalcType temp_jct =
      (exp(((CELLMODEL_PARAMVALUE(VT_eta)) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) *
       pow(Na_jct,
           (CELLMODEL_PARAMVALUE(VT_HNa))) * (CELLMODEL_PARAMVALUE(VT_Ca_o)) -
       exp((((CELLMODEL_PARAMVALUE(VT_eta)) - 1.00000) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) *
       pow((CELLMODEL_PARAMVALUE(VT_Na_o)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_jct) /
      (1.00000 +
       (CELLMODEL_PARAMVALUE(VT_ksat)) * exp((((CELLMODEL_PARAMVALUE(VT_eta)) - 1.00000) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))));
  const ML_CalcType temp_SL =
      (exp(((CELLMODEL_PARAMVALUE(VT_eta)) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) *
       pow(Na_SL,
           (CELLMODEL_PARAMVALUE(VT_HNa))) * (CELLMODEL_PARAMVALUE(VT_Ca_o)) -
       exp((((CELLMODEL_PARAMVALUE(VT_eta)) - 1.00000) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) *
       pow((CELLMODEL_PARAMVALUE(VT_Na_o)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_SL) /
      (1.00000 +
       (CELLMODEL_PARAMVALUE(VT_ksat)) * exp((((CELLMODEL_PARAMVALUE(VT_eta)) - 1.00000) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))));

  const ML_CalcType Ka_SL = 1.00000 / (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Kd_act)) / Ca_SL, 3.00000));
  const ML_CalcType Ka_jct = 1.00000 / (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Kd_act)) / Ca_jct, 3.00000));

  const ML_CalcType i_NaCa_jct =
      ((CELLMODEL_PARAMVALUE(VT_Fx_NCX_jct)) * (CELLMODEL_PARAMVALUE(VT_g_NCX)) * Ka_jct * (CELLMODEL_PARAMVALUE(VT_Q_NCX)) * temp_jct) /
      ((CELLMODEL_PARAMVALUE(VT_K_mCai)) *
       pow((CELLMODEL_PARAMVALUE(VT_Na_o)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) *
       (1.00000 +
        pow(Na_jct / (CELLMODEL_PARAMVALUE(VT_K_mNai)),
            (CELLMODEL_PARAMVALUE(VT_HNa)))) +
       pow((CELLMODEL_PARAMVALUE(VT_K_mNao)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_jct * (1.00000 + Ca_jct / (CELLMODEL_PARAMVALUE(VT_K_mCai))) + (CELLMODEL_PARAMVALUE(VT_K_mCao)) *
                                                                         pow(Na_jct, (CELLMODEL_PARAMVALUE(VT_HNa))) +
       pow(Na_jct, (CELLMODEL_PARAMVALUE(VT_HNa))) * (CELLMODEL_PARAMVALUE(VT_Ca_o)) + pow((CELLMODEL_PARAMVALUE(VT_Na_o)), (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_jct);
  const ML_CalcType i_NaCa_SL =
      ((CELLMODEL_PARAMVALUE(VT_Fx_NCX_SL)) * (CELLMODEL_PARAMVALUE(VT_g_NCX)) * Ka_SL * (CELLMODEL_PARAMVALUE(VT_Q_NCX)) * temp_SL) /
      ((CELLMODEL_PARAMVALUE(VT_K_mCai)) *
       pow((CELLMODEL_PARAMVALUE(VT_Na_o)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) *
       (1.00000 +
        pow(Na_SL / (CELLMODEL_PARAMVALUE(VT_K_mNai)),
            (CELLMODEL_PARAMVALUE(VT_HNa)))) +
       pow((CELLMODEL_PARAMVALUE(VT_K_mNao)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_SL * (1.00000 + Ca_SL / (CELLMODEL_PARAMVALUE(VT_K_mCai))) + (CELLMODEL_PARAMVALUE(VT_K_mCao)) *
                                                                       pow(Na_SL, (CELLMODEL_PARAMVALUE(VT_HNa))) +
       pow(Na_SL, (CELLMODEL_PARAMVALUE(VT_HNa))) * (CELLMODEL_PARAMVALUE(VT_Ca_o)) + pow((CELLMODEL_PARAMVALUE(VT_Na_o)), (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_SL);

  const ML_CalcType I_NaCa = (i_NaCa_jct + i_NaCa_SL);


  // I_Cap
  const ML_CalcType i_CaP_jct = ((CELLMODEL_PARAMVALUE(VT_Q_SLCaP)) * (CELLMODEL_PARAMVALUE(VT_g_Cap)) * (CELLMODEL_PARAMVALUE(VT_Fx_SLCaP_jct))) /
                                (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Km)) / Ca_jct, (CELLMODEL_PARAMVALUE(VT_HCap))));
  const ML_CalcType i_CaP_SL = ((CELLMODEL_PARAMVALUE(VT_Q_SLCaP)) * (CELLMODEL_PARAMVALUE(VT_g_Cap)) * (CELLMODEL_PARAMVALUE(VT_Fx_SLCaP_SL))) /
                               (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Km)) / Ca_SL, (CELLMODEL_PARAMVALUE(VT_HCap))));
  const ML_CalcType I_Cap = (i_CaP_jct + i_CaP_SL);


  // I_Cab
  const ML_CalcType i_Cab_jct = (CELLMODEL_PARAMVALUE(VT_g_CaBK)) * (CELLMODEL_PARAMVALUE(VT_Fx_CaBk_jct)) * (svolt - E_Ca_jct);
  const ML_CalcType i_Cab_SL = (CELLMODEL_PARAMVALUE(VT_g_CaBK)) * (CELLMODEL_PARAMVALUE(VT_Fx_CaBk_SL)) * (svolt - E_Ca_SL);
  const ML_CalcType I_Cab = (i_Cab_jct + i_Cab_SL);


  // Jrel_SR
  const ML_CalcType kCaSR = (CELLMODEL_PARAMVALUE(VT_Max_SR)) - ((CELLMODEL_PARAMVALUE(VT_Max_SR)) - (CELLMODEL_PARAMVALUE(VT_Min_SR))) /
                                             (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_EC50_SR)) / Ca_SR, (CELLMODEL_PARAMVALUE(VT_HSR))));
  const ML_CalcType koSRCa = (CELLMODEL_PARAMVALUE(VT_koCa)) / kCaSR;
  const ML_CalcType kiSRCa = (CELLMODEL_PARAMVALUE(VT_kiCa)) * kCaSR;
  const ML_CalcType RI = ((1.00000 - R) - O) - I;
  const ML_CalcType j_rel_SR =
      (CELLMODEL_PARAMVALUE(VT_ks)) * O * (Ca_SR - Ca_jct); // std::cout << j_rel_SR << " " <<  O << " " << Ca_SR
  // -Ca_jct << " " << kCaSR << std::endl;


  // Jleak_SR
  const ML_CalcType j_leak_SR = (CELLMODEL_PARAMVALUE(VT_KSRleak)) * (Ca_SR - Ca_jct);


  // Jpump_SR
  const ML_CalcType j_pump_SR =
      ((CELLMODEL_PARAMVALUE(VT_Q_SRCaP)) * (CELLMODEL_PARAMVALUE(VT_KSRPump)) *
       (pow(Ca_i / (CELLMODEL_PARAMVALUE(VT_Kmf)),
            (CELLMODEL_PARAMVALUE(VT_HSRPump))) -
        pow(Ca_SR / (CELLMODEL_PARAMVALUE(VT_Kmr)),
            (CELLMODEL_PARAMVALUE(VT_HSRPump))))) / (1.00000 + pow(Ca_i / (CELLMODEL_PARAMVALUE(VT_Kmf)), (CELLMODEL_PARAMVALUE(VT_HSRPump))) +
                                  pow(Ca_SR / (CELLMODEL_PARAMVALUE(VT_Kmr)), (CELLMODEL_PARAMVALUE(VT_HSRPump))));


  // ion diffusion
  const ML_CalcType J_Na_jct_SL = (Na_jct - Na_SL) * 1.83130e-14;
  const ML_CalcType J_Na_SL_myo = (Na_SL - Na_i) * 1.63860e-12;
  const ML_CalcType J_Ca_jct_SL = (Ca_jct - Ca_SL) * 8.24130e-13;
  const ML_CalcType J_Ca_SL_myo = (Ca_SL - Ca_i) * 3.72430e-12;

  // Na_buffer
  const ML_CalcType dNa_jct_buf =
      (CELLMODEL_PARAMVALUE(VT_kon)) * Na_jct * ((CELLMODEL_PARAMVALUE(VT_Bmax_jct)) - Na_jct_buf) - (CELLMODEL_PARAMVALUE(VT_koff)) * Na_jct_buf;
  const ML_CalcType dNa_SL_buf =
      (CELLMODEL_PARAMVALUE(VT_kon)) * Na_SL * ((CELLMODEL_PARAMVALUE(VT_Bmax_SL)) - Na_SL_buf) - (CELLMODEL_PARAMVALUE(VT_koff)) * Na_SL_buf;

  // Ca_buffer
  const ML_CalcType dCalsequestrin = (CELLMODEL_PARAMVALUE(VT_kon_Calsequestrin)) * Ca_SR *
                                     (((CELLMODEL_PARAMVALUE(VT_Bmax_Calsequestrin)) * (CELLMODEL_PARAMVALUE(VT_Vol_myo))) /
                                      (CELLMODEL_PARAMVALUE(VT_Vol_SR)) - Ca_Calsequestrin) -
                                     (CELLMODEL_PARAMVALUE(VT_koff_Calsequestrin)) *
                                     Ca_Calsequestrin;

  const ML_CalcType dCa_SLB_SL = (CELLMODEL_PARAMVALUE(VT_kon_SL)) * Ca_SL *
                                 (((CELLMODEL_PARAMVALUE(VT_Bmax_SLB_SL)) * (CELLMODEL_PARAMVALUE(VT_Vol_myo))) / (CELLMODEL_PARAMVALUE(VT_Vol_SL)) -
                                  Ca_SLB_SL) - (CELLMODEL_PARAMVALUE(VT_koff_SLB)) * Ca_SLB_SL;
  const ML_CalcType dCa_SLB_jct = (CELLMODEL_PARAMVALUE(VT_kon_SL)) * Ca_jct *
                                  (((CELLMODEL_PARAMVALUE(VT_Bmax_SLB_jct)) * 0.100000 * (CELLMODEL_PARAMVALUE(VT_Vol_myo))) /
                                   (CELLMODEL_PARAMVALUE(VT_Vol_jct)) - Ca_SLB_jct) - (CELLMODEL_PARAMVALUE(VT_koff_SLB)) * Ca_SLB_jct;

  const ML_CalcType dCa_SLHigh_SL = (CELLMODEL_PARAMVALUE(VT_kon_SL)) * Ca_SL *
                                    (((CELLMODEL_PARAMVALUE(VT_Bmax_SLHigh_SL)) * (CELLMODEL_PARAMVALUE(VT_Vol_myo))) / (CELLMODEL_PARAMVALUE(VT_Vol_SL)) -
                                     Ca_SLHigh_SL) - (CELLMODEL_PARAMVALUE(VT_koff_SLHigh)) * Ca_SLHigh_SL;
  const ML_CalcType dCa_SLHigh_jct = (CELLMODEL_PARAMVALUE(VT_kon_SL)) * Ca_jct *
                                     (((CELLMODEL_PARAMVALUE(VT_Bmax_SLHigh_jct)) * 0.100000 * (CELLMODEL_PARAMVALUE(VT_Vol_myo))) /
                                      (CELLMODEL_PARAMVALUE(VT_Vol_jct)) - Ca_SLHigh_jct) - (CELLMODEL_PARAMVALUE(VT_koff_SLHigh)) *
                                                                         Ca_SLHigh_jct;

  const ML_CalcType dCa_jct_tot_bound = dCa_SLB_jct + dCa_SLHigh_jct;
  const ML_CalcType dCa_SL_tot_bound = dCa_SLB_SL + dCa_SLHigh_SL;

  const ML_CalcType i_Ca_jct_tot = (i_CaL_Ca_jct - 2.00000 * i_NaCa_jct) + i_Cab_jct + i_CaP_jct;
  const ML_CalcType i_Ca_SL_tot = (i_CaL_Ca_SL - 2.00000 * i_NaCa_SL) + i_Cab_SL + i_CaP_SL;

  const ML_CalcType dCa_TroponinC =
      (CELLMODEL_PARAMVALUE(VT_kon_TroponinC)) * Ca_i * ((CELLMODEL_PARAMVALUE(VT_Bmax_TroponinC)) - Ca_TroponinC) -
      (CELLMODEL_PARAMVALUE(VT_koff_TroponinC)) * Ca_TroponinC;
  const ML_CalcType dCa_TroponinC_Ca_Mg = (CELLMODEL_PARAMVALUE(VT_kon_TroponinC_Ca_Mg_Ca)) * Ca_i *
                                          ((CELLMODEL_PARAMVALUE(VT_Bmax_TroponinC_Ca_Mg_Ca)) -
                                           (Ca_TroponinC_Ca_Mg + Mg_TroponinC_Ca_Mg)) -
                                          (CELLMODEL_PARAMVALUE(VT_koff_TroponinC_Ca_Mg_Ca)) *
                                          Ca_TroponinC_Ca_Mg;
  const ML_CalcType dMg_TroponinC_Ca_Mg = (CELLMODEL_PARAMVALUE(VT_kon_TroponinC_Ca_Mg_Mg)) * (CELLMODEL_PARAMVALUE(VT_Mg_i)) *
                                          ((CELLMODEL_PARAMVALUE(VT_Bmax_TroponinC_Ca_Mg_Mg)) -
                                           (Ca_TroponinC_Ca_Mg + Mg_TroponinC_Ca_Mg)) -
                                          (CELLMODEL_PARAMVALUE(VT_koff_TroponinC_Ca_Mg_Mg)) *
                                          Mg_TroponinC_Ca_Mg;

  const ML_CalcType dCa_Calmodulin =
      (CELLMODEL_PARAMVALUE(VT_kon_Calmodulin)) * Ca_i * ((CELLMODEL_PARAMVALUE(VT_Bmax_Calmodulin)) - Ca_Calmodulin) -
      (CELLMODEL_PARAMVALUE(VT_koff_Calmodulin)) * Ca_Calmodulin;
  const ML_CalcType dCa_Myosin =
      (CELLMODEL_PARAMVALUE(VT_kon_Myosin_Ca)) * Ca_i * ((CELLMODEL_PARAMVALUE(VT_Bmax_Myosin_Ca)) - (Ca_Myosin + Mg_Myosin)) -
      (CELLMODEL_PARAMVALUE(VT_koff_Myosin_Ca)) * Ca_Myosin;
  const ML_CalcType dMg_Myosin = (CELLMODEL_PARAMVALUE(VT_kon_Myosin_Mg)) * (CELLMODEL_PARAMVALUE(VT_Mg_i)) *
                                 ((CELLMODEL_PARAMVALUE(VT_Bmax_Myosin_Mg)) - (Ca_Myosin + Mg_Myosin)) -
                                 (CELLMODEL_PARAMVALUE(VT_koff_Myosin_Mg)) * Mg_Myosin;
  const ML_CalcType dCa_SRB =
      (CELLMODEL_PARAMVALUE(VT_kon_SRB)) * Ca_i * ((CELLMODEL_PARAMVALUE(VT_Bmax_SRB)) - Ca_SRB) - (CELLMODEL_PARAMVALUE(VT_koff_SRB)) * Ca_SRB;

  const ML_CalcType dCa_cytosol_tot_bound =
      dCa_TroponinC_Ca_Mg + dMg_TroponinC_Ca_Mg + dCa_Calmodulin + dCa_Myosin +
      dMg_Myosin + dCa_SRB;


  // rates
  // Na
  m += HT * (pSP->am[Vi] * (1 - m) - pSP->bm[Vi] * m);
  h += HT * (pSP->ah[Vi] * (1 - h) - pSP->bh[Vi] * h);
  j += HT * (pSP->aj[Vi] * (1 - j) - pSP->bj[Vi] * j);

  // kr,ks
  Xr += HT * ((pSP->Xrinf[Vi] - Xr) / pSP->Xrtau[Vi]);
  Xs += HT * ((pSP->Xsinf[Vi] - Xs) / pSP->Xstau[Vi]);

  // tos,tof
  Xtos += HT * ((pSP->Xtosinf[Vi] - Xtos) / pSP->Xtostau[Vi]);
  Ytos += HT * ((pSP->Ytosinf[Vi] - Ytos) / pSP->Ytostau[Vi]);
  Rtos += HT * ((pSP->Rtosinf[Vi] - Rtos) / pSP->Rtostau[Vi]);
  Xtof += HT * ((pSP->Xtosinf[Vi] - Xtof) / pSP->Xtoftau[Vi]);  // Xtosinf = Xtofinf
  Ytof += HT * ((pSP->Ytosinf[Vi] - Ytof) / pSP->Ytoftau[Vi]);  // Xtosinf = Xtofinf

  // CaL
  d += HT * ((pSP->dinf[Vi] - d) / pSP->dtau[Vi]);
  f += HT * ((pSP->finf[Vi] - f) / pSP->ftau[Vi]);

  // CaL fCa_gate
  fCaB_jct += HT * (1.7 * Ca_jct * (1 - fCaB_jct) - (11.9e-3 * fCaB_jct));
  fCaB_SL += HT * (1.7 * Ca_SL * (1 - fCaB_SL) - (11.9e-3 * fCaB_SL));

  // Jrel_SR
  O += HT * ((koSRCa * pow(Ca_jct, 2.00000) * R - (CELLMODEL_PARAMVALUE(VT_kom)) * O) -
             (kiSRCa * Ca_jct * O - (CELLMODEL_PARAMVALUE(VT_kim)) * I));
  R += HT * (((CELLMODEL_PARAMVALUE(VT_kim)) * RI - kiSRCa * Ca_jct * R) -
             (koSRCa * pow(Ca_jct, 2.00000) * R - (CELLMODEL_PARAMVALUE(VT_kom)) * O));
  I += HT * ((kiSRCa * Ca_jct * O - (CELLMODEL_PARAMVALUE(VT_kim)) * I) -
             ((CELLMODEL_PARAMVALUE(VT_kom)) * I - koSRCa * pow(Ca_jct, 2.00000) * RI));


  // Na_buffer
  Na_jct_buf += HT * dNa_jct_buf;
  Na_SL_buf += HT * dNa_SL_buf;

  Na_jct += HT *
            (((-(CELLMODEL_PARAMVALUE(VT_Cm)) * (i_Na_jct + 3.00000 * i_NaCa_jct + i_NaBk_jct + 3.00000 * i_NaK_jct +
                              i_CaL_Na_jct)) /
              ((CELLMODEL_PARAMVALUE(VT_Vol_jct)) * (CELLMODEL_PARAMVALUE(VT_F))) - J_Na_jct_SL / (CELLMODEL_PARAMVALUE(VT_Vol_jct))) - dNa_jct_buf);

  Na_SL += HT *
           (((-(CELLMODEL_PARAMVALUE(VT_Cm)) *
              (i_Na_SL + 3.00000 * i_NaCa_SL + i_NaBk_SL + 3.00000 * i_NaK_SL + i_CaL_Na_SL)) /
             ((CELLMODEL_PARAMVALUE(VT_Vol_SL)) * (CELLMODEL_PARAMVALUE(VT_F))) +
             (J_Na_jct_SL - J_Na_SL_myo) / (CELLMODEL_PARAMVALUE(VT_Vol_SL))) - dNa_SL_buf);

  Na_i += HT * (J_Na_SL_myo / (CELLMODEL_PARAMVALUE(VT_Vol_myo)));

  // Ca_buffer
  Ca_Calsequestrin += HT * dCalsequestrin;
  Ca_SLB_SL += HT * dCa_SLB_SL;
  Ca_SLB_jct += HT * dCa_SLB_jct;
  Ca_SLHigh_SL += HT * dCa_SLHigh_SL;
  Ca_SLHigh_jct += HT * dCa_SLHigh_jct;


  Ca_SR += HT * ((j_pump_SR - ((j_leak_SR * (CELLMODEL_PARAMVALUE(VT_Vol_myo))) / (CELLMODEL_PARAMVALUE(VT_Vol_SR)) + j_rel_SR)) -
                 dCalsequestrin);

  Ca_jct += HT *
            ((((-i_Ca_jct_tot * (CELLMODEL_PARAMVALUE(VT_Cm))) / ((CELLMODEL_PARAMVALUE(VT_Vol_jct)) * 2.00000 * (CELLMODEL_PARAMVALUE(VT_F))) -
               J_Ca_jct_SL / (CELLMODEL_PARAMVALUE(VT_Vol_jct))) +
              (j_rel_SR * (CELLMODEL_PARAMVALUE(VT_Vol_SR))) / (CELLMODEL_PARAMVALUE(VT_Vol_jct)) +
              (j_leak_SR * (CELLMODEL_PARAMVALUE(VT_Vol_myo))) / (CELLMODEL_PARAMVALUE(VT_Vol_jct))) - 1.00000 *
                                                                 dCa_jct_tot_bound);

  Ca_SL += HT *
           (((-i_Ca_SL_tot * (CELLMODEL_PARAMVALUE(VT_Cm))) / ((CELLMODEL_PARAMVALUE(VT_Vol_SL)) * 2.00000 * (CELLMODEL_PARAMVALUE(VT_F))) +
             (J_Ca_jct_SL - J_Ca_SL_myo) / (CELLMODEL_PARAMVALUE(VT_Vol_SL))) -
            1.00000 * dCa_SL_tot_bound);

  Ca_i += HT *
          (((-j_pump_SR * (CELLMODEL_PARAMVALUE(VT_Vol_SR))) / (CELLMODEL_PARAMVALUE(VT_Vol_myo)) + J_Ca_SL_myo / (CELLMODEL_PARAMVALUE(VT_Vol_myo))) -
           1.00000 * dCa_cytosol_tot_bound);

  // cytosolic Ca_buffer

  Ca_TroponinC += HT * dCa_TroponinC;
  Ca_TroponinC_Ca_Mg += HT * dCa_TroponinC_Ca_Mg;
  Mg_TroponinC_Ca_Mg += HT * dMg_TroponinC_Ca_Mg;
  Ca_Calmodulin += HT * dCa_Calmodulin;
  Ca_Myosin += HT * dCa_Myosin;
  Mg_Myosin += HT * dMg_Myosin;
  Ca_SRB += HT * dCa_SRB;

  const ML_CalcType I_tot =
      -i_external + I_Na + I_NaBk + I_NaK + I_Kr + I_Ks + I_tos + I_tof + I_Ki + I_NaCa +
      I_Cl_Ca + I_Clb + I_CaL + I_Cab + I_Cap + I_Kp;

  // cerr<< i_external << endl;
  return tinc * (-I_tot);
} // ShannonEtAl::Calc

void ShannonEtAl::Print(ostream &tempstr, double tArg, ML_CalcType V) {
  tempstr << tArg << ' ' << V << ' ' << m << ' ' << h << ' ' << j << ' ' << Xr << ' ' << Xs << ' '
          << Xtos << ' ' << Ytos << ' ' << Rtos << ' ' <<
          Xtof
          << ' ' << Ytof << ' ' << d << ' ' << f << ' ' << fCaB_jct << ' ' << fCaB_SL << ' ' << R
          << ' ' << O << ' ' << I << ' ' << Na_jct_buf
          << ' ' << Na_SL_buf << ' ' << Na_jct << ' ' << Na_SL << ' ' << Na_i << ' ' << Ca_SLB_SL
          << ' ' << Ca_SLB_jct << ' ' <<
          Ca_SLHigh_SL << ' ' << Ca_SLHigh_jct << ' ' << Ca_Calsequestrin
          << ' ' << Ca_SR << ' ' << Ca_jct << ' ' << Ca_SL << ' ' << Ca_i << ' ' << Ca_TroponinC
          << ' ' << Ca_TroponinC_Ca_Mg << ' ' <<
          Mg_TroponinC_Ca_Mg << ' ' << Ca_Calmodulin
          << ' ' << Ca_Myosin << ' ' << Mg_Myosin << ' ' << Ca_SRB;
}

void ShannonEtAl::LongPrint(ostream &tempstr, double tArg, ML_CalcType V) {
  Print(tempstr, tArg, V);
  const ML_CalcType svolt = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + svolt) + .5);


  // Nernst potentials
  const ML_CalcType E_Na_jct = (((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T))) / (CELLMODEL_PARAMVALUE(VT_F))) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_jct);
  const ML_CalcType E_Na_SL = (CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Na_o)) / Na_SL);
  const ML_CalcType E_Ca_jct = 0.5 * (CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / Ca_jct);
  const ML_CalcType E_Ca_SL = 0.5 * (CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / Ca_SL);
  const ML_CalcType E_K = (CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_K_o)) / (CELLMODEL_PARAMVALUE(VT_K_i)));
  const ML_CalcType E_Cl = (CELLMODEL_PARAMVALUE(VT_RTdF)) * log((CELLMODEL_PARAMVALUE(VT_Cl_o)) / (CELLMODEL_PARAMVALUE(VT_Cl_i)));

  //    const ML_CalcType E_Ks      =       (CELLMODEL_PARAMVALUE(VT_RTdF)) * log(((CELLMODEL_PARAMVALUE(VT_K_o)) + (((CELLMODEL_PARAMVALUE(VT_pKNa)) * ((CELLMODEL_PARAMVALUE(VT_Na_o)))) )) /
  // ((CELLMODEL_PARAMVALUE(VT_K_i)) + (((CELLMODEL_PARAMVALUE(VT_pKNa)) * (Na_i)) )) );
  const ML_CalcType E_Ks = (CELLMODEL_PARAMVALUE(VT_RTdF)) *
                           log(((CELLMODEL_PARAMVALUE(VT_K_o)) + (CELLMODEL_PARAMVALUE(VT_pKNa)) * (CELLMODEL_PARAMVALUE(VT_Na_o))) /
                               ((CELLMODEL_PARAMVALUE(VT_K_i)) + (CELLMODEL_PARAMVALUE(VT_pKNa)) * Na_i));

  // I_Na
  const ML_CalcType openProb = m * m * m * h * j;
  const ML_CalcType i_Na_SL = (CELLMODEL_PARAMVALUE(VT_Fx_Na_SL)) * (CELLMODEL_PARAMVALUE(VT_g_INa)) * openProb * (svolt - E_Na_SL);
  const ML_CalcType i_Na_jct = (CELLMODEL_PARAMVALUE(VT_Fx_Na_jct)) * (CELLMODEL_PARAMVALUE(VT_g_INa)) * openProb * (svolt - E_Na_jct);
  const ML_CalcType I_Na = (i_Na_jct + i_Na_SL);


  // I_NaBk
  const ML_CalcType i_NaBk_SL = (CELLMODEL_PARAMVALUE(VT_Fx_NaBk_SL)) * (CELLMODEL_PARAMVALUE(VT_g_INaBK)) * (svolt - E_Na_SL);
  const ML_CalcType i_NaBk_jct = (CELLMODEL_PARAMVALUE(VT_Fx_NaBk_jct)) * (CELLMODEL_PARAMVALUE(VT_g_INaBK)) * (svolt - E_Na_jct);
  const ML_CalcType I_NaBk = i_NaBk_jct + i_NaBk_SL;


  // INa_K
  const ML_CalcType i_NaK_SL =
      ((((CELLMODEL_PARAMVALUE(VT_Fx_NaK_SL)) * (CELLMODEL_PARAMVALUE(VT_g_NaK)) * pSP->f_NaK[Vi]) /
        (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Km_Nai)) / Na_SL, (CELLMODEL_PARAMVALUE(VT_H_NaK))))) * (CELLMODEL_PARAMVALUE(VT_K_o))) /
      ((CELLMODEL_PARAMVALUE(VT_K_o)) + (CELLMODEL_PARAMVALUE(VT_Km_Ko)));
  const ML_CalcType i_NaK_jct =
      ((((CELLMODEL_PARAMVALUE(VT_Fx_NaK_jct)) * (CELLMODEL_PARAMVALUE(VT_g_NaK)) * pSP->f_NaK[Vi]) /
        (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Km_Nai)) / Na_jct, (CELLMODEL_PARAMVALUE(VT_H_NaK))))) * (CELLMODEL_PARAMVALUE(VT_K_o))) /
      ((CELLMODEL_PARAMVALUE(VT_K_o)) + (CELLMODEL_PARAMVALUE(VT_Km_Ko)));
  const ML_CalcType I_NaK = i_NaK_jct + i_NaK_SL;


  // I_Kr
  const ML_CalcType I_Kr =
      (CELLMODEL_PARAMVALUE(VT_g_Kr)) * sqrt((CELLMODEL_PARAMVALUE(VT_K_o)) / 5.4) * Xr * pSP->rr[Vi] * (svolt - E_K);


  // I_Ks
  const ML_CalcType pCa_jct = (-1 * log10(Ca_jct / 1)) + 3;
  const ML_CalcType pCa_SL = (-1 * log10(Ca_SL / 1)) + 3;
  const ML_CalcType G_Ks_jct = (CELLMODEL_PARAMVALUE(VT_g_Ks)) * (0.057 + 0.19 / (1 + exp((-7.2 + pCa_jct) / 0.6)));
  const ML_CalcType G_Ks_SL = (CELLMODEL_PARAMVALUE(VT_g_Ks)) * (0.057 + 0.19 / (1 + exp((-7.2 + pCa_SL) / 0.6)));
  const ML_CalcType i_Ks_jct = (CELLMODEL_PARAMVALUE(VT_Fx_Ks_jct)) * G_Ks_jct * Xs * Xs * (svolt - E_Ks);
  const ML_CalcType i_Ks_SL = (CELLMODEL_PARAMVALUE(VT_Fx_Ks_SL)) * G_Ks_SL * Xs * Xs * (svolt - E_Ks);
  const ML_CalcType I_Ks = i_Ks_jct + i_Ks_SL;


  // I_Kp
  const ML_CalcType I_Kp = ((CELLMODEL_PARAMVALUE(VT_g_Kp)) * (V - E_K)) / (1 + exp(7.488 - (svolt / 5.98)));


  // I_tos
  const ML_CalcType I_tos = (CELLMODEL_PARAMVALUE(VT_g_tos)) * Xtos * (Ytos + 0.5 * Rtos) * (svolt - E_K);


  // I_tof
  const ML_CalcType I_tof = (CELLMODEL_PARAMVALUE(VT_g_tof)) * Xtof * Ytof * (V - E_K);


  // I_K1
  const ML_CalcType I_Ki =
      (CELLMODEL_PARAMVALUE(VT_g_Ki)) * sqrt((CELLMODEL_PARAMVALUE(VT_K_o)) / 5.4) * pSP->k1inf[Vi] * (svolt - (CELLMODEL_PARAMVALUE(VT_E_K)));


  // I_Cl_Ca
  const ML_CalcType I_Cl_Ca = (CELLMODEL_PARAMVALUE(VT_g_Cl)) * (svolt - E_Cl) *
                              ((CELLMODEL_PARAMVALUE(VT_Fx_Cl_jct)) / (1.00000 + (CELLMODEL_PARAMVALUE(VT_Kd_ClCa)) / Ca_jct) +
                               (CELLMODEL_PARAMVALUE(VT_Fx_Cl_SL)) / (1.00000 + (CELLMODEL_PARAMVALUE(VT_Kd_ClCa)) / Ca_SL));


  // I_CLb
  const ML_CalcType I_Clb = (CELLMODEL_PARAMVALUE(VT_g_Clbk)) * (svolt - E_Cl);


  // I_CaL
  const ML_CalcType fCa_jct = 1 - fCaB_jct;
  const ML_CalcType fCa_SL = 1 - fCaB_SL;

  const ML_CalcType temp =
      ((CELLMODEL_PARAMVALUE(VT_g_CaL)) * d * f * (CELLMODEL_PARAMVALUE(VT_Q_CaL)) * svolt * pow((CELLMODEL_PARAMVALUE(VT_F)), 2.00000)) /
      ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)));

  const ML_CalcType i_CaL_Ca_jct =
      (temp * fCa_jct * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_jct)) * (CELLMODEL_PARAMVALUE(VT_PCa)) * 4.00000 *
       ((CELLMODEL_PARAMVALUE(VT_gamma_Cai)) * Ca_jct * exp((2.00000 * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
        (CELLMODEL_PARAMVALUE(VT_gamma_Cao)) *
        (CELLMODEL_PARAMVALUE(VT_Ca_o)))) /
      (exp((2.00000 * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) - 1.00000);

  const ML_CalcType i_CaL_Na_jct =
      (temp * fCa_jct * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_jct)) * (CELLMODEL_PARAMVALUE(VT_PNa)) *
       ((CELLMODEL_PARAMVALUE(VT_gamma_Nai)) * Na_jct * exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
        (CELLMODEL_PARAMVALUE(VT_gamma_Nao)) * (CELLMODEL_PARAMVALUE(VT_Na_o)))) /
      (exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) - 1.00000);

  const ML_CalcType i_CaL_Ca_SL =
      (temp * fCa_SL * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_SL)) * (CELLMODEL_PARAMVALUE(VT_PCa)) * 4.00000 *
       ((CELLMODEL_PARAMVALUE(VT_gamma_Cai)) * Ca_SL * exp((2.00000 * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
        (CELLMODEL_PARAMVALUE(VT_gamma_Cao)) *
        (CELLMODEL_PARAMVALUE(VT_Ca_o)))) /
      (exp((2.00000 * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) - 1.00000);

  const ML_CalcType i_CaL_Na_SL =
      (temp * fCa_SL * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_SL)) * (CELLMODEL_PARAMVALUE(VT_PNa)) *
       ((CELLMODEL_PARAMVALUE(VT_gamma_Nai)) * Na_SL * exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
        (CELLMODEL_PARAMVALUE(VT_gamma_Nao)) * (CELLMODEL_PARAMVALUE(VT_Na_o)))) /
      (exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) - 1.00000);

  const ML_CalcType i_CaL_K =
      (temp * (fCa_SL * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_SL)) + fCa_jct * (CELLMODEL_PARAMVALUE(VT_Fx_ICaL_jct))) * (CELLMODEL_PARAMVALUE(VT_PK)) *
       ((CELLMODEL_PARAMVALUE(VT_gamma_Ki)) * (CELLMODEL_PARAMVALUE(VT_K_i)) * exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
        (CELLMODEL_PARAMVALUE(VT_gamma_Ko)) * (CELLMODEL_PARAMVALUE(VT_K_o)))) /
      (exp((svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) - 1.00000);


  const ML_CalcType I_CaL = (i_CaL_Ca_jct + i_CaL_Na_jct + i_CaL_Ca_SL + i_CaL_Na_SL + i_CaL_K);


  // I_NaCa
  const ML_CalcType temp_jct =
      (exp(((CELLMODEL_PARAMVALUE(VT_eta)) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) *
       pow(Na_jct,
           (CELLMODEL_PARAMVALUE(VT_HNa))) * (CELLMODEL_PARAMVALUE(VT_Ca_o)) -
       exp((((CELLMODEL_PARAMVALUE(VT_eta)) - 1.00000) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) *
       pow((CELLMODEL_PARAMVALUE(VT_Na_o)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_jct) /
      (1.00000 +
       (CELLMODEL_PARAMVALUE(VT_ksat)) * exp((((CELLMODEL_PARAMVALUE(VT_eta)) - 1.00000) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))));

  const ML_CalcType temp_SL =
      (exp(((CELLMODEL_PARAMVALUE(VT_eta)) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) *
       pow(Na_SL,
           (CELLMODEL_PARAMVALUE(VT_HNa))) * (CELLMODEL_PARAMVALUE(VT_Ca_o)) -
       exp((((CELLMODEL_PARAMVALUE(VT_eta)) - 1.00000) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) *
       pow((CELLMODEL_PARAMVALUE(VT_Na_o)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_SL) /
      (1.00000 +
       (CELLMODEL_PARAMVALUE(VT_ksat)) * exp((((CELLMODEL_PARAMVALUE(VT_eta)) - 1.00000) * svolt * (CELLMODEL_PARAMVALUE(VT_F))) / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))));

  const ML_CalcType Ka_SL = 1.00000 / (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Kd_act)) / Ca_SL, 3.00000));
  const ML_CalcType Ka_jct = 1.00000 / (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Kd_act)) / Ca_jct, 3.00000));

  const ML_CalcType i_NaCa_jct =
      ((CELLMODEL_PARAMVALUE(VT_Fx_NCX_jct)) * (CELLMODEL_PARAMVALUE(VT_g_NCX)) * Ka_jct * (CELLMODEL_PARAMVALUE(VT_Q_NCX)) * temp_jct) /
      ((CELLMODEL_PARAMVALUE(VT_K_mCai)) *
       pow((CELLMODEL_PARAMVALUE(VT_Na_o)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) *
       (1.00000 +
        pow(Na_jct / (CELLMODEL_PARAMVALUE(VT_K_mNai)),
            (CELLMODEL_PARAMVALUE(VT_HNa)))) +
       pow((CELLMODEL_PARAMVALUE(VT_K_mNao)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_jct * (1.00000 + Ca_jct / (CELLMODEL_PARAMVALUE(VT_K_mCai))) + (CELLMODEL_PARAMVALUE(VT_K_mCao)) *
                                                                         pow(Na_jct, (CELLMODEL_PARAMVALUE(VT_HNa))) +
       pow(Na_jct, (CELLMODEL_PARAMVALUE(VT_HNa))) * (CELLMODEL_PARAMVALUE(VT_Ca_o)) + pow((CELLMODEL_PARAMVALUE(VT_Na_o)), (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_jct);

  const ML_CalcType i_NaCa_SL =
      ((CELLMODEL_PARAMVALUE(VT_Fx_NCX_SL)) * (CELLMODEL_PARAMVALUE(VT_g_NCX)) * Ka_SL * (CELLMODEL_PARAMVALUE(VT_Q_NCX)) * temp_SL) /
      ((CELLMODEL_PARAMVALUE(VT_K_mCai)) *
       pow((CELLMODEL_PARAMVALUE(VT_Na_o)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) *
       (1.00000 +
        pow(Na_SL / (CELLMODEL_PARAMVALUE(VT_K_mNai)),
            (CELLMODEL_PARAMVALUE(VT_HNa)))) +
       pow((CELLMODEL_PARAMVALUE(VT_K_mNao)),
           (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_SL * (1.00000 + Ca_SL / (CELLMODEL_PARAMVALUE(VT_K_mCai))) + (CELLMODEL_PARAMVALUE(VT_K_mCao)) *
                                                                       pow(Na_SL, (CELLMODEL_PARAMVALUE(VT_HNa))) +
       pow(Na_SL, (CELLMODEL_PARAMVALUE(VT_HNa))) * (CELLMODEL_PARAMVALUE(VT_Ca_o)) + pow((CELLMODEL_PARAMVALUE(VT_Na_o)), (CELLMODEL_PARAMVALUE(VT_HNa))) * Ca_SL);

  const ML_CalcType I_NaCa = (i_NaCa_jct + i_NaCa_SL);


  // I_Cap
  const ML_CalcType i_CaP_jct = ((CELLMODEL_PARAMVALUE(VT_Q_SLCaP)) * (CELLMODEL_PARAMVALUE(VT_g_Cap)) * (CELLMODEL_PARAMVALUE(VT_Fx_SLCaP_jct))) /
                                (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Km)) / Ca_jct, (CELLMODEL_PARAMVALUE(VT_HCap))));
  const ML_CalcType i_CaP_SL = ((CELLMODEL_PARAMVALUE(VT_Q_SLCaP)) * (CELLMODEL_PARAMVALUE(VT_g_Cap)) * (CELLMODEL_PARAMVALUE(VT_Fx_SLCaP_SL))) /
                               (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_Km)) / Ca_SL, (CELLMODEL_PARAMVALUE(VT_HCap))));
  const ML_CalcType I_Cap = (i_CaP_jct + i_CaP_SL);


  // I_Cab
  const ML_CalcType i_Cab_jct = (CELLMODEL_PARAMVALUE(VT_g_CaBK)) * (CELLMODEL_PARAMVALUE(VT_Fx_CaBk_jct)) * (svolt - E_Ca_jct);
  const ML_CalcType i_Cab_SL = (CELLMODEL_PARAMVALUE(VT_g_CaBK)) * (CELLMODEL_PARAMVALUE(VT_Fx_CaBk_SL)) * (svolt - E_Ca_SL);
  const ML_CalcType I_Cab = (i_Cab_jct + i_Cab_SL);

  const ML_CalcType I_mem =
      I_Na + I_NaBk + I_NaK + I_Kr + I_Ks + I_tos + I_tof + I_Ki + I_NaCa + I_Cl_Ca + I_Clb +
      I_CaL + I_Cab + I_Cap + I_Kp;
  tempstr << ' ' << I_Na << ' ' << I_NaBk << ' ' << I_NaK << ' ' << I_Kr << ' ' << I_Ks << ' '
          << I_tos << ' ' << I_tof << ' ' << I_Ki <<
          ' ' << I_NaCa << ' ' << I_Cl_Ca << ' ' << I_Clb << ' ' << I_CaL << ' ' << I_Cab << ' '
          << I_Cap << ' ' << I_Kp << ' ' << I_mem <<
          ' ';
} // ShannonEtAl::LongPrint

void ShannonEtAl::GetParameterNames(vector<string> &getpara) {
  const int numpara = 38;
  const string ParaNames[numpara] =
      {"m", "h", "j", "Xr", "Xs",
       "Xtos",
       "Ytos",
       "Rtos",
       "Xtof", "Ytof", "d", "f", "fCaB_jct",
       "fCaB_SL",
       "R",
       "O", "I",
       "Na_jct_buf", "Na_SL_buf", "Na_jct", "Na_SL", "Na_i",
       "Ca_SLB_SL",
       "Ca_SLB_jct",
       "Ca_SLHigh_SL",
       "Ca_SLHigh_jct",
       "Ca_Calsequestrin", "Ca_SR", "Ca_jct", "Ca_SL", "Ca_i",
       "Ca_TroponinC",
       "Ca_TroponinC_Ca_Mg",
       "Mg_TroponinC_Ca_Mg",
       "Ca_Calmodulin", "Ca_Myosin", "Mg_Myosin", "Ca_SRB"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void ShannonEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 16;
  const string ParaNames[numpara] =
      {"I_Na", "I_NaBk", "I_NaK", "I_Kr", "I_Ks", "I_tos", "I_tof", "I_K1", "I_NaCa", "I_Cl_Ca",
       "I_Clb", "I_CaL",
       "I_Cab",
       "I_Cap", "I_Kp", " I_mem"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
