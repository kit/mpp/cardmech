/*      File: EarmHilgemannNobleParameters.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#ifndef EARMHILGEMANNNOBLEPARAMETERS_H
#define EARMHILGEMANNNOBLEPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_EarmHilgemannNobleParameters {
  enum varType {
    VT_R = vtFirst,
    VT_Tx,
    VT_F,
    VT_RTdF,
    VT_RTd2F,
    VT_FdRT,
    VT_F2dRT,
    VT_C_m,
    VT_dC_m,
    VT_k_cachoff,
    VT_K_mk1,
    VT_k_mk,
    VT_k_mNa,
    VT_k_NaCa,
    VT_d_NaCa,
    VT_diffCa,
    VT_k_mCa,
    VT_I_NaKmax,
    VT_G_Na,
    VT_G_to,
    VT_G_bK,
    VT_G_k1,
    VT_G_bNa,
    VT_G_bCa,
    VT_P_Ca,
    VT_P_CaK,
    VT_P_CaNa,
    VT_Ca_b,
    VT_K_o,
    VT_Na_o,
    VT_NapK,
    VT_Na_od140,
    VT_NNNO,
    VT_g,
    VT_k_cyCa,
    VT_k_xcs,
    VT_k_srCa,
    VT_KI_K1,
    VT_KI_NaK,
    VT_V_ecs,
    VT_radius,
    VT_lenght,
    VT_Vol,
    VT_V_cell,
    VT_V_up,
    VT_V_rel,
    VT_V_i,
    VT_V_SRup,
    VT_k_mCa2,
    VT_M_trop,
    VT_C_trop,
    VT_k_1dV_iF,
    VT_k_1dV_i2F,
    VT_k_VupdVrel,
    VT_k_Vspez,
    VT_k_V_idV_SRup,
    VT_k_Vspez2,
    VT_k_kckxdk_s,
    VT_k_kckxp,
    VT_K_i,
    VT_dK_i,
    VT_Na_i,
    VT_dNa_i,
    VT_E_Namh,
    VT_E_Na,
    VT_I_NaK,
    VT_Ca_o,
    VT_E_K,
    VT_NNNICO,
    VT_r,
    VT_f_activator,
    VT_f_product,
    VT_Ca_i,
    VT_Ca_calmod,
    VT_Ca_troponin,
    VT_Ca_up,
    VT_Ca_rel,
    VT_m,
    VT_h,
    VT_d,
    VT_f,
    VT_q,
    VT_Vm,
    VT_Amp,
    vtLast
  };
} // namespace NS_EarmHilgemannNobleParameters

using namespace NS_EarmHilgemannNobleParameters;

class EarmHilgemannNobleParameters : public vbNewElphyParameters {
public:
  EarmHilgemannNobleParameters(const char *, ML_CalcType);

  ~EarmHilgemannNobleParameters();

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType);

  void Init(const char *, ML_CalcType);

  ML_CalcType exp50;
  ML_CalcType exp50_2;

  ML_CalcType m_inf[RTDT];
  ML_CalcType exptau_m[RTDT];
  ML_CalcType h_inf[RTDT];
  ML_CalcType exptau_h[RTDT];
  ML_CalcType d_inf[RTDT];
  ML_CalcType exptau_d[RTDT];
  ML_CalcType a_f[RTDT];
  ML_CalcType b_f[RTDT];
  ML_CalcType q_[RTDT];
  ML_CalcType r_inf[RTDT];
  ML_CalcType exptau_r[RTDT];
  ML_CalcType KexpV[RTDT];
  ML_CalcType NaexpV[RTDT];
  ML_CalcType dexpV[RTDT];
  ML_CalcType dexpV50_2[RTDT];
  ML_CalcType VdV[RTDT];
  ML_CalcType VdV2[RTDT];
  ML_CalcType KINaCa[RTDT];
  ML_CalcType sechs100exp[RTDT];
  ML_CalcType CI_si[RTDT];
}; // class EarmHilgemannNobleParameters
#endif // ifndef EARMHILGEMANNNOBLEPARAMETERS_H
