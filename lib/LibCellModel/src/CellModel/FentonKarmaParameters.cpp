/*      File: FentonKarmaParameters.cpp
    automatically created by ExtractParameterClass.pl - done by dw (22.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#include <FentonKarmaParameters.h>

FentonKarmaParameters::FentonKarmaParameters(const char *initFile) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile);
}

FentonKarmaParameters::~FentonKarmaParameters() {
  // Destruktor
}

void FentonKarmaParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "Loading the FentonKarma parameter from " << initFile << " ...\n";
#endif // if KADEBUG

  // Init values
  // constant values used in Calculate()

  // Initialization of the Parameters ...
  P[VT_g_fi_max].name     =    "g_fi_max";
  P[VT_tau_r].name        =       "tau_r";
  P[VT_tau_si].name       =      "tau_si";
  P[VT_tau_O].name        =       "tau_O";
  P[VT_tau_v_plus].name   =  "tau_v_plus";
  P[VT_tau_v1_minus].name =        "tau_v1_minus";
  P[VT_tau_v2_minus].name =        "tau_v2_minus";
  P[VT_tau_w_plus].name   =  "tau_w_plus";
  P[VT_tau_w_minus].name  = "tau_w_minus";
  P[VT_u_c].name          = "u_c";
  P[VT_u_v].name          = "u_v";
  P[VT_u_csi].name        =       "u_csi";
  P[VT_C_m].name          = "C_m";
  P[VT_V_O].name          = "V_O";
  P[VT_V_fi].name         =        "V_fi";
  P[VT_Vm_init].name      =     "Vm_init";
  P[VT_k].name            =   "k";
  P[VT_Init_u].name       =      "Init_u";
  P[VT_Init_v].name       =      "Init_v";
  P[VT_Init_w].name       =      "Init_w";
  P[VT_V_fi_V_O].name     =    "V_fi_V_O";
  P[VT_dCm_V_fi_V_O].name =        "Cm_dV_fi_V_O";
  P[VT_dtau_d].name       =      "dtau_d";
  P[VT_dtau_r].name       =      "dtau_r";
  P[VT_d2tau_si].name     =    "d2tau_si";
  P[VT_dtau_O].name       =      "dtau_O";
  P[VT_dtau_v_plus].name  = "dtau_v_plus";
  P[VT_dtau_w_minus].name =        "dtau_w_minus";
  P[VT_dtau_w_plus].name  = "dtau_w_plus";
  P[VT_Amp].name          = "Amp";

  P[VT_V_fi_V_O].readFromFile     = false;
  P[VT_dCm_V_fi_V_O].readFromFile = false;
  P[VT_dtau_d].readFromFile       = false;
  P[VT_dtau_r].readFromFile       = false;
  P[VT_d2tau_si].readFromFile     = false;
  P[VT_dtau_O].readFromFile       = false;
  P[VT_dtau_v_plus].readFromFile  = false;
  P[VT_dtau_w_minus].readFromFile = false;
  P[VT_dtau_w_plus].readFromFile  = false;

  ParameterLoader EPL(initFile, EMT_FentonKarma);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  // End Initialization of the Parameters ...

  Calculate();
  InitTable();
} // FentonKarmaParameters::Init

void FentonKarmaParameters::Calculate() {
#if KADEBUG
  cerr << "FentonKarmaParameters - Calculate ..." << endl;
#endif // if KADEBUG

  // constants in J_stim_calculation and Vm_calculation
  P[VT_V_fi_V_O].value     = P[VT_V_fi].value - P[VT_V_O].value;
  P[VT_dCm_V_fi_V_O].value = 1.0 / (P[VT_C_m].value * (P[VT_V_fi].value - P[VT_V_O].value));

  // 1/tau_d calculation
  P[VT_dtau_d].value       = 1.0 / (P[VT_C_m].value / P[VT_g_fi_max].value);
  P[VT_dtau_r].value       = 1.0/P[VT_tau_r].value;
  P[VT_d2tau_si].value     = 1.0/(2.0*P[VT_tau_si].value);
  P[VT_dtau_O].value       = 1.0/P[VT_tau_O].value;
  P[VT_dtau_v_plus].value  = 1.0/P[VT_tau_v_plus].value;
  P[VT_dtau_w_minus].value = 1.0/P[VT_tau_w_minus].value;
  P[VT_dtau_w_plus].value  = 1.0/P[VT_tau_w_plus].value;
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
}

void FentonKarmaParameters::InitTable() {}

void FentonKarmaParameters::PrintParameters() {
  // print the parameter to the stdout
  cout<<"FentonKarmaParameters:"<<endl;
  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= "     << P[i].value <<endl;
  }
}
