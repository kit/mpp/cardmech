/*! \file IyerMazhariWinslowCa.h
   \brief Markov model of calcium channel of human ventricular myocytes
   Iyer et al., Biophys J 2004

   \author fs, CVRTI - University of Utah, USA
 */

// Definition of TIMOTHY and TIMOTHY2 --> IyerMazhariWinslowCaParameters.h

#ifndef IYER_MAZHARI_WINSLOW_CA_H
#define IYER_MAZHARI_WINSLOW_CA_H

#include <IyerMazhariWinslowCaParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_IyerMazhariWinslowCaParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pIMW->P[NS_IyerMazhariWinslowCaParameters::a].value
#endif // ifdef HETERO


class IyerMazhariWinslowCa : public vbElphyModel<ML_CalcType> {
public:
  double C0L;
  double C1L;
  double C2L;
  double C3L;
  double C4L;
  double OL;
  double CCa0L;
  double CCa1L;
  double CCa2L;
  double CCa3L;
  double CCa4L;
  double y;
#ifdef TIMOTHY
  double ytimothy;
#endif // ifdef TIMOTHY
#ifdef TIMOTHY2
  double C0Ltimothy;
  double C1Ltimothy;
  double C2Ltimothy;
  double C3Ltimothy;
  double C4Ltimothy;
  double OLtimothy;
  double CCa0Ltimothy;
  double CCa1Ltimothy;
  double CCa2Ltimothy;
  double CCa3Ltimothy;
  double CCa4Ltimothy;
  double ytimothy;
#endif // ifdef TIMOTHY2

  IyerMazhariWinslowCaParameters *pIMW;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  IyerMazhariWinslowCa(IyerMazhariWinslowCaParameters *);

  ~IyerMazhariWinslowCa() {}

  virtual void Init();

  virtual inline ML_CalcType Volume() { return 0; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.; }

  virtual inline ML_CalcType GetVm() { return 0.; }

  virtual inline ML_CalcType GetCai() { return CELLMODEL_PARAMVALUE(VT_Cai); }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Cao); }

  virtual inline ML_CalcType GetNai() { return 0; }

  virtual inline ML_CalcType GetNao() { return 0; }

  virtual inline ML_CalcType GetKi() { return 0; }

  virtual inline ML_CalcType GetKo() { return 0; }

  virtual inline int GetSize(void) {
    return sizeof(IyerMazhariWinslowCa) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(IyerMazhariWinslowCaParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return (ML_CalcType *) &C0L; }

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char) (adVm < .15e-6 ? 3 : (adVm < .3e-6 ? 2 : 1));
  }

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);
}; // class IyerMazhariWinslowCa

#endif // ifndef IYER_MAZHARI_WINSLOW_CA_H
