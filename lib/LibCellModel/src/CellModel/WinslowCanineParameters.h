/**@file WinslowCanineParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef WINSLOW_CANINE_PARAMETERS
#define WINSLOW_CANINE_PARAMETERS

#include <ParameterLoader.h>

namespace NS_WinslowCanineParameters {
enum varType {
  VT_F = vtFirst,
  VT_T,
  VT_R,
  VT_RTdF,
  VT_RTd2F,
  VT_FdRT,
  VT_FdRTm1k5,

  VT_Acap,
  VT_Vmyo,
  VT_Vol,
  VT_VJSR,
  VT_VNSR,
  VT_VSS,
  VT_VmyodVNSR,
  VT_VJSRdVNSR,
  VT_VJSRdVSS,
  VT_VmyodVSS,
  VT_AcapdVmyo,
  VT_Acapd2Vmyo,
  VT_AcapDVSS,

  VT_K_o,
  VT_Na_o,
  VT_Na_oK,
  VT_Ca_o,
  VT_sigma,
  VT_NNNO,
  VT_GKr,
  VT_GKs,
  VT_GK1,
  VT_GKp,
  VT_GNa,
  VT_kNaCa,
  VT_KmNa,
  VT_KkNaCa,
  VT_KmCa,
  VT_KmK1,
  VT_ksat,
  VT_eta,
  VT_etam1,
  VT_INaKmax,
  VT_KmNai,
  VT_KmKo,
  VT_IpCamax,
  VT_KmpCa,
  VT_GCab,
  VT_GNab,
  VT_KI_K1,

  VT_v1,
  VT_Kfb,
  VT_dKfb,
  VT_Krb,
  VT_dKrb,
  VT_KSR,
  VT_Nfb,
  VT_Nrb,
  VT_vmaxf,
  VT_vmaxr,
  VT_tautr,
  VT_dtautr,
  VT_tauxfer,
  VT_dtauxfer,
  VT_kaplus,
  VT_kaminus,
  VT_kbplus,
  VT_kbminus,
  VT_kcplus,
  VT_kcminus,
  VT_ncoop,
  VT_mcoop,

  VT_fL,
  VT_gL,
  VT_bL,
  VT_dbL,
  VT_aL,
  VT_omega,
  VT_Pscale,
  VT_PCa,
  VT_PK,
  VT_ICahalf,
  VT_dICahalf,

  VT_alphaa0Kv43,
  VT_aaKv43,
  VT_betaa0Kv43,
  VT_baKv43,

  VT_alphai0Kv43,
  VT_aiKv43,
  VT_betai0Kv43,
  VT_biKv43,
  VT_alphaa0Kv14,
  VT_aaKv14,
  VT_betaa0Kv14,
  VT_baKv14,
  VT_alphai0Kv14,
  VT_betai0Kv14,

  VT_f1Kv43,
  VT_df1Kv43,
  VT_f2Kv43,
  VT_df2Kv43,
  VT_f3Kv43,
  VT_df3Kv43,
  VT_f4Kv43,
  VT_df4Kv43,
  VT_b1Kv43,
  VT_db1Kv43,
  VT_b2Kv43,
  VT_db2Kv43,
  VT_b3Kv43,
  VT_db3Kv43,
  VT_b4Kv43,
  VT_db4Kv43,
  VT_f1Kv14,
  VT_df1Kv14,
  VT_f2Kv14,
  VT_df2Kv14,
  VT_f3Kv14,
  VT_df3Kv14,
  VT_f4Kv14,
  VT_df4Kv14,
  VT_b1Kv14,
  VT_db1Kv14,
  VT_b2Kv14,
  VT_db2Kv14,
  VT_b3Kv14,
  VT_db3Kv14,
  VT_b4Kv14,
  VT_db4Kv14,

  VT_C0Kv14_to_CI0Kv14,
  VT_C1Kv14_to_CI1Kv14,
  VT_C2Kv14_to_CI2Kv14,
  VT_C3Kv14_to_CI3Kv14,
  VT_OKv14_to_OIKv14,
  VT_CI0Kv14_to_C0Kv14,
  VT_CI1Kv14_to_C1Kv14,
  VT_CI2Kv14_to_C2Kv14,
  VT_CI3Kv14_to_C3Kv14,
  VT_OIKv14_to_OKv14,

  VT_kb2Kv43,
  VT_kb3Kv43,
  VT_kb4Kv43,
  VT_kf1Kv43,
  VT_kf2Kv43,
  VT_kf3Kv43,
  VT_kb2Kv14,
  VT_kb3Kv14,
  VT_kb4Kv14,
  VT_kf1Kv14,
  VT_kf2Kv14,
  VT_kf3Kv14,

  VT_KvScale,
  VT_Kv43Frac,
  VT_GKv43,
  VT_PKv14,

  VT_LTRPNtot,
  VT_HTRPNtot,
  VT_khtrpn_plus,
  VT_khtrpn_minus,
  VT_kltrpn_plus,
  VT_kltrpn_minus,
  VT_CMDNtot,
  VT_CSQNtot,
  VT_EGTAtot,
  VT_KmCMDN,
  VT_KmCSQN,
  VT_KmEGTA,
  VT_K1bSS,
  VT_K2bSS,
  VT_KbJSR,

  // Init parameters
  VT_init_mNa,
  VT_init_hNa,
  VT_init_jNa,
  VT_init_Na_i,
  VT_init_K_i,
  VT_init_Ca_i,
  VT_init_Ca_NSR,
  VT_init_Ca_SS,
  VT_init_Ca_JSR,
  VT_init_C1_RyR,
  VT_init_O1_RyR,
  VT_init_O2_RyR,
  VT_init_C2_RyR,
  VT_init_xKr,
  VT_init_xKs,
  VT_init_C0,
  VT_init_C1,
  VT_init_C2,
  VT_init_C3,
  VT_init_C4,
  VT_init_Open,
  VT_init_CCa0,
  VT_init_CCa1,
  VT_init_CCa2,
  VT_init_CCa3,
  VT_init_CCa4,
  VT_init_yCa,
  VT_init_LTRPNCa,
  VT_init_HTRPNCa,
  VT_init_C0Kv43,
  VT_init_C1Kv43,
  VT_init_C2Kv43,
  VT_init_C3Kv43,
  VT_init_OKv43,
  VT_init_CI0Kv43,
  VT_init_CI1Kv43,
  VT_init_CI2Kv43,
  VT_init_CI3Kv43,
  VT_init_OIKv43,
  VT_init_C0Kv14,
  VT_init_C1Kv14,
  VT_init_C2Kv14,
  VT_init_C3Kv14,
  VT_init_OKv14,
  VT_init_CI0Kv14,
  VT_init_CI1Kv14,
  VT_init_CI2Kv14,
  VT_init_CI3Kv14,
  VT_init_OIKv14,
  VT_Amp,
  vtLast
};
} // namespace NS_WinslowCanineParameters

using namespace NS_WinslowCanineParameters;

class WinslowCanineParameters : public vbNewElphyParameters {
 public:
  double alpha_m[RTDT];
  double beta_m[RTDT];
  double exptau_h[RTDT];
  double h_inf[RTDT];
  double exptau_j[RTDT];
  double j_inf[RTDT];
  double xKr_inf[RTDT];
  double exptau_xKr[RTDT];
  double xKs_inf[RTDT];
  double exptau_xKs[RTDT];
  double RV[RTDT];
  double KpV[RTDT];
  double alphaarray[RTDT];
  double bettaarray[RTDT];
  double yCa_inf[RTDT];
  double exptau_yCa[RTDT];
  double alpha_act43array[RTDT];
  double beta_act43array[RTDT];
  double alpha_inact43array[RTDT];
  double beta_inact43array[RTDT];
  double alpha_act14array[RTDT];
  double beta_act14array[RTDT];

  double expVFdRT[RTDT];
  double expm1VFdRT[RTDT];
  double KI_Kv14_K[RTDT];
  double KI_Kv14_Na[RTDT];
  double KI_Camax[RTDT];
  double KI_Ca[RTDT];
  double KI_NaK[RTDT];
  double KI_NaCa[RTDT];

  WinslowCanineParameters(const char*, ML_CalcType);
  ~WinslowCanineParameters() {}

  void PrintParameters();

  // virtual inline int GetSize(void){return (&R-&R+1)*sizeof(T);};
  // virtual inline T* GetBase(void){return R;};
  // virtual int GetNumParameters() { return 1; };

  void Init(const char*, ML_CalcType);
  void InitTable(ML_CalcType);
  void Calculate();
}; // class WinslowCanineParameters

#endif // ifndef WINSLOW_CANINE_PARAMETERS
