/**@file IyerMazhariWinslowCaParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef IYER_MAZHARI_WINSLOW_CA_PARAMETERS
#define IYER_MAZHARI_WINSLOW_CA_PARAMETERS

#include <ParameterLoader.h>

// TIMOTHY defines ratio of mutated channels
// original model: #undef TIMOTHY
// for WT: #define TIMOTHY 0
#undef TIMOTHY

// #define TIMOTHY 1

// #undef TIMOTHY2
#define TIMOTHY2 0.5


namespace NS_IyerMazhariWinslowCaParameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_Cao,
    VT_Cai,

    VT_C0L,
    VT_C1L,
    VT_C2L,
    VT_C3L,
    VT_C4L,
    VT_OL,
    VT_CCa0L,
    VT_CCa1L,
    VT_CCa2L,
    VT_CCa3L,
    VT_CCa4L,
    VT_y,
#ifdef TIMOTHY
    VT_ytimothy,
    VT_y1,
    VT_y2,
    VT_y3,
    VT_y4,
    VT_y5,
    VT_y6,
    VT_y7,
#endif // ifdef TIMOTHY
#ifdef TIMOTHY2
    VT_CaLShift,

    VT_C0Ltimothy,
    VT_C1Ltimothy,
    VT_C2Ltimothy,
    VT_C3Ltimothy,
    VT_C4Ltimothy,
    VT_OLtimothy,
    VT_CCa0Ltimothy,
    VT_CCa1Ltimothy,
    VT_CCa2Ltimothy,
    VT_CCa3Ltimothy,
    VT_CCa4Ltimothy,

    VT_ytimothy,
    VT_y1,
    VT_y2,
    VT_y3,
    VT_tytimothyscale,
#endif // ifdef TIMOTHY2

    VT_CaLf,
    VT_CaLg,
    VT_CaLa,
    VT_CaLb,
    VT_CaLo,

    VT_PCa,

    VT_ShiftVm,
    VT_Amp,
    vtLast
  };
} // namespace NS_IyerMazhariWinslowCaParameters

using namespace NS_IyerMazhariWinslowCaParameters;

class IyerMazhariWinslowCaParameters : public vbNewElphyParameters {
public:
  IyerMazhariWinslowCaParameters(const char *);

  ~IyerMazhariWinslowCaParameters() {}

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&ShiftVm-&Tx+1)*sizeof(T);};
  // virtual inline T* GetBase(void){return Tx;};
  /*virtual int GetNumParameters()
     {
     return 23
   #ifdef TIMOTHY
   +8
   #endif
   #ifdef TIMOTHY2
   +18
   #endif
      ;
     };*/
  void Init(const char *);

  void InitTable() {}

  void Calculate();
};

#endif // ifndef IYER_MAZHARI_WINSLOW_CA_PARAMETERS
