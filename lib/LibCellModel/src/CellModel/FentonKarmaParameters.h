/*      File: FentonKarmaParameters.h
    automatically created by ExtractParameterClass.pl - done by dw (22.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#ifndef FENTONKARMAPARAMETERS_H
#define FENTONKARMAPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_FentonKarmaParameters {
  enum varType {
    VT_g_fi_max = vtFirst,
    VT_tau_r,
    VT_tau_si,
    VT_tau_O,
    VT_tau_v_plus,
    VT_tau_v1_minus,
    VT_tau_v2_minus,
    VT_tau_w_plus,
    VT_tau_w_minus,
    VT_u_c,
    VT_u_v,
    VT_u_csi,
    VT_C_m,
    VT_V_O,
    VT_V_fi,
    VT_Vm_init,
    VT_k,
    VT_Init_u,
    VT_Init_v,
    VT_Init_w,
    VT_V_fi_V_O,
    VT_dCm_V_fi_V_O,
    VT_dtau_d,
    VT_dtau_r,
    VT_d2tau_si,
    VT_dtau_O,
    VT_dtau_v_plus,
    VT_dtau_w_minus,
    VT_dtau_w_plus,
    VT_Amp,
    vtLast
  };
} // namespace NS_FentonKarmaParameters

using namespace NS_FentonKarmaParameters;

class FentonKarmaParameters : public vbNewElphyParameters {
public:
  FentonKarmaParameters(const char *);

  ~FentonKarmaParameters();

  void PrintParameters();

  void Calculate();

  void InitTable();

  void Init(const char *);
};

#endif // ifndef FENTONKARMAPARAMETERS_H
