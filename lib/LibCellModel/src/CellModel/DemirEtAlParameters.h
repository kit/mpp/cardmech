/**@file DemirEtAlParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef DEMIRETAL_PARAMETERS
#define DEMIRETAL_PARAMETERS

#include <ParameterLoader.h>

namespace NS_DemirEtAlParameters {
  enum varType {
    VT_R = vtFirst,
    VT_Tx,
    VT_F,
    VT_RTdF,
    VT_RTd2F,
    VT_FdRT,
    VT_V_cell,
    VT_Vol,
    VT_VF,
    VT_V_i,
    VT_dV_iF,
    VT_V_c,
    VT_dV_cF,
    VT_V_Ca,
    VT_dV_Ca2F,
    VT_V_up,
    VT_V_up2F,
    VT_dV_up2F,
    VT_V_rel,
    VT_dV_rel2F,
    VT_C_m,
    VT_dC_m,
    VT_K_b,
    VT_Na_b,
    VT_Ca_b,
    VT_t_p,
    VT_dt_p,
    VT_g_CaL,
    VT_g_BNa,
    VT_g_CaT,
    VT_g_BCa,
    VT_g_BK,
    VT_g_K,
    VT_g_f,
    VT_g_fNa,
    VT_g_fK,
    VT_g,
    VT_k_NaCa,
    VT_d_NaCa,
    VT_k_mNa,
    VT_k_mK,
    VT_I_NaK_,
    VT_I_CaP_,
    VT_k_cyca,
    VT_k_xcs,
    VT_kmkpk,
    VT_k_rel,
    VT_k_SRCa,
    VT_a_up,
    VT_a_rel,
    VT_b_up,
    VT_P_Na,
    VT_E_CaL,
    VT_E_CaT,
    VT_Mg_i,
    VT_K_1,
    VT_Init_h_1,
    VT_Init_h_2,
    VT_Init_d_L,
    VT_Init_f_L,
    VT_Init_d_T,
    VT_Init_f_T,
    VT_Init_p_a,
    VT_Init_p_i,
    VT_Init_y,
    VT_Init_m,
    VT_Init_Na_i,
    VT_Init_Na_c,
    VT_Init_K_i,
    VT_Init_K_c,
    VT_Init_Ca_i,
    VT_Init_Ca_c,
    VT_Init_P_C,
    VT_Init_P_TC,
    VT_Init_P_TMGC,
    VT_Init_P_TMGM,
    VT_Init_P_Calse,
    VT_Init_Ca_up,
    VT_Init_Ca_rel,
    VT_Init_F1,
    VT_Init_F2,
    VT_Init_F3,
    VT_Init_Vm,
    VT_Amp,
    vtLast
  };
} // namespace NS_DemirEtAlParameters

using namespace NS_DemirEtAlParameters;

class DemirEtAlParameters : public vbNewElphyParameters {
public:
  DemirEtAlParameters(const char *, ML_CalcType);

  ~DemirEtAlParameters() {}

  double m_[RTDT];
  double et_m[RTDT];
  double h_1_[RTDT];
  double et_h_1[RTDT];
  double h_2_[RTDT];
  double et_h_2[RTDT];
  double d_L_[RTDT];
  double et_d_L[RTDT];
  double f_L_[RTDT];
  double et_f_L[RTDT];
  double d_T_[RTDT];
  double et_d_T[RTDT];
  double f_T_[RTDT];
  double et_f_T[RTDT];
  double p_a_[RTDT];
  double et_p_a_1[RTDT];
  double a_p_i[RTDT];
  double b_p_i[RTDT];
  double et_y_1[RTDT];
  double y_[RTDT];
  double expVm40[RTDT];
  double C_Na[RTDT];
  double C_NaK[RTDT];
  double C_NaCa[RTDT];
  double dexpV[RTDT];

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&Vm-&R+1)*sizeof(T); };
  // virtual inline T* GetBase(void) {return R; };
  // virtual int GetNumParameters() { return 60; };

  void Init(const char *, ML_CalcType);

  void Calculate();

  void InitTable(ML_CalcType);
}; // class DemirEtAlParameters
#endif // ifndef DEMIRETAL_PARAMETERS
