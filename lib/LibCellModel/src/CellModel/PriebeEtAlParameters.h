/**@file PriebeEtAlParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef PRIEBEETAL_PARAMETERS
#define PRIEBEETAL_PARAMETERS

#include <ParameterLoader.h>

namespace NS_PriebeEtAlParameters {
  enum varType {
    VT_R = vtFirst,
    VT_Tx,
    VT_F,
    VT_RTdF,
    VT_FdRT,
    VT_RTd2F,
    VT_F2dRT,
    VT_w2p,
    VT_K_Ca,
    VT_K_mNai,
    VT_K_mKo,

    VT_k_sat,
    VT_eta,
    VT_k_NaCa,

    VT_K_mNa,
    VT_K_mCa,

    VT_K_mCaAct_Weber,
    VT_K_mCao_Weber,
    VT_K_mCai_Weber,
    VT_K_mNao_Weber,
    VT_K_mNai_Weber,

    VT_K_mNaih3,
    VT_K_mNaoh3,

    VT_k_mup,
    VT_k_leak,
    VT_I_upmax,
    VT_g_Na,
    VT_g_Ca,
    VT_g_Ks,
    VT_g_Kr,
    VT_g_K1,
    VT_g_Cab,
    VT_g_Nab,
    VT_g_to,
    VT_g_NaK,
    VT_g_NaCa,
    VT_C_Ca,
    VT_t_on,
    VT_dt_on,
    VT_t_tr,
    VT_dt_tr,
    VT_Na_i,
    VT_Na_o,
    VT_K_i,
    VT_K_o,
    VT_Ca_o,
    VT_Ca_im,
    VT_Naoh3,
    VT_Naih3,
    VT_KNN,
    VT_cNaK,
    VT_dCaith,
    VT_sigm,
    VT_E_Na,
    VT_E_to,
    VT_E_Ks,
    VT_E_K,
    VT_radius,
    VT_lenght,
    VT_Vol,
    VT_V_cell,
    VT_A_Geo,
    VT_R_CG,
    VT_A_Cap,
    VT_V_myo,
    VT_A_Capd2Fm,
    VT_V_NSR,
    VT_VNdm,
    VT_V_JSR,
    VT_VJdm,
    VT_VJdVN,
    VT_TRPN_max,
    VT_k_mTRPN,

    // Initialisierung
    VT_init_Ca_itot,
    VT_init_Ca_JSRtot,
    VT_init_Ca_NSR,
    VT_init_Ca_itotVmax,
    VT_init_m,
    VT_init_h,
    VT_init_j,
    VT_init_d,
    VT_init_f,
    VT_init_r,
    VT_init_t,
    VT_init_Xs,
    VT_init_Xr,
    VT_init_dCa_i2,
    VT_init_t_CICR,
    VT_init_t_Vmax,
    VT_init_CICR,

    VT_a_r1,
    VT_a_r2,
    VT_a_r3,
    VT_a_r4,
    VT_a_r5,

    VT_b_r1,
    VT_b_r2,
    VT_b_r3,
    VT_b_r4,
    VT_b_r5,
    VT_b_r6,

    VT_a_t1,
    VT_a_t2,
    VT_a_t3,
    VT_a_t4,
    VT_a_t5,
    VT_a_t6,

    VT_a_Xs1,
    VT_a_Xs2,
    VT_a_Xs3,
    VT_a_Xs4,
    VT_a_Xs5,

    VT_b_Xs1,
    VT_b_Xs2,
    VT_b_Xs3,
    VT_b_Xs4,
    VT_b_Xs5,

    VT_C_IKr1,
    VT_C_IKr2,

    VT_a_Xr1,
    VT_a_Xr2,
    VT_a_Xr3,
    VT_a_Xr4,
    VT_a_Xr5,

    VT_b_Xr1,
    VT_b_Xr2,
    VT_b_Xr3,
    VT_b_Xr4,
    VT_b_Xr5,

    VT_b_j1,
    VT_b_j2,
    VT_b_j3,
    VT_b_j4,
    VT_b_j5,

    VT_a_K1_1,
    VT_a_K1_2,
    VT_a_K1_3,
    VT_a_K1_4,

    VT_b_K1_1,
    VT_b_K1_2,
    VT_b_K1_3,
    VT_b_K1_4,
    VT_b_K1_5,
    VT_b_K1_6,
    VT_b_K1_7,
    VT_b_K1_8,

    VT_d_E_K,

    VT_b_t1,
    VT_b_t2,
    VT_b_t3,
    VT_b_t4,
    VT_b_t5,
    VT_b_t6,

    VT_init_Vm,
    VT_Amp,

    VT_SQT_fraction,

    VT_g_Kr_SQT,
    VT_C_IKr2_SQT,
    VT_a_Xr1_SQT,
    VT_a_Xr3_SQT,
    VT_b_Xr1_SQT,
    VT_b_Xr3_SQT,

    VT_C_IKr3_SQT_o,
    VT_C_IKr3_SQT_m,
    VT_C_IKr3_SQT_b,
    VT_C_IKr4_SQT_o,
    VT_C_IKr4_SQT_m,
    VT_C_IKr4_SQT_b,
    vtLast
  };
} // namespace NS_PriebeEtAlParameters

using namespace NS_PriebeEtAlParameters;

class PriebeEtAlParameters : public vbNewElphyParameters {
public:
  PriebeEtAlParameters(const char *, ElphyModelType, ML_CalcType);

  ~PriebeEtAlParameters() {}

  ElphyModelType emt;

  double exptau_m[RTDT];
  double m_[RTDT];
  double C_INaCa1[RTDT];
  double C_INaCa2[RTDT];
  double C_INa[RTDT];
  double C_IconstV[RTDT];
  double C_IKr_SQT[RTDT];
  double Xr_SQT_inf[RTDT];
  double exptau_Xr_SQT[RTDT];

  double h_inf[RTDT];
  double exptau_h[RTDT];
  double j_inf[RTDT];
  double exptau_j[RTDT];
  double d_inf[RTDT];
  double exptau_d[RTDT];
  double f_inf[RTDT];
  double exptau_f[RTDT];
  double r_inf[RTDT];
  double exptau_r[RTDT];
  double t_inf[RTDT];
  double exptau_t[RTDT];
  double Xs_inf[RTDT];
  double exptau_Xs[RTDT];
  double Xr_inf[RTDT];
  double exptau_Xr[RTDT];
  double C_Ito[RTDT];
  double C_IKr[RTDT];
  double C_IKs[RTDT];

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&C_IKr4_SQT_b-&R+1)*sizeof(T);};
  // virtual inline T* GetBase(void) {return R;};
  // virtual int GetNumParameters() { return 135; };

  void Init(const char *, ElphyModelType, ML_CalcType);

  void Calculate();

  void InitTable(ML_CalcType);
}; // class PriebeEtAlParameters
#endif // ifndef PRIEBEETAL_PARAMETERS
