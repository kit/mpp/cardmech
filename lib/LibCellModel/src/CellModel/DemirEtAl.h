/**@file DemirEtAl.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef DEMIRETAL_H
#define DEMIRETAL_H

#include <DemirEtAlParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_DemirEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pDeaP->P[NS_DemirEtAlParameters::a].value
#endif // ifdef HETERO

class DemirEtAl : public vbElphyModel<ML_CalcType> {
public:
  DemirEtAlParameters *pDeaP;
  ML_CalcType Ca_i, Ca_c;
  ML_CalcType Na_i, Na_c;
  ML_CalcType K_i, K_c;

  ML_CalcType h_1, h_2;
  ML_CalcType d_L, f_L;
  ML_CalcType d_T, f_T;
  ML_CalcType p_a, p_i;
  ML_CalcType y, m;

  ML_CalcType P_C, P_TC, P_TMGC, P_TMGM, P_Calse;

  ML_CalcType Ca_up, Ca_rel;
  ML_CalcType F1, F2, F3;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  DemirEtAl(DemirEtAlParameters *);

  ~DemirEtAl() {}

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vol); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_Init_Vm); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return Ca_c; }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return Na_c; }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return K_c; }

  virtual inline int GetSize(void) {
    return sizeof(DemirEtAl) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(DemirEtAlParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline bool AddHeteroValue(string, double);

  virtual inline ML_CalcType *GetBase(void) { return &Ca_i; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline void SetCao(ML_CalcType val) { Ca_c = val; }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetNao(ML_CalcType val) { Na_c = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual inline void SetKo(ML_CalcType val) { K_c = val; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char) (adVm < .5e-6 ? 5 : (adVm < 1e-6 ? 4 : (adVm < 2e-6 ? 3 : (adVm < 4e-6
                                                                                      ? 2 : 1))));
  }

  virtual void Init();


  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);
}; // class DemirEtAl

#endif // ifndef DEMIRETAL_H
