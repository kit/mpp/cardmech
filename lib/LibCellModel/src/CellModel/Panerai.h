/**@file Panerai.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef PANERAI_H
#define PANERAI_H

#include <ForceModelBasis.h>

namespace PaneraiNS {
  const double a1 = 200;
  const double b1 = 5;
  const double t_d = 0.3;
  const double c1 = 200e12;
  const double c20 = 20;
  const double AT0 = 2;
  const double k_i = 30.9;
  const double q = 1.45;
  const double h = 12e-9;
  const double g1 = 80;
  const double g2 = 240;
  const double f1 = 140;
  const double Ca0 = .45;  // mikroM
}

template<class T>
class Panerai : public vbForceModel<T> {
public:
  T A, N;

  Panerai() {}

  ~Panerai() {}

  virtual inline int GetSize(void) { return sizeof(Panerai<T>) - sizeof(vbForceModel<T>); }

  virtual inline T *GetBase(void) { return &A; }

  virtual void Init() {
    A = 0;
    N = 0;
  }

  virtual T Calc(double tinc, T stretch, T Ca, int euler = 1) {
    return ForceEulerNEuler(euler, tinc / euler, stretch, Ca);
  }

  virtual inline T ForceEulerNEuler(int r, T tinc, T stretch, T Ca) {
    using namespace PaneraiNS;
    T x = h * stretch;
    while (r--) {
      T c2 = c20 * exp(k_i * pow(2.0 * stretch - 2.0, q));
      A += tinc * (c1 * Ca * Ca * (AT0 - A) - c2 * A);
      T f = (x < 0. || x > h ? 0. : f1 * stretch);
      T g = (x < 0. ? g2 : g1 * stretch);
      N += tinc * (f * (A - N) - g * N);
    }
    return N;
  }

  virtual void Print(ostream &tempstr) {
    using namespace PaneraiNS;
    tempstr << A << ' ' << N << ' ';
  }
}; // class Panerai

#endif // ifndef PANERAI_H
