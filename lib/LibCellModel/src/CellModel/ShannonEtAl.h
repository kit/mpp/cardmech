/* File: ShannonEtAl.h
    Robin Moss - UHZ Freiburg/Bad Krozingen
    Based on https://models.physiomeproject.org/exposure/aeec124feedddcd9139685b2ff1131ae
    xml can also be found in the originals folder*/

#ifndef SHANNONETAL
#define SHANNONETAL

#include <ShannonEtAlParameters.h>

#define HETERO
#undef CELLMODEL_PARAMVALUE


#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_ShannonEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pSP->P[NS_ShannonEtAlParameters::a].value
#endif // ifdef HETERO

class ShannonEtAl : public vbElphyModel<ML_CalcType> {
public:
  ShannonEtAlParameters *pSP;
  ML_CalcType m;
  ML_CalcType h;
  ML_CalcType j;
  ML_CalcType Xr;
  ML_CalcType Xs;
  ML_CalcType Xtos;
  ML_CalcType Ytos;
  ML_CalcType Rtos;
  ML_CalcType Xtof;
  ML_CalcType Ytof;
  ML_CalcType d;
  ML_CalcType f;
  ML_CalcType fCaB_jct;
  ML_CalcType fCaB_SL;
  ML_CalcType R;
  ML_CalcType O;
  ML_CalcType I;
  ML_CalcType Na_jct_buf;
  ML_CalcType Na_SL_buf;
  ML_CalcType Na_jct;
  ML_CalcType Na_SL;
  ML_CalcType Na_i;
  ML_CalcType Ca_Calsequestrin;
  ML_CalcType Ca_SLB_SL;
  ML_CalcType Ca_SLB_jct;
  ML_CalcType Ca_SLHigh_SL;
  ML_CalcType Ca_SLHigh_jct;
  ML_CalcType Ca_SR;
  ML_CalcType Ca_jct;
  ML_CalcType Ca_SL;
  ML_CalcType Ca_i;
  ML_CalcType Ca_TroponinC;
  ML_CalcType Ca_TroponinC_Ca_Mg;
  ML_CalcType Mg_TroponinC_Ca_Mg;
  ML_CalcType Ca_Calmodulin;
  ML_CalcType Ca_Myosin;
  ML_CalcType Mg_Myosin;
  ML_CalcType Ca_SRB;

  ShannonEtAl(ShannonEtAlParameters *pp);

  ~ShannonEtAl();

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType SurfaceToVolumeRatio() { return 1.0; }

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vol_Cell); }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_V_init); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return CELLMODEL_PARAMVALUE(VT_K_i); }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline ML_CalcType *GetBase(void) { return &Ca_SL; }

  virtual inline ML_CalcType GetIto() { return 0.0; }

  virtual inline ML_CalcType GetIKr() { return 0.0; }

  virtual inline ML_CalcType GetIKs() { return 0.0; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType GetSpeedupMax(void) { return .0; }

  virtual ML_CalcType GetAmplitude(void) { return CELLMODEL_PARAMVALUE(VT_stim_amplitude); }

  virtual inline ML_CalcType GetStimTime() { return CELLMODEL_PARAMVALUE(VT_stim_duration); }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0,
                           ML_CalcType stretch = 1., int euler = 2);

  virtual void Print(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);

  virtual inline ML_CalcType GetTCa() { return Ca_TroponinC; }

  virtual inline bool OutIsTCa() { return true; }
}; // class ShannonEtAl
#endif // ifndef SHANNONETAL
