/*! \file LuEtAlParameters.h
   \brief Implementation of electrophysiological model for describing hERG channels
   from Lu et al., J Phys 2001

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef LUETALPARAMETERS_H
#define LUETALPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_LuEtAlParameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_Acap,
    VT_Vmyo,
    VT_Ko,
    VT_Ki,
    VT_C0Kr,
    VT_C1Kr,
    VT_C2Kr,
    VT_OKr,
    VT_IKr,
    VT_G_C0Kr,
    VT_G_C1Kr,
    VT_G_C2Kr,
    VT_G_OKr,
    VT_G_IKr,
    VT_Kra0a,
    VT_Kra0z,
    VT_Krb0a,
    VT_Krb0z,
    VT_Krfa,
    VT_Krfz,
    VT_Krba,
    VT_Krbz,
    VT_Kra1a,
    VT_Kra1z,
    VT_Krb1a,
    VT_Krb1z,
    VT_Kraia,
    VT_Kraiz,
    VT_Krbia,
    VT_Krbiz,
    VT_Kracia,
    VT_Kraciz,
    VT_Krbcia,
    VT_Krbciz,
    VT_Amp,
    vtLast
  };
} // namespace NS_LuEtAlParameters

using namespace NS_LuEtAlParameters;

class LuEtAlParameters : public vbNewElphyParameters {
public:
  LuEtAlParameters(const char *);

  ~LuEtAlParameters() {}

  void PrintParameters();

  inline void Calculate() {}

  inline void InitTable() {}

  void Init(const char *);
};

#endif // ifndef LUETALPARAMETERS_H
