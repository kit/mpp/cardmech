/*! \file CellModel.h
   \brief Classes and methods for programs Elphy-, Force- and CellModelTest

   \author gs, dw IBT - Universität Karlsruhe (TH), fs, CVRTI - University of Utah
 */

#include <kaBasicIO.h>

using namespace nskaGlobal;

// needed for kaWrite/kaRead

#include <CellModelLayer.h>
#include <vector>

#include <limits>

#undef HUGE
#define HUGE std::numeric_limits<ML_CalcType>::max()

typedef ML_CalcType CalcType;

enum StatusPrint {
  StatusPrintBrief,
  StatusPrintBriefI,
  StatusPrintShort,
  StatusPrintLong,
  StatusPrintLongI,
  StatusPrintNormal
};
enum StretchFunction {
  SF_VELOCITY, SF_RECT, SF_NONE
};
enum ForceCoupling {
  TroponinCoupling, CalciumCoupling, CalciumFeedBackCoupling
};
enum CaFunction {
  CF_Default, CF_Rice, CF_Panerai, CF_Const, CF_Inc
};
enum CellSolver {
  CS_Euler, CS_RK2, CS_RK4
};

//! Class handles single step in clamping protocol
class Clamp {
public:
  double t;
  int type;
  double val;
};

//! Class handles parameters for a single cell model
class CellParameters {
public:
  int cellnr;

#ifdef USE_EMT
  CalcType Vm;
  ElphyModelType emt;
  string initEVFile;
  vbElphyModel<CalcType> *pem;
  vbElphyParameters<CalcType> *ppem;

  CalcType Ca_o;
  CalcType i_internal;
  double text, textlen, textbegin, textend, textdiv, freq;
  double amplitude, thresVm;
  bool opt, monophasic;
  double currentEndTime;
  vector<Clamp> vClamp;
  int currentClampIndex;
  typedef CalcType (vbElphyModel<CalcType>::*csMethod)(double, CalcType, CalcType, CalcType, int);

  csMethod csFunction;

#endif // ifdef USE_EMT

#ifdef USE_FMT
  CalcType Force;
  ForceModelType fmt;
  string initFVFile;
  vbForceModel<CalcType> *pfm;
  vbForceParameters<CalcType> *ppfm;

# ifdef USE_EMT
  ForceCoupling fc;
  bool fb;
# else // ifdef USE_EMT
  CaFunction cf;
  double tCabegin;
  CalcType CaConst;
# endif // ifdef USE_EMT
#endif // ifdef USE_FMT
  double stretch;
  double stretchdefault, stretchbegin, stretchend, restretch, stretchAmplitude;
  double velocity, velocitydefault;
  StretchFunction sf;

  CalcType Cai;
  char *loadStatus;
  char *saveStatus;

  char *outfile;
  FILE *fpo;

  CellSolver cs;

  CellParameters() {
    cellnr = 0;
#ifdef USE_EMT
    Vm                = 0.0;
    emt               = EMT_BeelerReuter;
    initEVFile        = "";
    pem               = NULL;
    ppem              = NULL;
    Ca_o              = HUGE;
    i_internal        = 0.;
    text              = 1.0;
    textbegin         = 0.1;
    textlen           = HUGE;
    textend           = HUGE;
    textdiv           = 1;
    freq              = HUGE;
    amplitude         = HUGE;
    thresVm           = HUGE;
    opt               = false;
    monophasic        = true;
    currentClampIndex = 0;
    currentEndTime    = 0;
#endif // ifdef USE_EMT

#ifdef USE_FMT
    Force      = 0.;
    fmt        = FMT_Peterson;
    initFVFile = "";
    pfm        = NULL;
    ppfm       = NULL;
# ifdef USE_EMT
    fb = false;
# else // ifdef USE_EMT
    cf       = CF_Default;
    tCabegin = 0.;
    CaConst  = 0.;
# endif // ifdef USE_EMT
#endif // ifdef USE_FMT

    stretch = stretchdefault = 1.;
    stretchbegin = 0.;
    stretchend = 0.;
    velocity = velocitydefault = 0.;
    restretch = HUGE;
    stretchAmplitude = 1.;

    Cai = 0.;
    sf = SF_NONE;
    loadStatus = NULL;
    saveStatus = NULL;

    outfile = NULL;
    fpo = NULL;

    cs = CS_Euler;
  }

  virtual ~CellParameters() {
    if (fpo)
      fclose(fpo);

#ifdef USE_EMT
    delete ppem;
    delete pem;
#endif // ifdef USE_EMT
#ifdef USE_FMT
    delete pfm;
    delete ppfm;
#endif // ifdef USE_FMT
  }

#ifdef USE_EMT

  // Read clamp configuration from file clampFile into vector

  void readClampFile(char *clampFile) {
    FILE *fp = fopen(clampFile, "r");

    if (!fp)
      throw kaBaseException("Can't open clampfile %s", clampFile);
    for (int numClamps = 0; !feof(fp); numClamps++) {
      Clamp tmp;
      char  buf[256];
      int   rc = fscanf(fp, "%s %lf %lf ", buf, &tmp.t, &tmp.val);
      if (rc < 3)
        break;
      if (!strcasecmp(buf, "Vm")) {
        tmp.type = 1;
      } else if (!strcasecmp(buf, "Cai")) {
        tmp.type = 2;
      } else if (!strcasecmp(buf, "Cai+"))
      { tmp.type = 3; currentClampIndex = 1; } else {
        throw kaBaseException("Unknown clamp type %s", buf);
      }

      // cerr << "# "<<numClamps<<" "<<tmp.type<<" "<<tmp.t<<" "<<tmp.val<<endl;
      vClamp.push_back(tmp);
    }
    if (vClamp.size() > 0) {
      currentEndTime = vClamp[0].t;

      // cerr<<"t=0, setting to "<<vClamp[currentClampIndex].val<<" until "<<currentEndTime<<endl;
    }
    fclose(fp);
  } // readClampFile

#endif // ifdef USE_EMT

  void init(bool verbose, ML_CalcType tinc) {
#ifdef USE_EMT
    if (initEVFile.size())
      emt = FindElphyModelFileType(initEVFile.c_str());
    else
      initEVFile = FindElphyModelFileName(emt);

    if (verbose)
      cerr << "ElphyModelType " << emt << " used in " << initEVFile << endl;

    initElphyParameters<CalcType>(&ppem, initEVFile.c_str(), emt, tinc);
    initElphyModel<CalcType>(&pem, ppem, emt, opt);

    if (amplitude == HUGE)
      amplitude = pem->GetAmplitude();
    if (textlen == HUGE) {
      textlen = pem->GetStimTime();
      if (verbose)
        cout << "Setting StimTime to " << textlen << endl;
    }
    Vm = pem->GetVm();

    /*!< Function to set the new tinc value given by parameter at command line */
    pem->SetTinc(tinc);

    if (Ca_o != HUGE)
      pem->SetCao(Ca_o);
#endif  // USE_EMT

#ifdef USE_FMT
    if (initFVFile.size())
      fmt = FindForceModelFileType(initFVFile.c_str());

    if (!initFVFile.size())
      initFVFile = FindForceModelFileName(fmt);
    if (verbose)
      cerr << "ForceModelType " << fmt << " used in " << initFVFile << endl;

    initForceParameters(&ppfm, initFVFile.c_str(), fmt);
    initForceModel(&pfm, ppfm, fmt);

# ifdef USE_EMT  // and USE_FMT
    // Inquiry if CellModel gives TCa and if ForceModel can use it
    if (pem->OutIsTCa() && pfm->InIsTCa())
      fc = TroponinCoupling;
    else
      fc = (fb ? CalciumFeedBackCoupling : CalciumCoupling);
# endif  // USE_EMT
#endif  // USE_FMT

    stretch = stretchdefault;

    if (outfile && strcmp(outfile, "NULL")) {
      fpo = fopen(outfile, "w");
      if (!fpo)
        throw kaBaseException("Opening file %s for output", outfile);
    }

#ifdef USE_EMT
    switch (cs) {
      case CS_Euler:
        csFunction = &vbElphyModel<CalcType>::Calc;
        break;
      case CS_RK2:
        csFunction = &vbElphyModel<CalcType>::CalcRungeKutta2;
        break;
      case CS_RK4:
        csFunction = &vbElphyModel<CalcType>::CalcRungeKutta4;
        break;
    }
#endif // ifdef USE_EMT
  } // init

  void calculate(const double t, const double tinc) {
    switch (sf) {
      case SF_VELOCITY: {
        if ((t >= stretchbegin) && (t <= stretchend))
          velocity = velocitydefault;
        else if ((t - stretchend >= restretch) && (t - 2. * stretchend + stretchbegin <= restretch))
          velocity = velocitydefault;
        else
          velocity = 0;
        stretch += tinc * velocity;
      }
        break;

      case SF_RECT: {
        if ((t >= stretchbegin) && (t <= stretchend))
          stretch = stretchAmplitude;
        else
          stretch = stretchdefault;
      }
        break;

      default:
        break;
    }

#ifdef USE_EMT
    if (currentClampIndex < vClamp.size()) {
      if (t > currentEndTime) {
        currentClampIndex++;
        if (vClamp[currentClampIndex].type == 3)
          currentClampIndex++;
        currentEndTime += vClamp[currentClampIndex].t;

        // if (currentClampIndex<vClamp.size())
        //    cerr<<"t="<<t<<", setting to "<<vClamp[currentClampIndex].val<<" until "<<currentEndTime<<endl;
      }
    }
    if (currentClampIndex < vClamp.size()) {
      if (vClamp[currentClampIndex].type == 1) {
        Vm = vClamp[currentClampIndex].val;
      } else if (vClamp[currentClampIndex].type == 2) {
        pem->SetCai(vClamp[currentClampIndex].val);
      } else if (vClamp[currentClampIndex].type == 3) {
        Vm = vClamp[currentClampIndex-1].val;
        pem->SetCai(vClamp[currentClampIndex].val);
      }
    }
    CalcType i_external;
    if ((t >= textbegin) && (t < textend)) {
      i_external = (Vm >= thresVm ? 0. : amplitude);
      if (freq != HUGE) {
        i_external = sin(((t-textbegin)*2*M_PI*freq)+.000001)*i_external;
        if (monophasic && (i_external < 0.0) )
          i_external = 0.0;
      }
      if (t >= textbegin+textlen) {
        text      /= textdiv;
        textbegin += text;
      }
    } else {
      i_external = 0.0;
    }
    Vm += (pem->*csFunction)(tinc, Vm, i_external+i_internal, stretch, 1);
    if (std::isnan(Vm))
      throw kaBaseException("Vm is NaN");

# ifdef USE_FMT
    switch (fc) {
      case TroponinCoupling:
        Force = pfm->CalcTrop(tinc, stretch, velocity, pem->GetTCa()*1000.0);
        break;

      case CalciumCoupling: {
        CalcType Calcium = pem->GetCai()*1000.0;
        Force = pfm->Calc(tinc, stretch, velocity, Calcium);
      }
      break;

      case CalciumFeedBackCoupling: {
        CalcType Calcium = pem->GetCai()*1000.0;
        Force = pfm->Calc(tinc, stretch, velocity, Calcium);
        pem->SetCai(Calcium*.001);
      }
      break;
    }
# endif // ifdef USE_FMT

#else // ifdef USE_EMT
    switch (cf) {
      case CF_Const:
        Cai = CaConst; // mikroM
        break;
      case CF_Default:
        Cai = 0.001;   // mikroM
        break;
      case CF_Rice:
        Cai = 0.1;     // mikroM
        break;
      case CF_Inc:
        Cai = 0.0;     // mikroM
        break;
      default:
        Cai = 0.0;
        break;
    }

    if (t >= tCabegin) {
      const double tb = t - tCabegin;
      switch (cf) {
        case CF_Default: {
          const double tau_Ca = 0.06;  // s
          const double Ca_max = 3; // mikroM
          Cai += (Ca_max - Cai) * tb / tau_Ca * exp(1 - tb / tau_Ca);
          break;
        }

        case CF_Inc:
          Cai += 100 * tb; // Cai will be linear from Cai=0 at t=tbegin and Cai=100 at t+=1s
          break;

        case CF_Rice: {
          const double tau1 = 0.025; // s
          const double tau2 = .1;
          const double Ca_max = 1; // mikroM Rice99 H1744
          Cai += 1.67 * Ca_max * (1 - exp(-tb / tau1)) * exp(1 - tb / tau2);
          break;
        }

        case CF_Panerai: {
          const double Ca_max = 0.45;  // mikroM
          Cai = Ca_max * (1 - exp(-200 * tb * tb)) *
                exp((tb < 0.3 ? 0 : -5) * (tb - 0.3) * (tb - 0.3));
          break;
        }

        case CF_Const:
          break;
      } // switch
    }

    Force = pfm->Calc(tinc, stretch, velocity, Cai);
#endif // ifdef USE_EMT
  } // calculate

  void outputStatusLine(const StatusPrint spm) {
    vector<string> paraNames;
    paraNames.push_back("t");
#ifdef USE_EMT
    paraNames.push_back("V_m");
#endif // ifdef USE_EMT
    switch (spm) {
      case StatusPrintBrief:
#ifdef USE_FMT
        paraNames.push_back("Tension");
#endif // ifdef USE_FMT
        break;

      case StatusPrintBriefI:
#ifdef USE_EMT
        paraNames.push_back("I_internal");
#endif // ifdef USE_EMT
        break;

      case StatusPrintShort:
        paraNames.push_back("Ca_i");
#ifdef USE_FMT
        paraNames.push_back("Tension");
#endif // ifdef USE_FMT
        break;

      case StatusPrintNormal:
#ifdef USE_EMT
        pem->GetParameterNames(paraNames);
# ifdef USE_FMT
        paraNames.push_back("Tension");
        pfm->GetParameterNames(paraNames);
# endif // ifdef USE_FMT
#else // ifdef USE_EMT
        paraNames.push_back("Ca_i");
        paraNames.push_back("Tension");
        pfm->GetParameterNames(paraNames);
#endif // ifdef USE_EMT
        break;

      case StatusPrintLong:
#ifdef USE_EMT
        pem->GetLongParameterNames(paraNames);
# ifdef USE_FMT
        paraNames.push_back("Tension");
        pfm->GetParameterNames(paraNames);
# endif // ifdef USE_FMT
#else // ifdef USE_EMT
        paraNames.push_back("Ca_i");
        paraNames.push_back("Tension");
        pfm->GetParameterNames(paraNames);
#endif // ifdef USE_EMT
        break;

      case StatusPrintLongI:
#ifdef USE_EMT
        pem->GetLongParameterNames(paraNames);
        paraNames.push_back("I_internal");
#endif // ifdef USE_EMT
        break;

      default:
        throw kaBaseException("Undefined print option");
        break;
    } // switch

    ostringstream tostr(ostringstream::out);
    tostr << "#  ";
    int length = paraNames.size();
    for (int i = 0; i < length; i++)
      tostr << i + 1 << ":" << paraNames[i] << "  ";
    tostr << endl;
    tostr << '\0';
    output(tostr.str().c_str());
  } // outputStatusLine

  inline void output(const char *buf) {
    if (fpo)
      fprintf(fpo, "%s", buf);
    else if (!outfile)
      cout << buf;
  }

  void output(const StatusPrint spm, const double t) {
    ostringstream tostr(ostringstream::out);

    tostr.setf(ios_base::scientific);

    switch (spm) {
      case StatusPrintBrief:
        tostr << t << ' ';
#ifdef USE_EMT
        tostr << Vm << ' ';
#endif // ifdef USE_EMT
#ifdef USE_FMT
        tostr << Force;
#endif // ifdef USE_FMT
        break;

      case StatusPrintBriefI:
        tostr << t;
#ifdef USE_EMT
        tostr<<' '<<Vm;
        tostr<<' '<<i_internal;
#endif // ifdef USE_EMT
        break;

      case StatusPrintShort:
        tostr << t << ' ';
#ifdef USE_EMT
        tostr<<Vm<<' ';
        Cai = pem->GetCai()*1000.0;
#endif // ifdef USE_EMT
        tostr << Cai << ' ';
#ifdef USE_FMT
        tostr << Force << ' ';
#endif // ifdef USE_FMT
        break;

      case StatusPrintNormal:
#ifdef USE_EMT
        pem->Print(tostr, t, Vm);
# ifdef USE_FMT
        tostr << ' ' << Force << ' ';
        pfm->Print(tostr);
# endif // ifdef USE_FMT
#else // ifdef USE_EMT
        pfm->Print(tostr, t, Cai, Force);
#endif // ifdef USE_EMT
        break;

      case StatusPrintLong:
#ifdef USE_EMT
        pem->LongPrint(tostr, t, Vm);
# ifdef USE_FMT
        tostr << ' ' << Force << ' ';
        pfm->Print(tostr);
# endif // ifdef USE_FMT
#else // ifdef USE_EMT
        pfm->Print(tostr, t, Cai, Force);
#endif // ifdef USE_EMT
        break;

      case StatusPrintLongI:
#ifdef USE_EMT
        pem->LongPrint(tostr, t, Vm);
        tostr<<' '<<i_internal;
#endif // ifdef USE_EMT
        break;

      default:
        throw kaBaseException("Undefined print option");
        break;
    } // switch
    tostr << endl;
    tostr << '\0';

    output(tostr.str().c_str());
  } // output

  void loadStatusFile() {
    FILE *fp = fopen(loadStatus, "rb");

    if (!fp)
      throw kaBaseException("Reading file %s", loadStatus);
#ifdef USE_EMT
    kaRead(&Vm, sizeof(CalcType), 1, fp);
    pem->ReadStatus(fp);
#endif // ifdef USE_EMT
#ifdef USE_FMT
    pfm->ReadStatus(fp);
#endif // ifdef USE_FMT
    fclose(fp);
  }

  void
  alloc_ida() {  // Beim Saucerman Tusscher Modell muss der IDA Solver Speicher fuer die Variablen und Gleichung
    // allokieren.
    // die Initialwerte der Variablen (entweder aus dem Ev-file oder ber die Load-Funktion sind dann im Speicherbereich
#ifdef USE_EMT        // des Solvers und koennen nicht mehr ohne weiteres geaendert werden. Deshalb wird die
    // Speicheralokierung fr den Solver erst
pem->alloc_ida(); // nach dem "Load" aus ElphymodelTest ausgefuehrt. Falls also Initialwerte aus einem gespeicherten
    // Zwischenzustand
#endif                // verwendet werden sollen und nicht die Standardwerte aus dem Ev-file so ist dies dadurch auch
    // moeglich.
  }                   // die Funktion alloc_ida() allokiert also den Speicher fr den IDA Solver

  void saveStatusFile() {
    FILE *fp = fopen(saveStatus, "wb");

    if (!fp)
      throw kaBaseException("Writing file %s", saveStatus);
#ifdef USE_EMT
    kaWrite(&Vm, sizeof(CalcType), 1, fp);
    pem->WriteStatus(fp);
#endif // ifdef USE_EMT
#ifdef USE_FMT
    pfm->WriteStatus(fp);
#endif // ifdef USE_FMT
    fclose(fp);
  }
}; // class CellParameters

#ifdef USE_EMT

//! Class handles coupling resistance between two cells
class CellCoupling {
 public:
  int cellnr1, cellnr2;
  double R, S;
  CellParameters *cell1, *cell2;
};
#endif // ifdef USE_EMT
