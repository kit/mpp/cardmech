/**@file DemirEtAlParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <DemirEtAlParameters.h>

DemirEtAlParameters::DemirEtAlParameters(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "DemirEtAlParameters::DemirEtAlParameters "<< initFile << endl;
#endif // if KADEBUG
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

void DemirEtAlParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "DemirEtAlParameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void DemirEtAlParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "DemirEtAlParameters:init " << initFile << endl;
#endif // if KADEBUG

  // Initialization of parameter names
  P[VT_R].name = "R";
  P[VT_Tx].name = "T";
  P[VT_F].name = "F";
  P[VT_RTdF].name = "RTdF";
  P[VT_RTd2F].name = "RTd2F";
  P[VT_FdRT].name = "FdRT";
  P[VT_V_cell].name = "V_cell";
  P[VT_Vol].name = "Vol";
  P[VT_VF].name = "VF";
  P[VT_V_i].name = "V_i";
  P[VT_dV_iF].name = "dV_iF";
  P[VT_V_c].name = "V_c";
  P[VT_dV_cF].name = "dV_cF";
  P[VT_V_Ca].name = "V_Ca";
  P[VT_dV_Ca2F].name = "dV_Ca2F";
  P[VT_V_up].name = "V_up";
  P[VT_V_up2F].name = "V_up2F";
  P[VT_dV_up2F].name = "dV_up2F";
  P[VT_V_rel].name = "V_rel";
  P[VT_dV_rel2F].name = "dV_rel2F";
  P[VT_C_m].name = "C_m";
  P[VT_dC_m].name = "dC_m";
  P[VT_K_b].name = "K_b";
  P[VT_Na_b].name = "Na_b";
  P[VT_Ca_b].name = "Ca_b";
  P[VT_t_p].name = "t_p";
  P[VT_dt_p].name = "dt_p";
  P[VT_g_CaL].name = "g_CaL";
  P[VT_g_BNa].name = "g_BNa";
  P[VT_g_CaT].name = "g_CaT";
  P[VT_g_BCa].name = "g_BCa";
  P[VT_g_BK].name = "g_BK";
  P[VT_g_K].name = "g_K";
  P[VT_g_f].name = "g_f";
  P[VT_g_fNa].name = "g_fNa";
  P[VT_g_fK].name = "g_fK";
  P[VT_g].name = "g";
  P[VT_k_NaCa].name = "k_NaCa";
  P[VT_d_NaCa].name = "d_NaCa";
  P[VT_k_mNa].name = "k_mNa";
  P[VT_k_mK].name = "k_mK";
  P[VT_I_NaK_].name = "I_NaK_";
  P[VT_I_CaP_].name = "I_CaP_";
  P[VT_k_cyca].name = "k_cyca";
  P[VT_k_xcs].name = "k_xcs";
  P[VT_kmkpk].name = "kmkpk";
  P[VT_k_rel].name = "k_rel";
  P[VT_k_SRCa].name = "k_SRCa";
  P[VT_a_up].name = "a_up";
  P[VT_a_rel].name = "a_rel";
  P[VT_b_up].name = "b_up";
  P[VT_P_Na].name = "P_Na";
  P[VT_E_CaL].name = "E_CaL";
  P[VT_E_CaT].name = "E_CaT";
  P[VT_Mg_i].name = "Mg_i";
  P[VT_K_1].name = "K_1";
  P[VT_Init_h_1].name = "Init_h_1";
  P[VT_Init_h_2].name = "Init_h_2";
  P[VT_Init_d_L].name = "Init_d_L";
  P[VT_Init_f_L].name = "Init_f_L";
  P[VT_Init_d_T].name = "Init_d_T";
  P[VT_Init_f_T].name = "Init_f_T";
  P[VT_Init_p_a].name = "Init_p_a";
  P[VT_Init_p_i].name = "Init_p_i";
  P[VT_Init_y].name = "Init_y";
  P[VT_Init_m].name = "Init_m";
  P[VT_Init_Na_i].name = "Init_Na_i";
  P[VT_Init_Na_c].name = "Init_Na_c";
  P[VT_Init_K_i].name = "Init_K_i";
  P[VT_Init_K_c].name = "Init_K_c";
  P[VT_Init_Ca_i].name = "Init_Ca_i";
  P[VT_Init_Ca_c].name = "Init_Ca_c";
  P[VT_Init_P_C].name = "Init_P_C";
  P[VT_Init_P_TC].name = "Init_P_TC";
  P[VT_Init_P_TMGC].name = "Init_P_TMGC";
  P[VT_Init_P_TMGM].name = "Init_P_TMGM";
  P[VT_Init_P_Calse].name = "Init_P_Calse";
  P[VT_Init_Ca_up].name = "Init_Ca_up";
  P[VT_Init_Ca_rel].name = "Init_Ca_rel";
  P[VT_Init_F1].name = "Init_F1";
  P[VT_Init_F2].name = "Init_F2";
  P[VT_Init_F3].name = "Init_F3";
  P[VT_Init_Vm].name = "Init_Vm";
  P[VT_Amp].name = "Amp";

  P[VT_RTdF].readFromFile = false;
  P[VT_RTd2F].readFromFile = false;
  P[VT_FdRT].readFromFile = false;
  P[VT_VF].readFromFile = false;
  P[VT_V_i].readFromFile = false;
  P[VT_dV_iF].readFromFile = false;
  P[VT_V_c].readFromFile = false;
  P[VT_dV_cF].readFromFile = false;
  P[VT_V_Ca].readFromFile = false;
  P[VT_dV_Ca2F].readFromFile = false;
  P[VT_V_up].readFromFile = false;
  P[VT_V_up2F].readFromFile = false;
  P[VT_dV_up2F].readFromFile = false;
  P[VT_V_rel].readFromFile = false;
  P[VT_dV_rel2F].readFromFile = false;
  P[VT_dC_m].readFromFile = false;
  P[VT_dt_p].readFromFile = false;
  P[VT_g_K].readFromFile = false;
  P[VT_g_fNa].readFromFile = false;
  P[VT_g_fK].readFromFile = false;
  P[VT_kmkpk].readFromFile = false;
  P[VT_b_up].readFromFile = false;
  P[VT_K_1].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_DemirEtAl);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
  InitTable(tinc);
} // DemirEtAlParameters::Init

void DemirEtAlParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "DemirEtAlParameters - Calculate ..."<<endl;
#endif // if KADEBUG
  P[VT_RTdF].value = P[VT_R].value * (P[VT_Tx].value) / (P[VT_F].value);
  P[VT_RTd2F].value = P[VT_RTdF].value * .5;
  P[VT_FdRT].value = 1.0 / (P[VT_RTdF].value);
  P[VT_VF].value = 1e9 / (P[VT_V_cell].value); // prom^3
  P[VT_V_i].value = .465 * (P[VT_V_cell].value);
  P[VT_dV_iF].value = 1.0 / (P[VT_F].value * (P[VT_V_i].value));
  P[VT_V_c].value = .136 * (P[VT_V_cell].value);
  P[VT_dV_cF].value = 1.0 / (P[VT_F].value * (P[VT_V_c].value));
  P[VT_V_Ca].value = P[VT_V_i].value;
  P[VT_dV_Ca2F].value = 1.0 / (2.0 * (P[VT_F].value) * (P[VT_V_Ca].value));
  P[VT_V_up].value = .01166 * (P[VT_V_Ca].value);
  P[VT_V_up2F].value = P[VT_V_up].value * 2.0 * (P[VT_F].value) * 15.581178;
  P[VT_dV_up2F].value = 1.0 / (P[VT_V_up].value * 2.0 * (P[VT_F].value));
  P[VT_V_rel].value = .001296 * (P[VT_V_Ca].value);
  P[VT_dV_rel2F].value = 1.0 / (2.0 * (P[VT_V_rel].value) * (P[VT_F].value));
  P[VT_dC_m].value = .001 / (P[VT_C_m].value);
  P[VT_dt_p].value = 1.0 / (P[VT_t_p].value);
  P[VT_g_K].value = 6.93e-3 * pow((double) P[VT_K_b].value, 0.59);
  P[VT_g_fNa].value = .34375 * (P[VT_g_f].value);
  P[VT_g_fK].value = .65625 * (P[VT_g_f].value);
  P[VT_kmkpk].value = P[VT_k_cyca].value * (P[VT_k_xcs].value) + (P[VT_k_cyca].value);
  P[VT_b_up].value = P[VT_k_xcs].value * (P[VT_a_up].value);
  P[VT_K_1].value = P[VT_k_cyca].value * (P[VT_k_xcs].value) / (P[VT_k_SRCa].value);
} // DemirEtAlParameters::Calculate

void DemirEtAlParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr << "DemirEtAlParameters - InitTable ...\n";
#endif // if KADEBUG
  tinc *= -1.0;  // Neg. for exptau calculation
  for (double V = -RangeTabhalf + .0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int) (DivisionTab * (RangeTab * .5 + V) + .5);

    double expV51 = exp(-(V + 51.9) * .11235955);
    double a_m = -824.0 * (V + 51.9) / (expV51 - 1.0);
    double b_m = 32960.0 * expV51;
    m_[Vi] = a_m / (a_m + b_m);

    et_m[Vi] = exp(tinc / (1.0 / (a_m + b_m) + 0.000015));

    double expV101 = exp(-(V + 101.3) * .079365079);
    double a_h_1 = 165.0 * expV101;
    double b_h_1 = 12360.0 / (320.0 * expV101 + 1.0);
    et_h_1[Vi] = exp(tinc * (a_h_1 + b_h_1));
    h_1_[Vi] = a_h_1 / (a_h_1 + b_h_1);
    et_h_2[Vi] = exp(tinc * (.05 * et_h_1[Vi]));
    h_2_[Vi] = h_1_[Vi];

    double a_d_L =
        -28.39 * (V + 35.0) / (exp(-(V + 35.0) * .4) - 1.0) - 84.9 * V / (exp(-0.208 * V) - 1.0);
    double b_d_L = 11.43 * (V - 5.0) / (exp((V - 5.0) * .4) - 1.0);
    et_d_L[Vi] = exp(tinc * (a_d_L + b_d_L));
    d_L_[Vi] = 1.0 / (1.0 + exp(-(V + 14.1) * .16666667));
    double a_f_L = 3.75 * (V + 28.0) / (exp((V + 28.0) * .25) - 1.0);
    double b_f_L = 30.0 / (1.0 + exp(-(V + 28.0) * .25));
    et_f_L[Vi] = exp(tinc * (a_f_L + b_f_L));
    f_L_[Vi] = 1.0 / (1.0 + exp((V + 30.0) * .2));

    double a_d = exp((V + 26.3) * .033333333);
    double a_d_T = 1068.0 * a_d;
    double b_d_T = 1068.0 / a_d;
    et_d_T[Vi] = exp(tinc * (a_d_T + b_d_T));
    d_T_[Vi] = 1.0 / (1.0 + exp(-(V + 26.3) * .16666667));
    double a_f_T = 15.3 * exp(-(V + 61.7) * .012004802);
    double b_f_T = 15.0 * exp((V + 61.7) * .065019506);
    et_f_T[Vi] = exp(tinc * (a_f_T + b_f_T));
    f_T_[Vi] = 1.0 / (1.0 + exp((V + 61.7) * 0.17857143));

    p_a_[Vi] = 1.0 / (1.0 + exp(-(V + 5.1) * .13513514));
    et_p_a_1[Vi] = exp(tinc * (17.0 * exp(.0398 * V) + 2.11 * exp(-.0510 * V)));
    a_p_i[Vi] = 100.0 * exp(-.0183 * V);
    b_p_i[Vi] = 656.0 * exp(.00942 * V);

    et_y_1[Vi] = exp(tinc * (1.6483 * exp(-(V + 54.06) * .041101521) +
                             14.010155 / (.7 + exp(-(V + 60.0) * .18181818))));
    y_[Vi] = 1.0 / (1.0 + exp((V + 72.2) * .11111111));

    expVm40[Vi] = exp((V - 40.0) * .08);
    double VFdRT = V * (P[VT_FdRT].value);
    C_Na[Vi] = (P[VT_P_Na].value) * (P[VT_F].value) * VFdRT / (exp(VFdRT) - 1.0);
    C_NaK[Vi] = (P[VT_I_NaK_].value) * (1.6 / (1.5 + exp((V + 60.0) * -.025)));
    C_NaCa[Vi] = (P[VT_k_NaCa].value) * exp(.03743 * V * (P[VT_g].value));
    dexpV[Vi] = exp(-.03743 * V);
  }
} // DemirEtAlParameters::InitTable
