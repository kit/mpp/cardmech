/**@file ClancyWTNa.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <ClancyWTNa.h>

ClancyWTNa::ClancyWTNa(ClancyWTNaParameters *pp) {
  pC = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pC, NS_ClancyWTNaParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

#ifdef HETERO

inline bool ClancyWTNa::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool ClancyWTNa::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

void ClancyWTNa::Init() {
  IC3 = CELLMODEL_PARAMVALUE(VT_IC3);
  IC2 = CELLMODEL_PARAMVALUE(VT_IC2);
  IF = CELLMODEL_PARAMVALUE(VT_IF);
  IM1 = CELLMODEL_PARAMVALUE(VT_IM1);
  IM2 = CELLMODEL_PARAMVALUE(VT_IM2);
  C1 = CELLMODEL_PARAMVALUE(VT_C1);
  C2 = CELLMODEL_PARAMVALUE(VT_C2);
  C3 = CELLMODEL_PARAMVALUE(VT_C3);
  O = CELLMODEL_PARAMVALUE(VT_O);
}

void ClancyWTNa::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' ';
  tempstr << C1 << ' ';  // 3
  tempstr << C2 << ' ';
  tempstr << C3 << ' ';  // 5
  tempstr << IF << ' ';
  tempstr << IC2 << ' ';
  tempstr << IC3 << ' ';
  tempstr << O << ' ';
  tempstr << IM1 << ' '; // 10
  tempstr << IM2 << ' ';

  //    tempstr << C1+C2+C3+IF+IC2+IC3+O+IM1+IM2 <<' ';
}

void ClancyWTNa::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);

  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);
  const double RTdF = ElphyModelConstants::R * Tx / ElphyModelConstants::F;
  const double ENa = RTdF * log(CELLMODEL_PARAMVALUE(VT_Nao) / CELLMODEL_PARAMVALUE(VT_Nai));
  ML_CalcType i_Na = CELLMODEL_PARAMVALUE(VT_g_Na) * (V - ENa) * O;
  tempstr << i_Na << ' ';  // 12
}

void ClancyWTNa::GetParameterNames(vector<string> &getpara) {
  const int numpara = 9;
  const string ParaNames[numpara] = {"C1", "C2", "C3", "IF", "IC2", "IC3", "O", "IM1", "IM2"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void ClancyWTNa::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 1;
  const string ParaNames[numpara] = {"I_Na"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType
ClancyWTNa::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
                 int euler = 1) {
  // cout << tinc << " " << V << " " << i_external << " " << stretch << endl;
  /* LongPrint(cout, tinc, V);
     cout << endl;
     pC->PrintParameters();
     cout << endl;
   */

  // pC->PrintParameters();
  ML_CalcType tinMs = tinc * 1000;

  const ML_CalcType a11 =
      CELLMODEL_PARAMVALUE(VT_a11a) / (CELLMODEL_PARAMVALUE(VT_a11b) * exp(-V / CELLMODEL_PARAMVALUE(VT_a11c)) + CELLMODEL_PARAMVALUE(VT_a11d) * exp(-V / CELLMODEL_PARAMVALUE(VT_a11e)));
  const ML_CalcType a12 =
      CELLMODEL_PARAMVALUE(VT_a12a) / (CELLMODEL_PARAMVALUE(VT_a12b) * exp(-V / CELLMODEL_PARAMVALUE(VT_a12c)) + CELLMODEL_PARAMVALUE(VT_a12d) * exp(-V / CELLMODEL_PARAMVALUE(VT_a12e)));
  const ML_CalcType a13 =
      CELLMODEL_PARAMVALUE(VT_a13a) / (CELLMODEL_PARAMVALUE(VT_a13b) * exp(-V / CELLMODEL_PARAMVALUE(VT_a13c)) + CELLMODEL_PARAMVALUE(VT_a13d) * exp(-V / CELLMODEL_PARAMVALUE(VT_a13e)));

  //    cout <<"'1! " << a11 << ' ' << a12 << ' ' << a13 << endl;

  const ML_CalcType b11 = CELLMODEL_PARAMVALUE(VT_b11a) * exp(-(V - CELLMODEL_PARAMVALUE(VT_b11b)) / CELLMODEL_PARAMVALUE(VT_b11c));
  const ML_CalcType b12 = CELLMODEL_PARAMVALUE(VT_b12a) * exp(-(V - CELLMODEL_PARAMVALUE(VT_b12b)) / CELLMODEL_PARAMVALUE(VT_b12c));
  const ML_CalcType b13 = CELLMODEL_PARAMVALUE(VT_b13a) * exp(-(V - CELLMODEL_PARAMVALUE(VT_b13b)) / CELLMODEL_PARAMVALUE(VT_b13c));

  //    cout <<"'2! " << b11 << ' ' << b12 << ' ' << b13 << endl;

  const ML_CalcType a2 = CELLMODEL_PARAMVALUE(VT_a2a) * exp(V / CELLMODEL_PARAMVALUE(VT_a2b));
  const ML_CalcType a3 = CELLMODEL_PARAMVALUE(VT_a3a) * exp(-V / CELLMODEL_PARAMVALUE(VT_a3b));

  const ML_CalcType b3 = CELLMODEL_PARAMVALUE(VT_b3a) + CELLMODEL_PARAMVALUE(VT_b3b) * V;
  const ML_CalcType b2 = (a13 * a2 * a3) / (b13 * b3);

  const ML_CalcType a4 = a2 / CELLMODEL_PARAMVALUE(VT_a4a);
  const ML_CalcType b4 = a3 / CELLMODEL_PARAMVALUE(VT_b4a);

  const ML_CalcType a5 = a2 / CELLMODEL_PARAMVALUE(VT_a5a);
  const ML_CalcType b5 = a3 / CELLMODEL_PARAMVALUE(VT_b5a);

  const ML_CalcType C3C2 = a11 * C3 - b11 * C2;
  const ML_CalcType C2C1 = a12 * C2 - b12 * C1;
  const ML_CalcType C1O = a13 * C1 - b13 * O;
  const ML_CalcType IC3IC2 = a11 * IC3 - b11 * IC2;
  const ML_CalcType IC2IF = a12 * IC2 - b12 * IF;
  const ML_CalcType OIF = a2 * O - b2 * IF;
  const ML_CalcType IFC1 = a3 * IF - b3 * C1;
  const ML_CalcType IC2C2 = a3 * IC2 - b3 * C2;
  const ML_CalcType IC3C3 = a3 * IC3 - b3 * C3;
  const ML_CalcType IFIM1 = a4 * IF - b4 * IM1;
  const ML_CalcType IM1IM2 = a5 * IM1 - b5 * IM2;

  C3 += tinMs * (IC3C3 - C3C2);
  C2 += tinMs * (C3C2 + IC2C2 - C2C1);
  C1 += tinMs * (C2C1 + IFC1 - C1O);

  IC3 += tinMs * (-IC3IC2 - IC3C3);
  IC2 += tinMs * (IC3IC2 - IC2C2 - IC2IF);
  IF += tinMs * (IC2IF - IFC1 + OIF - IFIM1);

  //    cout << C1O << ' ' << OIF << endl;
  IM1 += tinMs * (IFIM1 - IM1IM2);
  IM2 += tinMs * (IM1IM2);

  // O+=tinc*(C1O-OIF);
  O = 1. - C1 - C2 - C3 - IC3 - IC2 - IF - IM1 - IM2;

  return 0;  // ???
} // ClancyWTNa::Calc
