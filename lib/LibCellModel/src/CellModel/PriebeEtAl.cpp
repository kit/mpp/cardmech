/**@file PriebeEtAl.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <PriebeEtAl.h>

PriebeEtAl::PriebeEtAl(PriebeEtAlParameters *pp) {
  pPeaP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pPeaP, NS_PriebeEtAlParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

#ifdef HETERO

bool PriebeEtAl::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

bool PriebeEtAl::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

void PriebeEtAl::Init() {
#if KADEBUG
  cerr << "#initializing Class: PriebeEtAl ... " << endl;
#endif // if KADEBUG
  Ca_itot = CELLMODEL_PARAMVALUE(VT_init_Ca_itot);
  Ca_JSRtot = CELLMODEL_PARAMVALUE(VT_init_Ca_JSRtot);
  Ca_NSR = CELLMODEL_PARAMVALUE(VT_init_Ca_NSR);
  Ca_itotVmax = CELLMODEL_PARAMVALUE(VT_init_Ca_itotVmax);
  m = CELLMODEL_PARAMVALUE(VT_init_m);
  dCa_i2 = CELLMODEL_PARAMVALUE(VT_init_dCa_i2);
  t_CICR = CELLMODEL_PARAMVALUE(VT_init_t_CICR);
  t_Vmax = CELLMODEL_PARAMVALUE(VT_init_t_Vmax);
  CICR = CELLMODEL_PARAMVALUE(VT_init_CICR);
  Xr_SQT = CELLMODEL_PARAMVALUE(VT_init_Xr);
  h = CELLMODEL_PARAMVALUE(VT_init_h);
  j = CELLMODEL_PARAMVALUE(VT_init_j);
  d = CELLMODEL_PARAMVALUE(VT_init_d);
  f = CELLMODEL_PARAMVALUE(VT_init_f);
  r = CELLMODEL_PARAMVALUE(VT_init_r);
  t = CELLMODEL_PARAMVALUE(VT_init_t);
  Xs = CELLMODEL_PARAMVALUE(VT_init_Xs);
  Xr = CELLMODEL_PARAMVALUE(VT_init_Xr);
}

ML_CalcType
PriebeEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
                 int euler = 2) {
  const ML_CalcType tinc_int = tinc * 1000.0;
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);

  const double Ca_i = GetCai();
  const double Ca_JSR =
      -.047671 + Ca_JSRtot * (.11596 + Ca_JSRtot * (Ca_JSRtot * .0019814 - .0062985));
  const double VmE_Ca = V_int - ((CELLMODEL_PARAMVALUE(VT_RTd2F)) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / Ca_i));

  const double m__ = pPeaP->m_[Vi];

  m = m__ - (m__ - m) * pPeaP->exptau_m[Vi];

  // h+=tinc_int*(pPeaP->a_h[Vi]*(1-h)-pPeaP->b_h[Vi]*h);
  const double h_inf = pPeaP->h_inf[Vi];
  h = h_inf + (h - h_inf) * pPeaP->exptau_h[Vi];

  // j+=tinc_int*(pPeaP->a_j[Vi]*(1-j)-pPeaP->b_j[Vi]*j);
  const double j_inf = pPeaP->j_inf[Vi];
  j = j_inf + (j - j_inf) * pPeaP->exptau_j[Vi];

  // d+=tinc_int*(pPeaP->a_d[Vi]*(1-d)-pPeaP->b_d[Vi]*d);
  const double d_inf = pPeaP->d_inf[Vi];
  d = d_inf + (d - d_inf) * pPeaP->exptau_d[Vi];

  // f+=tinc_int*(pPeaP->a_f[Vi]*(1-f)-pPeaP->b_f[Vi]*f);
  const double f_inf = pPeaP->f_inf[Vi];
  f = f_inf + (f - f_inf) * pPeaP->exptau_f[Vi];

  checkGatingVariable(h);

  // r+=tinc_int*(pPeaP->a_r[Vi]*(1-r)-pPeaP->b_r[Vi]*r);
  const double r_inf = pPeaP->r_inf[Vi];
  r = r_inf + (r - r_inf) * pPeaP->exptau_r[Vi];

  // t+=tinc_int*(pPeaP->a_t[Vi]*(1-t)-pPeaP->b_t[Vi]*t);
  const double t_inf = pPeaP->t_inf[Vi];
  t = t_inf + (t - t_inf) * pPeaP->exptau_t[Vi];

  // Xs+=tinc_int*(pPeaP->a_Xs[Vi]*(1-Xs)-pPeaP->b_Xs[Vi]*Xs);
  const double Xs_inf = pPeaP->Xs_inf[Vi];
  Xs = Xs_inf + (Xs - Xs_inf) * pPeaP->exptau_Xs[Vi];

  // Xr+=tinc_int*(pPeaP->a_Xr[Vi]*(1-Xr)-pPeaP->b_Xr[Vi]*Xr);
  const double Xr_inf = pPeaP->Xr_inf[Vi];
  Xr = Xr_inf + (Xr - Xr_inf) * pPeaP->exptau_Xr[Vi];
  if (CELLMODEL_PARAMVALUE(VT_SQT_fraction) > 0) {
    // Xr_SQT+=tinc_int*(pPeaP->a_Xr_SQT[Vi]*(1-Xr_SQT)-pPeaP->b_Xr_SQT[Vi]*Xr_SQT);
    const double Xr_SQT_inf = pPeaP->Xr_SQT_inf[Vi];
    Xr_SQT = Xr_SQT_inf + (Xr_SQT - Xr_SQT_inf) * pPeaP->exptau_Xr_SQT[Vi];
  }

  const double I_Na = pPeaP->C_INa[Vi] * m * m * m * h * j;
  const double I_Ca = (CELLMODEL_PARAMVALUE(VT_C_Ca)) / (CELLMODEL_PARAMVALUE(VT_K_Ca) + Ca_i) * d * f * VmE_Ca;
  const double I_to = pPeaP->C_Ito[Vi] * r * t;
  const double I_Ks = pPeaP->C_IKs[Vi] * Xs * Xs;
  const double I_Kr = pPeaP->C_IKr[Vi] * Xr;
  if (CELLMODEL_PARAMVALUE(VT_SQT_fraction) > 0)
    I_Kr_SQT = pPeaP->C_IKr_SQT[Vi] * Xr_SQT;
  const double I_Cab = (CELLMODEL_PARAMVALUE(VT_g_Cab)) * VmE_Ca;
  const double I_Cages = I_Ca + I_Cab;

  const double I_constV = pPeaP->C_IconstV[Vi];

  const double I_NaCa = GetINaCa(Vi, Ca_i);

  const double dV = -tinc *
                    (I_Cages + I_constV + (1 - (CELLMODEL_PARAMVALUE(VT_SQT_fraction))) * I_Kr +
                     (CELLMODEL_PARAMVALUE(VT_SQT_fraction)) * I_Kr_SQT + I_Ks + I_Na + I_NaCa + I_to - i_external);
  const double V_new = V + dV;

  if ((V < -.0) && (V_new >= -.00) && !CICR) {
    Ca_itotVmax = Ca_itot;
    t_Vmax = .0;
  }

  double I_rel = .0;
  if (CICR == 1) {
    // const double hlp=exp(-t_CICR*(*pPeaP->dt_on));
    // I_rel=22.0*(dCa_i2/(dCa_i2+.0008))*(1.0-hlp)*hlp*(Ca_JSR-Ca_i);
    const double hlp = dCa_i2 - CELLMODEL_PARAMVALUE(VT_dCaith);
    const double hlp2 = exp(-(t_Vmax - t_CICR) / 4.);
    const double hlp3 = 22.0 * (hlp / (hlp + 0.0008)) * (1.0 - hlp2) * hlp2;
    I_rel = hlp3 * (Ca_JSR - Ca_i);
  }

  const double I_tr = (Ca_NSR - Ca_JSR) * (CELLMODEL_PARAMVALUE(VT_dt_tr));
  const double I_upmleak =
      (CELLMODEL_PARAMVALUE(VT_I_upmax)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mup))) - (CELLMODEL_PARAMVALUE(VT_k_leak)) * Ca_NSR;
  Ca_NSR += tinc_int * (I_upmleak - I_tr * (CELLMODEL_PARAMVALUE(VT_VJdVN)));
  Ca_JSRtot += tinc_int * (I_tr - I_rel);

  Ca_itot += tinc_int * (CELLMODEL_PARAMVALUE(VT_VJdm) * I_rel - I_upmleak * (CELLMODEL_PARAMVALUE(VT_VNdm)) -
                         (I_Cages - 2.0 * I_NaCa) * (CELLMODEL_PARAMVALUE(VT_A_Capd2Fm)));

  if ((t_Vmax <= 2.0) && ((t_Vmax + tinc_int) > 2.0)) {
    dCa_i2 = Ca_itot - Ca_itotVmax;
    if (dCa_i2 < 2. *
                 CELLMODEL_PARAMVALUE(VT_dCaith)) // necessary for high frequencies, but better solution is summing up of I_Ca during
      // first 2 ms or modeling of diadic space
      dCa_i2 = 2. * CELLMODEL_PARAMVALUE(VT_dCaith);

    //      if(dCa_i2>*pPeaP->dCaith) {
    CICR = 1;
    t_CICR = t_Vmax;

    // }
  }

  if ((t_Vmax <= 100.0) && ((t_Vmax + tinc_int) > 100.0))
    CICR = 0;
  t_Vmax += tinc_int;

  return dV;
} // PriebeEtAl::Calc

void PriebeEtAl::Print(ostream &tempstr, double tArg, ML_CalcType V) {
  tempstr << tArg << ' ' << V << ' '
          << m << ' '
          << h << ' ' << j << ' '
          << d << ' ' << f << ' '
          << r << ' ' << t << ' '
          << Xr << ' ' << Xs << ' '
          << GetCai() << ' '
          << Ca_itot << ' ' << Ca_itotVmax << ' '
          << dCa_i2 << ' ' << Ca_NSR << ' '
          << Ca_JSRtot << ' ' << t_CICR << ' '
          << t_Vmax << ' ' << CICR << ' ';
}

void PriebeEtAl::LongPrint(ostream &tempstr, double tArg, ML_CalcType V) {
  Print(tempstr, tArg, V);
  const ML_CalcType V_int = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  const double Ca_i = GetCai();
  const double Ca_JSR =
      -.047671 + Ca_JSRtot * (.11596 + Ca_JSRtot * (Ca_JSRtot * .0019814 - .0062985));
  const double VmE_Ca = V_int - ((CELLMODEL_PARAMVALUE(VT_RTd2F)) * log((CELLMODEL_PARAMVALUE(VT_Ca_o)) / Ca_i));
  const double VFdRT = V_int * (CELLMODEL_PARAMVALUE(VT_FdRT));
  const double fNaK = 1.0 / (1.0 + .1245 * exp(-.1 * VFdRT) + .0365 * (CELLMODEL_PARAMVALUE(VT_sigm)) * exp(-VFdRT));
  const double aK1 = CELLMODEL_PARAMVALUE(VT_a_K1_1) /
                     (CELLMODEL_PARAMVALUE(VT_a_K1_2) +
                      exp(CELLMODEL_PARAMVALUE(VT_a_K1_3) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_K) + (CELLMODEL_PARAMVALUE(VT_d_E_K))) + (CELLMODEL_PARAMVALUE(VT_a_K1_4)))));
  const double bK1 =
      (CELLMODEL_PARAMVALUE(VT_b_K1_1) * exp(CELLMODEL_PARAMVALUE(VT_b_K1_2) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_K) + (CELLMODEL_PARAMVALUE(VT_d_E_K))) + CELLMODEL_PARAMVALUE(VT_b_K1_3))) +
       exp(CELLMODEL_PARAMVALUE(VT_b_K1_4) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_K) + (CELLMODEL_PARAMVALUE(VT_d_E_K))) + CELLMODEL_PARAMVALUE(VT_b_K1_5)))) /
      (CELLMODEL_PARAMVALUE(VT_b_K1_6) + exp(CELLMODEL_PARAMVALUE(VT_b_K1_7) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_K) + (CELLMODEL_PARAMVALUE(VT_d_E_K)) + (CELLMODEL_PARAMVALUE(VT_b_K1_8))))));
  tempstr << pPeaP->C_INa[Vi] * m * m * m * h * j << ' '  // I_Na - 20
          << CELLMODEL_PARAMVALUE(VT_C_Ca) / (CELLMODEL_PARAMVALUE(VT_K_Ca) + Ca_i) * d * f * VmE_Ca << ' ' // I_Ca
          << CELLMODEL_PARAMVALUE(VT_g_Cab) * VmE_Ca << ' ' // I_Cab
          << pPeaP->C_Ito[Vi] * r * t << ' ' // I_to
          << pPeaP->C_IKs[Vi] * Xs * Xs << ' ' // I_Ks
          << pPeaP->C_IKr[Vi] * Xr << ' ' // I_Kr - 25
          << (CELLMODEL_PARAMVALUE(VT_g_K1)) * aK1 / (aK1 + bK1) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_K) + (CELLMODEL_PARAMVALUE(VT_d_E_K)))) << ' ' // I_K1
          << (CELLMODEL_PARAMVALUE(VT_g_Nab)) * (V_int - (CELLMODEL_PARAMVALUE(VT_E_Na))) << ' '                           // I_bNa
          << fNaK * (CELLMODEL_PARAMVALUE(VT_cNaK)) << ' '                                            // I_NaK
          << GetINaCa(Vi, Ca_i) << ' ' // I_NaCa
          << (Ca_NSR - Ca_JSR) * (CELLMODEL_PARAMVALUE(VT_dt_tr)) << ' ' // I_tr - 30
          << CELLMODEL_PARAMVALUE(VT_I_upmax) * Ca_i / (Ca_i + CELLMODEL_PARAMVALUE(VT_k_mup)) << ' ' // I_up
          << CELLMODEL_PARAMVALUE(VT_k_leak) * Ca_NSR << ' '; // I_leak

  if (CICR == 1) {
    // const double hlp=exp(-t_CICR*(*pPeaP->dt_on));
    // const double I_rel=3.*(dCa_i2/(dCa_i2+.0008))*(1.0-hlp)*hlp*(Ca_JSR-Ca_i);
    const double hlp = dCa_i2 - CELLMODEL_PARAMVALUE(VT_dCaith);
    const double hlp2 = exp(-(t_Vmax - t_CICR) / 4.);
    const double hlp3 = 22.0 * (hlp / (hlp + 0.0008)) * (1.0 - hlp2) * hlp2;
    const double I_rel = hlp3 * (Ca_JSR - Ca_i);
    tempstr << I_rel << ' ';
  } else {
    tempstr << 0.0 << ' ';  // I_rel - 33
  }
  if (CELLMODEL_PARAMVALUE(VT_SQT_fraction) > 0)
    tempstr << pPeaP->C_IKr_SQT[Vi] * Xr_SQT << ' '
            << (1 - (CELLMODEL_PARAMVALUE(VT_SQT_fraction))) * (pPeaP->C_IKr[Vi]) * Xr + (CELLMODEL_PARAMVALUE(VT_SQT_fraction)) *
                                                                      (pPeaP->C_IKr_SQT[Vi]) *
                                                                      Xr_SQT
            << ' ';  // XR_SQT , IKr, IKr_SQT
} // PriebeEtAl::LongPrint

void PriebeEtAl::GetParameterNames(vector<string> &getpara) {
  const char *ParaNames[] =
      {"m", "h", "j", "d", "f", "r", "t", "Xr", "Xs", "Cai", "Ca_itot", "Ca_itotVmax", "dCa_i2",
       "Ca_NSR",
       "Ca_JSRtot",
       "t_CICR", "t_Vmax", "CICR"};

  for (int i = 0; i < sizeof(ParaNames) / sizeof(char *); i++)
    getpara.push_back(ParaNames[i]);
}

void PriebeEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const string ParaNames[] =
      {"I_Na", "I_Ca", "I_bCa", "I_to", "I_Ks", "I_Kr", "I_K1", "I_bNa", "INaK", "I_NaCa", "I_tr",
       "I_up",
       "I_leak",
       "I_rel", "Xr_SQT", "IKr_(1-SQT)", "IKr_SQT"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(char *); i++)
    getpara.push_back(ParaNames[i]);
}
