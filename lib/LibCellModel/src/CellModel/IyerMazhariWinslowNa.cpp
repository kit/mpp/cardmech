/**@file IyerMazhariWinslowNa.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowNa.h>

IyerMazhariWinslowNa::IyerMazhariWinslowNa(IyerMazhariWinslowNaParameters *pIMWArg) {
  pIMW = pIMWArg;
#ifdef HETERO
  PS = new ParameterSwitch(pIMW, NS_IyerMazhariWinslowNaParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

void IyerMazhariWinslowNa::Init() {
  C0Na  = CELLMODEL_PARAMVALUE(VT_C0Na);
  C1Na  = CELLMODEL_PARAMVALUE(VT_C1Na);
  C2Na  = CELLMODEL_PARAMVALUE(VT_C2Na);
  C3Na  = CELLMODEL_PARAMVALUE(VT_C3Na);
  C4Na  = CELLMODEL_PARAMVALUE(VT_C4Na);
  O1Na  = CELLMODEL_PARAMVALUE(VT_O1Na);
  O2Na  = CELLMODEL_PARAMVALUE(VT_O2Na);
  CI0Na = CELLMODEL_PARAMVALUE(VT_CI0Na);
  CI1Na = CELLMODEL_PARAMVALUE(VT_CI1Na);
  CI2Na = CELLMODEL_PARAMVALUE(VT_CI2Na);
  CI3Na = CELLMODEL_PARAMVALUE(VT_CI3Na);
  CI4Na = CELLMODEL_PARAMVALUE(VT_CI4Na);
  INa   = CELLMODEL_PARAMVALUE(VT_INa);
}

void IyerMazhariWinslowNa::Print(ostream& tempstr, double t, ML_CalcType V) {
  tempstr << t<< ' '  // 1
          << V<< ' '
          << C0Na << ' '
          << C1Na << ' '
          << C2Na << ' ' // 5
          << C3Na << ' '
          << C4Na << ' '
          << O1Na << ' '
          << O2Na << ' '
          << CI0Na << ' ' // 10
          << CI1Na << ' '
          << CI2Na << ' '
          << CI3Na << ' '
          << CI4Na << ' '
          << INa; // 15
}

void IyerMazhariWinslowNa::LongPrint(ostream& tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);

  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);

  const double V_int = V*1000.0;
  const double RTdF  = ElphyModelConstants::R *Tx/ElphyModelConstants::F;

  const double Nao = CELLMODEL_PARAMVALUE(VT_Nao);
  const double Nai = CELLMODEL_PARAMVALUE(VT_Nai);

  const double ENa = RTdF*log(Nao/Nai);  // 70 mV for resting 1 Hz

  const double I_Na = CELLMODEL_PARAMVALUE(VT_GNa)*(O1Na+O2Na)*(V_int-ENa);

  tempstr << ' ' << I_Na;  // 16
}

void IyerMazhariWinslowNa::GetParameterNames(vector<string>& getpara) {
  const int numpara               = 1;
  const string ParaNames[numpara] = { "To be done..." };

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void IyerMazhariWinslowNa::GetLongParameterNames(vector<string>& getpara) {
  GetParameterNames(getpara);
  const int numpara               = 1;
  const string ParaNames[numpara] = { "To be done..." };
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType IyerMazhariWinslowNa::Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim = .0, ML_CalcType stretch = 1.,
                                       int euler = 1) {
  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);

  tinc *= 1000.0;
  const double V_int = V*1000.0;
  const double RTdF  = ElphyModelConstants::R *Tx/ElphyModelConstants::F;
  const double FdRT  = 1./RTdF;
  const double VFdRT = V_int*FdRT;

  const double Naa1 = CELLMODEL_PARAMVALUE(VT_Naa1), Naa2 = CELLMODEL_PARAMVALUE(VT_Naa2), Naa3 = CELLMODEL_PARAMVALUE(VT_Naa3);
  const double Nab1 = CELLMODEL_PARAMVALUE(VT_Nab1), Nab2 = CELLMODEL_PARAMVALUE(VT_Nab2), Nab3 = CELLMODEL_PARAMVALUE(VT_Nab3);
  const double Nag1 = CELLMODEL_PARAMVALUE(VT_Nag1), Nag2 = CELLMODEL_PARAMVALUE(VT_Nag2), Nag3 = CELLMODEL_PARAMVALUE(VT_Nag3);
  const double Nad1 = CELLMODEL_PARAMVALUE(VT_Nad1), Nad2 = CELLMODEL_PARAMVALUE(VT_Nad2), Nad3 = CELLMODEL_PARAMVALUE(VT_Nad3);
  const double NaOn1 = CELLMODEL_PARAMVALUE(VT_NaOn1), NaOn2 = CELLMODEL_PARAMVALUE(VT_NaOn2), NaOn3 = CELLMODEL_PARAMVALUE(VT_NaOn3);
  const double NaOf1 = CELLMODEL_PARAMVALUE(VT_NaOf1), NaOf2 = CELLMODEL_PARAMVALUE(VT_NaOf2), NaOf3 = CELLMODEL_PARAMVALUE(VT_NaOf3);
  const double Nagg1 = CELLMODEL_PARAMVALUE(VT_Nagg1), Nagg2 = CELLMODEL_PARAMVALUE(VT_Nagg2), Nagg3 = CELLMODEL_PARAMVALUE(VT_Nagg3);
  const double Nadd1 = CELLMODEL_PARAMVALUE(VT_Nadd1), Nadd2 = CELLMODEL_PARAMVALUE(VT_Nadd2), Nadd3 = CELLMODEL_PARAMVALUE(VT_Nadd3);
  const double Nae1 = CELLMODEL_PARAMVALUE(VT_Nae1), Nae2 = CELLMODEL_PARAMVALUE(VT_Nae2), Nae3 = CELLMODEL_PARAMVALUE(VT_Nae3);
  const double NaO1 = CELLMODEL_PARAMVALUE(VT_NaO1), NaO2 = CELLMODEL_PARAMVALUE(VT_NaO2), NaO3 = CELLMODEL_PARAMVALUE(VT_NaO3);
  const double Naeta1 = CELLMODEL_PARAMVALUE(VT_Naeta1), & Naeta2 = CELLMODEL_PARAMVALUE(VT_Naeta2), & Naeta3 = CELLMODEL_PARAMVALUE(VT_Naeta3);
  const double Nanu1 = CELLMODEL_PARAMVALUE(VT_Nanu1), Nanu2 = CELLMODEL_PARAMVALUE(VT_Nanu2), Nanu3 = CELLMODEL_PARAMVALUE(VT_Nanu3);
  const double NaCn1 = CELLMODEL_PARAMVALUE(VT_NaCn1), NaCn2 = CELLMODEL_PARAMVALUE(VT_NaCn2), NaCn3 = CELLMODEL_PARAMVALUE(VT_NaCn3);
  const double NaCf1 = CELLMODEL_PARAMVALUE(VT_NaCf1), NaCf2 = CELLMODEL_PARAMVALUE(VT_NaCf2), NaCf3 = CELLMODEL_PARAMVALUE(VT_NaCf3);
  const double NaScalinga = CELLMODEL_PARAMVALUE(VT_NaScalinga);
  const double NaQ        = CELLMODEL_PARAMVALUE(VT_NaQ);

  const double Naa =  NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -Naa1/(ElphyModelConstants::R*Tx)+Naa2/ElphyModelConstants::R  +Naa3*VFdRT);
  const double Nab =  NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -Nab1/(ElphyModelConstants::R*Tx)+Nab2/ElphyModelConstants::R  +Nab3*VFdRT);
  const double Nag =  NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -Nag1/(ElphyModelConstants::R*Tx)+Nag2/ElphyModelConstants::R  +Nag3*VFdRT);
  const double Nad =  NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -Nad1/(ElphyModelConstants::R*Tx)+Nad2/ElphyModelConstants::R  +Nad3*VFdRT);
  const double NaOn = NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -NaOn1/(ElphyModelConstants::R*Tx)+NaOn2/ElphyModelConstants::R +NaOn3*VFdRT);
  const double NaOf = NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -NaOf1/(ElphyModelConstants::R*Tx)+NaOf2/ElphyModelConstants::R +NaOf3*VFdRT);
  const double Nagg = NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -Nagg1/(ElphyModelConstants::R*Tx)+Nagg2/ElphyModelConstants::R +Nagg3*VFdRT);
  const double Nadd = NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -Nadd1/(ElphyModelConstants::R*Tx)+Nadd2/ElphyModelConstants::R +Nadd3*VFdRT);
  const double Nae =  NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -Nae1/(ElphyModelConstants::R*Tx)+Nae2/ElphyModelConstants::R  +Nae3*VFdRT);
  const double NaO =  NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -NaO1/(ElphyModelConstants::R*Tx)+NaO2/ElphyModelConstants::R  +NaO3*VFdRT);
  const double Naeta = NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -Naeta1/(ElphyModelConstants::R*Tx)+Naeta2/ElphyModelConstants::R+Naeta3*VFdRT);
  const double Nanu = NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -Nanu1/(ElphyModelConstants::R*Tx)+Nanu2/ElphyModelConstants::R +Nanu3*VFdRT);
  const double NaCn = NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -NaCn1/(ElphyModelConstants::R*Tx)+NaCn2/ElphyModelConstants::R +NaCn3*VFdRT);
  const double NaCf = NaQ*ElphyModelConstants::k *Tx/ElphyModelConstants::h*exp(
      -NaCf1/(ElphyModelConstants::R*Tx)+NaCf2/ElphyModelConstants::R +NaCf3*VFdRT);

  //    if (Nag>1e6) Nag=1e6;
  //    if (Naeta>1e6) Naeta=1e6;

  /*    cerr << "NaRates: " << Naa << ' ' //1
        << Nab << ' '
        << Nag << ' '
        << Nad << ' '
        << NaOn << ' ' //5
        << NaOf << ' '
        << Nagg << ' '
        << Nadd << ' '
        << Nae << ' '
        << NaO << ' ' //10
        << Naeta << ' '
        << Nanu  << ' '
        << NaCn << ' '
        << NaCf << endl;
   */

  const double NaScalinga2 = NaScalinga*NaScalinga;
  const double NaScalinga3 = NaScalinga2*NaScalinga;
  const double NaScalinga4 = NaScalinga3*NaScalinga;

  const int NaMaxi    = 50;
  const double tincNa = tinc/NaMaxi;

  for (int i = 0; i < NaMaxi; i++) {
    const double dC0Na = (-(NaCn+            4.*Naa)   *C0Na            +   Nab*C1Na       +NaCf            *CI0Na);
    const double dC1Na =
      (-(Nab+NaCn*NaScalinga +3.*Naa)   *C1Na+4.*Naa*C0Na+2.*Nab*C2Na       +NaCf/NaScalinga *CI1Na);
    const double dC2Na =
      (-(2.*Nab+NaCn*NaScalinga2+2.*Naa)   *C2Na+3.*Naa*C1Na+3.*Nab*C3Na       +NaCf/NaScalinga2*CI2Na);
    const double dC3Na =
      (-(3.*Nab+NaCn*NaScalinga3+   Naa)   *C3Na+2.*Naa*C2Na+4.*Nab*C4Na       +NaCf/NaScalinga3*CI3Na);
    const double dC4Na =
      (-(4.*Nab+NaCn*NaScalinga4+Nag+Naeta)*C4Na+   Naa*C3Na+Nad*O1Na+Nanu*O2Na+NaCf/NaScalinga4*CI4Na);
    const double dO1Na  = (-(Nad+Nae+NaOn)*O1Na+Nag*C4Na+NaO*O2Na+NaOf*INa);
    const double dO2Na  = (-(NaO+Nanu)*O2Na+Nae*O1Na+Naeta*C4Na);
    const double dCI0Na =
      (-(4.*Naa*NaScalinga+NaCf)            *CI0Na                        +   Nab/NaScalinga*CI1Na+NaCn            *
       C0Na);
    const double dCI1Na =
      (-(Nab/NaScalinga+3.*Naa*NaScalinga+NaCf/NaScalinga)*CI1Na+4.*Naa*NaScalinga*CI0Na+2.*Nab/NaScalinga*CI2Na+NaCn*
       NaScalinga *C1Na);
    const double dCI2Na =
      (-(2.*Nab/NaScalinga+2.*Naa*NaScalinga+NaCf/NaScalinga2)*CI2Na+3.*Naa*NaScalinga*CI1Na+3.*Nab/NaScalinga*CI3Na+
       NaCn*
       NaScalinga2*C2Na);
    const double dCI3Na =
      (-(3.*Nab/NaScalinga+   Naa*NaScalinga+NaCf/NaScalinga3)*CI3Na+2.*Naa*NaScalinga*CI2Na+4.*Nab/NaScalinga*CI4Na+
       NaCn*
       NaScalinga3*C3Na);
    const double dCI4Na =
      (-(4.*Nab/NaScalinga+Nagg             +NaCf/NaScalinga4)*CI4Na+   Naa*NaScalinga*CI3Na+Nadd*INa               +
       NaCn*
       NaScalinga4*C4Na);

    C0Na  += tincNa*dC0Na;
    C1Na  += tincNa*dC1Na;
    C2Na  += tincNa*dC2Na;
    C3Na  += tincNa*dC3Na;
    C4Na  += tincNa*dC4Na;
    O1Na  += tincNa*dO1Na;
    O2Na  += tincNa*dO2Na;
    CI0Na += tincNa*dCI0Na;
    CI1Na += tincNa*dCI1Na;
    CI2Na += tincNa*dCI2Na;
    CI3Na += tincNa*dCI3Na;
    CI4Na += tincNa*dCI4Na;
    INa    = 1.-C0Na-C1Na-C2Na-C3Na-C4Na-O1Na-O2Na-CI0Na-CI1Na-CI2Na-CI3Na-CI4Na;
    if ((INa < 0.) && (INa > -1.e-6) )
      INa = 0.;

    assert(C0Na >= 0.);
    assert(C1Na >= 0.);
    assert(C2Na >= 0.);
    assert(C3Na >= 0.);
    assert(C4Na >= 0.);
    assert(O1Na >= 0.);
    assert(O2Na >= 0.);
    assert(CI0Na >= 0.);
    assert(CI1Na >= 0.);
    assert(CI2Na >= 0.);
    assert(CI3Na >= 0.);
    assert(CI4Na >= 0.);
    assert(INa >= 0. && INa <= 1.);

    /*
       cerr << "NaState: " << C0Na << ' ' //1
       << C1Na << ' '
       << C2Na << ' '
       << C3Na << ' '
       << C4Na << ' ' //5
       << O1Na << ' '
       << O2Na << ' '
       << CI0Na << ' '
       << CI1Na << ' '
       << CI2Na << ' '//10
       << CI3Na << ' '
       << CI4Na << ' '
       << INa;
       cerr << ' '  << I_Na
       << endl;
     */
  }

  return 0.;
} // IyerMazhariWinslowNa::Calc
