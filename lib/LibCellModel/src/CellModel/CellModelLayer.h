/*
**      Name
**              CellModelLayer.h
**
**      Usage
**
**
**      Remark
**
**
**
**      History
**              10.09.02 gs
**
**
**  created at IBT - Universität Karlsruhe
*/

#ifndef CELLMODELLAYER_H
#define CELLMODELLAYER_H

#include "ForceDummy.h"
#include "Peterson.h"
#include "Landesberg.h"
#include "Rice3.h"
#include "Rice4.h"
#include "Rice5.h"
#include "Rice08.h"
#include "HunterDynamic.h"
#include "HybridModel.h"
#include "NobleForce.h"
#include "Land17.h"

#include "ElphyDummy.h"
#include "BeelerReuter.h"
#include "LuoRudy.h"
#include "CourtemancheEtAl.h"
#include "DemirEtAl.h"
#include "DiFrancescoNoble.h"
#include "EarmHilgemannNoble.h"
#include "NygrenEtAl.h"
#include "PriebeEtAl.h"
#include "NobleEtAl.h"
#include "NobleCa.h"
#include "WinslowCanine.h"
#include "Zhang.h"
#include "TenTusscher.h"
#include "TenTusscher2.h"

#ifdef osMac_Vec
# include <Iyer.h>
#endif // ifdef osMac_Vec

#include "KohlNoble.h"
#include "PiperEtAl.h"
#include "ClancyWTNa.h"
#include "BondarenkoCa.h"
#include "IyerMazhariWinslow.h"
#include "IyerMazhariWinslowCa.h"
#include "IyerMazhariWinslowNa.h"
#include "IyerMazhariWinslowKs.h"
#include "IyerMazhariWinslowKr.h"
#include "IyerMazhariWinslowK1.h"
#include "SachseEtAl.h"
#include "LuEtAl.h"
#include "PerryEtAl.h"
#include "PanditEtAl.h"
#include "PuglisiBers.h"
#include "FentonKarma.h"
#include "Kurata.h"
#include "SharedModel.h"
#include "Fenton4State.h"
#include "LindbladEtAl.h"
#include "MarkovGating.h"
#include "Hund.h"
#include "MaleckarEtAl.h"
#include "KoivumaekiEtAl.h"
#include "MahajanEtAl.h"
#include "GrandiEtAlVentricle.h"
#include "GrandiEtAlAtrium.h"
#include "ChandlerEtAl.h"
#include "FabbriEtAl.h"
#include "Himeno.h"
#include "ShannonEtAl.h"

template<class CalcType>

inline void
initForceParameters(vbForceParameters<CalcType> **pfp, const char *initFVFile, ForceModelType fmt) {
  if (*pfp) {
    delete *pfp;
    *pfp = NULL;
  }
  switch (fmt) {
    case FMT_Peterson:
      *pfp = new PetersonParameters(initFVFile);
      return;

    case FMT_Landesberg:
      *pfp = new LandesbergParameters(initFVFile);
      return;

    case FMT_Rice3:
      *pfp = new Rice3Parameters(initFVFile);
      return;

    case FMT_Rice4_1:
      *pfp = new Rice4_1Parameters(initFVFile);
      return;

    case FMT_Rice4_2:
      *pfp = new Rice4_2Parameters(initFVFile);
      return;

    case FMT_Rice4_3:
      *pfp = new Rice4_3Parameters(initFVFile);
      return;

    case FMT_Rice5_1:
      *pfp = new Rice5_1Parameters(initFVFile);
      return;

    case FMT_Rice5_2:
      *pfp = new Rice5_2Parameters(initFVFile);
      return;

    case FMT_Rice5_3:
      *pfp = new Rice5_3Parameters(initFVFile);
      return;

    case FMT_Rice08:
      *pfp = new Rice08Parameters(initFVFile);
      return;

    case FMT_HunterDynamic:
      *pfp = new HunterDynamicParameters(initFVFile);
      return;

    case FMT_NobleForce:
      *pfp = new NobleForceParameters(initFVFile);
      return;

    case FMT_Hybrid:
      *pfp = new HybridModelParameters(initFVFile);
      return;

    case FMT_Land17:
      *pfp = new Land17Parameters(initFVFile);
      return;

    case FMT_Dummy:
      return;

    case FMT_Panerai:
    case FMT_TwoStateModel:
    case FMT_TwoStateExtended:
    case FMT_Last:
    default:
      throw kaBaseException("Init force parameters failed");
      return;
  } // switch
} // initForceParameters

template<class CalcType>

inline void
initForceModel(vbForceModel<CalcType> **pfm, vbForceParameters<CalcType> *pfp, ForceModelType fmt) {
  if (*pfm) {
    delete *pfm;
    *pfm = NULL;
  }
  switch (fmt) {
    case FMT_Dummy:
      *pfm = new ForceDummy<CalcType>();
      break;
    case FMT_Peterson:
      *pfm = new Peterson((PetersonParameters *) pfp);
      break;
    case FMT_Landesberg:
      *pfm = new Landesberg((LandesbergParameters *) pfp);
      break;
    case FMT_Rice3:
      *pfm = new Rice3((Rice3Parameters *) pfp);
      break;
    case FMT_Rice4_1:
      *pfm = new Rice4((Rice4_1Parameters *) pfp);
      break;
    case FMT_Rice4_2:
      *pfm = new Rice4((Rice4_2Parameters *) pfp);
      break;
    case FMT_Rice4_3:
      *pfm = new Rice4((Rice4_3Parameters *) pfp);
      break;
    case FMT_Rice5_1:
      *pfm = new Rice5((Rice5_1Parameters *) pfp);
      break;
    case FMT_Rice5_2:
      *pfm = new Rice5((Rice5_2Parameters *) pfp);
      break;
    case FMT_Rice5_3:
      *pfm = new Rice5((Rice5_3Parameters *) pfp);
      break;
    case FMT_Rice08:
      *pfm = new Rice08((Rice08Parameters *) pfp);
      break;
    case FMT_HunterDynamic:
      *pfm = new HunterDynamic((HunterDynamicParameters *) pfp);
      break;
    case FMT_NobleForce:
      *pfm = new NobleForce((NobleForceParameters *) pfp);
      break;
    case FMT_Hybrid:
      *pfm = new HybridModel((HybridModelParameters *) pfp);
      break;
    case FMT_Land17:
      *pfm = new Land17((Land17Parameters *) pfp);
      break;
    default:
      throw kaBaseException("Undefined force model");
      break;
  } // switch
} // initForceModel

template<class CalcType>


inline void
initElphyParameters(vbElphyParameters<CalcType> **pep, const char *initEVFile, ElphyModelType emt,
                    ML_CalcType tinc) {
  if (*pep) {
    delete *pep;
    *pep = NULL;
  }
  switch (emt) {
    case EMT_BeelerReuter:
      *pep = new BeelerReuterParameters(initEVFile, tinc);
      return;

    case EMT_LuoRudy:
      *pep = new LuoRudyParameters(initEVFile, EMT_LuoRudy, tinc);
      return;

    case EMT_LuoRudyCa:
      *pep = new LuoRudyParameters(initEVFile, EMT_LuoRudyCa, tinc);
      return;

    case EMT_CourtemancheEtAl:
      *pep = new CourtemancheParameters(initEVFile, tinc);
      return;

    case EMT_DemirEtAl:
      *pep = new DemirEtAlParameters(initEVFile, tinc);
      return;

    case EMT_DiFrancescoNoble:
      *pep = new DiFrancescoNobleParameters(initEVFile, tinc);
      return;

    case EMT_EarmHilgemannNoble:
      *pep = new EarmHilgemannNobleParameters(initEVFile, tinc);
      return;

    case EMT_NygrenEtAl:
      *pep = new NygrenEtAlParameters(initEVFile, tinc);
      return;

    case EMT_PriebeEtAl:
    case EMT_PriebeEtAlCa:
    case EMT_PriebeWeber:
    case EMT_PriebeWeberCa:
      *pep = new PriebeEtAlParameters(initEVFile, emt, tinc);
      return;

    case EMT_WinslowCanine:
      *pep = new WinslowCanineParameters(initEVFile, tinc);
      return;

    case EMT_NobleSingleCell:
    case EMT_NobleStretch:
    case EMT_NobleStretchComplex:
    case EMT_NobleStretchSimple:
    case EMT_NobleCaSingleCell:
    case EMT_NobleCaStretchSimple:
    case EMT_NobleCaStretch:
    case EMT_NobleCaStretchComplex:
      *pep = new NobleEtAlParameters(initEVFile, emt, tinc);
      return;

    case EMT_Zhang:
      *pep = new ZhangParameters(initEVFile, tinc);
      return;

    case EMT_TenTusscher:
      *pep = new TenTusscherEtAlParameters(initEVFile, tinc);
      return;

    case EMT_TenTusscher2:
      *pep = new TenTusscher2Parameters(initEVFile, tinc);
      return;

#ifdef osMac_Vec
      case EMT_Iyer:
        *pep = new IyerParameters(initEVFile); return;

#endif // ifdef osMac_Vec
    case EMT_KohlNoble:
      *pep = new KohlNobleParameters(initEVFile);
      return;

    case EMT_PiperEtAl:
      *pep = new PiperEtAlParameters(initEVFile);
      return;

    case EMT_ClancyWTNa:
      *pep = new ClancyWTNaParameters(initEVFile);
      return;

    case EMT_BondarenkoCa:
      *pep = new BondarenkoCaParameters(initEVFile);
      return;

    case EMT_IyerMazhariWinslow:
      *pep = new IyerMazhariWinslowParameters(initEVFile);
      return;

    case EMT_IyerMazhariWinslowCa:
      *pep = new IyerMazhariWinslowCaParameters(initEVFile);
      return;

    case EMT_IyerMazhariWinslowNa:
      *pep = new IyerMazhariWinslowNaParameters(initEVFile);
      return;

    case EMT_IyerMazhariWinslowKs:
      *pep = new IyerMazhariWinslowKsParameters(initEVFile);
      return;

    case EMT_IyerMazhariWinslowKr:
      *pep = new IyerMazhariWinslowKrParameters(initEVFile);
      return;

    case EMT_IyerMazhariWinslowK1:
      *pep = new IyerMazhariWinslowK1Parameters(initEVFile);
      return;

    case EMT_SachseEtAl:
      *pep = new SachseEtAlParameters(initEVFile);
      return;

    case EMT_LuEtAl:
      *pep = new LuEtAlParameters(initEVFile);
      return;

    case EMT_PerryEtAl:
      *pep = new PerryEtAlParameters(initEVFile);
      return;

    case EMT_PanditEtAl:
      *pep = new PanditEtAlParameters(initEVFile, tinc);
      return;

    case EMT_PuglisiBers:
      *pep = new PuglisiBersParameters(initEVFile, tinc);
      return;

    case EMT_FentonKarma:
      *pep = new FentonKarmaParameters(initEVFile);
      return;

    case EMT_Fenton4State:
      *pep = new Fenton4StateParameters(initEVFile, tinc);
      return;

    case EMT_Kurata:
      *pep = new KurataParameters(initEVFile);
      return;

    case EMT_LindbladEtAl:
      *pep = new LindbladEtAlParameters(initEVFile, tinc);
      return;

    case EMT_MarkovGating:
      *pep = new MarkovGatingParameters(initEVFile);
      return;

    case EMT_Hund:
      *pep = new HundParameters(initEVFile, tinc);
      return;

    case EMT_MaleckarEtAl:
      *pep = new MaleckarEtAlParameters(initEVFile, tinc);
      return;

    case EMT_KoivumaekiEtAl:
      *pep = new KoivumaekiEtAlParameters(initEVFile, tinc);
      return;

    case EMT_MahajanEtAl:
      *pep = new MahajanEtAlParameters(initEVFile, tinc);
      return;

    case EMT_GrandiEtAlVentricle:
      *pep = new GrandiEtAlVentricleParameters(initEVFile, tinc);
      return;

    case EMT_GrandiEtAlAtrium:
      *pep = new GrandiEtAlAtriumParameters(initEVFile, tinc);
      return;

    case EMT_ChandlerEtAl:
      *pep = new ChandlerParameters(initEVFile, tinc);
      return;

    case EMT_FabbriEtAl:
      *pep = new FabbriParameters(initEVFile, tinc);
      return;

    case EMT_HimenoEtAl:
      *pep = new HimenoParameters(initEVFile, tinc);
      return;

    case EMT_ShannonEtAl:
      *pep = new ShannonEtAlParameters(initEVFile, tinc);
      return;

#ifdef osMac_Vec
      case EMT_SharedModel:
        *pep = new SharedModelParameters(initEVFile); return;

#endif // ifdef osMac_Vec
    case EMT_Dummy:
      return;

    case EMT_Last:
    default:
      throw kaBaseException("Init elphy parameters failed");
      return;
  } // switch
} // initElphyParameters

template<class CalcType>

inline void
initElphyModel(vbElphyModel<CalcType> **pem, vbElphyParameters<CalcType> *pep, ElphyModelType emt,
               bool opt = false) {
  if (*pem) {
    delete *pem;
    *pem = NULL;
  }
  switch (emt) {
    case EMT_Dummy:
      *pem = new ElphyDummy<CalcType>();
      break;

    case EMT_BeelerReuter:
      *pem = new BeelerReuter((BeelerReuterParameters *) pep);
      break;

    case EMT_LuoRudy:
      *pem = new LuoRudy((LuoRudyParameters *) pep);
      break;

    case EMT_LuoRudyCa:
      *pem = new LuoRudyCa((LuoRudyParameters *) pep);
      break;

    case EMT_CourtemancheEtAl:
      *pem = new Courtemanche((CourtemancheParameters *) pep);
      break;

    case EMT_DemirEtAl:
      *pem = new DemirEtAl((DemirEtAlParameters *) pep);
      break;

    case EMT_DiFrancescoNoble:
      *pem = new DiFrancescoNoble((DiFrancescoNobleParameters *) pep);
      break;

    case EMT_EarmHilgemannNoble:
      *pem = new EarmHilgemannNoble((EarmHilgemannNobleParameters *) pep);
      break;

    case EMT_NygrenEtAl:
      *pem = new NygrenEtAl((NygrenEtAlParameters *) pep);
      break;

    case EMT_NobleSingleCell:
      *pem = new NobleSingleCell((NobleEtAlParameters *) pep);
      break;

    case EMT_NobleStretch:
      *pem = new NobleStretch((NobleEtAlParameters *) pep);
      break;

    case EMT_NobleStretchSimple:
      *pem = new NobleStretchSimple((NobleEtAlParameters *) pep);
      break;

    case EMT_NobleStretchComplex:
      *pem = new NobleStretchComplex((NobleEtAlParameters *) pep);
      break;

    case EMT_PriebeEtAl:
      *pem = new PriebeEtAl((PriebeEtAlParameters *) pep);
      break;

    case EMT_PriebeEtAlCa:
      *pem = new PriebeEtAlCa((PriebeEtAlParameters *) pep);
      break;

    case EMT_PriebeWeber:
      *pem = new PriebeWeber((PriebeEtAlParameters *) pep);
      break;

    case EMT_PriebeWeberCa:
      *pem = new PriebeWeberCa((PriebeEtAlParameters *) pep);
      break;

    case EMT_NobleCaSingleCell:
      *pem = new NobleCaSingleCell((NobleEtAlParameters *) pep);
      break;

    case EMT_NobleCaStretchSimple:
      *pem = new NobleCaStretchSimple((NobleEtAlParameters *) pep);
      break;

    case EMT_NobleCaStretch:
      *pem = new NobleCaStretch((NobleEtAlParameters *) pep);
      break;

    case EMT_NobleCaStretchComplex:
      *pem = new NobleCaStretchComplex((NobleEtAlParameters *) pep);
      break;

    case EMT_WinslowCanine:
      if (!opt)
        *pem = new WinslowCanine((WinslowCanineParameters *) pep);
      else
        throw kaBaseException("Parameter -opt is not implemented for WinslowCanine ElphyModel");
      break;

    case EMT_Zhang:
      *pem = new Zhang((ZhangParameters *) pep);
      break;

    case EMT_TenTusscher:
      *pem = new TenTusscherEtAl((TenTusscherEtAlParameters *) pep);
      break;

    case EMT_TenTusscher2:
      *pem = new TenTusscherEtAl2((TenTusscher2Parameters *) pep);
      break;

#ifdef osMac_Vec
      case EMT_Iyer:
        *pem = new Iyer((IyerParameters*)pep);
        break;
#endif // ifdef osMac_Vec

    case EMT_KohlNoble:
      *pem = new KohlNoble((KohlNobleParameters *) pep);
      break;

    case EMT_PiperEtAl:
      *pem = new PiperEtAl((PiperEtAlParameters *) pep);
      break;

    case EMT_ClancyWTNa:
      *pem = new ClancyWTNa((ClancyWTNaParameters *) pep);
      break;

    case EMT_BondarenkoCa:
      *pem = new BondarenkoCa((BondarenkoCaParameters *) pep);
      break;

    case EMT_IyerMazhariWinslow:
      *pem = new IyerMazhariWinslow((IyerMazhariWinslowParameters *) pep);
      break;

    case EMT_IyerMazhariWinslowCa:
      *pem = new IyerMazhariWinslowCa((IyerMazhariWinslowCaParameters *) pep);
      break;

    case EMT_IyerMazhariWinslowNa:
      *pem = new IyerMazhariWinslowNa((IyerMazhariWinslowNaParameters *) pep);
      break;

    case EMT_IyerMazhariWinslowKs:
      *pem = new IyerMazhariWinslowKs((IyerMazhariWinslowKsParameters *) pep);
      break;

    case EMT_IyerMazhariWinslowKr:
      *pem = new IyerMazhariWinslowKr((IyerMazhariWinslowKrParameters *) pep);
      break;

    case EMT_IyerMazhariWinslowK1:
      *pem = new IyerMazhariWinslowK1((IyerMazhariWinslowK1Parameters *) pep);
      break;

    case EMT_SachseEtAl:
      *pem = new SachseEtAl((SachseEtAlParameters *) pep);
      break;

    case EMT_LuEtAl:
      *pem = new LuEtAl((LuEtAlParameters *) pep);
      break;

    case EMT_PerryEtAl:
      *pem = new PerryEtAl((PerryEtAlParameters *) pep);
      break;

    case EMT_PanditEtAl:
      *pem = new PanditEtAl((PanditEtAlParameters *) pep);
      break;

    case EMT_PuglisiBers:
      *pem = new PuglisiBers((PuglisiBersParameters *) pep);
      break;

    case EMT_FentonKarma:
      *pem = new FentonKarma((FentonKarmaParameters *) pep);
      break;

    case EMT_Fenton4State:
      *pem = new Fenton4State((Fenton4StateParameters *) pep);
      break;

    case EMT_Kurata:
      *pem = new Kurata((KurataParameters *) pep);
      break;

    case EMT_LindbladEtAl:
      *pem = new LindbladEtAl((LindbladEtAlParameters *) pep);
      break;

    case EMT_MarkovGating:
      *pem = new MarkovGating((MarkovGatingParameters *) pep);
      break;

    case EMT_Hund:
      *pem = new Hund((HundParameters *) pep);
      break;

    case EMT_MaleckarEtAl:
      *pem = new MaleckarEtAl((MaleckarEtAlParameters *) pep);
      break;

    case EMT_KoivumaekiEtAl:
      *pem = new KoivumaekiEtAl((KoivumaekiEtAlParameters *) pep);
      break;

    case EMT_MahajanEtAl:
      *pem = new MahajanEtAl((MahajanEtAlParameters *) pep);
      break;

    case EMT_GrandiEtAlVentricle:
      *pem = new GrandiEtAlVentricle((GrandiEtAlVentricleParameters *) pep);
      break;

    case EMT_GrandiEtAlAtrium:
      *pem = new GrandiEtAlAtrium((GrandiEtAlAtriumParameters *) pep);
      break;

    case EMT_ChandlerEtAl:
      *pem = new Chandler((ChandlerParameters *) pep);
      break;
    case EMT_ShannonEtAl:
      *pem = new ShannonEtAl((ShannonEtAlParameters *) pep);
      break;
#ifdef osMac_Vec
      case EMT_SharedModel:
        *pem = new SharedModel((SharedModelParameters*)pep);
        break;
#endif // ifdef osMac_Vec
    case EMT_FabbriEtAl:
      *pem = new Fabbri((FabbriParameters *) pep);
      break;
    case EMT_HimenoEtAl:
      *pem = new Himeno((HimenoParameters *) pep);
      break;


    default:
      throw kaBaseException("Undefined elphy model");
  } // switch
} // initElphyModel

template<class CalcType>

inline void
PreCalcModels(vbElphyModel<CalcType> **PCEM, vbForceModel<CalcType> **PCFM, CalcType bclmin,
              CalcType *Vm_loc, CalcType *Force_loc, double tinc, CalcType basis = 0.96) {
#if KADEBUG
  cerr<<"PreCalcModels\n";
#endif // if KADEBUG
  int nmrimp = 4;
  vector<CalcType> imptime;
  imptime.resize(nmrimp);
  double calcend = 4.0 * bclmin;
  if (bclmin < 1.0) {
    nmrimp += (int) (log(bclmin) / log(basis) + 1.0);
    imptime.resize(nmrimp);
    double addtime = 0.0;
    for (int i = 0; i < nmrimp - 4; i++) {
      imptime[i] = addtime;
      addtime += pow(basis, (CalcType) i);
    }
    calcend = addtime + 4.0 * bclmin;
  }

  imptime[nmrimp - 4] = calcend - 4.0 * bclmin;
  imptime[nmrimp - 3] = calcend - 3.0 * bclmin;
  imptime[nmrimp - 2] = calcend - 2.0 * bclmin;
  imptime[nmrimp - 1] = calcend - bclmin;

  CalcType istim = (*PCEM)->GetAmplitude(), dVm = 0.0;
  const CalcType defStimTime = (*PCEM)->GetStimTime();
  *Vm_loc = (*PCEM)->GetVm();
  for (double t = 0; t < calcend; t += tinc) {
    CalcType i_external = 0.0;
    for (int j = 0; j < nmrimp; j++) {
      i_external += ((t >= imptime[j] && t <= imptime[j] + defStimTime) ? istim : 0.0);
    }
    dVm = (*PCEM)->Calc(tinc, *Vm_loc, i_external);
    CalcType calcium_loc = (*PCEM)->GetCai() * 1000.0;
    *Force_loc = (*PCFM)->Calc(tinc, 1.0, .0, calcium_loc);
    *Vm_loc += dVm;
  }
#if KADEBUG
  cerr<<"PreCalcModels\n";
#endif // if KADEBUG
} // PreCalcModels

#endif // ifndef CELLMODELLAYER_H
