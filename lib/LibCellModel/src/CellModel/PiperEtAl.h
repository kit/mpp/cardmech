/*      File: PiperEtAl.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#ifndef PIPERETAL
#define PIPERETAL

#include <PiperEtAlParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_PiperEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pPEA->P[NS_PiperEtAlParameters::a].value
#endif // ifdef HETERO

class PiperEtAl : public vbElphyModel<ML_CalcType> {
public:
  PiperEtAlParameters *pPEA;
  ML_CalcType S0000, S0001, S0011, S0111, S1111, S1112, S1122, S1222, S2222;
  ML_CalcType C1, C2, O1, O2, I0, I1, I2;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  PiperEtAl(PiperEtAlParameters *pp);

  virtual inline bool AddHeteroValue(string desc, double val);

  ~PiperEtAl();

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &S0000; }

  virtual inline ML_CalcType Volume() { return 0.; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp);  /*0.0;*/ }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return -.020; }

  virtual inline ML_CalcType GetCai() { return 0.0; }

  virtual inline ML_CalcType GetCao() { return 0.0; }

  virtual inline ML_CalcType GetNai() { return 0.0; }

  virtual inline ML_CalcType GetNao() { return 0.0; }

  virtual inline ML_CalcType GetKi() { return 0.0; }

  virtual inline ML_CalcType GetKo() { return 0.0; }

  virtual inline void SetCai(ML_CalcType val) {}

  virtual void Init();

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                           int euler);
}; // class PiperEtAl
#endif // ifndef PIPERETAL
