/**@file NobleForce.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <NobleForce.h>

NobleForce::NobleForce(NobleForceParameters *pp) {
  nfp = pp;
  Init();
}

void NobleForce::Init() {
  Ca_calmod = CELLMODEL_PARAMVALUE(VT_aCa_calmod);
  TCa = CELLMODEL_PARAMVALUE(VT_aTCa);
  f_LightChain = CELLMODEL_PARAMVALUE(VT_af_LightChain);
  f_CrossBridge = CELLMODEL_PARAMVALUE(VT_af_CrossBridge);
}

ML_CalcType
NobleForce::Calc(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType &Ca,
                 int euler = 1) {
  return ForceEulerNEuler(euler, tinc / euler, stretch, Ca);
}

inline ML_CalcType
NobleForce::ForceEulerNEuler(int r, ML_CalcType tinc, ML_CalcType stretch, ML_CalcType &Ca) {
  double force;

  while (r--) {
    double Ca_change = CELLMODEL_PARAMVALUE(VT_a_troponin) * (CELLMODEL_PARAMVALUE(VT_Troponin) - TCa) * Ca - CELLMODEL_PARAMVALUE(VT_b_troponin) * TCa;
    Ca -= tinc * Ca_change * 1000;

    /*       //cerr<<"a_troponin "<<CELLMODEL_PARAMVALUE(VT_a_troponin)<<" Troponin "<<CELLMODEL_PARAMVALUE(VT_Troponin)<<" .b_troponin
       "<<CELLMODEL_PARAMVALUE(VT_b_troponin)<<endl; */
    double Cal_change = 100 * Ca * (CELLMODEL_PARAMVALUE(VT_Calmod) - Ca_calmod) - 50.0 * Ca_calmod;
    double SarcomereLength = stretch * CELLMODEL_PARAMVALUE(VT_SL);
    double T_act = (SarcomereLength > 1.0 ? 1.0 - exp(-3.0 * (SarcomereLength - 1.0)) : .0);
    double Overlap = (SarcomereLength > 2.0 ? 1.0 - 0.625 * (SarcomereLength - 2.0) : 1.0);

    TCa += tinc * Ca_change;
    Ca_calmod += tinc * Cal_change;
    f_LightChain += tinc * (CELLMODEL_PARAMVALUE(VT_KCalTrop) * Ca_calmod * Ca_calmod * TCa * (1.0 - f_LightChain) -
                            CELLMODEL_PARAMVALUE(VT_k_cont2) * f_LightChain);
    f_CrossBridge += tinc * (CELLMODEL_PARAMVALUE(VT_k_cont3) * f_LightChain * (1.0 - f_CrossBridge) -
                             CELLMODEL_PARAMVALUE(VT_k_cont4) * f_CrossBridge);
    force = f_CrossBridge * CELLMODEL_PARAMVALUE(VT_CBden) * Overlap * T_act +
            .0002 * exp(SarcomereLength + SarcomereLength);
  }
  return force;
}

void NobleForce::Print(ostream &tempstr) {
  tempstr << f_CrossBridge << ' ' << f_LightChain << ' '
          << TCa << ' ' << Ca_calmod << ' ';
}

void NobleForce::GetParameterNames(vector<string> &getpara) {
  const int numpara = 4;
  const string ParaNames[numpara] = {"f_CrossBridge", "f_LightChain", "TCa", "Ca_calmod"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
