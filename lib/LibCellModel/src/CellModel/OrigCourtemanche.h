/**@file OrigCourtemanche.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef COURTEMANCHE_H
#define COURTEMANCHE_H

#include <ElphyModelBasis.h>

namespace court {
/* the constants are global */
  double const vcell = 20100.0; /* um3 */
  double const vi = vcell * 0.68;
  double const vup = 0.0552 * vcell;
  double const vrel = 0.0048 * vcell;

  double const T = 310; /* 37 Celcius */
  double const Tfac = 3;
  double const Csp = 1e+6; /* pF/cm2 */
  double const Cm = 100.0; /* pF */
  double const F = 96.4867; /* coul/mmol */
  double const R = 8.3143; /* J K-1 mol-1 */

  double const kb = 5.4; /* mM */
  double const nab = 140; /* mM */
  double const cab = 1.8; /* mM */
  double const nac = nab;
  double const cac = cab;
  double const kc = kb;
  double const gna = 7.8; /* nS/pF */
  double const gto = 0.1652; /* nS/pF */
  double const gkr = 0.029411765; /* nS/pF */
  double const gks = 0.12941176; /* nS/pF */
  double const gcaL = 0.12375; /* nS/pF */
  double const ErL = 65.0; /* mV */
  double const gk1 = 0.09; /* nS/pF */
  double const gbna = 0.0006744375; /* nS/pF */
  double const gbk = 0.0;
  double const gbca = 0.001131; /* nS/pF */
  double const inakbar = 0.59933874; /* pA/pF */
  double const kmnai = 10.0; /* mM */
  double const kmko = 1.5; /* mM */
  double const icapbar = 0.275; /* pA/pF */
  double const kmcap = 0.0005; /* mM */
  double const knacalr = 1600.0; /* pA/pF */
  double const kmnalr = 87.5; /* mM */
  double const kmcalr = 1.38; /* mM */
  double const ksatlr = 0.1;
  double const gammalr = 0.35;
  double const trpnbar = 0.070; /* mM */
  double const cmdnbar = 0.050; /* mM */
  double const csqnbar = 10; /* mM */
  double const kmcsqn = 0.8; /* mM */
  double const kmcmdn = 0.00238; /* mM */
  double const kmtrpn = 0.0005; /* mM */
  double const grelbar = 30.0; /* ms-1 */
  double const kmup = 0.00092; /* mM */
  double const iupbar = 0.005; /* mM/ms */
  double const caupmax = 15.0; /* mM */
  double const kupleak = iupbar / caupmax; /* ms-1 */
  double const tautr = 180.0; /* ms */
} // namespace court

template<class T>
class CourtemancheParameters : public vbElphyParameters<T> {
public:
  CourtemancheParameters(const char *initFile) { Init(initFile); }

  ~CourtemancheParameters() {}

  virtual inline int GetSize(void) { return 0; }

  virtual inline T *GetBase(void) { return NULL; }

  virtual int GetNumParameters() { return 0; }

  void Init(const char *initFile) {}

  void Calculate() {}

  void InitTable() {}
};

template<class T>
class Courtemanche : public vbElphyModel<T> {
public:
  CourtemancheParameters<T> *pCmP;
  T cai;
  T caup, carel;
  T h, j, oi, ui, xr, xs, f, u, v, w, y;
  T nai, ki;
  T m, oa, ua, d, fca;

  Courtemanche(CourtemancheParameters<T> *pp) {
    pCmP = pp;
    Init();
  }

  ~Courtemanche() {}

  virtual inline T SurfaceToVolumeRatio() { return 0; }

  virtual inline T Volume() { return -0.081; }

  virtual inline T GetAmplitude() { return 13; }

  virtual inline T GetVm() { return -0.081; }

  virtual inline T GetCai() { return Ca_i; }

  virtual inline T GetCao() { return 0; }

  virtual inline T GetNao() { return 0; }

  virtual inline T GetKo() { return 0; }

  virtual inline T GetNai() { return Na_i; }

  virtual inline T GetKi() { return K_i; }

  virtual inline int GetSize(void) {
    return sizeof(Courtemanche<T>) - sizeof(vbElphyModel<T>) - sizeof(CourtemancheParameters<T> *);
  }

  virtual inline T *GetBase(void) { return &Ca_i; }

  virtual inline void SetCai(T val) { Ca_i = val; }

  virtual inline void SetNai(T val) { Na_i = val; }

  virtual inline void SetKi(T val) { K_i = val; }

  virtual inline unsigned char getSpeed(T adVm) {
    return (unsigned char) (adVm < 1e-6 ? 2 : 1);
  }

  virtual void Init() {
#if KADEBUG
    cerr << "Courtemanche::Init" << endl;
#endif // if KADEBUG
    nai = 1.117e+01;
    ki = 1.390e+02;
    m = 2.908e-03;
    oa = 3.043e-02;
    ua = 4.966e-03;
    d = 1.367e-04;
    fca = 7.755e-01;
    cai = 1.013e-04;
    caup = 1.488e+00;
    carel = 1.488e+00;
    h = 9.649e-01;
    j = 9.775e-01;
    oi = 9.992e-01;
    ui = 9.986e-01;
    xr = 3.296e-05;
    xs = 1.869e-02;
    f = 9.996e-01;
    u = 9.992e-01;
    v = 1.000e+00;
    w = 2.350e-112;
  }

  virtual T Calc(double tinc, T V, T i_external = .0, T stretch = 1., int euler = 2) {
    tinc *= 1000.0;
    V *= 1000.0;

    using namespace court;

    /* computed quantities */
    double Ek, Ena, Eca, ina, icaL, ik, ibna, ibk, ibca, inak, ik1;
    double ito, gkur, ikur, ikr, iks;
    double inaca, naidot, kidot;
    double caidot, irel;
    double itr, iup, iupleak;
    double fnak, caflux, icap, sigma;

    /* utility variables */
    double a, b, tau, inf, vshift;

    /* compute Ek */
    Ek = 26.71 * log(kc / ki);

    /* compute Ena */
    Ena = 26.71 * log(nac / nai);

    /* compute Eca */
    Eca = 13.35 * log(cac / cai);

    /* compute sodium current */
    ina = Cm * gna * m * m * m * h * j * (V - Ena);

    /* compute transient outward current */
    ito = Cm * gto * oa * oa * oa * oi * (V - Ek);

    /* compute ultra-rapid potassium current */
    gkur = 0.005 + 0.05 / (1 + exp((V - 15) / -13));
    ikur = Cm * gkur * ua * ua * ua * ui * (V - Ek);

    /* compute the rapid delayed outward rectifier K current */
    ikr = Cm * gkr * xr * (V - Ek) / (1 + exp((V + 15) / 22.4));

    /* compute the slow delayed outward rectifier K current */
    iks = Cm * gks * xs * xs * (V - Ek);

    ik = ikr + iks;

    /* compute calcium current */
    icaL = Cm * gcaL * d * f * fca * (V - ErL);

    /* update the fca gate immediately */
    inf = 1 / (1 + pow(cai / 0.00035, 1.0));
    tau = 2.0;
    fca = inf + (fca - inf) * exp(-dt / tau);

    /* compute time independent potassium current */
    ik1 = Cm * gk1 * (V - Ek) / (1 + exp(0.07 * (V + 80)));

    /* compute ibna background current */
    ibna = Cm * gbna * (V - Ena);

    /* compute potassium background current */
    ibk = Cm * gbk * (V - Ek);

    /* compute ibca background current */
    ibca = Cm * gbca * (V - Eca);

    /* compute inak sodium-potassium pump current, LR-style */
    sigma = (exp(nac / 67.3) - 1) / 7.0;
    fnak = 1 / (1 + 0.1245 * exp(-0.1 * V * F / (R * T)) + 0.0365 * sigma * exp(-V * F / (R * T)));
    inak = Cm * inakbar * fnak * kc / (kc + kmko) / (1 + pow(kmnai / nai, 1.5));

    /* compute icap calcium pump current LR-style */
    icap = Cm * icapbar * cai / (cai + kmcap);

    /* compute inaca exchanger current LR-style */
    inaca = Cm * knacalr / (pow(kmnalr, 3.0) + pow(nac, 3.0)) / (kmcalr + cac) /
            (1 + ksatlr * exp((gammalr - 1) * V * F / (R * T))) *
            (nai * nai * nai * cac * exp(V * gammalr * F / (R * T)) - nac * nac * nac * cai *
                                                                      exp(V * (gammalr - 1) * F /
                                                                          (R * T)));

    /* compute naidot sodium concentration derivative */
    naidot = (-3 * inak - 3 * inaca - ibna - ina) / (F * vi);

    /* compute kidot potassium concentration derivative */
    kidot = (2 * inak - ik1 - ito - ikur - ikr - iks - ibk) / (F * vi);

    /* calcium buffer dynamics */
    cmdn = cmdnbar * cai / (cai + kmcmdn);
    trpn = trpnbar * cai / (cai + kmtrpn);
    csqn = csqnbar * carel / (carel + kmcsqn);

    /* SR calcium handling */
    irel = grelbar * u * u * v * w * (carel - cai);
    iup = iupbar / (1 + kmup / cai);
    iupleak = kupleak * caup;
    itr = (caup - carel) / tautr;

    /* compute caidot calcium concentration derivative */
    /* using steady-state buffer approximation */
    caidot = ((2 * inaca - icap - icaL - ibca) / (2 * F * vi) +
              (iupleak - iup) * vup / vi + irel * vrel / vi) /
             (1 + trpnbar * kmtrpn / (cai * cai + 2 * cai * kmtrpn + kmtrpn * kmtrpn) +
              cmdnbar * kmcmdn / (cai * cai + 2 * cai * kmcmdn + kmcmdn * kmcmdn));

    /* update caup calcium in uptake compartment */
    caup = caup + dt * (iup - itr * vrel / vup - iupleak);

    /* update carel calcium in release compartment */
    carel = carel + dt * ((itr - irel) / (1 + csqnbar * kmcsqn /
                                              (carel * carel + 2 * carel * kmcsqn +
                                               kmcsqn * kmcsqn)));

    /* update all concentrations */
    nai = nai + dt * naidot;
    ki = ki + dt * kidot;
    cai = cai + dt * caidot;

    /* update ina m gate */
    a = 0.32 * (V + 47.13) / (1 - exp(-0.1 * (V + 47.13)));
    if (fabs(V + 47.13) < 1e-10)
      a = 3.2;  /* denominator = 0 */
    b = 0.08 * exp(-V / 11);
    tau = 1 / (a + b);
    inf = a * tau;
    m = inf + (m - inf) * exp(-dt / tau);

    /* update ina h gate */
    if (V >= -40.0) {
      a = 0.0;
      b = 1 / (0.13 * (1 + exp((V + 10.66) / -11.1)));
    } else {
      a = 0.135 * exp((V + 80) / -6.8);
      b = 3.56 * exp(0.079 * V) + 3.1e5 * exp(0.35 * V);
    }
    tau = 1 / (a + b);
    inf = a * tau;
    h = inf + (h - inf) * exp(-dt / tau);

    /* update ina j gate */
    if (V >= -40.0) {
      a = 0.0;
      b = 0.3 * exp(-2.535e-7 * V) / (1 + exp(-0.1 * (V + 32)));
    } else {
      a = (-1.2714e5 * exp(0.2444 * V) - 3.474e-5 * exp(-0.04391 * V)) * (V + 37.78) /
          (1 + exp(0.311 * (V + 79.23)));
      b = 0.1212 * exp(-0.01052 * V) / (1 + exp(-0.1378 * (V + 40.14)));
    }
    tau = 1 / (a + b);
    inf = a * tau;
    j = inf + (j - inf) * exp(-dt / tau);

    /* update oa ito gate */
    /* corrected for 37 deg */
    /* define an voltage shift due to junctional potential
              and effect of Cd++ */
    vshift = -10;
    a = 0.65 / (exp((V - vshift + 0.0) / -8.5) + exp((V - vshift - 40.0) / -59.0));
    b = 0.65 / (2.5 + exp((V - vshift + 72.0) / 17.0));
    tau = 1 / (a + b);
    inf = 1 / (1 + exp((V - vshift + 10.47) / -17.54));
    oa = inf + (oa - inf) * exp(-Tfac * dt / tau);

    /* update oi and ois ito gate */
    /* corrected for 37 deg */
    /* define an voltage shift due to junctional potential
       and effect of Cd++ */
    vshift = -10;
    a = 1 / (18.53 + exp((V - vshift + 103.7) / 10.95));
    b = 1 / (35.56 + exp((V - vshift - 8.74) / -7.44));
    tau = 1 / (a + b);
    inf = 1 / (1 + exp((V - vshift + 33.1) / 5.3));
    oi = inf + (oi - inf) * exp(-Tfac * dt / tau);

    /* update ua ikur gate */
    /* corrected for 37 deg */
    /* define an voltage shift due to junctional potential
              and effect of Cd++ */
    vshift = -10;
    a = 0.65 / (exp((V - vshift + 0.0) / -8.5) + exp((V - vshift - 40.0) / -59.0));
    b = 0.65 / (2.5 + exp((V - vshift + 72.0) / 17.0));
    tau = 1 / (a + b);
    inf = 1 / (1 + exp((V - vshift + 20.3) / -9.6));
    ua = inf + (ua - inf) * exp(-Tfac * dt / tau);

    /* update ui ikur gate */
    /* corrected for 37 deg */
    /* define an voltage shift due to junctional potential
              and effect of Cd++ */
    vshift = -10;
    a = 1 / (exp((V - vshift - 195) / -28) + 21);
    b = 1 / (exp((V - vshift - 168) / -16));
    tau = 1 / (a + b);
    inf = 1 / (1 + exp((V - vshift - 109.45) / 27.48));
    ui = inf + (ui - inf) * exp(-Tfac * dt / tau);

    /* update the xr ikr gate */
    vshift = 0;
    a = 0.0003 * (V - vshift + 14.1) / (1 - exp((V - vshift + 14.1) / -5));
    b = 0.000073898 * (V - vshift - 3.3328) / (exp((V - vshift - 3.3328) / 5.1237) - 1);
    if (fabs(V - vshift + 14.1) < 1e-10)
      a = 0.0015;        /* denominator = 0 */
    if (fabs(V - vshift - 3.3328) < 1e-10)
      b = 3.7836118e-4;  /* denominator = 0 */
    tau = 1 / (a + b);
    inf = 1 / (1 + exp((V - vshift + 14.1) / -6.5));
    xr = inf + (xr - inf) * exp(-dt / tau);

    /* update the xs ikr gate */
    vshift = 0;
    a = 0.00004 * (V - vshift - 19.9) / (1 - exp((V - vshift - 19.9) / -17));
    b = 0.000035 * (V - vshift - 19.9) / (exp((V - vshift - 19.9) / 9) - 1);
    if (fabs(V - vshift - 19.9) < 1e-10) { /* denominator = 0 */
      a = 0.00068;
      b = 0.000315;
    }

    /* tau reduced by 50% as described in manuscript */
    tau = 0.5 / (a + b);
    inf = sqrt(1 / (1 + exp((V - vshift - 19.9) / -12.7)));
    xs = inf + (xs - inf) * exp(-dt / tau);

    /* update icaL d gate */
    vshift = 0;
    a = 1 / (1 + exp((V - vshift + 10) / -6.24));
    tau = a * (1 - exp((V - vshift + 10) / -6.24)) / (0.035 * (V - vshift + 10));
    if (fabs(V - vshift + 10) < 1e-10)
      tau = a * 4.579;  /* denominator = 0 */
    inf = 1 / (1 + exp((V - vshift + 10) / -8));
    d = inf + (d - inf) * exp(-dt / tau);

    /* update icaL f gate */
    vshift = 0;
    inf = exp(-(V - vshift + 28) / 6.9) / (1 + exp(-(V - vshift + 28) / 6.9));
    tau = 1.5 * 2 * 3 / (0.0197 * exp(-0.0337 * 0.0337 * (V - vshift + 10) *
                                      (V - vshift + 10)) + 0.02);
    f = inf + (f - inf) * exp(-dt / tau);

    /* update the SR gating variables */
    /* caflux is expected in umoles/ms, hence the factor of 1000 */
    /* 1e-15 is used to scale the volumes! */
    vshift = 0;
    caflux = 1e3 * (1e-15 * vrel * irel - 1e-15 * (0.5 * icaL - 0.1 * 2 * inaca) / (2 * F));
    inf = 1 / (1 + exp(-(caflux - 3.4175e-13 - vshift) / 13.67e-16));
    tau = 8.0;
    u = inf + (u - inf) * exp(-dt / tau);
    inf = 1 - 1 / (1 + exp(-(caflux - 6.835e-14 - vshift) / 13.67e-16));
    tau = 1.91 + 2.09 / (1 + exp(-(caflux - 3.4175e-13 - vshift) / 13.67e-16));
    v = inf + (v - inf) * exp(-dt / tau);
    inf = 1 - 1 / (1 + exp(-(V - 40) / 17.0));
    tau = 6.0 * (1 - exp(-(V - 7.9) / 5.0)) / (1 + 0.3 * exp(-(V - 7.9) / 5.0)) / (V - 7.9);
    if (fabs(V - 7.9) < 1e-10)
      tau = 6 * 0.2 / 1.3;
    w = inf + (w - inf) * exp(-dt / tau);


    /* update membrane voltage */
    double Itot =
        ina + icaL + icap + ik1 + ito + ikur + ikr + iks + ibna + ibk + ibca + inak + inaca;

    return -1.0 / Cm * Itot * tinc;
  } // Calc

  virtual void Print(ostream &tempstr, double t, T V) {
    // virtual void Print(T t, T V){
    tempstr << t << ' ' << V << ' '
            << cai << ' ' << caup << ' ' << carel << ' '
            << h << ' ' << j << ' '
            << oi << ' ' << ui << ' '
            << xr << ' ' << xs << ' '
            << f << ' ' << u << ' ' << v << ' '
            << w << ' ' << nai << ' ' << ki << ' '
            << m << ' ' << oa << ' ' << ua << ' '
            << d << ' ' << fca << ' ';
  }

  virtual void LongPrint(ostream &tempstr, double t, T V) {
    Print(tempstr, t, V);
    V *= 1000.0;
  }

  virtual void GetParameterNames(vector<string> &getpara) {
    const int numpara = 23;
    const string ParaNames[numpara] =
        {"Ca_i", "Ca_up", "Ca_rel", "h", "j", "o_i", "u_i", "Xr",
         "Xs",
         "f", "u",
         "v", "w", "Na_i", "K_i", "m", "o_a", "u_a", "d",
         "f_Ca", "y",
         "dT", "fT"};

    for (int i = 0; i < numpara; i++)
      getpara.push_back(ParaNames[i]);
  }

  virtual void GetLongParameterNames(vector<string> &getpara) {
    GetParameterNames(getpara);
    const int numpara = 21;
    const string ParaNames[numpara] =
        {"I_Na", "I_bNa", "I_K1", "I_to", "I_Kur", "I_Kr", "I_Ks",
         "I_CaL",
         "I_NaK", "I_NaCa", "I_bCa",
         "I_pCa", "I_rel", "I_tr", "I_up", "I_upleak", "I_fNa", "I_fK",
         "I_CaT",
         "I_bK", "I_sus"};
    for (int i = 0; i < numpara; i++)
      getpara.push_back(ParaNames[i]);
  }
}; // class Courtemanche
#endif // ifndef COURTEMANCHE_H
