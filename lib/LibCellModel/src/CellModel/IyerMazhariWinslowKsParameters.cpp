/**@file IyerMazhariWinslowKsParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowKsParameters.h>

IyerMazhariWinslowKsParameters::IyerMazhariWinslowKsParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void IyerMazhariWinslowKsParameters::PrintParameters() {
  cout << "#IyerMazhariWinslowKsParameter:\n";

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void IyerMazhariWinslowKsParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "IyerMazhariWinslowKrParameters:init " << initFile << endl;
#endif // if KADEBUG

  P[VT_Tx].name = "Tx";
  P[VT_Acap].name = "Acap";
  P[VT_Vmyo].name = "Vmyo";

  P[VT_Ko].name = "Ko";
  P[VT_Ki].name = "Ki";

  P[VT_C0Ks].name = "C0Ks";
  P[VT_C1Ks].name = "C1Ks";
  P[VT_O1Ks].name = "O1Ks";
  P[VT_O2Ks].name = "O2Ks";

  P[VT_GKs].name = "GKs";

  P[VT_Ksa].name = "Ksa";
  P[VT_Ksb].name = "Ksb";
  P[VT_Ksg].name = "Ksg";
  P[VT_Ksd].name = "Ksd";
  P[VT_Kse].name = "Kse";
  P[VT_Kso].name = "Kso";
  P[VT_Amp].name = "Amp";

  ParameterLoader EPL(initFile, EMT_IyerMazhariWinslowKs);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
} // IyerMazhariWinslowKsParameters::Init

void IyerMazhariWinslowKsParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
}
