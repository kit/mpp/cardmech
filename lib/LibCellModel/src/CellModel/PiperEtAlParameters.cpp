/*      File: PiperEtAlParameters.cpp
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
        Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
        send comments to dw@ibt.uka.de  */


#include <PiperEtAlParameters.h>

PiperEtAlParameters::PiperEtAlParameters(const char *initFile) {
  // Konstruktor
  P = new Parameter[vtLast];
  Init(initFile);
}

PiperEtAlParameters::~PiperEtAlParameters() {
  // Destruktor
}

void PiperEtAlParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "PiperEtAlParameters:" << endl;
  for (int i = vtFirst; i < vtLast; i++)
    cout << P[i].name << "\t = " << P[i].value << endl;
}

void PiperEtAlParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "PiperEtAlParameters::init " << initFile << endl;
#endif // if KADEBUG
#if KADEBUG
  cerr << GetSize() << endl;
#endif // if KADEBUG


  // Initialization of the Parameters ...
  P[VT_C_m].name = "C_m";
  P[VT_g_HERG].name = "g_HERG";
  P[VT_E_HERG].name = "E_HERG";
  P[VT_S0000].name = "S0000";
  P[VT_S0001].name = "S0001";
  P[VT_S0011].name = "S0011";
  P[VT_S0111].name = "S0111";
  P[VT_S1111].name = "S1111";
  P[VT_S1112].name = "S1112";
  P[VT_S1122].name = "S1122";
  P[VT_S1222].name = "S1222";
  P[VT_S2222].name = "S2222";
  P[VT_C1].name = "C1";
  P[VT_C2].name = "C2";
  P[VT_O1].name = "O1";
  P[VT_O2].name = "O2";
  P[VT_I0].name = "I0";
  P[VT_I1].name = "I1";
  P[VT_I2].name = "I2";
  P[VT_S0S1A0].name = "S0S1A0";
  P[VT_S0S1B0].name = "S0S1B0";
  P[VT_S0S1ZA].name = "S0S1ZA";
  P[VT_S0S1ZB].name = "S0S1ZB";
  P[VT_S0S1C].name = "S0S1C";
  P[VT_S1S2A0].name = "S1S2A0";
  P[VT_S1S2B0].name = "S1S2B0";
  P[VT_S1S2ZA].name = "S1S2ZA";
  P[VT_S1S2ZB].name = "S1S2ZB";
  P[VT_S1S2C].name = "S1S2C";
  P[VT_S2C1A0].name = "S2C1A0";
  P[VT_S2C1B0].name = "S2C1B0";
  P[VT_S2C1ZA].name = "S2C1ZA";
  P[VT_S2C1ZB].name = "S2C1ZB";
  P[VT_C1C2A0].name = "C1C2A0";
  P[VT_C1C2B0].name = "C1C2B0";
  P[VT_C1C2ZA].name = "C1C2ZA";
  P[VT_C1C2ZB].name = "C1C2ZB";
  P[VT_C2O1A0].name = "C2O1A0";
  P[VT_C2O1B0].name = "C2O1B0";
  P[VT_C2O1ZA].name = "C2O1ZA";
  P[VT_C2O1ZB].name = "C2O1ZB";
  P[VT_O1O2A0].name = "O1O2A0";
  P[VT_O1O2B0].name = "O1O2B0";
  P[VT_O1O2ZA].name = "O1O2ZA";
  P[VT_O1O2ZB].name = "O1O2ZB";
  P[VT_C2I0A0].name = "C2I0A0";
  P[VT_C2I0B0].name = "C2I0B0";
  P[VT_C2I0ZA].name = "C2I0ZA";
  P[VT_C2I0ZB].name = "C2I0ZB";
  P[VT_O1I1A0].name = "O1I1A0";
  P[VT_O1I1B0].name = "O1I1B0";
  P[VT_O1I1ZA].name = "O1I1ZA";
  P[VT_O1I1ZB].name = "O1I1ZB";
  P[VT_O2I2A0].name = "O2I2A0";
  P[VT_O2I2B0].name = "O2I2B0";
  P[VT_O2I2ZA].name = "O2I2ZA";
  P[VT_O2I2ZB].name = "O2I2ZB";
  P[VT_I0I1A0].name = "I0I1A0";
  P[VT_I0I1B0].name = "I0I1B0";
  P[VT_I0I1ZA].name = "I0I1ZA";
  P[VT_I0I1ZB].name = "I0I1ZB";
  P[VT_I1I2A0].name = "I1I2A0";
  P[VT_I1I2B0].name = "I1I2B0";
  P[VT_I1I2ZA].name = "I1I2ZA";
  P[VT_I1I2ZB].name = "I1I2ZB";
  P[VT_Tx].name = "T";
  P[VT_FdRT].name = "FdRT";
  P[VT_dC_m].name = "dC_m";
  P[VT_Amp].name = "Amp";

  P[VT_FdRT].readFromFile = false;
  P[VT_dC_m].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_PiperEtAl);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  // End Initialization of the Parameters ...

  Calculate();
  InitTable();
} // PiperEtAlParameters::Init

void PiperEtAlParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "PiperEtAlParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_FdRT].value = 9.64867e4 / (8.3143 * P[VT_Tx].value);
  P[VT_dC_m].value = 1. / P[VT_C_m].value;
}

void PiperEtAlParameters::InitTable() {
#if KADEBUG
  cerr << "PiperEtAlParameters - InitTable()" << endl;
#endif // if KADEBUG
}
