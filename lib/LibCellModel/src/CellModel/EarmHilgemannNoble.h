/*      File: EarmHilgemannNoble.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#ifndef EARMHILGEMANNNOBLE
#define EARMHILGEMANNNOBLE

#include <EarmHilgemannNobleParameters.h>

#define HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_EarmHilgemannNobleParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pEHNP->P[NS_EarmHilgemannNobleParameters::a].value
#endif // ifdef HETERO

class EarmHilgemannNoble : public vbElphyModel<ML_CalcType> {
public:
  EarmHilgemannNobleParameters *pEHNP;
  ML_CalcType Ca_i;
  ML_CalcType Ca_o;
  ML_CalcType Na_i, K_i;
  ML_CalcType r, f_activator, f_product;
  ML_CalcType Ca_calmod, Ca_troponin, Ca_up, Ca_rel;
  ML_CalcType m, h, d, f, q;
#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  EarmHilgemannNoble(EarmHilgemannNobleParameters *pp);

  ~EarmHilgemannNoble();

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual void Init();

  virtual inline ML_CalcType Volume() { return 1.4 * 1e-13; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp);  /*4;*/ }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_Vm); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return Ca_o; }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void) { return &Ca_i; }

  virtual inline void SetCao(ML_CalcType val) { Ca_o = val; }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual ML_CalcType Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                           int euler);

  virtual void Print(ostream &tempstr, double t, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double t, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class EarmHilgemannNoble
#endif // ifndef EARMHILGEMANNNOBLE
