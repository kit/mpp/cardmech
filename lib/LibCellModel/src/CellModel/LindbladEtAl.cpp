/* File: LindbladEtAl.cpp
        automatically created by CellML2Elphymodel.pl
        Institute of Biomedical Engineering, Universität Karlsruhe (TH) */

#include <LindbladEtAl.h>

LindbladEtAl::LindbladEtAl(LindbladEtAlParameters *pp) {
  ptTeaP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(ptTeaP, NS_LindbladEtAlParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

LindbladEtAl::~LindbladEtAl() {}

#ifdef HETERO

inline bool LindbladEtAl::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool LindbladEtAl::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int LindbladEtAl::GetSize(void) {
  return sizeof(LindbladEtAl) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(LindbladEtAlParameters *)
         #ifdef HETERO
         - sizeof(ParameterSwitch *)
#endif // ifdef HETERO
      ;
}

inline unsigned char LindbladEtAl::getSpeed(ML_CalcType adVm) {
  return (unsigned char) 5;
}

void LindbladEtAl::Init() {
#if KADEBUG
  cerr << "#initializing Class: LindbladEtAl ... " << endl;
#endif // if KADEBUG
  Na_i = (CELLMODEL_PARAMVALUE(VT_Na_i_init));
  m = (CELLMODEL_PARAMVALUE(VT_m_init));
  h1 = (CELLMODEL_PARAMVALUE(VT_h1_init));
  h2 = (CELLMODEL_PARAMVALUE(VT_h2_init));
  d_L = (CELLMODEL_PARAMVALUE(VT_d_L_init));
  f_L = (CELLMODEL_PARAMVALUE(VT_f_L_init));
  d_T = (CELLMODEL_PARAMVALUE(VT_d_T_init));
  f_T = (CELLMODEL_PARAMVALUE(VT_f_T_init));
  K_i = (CELLMODEL_PARAMVALUE(VT_K_i_init));
  r = (CELLMODEL_PARAMVALUE(VT_r_init));
  s1 = (CELLMODEL_PARAMVALUE(VT_s1_init));
  s2 = (CELLMODEL_PARAMVALUE(VT_s2_init));
  s3 = (CELLMODEL_PARAMVALUE(VT_s3_init));
  z = (CELLMODEL_PARAMVALUE(VT_z_init));
  p_a = (CELLMODEL_PARAMVALUE(VT_p_a_init));
  p_i = (CELLMODEL_PARAMVALUE(VT_p_i_init));
  Ca_i = (CELLMODEL_PARAMVALUE(VT_Ca_i_init));
  O_C = (CELLMODEL_PARAMVALUE(VT_O_C_init));
  O_TC = (CELLMODEL_PARAMVALUE(VT_O_TC_init));
  O_TMgC = (CELLMODEL_PARAMVALUE(VT_O_TMgC_init));
  O_TMgMg = (CELLMODEL_PARAMVALUE(VT_O_TMgMg_init));
  Ca_rel = (CELLMODEL_PARAMVALUE(VT_Ca_rel_init));
  Ca_up = (CELLMODEL_PARAMVALUE(VT_Ca_up_init));
  O_Calse = (CELLMODEL_PARAMVALUE(VT_O_Calse_init));
  F1 = (CELLMODEL_PARAMVALUE(VT_F1_init));
  F2 = (CELLMODEL_PARAMVALUE(VT_F2_init));
} // LindbladEtAl::Init

ML_CalcType
LindbladEtAl::Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch,
                   int euler) {
  // InitTableWithtinc
  const ML_CalcType svolt = V * 1000;
  const ML_CalcType HT = tinc;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + svolt) + .5);

  // calculating algebraic part
  const ML_CalcType RTdF = (CELLMODEL_PARAMVALUE(VT_RTdF));
  const ML_CalcType FdRT = 1.0 / RTdF;
  const ML_CalcType K_c = (CELLMODEL_PARAMVALUE(VT_K_c));
  const ML_CalcType Na_c = (CELLMODEL_PARAMVALUE(VT_Na_c));
  const ML_CalcType Ca_c = (CELLMODEL_PARAMVALUE(VT_Ca_c));
  const ML_CalcType gamma = (CELLMODEL_PARAMVALUE(VT_gamma));
  const ML_CalcType FC = (CELLMODEL_PARAMVALUE(VT_F));
  const ML_CalcType k_cyca = (CELLMODEL_PARAMVALUE(VT_k_cyca));
  const ML_CalcType k_xcs = (CELLMODEL_PARAMVALUE(VT_k_xcs));
  const ML_CalcType k_srca = (CELLMODEL_PARAMVALUE(VT_k_srca));
  const ML_CalcType Vol_rel2FC = 2.00000 * (CELLMODEL_PARAMVALUE(VT_Vol_rel)) * FC;
  const ML_CalcType dVol_iFC = 1.0 / ((CELLMODEL_PARAMVALUE(VT_Vol_i)) * FC);
  const ML_CalcType F3 = 0.903 - F1 - F2;

  const ML_CalcType VmE_K = svolt - RTdF * log(K_c / K_i);
  const ML_CalcType VmE_Na = svolt - RTdF * log(Na_c / Na_i);
  const ML_CalcType VmE_Ca = svolt - RTdF * 0.5 * log(Ca_c / Ca_i);

  const ML_CalcType i_to = (CELLMODEL_PARAMVALUE(VT_g_to)) * r * (0.590000 * s1 * s1 * s1 + 0.410000 * s2 * s2 * s2) *
                           (0.600000 * s3 * s3 * s3 * s3 * s3 * s3 + 0.400000) * VmE_K;
  const ML_CalcType i_K1 = (CELLMODEL_PARAMVALUE(VT_g_K1)) * VmE_K * (CELLMODEL_PARAMVALUE(VT_powKc)) /
                           (1.00000 + exp((CELLMODEL_PARAMVALUE(VT_steepK1)) * FdRT * (VmE_K - (CELLMODEL_PARAMVALUE(VT_shiftK1)))));
  const ML_CalcType i_Kr = (CELLMODEL_PARAMVALUE(VT_g_Kr)) * p_a * p_i * VmE_K;
  const ML_CalcType i_Ks = (CELLMODEL_PARAMVALUE(VT_g_Ks)) * z * VmE_K;
  ML_CalcType powNai = pow(Na_i, 1.50000);
  const ML_CalcType i_p =
      (CELLMODEL_PARAMVALUE(VT_i_NaK_max)) * ptTeaP->i_p_Const[Vi] * powNai / (powNai + (CELLMODEL_PARAMVALUE(VT_powk_NaK_Na)));
  const ML_CalcType i_Na =
      (CELLMODEL_PARAMVALUE(VT_P_Na)) * m * m * m * (0.635000 * h1 + 0.365000 * h2) * ptTeaP->i_Na_Const[Vi] *
      (exp(VmE_Na * FdRT) - 1.00000);
  const ML_CalcType i_Ca_L =
      (CELLMODEL_PARAMVALUE(VT_g_Ca_L)) * d_L * f_L * ptTeaP->d_prime[Vi] * (svolt - (CELLMODEL_PARAMVALUE(VT_E_Ca_app)));
  const ML_CalcType i_Ca_T = (CELLMODEL_PARAMVALUE(VT_g_Ca_T)) * d_T * f_T * (svolt - (CELLMODEL_PARAMVALUE(VT_E_Ca_T)));
  const ML_CalcType i_B_Na = (CELLMODEL_PARAMVALUE(VT_g_B_Na)) * VmE_Na;
  const ML_CalcType i_B_Ca = (CELLMODEL_PARAMVALUE(VT_g_B_Ca)) * VmE_Ca;
  const ML_CalcType i_CaP = (CELLMODEL_PARAMVALUE(VT_i_CaP_max)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_CaP)));

  powNai = Na_i * Na_i * Na_i * Ca_c;
  const ML_CalcType powNac = (CELLMODEL_PARAMVALUE(VT_powNac)) * Ca_i;
  const ML_CalcType i_NaCa = (CELLMODEL_PARAMVALUE(VT_k_NaCa)) *
                             (powNai * exp(gamma * svolt * FdRT) -
                              powNac * exp((gamma - 1.00000) * svolt * FdRT)) /
                             (1.00000 +
                              (CELLMODEL_PARAMVALUE(VT_d_NaCa)) * (powNac + powNai) * (1.00000 + Ca_i / 0.00690000));
  const ML_CalcType dOCdt = 200000. * Ca_i * (1.00000 - O_C) - 476.000 * O_C;
  const ML_CalcType dOTCdt = 78400.0 * Ca_i * (1.00000 - O_TC) - 392.000 * O_TC;
  const ML_CalcType dOTMgCdt = 200000. * Ca_i * (1.00000 - O_TMgC - O_TMgMg) - 6.60000 * O_TMgC;
  const ML_CalcType dOCalsedt = 480.000 * Ca_rel * (1.00000 - O_Calse) - 400.000 * O_Calse;
  const ML_CalcType i_up = (CELLMODEL_PARAMVALUE(VT_I_up_max)) * (Ca_i / k_cyca - k_xcs * k_xcs * Ca_up / k_srca) /
                           ((Ca_i + k_cyca) / k_cyca + k_xcs * (Ca_up + k_srca) / k_srca);
  const ML_CalcType i_tr = (Ca_up - Ca_rel) * Vol_rel2FC / (CELLMODEL_PARAMVALUE(VT_tau_tr));
  ML_CalcType powF2 = F2 / (F2 + 0.250000);
  powF2 *= powF2;
  const ML_CalcType i_rel = (CELLMODEL_PARAMVALUE(VT_alpha_rel)) * powF2 * (Ca_rel - Ca_i);
  ML_CalcType powCai = Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_rel)));
  powCai *= powCai;
  powCai *= powCai;
  const ML_CalcType r_act = 203.800 * exp(0.0800000 * (svolt - 40.0000)) + 203.800 * powCai;
  const ML_CalcType r_inact = 33.9600 + 339.600 * powCai;


  // calculating rates part
  O_TMgMg += HT * (2000.00 * (CELLMODEL_PARAMVALUE(VT_Mg_i)) * (1.00000 - O_TMgC - O_TMgMg) - 666.000 * O_TMgMg);
  F1 += HT * ((CELLMODEL_PARAMVALUE(VT_k_F3)) * F3 - r_act * F1);
  F2 += HT * (r_act * F1 - r_inact * F2);

  s1 = ptTeaP->s1_infinity[Vi] - (ptTeaP->s1_infinity[Vi] - s1) * ptTeaP->exptau_s1[Vi];
  s2 = ptTeaP->s2_infinity[Vi] - (ptTeaP->s2_infinity[Vi] - s2) * ptTeaP->exptau_s2[Vi];
  s3 = ptTeaP->s3_infinity[Vi] - (ptTeaP->s3_infinity[Vi] - s3) * ptTeaP->exptau_s3[Vi];
  m = ptTeaP->m_infinity[Vi] - (ptTeaP->m_infinity[Vi] - m) * ptTeaP->exptau_m[Vi];
  h1 = ptTeaP->h_infinity[Vi] - (ptTeaP->h_infinity[Vi] - h1) * ptTeaP->exptau_h1[Vi];
  h2 = ptTeaP->h_infinity[Vi] - (ptTeaP->h_infinity[Vi] - h2) * ptTeaP->exptau_h2[Vi];
  r = ptTeaP->r_infinity[Vi] - (ptTeaP->r_infinity[Vi] - r) * ptTeaP->exptau_r[Vi];
  z = ptTeaP->z_infinity[Vi] - (ptTeaP->z_infinity[Vi] - z) * ptTeaP->exptau_z[Vi];
  p_a = ptTeaP->p_a_infinity[Vi] - (ptTeaP->p_a_infinity[Vi] - p_a) * ptTeaP->exptau_p_a[Vi];
  p_i = ptTeaP->p_i_infinity[Vi] - (ptTeaP->p_i_infinity[Vi] - p_i) * ptTeaP->exptau_p_i[Vi];
  d_T = ptTeaP->d_T_infinity[Vi] - (ptTeaP->d_T_infinity[Vi] - d_T) * ptTeaP->exptau_d_T[Vi];
  f_T = ptTeaP->f_T_infinity[Vi] - (ptTeaP->f_T_infinity[Vi] - f_T) * ptTeaP->exptau_f_T[Vi];
  d_L = ptTeaP->d_L_infinity[Vi] - (ptTeaP->d_L_infinity[Vi] - d_L) * ptTeaP->exptau_d_L[Vi];
  f_L = ptTeaP->f_L_infinity[Vi] - (ptTeaP->f_L_infinity[Vi] - f_L) * ptTeaP->exptau_f_L[Vi];

  K_i += HT * (-(i_to + i_K1 + i_Kr + i_Ks - 2.00000 * i_p) * dVol_iFC);
  Na_i += HT * (-(i_Na + i_B_Na + 3.00000 * i_p + 3.00000 * i_NaCa) * dVol_iFC);
  O_C += HT * (dOCdt);
  O_TC += HT * (dOTCdt);
  O_Calse += HT * (dOCalsedt);
  O_TMgC += HT * (dOTMgCdt);
  Ca_up += HT * ((i_up - i_tr) / (2.00000 * (CELLMODEL_PARAMVALUE(VT_Vol_up)) * FC));
  Ca_i += HT *
          (-(i_Ca_L + i_Ca_T + i_B_Ca + i_CaP - 2.00000 * i_NaCa + i_up - i_rel) /
           (2.00000 * (CELLMODEL_PARAMVALUE(VT_Vol_Ca)) * FC) -
           (0.0800000 * dOTCdt + 0.160000 * dOTMgCdt + 0.0450000 * dOCdt));
  Ca_rel += HT * ((i_tr - i_rel) / (Vol_rel2FC) - 31.0000 * dOCalsedt);
  const ML_CalcType I_tot =
      (1.0 / (CELLMODEL_PARAMVALUE(VT_Cm)) *
       (i_Kr + i_Ks + i_Na + i_Ca_L + i_Ca_T + i_to + i_K1 + i_B_Na + i_B_Ca + ptTeaP->i_B_Cl[Vi] +
        i_p + i_CaP + i_NaCa - i_external));

  return tinc * (-I_tot) * 0.001;
} // LindbladEtAl::Calc

void LindbladEtAl::Print(ostream &tempstr, double tArg, ML_CalcType V) {
  const ML_CalcType F3 = 0.903 - F1 - F2;

  tempstr << tArg << ' ' << V << ' ' << Na_i << ' ' << m << ' ' << h1 << ' ' << h2 << ' ' << d_L
          << ' ' << f_L << ' ' << d_T << ' ' << f_T << ' ' <<
          K_i
          << ' ' << r << ' ' << s1 << ' ' << s2 << ' ' << s3 << ' ' << z << ' ' << p_a << ' ' << p_i
          << ' ' << Ca_i << ' ' << O_C
          << ' ' << O_TC << ' ' << O_TMgC << ' ' << O_TMgMg << ' ' << Ca_rel << ' ' << Ca_up << ' '
          << O_Calse << ' ' << F1 << ' ' << F2 << ' ' <<
          F3;
}

void LindbladEtAl::LongPrint(ostream &tempstr, double tArg, ML_CalcType V) {
  Print(tempstr, tArg, V);
  const ML_CalcType svolt = V * 1000.0;
  const int Vi = (int) (DivisionTab * (RangeTabhalf + svolt) + .5);

  const ML_CalcType RTdF = (CELLMODEL_PARAMVALUE(VT_RTdF));
  const ML_CalcType FdRT = 1.0 / RTdF;
  const ML_CalcType K_c = (CELLMODEL_PARAMVALUE(VT_K_c));
  const ML_CalcType Na_c = (CELLMODEL_PARAMVALUE(VT_Na_c));
  const ML_CalcType Ca_c = (CELLMODEL_PARAMVALUE(VT_Ca_c));
  const ML_CalcType VmE_K = svolt - RTdF * log(K_c / K_i);
  const ML_CalcType VmE_Na = svolt - RTdF * log(Na_c / Na_i);
  const ML_CalcType VmE_Ca = svolt - RTdF * 0.5 * log(Ca_c / Ca_i);

  const ML_CalcType i_to = (CELLMODEL_PARAMVALUE(VT_g_to)) * r * (0.590000 * s1 * s1 * s1 + 0.410000 * s2 * s2 * s2) *
                           (0.600000 * s3 * s3 * s3 * s3 * s3 * s3 + 0.400000) * VmE_K;
  const ML_CalcType i_K1 = (CELLMODEL_PARAMVALUE(VT_g_K1)) * VmE_K * (CELLMODEL_PARAMVALUE(VT_powKc)) /
                           (1.00000 + exp((CELLMODEL_PARAMVALUE(VT_steepK1)) * FdRT * (VmE_K - (CELLMODEL_PARAMVALUE(VT_shiftK1)))));
  const ML_CalcType i_Kr = (CELLMODEL_PARAMVALUE(VT_g_Kr)) * p_a * p_i * VmE_K;
  const ML_CalcType i_Ks = (CELLMODEL_PARAMVALUE(VT_g_Ks)) * z * VmE_K;
  const ML_CalcType i_p = (CELLMODEL_PARAMVALUE(VT_i_NaK_max)) * (K_c) / ((K_c) + (CELLMODEL_PARAMVALUE(VT_k_NaK_K))) *
                          pow(Na_i,
                              1.50000) / (pow(Na_i, 1.50000) + pow((CELLMODEL_PARAMVALUE(VT_k_NaK_Na)), 1.50000)) *
                          1.60000 / (1.50000 + exp((svolt + 60.0000) / -40.0000));
  const ML_CalcType i_Na =
      (CELLMODEL_PARAMVALUE(VT_P_Na)) * pow(m, 3.00000) * (0.635000 * h1 + 0.365000 * h2) * (Na_c) * svolt * pow((CELLMODEL_PARAMVALUE(
                                                                                                  VT_F)),
                                                                                              2.00000) /
      ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T))) * (exp(VmE_Na * FdRT) - 1.00000) / (exp(svolt * FdRT) - 1.00000);
  const ML_CalcType i_Ca_L =
      (CELLMODEL_PARAMVALUE(VT_g_Ca_L)) * d_L * f_L * ptTeaP->d_prime[Vi] * (svolt - (CELLMODEL_PARAMVALUE(VT_E_Ca_app)));
  const ML_CalcType i_Ca_T = (CELLMODEL_PARAMVALUE(VT_g_Ca_T)) * d_T * f_T * (svolt - (CELLMODEL_PARAMVALUE(VT_E_Ca_T)));
  const ML_CalcType i_B_Na = (CELLMODEL_PARAMVALUE(VT_g_B_Na)) * VmE_Na;
  const ML_CalcType i_B_Ca = (CELLMODEL_PARAMVALUE(VT_g_B_Ca)) * VmE_Ca;
  const ML_CalcType i_CaP = (CELLMODEL_PARAMVALUE(VT_i_CaP_max)) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_CaP)));
  const ML_CalcType i_NaCa = (CELLMODEL_PARAMVALUE(VT_k_NaCa)) *
                             (pow(Na_i,
                                  3.00000) * (Ca_c) *
                              exp((CELLMODEL_PARAMVALUE(VT_gamma)) * (CELLMODEL_PARAMVALUE(VT_F)) * svolt / ((CELLMODEL_PARAMVALUE(VT_R)) * (CELLMODEL_PARAMVALUE(VT_T)))) -
                              pow((Na_c),
                                  3.00000) * Ca_i * exp(((CELLMODEL_PARAMVALUE(VT_gamma)) - 1.00000) * svolt * FdRT)) /
                             (1.00000 + (CELLMODEL_PARAMVALUE(VT_d_NaCa)) * (pow((Na_c), 3.00000) * Ca_i +
                                                          pow(Na_i, 3.00000) * (Ca_c)) *
                                        (1.00000 + Ca_i / 0.00690000));
  const ML_CalcType dOCdt = 200000. * Ca_i * (1.00000 - O_C) - 476.000 * O_C;
  const ML_CalcType dOTCdt = 78400.0 * Ca_i * (1.00000 - O_TC) - 392.000 * O_TC;
  const ML_CalcType dOTMgCdt = 200000. * Ca_i * (1.00000 - O_TMgC - O_TMgMg) - 6.60000 * O_TMgC;
  const ML_CalcType dOCalsedt = 480.000 * Ca_rel * (1.00000 - O_Calse) - 400.000 * O_Calse;
  const ML_CalcType i_up = (CELLMODEL_PARAMVALUE(VT_I_up_max)) *
                           (Ca_i / (CELLMODEL_PARAMVALUE(VT_k_cyca)) -
                            pow((CELLMODEL_PARAMVALUE(VT_k_xcs)),
                                2.00000) * Ca_up / (CELLMODEL_PARAMVALUE(VT_k_srca))) /
                           ((Ca_i + (CELLMODEL_PARAMVALUE(VT_k_cyca))) / (CELLMODEL_PARAMVALUE(VT_k_cyca)) +
                            (CELLMODEL_PARAMVALUE(VT_k_xcs)) * (Ca_up + (CELLMODEL_PARAMVALUE(VT_k_srca))) / (CELLMODEL_PARAMVALUE(VT_k_srca)));
  const ML_CalcType i_tr =
      (Ca_up - Ca_rel) * 2.00000 * (CELLMODEL_PARAMVALUE(VT_F)) * (CELLMODEL_PARAMVALUE(VT_Vol_rel)) / (CELLMODEL_PARAMVALUE(VT_tau_tr));
  const ML_CalcType i_rel =
      (CELLMODEL_PARAMVALUE(VT_alpha_rel)) * pow(F2 / (F2 + 0.250000), 2.00000) * (Ca_rel - Ca_i);
  const ML_CalcType r_act = 203.800 * exp(0.0800000 * (svolt - 40.0000)) + 203.800 *
                                                                           pow(Ca_i / (Ca_i +
                                                                                       (CELLMODEL_PARAMVALUE(VT_k_rel))),
                                                                               4.00000);
  const ML_CalcType r_inact = 33.9600 + 339.600 * pow(Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_rel))), 4.00000);

  const ML_CalcType I_mem =
      i_Na + i_Ca_L + i_Ca_T + i_to + i_Ks + i_Kr + i_K1 + i_B_Na + i_B_Ca + ptTeaP->i_B_Cl[Vi] +
      i_p + i_CaP + i_NaCa + i_up + i_tr + i_rel;
  tempstr << ' ' << i_Na << ' ' << i_Ca_L << ' ' << i_Ca_T << ' ' << i_to << ' ' << i_Ks << ' '
          << i_Kr << ' ' << i_K1 << ' ' << i_B_Na <<
          ' '
          << i_B_Ca << ' ' << ptTeaP->i_B_Cl[Vi] << ' ' << i_p << ' ' << i_CaP << ' ' << i_NaCa
          << ' ' << i_up << ' ' << i_tr << ' ' <<
          i_rel << ' ' << I_mem << ' ';
} // LindbladEtAl::LongPrint

void LindbladEtAl::GetParameterNames(vector<string> &getpara) {
  const int numpara = 27;
  const string ParaNames[numpara] =
      {"Na_i", "m", "h1", "h2", "d_L", "f_L", "d_T", "f_T", "K_i", "r",
       "s1",
       "s2", "s3",
       "z",
       "p_a",
       "p_i", "Ca_i", "O_C",
       "O_TC", "O_TMgC", "O_TMgMg", "Ca_rel", "Ca_up", "O_Calse", "F1", "F2", "F3"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void LindbladEtAl::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 17;
  const string ParaNames[numpara] =
      {"i_Na", "i_Ca_L", "i_Ca_T", "i_to", "i_Ks", "i_Kr", "i_K1", "i_B_Na", "i_B_Ca", "i_B_Cl",
       "i_p", "i_CaP",
       "i_NaCa",
       "i_up", "i_tr", "i_rel", "I_mem"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
