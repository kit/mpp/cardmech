/*
 *  HundParameters.h
 *
 *  Created by Christian Rombach on 04.08.08.
 *  Last modified Christian Rombach (15.09.09)
 *  Institute of Biomedical Engineering\n
 *  Universitaet Karlsruhe (TH)\n
 *  http://www.ibt.uni-karlsruhe.de\n
 *
 */

#ifndef HUNDPARAMETERS_H
#define HUNDPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_HundParameters {
  enum varType {
    VT_eta = vtFirst,
    VT_KmNai,
    VT_KmNao,
    VT_KmCai,
    VT_KmCao,
    VT_ibarpca,
    VT_kmpca,
    VT_PCl,
    VT_AA_init,
    VT_Kmto2,
    VT_GClb,
    VT_mL_init,
    VT_GNaL,
    VT_hL_init,
    VT_prnak,
    VT_dtau_rel_max,
    VT_CaMK0,
    VT_Km,
    VT_KmCaMK,
    VT_CaMKtrap_init,
    VT_ro_init,
    VT_ri_init,
    VT_dKmPLBmax,
    VT_dJupmax,
    VT_F,
    VT_K_o,
    VT_Ca_o,
    VT_Na_o,
    VT_Cl_o,
    VT_FonRT,
    VT_tissue,
    VT_V_init,
    VT_l,
    VT_a,
    VT_stim_offset,
    VT_stim_period,
    VT_stim_duration,
    VT_stim_amplitude,
    VT_H_init,
    VT_m_init,
    VT_J_init,
    VT_d_init,
    VT_dp_init,
    VT_f_init,
    VT_fca_init,
    VT_fca2_init,
    VT_f2_init,
    VT_pca,
    VT_gacai,
    VT_gacao,
    VT_iupmax,
    VT_Kmup,
    VT_nsrmax,
    VT_Na_i_init,
    VT_CTNaClmax,
    VT_K_i_init,
    VT_CTKClmax,
    VT_Cl_i_init,
    VT_Ca_i_init,
    VT_Ca_jsr_init,
    VT_Ca_nsr_init,
    VT_Ca_ss_init,
    VT_kmt,
    VT_kmc,
    VT_tbar,
    VT_cbar,
    VT_kmcsqn,
    VT_csqnbar,
    VT_BSRmax,
    VT_KmBSR,
    VT_BSLmax,
    VT_KmBSL,
    VT_xr_init,
    VT_xs1_init,
    VT_xs2_init,
    VT_gitodv,
    VT_ydv_init,
    VT_ydv2_init,
    VT_zdv_init,
    VT_kmnai,
    VT_kmko,
    VT_ibarnak,
    VT_KmCa,
    VT_NCXmax,
    VT_ksat,
    VT_vcell,
    VT_ageo,
    VT_Acap,
    VT_vmyo,
    VT_vmito,
    VT_vsr,
    VT_vnsr,
    VT_vjsr,
    VT_vss,
    VT_AF,
    VT_FONRT,
    VT_GNa,
    VT_sigma,
    vtLast
  };
} // namespace NS_HundParameters

using namespace NS_HundParameters;

class HundParameters : public vbNewElphyParameters {
public:
  HundParameters(const char *, ML_CalcType);

  ~HundParameters();

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType);

  void Init(const char *, ML_CalcType);

  ML_CalcType et1;
  ML_CalcType et3;
  ML_CalcType et10;
  ML_CalcType et600;

  ML_CalcType hss[RTDT];
  ML_CalcType eth[RTDT];
  ML_CalcType jss[RTDT];
  ML_CalcType etj[RTDT];
  ML_CalcType mss[RTDT];
  ML_CalcType etm[RTDT];
  ML_CalcType dss[RTDT];
  ML_CalcType etd[RTDT];
  ML_CalcType fss[RTDT];
  ML_CalcType f2ss[RTDT];
  ML_CalcType etf[RTDT];
  ML_CalcType etf2[RTDT];
  ML_CalcType dpss[RTDT];
  ML_CalcType r[RTDT];
  ML_CalcType xrss[RTDT];
  ML_CalcType etxr[RTDT];
  ML_CalcType xss[RTDT];
  ML_CalcType etxs[RTDT];
  ML_CalcType etxs2[RTDT];
  ML_CalcType rv[RTDT];
  ML_CalcType yss[RTDT];
  ML_CalcType ety[RTDT];
  ML_CalcType y2ss[RTDT];
  ML_CalcType ety2[RTDT];
  ML_CalcType zss[RTDT];
  ML_CalcType etz[RTDT];
  ML_CalcType mLss[RTDT];
  ML_CalcType etmL[RTDT];
  ML_CalcType hLss[RTDT];
}; // class HundParameters
#endif // ifndef HUNDPARAMETERS_H
