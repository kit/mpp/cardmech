/*      File: PiperEtAlParameters.h
    automatically created by ExtractParameterClass.pl - done by dw (05.03.2007)
        Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
        send comments to dw@ibt.uka.de  */

/*! \file PiperEtAl.h
   \brief Markov model of HERG potassium channel
   Piper, Varghese, Sanginetti, and Tristani-Firouzi
   Proc Natl Acad Sci U S A. 2003 Sep 2;100(18):10534-9

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef PIPERETALPARAMETERS_H
#define PIPERETALPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_PiperEtAlParameters {
  enum varType {
    VT_C_m = vtFirst,
    VT_g_HERG,
    VT_E_HERG,
    VT_S0000,
    VT_S0001,
    VT_S0011,
    VT_S0111,
    VT_S1111,
    VT_S1112,
    VT_S1122,
    VT_S1222,
    VT_S2222,
    VT_C1,
    VT_C2,
    VT_O1,
    VT_O2,
    VT_I0,
    VT_I1,
    VT_I2,
    VT_S0S1A0,
    VT_S0S1B0,
    VT_S0S1ZA,
    VT_S0S1ZB,
    VT_S0S1C,
    VT_S1S2A0,
    VT_S1S2B0,
    VT_S1S2ZA,
    VT_S1S2ZB,
    VT_S1S2C,
    VT_S2C1A0,
    VT_S2C1B0,
    VT_S2C1ZA,
    VT_S2C1ZB,
    VT_C1C2A0,
    VT_C1C2B0,
    VT_C1C2ZA,
    VT_C1C2ZB,
    VT_C2O1A0,
    VT_C2O1B0,
    VT_C2O1ZA,
    VT_C2O1ZB,
    VT_O1O2A0,
    VT_O1O2B0,
    VT_O1O2ZA,
    VT_O1O2ZB,
    VT_C2I0A0,
    VT_C2I0B0,
    VT_C2I0ZA,
    VT_C2I0ZB,
    VT_O1I1A0,
    VT_O1I1B0,
    VT_O1I1ZA,
    VT_O1I1ZB,
    VT_O2I2A0,
    VT_O2I2B0,
    VT_O2I2ZA,
    VT_O2I2ZB,
    VT_I0I1A0,
    VT_I0I1B0,
    VT_I0I1ZA,
    VT_I0I1ZB,
    VT_I1I2A0,
    VT_I1I2B0,
    VT_I1I2ZA,
    VT_I1I2ZB,
    VT_Tx,
    VT_FdRT,
    VT_dC_m,
    VT_Amp,
    vtLast
  };
} // namespace NS_PiperEtAlParameters

using namespace NS_PiperEtAlParameters;

class PiperEtAlParameters : public vbNewElphyParameters {
public:
  PiperEtAlParameters(const char *);

  ~PiperEtAlParameters();

  void PrintParameters();

  void Calculate();

  void InitTable();

  void Init(const char *);
};

#endif // ifndef PIPERETALPARAMETERS_H
