/**@file BondarenkoCaParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <BondarenkoCaParameters.h>

BondarenkoCaParameters::BondarenkoCaParameters(const char *initFile) {
#if KADEBUG
  cerr << "BondarenkoCaParameters::BondarenkoCaParameters "<< initFile << endl;
#endif // if KADEBUG
  P = new Parameter[vtLast];
  Init(initFile);
}

void BondarenkoCaParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "BondarenkoCaParameters::init " << initFile << endl;
#endif // if KADEBUG

  P[VT_C_m].name      = "C_m";
  P[VT_g_CaL].name    = "g_CaL";
  P[VT_E_CaL].name    = "E_CaL";
  P[VT_C1].name       = "C1";
  P[VT_C2].name       = "C2";
  P[VT_C3].name       = "C3";
  P[VT_C4].name       = "C4";
  P[VT_I1].name       = "I1";
  P[VT_I2].name       = "I2";
  P[VT_I3].name       = "I3";
  P[VT_O].name        = "O";
  P[VT_a1].name       = "a1";
  P[VT_a2].name       = "a2";
  P[VT_a3].name       = "a3";
  P[VT_a4].name       = "a4";
  P[VT_a5].name       = "a5";
  P[VT_a6].name       = "a6";
  P[VT_a7].name       = "a7";
  P[VT_a8].name       = "a8";
  P[VT_a9].name       = "a9";
  P[VT_a10].name      = "a10";
  P[VT_a11].name      = "a11";
  P[VT_a12].name      = "a12";
  P[VT_b1].name       = "b1";
  P[VT_b2].name       = "b2";
  P[VT_b3].name       = "b3";
  P[VT_K_pcmax].name  = "K_pcmax";
  P[VT_K_pchalf].name = "K_pchalf";
  P[VT_Ca_ss].name    = "Ca_ss";
  P[VT_K_pcf1].name   = "K_pcf1";
  P[VT_K_pcf2].name   = "K_pcf2";
  P[VT_K_pcf3].name   = "K_pcf3";
  P[VT_K_pcb].name    = "K_pcb";
  P[VT_dC_m].name     = "dC_m";
  P[VT_Amp].name      = "Amp";

  P[VT_dC_m].readFromFile = false;


  ParameterLoader EPL(initFile, EMT_BondarenkoCa);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
  InitTable();
} // BondarenkoCaParameters::Init

void BondarenkoCaParameters::PrintParameters() {
  // print the parameter to the stdout
  cout<<"BondarenkoParameters:"<<endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void BondarenkoCaParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "BondarenkoCaParameters - Calculate ..." << endl;
#endif // if KADEBUG

  P[VT_dC_m].value = 1./ P[VT_C_m].value;
}

void BondarenkoCaParameters::InitTable() {
#if KADEBUG
  cerr << "BondarenkoCaParameters - InitTable()" << endl;
#endif // if KADEBUG
}
