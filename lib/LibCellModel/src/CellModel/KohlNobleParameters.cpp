/**@file KohlNobleParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <KohlNobleParameters.h>

KohlNobleParameters::KohlNobleParameters(const char *initFile) {
#if KADEBUG
  cerr << "KohlNobleParameters::KohlNobleParameters "<< initFile << endl;
#endif // if KADEBUG
  P = new Parameter[vtLast];
  Init(initFile);
}

void KohlNobleParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "KohlNobleParameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void KohlNobleParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "KohlNobleParameters::init " << initFile << endl;
#endif // if KADEBUG

  // Initialization of parameter names
  P[VT_C_m].name = "C_m";
  P[VT_g_b].name = "g_b";
  P[VT_g_stretch].name = "g_stretch";
  P[VT_E_b].name = "E_b";
  P[VT_E_stretch].name = "E_stretch";
  P[VT_Vol].name = "Vol";
  P[VT_dC_m].name = "dC_m";
  P[VT_Amp].name = "Amp";

  P[VT_dC_m].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_KohlNoble);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
}

void KohlNobleParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "KohlNobleParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_dC_m].value = 1. / (P[VT_C_m].value);
}

void KohlNobleParameters::InitTable() {
#if KADEBUG
  cerr << "KohlNobleParameters - InitTable()" << endl;
#endif // if KADEBUG
}
