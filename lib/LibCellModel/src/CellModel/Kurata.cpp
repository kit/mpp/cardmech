/*      File: Kurata.cpp
    automatically created by ExtractParameterClass.pl - done by dw (22.03.2007)
    Institute of Biomedical Engineering, Universitt Karlsruhe (TH)
    send comments to dw@ibt.uka.de      */

#include <Kurata.h>

Kurata::Kurata(KurataParameters *pp) {
  pCmP = pp;
#ifdef HETERO
  PS = new ParameterSwitch(pCmP, NS_KurataParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

Kurata::~Kurata() {}

#ifdef HETERO

inline bool Kurata::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool Kurata::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int Kurata::GetSize(void) {
  return sizeof(Kurata) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(KurataParameters *)
#ifdef HETERO
    -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
      ;
}

inline unsigned char Kurata::getSpeed(ML_CalcType adVm) {
  return (unsigned char) (adVm < 1e-6 ? 2 : 1);
}

inline ML_CalcType Kurata::GetVm() {
  return CELLMODEL_PARAMVALUE(VT_Init_Vm);
}

void Kurata::Init() {
#if KADEBUG
  cerr << "Kurata::Init" << endl;
#endif // if KADEBUG

  // Init values
  gdL = CELLMODEL_PARAMVALUE(VT_Init_gdL);
  gfL = CELLMODEL_PARAMVALUE(VT_Init_gfL);
  gpa = CELLMODEL_PARAMVALUE(VT_Init_gpa);
  gn = CELLMODEL_PARAMVALUE(VT_Init_gn);
  gq = CELLMODEL_PARAMVALUE(VT_Init_gq);
  gh = CELLMODEL_PARAMVALUE(VT_Init_gh);
  Cai = CELLMODEL_PARAMVALUE(VT_Init_Cai);
  Carel = CELLMODEL_PARAMVALUE(VT_Init_Carel);
  Caup = CELLMODEL_PARAMVALUE(VT_Init_Caup);
  gdR = CELLMODEL_PARAMVALUE(VT_Init_gdR);
  gfR = CELLMODEL_PARAMVALUE(VT_Init_gfR);
  Rtc = CELLMODEL_PARAMVALUE(VT_Init_Rtc);
  Nai = CELLMODEL_PARAMVALUE(VT_Init_Nai);
  Ki = CELLMODEL_PARAMVALUE(VT_Init_Ki);
}

ML_CalcType
Kurata::Calc(double tinc, ML_CalcType V, ML_CalcType i_external = .0, ML_CalcType stretch = 1.,
             int euler = 2) {
  tinc *= 1000.0;                        // time is in ms (milliseconds, 10^3)
  const ML_CalcType V_int = V * 1000.0; // voltage is in mV (millivolt, 10^3)
  // for accessing the table
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  ML_CalcType I_stim = i_external;
  /**********************************************************************
  * Equilibrium Potentials
  **********************************************************************/
  const double ENa = CELLMODEL_PARAMVALUE(VT_RTdF) * log(CELLMODEL_PARAMVALUE(VT_Nao) / Nai);
  const double EK = CELLMODEL_PARAMVALUE(VT_RTdF) * log(CELLMODEL_PARAMVALUE(VT_Ko) / Ki);
  const double ECa = CELLMODEL_PARAMVALUE(VT_RTdF) * log(CELLMODEL_PARAMVALUE(VT_Cao) / Cai) / 2;
  /**********************************************************************
   * Gating Variables
   ***********************************************************************/

  // only one here, for the rest, see table
  const double gfCas = CELLMODEL_PARAMVALUE(VT_KmfCa) / (CELLMODEL_PARAMVALUE(VT_KmfCa) + Cai);  // (8)
  /**********************************************************************
   * Ionic Currents
   ***********************************************************************/

  // iCa,L - L-type Ca2+ channel current
  const double iCaL = CELLMODEL_PARAMVALUE(VT_gCaLmax) * (V_int - CELLMODEL_PARAMVALUE(VT_ECaL)) * gdL * gfL * gfCas;  // (3)
// iKr - Rapidly activating delayed-rectifier K+ current
  const double EKr = EK;
  const double iKr = CELLMODEL_PARAMVALUE(VT_gKrmax) * (V_int - EKr) * gpa *
                     pCmP->gpis[Vi];  // (9) gpa and gpis are gating variables,
  // dependent from time
// iKs - Slowly activating delayed-rectifier K+ current
  const double EKs =
      CELLMODEL_PARAMVALUE(VT_RTdF) * log((CELLMODEL_PARAMVALUE(VT_PNaKs) * CELLMODEL_PARAMVALUE(VT_Nao) + CELLMODEL_PARAMVALUE(VT_Ko)) / (CELLMODEL_PARAMVALUE(VT_PNaKs) * Nai + Ki));  // (16)
  const double iKs = CELLMODEL_PARAMVALUE(VT_gKsmax) * (V_int - EKs) * (gn * gn);  // (15)
// ito - Transient outward current
  const double Eto =
      CELLMODEL_PARAMVALUE(VT_RTdF) * log((CELLMODEL_PARAMVALUE(VT_PNato) * CELLMODEL_PARAMVALUE(VT_Nao) + CELLMODEL_PARAMVALUE(VT_Ko)) / (CELLMODEL_PARAMVALUE(VT_PNato) * Nai + Ki));  // (20)
  const double ito = CELLMODEL_PARAMVALUE(VT_gtomax) * (V_int - Eto) * pCmP->grs[Vi] * gq;  // (19)
// iNa - Na+ channel current
  const double Emh =
      CELLMODEL_PARAMVALUE(VT_RTdF) * log((CELLMODEL_PARAMVALUE(VT_Nao) + CELLMODEL_PARAMVALUE(VT_PKNa) * CELLMODEL_PARAMVALUE(VT_Ko)) / (Nai + CELLMODEL_PARAMVALUE(VT_PKNa) * Ki));  // (29)
  const double iNa =
      CELLMODEL_PARAMVALUE(VT_gNamax) * (V_int - Emh) * pCmP->gms[Vi] * pCmP->gms[Vi] * pCmP->gms[Vi] * gh *
      gh;  // (28)
// iK1 - Inward-rectifier K+ channels current
  const double gk1a = .1 / (1 + exp(.06 * (V_int - EK - 200)));  // (36)
  const double gk1b = (3 * exp(.0002 * (V_int - EK + 100)) + exp(.1 * (V_int - EK - 10))) /
                      (1 + exp(-(.5) * (V_int - EK)));  // (37)
  const double iK1 = CELLMODEL_PARAMVALUE(VT_gK1) * (V_int - EK) * gk1a / (gk1a + gk1b); // (35)
// background Na+/Ca2+ currents
  const double ibNa = CELLMODEL_PARAMVALUE(VT_gbNa) * (V_int - ENa);  // (38)
  const double ibCa = CELLMODEL_PARAMVALUE(VT_gbCa) * (V_int - ECa);  // (39)
  const double ibK = CELLMODEL_PARAMVALUE(VT_gbK) * (V_int - EK);

  // iNaK - Na+ K+ pump current
  const double iNaK = CELLMODEL_PARAMVALUE(VT_iNaKmax) * (CELLMODEL_PARAMVALUE(VT_Ko) / (CELLMODEL_PARAMVALUE(VT_Ko) + CELLMODEL_PARAMVALUE(VT_KmKp))) /
                      (1 +
                       pow((CELLMODEL_PARAMVALUE(VT_KmNap) / Nai),
                           CELLMODEL_PARAMVALUE(VT_nNa))) /
                      (1 + .1245 * exp(-.1 * CELLMODEL_PARAMVALUE(VT_FdRT) * V_int) +
                       .0365 * CELLMODEL_PARAMVALUE(VT_RhoNaK) * exp(-(CELLMODEL_PARAMVALUE(VT_FdRT)) * V_int));  // (40)
// iNaCa - Na+/Ca2+ exchanger current
  const double iNaCa = CELLMODEL_PARAMVALUE(VT_kNaCa) /
                       ((CELLMODEL_PARAMVALUE(VT_KmNaex)) * (CELLMODEL_PARAMVALUE(VT_KmNaex)) * (CELLMODEL_PARAMVALUE(VT_KmNaex)) +
                        (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao))) /
                       (CELLMODEL_PARAMVALUE(VT_KmCaex) + CELLMODEL_PARAMVALUE(VT_Cao)) *
                       (CELLMODEL_PARAMVALUE(VT_Cao) * Nai * Nai * Nai * exp(CELLMODEL_PARAMVALUE(VT_rNaCa) * CELLMODEL_PARAMVALUE(VT_FdRT) * V_int) -
                        (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao)) * Cai *
                        exp((CELLMODEL_PARAMVALUE(VT_rNaCa) - 1) * CELLMODEL_PARAMVALUE(VT_FdRT) * V_int)) /
                       (1 + CELLMODEL_PARAMVALUE(VT_ksat) * exp((CELLMODEL_PARAMVALUE(VT_rNaCa) - 1) * CELLMODEL_PARAMVALUE(VT_FdRT) * V_int));  //
  // (41)
  // ipCa - Sarcolemmal
  // Ca2+ pump current
  const double ipCa = CELLMODEL_PARAMVALUE(VT_ipCamax) / (1 + (CELLMODEL_PARAMVALUE(VT_KmCap) / Cai));  // (42)
  // total membrane
  // current
  double itotal = iCaL + iKr + iKs + ito + iNa + iK1 + ibNa + ibCa + ibK + iNaK + iNaCa +
                  ipCa; // (43) ibK=0 in control
  // net ion fluxes
  const double jNanet = (iNa + ibNa + 3 * iNaK + 3 * iNaCa) * CELLMODEL_PARAMVALUE(VT_CmdF);  // (44)
  const double jKnet =
      (iKr + iKs + ito + iK1 + ibK - 2 * iNaK) * CELLMODEL_PARAMVALUE(VT_CmdF); // (45) ibK=0 in control
  const double jCanet = (iCaL + ibCa - 2 * iNaCa + ipCa) * CELLMODEL_PARAMVALUE(VT_CmdTwoF);  // (46)
// Ca2+-induced Ca2+ release via Ryanodine receptor in SR (mM/msec)
// const double gdRt = 4 * 4; // (51)
// const double gfRt = 4 * 4; // Kurata.doc says 4 * 4; // (51)
  const double gdRt = 4 * 4;
  const double gfRt = 4 * 4;
  const double jrel =
      (CELLMODEL_PARAMVALUE(VT_Prel) / (1 + exp((iCaL + ibCa - 2 * iNaCa + ipCa + 5) / .9))) * (gdR * gfR) *
      (gdR * gfR) *
      (gdR * gfR) * (Carel - Cai);  // (47) + (48)
// Ca2+ uptake via Ca2+-pump in SR (mM/msec)
  const double jup = CELLMODEL_PARAMVALUE(VT_Pup) * (Cai * Cai) / ((Cai * Cai) + (CELLMODEL_PARAMVALUE(VT_Kup) * CELLMODEL_PARAMVALUE(VT_Kup)));  // (52)
// Ca2+ transfer/leak in SR (mM/msec)
  const double jtr = (Caup - Carel) / CELLMODEL_PARAMVALUE(VT_Ttr); // (53)
  const double jleak = CELLMODEL_PARAMVALUE(VT_Pleak) * (Caup - Cai);  // (54)
// Ca buffering flux
  const double Ftc = CELLMODEL_PARAMVALUE(VT_kfTC) * Cai * (1 - Rtc) - CELLMODEL_PARAMVALUE(VT_kbTC) * Rtc;  // (57)
  const double Bcm =
      1 / (1 + CELLMODEL_PARAMVALUE(VT_ConcCM) * CELLMODEL_PARAMVALUE(VT_KdCM) / (CELLMODEL_PARAMVALUE(VT_KdCM) + Cai) * (CELLMODEL_PARAMVALUE(VT_KdCM) + Cai));  // (58)
  const double Bcq =
      1 / (1 + CELLMODEL_PARAMVALUE(VT_ConcCQ) * CELLMODEL_PARAMVALUE(VT_KdCQ) / (CELLMODEL_PARAMVALUE(VT_KdCQ) + Carel) * (CELLMODEL_PARAMVALUE(VT_KdCQ) + Carel));  // (59)
  /**********************************************************************
   * Final Calculation
   ***********************************************************************/

  // (1) dVm/dt => see return
  // Gating Variables
  // (2) ddL/dt
  gdL += ((pCmP->gdLs[Vi] - gdL) / pCmP->gdLt[Vi]) * tinc;  // (56)
// (3) dfL/dt
  gfL += ((pCmP->gfLs[Vi] - gfL) / pCmP->gfLt[Vi]) * tinc;  // (56)
// (4) dpa/dt
  gpa += ((pCmP->gpas[Vi] - gpa) / pCmP->gpat[Vi]) * tinc;  // (56)
// (5) dn/dt
  gn += ((pCmP->gns[Vi] - gn) / pCmP->gnt[Vi]) * tinc;  // (56)
// (6) dq/dt
  gq += ((pCmP->gqs[Vi] - gq) / pCmP->gqt[Vi]) * tinc;  // (56)
// (7) dh/dt
  gh += ((pCmP->ghs[Vi] - gh) / pCmP->ght[Vi]) * tinc;  // (56)
// Ion Concentrations
// (8) d[Ca]i/dt
  Cai +=
      (Bcm * ((-jCanet + jrel * CELLMODEL_PARAMVALUE(VT_Vrel) - jup * CELLMODEL_PARAMVALUE(VT_Vup) + jleak * CELLMODEL_PARAMVALUE(VT_Vup)) / CELLMODEL_PARAMVALUE(VT_Vi) -
              CELLMODEL_PARAMVALUE(VT_ConcTC) * Ftc)) *
      tinc;  // (60)
  // (9) d[Ca]rel/dt
  Carel += (Bcq * (jtr - jrel)) * tinc;  // (61)
// (10) d[Ca]up/dt
  Caup += (jup - jtr * CELLMODEL_PARAMVALUE(VT_Vrel) / CELLMODEL_PARAMVALUE(VT_Vup) - jleak) * tinc;  // (62)
// (11) ddR/dt
  gdR += ((pCmP->gdRs[Vi] - gdR) / gdRt) * tinc;  // (56)
// (12) dfR/dt
  gfR += ((pCmP->gfRs[Vi] - gfR) / gfRt) * tinc;  // (56)
// (13) dfTnCa/dt
  Rtc += Ftc * tinc;

  // (14) d[Na]i/dt
  Nai += (-jNanet / CELLMODEL_PARAMVALUE(VT_Vi)) * tinc;  // (63)
// (15) d[K]i/dt
  Ki += ((I_stim * CELLMODEL_PARAMVALUE(VT_C_m) / CELLMODEL_PARAMVALUE(VT_F) - jKnet) / CELLMODEL_PARAMVALUE(VT_Vi)) * tinc;  // (64)
  return .001 * (I_stim - itotal) * tinc;  // (55)
} // Kurata::Calc

void Kurata::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' ' << V << ' ' << Cai << ' ' << Carel << ' ' << Caup << ' ' << Nai << ' ' << Ki
          << ' ';
}

void Kurata::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  const ML_CalcType V_int = V * 1000.0; // voltage is in mV (millivolt, 10^3)
  // for accessing the table
  const int Vi = (int) (DivisionTab * (RangeTabhalf + V_int) + .5);
  /**********************************************************************
  * Equilibrium Potentials
  **********************************************************************/
  const double ENa = CELLMODEL_PARAMVALUE(VT_RTdF) * log(CELLMODEL_PARAMVALUE(VT_Nao) / Nai);
  const double EK = CELLMODEL_PARAMVALUE(VT_RTdF) * log(CELLMODEL_PARAMVALUE(VT_Ko) / Ki);
  const double ECa = CELLMODEL_PARAMVALUE(VT_RTdF) * log(CELLMODEL_PARAMVALUE(VT_Cao) / Cai) / 2;
  /**********************************************************************
   * Gating Variables
   ***********************************************************************/

  // only one here, for the rest, see table
  const double gfCas = CELLMODEL_PARAMVALUE(VT_KmfCa) / (CELLMODEL_PARAMVALUE(VT_KmfCa) + Cai);  // (8)
  /**********************************************************************
   * Ionic Currents
   ***********************************************************************/

  // iCa,L - L-type Ca2+ channel current
  const double iCaL = CELLMODEL_PARAMVALUE(VT_gCaLmax) * (V_int - CELLMODEL_PARAMVALUE(VT_ECaL)) * gdL * gfL * gfCas;  // (3)
// iKr - Rapidly activating delayed-rectifier K+ current
  const double EKr = EK;
  const double iKr = CELLMODEL_PARAMVALUE(VT_gKrmax) * (V_int - EKr) * gpa *
                     pCmP->gpis[Vi];  // (9) gpa and gpis are gating variables,
  // dependent from time
// iKs - Slowly activating delayed-rectifier K+ current
  const double EKs =
      CELLMODEL_PARAMVALUE(VT_RTdF) * log((CELLMODEL_PARAMVALUE(VT_PNaKs) * CELLMODEL_PARAMVALUE(VT_Nao) + CELLMODEL_PARAMVALUE(VT_Ko)) / (CELLMODEL_PARAMVALUE(VT_PNaKs) * Nai + Ki));  // (16)
  const double iKs = CELLMODEL_PARAMVALUE(VT_gKsmax) * (V_int - EKs) * (gn * gn);  // (15)
// ito - Transient outward current
  const double Eto =
      CELLMODEL_PARAMVALUE(VT_RTdF) * log((CELLMODEL_PARAMVALUE(VT_PNato) * CELLMODEL_PARAMVALUE(VT_Nao) + CELLMODEL_PARAMVALUE(VT_Ko)) / (CELLMODEL_PARAMVALUE(VT_PNato) * Nai + Ki));  // (20)
  const double ito = CELLMODEL_PARAMVALUE(VT_gtomax) * (V_int - Eto) * pCmP->grs[Vi] * gq;  // (19)
// iNa - Na+ channel current
  const double Emh =
      CELLMODEL_PARAMVALUE(VT_RTdF) * log((CELLMODEL_PARAMVALUE(VT_Nao) + CELLMODEL_PARAMVALUE(VT_PKNa) * CELLMODEL_PARAMVALUE(VT_Ko)) / (Nai + CELLMODEL_PARAMVALUE(VT_PKNa) * Ki));  // (29)
  const double iNa =
      CELLMODEL_PARAMVALUE(VT_gNamax) * (V_int - Emh) * pCmP->gms[Vi] * pCmP->gms[Vi] * pCmP->gms[Vi] * gh *
      gh;  // (28)
// iK1 - Inward-rectifier K+ channels current
  const double gk1a = .1 / (1 + exp(.06 * (V_int - EK - 200)));  // (36)
  const double gk1b = (3 * exp(.0002 * (V_int - EK + 100)) + exp(.1 * (V_int - EK - 10))) /
                      (1 + exp(-(.5) * (V_int - EK)));  // (37)
  const double iK1 = CELLMODEL_PARAMVALUE(VT_gK1) * (V_int - EK) * gk1a / (gk1a + gk1b); // (35)
// background Na+/Ca2+ currents
  const double ibNa = CELLMODEL_PARAMVALUE(VT_gbNa) * (V_int - ENa);  // (38)
  const double ibCa = CELLMODEL_PARAMVALUE(VT_gbCa) * (V_int - ECa);  // (39)
  const double ibK = CELLMODEL_PARAMVALUE(VT_gbK) * (V_int - EK);

  // iNaK - Na+ K+ pump current
  const double iNaK = CELLMODEL_PARAMVALUE(VT_iNaKmax) * (CELLMODEL_PARAMVALUE(VT_Ko) / (CELLMODEL_PARAMVALUE(VT_Ko) + CELLMODEL_PARAMVALUE(VT_KmKp))) /
                      (1 +
                       pow((CELLMODEL_PARAMVALUE(VT_KmNap) / Nai),
                           CELLMODEL_PARAMVALUE(VT_nNa))) /
                      (1 + .1245 * exp(-.1 * CELLMODEL_PARAMVALUE(VT_FdRT) * V_int) +
                       .0365 * CELLMODEL_PARAMVALUE(VT_RhoNaK) * exp(-(CELLMODEL_PARAMVALUE(VT_FdRT)) * V_int));  // (40)
// iNaCa - Na+/Ca2+ exchanger current
  const double iNaCa = CELLMODEL_PARAMVALUE(VT_kNaCa) /
                       ((CELLMODEL_PARAMVALUE(VT_KmNaex)) * (CELLMODEL_PARAMVALUE(VT_KmNaex)) * (CELLMODEL_PARAMVALUE(VT_KmNaex)) +
                        (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao))) /
                       (CELLMODEL_PARAMVALUE(VT_KmCaex) + CELLMODEL_PARAMVALUE(VT_Cao)) *
                       (CELLMODEL_PARAMVALUE(VT_Cao) * Nai * Nai * Nai * exp(CELLMODEL_PARAMVALUE(VT_rNaCa) * CELLMODEL_PARAMVALUE(VT_FdRT) * V_int) -
                        (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao)) * (CELLMODEL_PARAMVALUE(VT_Nao)) * Cai *
                        exp((CELLMODEL_PARAMVALUE(VT_rNaCa) - 1) * CELLMODEL_PARAMVALUE(VT_FdRT) * V_int)) /
                       (1 + CELLMODEL_PARAMVALUE(VT_ksat) * exp((CELLMODEL_PARAMVALUE(VT_rNaCa) - 1) * CELLMODEL_PARAMVALUE(VT_FdRT) * V_int));  //
  // (41)
// ipCa - Sarcolemmal Ca2+ pump current
  const double ipCa = CELLMODEL_PARAMVALUE(VT_ipCamax) / (1 + (CELLMODEL_PARAMVALUE(VT_KmCap) / Cai)); // (42)
  const double I_mem = iCaL + iKr + iKs + ito + iNa + iK1 + ibNa + ibCa + ibK + iNaK + iNaCa + ipCa;

  tempstr << iCaL << ' ' << iKr << ' ' << iKs << ' ' << ito << ' ' << iNa << ' ' << iK1 << ' '
          << ibNa << ' ' << ibCa <<
          ' ' << ibK << ' '
          << iNaK << ' ' << iNaCa << ' ' << ipCa << ' ';
} // Kurata::LongPrint

void Kurata::GetParameterNames(vector<string> &getpara) {
  const int numpara = 5;
  const string ParaNames[numpara] = {"Cai", "Carel", "Caup", "Nai", "Ki"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void Kurata::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 13;
  const string ParaNames[numpara] =
      {"iCaL", "iKr", "iKs", "ito", "iNa", "iK1", "ibNa", "ibCa", "ibK", "iNaK", "iNaCa", "ipCa",
       "I_mem"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
