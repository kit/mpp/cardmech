/**@file IyerMazhariWinslowCaParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowCaParameters.h>

IyerMazhariWinslowCaParameters::IyerMazhariWinslowCaParameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void IyerMazhariWinslowCaParameters::Init(const char *initFile) {
#if KADEBUG
  cerr << "IyerMazhariWinslowCaParameters:init " << initFile << endl;
#endif // if KADEBUG

  // Initialization of parameter names
  P[VT_Tx].name = "Tx";
  P[VT_Cao].name = "Cao";
  P[VT_Cai].name = "Cai";

  P[VT_C0L].name = "C0L";
  P[VT_C1L].name = "C1L";
  P[VT_C2L].name = "C2L";
  P[VT_C3L].name = "C3L";
  P[VT_C4L].name = "C4L";
  P[VT_OL].name = "OL";
  P[VT_CCa0L].name = "CCa0L";
  P[VT_CCa1L].name = "CCa1L";
  P[VT_CCa2L].name = "CCa2L";
  P[VT_CCa3L].name = "CCa3L";
  P[VT_CCa4L].name = "CCa4L";
  P[VT_y].name = "y";
#ifdef TIMOTHY
  P[VT_ytimothy].name = "ytimothy";
  P[VT_y1].name       = "y1";
  P[VT_y2].name       = "y2";
  P[VT_y3].name       = "y3";
  P[VT_y4].name       = "y4";
  P[VT_y5].name       = "y5";
  P[VT_y6].name       = "y6";
  P[VT_y7].name       = "y7";
#endif // ifdef TIMOTHY
#ifdef TIMOTHY2
  P[VT_CaLShift].name = "CaLShift";

  P[VT_C0Ltimothy].name = "C0Ltimothy";
  P[VT_C1Ltimothy].name = "C1Ltimothy";
  P[VT_C2Ltimothy].name = "C2Ltimothy";
  P[VT_C3Ltimothy].name = "C3Ltimothy";
  P[VT_C4Ltimothy].name = "C4Ltimothy";
  P[VT_OLtimothy].name = "OLtimothy";
  P[VT_CCa0Ltimothy].name = "CCa0Ltimothy";
  P[VT_CCa1Ltimothy].name = "CCa1Ltimothy";
  P[VT_CCa2Ltimothy].name = "CCa2Ltimothy";
  P[VT_CCa3Ltimothy].name = "CCa3Ltimothy";
  P[VT_CCa4Ltimothy].name = "CCa4Ltimothy";

  P[VT_ytimothy].name = "ytimothy";

  P[VT_y1].name = "y1";
  P[VT_y2].name = "y2";
  P[VT_y3].name = "y3";

  P[VT_tytimothyscale].name = "tytimothyscale";
#endif // ifdef TIMOTHY2

  P[VT_CaLf].name = "CaLf";
  P[VT_CaLg].name = "CaLg";
  P[VT_CaLa].name = "CaLa";
  P[VT_CaLb].name = "CaLb";
  P[VT_CaLo].name = "CaLo";

  P[VT_PCa].name = "PCa";

  P[VT_ShiftVm].name = "ShiftVmInactivation";
  P[VT_Amp].name = "Amp";

  ParameterLoader EPL(initFile, EMT_IyerMazhariWinslowCa);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
} // IyerMazhariWinslowCaParameters::Init

void IyerMazhariWinslowCaParameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "IyerMazhariWinslowCaParameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void IyerMazhariWinslowCaParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
}
