/*! \file IyerMazhariWinslowKs.h
   \brief Implementation of I_Ks of electrophysiological model for describing human ventricular myocytes
   modified/corrected from Iyer et al., Biophys J 2004

   \author fs, CVRTI - University of Utah, USA
 */

#ifndef IYER_MAZHARI_WINSLOW_KS_H
#define IYER_MAZHARI_WINSLOW_KS_H

#include <IyerMazhariWinslowKsParameters.h>

// IKSOPEN defines ratio of mutated open to WT channels
// for WT: #undef IKSOPEN or #define IKSOPEN 0

#undef IKSOPEN
#define IKSOPEN 0.5

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_IyerMazhariWinslowKrParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pIMW->P[NS_IyerMazhariWinslowKsParameters::a].value
#endif // ifdef HETERO

class IyerMazhariWinslowKs : public vbElphyModel<ML_CalcType> {
public:
  double Ki;

  double C0Ks;
  double C1Ks;
  double O1Ks;
  double O2Ks;

  IyerMazhariWinslowKsParameters *pIMW;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  IyerMazhariWinslowKs(IyerMazhariWinslowKsParameters *);

  ~IyerMazhariWinslowKs() {}

  virtual void Init();

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vmyo); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.; }

  virtual inline ML_CalcType GetVm() { return -.09066; }

  virtual inline ML_CalcType GetCai() { return 0; }

  virtual inline ML_CalcType GetCao() { return 0; }

  virtual inline ML_CalcType GetNai() { return 0; }

  virtual inline ML_CalcType GetNao() { return 0; }

  virtual inline ML_CalcType GetKi() { return Ki; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_Ko); }

  virtual inline int GetSize(void) {
    return sizeof(IyerMazhariWinslowKs) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(IyerMazhariWinslowKsParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return (ML_CalcType *) &Ki; }

  virtual inline void SetCai(ML_CalcType val) {}

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);
}; // class IyerMazhariWinslowKs

#endif // ifndef IYER_MAZHARI_WINSLOW_KS_H
