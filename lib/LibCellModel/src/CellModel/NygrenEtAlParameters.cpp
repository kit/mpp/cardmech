/**@file NygrenEtAlParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <NygrenEtAlParameters.h>

NygrenEtAlParameters::NygrenEtAlParameters(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "NygrenEtAlParameters::NygrenEtAlParameters "<< initFile << endl;
#endif // if KADEBUG
  P = new Parameter[vtLast];
  Init(initFile, tinc);
}

void NygrenEtAlParameters::PrintParameters() {
  // print the parameter to the stdout
  cout<<"NygrenEtAlParameters:"<<endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void NygrenEtAlParameters::Init(const char *initFile, ML_CalcType tinc) {
#if KADEBUG
  cerr << "Loading the NygrenEtAl parameter from " << initFile << " ...\n";
#endif // if KADEBUG

  P[VT_R].name            = "R";
  P[VT_Tx].name           = "T";
  P[VT_F].name            = "F";
  P[VT_RTdF].name         = "RTdF";
  P[VT_RTd2F].name        = "RTd2F";
  P[VT_FdRT].name         = "FdRT";
  P[VT_C_m].name          = "C_m";
  P[VT_dC_m].name         = "dC_m";
  P[VT_Na_b].name         = "Na_b";
  P[VT_K_b].name          = "K_b";
  P[VT_Ca_b].name         = "Ca_b";
  P[VT_Mg_i].name         = "Mg_i";
  P[VT_K_Ca].name         = "K_Ca";
  P[VT_K_cyca].name       = "K_cyca";
  P[VT_dKcyca].name       = "dKcyca";
  P[VT_K_srca].name       = "K_srca";
  P[VT_dKsrca].name       = "dKsrca";
  P[VT_K_reli].name       = "K_reli";
  P[VT_K_reld].name       = "K_reld";
  P[VT_K_NaKK].name       = "K_NaKK";
  P[VT_K_NaKNa].name      = "K_NaKNa";
  P[VT_K_CaP].name        = "K_CaP";
  P[VT_K_xcs].name        = "K_xcs";
  P[VT_K_NaCa].name       = "K_NaCa";
  P[VT_K_NaKNapow].name   = "K_NaKNapow";
  P[VT_KxcsdKsrca].name   = "KxcsdKsrca";
  P[VT_Kxcsh2dKsrca].name = "Kxcsh2dKsrca";
  P[VT_V_i].name          = "V_i";
  P[VT_V_corrcell].name   = "V_corrcell";
  P[VT_V_c].name          = "V_c";
  P[VT_V_d].name          = "V_d";
  P[VT_V_rel].name        = "V_rel";
  P[VT_V_up].name         = "V_up";
  P[VT_Area].name         = "Area";
  P[VT_radius].name       = "radius";
  P[VT_length].name       = "length";
  P[VT_t_Na].name         = "t_Na";
  P[VT_dt_Na].name        = "dt_Na";
  P[VT_t_K].name          = "t_K";
  P[VT_dt_K].name         = "dt_K";
  P[VT_t_Ca].name         = "t_Ca";
  P[VT_dt_Ca].name        = "dt_Ca";
  P[VT_t_di].name         = "t_di";
  P[VT_t_tr].name         = "t_tr";
  P[VT_I_NaK_].name       = "I_NaK_";
  P[VT_I_CaP_].name       = "I_CaP_";
  P[VT_I_up_].name        = "I_up_";
  P[VT_I_Naen_].name      = "I_Naen_";
  P[VT_P_Na].name         = "P_Na";
  P[VT_g_CaL].name        = "g_CaL";
  P[VT_g_sus].name        = "g_sus";
  P[VT_g_t].name          = "g_t";
  P[VT_g_Ks].name         = "g_Ks";
  P[VT_g_Kr].name         = "g_Kr";
  P[VT_g_K1].name         = "g_K1";
  P[VT_g_BNa].name        = "g_BNa";
  P[VT_g_BCa].name        = "g_BCa";
  P[VT_E_Caapp].name      = "E_Caapp";
  P[VT_Gamma].name        = "Gamma";
  P[VT_d_NaCa].name       = "d_NaCa";
  P[VT_arel].name         = "arel";
  P[VT_rrecov].name       = "rrecov";

  P[VT_Ad2VdF].name = "Ad2VdF";
  P[VT_FVd2dt].name = "FVd2dt";
  P[VT_Ad2VrF].name = "Ad2VrF";
  P[VT_FVr2dt].name = "FVr2dt";
  P[VT_AdVcF].name  = "AdVcF";
  P[VT_AdVc2F].name = "AdVc2F";
  P[VT_AdViF].name  = "AdViF";
  P[VT_AdVi2F].name = "AdVi2F";
  P[VT_Ad2VuF].name = "Ad2VuF";
#ifdef NYEA_TASK
  P[VT_Koh_TASK].name     = "Koh_TASK";
  P[VT_P_TASK].name       = "P_TASK";
  P[VT_aCC_TASK].name     = "aCC_TASK";
  P[VT_zaCC_TASK].name    = "zaCC_TASK";
  P[VT_bCC_TASK].name     = "bCC_TASK";
  P[VT_zbCC_TASK].name    = "zbCC_TASK";
  P[VT_aCO_TASK].name     = "aCO_TASK";
  P[VT_zCO_TASK].name     = "zCO_TASK";
  P[VT_aOC_TASK].name     = "aOC_TASK";
  P[VT_zOC_TASK].name     = "zOC_TASK";
  P[VT_init_C1_TASK].name = "init_C1_TASK";
  P[VT_init_C2_TASK].name = "init_C2_TASK";
  P[VT_init_O_TASK].name  = "init_O_TASK";
#endif // ifdef NYEA_TASK

  // Initializing ...
  P[VT_init_h1].name      = "init_h1";
  P[VT_init_h2].name      = "init_h2";
  P[VT_init_fl2].name     = "init_fl2";
  P[VT_init_s].name       = "init_s";
  P[VT_init_ssus].name    = "init_ssus";
  P[VT_init_n].name       = "init_n";
  P[VT_init_pa].name      = "init_pa";
  P[VT_init_Ca_i].name    = "init_Ca_i";
  P[VT_init_Ca_d].name    = "init_Ca_d";
  P[VT_init_O_c].name     = "init_O_c";
  P[VT_init_O_tc].name    = "init_O_tc";
  P[VT_init_O_tmgc].name  = "init_O_tmgc";
  P[VT_init_O_calse].name = "init_O_calse";
  P[VT_init_Ca_up].name   = "init_Ca_up";
  P[VT_init_Ca_rel].name  = "init_Ca_rel";
  P[VT_init_Fl1].name     = "init_Fl1";
  P[VT_init_Fl2].name     = "init_Fl2";

  P[VT_init_m].name        = "init_m";
  P[VT_init_dl].name       = "init_dl";
  P[VT_init_fl1].name      = "init_fl1";
  P[VT_init_r].name        = "init_r";
  P[VT_init_rsus].name     = "init_rsus";
  P[VT_init_Na_i].name     = "init_Na_i";
  P[VT_init_Na_c].name     = "init_Na_c";
  P[VT_init_K_i].name      = "init_K_i";
  P[VT_init_K_c].name      = "init_K_c";
  P[VT_init_Ca_c].name     = "init_Ca_c";
  P[VT_init_O_tmgmg].name  = "init_O_tmgmg";
  P[VT_init_Vm].name       = "init_Vm";
  P[VT_Amp].name           = "Amp";
  P[VT_stim_duration].name = "stim_duration";

  P[VT_RTdF].readFromFile         = false;
  P[VT_RTd2F].readFromFile        = false;
  P[VT_FdRT].readFromFile         = false;
  P[VT_dC_m].readFromFile         = false;
  P[VT_dKcyca].readFromFile       = false;
  P[VT_dKsrca].readFromFile       = false;
  P[VT_K_NaKNapow].readFromFile   = false;
  P[VT_KxcsdKsrca].readFromFile   = false;
  P[VT_Kxcsh2dKsrca].readFromFile = false;
  P[VT_V_c].readFromFile          = false;
  P[VT_V_d].readFromFile          = false;
  P[VT_V_rel].readFromFile        = false;
  P[VT_V_up].readFromFile         = false;
  P[VT_dt_Na].readFromFile        = false;
  P[VT_dt_K].readFromFile         = false;
  P[VT_dt_Ca].readFromFile        = false;

  P[VT_Ad2VdF].readFromFile = false;
  P[VT_FVd2dt].readFromFile = false;
  P[VT_Ad2VrF].readFromFile = false;
  P[VT_FVr2dt].readFromFile = false;

  P[VT_AdVcF].readFromFile  = false;
  P[VT_AdVc2F].readFromFile = false;
  P[VT_AdViF].readFromFile  = false;
  P[VT_AdVi2F].readFromFile = false;
  P[VT_Ad2VuF].readFromFile = false;

  ParameterLoader EPL(initFile, EMT_NygrenEtAl);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
  InitTable(tinc);
} // NygrenEtAlParameters::Init

void NygrenEtAlParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "NygrenEtAlParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_RTdF].value         = P[VT_R].value*(P[VT_Tx].value)/(P[VT_F].value);
  P[VT_RTd2F].value        = P[VT_RTdF].value*.5;
  P[VT_FdRT].value         = 1.0/(P[VT_RTdF].value);
  P[VT_dC_m].value         = 1/(P[VT_C_m].value);
  P[VT_dKcyca].value       = 1.0/(P[VT_K_cyca].value);
  P[VT_dKsrca].value       = 1.0/(P[VT_K_srca].value);
  P[VT_K_NaKNapow].value   = P[VT_K_NaKNa].value*sqrt(P[VT_K_NaKNa].value);
  P[VT_KxcsdKsrca].value   = P[VT_K_xcs].value*(P[VT_dKsrca].value);
  P[VT_Kxcsh2dKsrca].value = P[VT_K_xcs].value*(P[VT_KxcsdKsrca].value);
  P[VT_V_c].value          = .136*(P[VT_V_i].value);
  P[VT_V_d].value          = .02*(P[VT_V_i].value);
  P[VT_V_rel].value        = .0074949014*(P[VT_V_i].value);
  P[VT_V_up].value         = .067454113*(P[VT_V_i].value);
  P[VT_dt_Na].value        = 1.0/(P[VT_t_Na].value);
  P[VT_dt_K].value         = 1.0/(P[VT_t_K].value);
  P[VT_dt_Ca].value        = 1.0/(P[VT_t_Ca].value);

  P[VT_Ad2VdF].value = 1./(2.*P[VT_F].value*P[VT_V_d].value);
  P[VT_FVd2dt].value = 1./(P[VT_t_di].value*(P[VT_Ad2VdF].value));
  P[VT_Ad2VrF].value = 1./(2.*P[VT_F].value*P[VT_V_rel].value);
  P[VT_FVr2dt].value = 1./(P[VT_t_tr].value*(P[VT_Ad2VrF].value));

  P[VT_AdVcF].value  = 1./(P[VT_V_c].value*P[VT_F].value);
  P[VT_AdVc2F].value = P[VT_AdVcF].value*.5;
  P[VT_AdViF].value  = 1./(P[VT_V_i].value*P[VT_F].value);
  P[VT_AdVi2F].value = P[VT_AdViF].value*.5;
  P[VT_Ad2VuF].value = 1./(2.*P[VT_F].value*P[VT_V_up].value);
} // NygrenEtAlParameters::Calculate

void NygrenEtAlParameters::InitTable(ML_CalcType tinc) {
#if KADEBUG
  cerr<<"NygrenEtAlParameters - InitTable\n";
#endif // if KADEBUG
  tinc *= -1000.0;  // sec. -> ms, negation for exptau calculation
  for (double V = -RangeTabhalf+0.0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int)(DivisionTab*(RangeTabhalf+V)+.5);

    m_[Vi]       = 1.0/(1.0+exp(-(V+27.12)*0.12180268));
    exptau_m[Vi] = exp(tinc / (.042*exp(-(V+25.57)*(V+25.57)*.0012056327)+.024));
    h_[Vi]       = 1.0/(1.0+exp((V+63.6)*.18867925));
    double temp = 1.0/(1.0+exp((V+35.1)*.3125));
    exptau_h1[Vi] = exp(tinc / (30.0*temp+.3));
    exptau_h2[Vi] = exp(tinc / (120.0*temp+3.0));

    dl_[Vi]        = 1.0/(1.0+exp(-(V+9.0)*.17241379));
    exptau_dl[Vi]  = exp(tinc / (2.7*exp(-(V+35.0)*(V+35.0)*.0011111111)+2.0));
    fl_[Vi]        = 1.0/(1.0+exp((V+27.4)*.14084507));
    temp           = (V+40.0)*(V+40.0);
    exptau_fl1[Vi] = exp(tinc / (161.0*exp(-temp*.0048225309)+10.0));
    exptau_fl2[Vi] = exp(tinc / (1332.3*exp(-temp*.0049593335)+62.6));

    r_[Vi]       = 1.0/(1.0+exp(-(V-1.0)*.090909091));
    exptau_r[Vi] = exp(tinc / (3.5*exp(-V*V*.0011111111)+1.5));
    s_[Vi]       = 1.0/(1.0+exp((V+40.5)*.086956522));
    exptau_s[Vi] = exp(tinc / (481.2*exp(-(V+52.45)*(V+52.45)*.0044622757)+14.14));

    n_[Vi]       = 1.0/(1.0+exp(-(V-19.9)*.078740157));
    exptau_n[Vi] = 1.0/(700.0+400.0*exp(-(V-20.0)*(V-20.0)*.0025));

    pa_[Vi]       = 1.0/(1.0+exp(-(V+15.0)*.16666667));
    exptau_pa[Vi] = exp(tinc / (31.18+217.18*exp(-(V+20.1376)*(V+20.1376)*.0020291292)));

    rsus_[Vi]       = 1.0/(1.0+exp(-(V+4.3)*.125));
    exptau_rsus[Vi] = exp(tinc / (9.0/(1.0+exp((V+5.0)*.083333333))+.5));
    ssus_[Vi]       = .4/(1.0+exp((V+20.0)*.1))+.6;
    exptau_ssus[Vi] = exp(tinc / (47.0/(1.0+exp((V+60.0)*.1))+300.0));

    pi[Vi] = 1.0/(1.0+exp((V+55.0)*.041666667));

    const double VFdRT = V*(P[VT_FdRT].value);
    C_Na[Vi]   = (P[VT_P_Na].value)*(P[VT_F].value)*VFdRT/(exp(VFdRT)-1.0);
    C_NaK[Vi]  = (P[VT_I_NaK_].value)*(V+150.0)/(V+200.0);
    C_NaCa[Vi] = (P[VT_K_NaCa].value)*exp((P[VT_Gamma].value)*VFdRT);
    dexpV[Vi]  = exp(-VFdRT);
  }
} // NygrenEtAlParameters::InitTable
