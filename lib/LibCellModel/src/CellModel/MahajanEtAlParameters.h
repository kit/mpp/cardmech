/* File: MahajanEtAlParameters.h
        automatically created by CellML2Elphymodel.pl
        Institute of Biomedical Engineering, Universität Karlsruhe (TH) */

#ifndef MAHAJANETALPARAMETERS_H
#define MAHAJANETALPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_MahajanEtAlParameters {
  enum varType {
    VT_R = vtFirst,
    VT_T,
    VT_F,
    VT_K_o,
    VT_Ca_o,
    VT_Na_o,
    VT_wca,
    VT_stim_offset,
    VT_stim_period,
    VT_stim_duration,
    VT_stim_amplitude,
    VT_gna,
    VT_gca,
    VT_pca,
    VT_vth,
    VT_s6,
    VT_vx,
    VT_sx,
    VT_vy,
    VT_sy,
    VT_vyr,
    VT_syr,
    VT_cat,
    VT_cpt,
    VT_k2,
    VT_k1t,
    VT_k2t,
    VT_r1,
    VT_r2,
    VT_s1t,
    VT_tca,
    VT_taupo,
    VT_tau3,
    VT_gkix,
    VT_gkr,
    VT_gks,
    VT_gtos,
    VT_gtof,
    VT_gNaK,
    VT_xkmko,
    VT_xkmnai,
    VT_gNaCa,
    VT_xkdna,
    VT_xmcao,
    VT_xmnao,
    VT_xmnai,
    VT_xmcai,
    VT_cstar,
    VT_gryr,
    VT_gbarsr,
    VT_gdyad,
    VT_ax,
    VT_ay,
    VT_av,
    VT_taua,
    VT_taur,
    VT_cup,
    VT_kj,
    VT_vup,
    VT_gleak,
    VT_bcal,
    VT_xkcal,
    VT_srmax,
    VT_srkd,
    VT_bmem,
    VT_kmem,
    VT_bsar,
    VT_ksar,
    VT_xkon,
    VT_xkoff,
    VT_btrop,
    VT_taud,
    VT_taups,
    VT_K_i,
    VT_prNaK,
    VT_FonRT,
    VT_s2t,
    VT_sigma,
    VT_bv,
    VT_ek,
    VT_V_init,
    VT_xm_init,
    VT_xh_init,
    VT_xj_init,
    VT_Ca_dyad_init,
    VT_c1_init,
    VT_c2_init,
    VT_xi1ca_init,
    VT_xi1ba_init,
    VT_xi2ca_init,
    VT_xi2ba_init,
    VT_xr_init,
    VT_Ca_i_init,
    VT_xs1_init,
    VT_xs2_init,
    VT_xtos_init,
    VT_ytos_init,
    VT_xtof_init,
    VT_ytof_init,
    VT_Na_i_init,
    VT_Ca_submem_init,
    VT_Ca_NSR_init,
    VT_Ca_JSR_init,
    VT_xir_init,
    VT_tropi_init,
    VT_trops_init,
    VT_cat3,
    VT_cptm4,
    VT_r1r2k2,
    VT_xkdna3,
    VT_xmnai3,
    vtLast
  };
} // namespace NS_MahajanEtAlParameters

using namespace NS_MahajanEtAlParameters;

class MahajanEtAlParameters : public vbNewElphyParameters {
public:
  MahajanEtAlParameters(const char *, ML_CalcType);

  ~MahajanEtAlParameters();

  void PrintParameters();

  void Calculate();

  void InitTable(ML_CalcType);

  void Init(const char *, ML_CalcType);

  ML_CalcType ah[RTDT];
  ML_CalcType bh[RTDT];
  ML_CalcType aj[RTDT];
  ML_CalcType bj[RTDT];
  ML_CalcType am[RTDT];
  ML_CalcType bm[RTDT];
  ML_CalcType xs1ss[RTDT];
  ML_CalcType tauxs1[RTDT];

  // ML_CalcType xkrv2[RTDT];
  ML_CalcType xkrinf[RTDT];

  // ML_CalcType rt1[RTDT];
  // ML_CalcType rt4[RTDT];
  // ML_CalcType poinf[RTDT];
  ML_CalcType recov[RTDT];
  ML_CalcType Pr[RTDT];
  ML_CalcType Ps[RTDT];

  // ML_CalcType poi[RTDT];
  // ML_CalcType rt2[RTDT];
  // ML_CalcType rt3[RTDT];
  // ML_CalcType rt5[RTDT];
  ML_CalcType za[RTDT];
  ML_CalcType sparkV[RTDT];
  ML_CalcType zw4[RTDT];
  ML_CalcType fNaK[RTDT];
  ML_CalcType aki[RTDT];
  ML_CalcType bki[RTDT];
  ML_CalcType rg[RTDT];
  ML_CalcType taukr[RTDT];
  ML_CalcType tauxs2[RTDT];
  ML_CalcType xtos_inf[RTDT];
  ML_CalcType txs[RTDT];
  ML_CalcType txf[RTDT];
  ML_CalcType alpha[RTDT];
  ML_CalcType beta[RTDT];
  ML_CalcType tauba[RTDT];
  ML_CalcType k3[RTDT];
  ML_CalcType ytos_inf[RTDT];
  ML_CalcType tys[RTDT];
  ML_CalcType tyf[RTDT];
  ML_CalcType xkin[RTDT];
  ML_CalcType rs_inf[RTDT];
  ML_CalcType comp[RTDT];
  ML_CalcType comp2[RTDT];
}; // class MahajanEtAlParameters

#endif // ifndef MAHAJANETALPARAMETERS_H
