/**@file Rice08.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <Rice08.h>

Rice08::Rice08(Rice08Parameters *pp) {
  r08p = pp;
  Init();
}

void Rice08::Init() {
#if KADEBUG
  cerr << "#initializing Class: Rice08 ... " << endl;
#endif // if KADEBUG
  SL = (CELLMODEL_PARAMVALUE(VT_SL_init));
  TRPNCaL = (CELLMODEL_PARAMVALUE(VT_TRPNCaL_init));
  TRPNCaH = (CELLMODEL_PARAMVALUE(VT_TRPNCaH_init));
  xXBpostr = (CELLMODEL_PARAMVALUE(VT_xXBpostr_init));
  xXBprer = (CELLMODEL_PARAMVALUE(VT_xXBprer_init));
  XBpostr = (CELLMODEL_PARAMVALUE(VT_XBpostr_init));
  XBprer = (CELLMODEL_PARAMVALUE(VT_XBprer_init));
  N_NoXB = (CELLMODEL_PARAMVALUE(VT_N_NoXB_init));
  P_NoXB = (CELLMODEL_PARAMVALUE(VT_P_NoXB_init));
  N = (CELLMODEL_PARAMVALUE(VT_N_init));
  intf = (CELLMODEL_PARAMVALUE(VT_intf_init));
  Tension = (CELLMODEL_PARAMVALUE(VT_Tension_init));
}

ML_CalcType Rice08::Calc(double tinc, ML_CalcType stretch, ML_CalcType velocity, ML_CalcType &Ca_i,
                         int euler = 1) {
  double HT = tinc * 1000;

  // calculating algebraic part
  double SLset = stretch * (CELLMODEL_PARAMVALUE(VT_SLrest));
  double preload = 0; // (stretch <= 1 ? (fabs(SLset - (CELLMODEL_PARAMVALUE(VT_SLrest)))/(SLset - (CELLMODEL_PARAMVALUE(VT_SLrest))))*(CELLMODEL_PARAMVALUE(VT_PCon_t))*(exp(
  // (CELLMODEL_PARAMVALUE(VT_PExp_t))*fabs(SLset - (CELLMODEL_PARAMVALUE(VT_SLrest)))) - 1.00000) : 0.0000);


  double sovr_ze = ((CELLMODEL_PARAMVALUE(VT_len_thick)) / 2.00000 < SL / 2.00000 ? (CELLMODEL_PARAMVALUE(VT_len_thick)) / 2.00000 : SL /
                                                                                               2.00000);
  double sovr_cle =
      (SL / 2.00000 - (SL - (CELLMODEL_PARAMVALUE(VT_len_thin))) >
       (CELLMODEL_PARAMVALUE(VT_len_hbare)) / 2.00000 ? SL / 2.00000 - (SL - (CELLMODEL_PARAMVALUE(VT_len_thin))) : (CELLMODEL_PARAMVALUE(VT_len_hbare)) /
                                                                              2.00000);
  double len_sovr = sovr_ze - sovr_cle;
  double SOVFThin = len_sovr / (CELLMODEL_PARAMVALUE(VT_len_thin));
  double Tropreg = (1.00000 - SOVFThin) * TRPNCaL + SOVFThin * TRPNCaH;
  double permtot = pow(fabs(1.00000 / (1.00000 + pow((CELLMODEL_PARAMVALUE(VT_perm50)) / Tropreg, (CELLMODEL_PARAMVALUE(VT_nperm))))),
                       1.0 / 2);
  double kn_pT = (CELLMODEL_PARAMVALUE(VT_kn_p)) * permtot * pow((CELLMODEL_PARAMVALUE(VT_Qkn_p)), ((CELLMODEL_PARAMVALUE(VT_TmpC)) - 37.0000) / 10.0000);
  double inprmt = (1.00000 / permtot < 100.000 ? 1.00000 / permtot : 100.000);
  double kp_nT = (CELLMODEL_PARAMVALUE(VT_kp_n)) * inprmt * pow((CELLMODEL_PARAMVALUE(VT_Qkp_n)), ((CELLMODEL_PARAMVALUE(VT_TmpC)) - 37.0000) / 10.0000);
  double SOVFThick = (len_sovr * 2.00000) / ((CELLMODEL_PARAMVALUE(VT_len_thick)) - (CELLMODEL_PARAMVALUE(VT_len_hbare)));
  double force = (CELLMODEL_PARAMVALUE(VT_kxb)) * SOVFThick * (xXBpostr * XBpostr + xXBprer * XBprer);
  double active = (1.00000 * force) / (CELLMODEL_PARAMVALUE(VT_Fnordv));
  double ppforce_t = ((SL - (CELLMODEL_PARAMVALUE(VT_SLrest))) / fabs(SL - (CELLMODEL_PARAMVALUE(VT_SLrest)))) * (CELLMODEL_PARAMVALUE(VT_PCon_t)) *
                     (exp((CELLMODEL_PARAMVALUE(VT_PExp_t)) * fabs(SL - (CELLMODEL_PARAMVALUE(VT_SLrest)))) - 1.00000);
  double ppforce_c =
      (SL > (CELLMODEL_PARAMVALUE(VT_SL_c)) ? (CELLMODEL_PARAMVALUE(VT_PCon_c)) *
                           (exp((CELLMODEL_PARAMVALUE(VT_PExp_c)) * fabs(SL - (CELLMODEL_PARAMVALUE(VT_SL_c)))) - 1.00000) : 0.00000);
  double ppforce = ppforce_t + ppforce_c;
  double afterload = 0; // ((CELLMODEL_PARAMVALUE(VT_SEon))==1.00000 ?  (CELLMODEL_PARAMVALUE(VT_KSE))*(SLset - SL) : 0.00000);
  double hfmd = exp(
      (-xXBprer / fabs(xXBprer)) * (CELLMODEL_PARAMVALUE(VT_hfmdc)) * pow(xXBprer / (CELLMODEL_PARAMVALUE(VT_x_0)), 2.00000));
  double hfT =
      (CELLMODEL_PARAMVALUE(VT_hf)) * hfmd * (CELLMODEL_PARAMVALUE(VT_xbmodsp)) * pow((CELLMODEL_PARAMVALUE(VT_Qhf)), ((CELLMODEL_PARAMVALUE(VT_TmpC)) - 37.0000) / 10.0000);
  double hbmd =
      exp(((xXBpostr - (CELLMODEL_PARAMVALUE(VT_x_0))) / fabs(xXBpostr - (CELLMODEL_PARAMVALUE(VT_x_0)))) * (CELLMODEL_PARAMVALUE(VT_hbmdc)) *
          pow((xXBpostr - (CELLMODEL_PARAMVALUE(VT_x_0))) / (CELLMODEL_PARAMVALUE(VT_x_0)), 2.00000));
  double hbT =
      (CELLMODEL_PARAMVALUE(VT_hb)) * hbmd * (CELLMODEL_PARAMVALUE(VT_xbmodsp)) * pow((CELLMODEL_PARAMVALUE(VT_Qhb)), ((CELLMODEL_PARAMVALUE(VT_TmpC)) - 37.0000) / 10.0000);
  double gxbmd =
      (xXBpostr <
       (CELLMODEL_PARAMVALUE(VT_x_0)) ? exp((CELLMODEL_PARAMVALUE(VT_sigmap)) *
                         pow(((CELLMODEL_PARAMVALUE(VT_x_0)) - xXBpostr) / (CELLMODEL_PARAMVALUE(VT_x_0)),
                             2.00000)) : exp(
          (CELLMODEL_PARAMVALUE(VT_sigman)) * pow((xXBpostr - (CELLMODEL_PARAMVALUE(VT_x_0))) / (CELLMODEL_PARAMVALUE(VT_x_0)), 2.00000)));
  double gxbT =
      (CELLMODEL_PARAMVALUE(VT_gxb)) * gxbmd * (CELLMODEL_PARAMVALUE(VT_xbmodsp)) * pow((CELLMODEL_PARAMVALUE(VT_Qgxb)), ((CELLMODEL_PARAMVALUE(VT_TmpC)) - 37.0000) / 10.0000);
  double dXBpostr = hfT * XBprer - (hbT * XBpostr + gxbT * XBpostr);
  double P = ((1.00000 - N) - XBprer) - XBpostr;
  double gapslmd = 1.00000 + (1.00000 - SOVFThick) * (CELLMODEL_PARAMVALUE(VT_gslmod));
  double gappT = (CELLMODEL_PARAMVALUE(VT_gapp)) * gapslmd * (CELLMODEL_PARAMVALUE(VT_xbmodsp)) *
                 pow((CELLMODEL_PARAMVALUE(VT_Qgapp)), ((CELLMODEL_PARAMVALUE(VT_TmpC)) - 37.0000) / 10.0000);
  double dXBprer = ((CELLMODEL_PARAMVALUE(VT_fappT)) * P + hbT * XBpostr) - (gappT * XBprer + hfT * XBprer);
  double dSL = 0; // (SL<=(CELLMODEL_PARAMVALUE(VT_SLmax))&&SL>(CELLMODEL_PARAMVALUE(VT_SLmin)) ? (intf+ (SLset - SL)*(CELLMODEL_PARAMVALUE(VT_visc)))/(CELLMODEL_PARAMVALUE(VT_massf)) :
  // 0.00000);
  double dutyprer = (hbT * (CELLMODEL_PARAMVALUE(VT_fappT)) + gxbT * (CELLMODEL_PARAMVALUE(VT_fappT))) /
                    ((CELLMODEL_PARAMVALUE(VT_fappT)) * hfT + gxbT * hfT + gxbT * gappT + hbT * (CELLMODEL_PARAMVALUE(VT_fappT)) +
                     hbT * gappT + gxbT * (CELLMODEL_PARAMVALUE(VT_fappT)));
  double dxXBprer = dSL / 2.00000 + ((CELLMODEL_PARAMVALUE(VT_xPsi)) / dutyprer) *
                                    ((CELLMODEL_PARAMVALUE(VT_fappT)) * -xXBprer +
                                     hbT * (xXBpostr - ((CELLMODEL_PARAMVALUE(VT_x_0)) + xXBprer)));
  double dutypostr = ((CELLMODEL_PARAMVALUE(VT_fappT)) * hfT) /
                     ((CELLMODEL_PARAMVALUE(VT_fappT)) * hfT + gxbT * hfT + gxbT * gappT + hbT * (CELLMODEL_PARAMVALUE(VT_fappT)) +
                      hbT * gappT + gxbT * (CELLMODEL_PARAMVALUE(VT_fappT)));
  double dxXBpostr =
      dSL / 2.00000 + ((CELLMODEL_PARAMVALUE(VT_xPsi)) / dutypostr) * hfT * ((xXBprer + (CELLMODEL_PARAMVALUE(VT_x_0))) - xXBpostr);
  double dTRPNCaL = (CELLMODEL_PARAMVALUE(VT_konT)) * Ca_i * (1.00000 - TRPNCaL) - (CELLMODEL_PARAMVALUE(VT_koffLT)) * TRPNCaL;
  double dTRPNCaH = (CELLMODEL_PARAMVALUE(VT_konT)) * Ca_i * (1.00000 - TRPNCaH) - (CELLMODEL_PARAMVALUE(VT_koffHT)) * TRPNCaH;

  // std::cout << preload << " " << afterload << std::endl;


  double FrSBXB = (XBpostr + XBprer) / ((CELLMODEL_PARAMVALUE(VT_SSXBpostr)) + (CELLMODEL_PARAMVALUE(VT_SSXBprer)));
  double dFrSBXB = (dXBpostr + dXBprer) / ((CELLMODEL_PARAMVALUE(VT_SSXBpostr)) + (CELLMODEL_PARAMVALUE(VT_SSXBprer)));
  double TropTot = (CELLMODEL_PARAMVALUE(VT_Trop_conc)) *
                   ((1.00000 - SOVFThin) * TRPNCaL +
                    SOVFThin * (FrSBXB * TRPNCaH + (1.00000 - FrSBXB) * TRPNCaL));
  double dsovr_ze = (SL < (CELLMODEL_PARAMVALUE(VT_len_thick)) ? -0.500000 * dSL : 0.00000);
  double dsovr_cle = (2.00000 * (CELLMODEL_PARAMVALUE(VT_len_thin)) - SL > (CELLMODEL_PARAMVALUE(VT_len_hbare)) ? -0.500000 * dSL
                                                                          : 0.00000);
  double dlen_sovr = dsovr_ze - dsovr_cle;
  double dSOVFThin = dlen_sovr / (CELLMODEL_PARAMVALUE(VT_len_thin));
  double dSOVFThick = (2.00000 * dlen_sovr) / ((CELLMODEL_PARAMVALUE(VT_len_thick)) - (CELLMODEL_PARAMVALUE(VT_len_hbare)));

  double dforce =
      (CELLMODEL_PARAMVALUE(VT_kxb)) * dSOVFThick * (xXBpostr * XBpostr + xXBprer * XBprer) + (CELLMODEL_PARAMVALUE(VT_kxb)) * SOVFThick *
                                                                           (dxXBpostr * XBpostr +
                                                                            xXBpostr * dXBpostr +
                                                                            dxXBprer * XBprer +
                                                                            xXBprer * dXBprer);

  double dTropTot = (CELLMODEL_PARAMVALUE(VT_Trop_conc)) *
                    (-dSOVFThin * TRPNCaL + (1.00000 - SOVFThin) * dTRPNCaL +
                     dSOVFThin * (FrSBXB * TRPNCaH + (1.00000 - FrSBXB) * TRPNCaL) +
                     SOVFThin *
                     ((dFrSBXB * TRPNCaH + FrSBXB * dTRPNCaH + (1.00000 - FrSBXB) * dTRPNCaL) -
                      dFrSBXB * TRPNCaL));

  Ca_i -= dTropTot;

  // calculating rates part

  N_NoXB += HT * (kp_nT * P_NoXB - kn_pT * N_NoXB);
  P_NoXB += HT * (kn_pT * N_NoXB - kp_nT * P_NoXB);
  intf += HT * ((preload + afterload) - (ppforce + active));
  XBpostr += HT * (dXBpostr);
  N += HT * (kp_nT * P - kn_pT * N);
  XBprer += HT * (dXBprer);
  SL = SLset; // += HT*( dSL);
  xXBprer += HT * (dxXBprer);
  xXBpostr += HT * (dxXBpostr);
  TRPNCaL += HT * (dTRPNCaL);
  TRPNCaH += HT * (dTRPNCaH);
  Tension += dforce;


  return Tension / (CELLMODEL_PARAMVALUE(VT_Fmax));
} // Rice08::Calc

void Rice08::Print(ostream &tempstr) {
  tempstr << ' ' << SL << ' ' << TRPNCaL << ' ' << TRPNCaH << ' ' << xXBpostr << ' ' << xXBprer
          << ' ' << XBpostr << ' ' << XBprer << ' ' <<
          N_NoXB << ' ' << P_NoXB
          << ' ' << N << ' ' << intf;
}

void Rice08::GetParameterNames(vector<string> &getpara) {
  const int numpara = 11;
  const string ParaNames[numpara] =
      {"SL", "TRPNCaL", "TRPNCaH", "xXBpostr", "xXBprer", "XBpostr", "XBprer", "N_NoXB", "P_NoXB",
       "N", "intf"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}
