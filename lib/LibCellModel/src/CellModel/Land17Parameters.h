// created by Robin Moss

// based on the available Matlab code from cemrg.co.uk

#ifndef LAND17PARAMETERS_H
#define LAND17PARAMETERS_H

#include <ParameterLoader.h>

namespace NS_Land17Parameters {
  enum varType {
    VT_perm50 = vtFirst,
    VT_TRPN_n,
    VT_koff,
    VT_dr,
    VT_wfrac,
    VT_TOT_A,
    VT_ktm_unblock,
    VT_beta_1,
    VT_beta_0,
    VT_gamma,
    VT_gamma_wu,
    VT_phi,
    VT_par_k,
    VT_b,
    VT_eta_l,
    VT_eta_s,
    VT_a,
    VT_skinned,
    VT_passive_contribution,
    VT_nperm_skinned,
    VT_ca50_skinned,
    VT_Tref_skinned,
    VT_nu_skinned,
    VT_mu_skinned,
    VT_nperm_intact,
    VT_ca50_intact,
    VT_Tref_intact,
    VT_nu_intact,
    VT_mu_intact,
    VT_k_ws,
    VT_k_uw,
    VT_cdw,
    VT_cds,
    VT_k_wu,
    VT_k_su,
    VT_A,
    VT_nperm,
    VT_ca50,
    VT_Tref,
    VT_nu,
    VT_mu,
    VT_XS_init,
    VT_XW_init,
    VT_CaTRPN_init,
    VT_B_init,
    VT_ZETAS_init,
    VT_ZETAW_init,
    VT_Cd_init,
    vtLast
  };
}  // namespace NS_Land17Parameters

using namespace NS_Land17Parameters;

class Land17Parameters : public vbNewForceParameters {
public:
  Land17Parameters(const char *);

  ~Land17Parameters() {}

  void Init(const char *);

  void Calculate();

  void PrintParameters();
};

#endif  // ifndef LAND17PARAMETERS_H
