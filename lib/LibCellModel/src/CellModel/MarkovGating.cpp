/*! \file MarkovGating.h
   \brief Implementation of Markov model with gating current calculation
   (based on Hille, Ion Channels of Excitable Membranes)

   \author fs, CVRTI - University of Utah, USA
 */

#include <MarkovGating.h>

MarkovGating::MarkovGating(MarkovGatingParameters *pIMWArg) {
  pIMW = pIMWArg;
#ifdef HETERO
  PS = new ParameterSwitch(pIMW, NS_MarkovGatingParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

#ifdef HETERO

inline bool MarkovGating::AddHeteroValue(string desc, double val) {
  Parameter EP(desc, val);

  return PS->addDynamicParameter(EP);
}

#else // ifdef HETERO

inline bool MarkovGating::AddHeteroValue(string desc, double val) {
  throw kaBaseException("compile with HETERO to use this feature!\n");
}

#endif // ifdef HETERO

inline int MarkovGating::GetSize(void) {
  return sizeof(MarkovGating) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(MarkovGatingParameters *)
#ifdef HETERO
    -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
      ;
}

void MarkovGating::Init() {
  A = CELLMODEL_PARAMVALUE(VT_A);
  B = CELLMODEL_PARAMVALUE(VT_B);
  C = CELLMODEL_PARAMVALUE(VT_C);
}

void MarkovGating::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' '  // 1
          << V << ' '
          << A << ' '
          << B << ' '
          << C << ' ';
}

void MarkovGating::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);
  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);
  const double I_A = CELLMODEL_PARAMVALUE(VT_G) * B * (V - CELLMODEL_PARAMVALUE(VT_E));
  tempstr << ' ' << I_A;  // 6

  const double Eq_kT = V * ElphyModelConstants::qe / (ElphyModelConstants::k * CELLMODEL_PARAMVALUE(VT_Tx));
  const double KAB = CELLMODEL_PARAMVALUE(VT_KABa) * exp(CELLMODEL_PARAMVALUE(VT_zgAB) * CELLMODEL_PARAMVALUE(VT_KABz) * Eq_kT);
  const double KBA = CELLMODEL_PARAMVALUE(VT_KBAa) * exp(CELLMODEL_PARAMVALUE(VT_zgAB) * -CELLMODEL_PARAMVALUE(VT_KBAz) * Eq_kT);

  const double I_Gating = CELLMODEL_PARAMVALUE(VT_zgAB) * ElphyModelConstants::qe * (KAB * A - KBA * B);
  tempstr << ' ' << I_Gating;  // 7
}

void MarkovGating::GetParameterNames(vector<string> &getpara) {
  const char *ParaNames[] = {"A", "B", "C"};

  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

void MarkovGating::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const char *ParaNames[] = {"I_B", "I_Gating"};
  for (int i = 0; i < sizeof(ParaNames) / sizeof(ParaNames[0]); i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType
MarkovGating::Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim = .0, ML_CalcType stretch = 1.,
                   int euler = 1) {
  const double Eq_kT = V * ElphyModelConstants::qe / (ElphyModelConstants::k * CELLMODEL_PARAMVALUE(VT_Tx));

  const double KAB = CELLMODEL_PARAMVALUE(VT_KABa) * exp(CELLMODEL_PARAMVALUE(VT_zgAB) * CELLMODEL_PARAMVALUE(VT_KABz) * Eq_kT);
  const double KBA = CELLMODEL_PARAMVALUE(VT_KBAa) * exp(CELLMODEL_PARAMVALUE(VT_zgAB) * -CELLMODEL_PARAMVALUE(VT_KBAz) * Eq_kT);
  const double KBC = CELLMODEL_PARAMVALUE(VT_KBCa) * exp(CELLMODEL_PARAMVALUE(VT_zgBC) * CELLMODEL_PARAMVALUE(VT_KBCz) * Eq_kT);
  const double KCB = CELLMODEL_PARAMVALUE(VT_KCBa) * exp(CELLMODEL_PARAMVALUE(VT_zgBC) * -CELLMODEL_PARAMVALUE(VT_KCBz) * Eq_kT);

  const double dBA = (-(KBA + KBC) * B + KAB * A + KCB * C);

  B += tinc * dBA;
  if (B > 1.)
    B = 1.;
  else if (B < 0)
    B = 0.;

  const double dBC = (-KCB * C + KBC * B);
  C += tinc * dBC;
  if (C > 1.)
    C = 1.;
  else if (C < 0)
    C = 0.;

  A = 1. - B - C;
  if (A > 1.)
    A = 1.;
  else if (A < 0)
    A = 0.;

  return 0.;
} // MarkovGating::Calc

void MarkovGating::GetStatus(double *p) const {
  p[0] = A;
  p[1] = B;
  p[2] = C;
}

void MarkovGating::SetStatus(const double *p) {
  A = p[0];
  B = p[1];
  C = p[2];
}
