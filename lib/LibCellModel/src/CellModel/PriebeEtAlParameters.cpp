/**@file PriebeEtAlParameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <PriebeEtAlParameters.h>

PriebeEtAlParameters::PriebeEtAlParameters(const char *initFile, ElphyModelType emt, ML_CalcType tinc) {
#if KADEBUG
  cerr << "PriebeEtAlParameters::PriebeEtAlParameters "<< initFile << endl;
#endif // if KADEBUG
  P = new Parameter[vtLast];
  Init(initFile, emt, tinc);
}

void PriebeEtAlParameters::PrintParameters() {
  // print the parameter to the stdout
  cout<<"PriebeEtAlParameters:"<<endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void PriebeEtAlParameters::Init(const char *initFile, ElphyModelType emtArg, ML_CalcType tinc) {
#if KADEBUG
  cerr << "#Loading the PriebeEtAl parameter from " << initFile << " ...\n";
#endif // if KADEBUG
  emt = emtArg;
  switch (emt) {
    case EMT_PriebeEtAl:
    case EMT_PriebeEtAlCa:
      P[VT_K_mCaAct_Weber].readFromFile = false;
      P[VT_K_mCao_Weber].readFromFile   = false;
      P[VT_K_mCai_Weber].readFromFile   = false;
      P[VT_K_mNao_Weber].readFromFile   = false;
      P[VT_K_mNai_Weber].readFromFile   = false;
      P[VT_k_NaCa].readFromFile         = false;
      break;
    case EMT_PriebeWeber:
    case EMT_PriebeWeberCa:
      break;
    default:
      throw kaBaseException("PriebeEtAlParameters::Wrong ElphyModelType!");
      break;
  }

  P[VT_R].name      = "R";
  P[VT_Tx].name     = "T";
  P[VT_F].name      = "F";
  P[VT_RTdF].name   = "RTdF";
  P[VT_FdRT].name   = "FdRT";
  P[VT_RTd2F].name  = "RTd2F";
  P[VT_F2dRT].name  = "F2dRT";
  P[VT_w2p].name    = "w2p";
  P[VT_K_Ca].name   = "K_Ca";
  P[VT_K_mNai].name = "K_mNai";
  P[VT_K_mKo].name  = "K_mKo";

  P[VT_k_sat].name  = "k_sat";
  P[VT_eta].name    = "eta";
  P[VT_k_NaCa].name = "k_NaCa";

  P[VT_K_mNa].name = "K_mNa";
  P[VT_K_mCa].name = "K_mCa";

  P[VT_K_mCaAct_Weber].name = "K_mCaAct_Weber";
  P[VT_K_mCao_Weber].name   = "K_mCao_Weber";
  P[VT_K_mCai_Weber].name   = "K_mCai_Weber";
  P[VT_K_mNao_Weber].name   = "K_mNao_Weber";
  P[VT_K_mNai_Weber].name   = "K_mNai_Weber";

  P[VT_K_mNaih3].name = "K_mNaih3";
  P[VT_K_mNaoh3].name = "K_mNaoh3";

  P[VT_k_mup].name     = "k_mup";
  P[VT_k_leak].name    = "k_leak";
  P[VT_I_upmax].name   = "I_upmax";
  P[VT_g_Na].name      = "g_Na";
  P[VT_g_Ca].name      = "g_Ca";
  P[VT_g_Ks].name      = "g_Ks";
  P[VT_g_Kr].name      = "g_Kr";
  P[VT_g_K1].name      = "g_K1";
  P[VT_g_Cab].name     = "g_Cab";
  P[VT_g_Nab].name     = "g_Nab";
  P[VT_g_to].name      = "g_to";
  P[VT_g_NaK].name     = "g_NaK";
  P[VT_g_NaCa].name    = "g_NaCa";
  P[VT_C_Ca].name      = "C_Ca";
  P[VT_t_on].name      = "t_on";
  P[VT_dt_on].name     = "dt_on";
  P[VT_t_tr].name      = "t_tr";
  P[VT_dt_tr].name     = "dt_tr";
  P[VT_Na_i].name      = "Na_i";
  P[VT_Na_o].name      = "Na_o";
  P[VT_K_i].name       = "K_i";
  P[VT_K_o].name       = "K_o";
  P[VT_Ca_o].name      = "Ca_o";
  P[VT_Ca_im].name     = "Ca_im";
  P[VT_Naoh3].name     = "Naoh3";
  P[VT_Naih3].name     = "Naih3";
  P[VT_KNN].name       = "KNN";
  P[VT_cNaK].name      = "cNaK";
  P[VT_dCaith].name    = "dCaith";
  P[VT_sigm].name      = "sigm";
  P[VT_E_Na].name      = "E_Na";
  P[VT_E_to].name      = "E_to";
  P[VT_E_Ks].name      = "E_Ks";
  P[VT_E_K].name       = "E_K";
  P[VT_radius].name    = "radius";
  P[VT_lenght].name    = "lenght";
  P[VT_Vol].name       = "Vol";
  P[VT_V_cell].name    = "V_cell";
  P[VT_A_Geo].name     = "A_Geo";
  P[VT_R_CG].name      = "R_CG";
  P[VT_A_Cap].name     = "A_Cap";
  P[VT_V_myo].name     = "V_myo";
  P[VT_A_Capd2Fm].name = "A_Capd2Fm";
  P[VT_V_NSR].name     = "V_NSR";
  P[VT_VNdm].name      = "VNdm";
  P[VT_V_JSR].name     = "V_JSR";
  P[VT_VJdm].name      = "VJdm";
  P[VT_VJdVN].name     = "VJdVN";
  P[VT_TRPN_max].name  = "TRPN_max";
  P[VT_k_mTRPN].name   = "k_mTRPN";

  // Initialisierung
  P[VT_init_Ca_itot].name     = "init_Ca_itot";
  P[VT_init_Ca_JSRtot].name   = "init_Ca_JSRtot";
  P[VT_init_Ca_NSR].name      = "init_Ca_NSR";
  P[VT_init_Ca_itotVmax].name = "init_Ca_itotVmax";
  P[VT_init_m].name           = "init_m";
  P[VT_init_h].name           = "init_h";
  P[VT_init_j].name           = "init_j";
  P[VT_init_d].name           = "init_d";
  P[VT_init_f].name           = "init_f";
  P[VT_init_r].name           = "init_r";
  P[VT_init_t].name           = "init_t";
  P[VT_init_Xs].name          = "init_Xs";
  P[VT_init_Xr].name          = "init_Xr";
  P[VT_init_dCa_i2].name      = "init_dCa_i2";
  P[VT_init_t_CICR].name      = "init_t_CICR";
  P[VT_init_t_Vmax].name      = "init_t_Vmax";
  P[VT_init_CICR].name        = "init_CICR";

  P[VT_a_r1].name = "a_r1";
  P[VT_a_r2].name = "a_r2";
  P[VT_a_r3].name = "a_r3";
  P[VT_a_r4].name = "a_r4";
  P[VT_a_r5].name = "a_r5";

  P[VT_b_r1].name = "b_r1";
  P[VT_b_r2].name = "b_r2";
  P[VT_b_r3].name = "b_r3";
  P[VT_b_r4].name = "b_r4";
  P[VT_b_r5].name = "b_r5";
  P[VT_b_r6].name = "b_r6";

  P[VT_a_t1].name = "a_t1";
  P[VT_a_t2].name = "a_t2";
  P[VT_a_t3].name = "a_t3";
  P[VT_a_t4].name = "a_t4";
  P[VT_a_t5].name = "a_t5";
  P[VT_a_t6].name = "a_t6";

  P[VT_a_Xs1].name = "a_Xs1";
  P[VT_a_Xs2].name = "a_Xs2";
  P[VT_a_Xs3].name = "a_Xs3";
  P[VT_a_Xs4].name = "a_Xs4";
  P[VT_a_Xs5].name = "a_Xs5";

  P[VT_b_Xs1].name = "b_Xs1";
  P[VT_b_Xs2].name = "b_Xs2";
  P[VT_b_Xs3].name = "b_Xs3";
  P[VT_b_Xs4].name = "b_Xs4";
  P[VT_b_Xs5].name = "b_Xs5";

  P[VT_C_IKr1].name = "C_IKr1";
  P[VT_C_IKr2].name = "C_IKr2";

  P[VT_a_Xr1].name = "a_Xr1";
  P[VT_a_Xr2].name = "a_Xr2";
  P[VT_a_Xr3].name = "a_Xr3";
  P[VT_a_Xr4].name = "a_Xr4";
  P[VT_a_Xr5].name = "a_Xr5";

  P[VT_b_Xr1].name = "b_Xr1";
  P[VT_b_Xr2].name = "b_Xr2";
  P[VT_b_Xr3].name = "b_Xr3";
  P[VT_b_Xr4].name = "b_Xr4";
  P[VT_b_Xr5].name = "b_Xr5";

  P[VT_b_j1].name = "b_j1";
  P[VT_b_j2].name = "b_j2";
  P[VT_b_j3].name = "b_j3";
  P[VT_b_j4].name = "b_j4";
  P[VT_b_j5].name = "b_j5";

  P[VT_a_K1_1].name = "a_K1_1";
  P[VT_a_K1_2].name = "a_K1_2";
  P[VT_a_K1_3].name = "a_K1_3";
  P[VT_a_K1_4].name = "a_K1_4";

  P[VT_b_K1_1].name = "b_K1_1";
  P[VT_b_K1_2].name = "b_K1_2";
  P[VT_b_K1_3].name = "b_K1_3";
  P[VT_b_K1_4].name = "b_K1_4";
  P[VT_b_K1_5].name = "b_K1_5";
  P[VT_b_K1_6].name = "b_K1_6";
  P[VT_b_K1_7].name = "b_K1_7";
  P[VT_b_K1_8].name = "b_K1_8";

  P[VT_d_E_K].name = "d_E_K";

  P[VT_b_t1].name = "b_t1";
  P[VT_b_t2].name = "b_t2";
  P[VT_b_t3].name = "b_t3";
  P[VT_b_t4].name = "b_t4";
  P[VT_b_t5].name = "b_t5";
  P[VT_b_t6].name = "b_t6";

  P[VT_init_Vm].name = "init_Vm";
  P[VT_Amp].name     = "Amp";

  P[VT_SQT_fraction].name = "SQT_fraction";

  P[VT_g_Kr_SQT].name   = "g_Kr_SQT";
  P[VT_C_IKr2_SQT].name = "C_IKr2_SQT";
  P[VT_a_Xr1_SQT].name  = "a_Xr1_SQT";
  P[VT_a_Xr3_SQT].name  = "a_Xr3_SQT";
  P[VT_b_Xr1_SQT].name  = "b_Xr1_SQT";
  P[VT_b_Xr3_SQT].name  = "b_Xr3_SQT";

  P[VT_C_IKr3_SQT_o].name = "C_IKr3_SQT_o";
  P[VT_C_IKr3_SQT_m].name = "C_IKr3_SQT_m";
  P[VT_C_IKr3_SQT_b].name = "C_IKr3_SQT_b";
  P[VT_C_IKr4_SQT_o].name = "C_IKr4_SQT_o";
  P[VT_C_IKr4_SQT_m].name = "C_IKr4_SQT_m";
  P[VT_C_IKr4_SQT_b].name = "C_IKr4_SQT_b";

  P[VT_RTdF].readFromFile      = false;
  P[VT_FdRT].readFromFile      = false;
  P[VT_RTd2F].readFromFile     = false;
  P[VT_F2dRT].readFromFile     = false;
  P[VT_w2p].readFromFile       = false;
  P[VT_K_mNaih3].readFromFile  = false;
  P[VT_K_mNaoh3].readFromFile  = false;
  P[VT_C_Ca].readFromFile      = false;
  P[VT_dt_on].readFromFile     = false;
  P[VT_dt_tr].readFromFile     = false;
  P[VT_Naoh3].readFromFile     = false;
  P[VT_Naih3].readFromFile     = false;
  P[VT_KNN].readFromFile       = false;
  P[VT_cNaK].readFromFile      = false;
  P[VT_sigm].readFromFile      = false;
  P[VT_E_Na].readFromFile      = false;
  P[VT_E_to].readFromFile      = false;
  P[VT_E_Ks].readFromFile      = false;
  P[VT_E_K].readFromFile       = false;
  P[VT_Vol].readFromFile       = false;
  P[VT_V_cell].readFromFile    = false;
  P[VT_A_Cap].readFromFile     = false;
  P[VT_A_Capd2Fm].readFromFile = false;
  P[VT_VNdm].readFromFile      = false;
  P[VT_VJdm].readFromFile      = false;
  P[VT_VJdVN].readFromFile     = false;

  ParameterLoader EPL(initFile, emt);
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = EPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
  InitTable(tinc);
} // PriebeEtAlParameters::Init

void PriebeEtAlParameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();
#if KADEBUG
  cerr << "#PriebeEtAlParameters - Calculate ..." << endl;
#endif // if KADEBUG
  P[VT_RTdF].value  = P[VT_R].value*(P[VT_Tx].value)/(P[VT_F].value);
  P[VT_FdRT].value  = 1.0/(P[VT_RTdF].value);
  P[VT_RTd2F].value = P[VT_RTdF].value*.5;
  P[VT_F2dRT].value = 1.0/(P[VT_RTd2F].value);
  P[VT_w2p].value   = sqrt(2.0*M_PI);
  if ((emt == EMT_PriebeEtAl) || (emt == EMT_PriebeEtAlCa) ) {
    P[VT_K_mNaih3].value = P[VT_K_mNa].value*(P[VT_K_mNa].value)*(P[VT_K_mNa].value);
    P[VT_K_mNaoh3].value = 0;
  }

  if ((emt == EMT_PriebeWeber) || (emt == EMT_PriebeWeberCa) ) {
    P[VT_K_mNaih3].value = P[VT_K_mNai_Weber].value*(P[VT_K_mNai_Weber].value)*(P[VT_K_mNai_Weber].value);
    P[VT_K_mNaoh3].value = P[VT_K_mNao_Weber].value*(P[VT_K_mNao_Weber].value)*(P[VT_K_mNao_Weber].value);
  }

  P[VT_C_Ca].value  = P[VT_g_Ca].value*(P[VT_K_Ca].value);
  P[VT_dt_on].value = 1/(P[VT_t_on].value);
  P[VT_dt_tr].value = 1/(P[VT_t_tr].value);
  P[VT_Naoh3].value = P[VT_Na_o].value*(P[VT_Na_o].value)*(P[VT_Na_o].value);
  P[VT_Naih3].value = P[VT_Na_i].value*(P[VT_Na_i].value)*(P[VT_Na_i].value);
  P[VT_KNN].value   = P[VT_K_mNai].value/(P[VT_Na_i].value);
  P[VT_cNaK].value  = P[VT_g_NaK].value/(1.0+sqrt(P[VT_KNN].value*(P[VT_KNN].value)*(P[VT_KNN].value)))*
    (P[VT_K_o].value)/(P[VT_K_o].value+(P[VT_K_mKo].value));
  P[VT_sigm].value = (exp(P[VT_Na_o].value*0.014858841)-1.0)*.14285714;
  P[VT_E_Na].value = P[VT_RTdF].value*log(P[VT_Na_o].value/(P[VT_Na_i].value));
  P[VT_E_to].value = P[VT_RTdF].value*
    log((.043*(P[VT_Na_o].value)+(P[VT_K_o].value))/(.043*(P[VT_Na_i].value)+(P[VT_K_i].value)));
  P[VT_E_Ks].value = P[VT_RTdF].value*
    log((.01833*(P[VT_Na_o].value)+(P[VT_K_o].value))/(.01833*(P[VT_Na_i].value)+(P[VT_K_i].value)));
  P[VT_E_K].value       = P[VT_RTdF].value*log(P[VT_K_o].value/(P[VT_K_i].value));
  P[VT_Vol].value       = M_PI*(P[VT_radius].value)*(P[VT_radius].value)*(P[VT_lenght].value);
  P[VT_V_cell].value    = 1000.0*(P[VT_Vol].value)*1e9;
  P[VT_A_Cap].value     = P[VT_R_CG].value*(P[VT_A_Geo].value);
  P[VT_A_Capd2Fm].value = P[VT_A_Cap].value/(P[VT_F].value*(P[VT_V_myo].value))*.5;
  P[VT_VNdm].value      = P[VT_V_NSR].value/(P[VT_V_myo].value);
  P[VT_VJdm].value      = P[VT_V_JSR].value/(P[VT_V_myo].value);
  P[VT_VJdVN].value     = P[VT_V_JSR].value/(P[VT_V_NSR].value);
} // PriebeEtAlParameters::Calculate

void PriebeEtAlParameters::InitTable(ML_CalcType tinc) {
  tinc *= -1000.0;  // sec->ms, neg for exptau calculation
  double a, b;
  for (double V = -RangeTabhalf+.0001; V < RangeTabhalf; V += dDivisionTab) {
    int Vi = (int)(DivisionTab*(RangeTabhalf+V)+.5);

    if (P[VT_SQT_fraction].value > 0) {
      a = (P[VT_a_Xr1_SQT].value*exp(P[VT_a_Xr2].value*(V+(P[VT_a_Xr3_SQT].value))))/
        (P[VT_a_Xr4].value+exp(P[VT_a_Xr5].value*(V+(P[VT_a_Xr3_SQT].value))));
      b = a+
        ((P[VT_b_Xr1_SQT].value*exp(P[VT_b_Xr2].value*(V+(P[VT_b_Xr3_SQT].value))))/
         (P[VT_b_Xr4].value+exp(P[VT_b_Xr5].value*(V+(P[VT_b_Xr3_SQT].value)))));
      Xr_SQT_inf[Vi]    = 0; // a/b
      exptau_Xr_SQT[Vi] = exp(tinc * b);
    }

    if (V >= -40.0) {
      a            = .0;
      b            = 1.0/(.13*(1.0+exp(-(V+10.66)*.09009009)));
      h_inf[Vi]    = 0; // a/b
      exptau_h[Vi] = exp(tinc * b);
      b            = P[VT_b_j1].value*exp((P[VT_b_j2].value)*V)/
        ((P[VT_b_j3].value)+exp((P[VT_b_j4].value)*(V+(P[VT_b_j5].value))));
      j_inf[Vi]    = 0;
      exptau_j[Vi] = exp(tinc * b);
    } else {
      a            = .135*exp(-(V+80.0)*.14705882);
      b            = a+(3.56*exp(.079*V)+310000.0*exp(.35*V));
      h_inf[Vi]    = a / b;
      exptau_h[Vi] = exp(tinc * b);
      a            = (-127140.0*exp(.2444*V)-.00003474*exp(-.04391*V))
        *((V+37.78)/(1.0+exp(.311*(V+79.23))));
      b            = a+(.1212*exp(-.01052*V)/(1.0+exp(-.1378*(V+40.14))));
      j_inf[Vi]    = a / b;
      exptau_j[Vi] = exp(tinc * b);
    }
    a            = 14.98/(16.6813*(P[VT_w2p].value))*exp(-(V-22.36)*(V-22.36)*.0017968434);
    b            = a+(0.1471-5.3/(14.93*(P[VT_w2p].value))*exp(-(V-6.2744)*(V-6.2744)*.0022431091));
    d_inf[Vi]    = a / b;
    exptau_d[Vi] = exp(tinc * b);

    a            = .006872/(1.0+exp(-(6.1546-V)*.16333731));
    b            = a+((.0687*exp(-.1081*(V+9.8255))+.0112)/(1.0+exp(-.2779*(V+9.8255)))+.0005474); // corrected f)s
    f_inf[Vi]    = a / b;
    exptau_f[Vi] = exp(tinc * b);

    a = (P[VT_a_r1].value*exp(P[VT_a_r2].value*(V+(P[VT_a_r3].value))))/
      (P[VT_a_r4].value+exp(P[VT_a_r5].value*(V+(P[VT_a_r3].value))));
    b = a+
      ((P[VT_b_r1].value*exp(P[VT_b_r2].value*(V+(P[VT_b_r3].value)))+(P[VT_b_r4].value)*V)/
       (P[VT_b_r5].value+exp(P[VT_b_r6].value*(V-(P[VT_b_r4].value)))));
    r_inf[Vi]    = a / b;
    exptau_r[Vi] = exp(tinc * b);

    a = (P[VT_a_t1].value*exp(P[VT_a_t2].value*(V+(P[VT_a_t3].value)))+(P[VT_a_t4].value)*V)/
      (P[VT_a_t5].value+exp((P[VT_a_t6].value)*(V+(P[VT_a_t3].value))));
    b = a+
      ((P[VT_b_t1].value*exp(P[VT_b_t2].value*(V+(P[VT_b_t3].value)))+(P[VT_b_t4].value)*V)/
       (P[VT_b_t5].value+exp((P[VT_b_t6].value)*(V+(P[VT_b_t3].value)))));
    t_inf[Vi]    = a / b;
    exptau_t[Vi] = exp(tinc * b);

    a = P[VT_a_Xs1].value/
      (P[VT_a_Xs2].value+exp((P[VT_a_Xs3].value-(V+(P[VT_a_Xs4].value)))*(P[VT_a_Xs5].value)));
    b = a+
      (P[VT_b_Xs1].value/(P[VT_b_Xs2].value+exp((P[VT_b_Xs3].value+(V+(P[VT_b_Xs4].value)))*(P[VT_b_Xs5].value))));
    Xs_inf[Vi]    = a / b;
    exptau_Xs[Vi] = exp(tinc * b);

    a = (P[VT_a_Xr1].value*exp(P[VT_a_Xr2].value*(V+(P[VT_a_Xr3].value))))/
      (P[VT_a_Xr4].value+exp(P[VT_a_Xr5].value*(V+(P[VT_a_Xr3].value))));
    b = a+
      ((P[VT_b_Xr1].value*exp(P[VT_b_Xr2].value*(V+(P[VT_b_Xr3].value))))/
       (P[VT_b_Xr4].value+exp(P[VT_b_Xr5].value*(V+(P[VT_b_Xr3].value)))));
    Xr_inf[Vi]    = a / b;
    exptau_Xr[Vi] = exp(tinc * b);

    if ((fabs(V+47.13)) > .001)
      a = .32*(V+47.13)/(1.0-exp(-.1*(V+47.13)));
    else
      a = 3.2;
    const double b = a+.08*exp(-V*.090909091);
    exptau_m[Vi] = exp(tinc * b);
    m_[Vi]       = a/b;

    const double VFdRT = V*(P[VT_FdRT].value);
    const double fNaK  = 1.0/(1.0+.1245*exp(-.1*VFdRT)+.0365*(P[VT_sigm].value)*exp(-VFdRT));
    const double aK1   = P[VT_a_K1_1].value/
      (P[VT_a_K1_2].value+exp(P[VT_a_K1_3].value*(V-(P[VT_E_K].value+(P[VT_d_E_K].value))+(P[VT_a_K1_4].value))));
    const double bK1 =
      (P[VT_b_K1_1].value*exp(P[VT_b_K1_2].value*(V-(P[VT_E_K].value+(P[VT_d_E_K].value))+P[VT_b_K1_3].value))+
       exp(P[VT_b_K1_4].value*(V-(P[VT_E_K].value+(P[VT_d_E_K].value))+P[VT_b_K1_5].value)))/
      (P[VT_b_K1_6].value+exp(P[VT_b_K1_7].value*(V-(P[VT_E_K].value+(P[VT_d_E_K].value)+(P[VT_b_K1_8].value)))));
    C_IconstV[Vi] = (P[VT_g_K1].value)*aK1/(aK1+bK1)*(V-(P[VT_E_K].value+(P[VT_d_E_K].value)))+(P[VT_g_Nab].value)*
      (V-(P[VT_E_Na].value))+fNaK*(P[VT_cNaK].value);

    if ((emt == EMT_PriebeEtAl) || (emt == EMT_PriebeEtAlCa) ) {
      const double etaV  = exp(((P[VT_eta].value)-1.0)*VFdRT);
      const double fNaCa = (P[VT_g_NaCa].value)/
        (((P[VT_K_mNaih3].value)+(P[VT_Naoh3].value))*((P[VT_K_mCa].value)+(P[VT_Ca_o].value))*
         (1.0+(P[VT_k_sat].value)*etaV));
      C_INaCa1[Vi] = fNaCa*exp((P[VT_eta].value)*VFdRT)*(P[VT_Na_i].value * P[VT_Na_i].value * P[VT_Na_i].value)*
        (P[VT_Ca_o].value);
      C_INaCa2[Vi] = fNaCa*etaV*(P[VT_Na_o].value * P[VT_Na_o].value * P[VT_Na_o].value);
    }
    if ((emt == EMT_PriebeWeber) || (emt == EMT_PriebeWeberCa) ) {
      C_INaCa1[Vi] = exp(P[VT_eta].value*VFdRT*1.e3/1.e3);
      C_INaCa2[Vi] = exp((P[VT_eta].value-1.)*VFdRT*1.e3/1.e3);
    }

    C_INa[Vi] = (P[VT_g_Na].value)*(V-(P[VT_E_Na].value));
    C_Ito[Vi] = (P[VT_g_to].value)*(V-(P[VT_E_to].value));
    C_IKr[Vi] = (P[VT_g_Kr].value)/(1.0+exp((V+(P[VT_C_IKr1].value))*(P[VT_C_IKr2].value)))*(V-(P[VT_E_K].value));
    C_IKs[Vi] = (P[VT_g_Ks].value)*(V-(P[VT_E_Ks].value));
    if (P[VT_SQT_fraction].value > 0)
      C_IKr_SQT[Vi] = (P[VT_g_Kr_SQT].value)/(1.0+exp((V+(P[VT_C_IKr1].value))*(P[VT_C_IKr2_SQT].value)))*
        (V-(P[VT_E_K].value))*
        (1+1/(1+exp(P[VT_C_IKr3_SQT_o].value-V))*(P[VT_C_IKr3_SQT_m].value*V+P[VT_C_IKr3_SQT_b].value))*
        (1+1/(1+exp(P[VT_C_IKr4_SQT_o].value-V))*(P[VT_C_IKr4_SQT_m].value*V+P[VT_C_IKr4_SQT_b].value));
  }
} // PriebeEtAlParameters::InitTable
