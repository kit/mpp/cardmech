/**@file WinslowCanine.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef WINSLOW_CANINE_H
#define WINSLOW_CANINE_H

#include <WinslowCanineParameters.h>

// Das Modell benoetigt double Werte, mit float bekommt man eine zu grosse Abweichung. Um die orginalkurve mit dieser zu
// ueberlagern muss man ein dleta t von 0.0195 wahlen. Das Orginal File und der Orginal Code ist im Verzeichniss
// ~cr/Projects/CellModel/Canine2/states01.000.dat. Die groesst moegliche zeitliche Aufloesung ist 0.00000019 [s]. Das
// Problem ist das das Calcium Handling die kritischen Bereiche sind. Es bringt z.B. nichts mNa, hNa und jNa als
// einziges feiner zu berchnen.

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_WinslowCanineParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) WCp->P[NS_WinslowCanineParameters::a].value
#endif // ifdef HETERO

class WinslowCanine : public vbElphyModel<ML_CalcType> {
public:
  ML_CalcType Na_i, K_i;
  ML_CalcType mNa, hNa, jNa, xKr, xKs;
  double Ca_i, Ca_NSR;
  ML_CalcType Ca_SS, Ca_JSR;
  double C1_RyR;
  ML_CalcType O1_RyR;
  double C2_RyR;
  ML_CalcType O2_RyR;
  double C0;
  ML_CalcType C1, C2, C3, C4;
  ML_CalcType Open;
  ML_CalcType CCa0, CCa1, CCa2, CCa3, CCa4;
  double HTRPNCa, LTRPNCa;
  ML_CalcType yCa;
  ML_CalcType C0Kv43, C1Kv43, C2Kv43, C3Kv43, OKv43;
  ML_CalcType CI0Kv43, CI1Kv43, CI2Kv43, CI3Kv43, OIKv43;
  ML_CalcType C0Kv14, C1Kv14, C2Kv14, C3Kv14, OKv14;
  ML_CalcType CI0Kv14, CI1Kv14, CI2Kv14, CI3Kv14, OIKv14;
  WinslowCanineParameters *WCp;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  WinslowCanine(WinslowCanineParameters *pp);

  ~WinslowCanine() {}

  virtual inline ML_CalcType Volume() { return CELLMODEL_PARAMVALUE(VT_Vol); }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return -.09543106; }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline int GetSize(void) {
    return sizeof(WinslowCanine) - sizeof(vbElphyModel<ML_CalcType>) -
           sizeof(WinslowCanineParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual inline ML_CalcType *GetBase(void) { return &Na_i; }

  virtual inline void SetCai(ML_CalcType val) { Ca_i = val; }

  virtual inline void SetNai(ML_CalcType val) { Na_i = val; }

  virtual inline void SetKi(ML_CalcType val) { K_i = val; }

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char) (adVm < .5e-6 ? 5 : (adVm < 1e-6 ? 4 : (adVm < 2e-6 ? 3 : (adVm < 4e-6
                                                                                      ? 2 : 1))));
  }

  virtual void Init();

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);
}; // class WinslowCanine
#endif // ifndef WINSLOW_CANINE_H
