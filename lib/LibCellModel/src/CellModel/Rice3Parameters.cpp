/**@file Rice3Parameters.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <Rice3Parameters.h>

Rice3Parameters::Rice3Parameters(const char *initFile) {
  P = new Parameter[vtLast];
  Init(initFile);
}

void Rice3Parameters::PrintParameters() {
  // print the parameter to the stdout
  cout << "Rice3Parameters:" << endl;

  for (int i = vtFirst; i < vtLast; i++) {
    cout << "\t" << P[i].name << "\t= " << P[i].value << endl;
  }
}

void Rice3Parameters::Init(const char *initFile) {
  P[VT_P1a].name = "P1";
  P[VT_P0a].name = "P0";
  P[VT_N1a].name = "N1";
  P[VT_TCaa].name = "T_Ca";
  P[VT_k_m1].name = "k_m1";
  P[VT_f].name = "f";
  P[VT_g].name = "g";
  P[VT_k_on].name = "k_on";
  P[VT_k_off].name = "k_off";
  P[VT_K_Ca].name = "K_Ca";
  P[VT_TCaMax].name = "TCa_max";
  P[VT_Fmax].name = "Fmax";
  P[VT_dFmax].name = "dFmax";

  P[VT_K_Ca].readFromFile = false;
  P[VT_Fmax].readFromFile = false;
  P[VT_dFmax].readFromFile = false;

  ParameterLoader FPL(initFile, FMT_Rice3);
  this->setOverlapParameters(FPL.getOverlapString());
  for (int x = 0; x < vtLast; x++)
    if (P[x].readFromFile)
      P[x].value = FPL.getParameterValue(P[x].name.c_str(), P[x].readFromFile);

  Calculate();
} // Rice3Parameters::Init

void Rice3Parameters::Calculate() {
  if (PrintParameterMode == PrintParameterModeOn)
    PrintParameters();

  P[VT_K_Ca].value = P[VT_k_off].value / (P[VT_k_on].value);
  double k_1 = P[VT_k_m1].value * pow((1.0 + P[VT_K_Ca].value), 8.5);
  P[VT_Fmax].value = (P[VT_f].value * k_1 * (P[VT_g].value + k_1 + P[VT_k_m1].value)) /
                     (P[VT_f].value * P[VT_g].value * P[VT_k_m1].value +
                      P[VT_g].value * P[VT_g].value * P[VT_k_m1].value + P[VT_g].value *
                                                                         P[VT_k_m1].value *
                                                                         P[VT_k_m1].value +
                      P[VT_f].value * P[VT_g].value * k_1 + P[VT_g].value * P[VT_g].value * k_1 +
                      P[VT_f].value *
                      k_1 *
                      P[VT_k_m1].value + 2 * P[VT_g].value * k_1 * P[VT_k_m1].value +
                      P[VT_f].value * k_1 * k_1 + P[VT_g].value * k_1 * k_1);
  P[VT_dFmax].value = 1 / (P[VT_Fmax].value);
}
