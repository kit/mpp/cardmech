/**@file NygrenEtAlParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef NYGRENETAL_PARAMETERS
#define NYGRENETAL_PARAMETERS

#define NYEA_TASK

#include <ParameterLoader.h>

namespace NS_NygrenEtAlParameters {
  enum varType {
    VT_R = vtFirst,
    VT_Tx,
    VT_F,
    VT_RTdF,
    VT_RTd2F,
    VT_FdRT,
    VT_C_m,
    VT_dC_m,
    VT_Na_b,
    VT_K_b,
    VT_Ca_b,
    VT_Mg_i,
    VT_K_Ca,
    VT_K_cyca,
    VT_dKcyca,
    VT_K_srca,
    VT_dKsrca,
    VT_K_reli,
    VT_K_reld,
    VT_K_NaKK,
    VT_K_NaKNa,
    VT_K_CaP,
    VT_K_xcs,
    VT_K_NaCa,
    VT_K_NaKNapow,
    VT_KxcsdKsrca,
    VT_Kxcsh2dKsrca,
    VT_V_i,
    VT_V_c,
    VT_V_d,
    VT_V_rel,
    VT_V_up,
    VT_V_corrcell,
    VT_Area,
    VT_radius,
    VT_length,
    VT_t_Na,
    VT_dt_Na,
    VT_t_K,
    VT_dt_K,
    VT_t_Ca,
    VT_dt_Ca,
    VT_t_di,
    VT_t_tr,
    VT_I_NaK_,
    VT_I_CaP_,
    VT_I_up_,
    VT_I_Naen_,
    VT_P_Na,
    VT_g_CaL,
    VT_g_sus,
    VT_g_t,
    VT_g_Ks,
    VT_g_Kr,
    VT_g_K1,
    VT_g_BNa,
    VT_g_BCa,
    VT_E_Caapp,
    VT_Gamma,
    VT_d_NaCa,
    VT_arel,
    VT_rrecov,

    VT_Ad2VdF,
    VT_FVd2dt,
    VT_Ad2VrF,
    VT_FVr2dt,
    VT_AdVcF,
    VT_AdVc2F,
    VT_AdViF,
    VT_AdVi2F,
    VT_Ad2VuF,
#ifdef NYEA_TASK
    VT_Koh_TASK,
    VT_P_TASK,
    VT_aCC_TASK,
    VT_zaCC_TASK,
    VT_bCC_TASK,
    VT_zbCC_TASK,
    VT_aCO_TASK,
    VT_zCO_TASK,
    VT_aOC_TASK,
    VT_zOC_TASK,
    VT_init_C1_TASK,
    VT_init_C2_TASK,
    VT_init_O_TASK,
#endif // ifdef NYEA_TASK

    // Initializing ...
    VT_init_h1,
    VT_init_h2,
    VT_init_fl2,
    VT_init_s,
    VT_init_ssus,
    VT_init_n,
    VT_init_pa,
    VT_init_Ca_i,
    VT_init_Ca_d,
    VT_init_O_c,
    VT_init_O_tc,
    VT_init_O_tmgc,
    VT_init_O_calse,
    VT_init_Ca_up,
    VT_init_Ca_rel,
    VT_init_Fl1,
    VT_init_Fl2,

    VT_init_m,
    VT_init_dl,
    VT_init_fl1,
    VT_init_r,
    VT_init_rsus,
    VT_init_Na_i,
    VT_init_Na_c,
    VT_init_K_i,
    VT_init_K_c,
    VT_init_Ca_c,
    VT_init_O_tmgmg,
    VT_init_Vm,
    VT_Amp,
    VT_stim_duration,
    vtLast
  };
} // namespace NS_NygrenEtAlParameters

using namespace NS_NygrenEtAlParameters;

class NygrenEtAlParameters : public vbNewElphyParameters {
public:
  NygrenEtAlParameters(const char *, ML_CalcType);

  ~NygrenEtAlParameters() {}

  double m_[RTDT];
  double exptau_m[RTDT];
  double h_[RTDT];
  double exptau_h1[RTDT];
  double exptau_h2[RTDT];
  double dl_[RTDT];
  double exptau_dl[RTDT];
  double fl_[RTDT];
  double exptau_fl1[RTDT];
  double exptau_fl2[RTDT];
  double r_[RTDT];
  double s_[RTDT];
  double exptau_r[RTDT];
  double exptau_s[RTDT];
  double rsus_[RTDT];
  double ssus_[RTDT];
  double exptau_rsus[RTDT];
  double exptau_ssus[RTDT];
  double n_[RTDT];
  double exptau_n[RTDT];
  double pa_[RTDT];
  double exptau_pa[RTDT];
  double pi[RTDT];
  double C_Na[RTDT];
  double C_NaK[RTDT];
  double C_NaCa[RTDT];
  double dexpV[RTDT];

  void PrintParameters();

  // virtual inline int GetSize(void){return (&Vm-&R+1)*sizeof(T);};
  // virtual inline T* GetBase(void){return R;};
  /*virtual int GetNumParameters() { return 74
   #if NYEA_TASK
   +13
   #endif
        ; };*/

  void Init(const char *, ML_CalcType);

  void Calculate();

  void InitTable(ML_CalcType tinc);
}; // class NygrenEtAlParameters

#endif // ifndef NYGRENETAL_PARAMETERS
