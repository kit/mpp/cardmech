/**@file NobleForce.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef NOBLEFORCE_H
#define NOBLEFORCE_H

#include <NobleForceParameters.h>

#undef CELLMODEL_PARAMVALUE
#define CELLMODEL_PARAMVALUE(a) nfp->P[NS_NobleForceParameters::a].value

class NobleForce : public vbForceModel<ML_CalcType> {
public:
  NobleForceParameters *nfp;

  ML_CalcType TCa;
  ML_CalcType f_LightChain;
  ML_CalcType f_CrossBridge;
  ML_CalcType Ca_calmod;

  NobleForce(NobleForceParameters *);

  ~NobleForce() {}

  virtual inline int GetSize(void) {
    return sizeof(NobleForce) - sizeof(NobleForceParameters *) - sizeof(vbForceModel<ML_CalcType>);
  }

  virtual inline ML_CalcType *GetBase(void) { return &TCa; }

  void Init();

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType &, int);

  virtual inline ML_CalcType ForceEulerNEuler(int, ML_CalcType, ML_CalcType, ML_CalcType &);


  // virtual ML_CalcType Get_Ca_change() {return Ca_change*1000;};

  //   virtual void steadyState(ML_CalcType Ca, ML_CalcType stretch) {

  /*  cout<<Ca<<' '<<CELLMODEL_PARAMVALUE(VT_p_f)/(CELLMODEL_PARAMVALUE(VT_p_g_s)+CELLMODEL_PARAMVALUE(VT_p_g)+CELLMODEL_PARAMVALUE(VT_p_f))<<' '<<1/(CELLMODEL_PARAMVALUE(VT_p_g_s)+CELLMODEL_PARAMVALUE(VT_p_g)+CELLMODEL_PARAMVALUE(VT_p_f))<<'
     '<<CELLMODEL_PARAMVALUE(VT_p_f)<<' '<<CELLMODEL_PARAMVALUE(VT_p_g)<<' '<<CELLMODEL_PARAMVALUE(VT_p_g_s)<<' '<<CELLMODEL_PARAMVALUE(VT_p_k_on)*Ca<<' '<<CELLMODEL_PARAMVALUE(VT_p_k_on_s)*Ca<<' '<<CELLMODEL_PARAMVALUE(VT_p_k_off)<<'
     '<<CELLMODEL_PARAMVALUE(VT_p_k_off_s)<<endl; */

  //   }
  virtual void Print(ostream &);

  virtual void GetParameterNames(vector<string> &);
}; // class NobleForce
#endif // ifndef NOBLEFORCE_H
