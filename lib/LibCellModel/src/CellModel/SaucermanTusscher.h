/*! \file SaucermanTusscher.h
   \human cell model of ten Tusscher including Saucermans intracellular model adrenergic signaling

   \version 0.0.0

   \date Created Carola Otto (21.02.2009)\n
   template (00.00.00)\n
   man Gunnar Seemann (27.02.03)\n
   Last Modified Eike Wuelfers (03.02.10)

   \author Carola Otto\n
   Institute of Biomedical Engineering\n
   Universitaet Karlsruhe (TH)\n
   http://www.ibt.uni-karlsruhe.de\n
   Copyright 2000-2009 - All rights reserved.

   // \sa  \ref SaucermanTusscher
 */

#ifndef SAUCERMANTUSSCHER
#define SAUCERMANTUSSCHER

#include <SaucermanTusscherParameters.h>
#include <ida/ida.h>
#include <ida/ida_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>

// #include <cvode/cvode.h>             /* prototypes for CVODE fcts. and consts. */
// #include <nvector/nvector_serial.h>  /* serial N_Vector types, fcts., and macros */
// #include <cvode/cvode_dense.h>       /* prototype for CVDense */
// #include <sundials/sundials_dense.h> /* definitions DenseMat DENSE_ELEM */
// #include <sundials/sundials_types.h> /* definition of type realtype */

/*#include <stdio.h>
#include<iostream>*/

#include <cmath>
#include <fstream>

// #define Ith(w,i)    NV_Ith_S(w,i-1)  */     /* Ith numbers components 1..NEQ */

// #define NEQ   49
// #define RTOL  RCONST(1.0e-7)
/*#define RTOL  RCONST(1.0e-5)
   //#define ATOL RCONST(1.0e-7)
 #define ATOL RCONST(1.0e-6)
 #define T0    RCONST(0.0)
   //#define HMAX RCONST(0.01)
 #define HMAX RCONST(0.001)
   //#define MXSTEPS RCONST(1000000)
 #define MXSTEPS RCONST(10000)*/


#define HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_SaucermanTusscherParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) ptTeaP->P[NS_SaucermanTusscherParameters::a].value
#endif // ifdef HETERO

#ifdef KO
# define vK_o K_o
#else // ifdef KO
# define vK_o CELLMODEL_PARAMVALUE(VT_K_o)
#endif // ifdef KO
#ifdef GCAL
# define vg_CaL g_CaL
#else // ifdef GCAL
# define vg_CaL CELLMODEL_PARAMVALUE(VT_g_CaL)
#endif // ifdef GCAL
#ifdef GNA
# define vg_Na g_Na
#else // ifdef GNA
# define vg_Na CELLMODEL_PARAMVALUE(VT_g_Na)
#endif // ifdef GNA
#ifdef ATP
# define vatpi atpi
# define vadpi adpi
#else // ifdef ATP
# define vatpi CELLMODEL_PARAMVALUE(VT_atpi)
# define vadpi CELLMODEL_PARAMVALUE(VT_adpi)
#endif // ifdef ATP
#ifdef MGI
# define vMgi Mgi;
#else // ifdef MGI
# define vMgi CELLMODEL_PARAMVALUE(VT_Mgi)
#endif // ifdef MGI

#define pa(value) ((ptTeaP->value[Vi]));

// **********************************************************************************
// *                                                            Ischemia                                                                                *
// **********************************************************************************
// the rest moved to SaucermanTusscherParameters.h for performance and memory reasons
struct STDATA {
  realtype p[156];

  // realtype p[149];
};

class SaucermanTusscher : public vbElphyModel<ML_CalcType> {
public:
  SaucermanTusscherParameters *ptTeaP;
  ML_CalcType Ca_i, CaSR, CaSS, Na_i, K_i;

#ifdef ACTIVATE_IKATP_CHANNEL
# ifdef ISCHEMIA

  // things that should be included into the backup for ischemia
  ML_CalcType time_bp;

  // The following variables are normally of type bool however, the size of bool differs from 32 to 64 bit.
  // Therefore, ML_CalcType has been chosen which is equal in 32 and 64 bit
  // for details see: http://developer.apple.com/macosx/64bit.html
  // ML_CalcType printed0_bp, printed1_bp, printed2_bp;

#  ifdef KO
  ML_CalcType K_o;
#  endif // ifdef KO
#  ifdef dVmNa
  ML_CalcType dVm_Na;
#  endif // ifdef dVmNa
#  ifdef GCAL
  ML_CalcType g_CaL;
#  endif // ifdef GCAL
#  ifdef GNA
  ML_CalcType g_Na;
#  endif // ifdef GNA
#  ifdef MGI
  ML_CalcType Mgi;
#  endif // ifdef MGI
#  ifdef ATP
  ML_CalcType atpi, adpi;
#  endif // ifdef ATP

# endif // ifdef ISCHEMIA
#endif // ifdef ACTIVATE_IKATP_CHANNEL

  /*void *f_data;*/

  // N_Vector w;
  // void *cvode_mem;
  // int flag;
  realtype t;

  ML_CalcType m, h, j;          // I_Na
  ML_CalcType xr1, xr2, xs;     // IKr & IKs
  ML_CalcType r, s;             // Ito1
  ML_CalcType d, f, f2, fCa, O; // ICa
  // ML_CalcType g;                             //IRel
  ML_CalcType Rq;               // new
  ML_CalcType L, R, Gs;
  ML_CalcType b1ARd, b1ARtot, b1ARp;
  ML_CalcType Gsagtptot, Gsagdp, Gsbg, Gsa_gtp;
  ML_CalcType AC, Fsk, PDE, cAMP, IBMX;
  ML_CalcType PKACI, PKACII, cAMPtot;
  ML_CalcType PLBs, PP1, Inhib1ptot, Inhib1p;
  ML_CalcType LCCap, LCCbp, RyRp, TnIp, Iks, Yotiao, Iksp;
  ML_CalcType trel;
  ML_CalcType xs05;

  // Vm_old=(CELLMODEL_PARAMVALUE(VT_Vm_init))*1000;
  ML_CalcType Vm_old;

  // required to set initial solver conditions
  realtype y0[49];      // initial variable conditions
  realtype m0[49];      // mass vector
  STDATA stdata;        // user data parameters
  void *rdata;
  N_Vector w;           // y in solver
  N_Vector wd;          // y' in solver

  int neq;
  realtype abstol;      // absolute tolerance
  realtype reltol;      // relative tolerance
  realtype tout;        // time at which next solver output is desired
  realtype hmax;        // maximal time step of solver integration
  realtype mxsteps;     // maximal number of steps until an error flag of the solver is set
  void *ida_mem;        // required for solver memory allocation
  N_Vector id;          // contains mass matrix information in a N_Vector
  N_Vector constraints; // only required if solver should be contraint (e.g. y[46]>=0)
  int flag;             // used do get information about memory allocation and solver initialization success
  realtype *yval, *ypval;
  realtype tout1;

  // realtype I_app;            // uncomment, if internal stimulation is desired

  // ML_CalcType I_to,I_Kr,I_Ks,I_K1,I_bNa,I_bCa,I_K;
  // temporary current values
  // ML_CalcType I_Na,I_NaK,I_CaL,I_NaCa,I_rel,I_leak,I_xfer, I_Katp;
  // ML_CalcType O; //new

  // end of the variables that will be stored in an Backup (see GetSize() Function: from &Ca_i to &O)

  // the InitTableDone variable has to be excluded from the backup (see GetSize). If not, the table of each cell model
  // isn't
  // initialized correctly if using a backup (with initializeXCLT)
  bool InitTableDone;

#ifdef ACTIVATE_IKATP_CHANNEL
# ifdef ISCHEMIA
  ML_CalcType time, stage0, stage1, stage2;
  bool zonefactor_calculated, restore;

#  ifdef ISCHEMIA_VERBOSE_OUPUT
  bool printed0, printed1, printed2;
#  endif // ifdef ISCHEMIA_VERBOSE_OUPUT

  // **************************************************************************************************************
  // *** definition of the individual zoneFactors for the ischemia effects. These MUST be stored here in the cell model
  // *** instead of the parameter set because the VT_ZoneFactor will be overwritten for the individual cells by a
  // *** heterogeneous lattice (AddHeteroValue call) and thus, the value of zonefactor_X changes from cell 2 cell

#  ifdef ACIDOSIS
  ML_CalcType zonefactor_fpH;
#  endif // ifdef ACIDOSIS
#  ifdef HYPERKALEMIA
  ML_CalcType zonefactor_Ko;
#  endif // ifdef HYPERKALEMIA
#  ifdef HYPOXIA
  ML_CalcType zonefactor_pO;
#  endif // ifdef HYPOXIA

  // *** end of: definition of the individual zoneFactors for the ischemia effects.
  // **************************************************************************************************************


  // **************************************************************************************************************
  // *** definition of the change of the ischemia effects over time from stage0 via stage1 to stage2
  // *** this values also depend on the individual zonefactor_X values and must therefore also be
  // *** stored here in the cell model

#  ifdef KO
  ML_CalcType KoAdder1, KoAdder2;
#  endif // ifdef KO

#  ifdef GCAL
  ML_CalcType CaLAdder1, CaLAdder2;
#  endif // ifdef GCAL

#  ifdef GNA
  ML_CalcType NaAdder1, NaAdder2;
#  endif // ifdef GNA

#  ifdef dVmNa
  ML_CalcType dVmNaAdder1, dVmNaAdder2;
#  endif // ifdef dVmNa

#  ifdef ATP
  ML_CalcType ATPAdder1, ATPAdder2, ADPAdder1, ADPAdder2;
#  endif // ifdef ATP

#  ifdef MGI
  ML_CalcType MgiAdder1, MgiAdder2;
#  endif // ifdef MGI

  // *** end of: definition of the change of the ischemia effects over time from stage0 via stage1 to stage2
  // **************************************************************************************************************
# endif // ifdef ISCHEMIA
#endif // ifdef ACTIVATE_IKATP_CHANNEL

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  SaucermanTusscher(SaucermanTusscherParameters *pp);

  ~SaucermanTusscher();

  virtual inline bool AddHeteroValue(string desc, double val);

  virtual inline ML_CalcType SurfaceToVolumeRatio() { return 1.0; }

  // virtual inline  ML_CalcType SurfaceToVolumeRatio(){ return 0.2; };
  // TT2 orig Paper: 0.2 1/mycrometer // wird von xCELLent bisher noch nicht aufgerufen

  // TT2 original Volume
  // virtual inline  ML_CalcType Volume(){ return CELLMODEL_PARAMVALUE(VT_volforCall)*4.5; };

  // value from SaucermanTusscher.h (Version 1 of CellModel)
  virtual inline ML_CalcType Volume() { return 2.064403e-13 * 5.6; }

  // virtual inline  ML_CalcType Volume(){ return 3.80132711e-14 * 12.5; };
  // 3.34768e-14;}; //2.064403e-13*5.6 //3.34768e-05 //3.34768e-14

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_V_init); }

  virtual inline ML_CalcType GetCai() { return Ca_i; }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return Na_i; }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return K_i; }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline ML_CalcType GetIto() { return 0.0; }

  virtual inline ML_CalcType GetIKr() { return 0.0; }

  virtual inline ML_CalcType GetIKs() { return 0.0; }

  virtual inline int GetSize(void);

  virtual inline ML_CalcType *GetBase(void);

  virtual inline ML_CalcType GetSpeedupMax(void) { return .0; }

  virtual ML_CalcType GetAmplitude(void) { return CELLMODEL_PARAMVALUE(VT_Amp);  /*38.0;*/ }

  // dw: Änderung von 9.6 auf 4.0 -> Overshoot auf ca. +40 mV begrenzt und deutlicheres notch//new vorher: 26.
  virtual inline ML_CalcType GetStimTime() { return 0.001; }

  // virtual inline  ML_CalcType GetStimTime() {return 0.002; };
  // dw:ganz original ist es 0.00098
  // mi096: changed StimTime to 0.002 for ischemia simulations because hyperkalemia requires a greater stimulus or
  // longer stim time
  virtual inline unsigned char getSpeed(ML_CalcType adVm);

  virtual void Init();

  virtual void alloc_ida();

#ifdef ACTIVATE_IKATP_CHANNEL
# ifdef ISCHEMIA
  virtual void InitIschemiaTimeCourse();
  virtual void CalcIschemiaTimeCourse(double mytinc);
# endif // ifdef ISCHEMIA
#endif // ifdef ACTIVATE_IKATP_CHANNEL

  virtual ML_CalcType
  Calc(double tinc, ML_CalcType V, ML_CalcType i_external, ML_CalcType stretch, int euler);

  virtual void Print(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void LongPrint(ostream &tempstr, double tArg, ML_CalcType V);

  virtual void GetParameterNames(vector<string> &getpara);

  virtual void GetLongParameterNames(vector<string> &getpara);
}; // class SaucermanTusscher
#endif // ifndef SAUCERMANTUSSCHER
