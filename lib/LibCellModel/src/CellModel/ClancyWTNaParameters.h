/**@file ClancyWTNaParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef CLANCYWTNA_PARAMETERS
#define CLANCYWTNA_PARAMETERS

#include <ParameterLoader.h>

namespace NS_ClancyWTNaParameters {
  enum varType {
    VT_Tx = vtFirst,
    VT_Nao,
    VT_Nai,
    VT_g_Na,
    VT_IC3,
    VT_IC2,
    VT_IF,
    VT_IM1,
    VT_IM2,
    VT_C1,
    VT_C2,
    VT_C3,
    VT_O,
    VT_a11a,
    VT_a11b,
    VT_a11c,
    VT_a11d,
    VT_a11e,
    VT_a12a,
    VT_a12b,
    VT_a12c,
    VT_a12d,
    VT_a12e,
    VT_a13a,
    VT_a13b,
    VT_a13c,
    VT_a13d,
    VT_a13e,
    VT_b11a,
    VT_b11b,
    VT_b11c,
    VT_b12a,
    VT_b12b,
    VT_b12c,
    VT_b13a,
    VT_b13b,
    VT_b13c,
    VT_a2a,
    VT_a2b,
    VT_a3a,
    VT_a3b,
    VT_b3a,
    VT_b3b,
    VT_a4a,
    VT_b4a,
    VT_a5a,
    VT_b5a,
    VT_Amp,
    vtLast
  };
} // namespace NS_ClancyWTNaParameters

using namespace NS_ClancyWTNaParameters;

class ClancyWTNaParameters : public vbNewElphyParameters {
public:
  ClancyWTNaParameters(const char *);

  ~ClancyWTNaParameters() {}

  void PrintParameters();

  // virtual inline int GetSize(void) {return (&b5a-&Tx+1)*sizeof(T);};
  // virtual inline ML_CalcType* GetBase(void) {return Tx;};
  // virtual int GetNumParameters() { return 47; };
  void Init(const char *);

  void Calculate();

  void InitTable();
};

#endif // ifndef CLANCYWTNA_PARAMETERS
