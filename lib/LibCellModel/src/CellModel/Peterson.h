/**@file Peterson.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef PETERSON_H
#define PETERSON_H

#include <PetersonParameters.h>

#define CELLMODEL_PARAMVALUE(a) pp->P[NS_PetersonParameters::a].value

class Peterson : public vbForceModel<ML_CalcType> {
public:
  PetersonParameters *pp;

  ML_CalcType P1, P0, N1;

  Peterson(PetersonParameters *);

  ~Peterson() {}

  virtual inline int GetSize(void) {
    return sizeof(Peterson) - sizeof(PetersonParameters *) - sizeof(vbForceModel<ML_CalcType>);
  }

  virtual inline ML_CalcType *GetBase(void) { return &P1; }

  void Init();

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType &, int);

  virtual inline ML_CalcType ForceEulerNEuler(int, ML_CalcType, ML_CalcType, ML_CalcType &);

  virtual void steadyState(ML_CalcType, ML_CalcType);

  virtual void Print(ostream &);

  virtual void GetParameterNames(vector<string> &);
};

#endif // ifndef PETERSON_H
