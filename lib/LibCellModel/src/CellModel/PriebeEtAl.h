/**@file PriebeEtAl.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef PRIEBEETAL_H
#define PRIEBEETAL_H

#include <PriebeEtAlParameters.h>

#undef HETERO
#undef CELLMODEL_PARAMVALUE

#ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) PS->getValue(NS_PriebeEtAlParameters::a)
#else // ifdef HETERO
# define CELLMODEL_PARAMVALUE(a) pPeaP->P[NS_PriebeEtAlParameters::a].value
#endif // ifdef HETERO


class PriebeEtAl : public vbElphyModel<ML_CalcType> {
public:
  PriebeEtAlParameters *pPeaP;
  ML_CalcType Ca_itot, Ca_JSRtot, Ca_NSR, Ca_itotVmax, dCa_i2, I_Kr_SQT, Xr_SQT;
  ML_CalcType m;
  ML_CalcType t_CICR, t_Vmax;
  bool CICR;
  ML_CalcType h, j, d, f, r, t, Xr, Xs;

#ifdef HETERO
  ParameterSwitch *PS;
#endif // ifdef HETERO

  PriebeEtAl(PriebeEtAlParameters *);

  ~PriebeEtAl() {}

  virtual inline ML_CalcType Volume() { return (CELLMODEL_PARAMVALUE(VT_Vol)) * 7250.0; }

  virtual inline ML_CalcType GetAmplitude() { return CELLMODEL_PARAMVALUE(VT_Amp); }

  virtual inline ML_CalcType GetStimTime() { return 0.003; }

  virtual inline ML_CalcType GetVm() { return CELLMODEL_PARAMVALUE(VT_init_Vm); }

  virtual inline ML_CalcType GetCai() {
    return -5.8407E-5 + Ca_itot * (0.012799 + Ca_itot * (Ca_itot * 3.8692 - 0.17534));
  }

  virtual inline ML_CalcType GetCao() { return CELLMODEL_PARAMVALUE(VT_Ca_o); }

  virtual inline ML_CalcType GetNai() { return CELLMODEL_PARAMVALUE(VT_Na_i); }

  virtual inline ML_CalcType GetNao() { return CELLMODEL_PARAMVALUE(VT_Na_o); }

  virtual inline ML_CalcType GetKi() { return CELLMODEL_PARAMVALUE(VT_K_i); }

  virtual inline ML_CalcType GetKo() { return CELLMODEL_PARAMVALUE(VT_K_o); }

  virtual inline int GetSize(void) {
    return sizeof(PriebeEtAl) - sizeof(vbElphyModel<ML_CalcType>) - sizeof(PriebeEtAlParameters *)
#ifdef HETERO
      -sizeof(ParameterSwitch*)
#endif // ifdef HETERO
        ;
  }

  virtual bool AddHeteroValue(string, double);

  virtual inline ML_CalcType *GetBase(void) { return &Ca_itot; }

  virtual inline ML_CalcType GetTCa() {
    const ML_CalcType Ca_i = GetCai();

    return CELLMODEL_PARAMVALUE(VT_TRPN_max) * Ca_i / (Ca_i + (CELLMODEL_PARAMVALUE(VT_k_mTRPN)));
  }

  virtual inline bool OutIsTCa() { return true; }

  virtual inline void SetCai(ML_CalcType val) {
    // Inversion of GetCai
    double tmp = -0.0437585 + 404.20913328000006 * val;
    double tmp2 = sqrt(0.0065423573481079534 + tmp * tmp);

    tmp = pow(tmp + tmp2, 0.3333333333333333);
    Ca_itot = 0.015105620455563594 - 0.012788678068136775 / tmp + 0.06837765998002168 * tmp;
  }

  virtual inline void SetSL(ML_CalcType val) {}

  virtual inline unsigned char getSpeed(ML_CalcType adVm) {
    return (unsigned char) (adVm < 1e-6 ? 5 : (adVm < 2e-6 ? 4 : (adVm < 4e-6 ? 3 : (adVm < 8e-6 ? 2
                                                                                                 : 1))));
  }

  virtual const ML_CalcType GetINaCa(const int Vi, const ML_CalcType Ca_i) {
    return pPeaP->C_INaCa1[Vi] - Ca_i * pPeaP->C_INaCa2[Vi];
  }

  virtual void Init();

  virtual ML_CalcType Calc(double, ML_CalcType, ML_CalcType, ML_CalcType, int);

  virtual void Print(ostream &, double, ML_CalcType);

  virtual void LongPrint(ostream &, double, ML_CalcType);

  virtual void GetParameterNames(vector<string> &);

  virtual void GetLongParameterNames(vector<string> &);
}; // class PriebeEtAl

class PriebeEtAlCa : public PriebeEtAl {
public:
  PriebeEtAlCa(PriebeEtAlParameters *pp) : PriebeEtAl(pp) {}

  virtual inline ML_CalcType GetTCa() { return 0.0; }

  virtual inline bool OutIsTCa() { return false; }
};


class PriebeWeber : public PriebeEtAl {
public:

  virtual inline ML_CalcType GetCai() {
    return -2.1E-4 +
           1.58 * this->Ca_itot * (0.012799 + this->Ca_itot * (this->Ca_itot * 3.8692 - 0.17534));
  }

  virtual const ML_CalcType GetINaCa(const int Vi, const ML_CalcType Ca_i) {
    const PriebeEtAlParameters *pPeaP = this->pPeaP;
    const ML_CalcType Nai3 = CELLMODEL_PARAMVALUE(VT_Naih3);
    const ML_CalcType Nao3 = CELLMODEL_PARAMVALUE(VT_Naoh3);
    const ML_CalcType K_mNai3 = CELLMODEL_PARAMVALUE(VT_K_mNaih3);
    const ML_CalcType K_mNao3 = CELLMODEL_PARAMVALUE(VT_K_mNaoh3);
    const ML_CalcType K_mCai = CELLMODEL_PARAMVALUE(VT_K_mCai_Weber);
    const ML_CalcType K_mCao = CELLMODEL_PARAMVALUE(VT_K_mCao_Weber);
    const ML_CalcType K_mCaAct = CELLMODEL_PARAMVALUE(VT_K_mCaAct_Weber);
    const ML_CalcType Ca_o = CELLMODEL_PARAMVALUE(VT_Ca_o);
    ML_CalcType Ca_sm = Ca_i;
    const ML_CalcType t_Vmax = this->t_Vmax;

    if (t_Vmax > 4)                                                     // peak after 24 ms
      Ca_sm += 1.1e-3 * (1. - exp(-(t_Vmax - 4) / 8.)) * exp(1. - (t_Vmax - 4) / 92.);  // in mmol
    ML_CalcType rc =
        CELLMODEL_PARAMVALUE(VT_k_NaCa) * (Nai3 * Ca_o * pPeaP->C_INaCa1[Vi] - Nao3 * Ca_sm * pPeaP->C_INaCa2[Vi]) /
        (1. + ((K_mCaAct * K_mCaAct) / (Ca_sm * Ca_sm))) /
        ((K_mCao * Nai3 + K_mNao3 * Ca_sm + K_mNai3 * Ca_o * (1. + Ca_sm / K_mCai) +
          K_mCai * Nao3 * (1. + Nai3 / K_mNai3) + Nai3 * Ca_o + Nao3 * Ca_sm) *
         (1. + CELLMODEL_PARAMVALUE(VT_k_sat) * pPeaP->C_INaCa2[Vi]));

    //  printf("! %d %lf %lf %lf\n", Vi, Ca_i, Ca_sm, rc);

    return rc;
  }

  PriebeWeber(PriebeEtAlParameters *pp) : PriebeEtAl(pp) {}
}; // class PriebeWeber

class PriebeWeberCa : public PriebeWeber {
public:
  PriebeWeberCa(PriebeEtAlParameters *pp) : PriebeWeber(pp) {}

  virtual inline ML_CalcType GetTCa() { return 0.0; }

  virtual inline bool OutIsTCa() { return false; }
};

#endif // ifndef PRIEBEETAL_H
