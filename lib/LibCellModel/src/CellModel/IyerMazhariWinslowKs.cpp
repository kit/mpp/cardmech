/**@file IyerMazhariWinslowKs.cpp
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#include <IyerMazhariWinslowKs.h>

IyerMazhariWinslowKs::IyerMazhariWinslowKs(IyerMazhariWinslowKsParameters *pIMWArg) {
  pIMW = pIMWArg;
#ifdef HETERO
  PS = new ParameterSwitch(pIMW, NS_IyerMazhariWinslowKsParameters::vtLast);
#endif // ifdef HETERO
  Init();
}

void IyerMazhariWinslowKs::Init() {
  Ki = CELLMODEL_PARAMVALUE(VT_Ki);

  C0Ks = CELLMODEL_PARAMVALUE(VT_C0Ks);
  C1Ks = CELLMODEL_PARAMVALUE(VT_C1Ks);
  O1Ks = CELLMODEL_PARAMVALUE(VT_O1Ks);
  O2Ks = CELLMODEL_PARAMVALUE(VT_O2Ks);
}

void IyerMazhariWinslowKs::Print(ostream &tempstr, double t, ML_CalcType V) {
  tempstr << t << ' '  // 1
          << V << ' '
          << Ki << ' '
          << C0Ks << ' '
          << C1Ks << ' ' // 5
          << O1Ks << ' '
          << O2Ks << ' ';
}

void IyerMazhariWinslowKs::LongPrint(ostream &tempstr, double t, ML_CalcType V) {
  Print(tempstr, t, V);

  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);

  /*     const double Acap=CELLMODEL_PARAMVALUE(VT_Acap); */
  /*     const double Vmyo=CELLMODEL_PARAMVALUE(VT_Vmyo); */

  const double V_int = V * 1000.0;
  const double RTdF = ElphyModelConstants::R * Tx / ElphyModelConstants::F;

  /*     const double FdRT=1./RTdF; */
  /*     const double VFdRT=V_int*FdRT; */

  //    const double expVFdRT=exp(VFdRT);

  const double Ko = CELLMODEL_PARAMVALUE(VT_Ko);

  const double EK = RTdF * log(Ko / Ki); // in range -94 to -88 mV => use 91 mV

  //    cerr << ' ' << Ko << EK << endl;

#ifdef IKSOPEN
  double I_Ks = CELLMODEL_PARAMVALUE(VT_GKs) * ((O1Ks + O2Ks) * (1. - IKSOPEN) + IKSOPEN) * (V_int - EK);
#else // ifdef IKSOPEN
  double I_Ks = CELLMODEL_PARAMVALUE(VT_GKs)*(O1Ks+O2Ks)*(V_int-EK);
#endif // ifdef IKSOPEN

  tempstr << ' ' << I_Ks; // 8
} // IyerMazhariWinslowKs::LongPrint

void IyerMazhariWinslowKs::GetParameterNames(vector<string> &getpara) {
  const int numpara = 5;
  const string ParaNames[numpara] = {"Ki", "COKs", "C1Ks", "O1Ks", "O2Ks"};

  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

void IyerMazhariWinslowKs::GetLongParameterNames(vector<string> &getpara) {
  GetParameterNames(getpara);
  const int numpara = 1;
  const string ParaNames[numpara] = {"I_Ks"};
  for (int i = 0; i < numpara; i++)
    getpara.push_back(ParaNames[i]);
}

ML_CalcType IyerMazhariWinslowKs::Calc(double tinc, ML_CalcType V, ML_CalcType I_Stim = .0,
                                       ML_CalcType stretch = 1.,
                                       int euler = 1) {
  const double Tx = CELLMODEL_PARAMVALUE(VT_Tx);

  /*     const double Acap=CELLMODEL_PARAMVALUE(VT_Acap); */
  /*     const double Vmyo=CELLMODEL_PARAMVALUE(VT_Vmyo); */

  tinc *= 1000.0;
  const double V_int = V * 1000.0;

  /*     const double RTdF=R*Tx/F; */
  /*     const double FdRT=1./RTdF; */
  /*     const double VFdRT=V_int*FdRT; */
  /*    const double expVFdRT=exp(VFdRT); */

  /*     const double Ko=CELLMODEL_PARAMVALUE(VT_Ko); */

  /*     const double EK=RTdF*log(Ko/Ki); */

  const double Qb = pow(2., (Tx - 310.) / 10.);

  const double Ksa = CELLMODEL_PARAMVALUE(VT_Ksa) * Qb;
  const double Ksb = CELLMODEL_PARAMVALUE(VT_Ksb) * exp(-.00002 * V_int) * Qb;
  const double Ksg = CELLMODEL_PARAMVALUE(VT_Ksg) * Qb;
  const double Ksd = CELLMODEL_PARAMVALUE(VT_Ksd) * exp(-.15 * V_int) * Qb;
  const double Kse = CELLMODEL_PARAMVALUE(VT_Kse) * exp(.087 * V_int) * Qb;
  const double Kso = CELLMODEL_PARAMVALUE(VT_Kso) * exp(-.014 * V_int) * Qb;

  const double dC0Ks = (-Ksa * C0Ks + Ksb * C1Ks);
  const double dC1Ks = (-(Ksb + Ksg) * C1Ks + Ksa * C0Ks + Ksd * O1Ks);
  const double dO1Ks = (-(Ksd + Kse) * O1Ks + Ksg * C1Ks + Kso * O2Ks);

  C0Ks += tinc * dC0Ks;
  if (C0Ks > 1.)
    C0Ks = 1.;
  C1Ks += tinc * dC1Ks;
  if (C1Ks > 1.)
    C1Ks = 1.;
  O1Ks += tinc * dO1Ks;
  if (O1Ks < 0.)
    O1Ks = 0.;
  O2Ks = 1. - C0Ks - C1Ks - O1Ks;
  if (O2Ks < 0.)
    O2Ks = 0.;

  assert(C0Ks >= 0. && C0Ks <= 1.);
  assert(C1Ks >= 0. && C1Ks <= 1.);
  assert(O1Ks >= 0. && O1Ks <= 1.);
  assert(O2Ks >= 0. && O2Ks <= 1.);

  /*     double I_Ks=CELLMODEL_PARAMVALUE(VT_GKs)*(O1Ks+O2Ks)*(V_int-EK); */
  /*     tinc*=0.001; */

  /*     Ki+=tinc*-(I_Ks-I_Stim)*ACVmyoF; */

  /*     return -tinc*(I_Ks-I_Stim); */

  return 0.;
} // IyerMazhariWinslowKs::Calc
