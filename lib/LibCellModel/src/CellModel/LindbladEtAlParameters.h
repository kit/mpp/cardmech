/* File: LindbladEtAlParameters.h
        automatically created by CellML2Elphymodel.pl
        Institute of Biomedical Engineering, Universität Karlsruhe (TH) */

#ifndef LINDBLADETALPARAMETERS_H
#define LINDBLADETALPARAMETERS_H

#include <ParameterLoader.h>

namespace NS_LindbladEtAlParameters {
enum varType {
  VT_R = vtFirst,
  VT_T,
  VT_F,
  VT_Cm,
  VT_stim_start,
  VT_stim_end,
  VT_stim_period,
  VT_stim_duration,
  VT_stim_amplitude,
  VT_P_Na,
  VT_Na_c,
  VT_g_Ca_L,
  VT_E_Ca_app,
  VT_g_Ca_T,
  VT_E_Ca_T,
  VT_g_to,
  VT_K_c,
  VT_g_Ks,
  VT_g_Kr,
  VT_g_K1,
  VT_KmK1,
  VT_steepK1,
  VT_shiftK1,
  VT_g_B_Na,
  VT_g_B_Ca,
  VT_g_B_Cl,
  VT_Ca_c,
  VT_Cl_c,
  VT_Cl_i,
  VT_k_NaK_K,
  VT_k_NaK_Na,
  VT_i_NaK_max,
  VT_i_CaP_max,
  VT_k_CaP,
  VT_k_NaCa,
  VT_d_NaCa,
  VT_gamma,
  VT_Vol_i,
  VT_Vol_Ca,
  VT_Mg_i,
  VT_I_up_max,
  VT_k_cyca,
  VT_k_srca,
  VT_k_xcs,
  VT_alpha_rel,
  VT_Vol_up,
  VT_Vol_rel,
  VT_tau_tr,
  VT_k_rel,
  VT_k_F3,
  VT_E_Cl,
  VT_E_B_Cl,
  VT_V_init,
  VT_Na_i_init,
  VT_m_init,
  VT_h1_init,
  VT_h2_init,
  VT_d_L_init,
  VT_f_L_init,
  VT_d_T_init,
  VT_f_T_init,
  VT_K_i_init,
  VT_r_init,
  VT_s1_init,
  VT_s2_init,
  VT_s3_init,
  VT_z_init,
  VT_p_a_init,
  VT_p_i_init,
  VT_Ca_i_init,
  VT_O_C_init,
  VT_O_TC_init,
  VT_O_TMgC_init,
  VT_O_TMgMg_init,
  VT_Ca_rel_init,
  VT_Ca_up_init,
  VT_O_Calse_init,
  VT_F1_init,
  VT_F2_init,
  VT_RTdF,
  VT_powKc,
  VT_powNac,
  VT_powk_NaK_Na,
  vtLast
};
} // namespace NS_LindbladEtAlParameters

using namespace NS_LindbladEtAlParameters;

class LindbladEtAlParameters : public vbNewElphyParameters {
 public:
  LindbladEtAlParameters(const char*, ML_CalcType);
  ~LindbladEtAlParameters();
  void PrintParameters();
  void Calculate();
  void InitTable(ML_CalcType);
  void Init(const char*, ML_CalcType);
  ML_CalcType tau_s1[RTDT];
  ML_CalcType tau_s2[RTDT];
  ML_CalcType tau_s3[RTDT];
  ML_CalcType alpha_m[RTDT];
  ML_CalcType beta_m[RTDT];
  ML_CalcType tau_h1[RTDT];
  ML_CalcType tau_h2[RTDT];
  ML_CalcType tau_r[RTDT];
  ML_CalcType tau_z[RTDT];
  ML_CalcType tau_p_a[RTDT];
  ML_CalcType tau_p_i[RTDT];
  ML_CalcType tau_d_T[RTDT];
  ML_CalcType tau_f_T[RTDT];
  ML_CalcType tau_d_L[RTDT];
  ML_CalcType tau_f_L[RTDT];

  ML_CalcType exptau_s1[RTDT];
  ML_CalcType s1_infinity[RTDT];
  ML_CalcType exptau_s2[RTDT];
  ML_CalcType s2_infinity[RTDT];
  ML_CalcType exptau_s3[RTDT];
  ML_CalcType s3_infinity[RTDT];
  ML_CalcType m_infinity[RTDT];
  ML_CalcType exptau_m[RTDT];
  ML_CalcType h_infinity[RTDT];
  ML_CalcType exptau_h1[RTDT];
  ML_CalcType exptau_h2[RTDT];
  ML_CalcType exptau_r[RTDT];
  ML_CalcType r_infinity[RTDT];
  ML_CalcType exptau_z[RTDT];
  ML_CalcType z_infinity[RTDT];
  ML_CalcType exptau_p_a[RTDT];
  ML_CalcType p_a_infinity[RTDT];
  ML_CalcType exptau_p_i[RTDT];
  ML_CalcType p_i_infinity[RTDT];
  ML_CalcType d_T_infinity[RTDT];
  ML_CalcType exptau_d_T[RTDT];
  ML_CalcType f_T_infinity[RTDT];
  ML_CalcType exptau_f_T[RTDT];
  ML_CalcType d_L_infinity[RTDT];
  ML_CalcType exptau_d_L[RTDT];
  ML_CalcType f_L_infinity[RTDT];
  ML_CalcType exptau_f_L[RTDT];
  ML_CalcType d_prime[RTDT];
  ML_CalcType i_B_Cl[RTDT];
  ML_CalcType i_p_Const[RTDT];
  ML_CalcType i_Na_Const[RTDT];
}; // class LindbladEtAlParameters

#endif // ifndef LINDBLADETALPARAMETERS_H
