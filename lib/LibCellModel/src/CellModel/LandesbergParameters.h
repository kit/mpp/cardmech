/**@file LandesbergParameters.h
 * @brief <Brief (one-line) description here.>
 *
 * Please see the wiki for details on how to fill out this header:
 * https://intern.ibt.uni-karlsruhe.de/wiki/Document_a_IBT_C%2B%2B_tool
 *
 * @version 1.0.0
 *
 * @date Created <Your Name> (yyyy-mm-dd)
 *
 * @author Your Name\n
 *         Institute of Biomedical Engineering\n
 *         Karlsruhe Institute of Technology (KIT)\n
 *         http://www.ibt.kit.edu\n
 *         Copyright yyyy - All rights reserved.
 *
 * @see ...
 */

#ifndef LANDESBERG_PARAMETERS
#define LANDESBERG_PARAMETERS

#include <ParameterLoader.h>

namespace NS_LandesbergParameters {
  enum varType {
    VT_P1a = vtFirst,
    VT_P0a,
    VT_N1a,
    VT_p_k_on,
    VT_Kapp0,
    VT_Kapp1,
    VT_Kapp2,
    VT_Kapp3,
    VT_p_f,
    VT_p_g,
    VT_Fmax,
    VT_dFmax,
    vtLast
  };
}

using namespace NS_LandesbergParameters;

class LandesbergParameters : public vbNewForceParameters {
public:
  LandesbergParameters(const char *);

  ~LandesbergParameters() {}

  // virtual inline int GetSize(void){return /*(&dFmax-&P1a+1)*sizeof(T)*/0;};
  // virtual inline ML_CalcType* GetBase(void){return NULL/*P1a*/;};
  // virtual int GetNumParameters() { return 10; };
  void PrintParameters();

  void Init(const char *);

  void Calculate();
};

#endif // ifndef LANDESBERG_PARAMETERS
