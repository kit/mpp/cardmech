# = Removes all files in the data folder upon start ======
ClearData = true
# ========================================================
h0=1e-4
h_min=1e-6

Model = ActiveStrainElasticity
MechRuntype = Default
MechDynamics = Static
MechDiscretization = DG #EG#Conforming

CalculateReference = 0
ReferenceLevel = 1
InterpolateStartVector = false

MechProblem = CardiacBeam
MechMeshPart = vessel_base

SetDisplacement = 0

ProblemDimension = 3
ProblemGeometry = Tet

# Amount of pressure steps per iteration
PressureSteps = 10
PressureIterations = 10
PressureDepth = 0

DebugPressure = 1

MechLevel = 1
MechPolynomialDegree = 1

WithPrestress=false
PrestressSteps=1

######################
#   Mech Parameters  #
######################

#------------------------
# Newmark Parameters
#------------------------
NewmarkBeta = 0.25
NewmarkGamma = 0.5

#------------------------
# Stretch Parameters
#------------------------
StretchFactorSheet = 1.0
StretchFactorNormal = 4.0


######################
# Material Parameters#
######################
ActiveMaterial = Linear
#ActiveMaterial = NeoHooke
#ActiveMaterial = Bonet
ActiveMaterial = Holzapfel

Density = 0.00
#Density = 0.001
#Density = 1.082
#Density = 0.000001082

#------------------------
# Volumetric Penalty
#------------------------
Incompressible = false
QuasiCompressiblePenalty = None
VolumetricPenalty = 2
PermeabilityPenalty = 2

#------------------------
# Linear Material
#------------------------
LinearMat_Mu = 2
LinearMat_Lambda = 4

#------------------------
# Guccione Material
#------------------------
GuccioneMat_C=2
GuccioneMat_bf=8
GuccioneMat_bs=2
GuccioneMat_bfs=4




######################
# Pressure Parameters#
######################
#ConstantPressure = -10
LVPressure = 0
RVPressure = 0
LAPressure = 0.004
RAPressure = 0


######################
#      Plotting      #
######################
PlotStress = 0
PlotVertexPotential = 0
PlotCellPotential = 0


# vtk Dateien werden erstellt
PlotVTK  = -1
PlottingSteps = 1


# === Mechanics Solver ============================================
MechSolver = gmres
MechPreconditioner = GaussSeidel;
MechEpsilon = 1e-8;
MechReduction = 1e-12;
MechSteps = 10000;

# === Newton Method === #
NewtonEpsilon = 1e-6
NewtonReduction = 1e-12;
NewtonSteps = 200
NewtonLineSearchSteps = 10;
NewtonLinearizationReduction = 1e-12
#NewtonJacobiUpdate = 8
#NewtonDamping = 0.75

#Transfer = Serendipity;
#SimpleMatrixTransfer = 1;


#BaseSolver = LS;
#BasePreconditioner = LIB_PS;
#BaseSolverSteps = 10;
#BaseSolverEpsilon = 1e-10;
#BaseSolverReduction = 1e-11;

#Smoother = GaussSeidel;
#Smoother = PointBlockGaussSeidel;
#SmootherDamp = 0.9;
presmoothing = 3;
postsmoothing = 3;

DGSign=-1.0
DGPenalty= 9.0

DebugLevel = 0;
TimeLevel = 0;
#gnuplot  = 1;
precision = 10;

ConfigVerbose = 1
MeshVerbose = 2
AssembleVerbose = 1
ReferenceVerbose = 2
MainMechVerbose = 1
MechProblemVerbose = 1

# = Solver Verbose =
MultigridVerbose = 0
BaseSolverVerbose = -1
LinearVerbose = -5
NewtonVerbose = 1
EulerVerbose = 0
LinearImplicitVerbose = -5
MonodomainVerbose =-1
NonLinearVerbose = -5
ElphySolverVerbose = -1
ElphyLinearSolverVerbose =-1
PressureSolverVerbose = 1
DynamicSolverVerbose = -1
CoupledSolverVerbose = 2
CellModelVerbose = -10

# = Main Verbose =
ElphyVerbose = -1
MechVerbose = -1

# = Plot Verbose =
MechPlot=1
ElphySolverVTK = -1
PressureSolverVTK = 1
DynamicSolverVTK = -1
CoupledSolverVTK = -1

PrestressPlotVerbose = -1
PressurePlotVerbose = 1
DynamicPlotVerbose = -1
PlotVTK = 1

ParallelPlotting = 0

