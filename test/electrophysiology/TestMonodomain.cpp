#include "TestMonodomain.hpp"
#include "TestEnvironmentCardMech.hpp"
#include "MainMonodomain.hpp"

constexpr double TEST_TOLERANCE = 1e-4;


class MonodomainTest : public TestWithParam<TestMonodomainParameter> {
protected:
  MainMonodomain *cmMain;

  MonodomainTest() {}

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;
    testConfig["Distribution"] = "Stripes";
    testConfig["ElphyPreconditioner"] = "SuperLU";
    testConfig["Model"] = "Monodomain";
    testConfig["ElphyModel"] = GetParam().elphyTimeIntegration;
    testConfig["ElphySplittingMethod"] = GetParam().splittingMethod;
    testConfig["ReassembleRHSonce"] = "1";
    testConfig["ElphyLevel"] = "0";
    testConfig["ElphySubsteps"] = std::to_string(GetParam().elphySubsteps);
    testConfig["ElphyMeshPart"] = "fourchamber";
    testConfig["IextInPDE"] = "0";
    testConfig["ExternalCurrentSmoothingInSpace"] = GetParam().externalCurrentInSpace;
    testConfig["ElphyPolynomialDegree"] = "1";
    testConfig["ExternalCurrentSmoothingInTime"] = GetParam().externalCurrentInTime;

    testConfig["TimeSeries"] = "uniformabaqus";
    testConfig["StartTime"] = "0.0";
    testConfig["EndTime"] = std::to_string(GetParam().endTime);
    testConfig["DeltaTime"] = std::to_string(GetParam().deltaTime);
    "0.00001";
    testConfig ["OrderStaggeredScheme"] = "wVc";
    testConfig["ElphyModelClass"] = GetParam().elphyCellModelClass;
    testConfig["VentricleModel"] = "IBT";
    testConfig["AtriaModel"] = "IBT";
    testConfig["ScaleExcitationAmplitude"] = "0.04101";
    testConfig["IBTVentricleModel"] = "TenTusscher2";
    testConfig["IBTVentricleParameters"] = "lib/LibCellModel/data/CellModel/TenTusscher2_Benchmark.ev";
    testConfig["IBTAtriaModel"] = "TenTusscher2";
    testConfig["IBTAtriaParameters"] = "lib/LibCellModel/data/CellModel/TenTusscher2_Benchmark.ev";
    //oder:
    //testConfig["IBTVentricleParameters"] = "CellModel/TenTusscher2_Benchmark.ev";
    testConfig["ElphyModelName"] = "BeelerReuter";
    testConfig["CellModelScheme"] = "ExplicitEuler";

    testConfig["SurfaceToVolumeRatio"] = "140";
    testConfig["MembraneCapacitance"] = "0.01";
    testConfig["CrankNicolsonTheta"] = "0.5";
    testConfig["ElphyIntegrator"] = "ExplicitEuler";
    testConfig["ThresholdVForActivation"]="-40.0";

    testConfig["EulerReAssemble"] = "0";
    testConfig["UseTNewForIextUpdate"] ="0";

    //testConfig["ConfigVerbose"]="5";

    testConfig["ElphyProblem"] = GetParam().problemName;
    Config::Initialize(testConfig);

    cmMain = new MainMonodomain();
    cmMain->Initialize();

  }

  void TearDown() override {
    Config::Close();
  }

  ~MonodomainTest() override {
    delete cmMain;
  }

};


//TODO: @Laura Lindner: Dieser Test zeigt, warum die Ergebnisse in den Testproblemen geändert werden mussten.
/*TEST_P(MonodomainTest, NextTStepConsistency){
  TimeSeries oldTS(0.0,GetParam().endTime, GetParam().deltaTime);
  TimeSeries newTS(0.0,GetParam().endTime, GetParam().deltaTime);

  EXPECT_DOUBLE_EQ(oldTS.FirstTStep(), newTS.FirstTStep());
  for(int s=0;s<oldTS.Steps();++s){
    EXPECT_DOUBLE_EQ(oldTS.NextTStepJ(), newTS.NextTimeStep());
    EXPECT_DOUBLE_EQ(oldTS.CurrentTStep(), newTS.CurrentTStep());
  }
  EXPECT_DOUBLE_EQ(oldTS.LastTStep(), newTS.LastTStep());
}*/

TEST_P(MonodomainTest, CheckValueWithoutDiffusion) {
  Vector &solution = cmMain->Run();
  auto evalResults = cmMain->Evaluate(solution);
  for (auto result: evalResults)
    ASSERT_NEAR(result, 0.0, TEST_TOLERANCE);
}

INSTANTIATE_TEST_SUITE_P(TestMonodomain, MonodomainTest, Values(
    TestMonodomainParameter(
        {"WithoutDiffusion", "Splitting", "Strang", 1, 0.00001, 0.00001, "IBTElphyModel",
         "discrete", "Stepfunction"}),
    TestMonodomainParameter(
        {"WithoutDiffusion", "Splitting", "Godunov", 2, 0.00001, 0.00001, "IBTElphyModel",
         "discrete", "Stepfunction"}) ,
    TestMonodomainParameter(
       {"HexaTestProblemWA", "Splitting", "Strang", 1, 0.00001, 0.006, "MElphyModel",
         "discrete", "Stepfunction"}),
    TestMonodomainParameter(
        {"Hexa2dTestProblem", "LinearImplicit", "", 1, 0.00001, 0.01, "MElphyModel", "discrete",
         "Stepfunction"}),
    TestMonodomainParameter(
        {"TetraTestProblem", "LinearImplicit", "", 1, 0.0001, 0.004, "MElphyModel", "linear", "Arctan"})
));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}
