#ifndef CARDMECH_TESTMONODOMAIN_HPP
#define CARDMECH_TESTMONODOMAIN_HPP

#include "../TestConfigurations.hpp"

struct TestMonodomainParameter {
  std::string problemName;
  std::string elphyTimeIntegration;
  std::string splittingMethod;
  int elphySubsteps;
  double deltaTime;
  double endTime;
  std::string elphyCellModelClass;
  std::string externalCurrentInSpace;
  std::string externalCurrentInTime;
};

#endif //CARDMECH_TESTMONODOMAIN_HPP
