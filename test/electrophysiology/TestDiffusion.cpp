#include "MainMonodomain.hpp"
#include "TestConfigurations.hpp"
#include "TestEnvironmentCardMech.hpp"

struct DiffusionParameter {
  int example{1};
  int minLevel{1};
  int maxLevel{1};
  int dimension{2};
  bool isTet{true};

  double deltaX(int level) const {
    return 1 / ((double) pow(2, level));
  }

  double deltaT(int level) const {
    return 12.5 * deltaX(level) * deltaX(level);
  }
};

class DiffusionTest : public TestWithParam<DiffusionParameter> {
protected:
  MainMonodomain *cmMain{};

  DiffusionTest() {}

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "Monodomain";
    testConfig["ElphyModel"] = "Diffusion";
    testConfig["ElphyLevel"] = std::to_string(GetParam().minLevel);
    testConfig["ElphySubsteps"] = "1";
    testConfig["ElphyMeshPart"] = "fourchamber";
    testConfig["ElphyPolynomialDegree"] = "1";
    testConfig["IsExternalCurrentSmooth"] = "0";
    testConfig["IextInPDE"] = "0";
    testConfig["ExternalCurrentSmoothingInSpace"]="discrete";

    testConfig["StartTime"] = "0.0";
    testConfig["EndTime"] = "0.5";
    testConfig["DeltaTime"] = "0.0001";
    //oder:
    //testConfig["IBTVentricleParameters"] = "CellModel/TenTusscher2_Benchmark.ev";

    testConfig["SurfaceToVolumeRatio"] = "1";
    testConfig["MembraneCapacitance"] = "1";
    testConfig["CrankNicolsonTheta"] = "0";

    testConfig["ElphyProblem"] = "DiffusionProblem" + std::to_string(GetParam().example);
    testConfig["ProblemDimension"] = std::to_string(GetParam().dimension);
    testConfig["ProblemGeometry"] = std::to_string(GetParam().isTet);

    testConfig["ConfigVerbose"] = "-1";
    testConfig["MeshVerbose"] = "-1";
    testConfig["AssembleVerbose"] = "-1";

    Config::Initialize(testConfig);

  }

  void TearDown() override {
    Config::Close();
  }

  ~DiffusionTest() override {
    delete cmMain;
  }

};

TEST_P(DiffusionTest, ElphyDiffusion) {
  cmMain = new MainMonodomain();

  std::vector<double> errors{};
  for (int l = GetParam().minLevel; l <= GetParam().maxLevel; ++l) {
    cmMain->ResetLevel(l);
    cmMain->Initialize();
    Vector &solution = cmMain->Run();
    auto evalResults = cmMain->Evaluate(solution);
    errors.emplace_back(evalResults[0]);
  }

  for (int l = 0; l < errors.size() - 1; ++l) {
    EXPECT_GT(errors[l], errors[l + 1]);
    EXPECT_GT(errors[l] / errors[l + 1], 1.5);
  }
}


INSTANTIATE_TEST_SUITE_P(MroueExamples, DiffusionTest, Values(
    DiffusionParameter{1, 1, 8, 2, false},
    DiffusionParameter{1, 1, 6, 3, false}
));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}