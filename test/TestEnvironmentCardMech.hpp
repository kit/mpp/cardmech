#ifndef TESTENVIRONMENTCARDMECH_HPP
#define TESTENVIRONMENTCARDMECH_HPP

#include "TestEnvironment.hpp"
#include "Vector.hpp"


static void printPythonVector(const std::string &name, const std::vector<double> &vec) {
  mout << name << " = [" << vec[0];
  for (int i = 1; i < vec.size(); ++i) {
    mout << ", " << vec[i];
  }
  mout << "]" << endl;
}

static void printPythonVector(const std::string &name, const std::vector<std::vector<double>> &vec,
                              int index = 0, int step = 1) {
  mout << name << " = [" << vec[0][index];
  for (int i = step; i < vec.size(); i += step) {
    mout << ", " << vec[i][index];
  }
  mout << "]" << endl;
}

#endif //TESTENVIRONMENTCARDMECH_HPP
