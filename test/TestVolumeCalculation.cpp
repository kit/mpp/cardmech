#include "TestEnvironment.hpp"
#include "Celltype.hpp"
#include "CardiacData.hpp"

struct CellParam {
  CELLTYPE type;
  std::vector<Point> corners;
  double volume;
};


class CellVolumeTest : public TestWithParam<CellParam> {
protected:
  double volume;
public:
  CellVolumeTest() = default;
};


TEST_P(CellVolumeTest, Volumes) {
  ASSERT_DOUBLE_EQ(Volume(GetParam().type, GetParam().corners), GetParam().volume);
}

INSTANTIATE_TEST_SUITE_P(TestVolumeFunctions, CellVolumeTest, Values(
    CellParam{TRIANGLE, {Point(0.0, 0.0, 0.0), Point(1.0, 0.0, 0.0), Point(0.0, 1.0, 0.0)}, 0.5},
    CellParam{TRIANGLE, {Point(0.0, 0.0, 0.0), Point(1.0, 0.0, 0.0), Point(0.5, 1.0, 0.0)}, 0.5},
    CellParam{TRIANGLE, {Point(0.0, 0.0, 0.0), Point(1.0, 0.0, 0.0), Point(0.0, 2.0, 0.0)}, 1.0},
    CellParam{TRIANGLE, {Point(0.0, 0.0, 0.0), Point(2.0, 0.0, 0.0), Point(1.0, 1.0, 0.0)}, 1.0},
    CellParam{QUADRILATERAL, {Point(0.0, 0.0, 0.0), Point(1.0, 0.0, 0.0), Point(1.0, 1.0, 0.0),
                              Point(0.0, 1.0, 0.0)}, 1.0},
    CellParam{QUADRILATERAL, {Point(0.0, 0.0, 0.0), Point(4.0, 0.0, 0.0), Point(3.0, 1.0, 0.0),
                              Point(1.0, 1.0, 0.0)}, 3.0},
    CellParam{TETRAHEDRON, {Point(0.0, 0.0, 0.0), Point(1.0, 0.0, 0.0), Point(0.0, 1.0, 0.0),
                            Point(0.0, 0.0, 1.0)}, 1.0 / 6.0},
    CellParam{TETRAHEDRON, {Point(0.0, 0.0, 0.0), Point(2.0, 0.0, 0.0), Point(1.0, 1.0, 0.0),
                            Point(0.0, 1.0, 3.0)}, 1.0},
    CellParam{HEXAHEDRON, {Point(0.0, 0.0, 0.0), Point(1.0, 0.0, 0.0), Point(1.0, 1.0, 0.0),
                           Point(0.0, 1.0, 0.0), Point(0.0, 0.0, 1.0), Point(1.0, 0.0, 1.0),
                           Point(1.0, 1.0, 1.0), Point(0.0, 1.0, 1.0)}, 1.0}
));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}