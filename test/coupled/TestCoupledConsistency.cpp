#include "TestEnvironmentCardMech.hpp"
#include "TestEnvironmentLib4.hpp"
#include "TestConfigurations.hpp"

#include "MCellModel.hpp"
#include "assemble/IElphyAssemble.hpp"
#include "assemble/IElasticity.hpp"
#include "coupled/problem/CoupledProblem.hpp"
#include "solvers/ElphySolver.hpp"
#include "solvers/DynamicMechanicsSolver.hpp"
#include "coupled/solvers/SegregatedSolver.hpp"

struct ElphyCombination {
  std::string ElphyModel;
  std::string ElphyCellModel;
};

/**
 * Tests if electrophysiological caluclations are consistent in the coupled context
 */
class CoupledConsistencyTest : public TestWithParam<ElphyCombination> {
protected:
  std::unique_ptr<CoupledProblem> problem = nullptr;
  std::unique_ptr<MTensionModel> cellModel = nullptr;
  std::unique_ptr<IElphyAssemble> elphyA = nullptr;
  std::unique_ptr<IElphyAssemble> coupledA = nullptr;
  std::unique_ptr<IElasticity> mechA = nullptr;

  CoupledConsistencyTest() {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;
    initializeConfig(testConfig);

    problem = GetCoupledProblem("ContractingBeam");
    problem->CreateMeshes(3, 2);

    auto elphyModel = GetElphyModel(GetParam().ElphyCellModel);
    cellModel = GetTensionModel("Rossi", std::move(elphyModel));
    elphyA = CreateElphyAssemble(*problem, cellModel->ElphyModel(), 1);
    coupledA = CreateElphyAssemble(*problem, *cellModel, 1);
    elphyA->ResetTime(0.0, 0.1, 0.0001);
    coupledA->ResetTime(0.0, 0.1, 0.0001);
    mechA = CreateFiniteElasticityAssemble(*problem, 1, false);
    mechA->ResetTime(0.0, 0.1, 0.0001);
  }

  void initializeConfig(std::map<std::string, std::string> &testConfig) {
    testConfig["Model"] = "Monodomain";
    testConfig["ElphyModel"] = GetParam().ElphyModel;
    testConfig["ElphyLevel"] = "2";
    testConfig["ElphyPolynomialDegree"] = "1";
    testConfig["ElphySubsteps"] = "1";
    testConfig["ElphyMeshPart"] = "fourchamber";

    testConfig["IextInPDE"] = "1";
    testConfig["ExternalCurrentSmoothingInTime"] = "Arctan";
    testConfig["ExternalCurrentSmoothingInSpace"] = "linear";

    testConfig["StartTime"] = "0.0";
    testConfig["EndTime"] = "0.1";
    testConfig["DeltaTime"] = "0.0001";
    testConfig["MechDeltaTime"] = "0.0001";
    testConfig["ElphyDeltaTime"] = "0.0001";
    //oder:
    //testConfig["IBTVentricleParameters"] = "CellModel/TenTusscher2_Benchmark.ev";

    testConfig["SurfaceToVolumeRatio"] = "140";
    testConfig["MembraneCapacitance"] = "0.01";
    testConfig["CrankNicolsonTheta"] = "0.5";

    testConfig["IntraLongitudinal"] = " 0.170";
    testConfig["ExtraLongitudinal"] = "0.620";
    testConfig["IntraTransversal"] = "0.019";
    testConfig["ExtraTransversal"] = "0.240";

    testConfig["ElphyProblem"] = "DiffusionProblem";
    testConfig["ProblemDimension"] = "3";
    testConfig["ProblemGeometry"] = "Tet";

    testConfig["ActiveMaterial"] = "Laplace";
    testConfig["QuasiCompressiblePenalty"] = "None";

    testConfig["LaplaceMat_k"] = "0";
    testConfig["LinearMat_Mu"] = "1";

    testConfig["Density"] = "0";

    Config::Initialize(testConfig);
  }

  void TearDown() override {
    Config::Close();
  }

  ~CoupledConsistencyTest() override = default;
};


TEST_P(CoupledConsistencyTest, CompareCellModels) {
  Vector elphyPot(elphyA->GetSharedDisc());
  Vector tensionPot(coupledA->GetSharedDisc());


  auto elphySolver = GetElphySolver(GetParam().ElphyModel, cellModel->ElphyModel(),
                                    elphyA->ExcitationData());
  auto tensionSolver = GetElphySolver(GetParam().ElphyModel, *cellModel,
                                      coupledA->ExcitationData());

  elphySolver->Initialize(*elphyA, elphyPot);
  tensionSolver->Initialize(*coupledA, tensionPot);

  Vectors elphyV(3, elphyPot, false);
  elphyV[0] = elphyPot;
  elphyV[1] = 0.0;  // Stretch
  elphyV[2] = 1.0; // Iota4

  Vectors tensionV(3, tensionPot, false);
  tensionV[0] = tensionPot;
  tensionV[1] = 0.0;  // Stretch
  tensionV[2] = 1.0; // Iota4

  while (!elphyA->IsFinished()) {
    elphySolver->Step(*elphyA, elphyV);
    tensionSolver->Step(*coupledA, tensionV);

    EXPECT_EQ(elphyA->Time(), coupledA->Time());
    ASSERT_MPPVECTOR_EQ(elphyV[0], tensionV[0]);
  }
}


TEST_P(CoupledConsistencyTest, CompareSolvers) {
  Vector elphyPot(0.0, elphyA->GetSharedDisc());
  Vector coupledPot(0.0, coupledA->GetSharedDisc());
  Vector displacement(0.0, mechA->GetSharedDisc());


  auto elphySolver = GetElphySolver(GetParam().ElphyModel, *cellModel,
                                    elphyA->ExcitationData());
  auto coupledElphySolver = GetElphySolver(GetParam().ElphyModel, *cellModel,
                                           coupledA->ExcitationData());
  auto coupledMechSolver = std::make_unique<ElastodynamicTimeIntegrator>("Euler");
  auto coupledSolver = SegregatedSolver(std::move(coupledElphySolver),
                                        std::move(coupledMechSolver), 1);


  Vectors elphyV(3, elphyPot, false);
  elphyV[1] = 0.0;  // Stretch
  elphyV[2] = 1.0; // Iota4

  Vectors coupledV(3, coupledPot, false);
  coupledV[1] = 0.0;  // Stretch
  coupledV[2] = 1.0; // Iota4

  Vectors coupledU(4, displacement, false);
  coupledU[1] = 0.0;  // Stretch
  coupledU[2] = 1.0;  // Iota4
  coupledU[3] = 0.0;  // Potential for plotting

  elphySolver->Initialize(*elphyA, elphyV[0]);
  coupledSolver.Initialize(*coupledA, *mechA, coupledV[0], coupledU[0]);
  ASSERT_MPPVECTOR_EQ(elphyA->ExternalCurrent(), coupledA->ExternalCurrent());

  while (!mechA->IsFinished()) {
    elphySolver->Step(*elphyA, elphyV);
    coupledSolver.Step(*coupledA, *mechA, coupledV, coupledU);

    EXPECT_EQ(elphyA->Time(), mechA->Time());
    ASSERT_MPPVECTOR_NEAR(elphyV[0], coupledV[0]);
  }
}

INSTANTIATE_TEST_SUITE_P(TestElphySolverConsistency, CoupledConsistencyTest, Values(
    ElphyCombination{"SemiImplicit", "BeelerReuter"}));


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig();
  return mppTest.RUN_ALL_MPP_TESTS();
}