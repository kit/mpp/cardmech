#include <MeshesCreator.hpp>
#include "TestEnvironmentCardMech.hpp"
#include "TestTransfers.hpp"
#include "CardiacData.hpp"
#include "coupled/transfers/MultiPartTransfer.hpp"


struct CTransferParam {
  int dim;
  int coarseLevel;
  int coarseDegree;
  int fineLevel;
  int fineDegree;
  std::string coarseChambers;
  std::string fineChambers;
  std::string meshName;
};

class TestCardiacTransfer : public TestWithParam<CTransferParam> {
protected:
  std::unique_ptr<Meshes> coarseMeshes;
  std::unique_ptr<Meshes> fineMeshes;
  std::shared_ptr<LagrangeDiscretization> coarseDisc;
  std::shared_ptr<MultiPartDiscretization> fineDisc;
  std::unique_ptr<Vector> coarse;
  std::unique_ptr<Vector> fine;

  TestCardiacTransfer() {

    fineMeshes = MeshesCreator(GetParam().meshName).
        WithLevel(GetParam().fineLevel).
        WithIncludeVector(getIncludedChambers(GetParam().fineChambers)).CreateUnique();

    coarseMeshes = MeshesCreator(GetParam().meshName).
        WithDistribution(fineMeshes->GetDistribution()).
        WithLevel(GetParam().coarseLevel).
        WithIncludeVector(getIncludedChambers(GetParam().coarseChambers)).CreateUnique();


    coarseDisc = std::make_shared<LagrangeDiscretization>(*coarseMeshes, GetParam().coarseDegree,
                                                          GetParam().dim);
    coarse = std::make_unique<Vector>(coarseDisc, GetParam().coarseLevel);
    fineDisc = std::make_shared<MultiPartDiscretization>(*fineMeshes, GetParam().fineDegree,
                                                         DomainPart, GetParam().dim);
    fine = std::make_unique<Vector>(fineDisc, GetParam().fineLevel);
  }

  void SetUp() override {
    setSeed((unsigned) std::time(nullptr));
  }

  virtual ~TestCardiacTransfer() {
  }
};


void FillCardiacVector(Vector &vec) {
  for (row r = vec.rows(); r != vec.rows_end(); ++r) {
    auto p = r();
    for (int i = 0; i < r.n(); ++i) {
      vec(r, i) = p[2];
    }
  }
}

void ASSERT_MULTIPART_CONSTANT(const Vector &vec, const std::vector<int> &included, double d, double eps=tolerance) {
  for (cell c = vec.cells(); c != vec.cells_end(); ++c) {
    if (std::find(included.begin(), included.end(), c.Subdomain()) == included.end())
      continue;

    auto z = vec.GetNodalPoints(*c);
    for (const auto &np: z) {
      row r = vec.find_row(np);
      for (int i = 0; i < r.n(); ++i) {
        EXPECT_NEAR(vec(r, i), d, eps);
      }
    }
  }
}

void ASSERT_MULTIPART_LINEAR(const Vector &vec, const std::vector<int> &included, double eps=tolerance) {
  for (cell c = vec.cells(); c != vec.cells_end(); ++c) {
    if (std::find(included.begin(), included.end(), c.Subdomain()) == included.end())
      continue;

    auto z = vec.GetNodalPoints(*c);
    for (const auto &np: z) {
      row r = vec.find_row(np);
      for (int i = 0; i < r.n(); ++i) {
        EXPECT_NEAR(vec(r, i), np[2], eps);
      }
    }
  }
}


TEST_P(TestCardiacTransfer, ConstantProject) {
  FillVectorConstant(*coarse, 0.0);
  FillVectorConstant(*fine, 2.0);

  MultiPartLagrangeTransfer T(*coarse, *fine);
  T.Project(*coarse, *fine);

  ASSERT_MULTIPART_CONSTANT(*coarse, fine->GetMesh().IncludedSubdomains(), 2.0, 1e-12);
}


TEST_P(TestCardiacTransfer, LinearProject) {
  bool heartGeo = coarse->GetMesh().Name().find("Heart") != std::string::npos;

  FillVectorConstant(*coarse, 0.0);
  FillCardiacVector(*fine);

  MultiPartLagrangeTransfer T(*coarse, *fine);
  T.Project(*coarse, *fine);

  ASSERT_MULTIPART_LINEAR(*coarse, fine->GetMesh().IncludedSubdomains(), heartGeo? 1e-12 : tolerance);
}

/*
TEST_P(TestCardiacTransfer, ConstantRestrict) {
  FillVectorConstant(*coarse, 0.0);
  FillVectorConstant(*fine, 2.0);

  MultiPartTransfer T(*coarse, *fine);
  T.Restrict(*coarse, *fine);

  ASSERT_RESTRICTION(*coarse, *fine, T.AsMatrix());
}

TEST_P(TestCardiacTransfer, LinearRestrict) {
  FillVectorConstant(*coarse, 0.0);
  FillCardiacVector(*fine);

  MultiPartTransfer T(*coarse, *fine);
  T.Restrict(*coarse, *fine);

  ASSERT_RESTRICTION(*coarse, *fine, T.AsMatrix());
}*/

TEST_P(TestCardiacTransfer, ConstantElongate) {
  FillVectorConstant(*coarse, 2.0);
  FillVectorConstant(*fine, 0.0);

  MultiPartLagrangeTransfer T(*coarse, *fine);
  T.Prolongate(*coarse, *fine);

  ASSERT_MULTIPART_CONSTANT(*fine, coarse->GetMesh().IncludedSubdomains(), 2.0);
}

TEST_P(TestCardiacTransfer, LinearElongate) {
  bool heartGeo = coarse->GetMesh().Name().find("Heart") != std::string::npos;

  FillCardiacVector(*coarse);
  FillVectorConstant(*fine, 0.0);

  MultiPartLagrangeTransfer T(*coarse, *fine);
  T.Prolongate(*coarse, *fine);

  ASSERT_MULTIPART_LINEAR(*fine, coarse->GetMesh().IncludedSubdomains(), heartGeo? 1e-12 : tolerance);
}


std::vector<CTransferParam>
CardiacTransferParameters(std::string chamber, std::string mesh, int dim = 1) {
  return std::vector<CTransferParam>{{dim, 0, 1, 0, 1, chamber, "fourchamber", mesh},
                                     {dim, 0, 1, 1, 1, chamber, "fourchamber", mesh},
                                     {dim, 0, 1, 2, 1, chamber, "fourchamber", mesh},
//                                     {dim, 0, 1, 3, 1, chamber, "fourchamber", mesh},
                                     {dim, 0, 2, 1, 1, chamber, "fourchamber", mesh},
//                                     {dim, 0, 2, 2, 1, chamber, "fourchamber", mesh},
//                                     {dim, 1, 2, 2, 1, chamber, "fourchamber", mesh},
                                     {dim, 0, 2, 0, 2, chamber, "fourchamber", mesh}
//                                     {dim, 0, 2, 1, 2, chamber, "fourchamber", mesh}
  };
}

INSTANTIATE_TEST_SUITE_P(FourChamberTriangle, TestCardiacTransfer,
                         ValuesIn(CardiacTransferParameters("fourchamber", "TransferBlock2DTet")));

INSTANTIATE_TEST_SUITE_P(FourChamberSquare, TestCardiacTransfer,
                         ValuesIn(CardiacTransferParameters("fourchamber", "TransferBlock2DQuad")));

INSTANTIATE_TEST_SUITE_P(FourChamberTetrahedron, TestCardiacTransfer,
                         ValuesIn(CardiacTransferParameters("fourchamber", "TransferBlock3DTet")));

INSTANTIATE_TEST_SUITE_P(FourChamberHexahedron, TestCardiacTransfer,
                         ValuesIn(CardiacTransferParameters("fourchamber", "TransferBlock3DQuad")));


INSTANTIATE_TEST_SUITE_P(MultiDimTriangle, TestCardiacTransfer,
                         ValuesIn(
                             CardiacTransferParameters("fourchamber", "TransferBlock2DTet", 3)));

INSTANTIATE_TEST_SUITE_P(MultiDimSquare, TestCardiacTransfer,
                         ValuesIn(
                             CardiacTransferParameters("fourchamber", "TransferBlock2DQuad", 3)));

INSTANTIATE_TEST_SUITE_P(MultiDimTetrahedron, TestCardiacTransfer,
                         ValuesIn(
                             CardiacTransferParameters("fourchamber", "TransferBlock3DTet", 3)));

INSTANTIATE_TEST_SUITE_P(MultiDimHexahedron, TestCardiacTransfer,
                         ValuesIn(
                             CardiacTransferParameters("fourchamber", "TransferBlock3DQuad", 3)));

INSTANTIATE_TEST_SUITE_P(VesselsTriangle, TestCardiacTransfer,
                         ValuesIn(CardiacTransferParameters("with_vessels", "TransferBlock2DTet")));

INSTANTIATE_TEST_SUITE_P(VesselsSquare, TestCardiacTransfer,
                         ValuesIn(
                             CardiacTransferParameters("with_vessels", "TransferBlock2DQuad")));

INSTANTIATE_TEST_SUITE_P(VesselsTetrahedron, TestCardiacTransfer,
                         ValuesIn(CardiacTransferParameters("with_vessels", "TransferBlock3DTet")));

INSTANTIATE_TEST_SUITE_P(VesselsHexahedron, TestCardiacTransfer,
                         ValuesIn(
                             CardiacTransferParameters("with_vessels", "TransferBlock3DQuad")));

INSTANTIATE_TEST_SUITE_P(FullHeart, TestCardiacTransfer, ValuesIn(std::vector<CTransferParam>{
    {1, 0, 1, 2, 1, "with_vessels", "fourchamber", "KovachevaHeart"},
    {1, 0, 2, 1, 1, "with_vessels", "fourchamber", "KovachevaHeart"}
}));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithScreenLogging().
      WithGeoPath(std::string(ProjectSourceDir) + "/geo/").
      WithConfigEntry("ClearDistribution", "0").
      WithConfigEntry("Distribution", "RCB");
  return mppTest.RUN_ALL_MPP_TESTS();
}