#include "TestEnvironmentCardMech.hpp"
#include "TestConfigurations.hpp"

#include "assemble/MonodomainSplitting.hpp"
#include "assemble/LagrangeElasticity.hpp"

#include "problem/DiffusionProblem.hpp"
#include "problem/ElasticityProblem.hpp"
#include "coupled/problem/DeformedProblem.hpp"

class DeformedDiffusionTest : public Test {
protected:
  std::unique_ptr<MonodomainSplitting> elphyA = nullptr;
  std::unique_ptr<LagrangeElasticity> mechA = nullptr;
  std::unique_ptr<CoupledProblem> problem = nullptr;
  std::unique_ptr<CoupledProblem> deformedProblem = nullptr;

  DeformedDiffusionTest() {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;
    initializeConfig(testConfig);

    problem = std::make_unique<CompositeCoupledProblem>(
        std::make_unique<DiffusionProblem>
            (std::array<double, 9>{8.0, 0.0, 0.0, 0.0, 4.0, 0.0, 0.0, 0.0, 4.0}),
        std::make_unique<DefaultElasticityProblem>());
    problem->CreateMeshes(2, 2);

    deformedProblem = std::make_unique<DeformedCoupledProblem>(
        std::make_unique<CompositeCoupledProblem>(
            std::make_unique<DiffusionProblem>
                (std::array<double, 9>{8.0, 0.0, 0.0, 0.0, 4.0, 0.0, 0.0, 0.0, 4.0}),
            std::make_unique<DefaultElasticityProblem>()));
    deformedProblem->CreateMeshes(2, 2);

    elphyA = std::make_unique<MonodomainSplitting>(*problem, 1);
    mechA = std::make_unique<LagrangeElasticity>(*problem, 1, false);
  }

  void initializeConfig(std::map<std::string, std::string> &testConfig) {
    testConfig["Model"] = "Monodomain";
    testConfig["ElphyModel"] = "Diffusion";
    testConfig["ElphyLevel"] = "2";
    testConfig["ElphyPolynomialDegree"] = "1";
    testConfig["ElphySubsteps"] = "1";
    testConfig["ElphyMeshPart"] = "fourchamber";

    testConfig["IextInPDE"] = "0";
    testConfig["ExternalCurrentSmoothingInTime"] = "Arctan";
    testConfig["ExternalCurrentSmoothingInSpace"] = "discrete";

    testConfig["StartTime"] = "0.0";
    testConfig["EndTime"] = "0.0001";
    testConfig["DeltaTime"] = "0.0001";
    //oder:
    //testConfig["IBTVentricleParameters"] = "CellModel/TenTusscher2_Benchmark.ev";

    testConfig["SurfaceToVolumeRatio"] = "1";
    testConfig["MembraneCapacitance"] = "1";
    testConfig["CrankNicolsonTheta"] = "0";

    testConfig["ElphyProblem"] = "DiffusionProblem";
    testConfig["ProblemDimension"] = "3";
    testConfig["ProblemGeometry"] = "Tet";

    testConfig["Mesh"] = "UnitBlock3DTet";

    Config::Initialize(testConfig);
  }

  void TearDown() override {
    Config::Close();
  }

  ~DeformedDiffusionTest() override {
  }

};

TEST_F(DeformedDiffusionTest, CheckReference) {
  Vector elphyVec(0.0, elphyA->GetSharedDisc());
  Vector mechVec(1.0, mechA->GetSharedDisc());
  Vector vel(0.0, mechA->GetSharedDisc());

  std::unordered_map<Point, Tensor> conductivities{};
  for (cell c = elphyVec.cells(); c != elphyVec.cells_end(); ++c) {
    conductivities.try_emplace(c(), problem->Conductivity(*c));
  }
  problem->UpdateDeformation(mechVec, vel, elphyVec);
  for (cell c = elphyVec.cells(); c != elphyVec.cells_end(); ++c) {
    auto referenceC = problem->Conductivity(*c);
    EXPECT_EQ(conductivities[c()], problem->Conductivity(*c));
  }
}

TEST_F(DeformedDiffusionTest, NoDeformation) {
  Vector elphyVec(0.0, elphyA->GetSharedDisc());
  Vector mechVec(0.0, mechA->GetSharedDisc());
  Vector vel(0.0, mechA->GetSharedDisc());

  deformedProblem->UpdateDeformation(mechVec, vel, elphyVec);

  for (cell c = elphyVec.cells(); c != elphyVec.cells_end(); ++c) {
    auto referenceC = problem->Conductivity(*c);
    auto deformedC = deformedProblem->Conductivity(*c);

    EXPECT_EQ(referenceC, deformedC);
  }
}

TEST_F(DeformedDiffusionTest, WithDeformation) {
  Vector elphyVec(0.0, elphyA->GetSharedDisc());
  Vector mechVec(0.0, mechA->GetSharedDisc());
  Vector vel(0.0, mechA->GetSharedDisc());
  for (row r = mechVec.rows(); r != mechVec.rows_end(); ++r) {
    auto x = r();
    mechVec(r, 0) = 2.0 * x[0];
    mechVec(r, 1) = 2.0 * x[1];
    mechVec(r, 2) = 3.0 * x[2];
  }
  deformedProblem->UpdateDeformation(mechVec, vel, elphyVec);

  for (cell c = elphyVec.cells(); c != elphyVec.cells_end(); ++c) {
    auto referenceC = problem->Conductivity(*c);
    auto deformedC = deformedProblem->Conductivity(*c);
    EXPECT_NE(referenceC, deformedC);
  }
}


TEST_F(DeformedDiffusionTest, StretchAlongFiber) {
  Vector elphyVec(0.0, elphyA->GetSharedDisc());
  Vector mechVec(0.0, mechA->GetSharedDisc());
  Vector vel(0.0, mechA->GetSharedDisc());
  for (row r = mechVec.rows(); r != mechVec.rows_end(); ++r) {
    mechVec(r, 0) = 2.0 * r()[0];
  }
  deformedProblem->UpdateDeformation(mechVec, vel, elphyVec);

  for (cell c = elphyVec.cells(); c != elphyVec.cells_end(); ++c) {
    auto referenceC = problem->Conductivity(*c);
    auto deformedC = deformedProblem->Conductivity(*c);

    // TODO: Just insert the right number instead.
    EXPECT_EQ((1.0 / 3.0) * referenceC[0][0], deformedC[0][0]);
  }
}


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}