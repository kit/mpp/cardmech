//
// Created by laura on 13.06.22.
//

#ifndef CARDMECH_TESTCARDIACBEAMCONFIGURATION_H
#define CARDMECH_TESTCARDIACBEAMCONFIGURATION_H

#include <map>



struct ConfigMaps {
    std::string confPath;
    std::map<std::string, std::string> defaultMap;
    std::map<std::string, std::string> additionalMap;
};



static const std::map<std::string, std::string> CARDIACBEAM_TEST_CONFIG{
    // ----- Problem Settings -----
    {"EulerReAssemble",      "0"},

    {"PressureIterations",   "10"},
    {"PressureDepth",        "0"},

// === Linear Solver === //
    {"LinearSolver",         "gmres"},
    {"LinearEpsilon",        "1e-8"},
    {"LinearReduction",      "1e-8"},
    {"LinearReduction",      "1e-11"},
    {"LinearSteps",          "10000"},

    {"BasePreconditioner",   "GaussSeidel"},
    {"Preconditioner",       "GaussSeidel"},

// =================================================================
// === Gating Solver ===============================================
    {"GatingSolver", "gmres"},
    {"GatingPreconditioner", "GaussSeidel"},
    {"GatingEpsilon", "1e-8"},
    {"GatingReduction", "1e-12"},

// === Elphy Solver ================================================
    {"ElphySolver", "gmres"},
    {"ElphyPreconditioner", "GaussSeidel"},
    {"ElphyEpsilon", "1e-8"},
    {"ElphyReduction", "1e-12"},

// === Stretch Solver ==============================================
    {"StretchSolver", "gmres"},
    {"StretchPreconditioner", "GaussSeidel"},
    {"StretchEpsilon", "1e-8"},
    {"StretchReduction", "1e-12"},

// === Mechanics Solver ============================================
    {"MechSolver",                   "gmres"},
    {"MechPreconditioner",           "GaussSeidel"},
    {"MechEpsilon",                  "1e-8"},
    {"MechReduction",                "1e-12"},
    {"MechSteps",                    "10000"},

// === Newton Method === //
    {"NewtonEpsilon",                "1e-8"},
    {"NewtonReduction",              "1e-10"},
    {"NewtonSteps",                  "200"},
    {"NewtonLineSearchSteps",        "50"},
    {"NewtonLinearizationReduction", "1e-12"},

    {"Smoother",                     "GaussSeidel"},
    {"presmoothing",                 "3"},
    {"postsmoothing",                "3"},

    {"Overlap_Distribution",     "0"},

    {"DebugLevel",               "0"},
    {"TimeLevel",                "0"},
    {"precision",                "10"},

    {"MechPlot",                 "0"},
    {"PlotVertexPotential",      "0"},

    {"PlotVTK",                  "0"},
    {"PlottingSteps",            "1"},

    {"h0",                       "1e-4"},
    {"h_min",                    "1e-6"},

    {"PlotStress",               "0"},

    {"plevel",                   "0"},

    {"Model",                        "ActiveStrainElasticity"},
    {"Mesh",                         "BenchmarkBeam3DTet"},

    {"Incompressible",               "false"},
    {"QuasiCompressiblePenalty",     "None"},
    {"VolumetricPenalty",            "2"},
    {"PermeabilityPenalty",          "2"},


// === Material Parameters ===//


/**************************************************
 *  Linear Material
 **************************************************/
    {"LinearMat_Mu" , "2"},
    {"LinearMat_Lambda", "4"},

/**************************************************
 *  Bonet Material
 **************************************************/
        {"GuccioneMat_C","2"},
        {"GuccioneMat_bf", "8"},
        {"GuccioneMat_bs", "2"},
        {"GuccioneMat_bfs", "4"},

// === Verbose Levels ===//
    {"AssembleVerbose",          "-1"},
    {"MultigridVerbose",         "-1"},
    {"BaseSolverVerbose",        "-1"},
    {"LinearVerbose",            "-1"},
    {"NewtonVerbose",            "-1"},
    {"MechVerbose",              "-1"},
    {"ElphyVerbose",             "-1"},
    {"ElphyLinearSolverVerbose", "-1"},
    {"ElphySolverVerbose",       "-1"}
    };


#endif //CARDMECH_TESTCARDIACBEAMCONFIGURATION_H


