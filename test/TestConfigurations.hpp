#ifndef TESTCONFIGURATIONS_H
#define TESTCONFIGURATIONS_H

#include <map>
#include <string>


struct ConfigMaps {
  std::string confPath;
  std::map<std::string, std::string> defaultMap;
  std::map<std::string, std::string> additionalMap;
};



static const std::map<std::string, std::string> CARDIAC_TEST_CONFIG{
    // ----- Problem Settings -----

    {"GeoPath",              std::string(ProjectSourceDir) + "/geo/"},
    {"EulerReAssemble",      "0"},

    {"PressureIterations",   "10"},
    {"PressureDepth",        "0"},

// === Linear Solver === //
    {"LinearSolver",         "gmres"},
    {"LinearEpsilon",        "1e-8"},
    {"LinearReduction",      "1e-8"},
    {"LinearReduction",      "1e-11"},
    {"LinearSteps",          "10000"},

    {"BasePreconditioner",   "GaussSeidel"},
    {"Preconditioner",       "GaussSeidel"},

// =================================================================
// === Gating Solver ===============================================
    {"GatingSolver", "gmres"},
    {"GatingPreconditioner", "GaussSeidel"},
    {"GatingEpsilon", "1e-8"},
    {"GatingReduction", "1e-12"},

// === Elphy Solver ================================================
    {"ElphySolver", "gmres"},
    {"ElphyPreconditioner", "GaussSeidel"},
    {"ElphyEpsilon", "1e-8"},
    {"ElphyReduction", "1e-12"},

// === Stretch Solver ==============================================
    {"StretchSolver", "gmres"},
    {"StretchPreconditioner", "GaussSeidel"},
    {"StretchEpsilon", "1e-8"},
    {"StretchReduction", "1e-12"},

// === Mechanics Solver ============================================
    {"MechSolver",                   "gmres"},
    {"MechPreconditioner",           "GaussSeidel"},
    {"MechEpsilon",                  "1e-8"},
    {"MechReduction",                "1e-12"},
    {"MechSteps",                    "10000"},

// === Newton Method === //
    {"NewtonEpsilon",                "1e-8"},
    {"NewtonReduction",              "1e-10"},
    {"NewtonSteps",                  "200"},
    {"NewtonLineSearchSteps",        "50"},
    {"NewtonLinearizationReduction", "1e-12"},

    {"Smoother",                     "GaussSeidel"},

    {"presmoothing",                 "3"},
    {"postsmoothing",                "3"},

    {"Overlap_Distribution",     "0"},

    {"DebugLevel",               "0"},
    {"TimeLevel",                "0"},
    {"precision",                "10"},

    {"MechPlot",                 "0"},
    {"PlotVertexPotential",      "0"},

    {"PlotVTK",                  "0"},
    {"PlottingSteps",            "1"},

    {"h0",                       "1e-4"},
    {"h_min",                    "1e-6"},

    {"PlotStress",               "0"},

    {"plevel",                   "0"},
    // === Verbose Levels ===//
    {"AssembleVerbose",          "1"},
    {"ConfigVerbose",            "2"},
    {"MultigridVerbose",         "-1"},
    {"BaseSolverVerbose",        "-1"},
    {"LinearVerbose",            "1"},
    {"NewtonVerbose",            "1"},
    {"MechVerbose",              "1"},
    {"ElphyVerbose",             "-1"},
    {"ElphyLinearSolverVerbose", "-1"},
    {"ElphySolverVerbose",       "-1"}
};

const std::map<std::string, std::string> defaultEllipsoidConfig = {
    // ----- Problem Settings -----

    {"GeoPath",                      "../geo/tetrahedral/"},
    {"EulerReAssemble",              "0"},

// === Linear Solver === //
    {"LinearSolver",                 "gmres"},
    {"LinearEpsilon",                "1e-8"},
    {"LinearReduction",              "1e-8"},
    {"LinearReduction",              "1e-11"},
    {"LinearSteps",                  "10000"},

    {"Preconditioner",               "LIB_PS"},

// =================================================================
// === Gating Solver ===============================================
    {"GatingSolver",                 "gmres"},
    {"GatingPreconditioner",         "GaussSeidel"},
    {"GatingEpsilon",                "1e-8"},
    {"GatingReduction",              "1e-12"},

// === Elphy Solver ================================================
    {"ElphySolver",                  "gmres"},
    {"ElphyPreconditioner",          "GaussSeidel"},
    {"ElphyEpsilon",                 "1e-8"},
    {"ElphyReduction",               "1e-12"},

// === Stretch Solver ==============================================
    {"StretchSolver",                "gmres"},
    {"StretchPreconditioner",        "GaussSeidel"},
    {"StretchEpsilon",               "1e-8"},
    {"StretchReduction",             "1e-12"},

// === Mechanics Solver ============================================
    //{"MechSolver",                 "BiCGStab"},
    {"MechSolver",                   "gmres"},
    {"MechPreconditioner",           "LIB_PS"},
    {"MechEpsilon",                  "1e-8"},
    {"MechReduction",                "1e-12"},
    {"MechSteps",                    "10000"},

// === Newton Method === //
    {"NewtonEpsilon",                "1e-7"},
    {"NewtonReduction",              "1e-10"},
    {"NewtonSteps",                  "200"},
    {"NewtonLineSearchSteps",        "50"},
    {"NewtonLinearizationReduction", "1e-12"},

    {"Smoother",                     "GaussSeidel"},

    {"presmoothing",                 "3"},
    {"postsmoothing",                "3"},

    {"Overlap_Distribution",         "0"},

    {"DebugLevel",                   "0"},
    {"TimeLevel",                    "0"},
    {"precision",                    "10"},

    {"Model",                        "NoDiffusion"},
    {"Mesh",                         "OrientedEllipsoid"},

    {"ElphyLevel",                   "1"},
    {"MechLevel",                    "0"},

    {"StartTime",                    "0.0"},
    {"EndTime",                      "0.2"},
    {"DeltaTime",                    "0.002"},
    {"DeltaTimeMin",                 "0.0001"},
    {"DeltaTimeMax",                 "0.001"},

    {"MechPlot",                     "0"},
    {"PlotVertexPotential",          "0"},

    {"PlotVTK",                      "0"},
    {"PlottingSteps",                "1"},

    {"h0",                           "1e-4"},
    {"h_min",                        "1e-6"},

    {"Model",                        "StrainElasticity"},
    {"MechProblem",                  "ConstantPressure"},

    {"MechDiscretization",           "linear"},
    {"MechSubsteps",                 "32"},
    {"MechMeshPart",                 "witharteries"},
    {"MechScaling",                  "0.115"},

    {"ActiveStrain",                 "0"},
    {"ActiveMaterial",               "Linear"},

    {"PassiveMaterial",              "Linear"},
    {"Incompressible",               "false"},
    {"QuasiCompressiblePenalty",     "None"},
    {"VolumetricPenalty",            "20000"},
    {"PermeabilityPenalty",          "20000"},

    {"LinearMat_Lambda",             "0.1"},
    {"LinearMat_Mu",                 "0.1"},

    {"ConstantPressure",             "0"},

    {"PressureSteps",                "10"},
    {"UsePrestress",                 "0"},

    {"PlotStress",                   "0"},
    {"PlotVertexPotential",          "0"},
    {"PlotCellPotential",            "0"},

    {"ElphyProblem",                 "Monodomain"},

    {"ElphyDiscretization",          "linear"},
    {"ElphyDiscretization",          "serendepity"},

    {"ElphyLevel",                   "0"},
    {"ElphySubsteps",                "32"},

    {"ElphyMeshPart",                "fourchamber"},

    {"IsExternalCurrentSmooth",      "0"},

    {"SurfaceToVolumeRatio",         "140"},
    {"MembraneCapacitance",          "0.01"},

    {"IntraLongitudinal",            "0.170"},
    {"ExtraLongitudinal",            "0.620"},
    {"IntraTransversal",             "0.019"},
    {"ExtraTransversal",             "0.240"},

    {"ElphySplittingMethod",         "Strang"},
    {"CrankNicolsonTheta",           "0.5"},

    {"ElphyIntegrator",              "ExplicitEuler"},

    {"ElphyModelClass",              " IBTElphyModel"},
    {"VentricleModel",               "IBT"},
    {"AtriaModel",                   "IBT"},
    {"TensionModel",                 "IBT"},

    {"ElphyParameters",              "../cellmodels/data/TenTusscher.ev"},

    {"TensionParameters",            "../lib/LibCellModel/data/CellModel/Land17_TT2.fv"},

// =================================================================
    {"IBTVentricleModel",            "TenTusscher2"},
    {"IBTVentricleParameters",       "../lib/LibCellModel/data/CellModel/TenTusscher2_Benchmark.ev"},

    {"IBTAtriaModel",                "TenTusscher2"},
    {"IBTAtriaParameters",           "../lib/LibCellModel/data/CellModel/TenTusscher2_Benchmark.ev"},

    {"IBTTensionModel",              "Land17"},
    {"IBTTensionParameters",         "../lib/LibCellModel/data/CellModel/Land17_TT2.fv"},
// =================================================================
    {"plevel",                       "0"}
};

#endif //TESTCONFIGURATIONS_H
