#include <cellmodels/solvers/MElphySolver.hpp>
#include "TimeSeries.hpp"
#include <MeshesCreator.hpp>
#include <LagrangeDiscretization.hpp>
#include "TestEnvironmentCardMech.hpp"

#include "MFitzHughNagumo.hpp"
#include "MBeelerReuter.hpp"
#include "MTenTusscher.hpp"

template<class T>
class MElphyTest : public Test {
protected:
  std::unique_ptr<MElphyModel> cellModel;
  std::unique_ptr<MElphySolver> cellSolver;
  std::unique_ptr<Vector> dummyData;

  int powerTwoSteps(int n) {
    return (int) (pow(2, n) + 1e-4);
  }

  void printPythonVector(const std::string &name, const std::vector<double> vec) {
    mout << name << " = [" << vec[0];
    for (int i = 1; i < vec.size(); ++i) {
      mout << ", " << vec[i];
    }
    mout << "]" << endl;
  }

  void FillODESolution(TimeSeries &TS, std::vector<double> &t, std::vector<double> &v) {
    t.emplace_back(TS.FirstTStep());
    while(TS.CurrentTStep() < TS.LastTStep()){
      t.emplace_back(TS.NextTimeStep());
    }
    TS.Reset();
    cellSolver->Solve(TS, v);
  }

  std::vector<double> GlobalError(const std::string &methodName,
                                  std::vector<double> &error) {
    int O = 14;
    int N = 20;

    std::vector<double> refTime;
    std::vector<double> ref;
    cellSolver = std::make_unique<MElphySolver>(*cellModel, *dummyData, "RungeKutta4");
    TimeSeries TS(0.0, 0.4 * cellModel->TimeScale(), powerTwoSteps(N));
    FillODESolution(TS, refTime, ref);

    std::vector<std::vector<double>> time(N - O);
    std::vector<std::vector<double>> value(N - O);
    std::vector<std::vector<double>> gating(N - O);
    cellSolver = std::make_unique<MElphySolver>(*cellModel, *dummyData, methodName);
    for (int n = O; n < N; ++n) {
      TS.Reset(0.0, 0.4 * cellModel->TimeScale(), powerTwoSteps(n));
      FillODESolution(TS, time[n - O], value[n - O]);
    }

    error.resize(N - O);
    for (int n = O; n < N; ++n) {
      double e = 0.0;
      int dn = powerTwoSteps(N - n);
      for (int i = 0; i < value[n - O].size(); ++i) {
        double t = time[n - O][i];
        double tref = refTime[dn * i];

        double v = value[n - O][i];
        double r = ref[dn * i];
        e = std::max(e, abs(v - r));
      }
      error[n - O] = e;
    }
    return error;
  }

public:
  MElphyTest() {
    cellModel = std::make_unique<T>();

    auto meshes = MeshesCreator().
        WithMeshName("Interval").
        WithCellData(DataContainer({1, 0, 0, 0, 1, 0, 0, 0, 1})).
        WithVertexData(DataContainer({0.0, 0.002, 1.0})).
        CreateUnique();
    auto disc = std::make_shared<const LagrangeDiscretization>(*meshes, 1);
    dummyData = std::make_unique<Vector>(0.0, disc);
  }

  virtual ~MElphyTest() = default;
};

using testing::Types;


typedef Types<MFitzHughNagumo, MBeelerReuter/*, MTenTusscher*/> CellModelTypes; //TenTusscherModel is too stiff, explicit methods will not have the right convergence rate
TYPED_TEST_SUITE(MElphyTest, CellModelTypes);

TYPED_TEST(MElphyTest, ConvergenceEuler) {
  SelectExternalCurrentFunction("Arctan");
  this->cellModel->SetExternal(0.0, 0.002,20.0);

  std::vector<double> error;
  this->GlobalError("ExplicitEuler", error);
  for (int i = 1; i < error.size(); ++i) {
    if (error[i] < 1e-12) continue;
    mout << "i=" << i << "error(i)=" << error[i] << endl;
    EXPECT_GE(log2(error[i - 1] / error[i]), 1.0 - 0.1);
  }
}

TYPED_TEST(MElphyTest, ConvergenceRK2) {
  SelectExternalCurrentFunction("Arctan");
  this->cellModel->SetExternal(0.0, 0.002,20.0);

  std::vector<double> error;
  this->GlobalError("RungeKutta2", error);
  for (int i = 1; i < error.size(); ++i) {
    if (error[i] < 1e-12) continue;
    EXPECT_GE(log2(error[i - 1] / error[i]), 2.0 - 0.1);
  }
}

TYPED_TEST(MElphyTest, ConvergenceRK4) {
  SelectExternalCurrentFunction("Arctan");
  this->cellModel->SetExternal(0.0, 0.002,20.0);

  std::vector<double> error;
  this->GlobalError("RungeKutta4", error);
  for (int i = 1; i < error.size(); ++i) {
    if (error[i] < 1e-12) continue;
    EXPECT_GE(log2(error[i - 1] / error[i]), 4.0 - 0.1);
  }
}

TYPED_TEST(MElphyTest, PeakPotential) {
  SelectExternalCurrentFunction("Arctan");
  this->cellModel->SetExternal(0.0, 0.002,325);

  std::vector<double> refTime;
  std::vector<double> ref;
  this->cellSolver = std::make_unique<MElphySolver>(*(this->cellModel), *(this->dummyData),
                                                    "RungeKutta4");
  TimeSeries TS(0.0, 0.4 * this->cellModel->TimeScale(), this->powerTwoSteps(14));
  this->FillODESolution(TS, refTime, ref);

  EXPECT_GE(*(std::max_element(ref.begin(), ref.end())), 10.0);
}


TYPED_TEST(MElphyTest, RestingPotential) {
  SelectExternalCurrentFunction("Arctan");
  this->cellModel->SetExternal(0.0, 0.002,20.0);

  std::vector<double> refTime;
  std::vector<double> ref;
  this->cellSolver = std::make_unique<MElphySolver>(*(this->cellModel), *(this->dummyData),
                                                    "RungeKutta4");
  TimeSeries TS(0.0, 0.6 * this->cellModel->TimeScale(), this->powerTwoSteps(16));
  this->FillODESolution(TS, refTime, ref);

  EXPECT_NEAR(ref[0], ref[ref.size() - 1], 1.0);
}


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}
