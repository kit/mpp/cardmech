#include <MeshesCreator.hpp>
#include <Vector.hpp>
#include <LagrangeDiscretization.hpp>
#include <MCellModel.hpp>
#include "TestEnvironmentCardMech.hpp"
#include "solvers/MElphySolver.hpp"
#include "TimeSeries.hpp"
#include "MFitzHughNagumo.hpp"
#include "MBeelerReuter.hpp"
#include "MTenTusscher.hpp"
#include "MRossi.hpp"


template<class T>
class MTensionTest : public Test {
protected:
  std::unique_ptr<MTensionModel> cellModel;
  std::unique_ptr<Vector> dummyData;

public:
  MTensionTest() {
    cellModel = std::make_unique<MRossi>(std::make_unique<T>());

    auto meshes = MeshesCreator().
        WithMeshName("Interval").
        WithCellData(DataContainer({1, 0, 0, 0, 1, 0, 0, 0, 1})).
        WithVertexData(DataContainer({0.0, 0.002, 1.0})).
        CreateUnique();
    auto disc = std::make_shared<const LagrangeDiscretization>(*meshes, 1);
    dummyData = std::make_unique<Vector>(0.0, disc);
  }

  virtual ~MTensionTest() = default;
};

using testing::Types;
typedef Types<MFitzHughNagumo, MBeelerReuter, MTenTusscher> CellModelTypes;
TYPED_TEST_SUITE(MTensionTest, CellModelTypes);

TYPED_TEST(MTensionTest, EulerValues) {
  SelectExternalCurrentFunction("Arctan");
  this->cellModel->SetExternal(0.0, 0.002, 20.0);

  TimeSeries timeSeries(0.0, 400.0, 0.0001);

  MElphySolver elphySolver(this->cellModel->ElphyModel(), *(this->dummyData), "ExplicitEuler");
  std::vector<double> elphyValues{};
  elphySolver.Solve(timeSeries, elphyValues);

  timeSeries.Reset();
  MElphySolver tensionSolver(*(this->cellModel), *(this->dummyData), "ExplicitEuler");
  std::vector<double> tensionValues{};
  tensionSolver.Solve(timeSeries, tensionValues);

  for (int i = 0; i < elphyValues.size(); ++i) {
    EXPECT_EQ(elphyValues[i], tensionValues[i]);
  }

  auto elphyMax = *(std::max_element(elphyValues.begin(), elphyValues.end()));
  auto tensionMax = *(std::max_element(tensionValues.begin(), tensionValues.end()));

  EXPECT_GT(elphyMax, 0.0);
  EXPECT_GT(tensionMax, 0.0);
}


TYPED_TEST(MTensionTest, RK2Values) {
  SelectExternalCurrentFunction("Arctan");
  this->cellModel->SetExternal(0.0, 0.002, 20.0);

  TimeSeries timeSeries(0.0, 400.0, 0.0002);

  MElphySolver elphySolver(this->cellModel->ElphyModel(), *(this->dummyData), "RungeKutta2");
  std::vector<double> elphyValues{};
  elphySolver.Solve(timeSeries, elphyValues);

  timeSeries.Reset();
  MElphySolver tensionSolver(*(this->cellModel), *(this->dummyData), "RungeKutta2");
  std::vector<double> tensionValues{};
  tensionSolver.Solve(timeSeries, tensionValues);

  for (int i = 0; i < elphyValues.size(); ++i) {
    EXPECT_EQ(elphyValues[i], tensionValues[i]);
  }

  auto elphyMax = *(std::max_element(elphyValues.begin(), elphyValues.end()));
  auto tensionMax = *(std::max_element(tensionValues.begin(), tensionValues.end()));

  EXPECT_GT(elphyMax, 0.0);
  EXPECT_GT(tensionMax, 0.0);
}


TYPED_TEST(MTensionTest, RK4Values) {
  SelectExternalCurrentFunction("Arctan");
  this->cellModel->SetExternal(0.0, 0.002, 20.0);

  TimeSeries timeSeries(0.0, 400.0, 0.0002);

  MElphySolver elphySolver(this->cellModel->ElphyModel(), *(this->dummyData), "RungeKutta4");
  std::vector<double> elphyValues{};
  elphySolver.Solve(timeSeries, elphyValues);

  timeSeries.Reset();
  MElphySolver tensionSolver(*(this->cellModel), *(this->dummyData), "RungeKutta4");
  std::vector<double> tensionValues{};
  tensionSolver.Solve(timeSeries, tensionValues);


  for (int i = 0; i < elphyValues.size(); ++i) {
    EXPECT_EQ(elphyValues[i], tensionValues[i]);
  }

  auto elphyMax = *(std::max_element(elphyValues.begin(), elphyValues.end()));
  auto tensionMax = *(std::max_element(tensionValues.begin(), tensionValues.end()));

  EXPECT_GT(elphyMax, 0.0);
  EXPECT_GT(tensionMax, 0.0);
}

TYPED_TEST(MTensionTest, StretchValues) {
  SelectExternalCurrentFunction("Arctan");
  this->cellModel->SetExternal(0.0, 0.002, 20.0);

  TimeSeries timeSeries(0.0, 250.0, 0.0002);
  MElphySolver tensionSolver(*(this->cellModel), *(this->dummyData), "RungeKutta2");

  std::vector<std::vector<double>> tensionValues{};
  tensionSolver.Solve(timeSeries, tensionValues);

  //printPythonVector("potential", tensionValues, 0);
  //printPythonVector("calcium", tensionValues, this->cellModel->CalciumIndex());
  //printPythonVector("stretch", tensionValues, this->cellModel->ElphySize(), 10);

  std::vector<double> stretch{};
  for (const auto &val: tensionValues) {
    stretch.emplace_back(val[this->cellModel->ElphySize()]);
  }
  auto minIndex = std::min_element(stretch.begin(), stretch.end());
  mout << "Min Stretch = " << *minIndex << endl;

  EXPECT_TRUE(true);
}

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).
      WithScreenLogging().
      WithPPM();
  return mppTest.RUN_ALL_MPP_TESTS();
}