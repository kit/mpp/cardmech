#ifndef MTEST_HPP
#define MTEST_HPP

#include <numbers>

#include "MCellModel.hpp"

class MTest : public MElphyModel {
protected:
  double initialPotential() const override {
    return 0.0;
  }

public:
  explicit MTest() : MElphyModel(1, 1.0, 1) {
  }

  int CalciumIndex() const override {
    return 1;
  }

  int GatingIndex() const override {
    return 1;
  }

  void Initialize(Vectors &VW) override {
    VW[0] = 0;
  };

  void Initialize(std::vector<double> &vw) override {
    InitialValues(0, vw);
  }

  double IExt(double t) const override {
    return t;
  }

  void RHSUpdate(const std::vector<double> &var, std::vector<double> &g,
                 double t) const override {
    g[0] = Diff(t, var);
  }

  void UpdateValues(double dt, std::vector<double> &var,
                    const std::vector<double> &g) const override {
    var[0] += dt * g[0];
  }

  virtual void InitialValues(double t, std::vector<double> &vw) = 0;

  virtual double Diff(double t, const std::vector<double> &vw) const = 0;

  virtual double Value(double t, const std::vector<double> &vw) const = 0;

};

class MExact : public MTest {
public:
  MExact() : MTest() {}

  double Value(double t, const std::vector<double> &vw) const override {
    return sin(2 * std::numbers::pi * t);
  }

  double Diff(double t, const std::vector<double> &vw) const override {
    return 2 * std::numbers::pi * cos(2 * std::numbers::pi * t);
  }

  void InitialValues(double t, std::vector<double> &vw) override {
    vw[0] = Value(0, vw);
  }
};


class MNonExact : public MTest {
public:
  MNonExact() : MTest() {}

  double Value(double t, const std::vector<double> &vw) const override {
    return 0.0;
  }

  double Diff(double t, const std::vector<double> &vw) const override {
    return sin((vw[0] + t) * (vw[0] + t));
  }

  void InitialValues(double t, std::vector<double> &vw) override {
    vw[0] = -1.0;
  }
};


#endif //MTEST_HPP
