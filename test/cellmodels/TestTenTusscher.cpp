#include <cellmodels/solvers/MElphySolver.hpp>
#include "TimeSeries.hpp"
#include <fstream>

#include "TestTenTusscher.hpp"


/*
TEST_F(MTenTusscherTest, TestVariables){
  SelectExternalCurrentFunction("Stepfunction");
  std::ifstream variableFile(std::string(ProjectSourceDir) + "/test/cellmodels/data/TenTusscherMyo.txt");
  ASSERT_TRUE(variableFile.is_open());

  std::vector<std::vector<double>> fileData{};
  std::string variableLine;
  char* lineValue;
  while (variableFile){
    try{
      std::getline(variableFile, variableLine);
      if(variableLine.empty()) break;
      auto lineData = splitToDouble(variableLine, "\t");
      fileData.emplace_back(lineData);
    } catch(std::exception &e){
      Warning("line could not be processed!")
      mout << variableLine << endl;
    }
  }
  variableFile.close();
  this->cellModel->SetExternal(0.0, 0.003, 30.0);

  std::vector<double> refTime;
  std::vector<std::vector<double>> ref;
  this->cellSolver = std::make_unique<MElphySolver>(*(this->cellModel), *(this->dummyData),
                                                    "ExponentialIntegrator");
  TimeSeries TS(0.0, 600.0, 0.01);
  this->FillODESolution(TS, refTime, ref);
  this->printPythonVector("Potential", ref, 0, 100);

  int j=0;
  for(int i = 0; i< ref.size();++i){
    if(refTime[i] == fileData[j][0]){
      EXPECT_NEAR(ref[i][0], fileData[j][1], 1e-3);
      j++;
    }
    else{
      std::cout<<" Time not found: "<<refTime[i] <<std::endl;
    }
}*/

TEST_F(MTenTusscherTest, TestExplicitEuler) {
  SelectExternalCurrentFunction("Stepfunction");
  this->cellModel->SetExternal(0.0, 0.003, 30.0);

  std::vector<double> refTime;
  std::vector<std::vector<double>> ref;
  this->cellSolver = std::make_unique<MElphySolver>(*(this->cellModel), *(this->dummyData),
                                                    "ExplicitEuler");
  TimeSeries TS(0.0, 600.0, 0.001);
  this->FillODESolution(TS, refTime, ref);
  this->printPythonVector("Potential", ref, 0, 1000);
  for (int i = 0; i < eeReference.size(); ++i) {
    EXPECT_NEAR(ref[i*1000][0], eeReference[i], 1e-3);
  }
}

TEST_F(MTenTusscherTest, TestRungeKutta2) {
  SelectExternalCurrentFunction("Stepfunction");
  this->cellModel->SetExternal(0.0, 0.003, 30.0);

  std::vector<double> refTime;
  std::vector<std::vector<double>> ref;
  this->cellSolver = std::make_unique<MElphySolver>(*(this->cellModel), *(this->dummyData),
                                                    "RungeKutta2");
  TimeSeries TS(0.0, 600.0, 0.001);
  this->FillODESolution(TS, refTime, ref);
  this->printPythonVector("Potential", ref, 0, 1000);
  for (int i = 0; i < rk2Reference.size(); ++i) {
    EXPECT_NEAR(ref[i*1000][0], rk2Reference[i], 1e-3);
  }
}

TEST_F(MTenTusscherTest, TestRungeKutta4) {
  SelectExternalCurrentFunction("Stepfunction");
  this->cellModel->SetExternal(0.0, 0.003, 30.0);

  std::vector<double> refTime;
  std::vector<std::vector<double>> ref;
  this->cellSolver = std::make_unique<MElphySolver>(*(this->cellModel), *(this->dummyData),
                                                    "RungeKutta4");
  TimeSeries TS(0.0, 600.0, 0.001);
  this->FillODESolution(TS, refTime, ref);
  this->printPythonVector("Potential", ref, 0, 1000);
  for (int i = 0; i < rk4Reference.size(); ++i) {
    EXPECT_NEAR(ref[i*1000][0], rk4Reference[i], 1e-3);
  }
}

TEST_F(MTenTusscherTest, RestingPotential) {
  SelectExternalCurrentFunction("Stepfunction");
  this->cellModel->SetExternal(0.0, 0.003, 30.0);

  std::vector<double> refTime;
  std::vector<std::vector<double>> ref;
  this->cellSolver = std::make_unique<MElphySolver>(*(this->cellModel), *(this->dummyData),
                                                    "ExponentialIntegrator");
  TimeSeries TS(0.0, 600.0, 0.001);

  this->FillODESolution(TS, refTime, ref);
  this->printPythonVector("Potential", ref, 0, 1000);
  EXPECT_NEAR(ref[0][0], ref[ref.size() - 1][0], 1.0);
}


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}
