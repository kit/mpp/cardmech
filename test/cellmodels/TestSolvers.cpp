#include <MTest.hpp>
#include <cellmodels/solvers/MElphySolver.hpp>
#include "TimeSeries.hpp"
#include <BeelerReuter.h>
#include <MBeelerReuter.hpp>
#include <LagrangeDiscretization.hpp>
#include <MeshesCreator.hpp>
#include "TestEnvironmentCardMech.hpp"

struct SolverParam {
  std::string scheme;
  int expectedOrder;
};

class MCellSolverTest : public TestWithParam<SolverParam> {
protected:
  std::unique_ptr<MTest> cellModel;
  std::unique_ptr<MElphySolver> cellSolver;
  std::unique_ptr<Vector> dummyData;

  int powerTwoSteps(int n) {
    return (int) (pow(2, n) + 1e-4);
  }

  void FillODESolution(TimeSeries &TS, std::vector<double> &t, std::vector<double> &v) {
    t.resize(TS.Steps() + 1);
    t[0] = TS.Time();
    while (!TS.IsFinished()) {
      TS.NextTimeStep();
      t[TS.Step()] = TS.Time();
    }
    TS.Reset();
    cellSolver->Solve(TS, v);
  }

public:
  MCellSolverTest() {
    auto meshes = MeshesCreator("Interval").
        WithCellData(DataContainer({1, 0, 0, 0, 1, 0, 0, 0, 1})).
        WithVertexData(DataContainer({0.0, 0.002, 1.0})).
        CreateUnique();
    auto disc = std::make_shared<const LagrangeDiscretization>(*meshes, 0);
    dummyData = std::make_unique<Vector>(0.0, disc);
  }

  virtual ~MCellSolverTest() = default;
};

TEST_P(MCellSolverTest, WithExactSolution) {
  cellModel = std::make_unique<MExact>();
  cellSolver = std::make_unique<MElphySolver>(*cellModel, *(this->dummyData), GetParam().scheme);

  int O = 2;
  int N = 10;
  std::vector<std::vector<double>> time(N - O);
  std::vector<std::vector<double>> value(N - O);
  std::vector<std::vector<double>> gating(N - O);

  for (int n = O; n < N; ++n) {
    TimeSeries TS(0.0, 1.0, powerTwoSteps(n));
    FillODESolution(TS, time[n - O], value[n - O]);
  }

  std::vector<double> error(N - O);
  for (int n = O; n < N; ++n) {
    double e = 0.0;
    for (int i = 0; i < value[n - O].size(); ++i) {
      double t = time[n - O][i];
      double v = value[n - O][i];
      e = std::max(e, v - cellModel->Value(t, std::vector<double>{v}));
    }
    error[n - O] = e;
  }

  for (int i = 1; i < error.size(); ++i) {
    if (error[i] < 1e-12) continue;
    EXPECT_NEAR(log2(error[i - 1] / error[i]), GetParam().expectedOrder, 0.1);
  }


  /*
  Logging::Precision(12);
  mout << "[";
  for (auto e : error) {
    mout << e << ", ";
  }
  mout << "]" << endl;

  mout << "Convergence per level: ";
  for(int i = 1; i<error.size(); ++i){
    mout << log2(error[i-1]/error[i]) << " - ";
  }
  mout << endl;
  */
}


TEST_P(MCellSolverTest, WithoutExactSolution) {
  cellModel = std::make_unique<MNonExact>();

  int O = 6;
  int N = 12;

  std::vector<double> refTime;
  std::vector<double> ref;
  cellSolver = std::make_unique<MElphySolver>(*cellModel, *(this->dummyData), "RungeKutta4");
  TimeSeries TS(0.0, 4.0, powerTwoSteps(N));
  FillODESolution(TS, refTime, ref);

  std::vector<std::vector<double>> time(N - O);
  std::vector<std::vector<double>> value(N - O);
  std::vector<std::vector<double>> gating(N - O);
  cellSolver = std::make_unique<MElphySolver>(*cellModel, *(this->dummyData), GetParam().scheme);
  for (int n = O; n < N; ++n) {
    TS.Reset(0.0, 4.0, powerTwoSteps(n));
    FillODESolution(TS, time[n - O], value[n - O]);
  }

  std::vector<double> error(N - O);
  for (int n = O; n < N; ++n) {
    double e = 0.0;
    int dn = powerTwoSteps(N - n);
    for (int i = 0; i < value[n - O].size(); ++i) {
      double t = time[n - O][i];
      double tref = refTime[dn * i];
      ASSERT_DOUBLE_EQ(t, tref);

      double v = value[n - O][i];
      double r = ref[dn * i];
      e = std::max(e, abs(v - r));
    }
    error[n - O] = e;
  }

  for (int i = 1; i < error.size(); ++i) {
    if (error[i] < 1e-12) continue;
    EXPECT_NEAR(log2(error[i - 1] / error[i]), GetParam().expectedOrder, 0.1);
  }
}


INSTANTIATE_TEST_SUITE_P(ExplicitSolver, MCellSolverTest, Values(
    SolverParam({"ExplicitEuler", 1}),
    SolverParam({"RungeKutta2", 2}),
    SolverParam({"RungeKutta4", 4})));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}