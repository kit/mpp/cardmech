//
// Created by lstengel on 18.07.22.
//

#include "elasticity/MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"

#include "TestConfigurations.hpp"

constexpr double DIRICHLET_TEST_TOLERANCE = 1e-6;
constexpr double TOLERANCE = 1e-10;

struct TestDirichletParameter {
  std::string discretization;
  std::string problemName;

  std::string problemGeo;
  int problemLevel;
  int problemDegree;

  friend void PrintTo(const TestDirichletParameter& param, std::ostream* os) {
    *os << param.discretization << param.problemName << param.problemGeo
        << "_l" << param.problemLevel << "_deg" << param.problemDegree;
  }
};

class DirichletBeamTest : public TestWithParam<TestDirichletParameter> {
protected:
  std::unique_ptr<MainElasticity> cmMain = nullptr;

  DirichletBeamTest() = default;

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Static";
    testConfig["PressureSteps"] = "1";

    testConfig["MechDiscretization"] = GetParam().discretization;
    if (GetParam().discretization == "Conforming") {
      testConfig["Overlap_Distribution"] = "0";
      testConfig["Overlap"] = "NoOverlap";
    }
    if (GetParam().discretization == "DG")  {
      testConfig["Overlap_Distribution"] = "1";
      testConfig["Overlap"] = "dG1";
      testConfig["DGSign"] = "-1";
      testConfig["DGPenalty"] = "3.0";
    }
    if (GetParam().discretization == "EG")  {
      testConfig["Overlap_Distribution"] = "1";
      testConfig["Overlap"] = "EG";
      testConfig["DGSign"] = "-1";
      testConfig["DGPenalty"] = "3.0";
    }
    testConfig["MechProblem"] = GetParam().problemName;
    testConfig["ProblemGeometry"] = GetParam().problemGeo;
    testConfig["MechLevel"] = std::to_string(GetParam().problemLevel);
    testConfig["MechPolynomialDegree"] = std::to_string(GetParam().problemDegree);
    Config::Initialize(testConfig);
    Config::PrintInfo();

    cmMain = std::make_unique<MainElasticity>();
  }

  void TearDown() override {
    Config::Close();
  }
};

TEST_P(DirichletBeamTest, ErrorTest) {
  double l2_err = 0.0;
  double h1_err = 0.0;
  double energy_err = 0.0;
  double l2_err_ex = 0.0;
  double h1_err_ex = 0.0;
  double energy_err_ex = 0.0;

  cmMain->ResetLevel(GetParam().problemLevel);
  cmMain->Initialize();
  Vector exactSolution(cmMain->GetDisplacement());
  cmMain->SetDisplacement(true, exactSolution);
  Vector &solution = cmMain->Run();

  l2_err_ex = cmMain->EvaluateQuantity(exactSolution, "L2");
  h1_err_ex = cmMain->EvaluateQuantity(exactSolution, "H1");
  energy_err_ex = cmMain->EvaluateQuantity(exactSolution, "Energy");

  l2_err = cmMain->EvaluateQuantity(solution, "L2");
  h1_err = cmMain->EvaluateQuantity(solution, "H1");
  energy_err = cmMain->EvaluateQuantity(solution, "Energy");

  mout << "L2 error for exact solution " << l2_err_ex << endl << endl;
  mout << "L2 error for numerical solution " << l2_err << endl << endl;

  ASSERT_NEAR(l2_err_ex, 0.0, TOLERANCE);
  ASSERT_NEAR(l2_err, 0.0, DIRICHLET_TEST_TOLERANCE);

  mout << "H1 error for exact solution " << h1_err_ex << endl << endl;
  mout << "H1 error for  numerical solution " << h1_err << endl << endl;

  ASSERT_NEAR(h1_err_ex, 0.0, TOLERANCE);
  ASSERT_NEAR(h1_err, 0.0, DIRICHLET_TEST_TOLERANCE);

  mout << "energy error for exact solution " << energy_err_ex << endl << endl;
  mout << "energy error for numerical solution " << energy_err << endl << endl;

  ASSERT_NEAR(energy_err_ex, 0.0, TOLERANCE);
  ASSERT_NEAR(energy_err, 0.0, DIRICHLET_TEST_TOLERANCE);
}

std::vector<TestDirichletParameter> DirichletParameters(){
  return std::vector<TestDirichletParameter>{
      TestDirichletParameter({"Conforming","DirichletBeamDegree1",  "Tet" ,  0, 1}),
      TestDirichletParameter({"Conforming","DirichletBeamDegree1", "Tet", 1, 1}),
      //TestDirichletParameter({"Conforming","DirichletBeam",  "Tet",  2, 1}),
      TestDirichletParameter({"Conforming","DirichletBeamDegree1",  "Tet" , 0, 2}),
      TestDirichletParameter({"Conforming","DirichletBeamDegree1",   "Tet" ,  1, 2}),
      //TestDirichletParameter({"Conforming","DirichletBeam",  "Tet", 2, 2}),
      TestDirichletParameter({"Conforming","DirichletBeamDegree2",  "Tet", 0, 2}),
      TestDirichletParameter({"Conforming","DirichletBeamDegree2",  "Tet", 1, 2}),
      //TestDirichletParameter({"Conforming","DirichletBeamQuadratic",  "Tet", 2, 2}),
      TestDirichletParameter({"Conforming","DirichletBeamDegree3",  "Tet", 0, 3}),
      TestDirichletParameter({"Conforming","DirichletBeamDegree3",  "Tet", 1, 3}),
      TestDirichletParameter({"DG","DirichletBeamDegree1", "Tet",  0, 1}),
      TestDirichletParameter({"DG","DirichletBeamDegree1",  "Tet", 1, 1}),
      //TestDirichletParameter({"DG","DirichletBeam", "Tet", 2, 1}),
      TestDirichletParameter({"DG","DirichletBeamDegree2", "Tet",  0, 2}),
      TestDirichletParameter({"DG","DirichletBeamDegree2",  "Tet", 1, 2}),
      //TestDirichletParameter({"DG","DirichletBeamQuadratic", "Tet", 2, 2}),
      TestDirichletParameter({"DG","DirichletBeamDegree3",  "Tet", 0, 3}),
      //TestDirichletParameter({"DG","DirichletBeamKubic",  "Tet", 1, 3}),
      TestDirichletParameter({"EG","DirichletBeamDegree1", "Tet" , 0, 1}),
      TestDirichletParameter({"EG","DirichletBeamDegree1", "Tet" , 1, 1}),
      //TestDirichletParameter({"EG","DirichletBeam",  "Tet" , 2, 1}),
      TestDirichletParameter({"EG","DirichletBeamDegree2", "Tet",  0, 2}),
      TestDirichletParameter({"EG","DirichletBeamDegree2",  "Tet", 1, 2}),
      //TestDirichletParameter({"EG","DirichletBeamQuadratic", "Tet", 2, 2}),
      TestDirichletParameter({"EG","DirichletBeamDegree3",  "Tet", 0, 3}),
      TestDirichletParameter({"EG","DirichletBeamDegree3",  "Tet", 1, 3})
  };
}

INSTANTIATE_TEST_SUITE_P(Test3DTetraBeam, DirichletBeamTest, ValuesIn(DirichletParameters()));


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}