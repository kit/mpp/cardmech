#include "elasticity/MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"

#include "TestConfigurations.hpp"

constexpr double DIRICHLET_TEST_TOLERANCE = 1e-6;

struct TestDirichletParameter {
  std::string problemName;
  bool exact;

  std::string problemGeo;
  int problemDimension;
  int problemLevel;
  int problemDegree;
};

class DirichletBeamTest : public TestWithParam<TestDirichletParameter> {
protected:
  std::unique_ptr<MainElasticity> cmMain = nullptr;

  DirichletBeamTest() = default;

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Static";
    testConfig["PressureSteps"] = "1";

    testConfig["MechProblem"] = GetParam().problemName;
    testConfig["ProblemDimension"] = std::to_string(GetParam().problemDimension);
    testConfig["ProblemGeometry"] = GetParam().problemGeo;
    testConfig["MechLevel"] = std::to_string(GetParam().problemLevel);
    testConfig["MechPolynomialDegree"] = std::to_string(GetParam().problemDegree);
    Config::Initialize(testConfig);

    cmMain = std::make_unique<MainElasticity>();
    cmMain->Initialize();
  }

  void TearDown() override {
    Config::Close();
  }
};

TEST_P(DirichletBeamTest, L2ErrorTest) {
  Vector exactSolution(cmMain->GetDisplacement());
  cmMain->SetDisplacement(true, exactSolution);
  Vector &solution = cmMain->Run();

  double diff = norm(exactSolution-solution);
  ASSERT_NEAR(diff, 0.0, DIRICHLET_TEST_TOLERANCE);
}

std::vector<TestDirichletParameter> DirichletParameters(int dimension, bool isTet){
  return std::vector<TestDirichletParameter>{
      //TestDirichletParameter({"DirichletBeamDegree1", true, isTet? "Tet" : "Quad", dimension, 0, 1}),
      TestDirichletParameter({"DirichletBeamDegree1", true, isTet? "Tet" : "Quad", dimension, 1, 1}),
      TestDirichletParameter({"DirichletBeamDegree1", true, isTet? "Tet" : "Quad", dimension, 2, 1}),
      TestDirichletParameter({"DirichletBeamDegree1", true, isTet? "Tet" : "Quad", dimension, 3, 1}),
      TestDirichletParameter({"DirichletBeamDegree1", true, isTet? "Tet" : "Quad", dimension, 0, 2}),
      TestDirichletParameter({"DirichletBeamDegree1", true, isTet? "Tet" : "Quad", dimension, 1, 2}),
      TestDirichletParameter({"DirichletBeamDegree1", true, isTet? "Tet" : "Quad", dimension, 2, 2})/*,
      TestDirichletParameter({"DirichletBeam", true, isTet? "Tet" : "Quad", dimension, 0, 3}),
    TestDirichletParameter({"DirichletBeam", true, isTet? "Tet" : "Quad", dimension, 1, 3}),
    TestDirichletParameter({"DirichletBeam", true, isTet? "Tet" : "Quad", dimension, 0, 4})*/
  };
}

INSTANTIATE_TEST_SUITE_P(Test2DTetraBeam, DirichletBeamTest, ValuesIn(DirichletParameters(2, true)));
INSTANTIATE_TEST_SUITE_P(Test2DQuadBeam, DirichletBeamTest, ValuesIn(DirichletParameters(2, false)));
INSTANTIATE_TEST_SUITE_P(Test3DTetraBeam, DirichletBeamTest, ValuesIn(DirichletParameters(3, true)));
INSTANTIATE_TEST_SUITE_P(Test3DQuadBeam, DirichletBeamTest, ValuesIn(DirichletParameters(3, false)));


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}