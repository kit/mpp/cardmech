//
// Created by laura on 02.06.22.
//

#include <utility>

#include "MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"
#include "TestCardiacBeamConfiguration.hpp"

std::vector<std::string> elasticityProblems{"CardiacBeam"};

struct TestBeamParameter {
  int problemLevel;
  int degree;
  std::string material;
  std::string QuasiCompressiblePenalty;
  int DGPenalty;

};

class ElasticityTest : public TestWithParam<TestBeamParameter> {
protected:

  std::unique_ptr<ElasticityProblem> elastProblem;
  std::unique_ptr<IElasticity> assemble;
  std::unique_ptr<Meshes> meshes;

  std::unique_ptr<Vector> u;

  std::string model;

  explicit ElasticityTest(std::string &&model) : model(std::move(model)) {}


  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIACBEAM_TEST_CONFIG;

    testConfig["MechDynamics"] = "Static";
    testConfig["LAPressure"] = "0.004";
    testConfig["MechProblem"] = "CardiacBeam"; //TODO noch anpassen auf mehrere Probleme

    testConfig["ConfigVerbose"] = "5";

    testConfig["MechLevel"] = std::to_string(GetParam().problemLevel);
    testConfig["ActiveMaterial"] = GetParam().material;
    testConfig["QuasiCompressiblePenalty"] = GetParam().QuasiCompressiblePenalty;
    testConfig["DGPenalty"] = std::to_string(GetParam().DGPenalty);

    if (model == "DG") {
      testConfig["MechDiscretization"] = "DG";
      testConfig["Overlap_Distribution"] = "1";
      testConfig["Overlap"] = "dG1";
    } else if (model == "EG") {
      testConfig["MechDiscretization"] = "EG";
      testConfig["Overlap_Distribution"] = "1";
      testConfig["Overlap"] = "EG";
    }

    Config::Initialize(testConfig);

    Config::PrintInfo();

    elastProblem = GetElasticityProblem();//CardiacBeam
    elastProblem->CreateMeshes(MeshesCreator());

    assemble = CreateFiniteElasticityAssemble(*elastProblem, model,
                                              GetParam().degree, false);
    u = std::make_unique<Vector>(0.0, assemble->GetSharedDisc());

    assemble->SetInitialValue(*u);
    assemble->Initialize(*u);

  }

  void TearDown() override {
    Config::Close();
  }
};

/**************************************************
 *  Lagrange Consistency
 **************************************************/
class LagrangeElasticityTest : public ElasticityTest {
protected:
  LagrangeElasticityTest() : ElasticityTest("Conforming") {
  }
};

TEST_P(LagrangeElasticityTest, AssembleConsistency) {
  EXPECT_TRUE(isAssembleConsistent(*assemble, *u));
}

INSTANTIATE_TEST_SUITE_P(Degree_1_Consistency, LagrangeElasticityTest, Values(
//    TestBeamParameter({0, 1, "Laplace", "None", 0}),
//    TestBeamParameter({1, 1, "Laplace", "None", 0}),
    TestBeamParameter({0, 1, "Linear", "None", 0}),
    TestBeamParameter({1, 1, "Linear", "None", 0}),
    /*TestBeamParameter({ 2, 1, "Linear", "None", 0}),*/
    TestBeamParameter({0, 1, "Bonet", "Ciarlet", 0}),
    TestBeamParameter({1, 1, "Bonet", "Ciarlet", 0})
    //TestBeamParameter({0, 1, "Holzapfel", "Ciarlet", 0}),
    //TestBeamParameter({1, 1, "Holzapfel", "Ciarlet", 0})
));

INSTANTIATE_TEST_SUITE_P(Degree_2_Consistency, LagrangeElasticityTest, Values(
//    TestBeamParameter({0, 2, "Laplace", "None", 0}),
//    TestBeamParameter({1, 2, "Laplace", "None", 0}),
    TestBeamParameter({0, 2, "Linear", "None", 0}),
//    TestBeamParameter({1, 2, "Linear", "None", 0}),
    TestBeamParameter({0, 2, "Bonet", "Ciarlet", 0})
//    TestBeamParameter({1, 2, "Bonet", "Ciarlet", 0})
    //TestBeamParameter({0, 1, "Holzapfel", "Ciarlet", 0}),
    //TestBeamParameter({1, 1, "Holzapfel", "Ciarlet", 0})
));


/**************************************************
 *  DG Consistency
 **************************************************/
class DGElasticityTest : public ElasticityTest {
protected:
  DGElasticityTest() : ElasticityTest("DG") {
  }
};

TEST_P(DGElasticityTest, AssembleConsistency) {
  EXPECT_TRUE(isAssembleConsistent(*assemble, *u));
}

INSTANTIATE_TEST_SUITE_P(DG_Degree_1_Consistency, DGElasticityTest, Values(
//    TestBeamParameter({0, 1, "Laplace", "None", 0}),
//    TestBeamParameter({1, 1, "Laplace", "None", 0}),
    TestBeamParameter({0, 1, "Linear", "None", 9}),
    TestBeamParameter({1, 1, "Linear", "None", 9}),
    //TestBeamParameter({2, 1, "Linear", "None", 9}),
    TestBeamParameter({0, 1, "Bonet", "Ciarlet", 9}),
    TestBeamParameter({1, 1, "Bonet", "Ciarlet", 9})
    //TestBeamParameter({2, 1, "Bonet", "Ciarlet", 9})
));

INSTANTIATE_TEST_SUITE_P(DG_Degree_2_Consistency, DGElasticityTest, Values(
//    TestBeamParameter({0, 2, "Laplace", "None", 0}),
    //TestBeamParameter({1, 2, "Laplace", "None", 0}),
    TestBeamParameter({0, 2, "Linear", "None", 9}),
    //TestBeamParameter({1, 2, "Linear", "None", 9 }),
    TestBeamParameter({0, 2, "Bonet", "Ciarlet", 9})//,
    //TestBeamParameter({1, 2, "Bonet", "Ciarlet", 9})
    //TestBeamParameter({1, 2, "Bonet", "Ciarlet", 9})
    //TestBeamParameter({2, 2, "Bonet", "Ciarlet", 9})
));

/**************************************************
 *  EG Consistency
 **************************************************/
class EGElasticityTest : public ElasticityTest {
protected:
  EGElasticityTest() : ElasticityTest("EG") {
  }
};

TEST_P(EGElasticityTest, AssembleConsistency) {
  EXPECT_TRUE(isAssembleConsistent(*assemble, *u));
}

INSTANTIATE_TEST_SUITE_P(EG_Degree_1_Consistency, EGElasticityTest, Values(
//    TestBeamParameter({0, 1, "Laplace", "None", 0}),
//    TestBeamParameter({1, 1, "Laplace", "None", 0}),
    TestBeamParameter({0, 1, "Linear", "None", 2}),
    TestBeamParameter({1, 1, "Linear", "None", 2}),
    //TestBeamParameter({2, 1, "Linear", "None", 2}),
    TestBeamParameter({0, 1, "Bonet", "Ciarlet", 2}),
    TestBeamParameter({1, 1, "Bonet", "Ciarlet", 2})
    //TestBeamParameter({2, 1, "Bonet", "Ciarlet", 2})
));

INSTANTIATE_TEST_SUITE_P(EG_Degree_2_Consistency, EGElasticityTest, Values(
//    TestBeamParameter({0, 2, "Laplace", "None", 0}),
//    TestBeamParameter({1, 2, "Laplace", "None", 0}),
    TestBeamParameter({0, 2, "Linear", "None", 2}),
//    TestBeamParameter({1, 2, "Linear", "None", 2}),
    //TestBeamParameter({2, 2, "Linear", "None", 2}),
    TestBeamParameter({0, 2, "Bonet", "Ciarlet", 2})
//    TestBeamParameter({1, 2, "Bonet", "Ciarlet", 2})
    //TestBeamParameter({2, 2, "Bonet", "Ciarlet", 2})
));

int main(int argc, char **argv) {
  return MppTest(
      MppTestBuilder(argc, argv).
          WithoutDefaultConfig().
          WithScreenLogging().
          WithConfPath(std::string(ProjectSourceDir) + "/conf/elasticity/").
          WithGeoPath(std::string(ProjectSourceDir) + "/geo/").
          WithPPM()
  ).RUN_ALL_MPP_TESTS();
}