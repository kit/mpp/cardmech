#include "MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"

#include "../TestConfigurations.hpp"

struct TestBlockParameter {
  std::string problemName;
  std::string problemGeo;
  int problemDimension;
};

constexpr double BEAM_TEST_TOLERANCE = 1e-1;

class ElasticityBlockTest : public TestWithParam<TestBlockParameter> {
protected:
  MainElasticity *cmMain;

  ElasticityBlockTest() {}

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Static";
    testConfig["MechDiscretization"] = "Passive";
    testConfig["PressureSteps"] = "10";
    testConfig["Distribution"] = "RCBz";
    testConfig["WithPrestress"] = "0";

    testConfig["MechProblem"] =
        GetParam().problemName + std::to_string(GetParam().problemDimension) + "D";
    testConfig["ProblemDimension"] = std::to_string(GetParam().problemDimension);
    testConfig["ProblemGeometry"] = GetParam().problemGeo;
    testConfig["MechLevel"] = "0";
    testConfig["MechPolynomialDegree"] = "1";
    Config::Initialize(testConfig);

    cmMain = new MainElasticity();
    cmMain->Initialize();
  }

  void TearDown() override {
    Config::Close();
  }

  ~ElasticityBlockTest() override {
    delete cmMain;
  }
};


TEST_P(ElasticityBlockTest, CheckReferenceValues) {
  Vector &solution = cmMain->Run();
  auto evalResults = cmMain->Evaluate(solution);

  for (auto result : evalResults)
    ASSERT_NEAR(result, 0.0, BEAM_TEST_TOLERANCE);

  //ASSERT_NEAR(cmMain->PressureError(solution), 0.0, BEAM_TEST_TOLERANCE);

}
/*
INSTANTIATE_TEST_SUITE_P(Test2DBlock, ElasticityBlockTest, Values(
    TestBlockParameter({"ElasticityBlock", "Quad", 2}),
    TestBlockParameter({"ElasticityBlock", "Tet", 2})
));

INSTANTIATE_TEST_SUITE_P(Test3DBlock, ElasticityBlockTest, Values(
    TestBlockParameter({"ElasticityBlock", "Quad", 3}),
    TestBlockParameter({"ElasticityBlock", "Tet", 3})
));*/

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).
      WithPPM().
      WithScreenLogging().
      WithoutDefaultConfig();
  return mppTest.RUN_ALL_MPP_TESTS();
}