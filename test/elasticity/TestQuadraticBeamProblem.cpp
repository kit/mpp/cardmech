//
// Created by lstengel on 15.07.22.
//

#include "elasticity/MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"

#include "TestConfigurations.hpp"

constexpr double DIRICHLET_TEST_TOLERANCE = 1e-6;
constexpr double DIRICHLET_TOLERANCE = 1e-4;


struct TestQuadraticDirichletParameter {
  std::string discretization;
  std::string problemName;
  bool exact;

  std::string problemGeo;
  std::vector<int> problemLevels;
  int problemDegree;

  int minMechLevel() const {
    //mout << problemLevels[0] << endl;
    return problemLevels[0];
  }

  int LevelSize() const {
    return problemLevels.size();
  }

  friend void PrintTo(const TestQuadraticDirichletParameter& param, std::ostream* os) {
    *os << param.discretization << param.problemName << param.problemGeo
        << "_deg" << param.problemDegree;
  }

};

class QuadraticDirichletBeamTest : public TestWithParam<TestQuadraticDirichletParameter> {
protected:
  std::unique_ptr<MainElasticity> cmMain = nullptr;

  QuadraticDirichletBeamTest() = default;

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Static";
    testConfig["MechDiscretization"] = GetParam().discretization;
    if (GetParam().discretization == "Conforming") {
      testConfig["Overlap_Distribution"] = "0";
      testConfig["Overlap"] = "NoOverlap";
    }
    if (GetParam().discretization == "DG")  {
      testConfig["Overlap_Distribution"] = "1";
      testConfig["Overlap"] = "dG1";
      testConfig["DGSign"] = "-1";
      testConfig["DGPenalty"] = "8.0";
    }
    if (GetParam().discretization == "EG")  {
      testConfig["Overlap_Distribution"] = "1";
      testConfig["Overlap"] = "EG";
      testConfig["DGSign"] = "-1";
      if (GetParam().problemDegree == 1) {
        testConfig["DGPenalty"] = "6.0";
      }
      else if ( GetParam().problemDegree == 2){
        testConfig["DGPenalty"] = "1.5";
      }
    }

    testConfig["PressureSteps"] = "1";

    testConfig["MechProblem"] = GetParam().problemName;
    testConfig["ProblemGeometry"] = GetParam().problemGeo;
    testConfig["MechPolynomialDegree"] = std::to_string(GetParam().problemDegree);
    testConfig["MechLevel"] = std::to_string(GetParam().minMechLevel());
    Config::Initialize(testConfig);
    Config::PrintInfo();

    cmMain = std::make_unique<MainElasticity>();
  }

  void TearDown() override {
    Config::Close();
  }

};

TEST_P(QuadraticDirichletBeamTest, ErrorTest) {
  std::vector<double> l2_err(GetParam().LevelSize());
  std::vector<double> h1_err(GetParam().LevelSize());
  std::vector<double> energy_err(GetParam().LevelSize());

  for (int l = 0; l < GetParam().LevelSize(); ++l) {
    cmMain->ResetLevel(GetParam().problemLevels[l]);
    cmMain->Initialize();
    Vector exactSolution(cmMain->GetDisplacement());
    cmMain->SetDisplacement(true, exactSolution);
    Vector &solution = cmMain->Run();

    l2_err[l] = cmMain->EvaluateQuantity(solution, "L2");
    mout << "l2_err[" << l << "] " << l2_err[l] << endl;

    h1_err[l] = cmMain->EvaluateQuantity(solution, "H1");
    mout << "h1_err[" << l << "] " << h1_err[l] << endl;

    energy_err[l] = cmMain->EvaluateQuantity(solution, "Energy");
    mout << "energy_err[ " << l << "]" << energy_err[l] << endl;
  }

  for (int i = 0; i < l2_err.size() - 1; ++i) {
    mout << "l2_err[" << i << "]/l2_err[" << i + 1 << "] " << l2_err[i]/l2_err[i + 1] << endl;
    EXPECT_LE(l2_err[i + 1], l2_err[i]);

    mout << "h1_err[" << i << "]/h1_err[" << i + 1 << "] " << h1_err[i]/h1_err[i + 1] << endl;
    EXPECT_LE(h1_err[i + 1], h1_err[i]);

    mout << "energy_err[" << i << "]/energy_err[" << i + 1 << "] " << energy_err[i]/energy_err[i + 1] << endl;
    EXPECT_LE(energy_err[i + 1], energy_err[i]);
  }
  for(int i = 1; i < l2_err.size() ; ++i){
    mout << "log2(l2_err[" << i - 1 << "]/ l2_err[" << i << "]) " << log2(l2_err[i-1]/l2_err[i]) << endl << endl;
    mout << "h1_err[" << i -1 << "]/h1_err[" << i << "] " << h1_err[i-1]/h1_err[i] << endl;
    mout << "energy_err[" << i -1 << "]/energy_err[" << i << "] " << energy_err[i-1]/energy_err[i] << endl;
      if (GetParam().problemDegree == 1) {
        // EOC
        EXPECT_NEAR(log2(l2_err[i - 1] / l2_err[i]), 2.0, 0.3);
        // convergence rate
        //EXPECT_NEAR(l2_err[i - 1] / l2_err[i], 4.0, 0.2);

        // EOC
        EXPECT_NEAR(log2(h1_err[i - 1] / h1_err[i]), 1.0, 0.15);
        EXPECT_NEAR(log2(energy_err[i - 1] / energy_err[i]), 1.0, 0.15);
        // convergence rate (err = h1_err oder energy_err)
        //EXPECT_NEAR(err[i - 1] / err[i], 2.0, 0.1);
      }
      else if (GetParam().problemDegree == 2) {
        // EOC
        EXPECT_NEAR(log2(l2_err[i - 1] / l2_err[i]), 3.0, 0.2);
        // convergence rate
        //EXPECT_NEAR(l2_err[i - 1] / l2_err[i], 8.0, 0.2);

        // EOC
        EXPECT_NEAR(log2(h1_err[i - 1] / h1_err[i]), 2.0, 0.15);
        EXPECT_NEAR(log2(energy_err[i - 1] / energy_err[i]), 2.0, 0.15);
        // convergence rate (err = h1_err oder energy_err)
        //EXPECT_NEAR(err[i - 1] / err[i], 4.0, 0.1);
      }
  }
}

std::vector<TestQuadraticDirichletParameter> QuadraticDirichletParameters(bool isTet){
  return std::vector<TestQuadraticDirichletParameter>{
     TestQuadraticDirichletParameter({"Conforming","DirichletBeamDegree2", true, "Tet", {0, 1, 2}, 1}),
     TestQuadraticDirichletParameter({"Conforming","DirichletBeamDegree3", true, "Tet", {0, 1, 2}, 2}),
     TestQuadraticDirichletParameter({"DG","DirichletBeamDegree2", true, "Tet", {0, 1, 2}, 1}),
     TestQuadraticDirichletParameter({"DG","DirichletBeamDegree3", true, "Tet", {0, 1, 2}, 2}),
     TestQuadraticDirichletParameter({"EG","DirichletBeamDegree2", true, "Tet", {0, 1, 2}, 1}),
     TestQuadraticDirichletParameter({"EG","DirichletBeamDegree3", true, "Tet", {0, 1, 2}, 2})
  };
}

INSTANTIATE_TEST_SUITE_P(Test3DTetraBeam, QuadraticDirichletBeamTest, ValuesIn(QuadraticDirichletParameters(true)));


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}