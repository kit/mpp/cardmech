#include "materials/TestLaplaceMaterial.hpp"
#include "materials/TestLinearMaterial.hpp"
#include "materials/TestNeoHookeMaterial.hpp"
#include "materials/TestStVenantMaterial.hpp"
#include "materials/TestGuccioneMaterial.hpp"
#include "materials/TestHolzapfelMaterial.hpp"


TEST(MaterialFunctionTest, DeformationGradient) {
  Tensor S{0, 0, 1, 0, 1, 0, 1, 0, 0};
  Tensor F_orig{1, 0, 0, 0, 1, 0, 0, 0, 2};
  Tensor F_rot{2, 0, 0, 0, 1, 0, 0, 0, 1};
  EXPECT_EQ(RotatedDeformationGradient(F_orig, S), F_rot  );
}

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv);
  return mppTest.RUN_ALL_MPP_TESTS();
}