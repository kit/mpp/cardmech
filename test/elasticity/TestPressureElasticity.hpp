//
// Created by laura on 17.11.22.
//

#ifndef CARDMECH_TESTPRESSUREELASTICITY_PP
#define CARDMECH_TESTPRESSUREELASTICITY_PP


#include "elasticity/MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"

#include "TestConfigurations.hpp"

constexpr double TEST_TOLERANCE = 1e-9;
constexpr double TOLERANCE = 1e-10;

struct TestPressureParameter {
  std::string discretization;
  std::string problemName;

  std::string meshName;
  int problemLevel;
  int problemDegree;

  friend void PrintTo(const TestPressureParameter& param, std::ostream* os) {
    *os << param.discretization << param.problemName << param.meshName
        << "_l" << param.problemLevel << "_deg" << param.problemDegree;
  }
};

class TestPressureElasticity : public TestWithParam<TestPressureParameter> {
protected:
  std::unique_ptr<MainElasticity> cmMain = nullptr;

  TestPressureElasticity() = default;

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Static";
    testConfig["MechRunType"] = "Default";
    testConfig["MechPreconditioner"] = "SuperLU";

    testConfig["MechDiscretization"] = GetParam().discretization;
    if (GetParam().discretization == "Conforming") {
      testConfig["Overlap_Distribution"] = "0";
      testConfig["Overlap"] = "NoOverlap";
    }
    if (GetParam().discretization == "DG") {
      testConfig["Overlap_Distribution"] = "1";
      testConfig["Overlap"] = "dG1";
      testConfig["DGSign"] = "-1";
      testConfig["DGPenalty"] = "5.0";
    }
    if (GetParam().discretization == "EG") {
      testConfig["Overlap_Distribution"] = "1";
      testConfig["Overlap"] = "EG";
      testConfig["DGSign"] = "-1";
      testConfig["DGPenalty"] = "3.0";
    }
    testConfig["MechProblem"] = GetParam().problemName;
    testConfig["Mesh"] = GetParam().meshName;
    testConfig["MechLevel"] = std::to_string(GetParam().problemLevel);
    testConfig["MechPolynomialDegree"] = std::to_string(GetParam().problemDegree);
    Config::Initialize(testConfig);
    Config::PrintInfo();

    cmMain = std::make_unique<MainElasticity>();
  }

  void TearDown() override {
    Config::Close();
  }
};


TEST_P(TestPressureElasticity, ErrorTest) {
  double l2_err = 0.0;
  double h1_err = 0.0;
  double l2_err_ex = 0.0;
  double h1_err_ex = 0.0;

  cmMain->ResetLevel(GetParam().problemLevel);
  cmMain->Initialize();
  Vector exactSolution(cmMain->GetDisplacement());
  cmMain->SetDisplacement(true, exactSolution);
  Vector &solution = cmMain->Run();

  l2_err_ex = cmMain->EvaluateQuantity(exactSolution, "L2");
  h1_err_ex = cmMain->EvaluateQuantity(exactSolution, "H1");

  l2_err = cmMain->EvaluateQuantity(solution, "L2");
  h1_err = cmMain->EvaluateQuantity(solution, "H1");

  mout << "L2 error for exact solution " << l2_err_ex << endl << endl;
  mout << "L2 error for numerical solution " << l2_err << endl << endl;
  ASSERT_NEAR(l2_err_ex, 0.0, TOLERANCE);
  ASSERT_NEAR(l2_err, 0.0, TEST_TOLERANCE);

  mout << "H1 error for exact solution " << h1_err_ex << endl << endl;
  mout << "H1 error for  numerical solution " << h1_err << endl << endl;
  ASSERT_NEAR(h1_err_ex, 0.0, TOLERANCE);
  ASSERT_NEAR(h1_err, 0.0, TEST_TOLERANCE);

  auto initialVolume_exact = cmMain->GetGeometryVolume(exactSolution).first;
  auto initialVolume = cmMain->GetGeometryVolume(solution).first;
  auto deformedVolume_exact = cmMain->GetGeometryVolume(exactSolution).second;
  auto deformedVolume = cmMain->GetGeometryVolume(solution).second;

  if (GetParam().problemName == "ElasticityPullBeam") {
    ASSERT_NEAR(deformedVolume_exact, 2 * initialVolume_exact, TOLERANCE);
    ASSERT_NEAR(deformedVolume, 2 * initialVolume, TOLERANCE);
  } else if (GetParam().problemName == "ElasticityPushBeam") {
    ASSERT_NEAR(deformedVolume_exact, -initialVolume, TOLERANCE);
    ASSERT_NEAR(deformedVolume, -initialVolume, TEST_TOLERANCE);
  }
}

std::vector<TestPressureParameter> PullPressureParameters() {
  return std::vector<TestPressureParameter>{
      TestPressureParameter(
          {"Conforming", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 0, 1}),
      TestPressureParameter(
          {"Conforming", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 1, 1}),
      TestPressureParameter(
          {"Conforming", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 0, 2}),
      TestPressureParameter(
          {"Conforming", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 1, 2}),
      TestPressureParameter({"Conforming", "ElasticityPullBeam", "PressureUnitBlock3DTet", 0, 1}),
      TestPressureParameter({"Conforming", "ElasticityPullBeam", "PressureUnitBlock3DTet", 1, 1}),
      TestPressureParameter({"Conforming", "ElasticityPullBeam", "PressureUnitBlock3DTet", 0, 2}),
      TestPressureParameter({"Conforming", "ElasticityPullBeam", "PressureUnitBlock3DTet", 1, 2}),
      TestPressureParameter({"Conforming", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 0, 1}),
      TestPressureParameter({"Conforming", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 1, 1}),
      TestPressureParameter({"Conforming", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 0, 2}),
      TestPressureParameter({"Conforming", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 1, 2}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 0, 1}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 1, 1}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 0, 2}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 1, 2}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "PressureUnitBlock3DTet", 0, 1}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "PressureUnitBlock3DTet", 1, 1}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "PressureUnitBlock3DTet", 0, 2}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "PressureUnitBlock3DTet", 1, 2}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 0, 1}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 1, 1}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 0, 2}),
      TestPressureParameter({"DG", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 1, 2}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 0, 1}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 1, 1}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 0, 2}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "BenchmarkPressureBeam3DTet", 1, 2}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "PressureUnitBlock3DTet", 0, 1}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "PressureUnitBlock3DTet", 1, 1}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "PressureUnitBlock3DTet", 0, 2}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "PressureUnitBlock3DTet", 1, 2}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 0, 1}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 1, 1}),
      TestPressureParameter({"EG", "ElasticityPullBeam", "PressureUnitBlock3DQuad", 0, 2})
  };
}

std::vector<TestPressureParameter> PushPressureParameters() {
  return std::vector<TestPressureParameter>{
      TestPressureParameter(
          {"Conforming", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 0, 1}),
      TestPressureParameter(
          {"Conforming", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 1, 1}),
      TestPressureParameter(
          {"Conforming", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 0, 2}),
      TestPressureParameter(
          {"Conforming", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 1, 2}),
      TestPressureParameter({"Conforming", "ElasticityPushBeam", "PressureUnitBlock3DTet", 0, 1}),
      TestPressureParameter({"Conforming", "ElasticityPushBeam", "PressureUnitBlock3DTet", 1, 1}),
      TestPressureParameter({"Conforming", "ElasticityPushBeam", "PressureUnitBlock3DTet", 0, 2}),
      TestPressureParameter({"Conforming", "ElasticityPushBeam", "PressureUnitBlock3DTet", 1, 2}),
      TestPressureParameter({"Conforming", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 0, 1}),
      TestPressureParameter({"Conforming", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 1, 1}),
      TestPressureParameter({"Conforming", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 0, 2}),
      TestPressureParameter({"Conforming", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 1, 2}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 0, 1}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 1, 1}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 0, 2}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 1, 2}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "PressureUnitBlock3DTet", 0, 1}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "PressureUnitBlock3DTet", 1, 1}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "PressureUnitBlock3DTet", 0, 2}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "PressureUnitBlock3DTet", 1, 2}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 0, 1}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 1, 1}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 0, 2}),
      TestPressureParameter({"DG", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 1, 2}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 0, 1}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 1, 1}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 0, 2}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "BenchmarkPressureBeam3DTet", 1, 2}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "PressureUnitBlock3DTet", 0, 1}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "PressureUnitBlock3DTet", 1, 1}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "PressureUnitBlock3DTet", 0, 2}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "PressureUnitBlock3DTet", 1, 2}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 0, 1}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 1, 1}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 0, 2}),
      TestPressureParameter({"EG", "ElasticityPushBeam", "PressureUnitBlock3DQuad", 1, 2})
  };
}


#endif //CARDMECH_TESTPRESSUREELASTICITY_PP
