#include "TestEnvironmentCardMech.hpp"
#include "../TestConfigurations.hpp"
#include "coupled/MainCoupled.hpp"


class TestMainProgram : public TestWithParam<ConfigMaps> {
protected:
  std::unique_ptr<MainCoupled> mainProgram;

  void SetUp() override {
    Logging::DisableFileLogging();
    Logging::DisableScreenLogging();

    Config::SetSearchPath(GetParam().confPath);
    Config::Initialize(GetParam().defaultMap);

    mainProgram = std::make_unique<MainCoupled>();
    mainProgram->Initialize();
  }

  void checkPlotting() {
    if (!PPM->master())
      return;

    /*
    int level = mainProgram->initLevels[0];
    for (auto &sampleAmount : mainProgram->initSampleAmount) {
        for (int sample = 0; sample < sampleAmount; sample++) {
            std::string permeability = "sample_" +  std::to_string(level) +
                "_" +  std::to_string(sample) + "/permeability";
            std::string u = "sample_" +  std::to_string(level) +
                "_" +  std::to_string(sample) + "/u";
            ASSERT_TRUE(FileExists("data/vtk/" + permeability + ".vtk"));
            ASSERT_TRUE(FileExists("data/vtk/" + u + ".vtk"));

            if (level != mainProgram->initLevels[0]) {
                std::string name_coarse = "sample_coarse_" +
    std::to_string(level) +
                    "_" +  std::to_string(sample) + "/permeability";
                std::string u_coarse = "sample_coarse_" +  std::to_string(level)
    +
                    "_" +  std::to_string(sample) + "/u";
                ASSERT_TRUE(FileExists("data/vtk/" + name_coarse + ".vtk"));
                ASSERT_TRUE(FileExists("data/vtk/" + u_coarse + ".vtk"));
            }
        }
        level++;
    }*/
  }

  void TearDown() {
    Config::Close();

    if (PPM->master()) {
      // system("rm -rf data/vtk/*");
    }
  }
};

TEST_P(TestMainProgram, Coupled) {
  mainProgram->Run();
  ASSERT_TRUE(true);
}

INSTANTIATE_TEST_SUITE_P(CalculationTests, TestMainProgram, Values(
    ConfigMaps({"", defaultEllipsoidConfig, {}})
));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}
