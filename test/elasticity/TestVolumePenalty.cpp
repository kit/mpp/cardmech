#include "TestEnvironmentCardMech.hpp"
#include "TestConfigurations.hpp"
#include "MainElasticity.hpp"

constexpr double BEAM_TEST_TOLERANCE = 1e-1;

class VolumePenaltyTest : public TestWithParam<std::string> {
protected:
  MainElasticity *cmMain;

  VolumePenaltyTest() {}

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Static";
    testConfig["MechDiscretization"] = "Passive";
    testConfig["PressureSteps"] = "10";
    testConfig["WithPrestress"] = "0";

    testConfig["MechProblem"] = "ConstantPressure";
    testConfig["LVPressure"] = "10000";
    testConfig["RVPressure"] = "0";
    testConfig["RAPressure"] = "0";
    testConfig["LAPressure"] = "0";

    testConfig["ProblemDimension"] = "3";
    testConfig["ProblemGeometry"] = "Tet";

    testConfig["Mesh"] = "QuarterEllipsoid";
    testConfig["MechLevel"] = "0";
    testConfig["MechPolynomialDegree"] = "1";


    testConfig["ActiveMaterial"] = "Bonet";
    testConfig["GuccioneMat_C"] = "10000";
    testConfig["GuccioneMat_bf"] = "10";
    testConfig["GuccioneMat_bs"] = "10";
    testConfig["GuccioneMat_bfs"] = "10";

    testConfig["VolumetricSplit"] = "0";
    testConfig["Incompressible"] = "false";
    testConfig["QuasiCompressiblePenalty"] = GetParam();
    testConfig["VolumetricPenalty"] = "20000";
    testConfig["PermeabilityPenalty"] = "20000";

    Config::Initialize(testConfig);

    cmMain = new MainElasticity();
    cmMain->Initialize();
  }

  void TearDown() override {
    Config::Close();
  }

  ~VolumePenaltyTest() override {
    delete cmMain;
  }
};


TEST_P(VolumePenaltyTest, Consistency) {
  Vector &solution = cmMain->Run();

  mout << norm(solution) << endl;

}


TEST_P(VolumePenaltyTest, Compressibility) {
  mout << "Can't do multiple runs with different volume penalties." << endl;
}

INSTANTIATE_TEST_SUITE_P(TestVolumeFunctions, VolumePenaltyTest, Values(
    "None", "Ciarlet", "Logarithmic"
));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).
      WithPPM().
      WithScreenLogging().
      WithoutDefaultConfig();
  return mppTest.RUN_ALL_MPP_TESTS();
}