add_mpp_test(TestMaterial ELASTICITY)
add_mpp_test(TestCirculation ELASTICITY)
#add_mpp_test(TestTimeStepping ELASTICITY)
add_mpp_test(TestActiveStrain ELASTICITY)
add_mpp_test(TestAssembleConsistency ELASTICITY)
# These should not yet go in the pipeline
#add_mpp_test(TestLShape CardMech)
#add_mpp_test(TestDirichletBeam CardMech)


# These tests are also tested in parallel
add_mpp_test(TestDirichletBeam ELASTICITY)
add_mpp_test(TestPressureElasticity ELASTICITY)
add_mpp_test(TestLinearBeamProblem ELASTICITY)
add_mpp_test(TestQuadraticBeamProblem ELASTICITY)
add_mpp_test(TestLaplaceElasticity ELASTICITY)
#add_mpp_test(TestElasticityBeam ELASTICITY)
#add_mpp_test(TestElasticityBlock ELASTICITY)
#add_mpp_test(TestDynamicBoundary ELASTICITY)
#add_mpp_test(TestOscillation ELASTICITY)
#add_mpp_test(TestCFLCondition ELASTICITY)
#add_mpp_test(TestPrestress ELASTICITY)
add_mpp_test(TestVolume ELASTICITY)
#add_mpp_test(TestVolumePenalty ELASTICITY)

# === Parallel MPI tests ===
#add_mpi_test(TestAssembleConsistency ELASTICITY) DOES NOT WORK IN PARALLEL YET

add_mpi_test(TestDirichletBeam ELASTICITY)
add_mpi_test(TestLaplaceElasticity ELASTICITY)
#TODO has to be fixed
#add_mpi_test(TestViscoElasticity ELASTICITY)
add_mpi_test(TestLinearBeamProblem ELASTICITY)
add_mpi_test(TestQuadraticBeamProblem ELASTICITY)
#add_mpi_test(TestElasticityBeam ELASTICITY)
#add_mpi_test(TestElasticityBlock ELASTICITY)
add_mpi_test(TestDynamicBoundary ELASTICITY)
#add_mpi_test(TestCFLCondition ELASTICITY)
#add_mpi_test(TestPrestress ELASTICITY)
add_mpi_test(TestVolume ELASTICITY)

#add_mpi_test(TestOscillation ELASTICITY)
