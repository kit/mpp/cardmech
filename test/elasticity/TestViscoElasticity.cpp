#include "MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"

#include "TestConfigurations.hpp"
#include "solvers/DynamicMechanicsSolver.hpp"

struct ViscoTestParameter {
  int mechDegree;
  std::string mechDisc;
  std::string mechDynamics;

  friend void PrintTo(const ViscoTestParameter &param, std::ostream *os) {
    *os << param.mechDisc << "ElasticityP" << param.mechDegree << "With" << param.mechDynamics;
  }
};

class ViscoElasticTest : public TestWithParam<ViscoTestParameter> {
protected:
  std::map<std::string, std::string> testConfig;
  std::unique_ptr<MainElasticity> cmMain;

  void SetUp() override {
    testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = GetParam().mechDynamics;
    testConfig["MechDiscretization"] = GetParam().mechDisc;
    testConfig["PressureSteps"] = "1";
    testConfig["Incompressible"] = "false";
    testConfig["QuasiCompressiblePenalty"] = "None";

    testConfig["MechProblem"] = "Oscillation";
    testConfig["MechLevel"] = "1";
    testConfig["MechPolynomialDegree"] = std::to_string(GetParam().mechDegree);
    testConfig["ConfigVerbose"] = "1";

    testConfig["StartTime"] = "0.0";
    testConfig["EndTime"] = "0.01";
    testConfig["DeltaTime"] = "0.001";

    testConfig["Density"] = "1.082";
  }

  void TearDown() override {
    Config::Close();
  }

  ~ViscoElasticTest() override {
  }
};


TEST_P(ViscoElasticTest, ZeroDampingTest) {
  Config::Initialize(testConfig);
  Config::PrintInfo();

  cmMain = std::make_unique<MainElasticity>();
  cmMain->Initialize();

  auto &assemble = cmMain->GetAssemble();
  Vector solution(0.0, cmMain->GetDisplacement());
  ElastodynamicTimeIntegrator solver(GetParam().mechDynamics);
  solver.Method(assemble, solution);

  assemble.ResetTime(0.0, 0.01, 0.001);
  Vector viscoseSolution(0.0, cmMain->GetDisplacement());
  ElastodynamicTimeIntegrator viscoseSolver("Visco" + GetParam().mechDynamics);
  viscoseSolver.Method(assemble, viscoseSolution);

  EXPECT_NEAR(cmMain->EvaluateQuantity(solution - viscoseSolution, "L2Norm"), 0.0, 1e-8);
}


TEST_P(ViscoElasticTest, KelvinVoigtDifferenceTest) {
  testConfig["ViscoelasticDamping"] = "KelvinVoigt";
  testConfig["KelvinVoigtEta"] = "0.5";
  Config::Initialize(testConfig);
  Config::PrintInfo();

  cmMain = std::make_unique<MainElasticity>();
  cmMain->Initialize();

  auto &assemble = cmMain->GetAssemble();
  Vector solution(0.0, cmMain->GetDisplacement());
  ElastodynamicTimeIntegrator solver(GetParam().mechDynamics);
  solver.Method(assemble, solution);

  assemble.ResetTime(0.0, 0.01, 0.001);
  Vector viscoseSolution(0.0, cmMain->GetDisplacement());
  ElastodynamicTimeIntegrator viscoseSolver("Visco" + GetParam().mechDynamics);
  viscoseSolver.Method(assemble, viscoseSolution);

  EXPECT_GT(cmMain->EvaluateQuantity(solution - viscoseSolution, "L2Norm"), 1e-3);
}

INSTANTIATE_TEST_SUITE_P(TestViscoelasticValues, ViscoElasticTest, Values(
    ViscoTestParameter({1, "Conforming", "Euler"}),
    ViscoTestParameter({2, "Conforming", "Euler"}),
    ViscoTestParameter({1, "Conforming", "Newmark"}),
    ViscoTestParameter({2, "Conforming", "Newmark"})
), testing::PrintToStringParamName());


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}