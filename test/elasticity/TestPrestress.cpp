#include "MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"

#include "../TestConfigurations.hpp"

struct TestPrestressParameter {
  int level;
  int degree;
  std::array<double, 4> pressure;
  std::string problemName;
  std::string discType;
};

constexpr double NEWTON_TOLERANCE = 1e-8;

class PrestressTest : public TestWithParam<TestPrestressParameter> {
protected:
  MainElasticity *cmMain;

  PrestressTest() {}

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechRuntype"] = "Default";
    testConfig["MechDynamics"] = "Static";
    testConfig["MechDiscretization"] = GetParam().discType;
    testConfig["PressureSteps"] = "1";
    testConfig["PrestressSteps"] = "5";
    testConfig["WithPrestress"] = "true";

    testConfig["MechEpsilon"] = "1e-12";
    testConfig["NewtonEpsilon"] = "1e-11";
    testConfig["NewtonReduction"] = "1e-12";
    testConfig["WithPrestress"] = "true";


    //testConfig["Mesh"] = GetParam().meshName;
    testConfig["Mechlevel"] = std::to_string(GetParam().level);
    testConfig["MechPolynomialDegree"] = std::to_string(GetParam().degree);

    testConfig["MechProblem"] = GetParam().problemName;
    testConfig["ConstantPressure"] = "0.0";
    testConfig["LVPressure"] = std::to_string(GetParam().pressure[0]);
    testConfig["RVPressure"] = std::to_string(GetParam().pressure[1]);
    testConfig["LAPressure"] = std::to_string(GetParam().pressure[2]);
    testConfig["RAPressure"] = std::to_string(GetParam().pressure[3]);

    Config::Initialize(testConfig);

    cmMain = new MainElasticity();
    cmMain->Initialize();
  }

  void TearDown() override {
    Config::Close();
  }

  ~PrestressTest() override {
    delete cmMain;
  }
};


TEST_P(PrestressTest, TestInitialValue) {
  Vector &solution = cmMain->Run();

  mout << "solution " << solution << endl;
  mout << "num " << cmMain->EvaluateQuantity(solution, "L2") << endl << endl;
  for (row r = solution.rows(); r != solution.rows_end(); ++r) {
    for (int i = 0; i < r.n(); ++i) {
      EXPECT_NEAR(solution(r, i), 0.0, NEWTON_TOLERANCE);
    }
  }

}

INSTANTIATE_TEST_SUITE_P(ConformingBeam, PrestressTest, Values(
    //TestPrestressParameter({0, 0, {0.0, 0.0,-0.0005,0.0}, "BenchmarkBeam3DTet"}),
    TestPrestressParameter({0, 1, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "Conforming"}),
    TestPrestressParameter({1, 1, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "Conforming"}),
    TestPrestressParameter({2, 1, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "Conforming"}),
    TestPrestressParameter({0, 2, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "Conforming"}),
    TestPrestressParameter({1, 2, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "Conforming"})
));
/*
INSTANTIATE_TEST_SUITE_P(DGBeam, PrestressTest, Values(
    //TestPrestressParameter({0, 0, {0.0, 0.0,-0.0005,0.0}, "BenchmarkBeam3DTet"}),
    TestPrestressParameter({0, 1, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "DG"}),
    TestPrestressParameter({1, 1, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "DG"}),
    TestPrestressParameter({2, 1, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "DG"}),
    TestPrestressParameter({0, 2, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "DG"}),
    TestPrestressParameter({1, 2, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "DG"})
));

INSTANTIATE_TEST_SUITE_P(EGBeam, PrestressTest, Values(
    //TestPrestressParameter({0, 0, {0.0, 0.0,-0.0005,0.0}, "BenchmarkBeam3DTet"}),
    TestPrestressParameter({0, 1, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "EG"}),
    TestPrestressParameter({1, 1, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "EG"}),
    TestPrestressParameter({2, 1, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "EG"}),
    TestPrestressParameter({0, 2, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "EG"}),
    TestPrestressParameter({1, 2, {10.0, 0.0, 1.0, 0.0}, "PrestressBeam", "EG"})
));
 */

//TODO Why won't this work?
/*
INSTANTIATE_TEST_SUITE_P(OnEllipsoid, PrestressTest, Values(
    TestPrestressParameter({0, 0, {0.001, 0.0,0.0,0.0}, "PrestressEllipsoid"}),
    TestPrestressParameter({0, 1, {0.005, 0.0,0.0,0.0}, "PrestressEllipsoid"}),
    TestPrestressParameter({0, 2, {0.005, 0.0,0.0,0.0}, "PrestressEllipsoid"}),
    TestPrestressParameter({1, 1, {0.005, 0.0,0.0,0.0}, "PrestressEllipsoid"})
));
 */

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging().WithFileLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}