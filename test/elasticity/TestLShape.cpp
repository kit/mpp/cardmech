#include "TestEnvironmentCardMech.hpp"
#include "TestLShape.hpp"


class LShapeTest : public TestWithParam<TestLShapeParameter> {

protected:
  MainElasticity *cmMain;

  void SetUp() override {
    std::map<std::string, std::string> testConfig = lShapeConfig;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Static";
    testConfig["PressureSteps"] = "1";

    testConfig["MechProblem"] = "LShape";
    testConfig["ProblemDimension"] = std::to_string(GetParam().problemDimension);
    testConfig["ProblemGeometry"] = GetParam().problemGeo;
    testConfig["MechLevel"] = std::to_string(GetParam().problemLevel);
    testConfig["MechPolynomialDegree"] = std::to_string(GetParam().problemDegree);

    testConfig["ExactSolution"] = GetParam().problemDimension < 3;
    Config::Initialize(testConfig);

    cmMain = new MainElasticity();
    cmMain->Initialize();
  }

  void TearDown() override {
    Config::Close();
  }

  ~LShapeTest() override {
    delete cmMain;
  }
};

TEST_P(LShapeTest, L2ErrorTest) {
  Vector &solution = cmMain->Run();

  auto exactSolution = new Vector(cmMain->GetDisplacement());
  cmMain->SetDisplacement(true, *exactSolution);

  double interpolationError = cmMain->L2Error(*exactSolution);

  double totalError = cmMain->L2Error(solution);
  std::cout << GetParam().problemLevel << " - " << exactSolution->size()
            << (exactSolution->size() < 10000 ? "\t" : "") << "\t- "
            << interpolationError << " \t - " << totalError << std::endl;
  //ASSERT_TRUE(true);
  ASSERT_NEAR(interpolationError, totalError,
            pow(solution.dim(), GetParam().problemLevel) * interpolationError);
}

INSTANTIATE_TEST_SUITE_P(TestLShape_Triangle, LShapeTest, Values(
    TestLShapeParameter{"Tet", 2, 0, 1},
    TestLShapeParameter{"Tet", 2, 1, 1},
    TestLShapeParameter{"Tet", 2, 2, 1},
    TestLShapeParameter{"Tet", 2, 3, 1},
    TestLShapeParameter{"Tet", 2, 4, 1},
    TestLShapeParameter{"Tet", 2, 5, 1}
));

INSTANTIATE_TEST_SUITE_P(TestLShape_Quad, LShapeTest, Values(
    TestLShapeParameter{"Quad", 2, 0, 1},
    TestLShapeParameter{"Quad", 2, 1, 1},
    TestLShapeParameter{"Quad", 2, 2, 1},
    TestLShapeParameter{"Quad", 2, 3, 1},
    TestLShapeParameter{"Quad", 2, 4, 1},
    TestLShapeParameter{"Quad", 2, 5, 1}
));


INSTANTIATE_TEST_SUITE_P(TestLShape_Tetrahedron, LShapeTest, Values(
    TestLShapeParameter{"Tet", 3, 0, 1},
    TestLShapeParameter{"Tet", 3, 1, 1},
    TestLShapeParameter{"Tet", 3, 2, 1},
    TestLShapeParameter{"Tet", 3, 3, 1},
    TestLShapeParameter{"Tet", 3, 4, 1}
));

INSTANTIATE_TEST_SUITE_P(TestLShape_Hexahedron, LShapeTest, Values(
    TestLShapeParameter{"Quad", 3, 0, 1},
    TestLShapeParameter{"Quad", 3, 1, 1},
    TestLShapeParameter{"Quad", 3, 2, 1},
    TestLShapeParameter{"Quad", 3, 3, 1},
    TestLShapeParameter{"Quad", 3, 4, 1}
));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig();
  return mppTest.RUN_ALL_MPP_TESTS();
}