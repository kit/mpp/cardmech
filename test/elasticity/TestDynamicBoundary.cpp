#include "MainElasticity.hpp"

#include "TestEnvironmentCardMech.hpp"
#include "TestConfigurations.hpp"


struct BoundaryTestParam {
  int problemDimension;
  double endTime;
  std::string problemGeo;
  std::string bndType;
  std::string funcType;
  int degree;
  int level;

  std::string problemName() const {
    return "Dynamic" + bndType + "Test" + funcType;
  }

  double tolerance() const {
    return (bndType == "Pol" ? 1e-10 : 1e-4) * (endTime * 10.0);
  }

  std::string geometry() const {
    return std::to_string(problemDimension) + "DCube" + problemGeo;
  }

  friend void PrintTo(const BoundaryTestParam &param, std::ostream *os) {
    *os << param.problemName() << "On" << param.geometry();
  }
};

class DBoundaryTest : public TestWithParam<BoundaryTestParam> {
protected:
  MainElasticity *cmMain;

  DBoundaryTest() {
  }

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;
    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Newmark";
    testConfig["MechDiscretization"] = "Lagrange";
    testConfig["PressureSteps"] = "1";
    testConfig["WithPrestress"] = "0";
    testConfig["MechVerbose"] = "1";
    testConfig["ConfigVerbose"] = "1";

    testConfig["MechProblem"] = GetParam().problemName();

    testConfig["Incompressible"] = "false";
    testConfig["QuasiCompressiblePenalty"] = "None";

    testConfig["ProblemDimension"] = std::to_string(GetParam().problemDimension);
    testConfig["ProblemGeometry"] = GetParam().problemGeo;

    testConfig["NewmarkBeta"] = "0.25";
    testConfig["NewmarkGamma"] = "0.5";
    testConfig["MechLevel"] =  std::to_string(GetParam().level);
    testConfig["MechPolynomialDegree"] =  std::to_string(GetParam().degree);

    testConfig["StartTime"] = "0.0";
    testConfig["EndTime"] = std::to_string(GetParam().endTime);
    testConfig["DeltaTime"] = "0.02";
    testConfig["MeshVerbose"] = "10";

    testConfig["Density"] = "1.0";
    Config::Initialize(testConfig);

    cmMain = new MainElasticity();
    //cmMain->Initialize();

  }

  void TearDown() override {
    Config::Close();
    //Plotting::Instance().Clear();
  }

  ~DBoundaryTest() override {
    delete cmMain;
  }
};


TEST_P(DBoundaryTest, L2ErrorTest) {
  cmMain->Initialize();
  Vector &solution = cmMain->Run();

  mout << "num " << cmMain->EvaluateQuantity(solution, "L2") << endl << endl;

  Vector exactSolution(cmMain->GetDisplacement());
  cmMain->SetDisplacement(true, exactSolution);

  double exact = cmMain->EvaluateQuantity(exactSolution, "L2");
  mout << "exact " << exact << endl << endl;
  ASSERT_NEAR(cmMain->EvaluateQuantity(exactSolution, "L2"), 0.0, GetParam().tolerance());
  ASSERT_NEAR(cmMain->EvaluateQuantity(solution, "L2"), 0.0, GetParam().tolerance());
}


INSTANTIATE_TEST_SUITE_P(DynamicTestOneStep, DBoundaryTest, Values(
    BoundaryTestParam({2, 0.02, "Quad", "Dirichlet", "Pol",2,0}),
    BoundaryTestParam({2, 0.02, "Quad", "Dirichlet", "Exp",2,0}),
   BoundaryTestParam({2, 0.02, "Quad", "Stiffness", "Exp",2,0}),

    BoundaryTestParam({2, 0.02, "Tet", "Dirichlet", "Pol",3,0}),
    BoundaryTestParam({2, 0.02, "Tet", "Dirichlet", "Exp",3,0}),
    BoundaryTestParam({2, 0.02, "Tet", "Stiffness", "Exp",3,0}),
//
   BoundaryTestParam({3, 0.02, "Quad", "Dirichlet", "Pol",2,0}),
   BoundaryTestParam({3, 0.02, "Quad", "Dirichlet", "Exp",2,0}),
   BoundaryTestParam({3, 0.02, "Quad", "Stiffness", "Exp",2,0}),
   BoundaryTestParam({3, 0.02, "Quad", "Neumann", "Pol",2,0}),
   BoundaryTestParam({3, 0.02, "Quad", "Neumann", "Exp",2,1}),

    BoundaryTestParam({3, 0.02, "Tet", "Dirichlet", "Pol",3,0}),
    BoundaryTestParam({3, 0.02, "Tet", "Dirichlet", "Exp",3,0}),
    BoundaryTestParam({3, 0.02, "Tet", "Stiffness", "Exp",3,0}),
    BoundaryTestParam({3, 0.02, "Tet", "Neumann", "Pol",3,0}),
    BoundaryTestParam({3, 0.02, "Tet", "Neumann", "Exp",3,0})
), testing::PrintToStringParamName());


INSTANTIATE_TEST_SUITE_P(DynamicTestTwoStep, DBoundaryTest, Values(
    BoundaryTestParam({2, 0.04, "Quad", "Dirichlet", "Pol",2,0}),
    BoundaryTestParam({2, 0.04, "Quad", "Dirichlet", "Exp",2,0}),
    BoundaryTestParam({2, 0.04, "Quad", "Stiffness", "Exp",2,0}),
//
    BoundaryTestParam({2, 0.04, "Tet", "Dirichlet", "Pol",3,0}),
    BoundaryTestParam({2, 0.04, "Tet", "Dirichlet", "Exp",3,0}),
    BoundaryTestParam({2, 0.04, "Tet", "Stiffness", "Exp",3,0}),
//
    BoundaryTestParam({3, 0.04, "Quad", "Dirichlet", "Pol",2,0}),
    BoundaryTestParam({3, 0.04, "Quad", "Dirichlet", "Exp",2,0}),
    BoundaryTestParam({3, 0.04, "Quad", "Stiffness", "Exp",2,0}),
    BoundaryTestParam({3, 0.04, "Quad", "Neumann", "Pol",2,0}),
    BoundaryTestParam({3, 0.04, "Quad", "Neumann", "Exp",2,2}),

    BoundaryTestParam({3, 0.04, "Tet", "Dirichlet", "Pol",3,0}),
    BoundaryTestParam({3, 0.04, "Tet", "Dirichlet", "Exp",3,0}),
    BoundaryTestParam({3, 0.04, "Tet", "Stiffness", "Exp",3,0}),
    BoundaryTestParam({3, 0.04, "Tet", "Neumann", "Pol",3,0}),
    BoundaryTestParam({3, 0.04, "Tet", "Neumann", "Exp",3,1})
), testing::PrintToStringParamName());


INSTANTIATE_TEST_SUITE_P(DynamicTestFullTime, DBoundaryTest, Values(
    BoundaryTestParam({2, 1.0, "Quad", "Dirichlet", "Pol",3,0}),
    BoundaryTestParam({2, 1.0, "Quad", "Dirichlet", "Exp",2,4}),
    BoundaryTestParam({2, 1.0, "Quad", "Stiffness", "Exp",2,4}),

    BoundaryTestParam({2, 1.0, "Tet", "Dirichlet", "Pol",3,0}),
    BoundaryTestParam({2, 1.0, "Tet", "Dirichlet", "Exp",2,4}),
    BoundaryTestParam({2, 1.0, "Tet", "Stiffness", "Exp",2,4}),

    BoundaryTestParam({3, 1.0, "Quad", "Dirichlet", "Pol",3,0}),
    BoundaryTestParam({3, 1.0, "Quad", "Dirichlet", "Exp",3,0}),
    BoundaryTestParam({3, 1.0, "Quad", "Stiffness", "Exp",3,0}),
    BoundaryTestParam({3, 1.0, "Quad", "Neumann", "Pol",3,0}),
    BoundaryTestParam({3, 1.0, "Quad", "Neumann", "Exp",3,0}),
//
    BoundaryTestParam({3, 1.0, "Tet", "Dirichlet", "Pol",3,0}),
    BoundaryTestParam({3, 1.0, "Tet", "Dirichlet", "Exp",3,0}),
    BoundaryTestParam({3, 1.0, "Tet", "Stiffness", "Exp",3,0}),
    BoundaryTestParam({3, 1.0, "Tet", "Neumann", "Pol",3,0}),
    BoundaryTestParam({3, 1.0, "Tet", "Neumann", "Exp",3,1})
), testing::PrintToStringParamName());


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).
      WithPPM().
      WithFileLogging().
      WithScreenLogging().
      WithoutDefaultConfig();
  return mppTest.RUN_ALL_MPP_TESTS();
}