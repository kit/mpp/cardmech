#include <numbers>

#include <GlobalDefinitions.hpp>
#include "pressure_bnd/MCircModel.hpp"
#include "TestEnvironmentCardMech.hpp"
#include "TimeSeries.hpp"


TEST(CirculationTest, SingleStep) {
  MCircModel circModel;
}


TEST(CirculationTest, Periodic){
  using std::numbers::pi;

  MCircModel circModel;
  TimeSeries TS(0.0, 1.0, 0.001);

  double t = TS.FirstTStep();
  std::array<double, 8> volumes{
      188.5126343, 200.5630151, 98.54820298, 70.40868708,
      800.0, 2850.0, 150.0, 200.0
  };
  std::array<double, 4> pressure{};

  double periodicTime{0.0};
  while(t != TS.LastTStep()) {
    if (periodicTime >= 1.0) periodicTime -= 1.0;

    if (periodicTime < 0.025) {
      pressure[2] = pressure[3] = 5.0 * (-cos(0.5 * pi / 0.025 * periodicTime) + 1);
    } else if (periodicTime < 0.05) {
      pressure[2] = pressure[3] = 5.0 * (cos(0.5 * pi / 0.025 * (periodicTime - 0.025)) + 1);
    } else if (periodicTime > 0.35) {
      pressure[2] = pressure[3] = 0.0;
    } else if (periodicTime > 0.3) {
      pressure[2] = pressure[3] = 5.0 * (cos(0.5 * pi / 0.05 * (periodicTime - 0.3)) + 1);
    } else if (periodicTime > 0.15) {
      pressure[2] = pressure[3] = 5.0 * (-cos(0.5 * pi / 0.15 * (periodicTime - 0.15)) + 1);
    } else {
      pressure[2] = pressure[3] = 0.0;
    }

    if (periodicTime < 0.15) {
      pressure[0] = pressure[1] = 60 * (-cos(0.5 * pi / 0.15 * periodicTime) + 1);
    } else if (periodicTime < 0.3) {
      pressure[0] = pressure[1] = 60.0 * (cos(0.5 * pi / 0.15 * (periodicTime - 0.15)) + 1);
    } else pressure[0] = pressure[1] = 0.0;

    std::cout << "Volumes at t=" << t << " : " << std::endl;
    for (const auto &v: volumes) {
      std::cout << v << "      ";
    }
    std::cout << std::endl;

    circModel.SolveStep(TS.StepSize(), t, volumes, pressure);

    periodicTime += TS.StepSize();
    t = TS.NextTimeStep();
  }
}

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}