//
// Created by laura on 17.11.22.
//

#include "TestPressureElasticity.hpp"


INSTANTIATE_TEST_SUITE_P(Test3DPullPressureElasticity, TestPressureElasticity, ValuesIn(PullPressureParameters()));
INSTANTIATE_TEST_SUITE_P(Test3DPushPressureElasticity, TestPressureElasticity, ValuesIn(PushPressureParameters()));


int main(int argc, char **argv) {
    MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging();
    return mppTest.RUN_ALL_MPP_TESTS();
}
