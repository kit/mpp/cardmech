#ifndef TESTELASTICITY_H
#define TESTELASTICITY_H

#include "gtest/gtest.h"

#include "elasticity/MainElasticity.hpp"
struct TestLShapeParameter {
    std::string problemGeo;
    int problemDimension;
    int problemLevel;
    int problemDegree;
};

const std::map<std::string, std::string> lShapeConfig = {
    // ----- Problem Settings -----

    {"GeoPath",                      "../../../geo/"},
    {"EulerReAssemble",              "0"},

// === Linear Solver === //
    {"LinearSolver",                 "gmres"},
    {"LinearEpsilon",                "1e-8"},
    {"LinearReduction",              "1e-8"},
    {"LinearReduction",              "1e-11"},
    {"LinearSteps",                  "10000"},

    {"Preconditioner",               "LIB_PS"},

// =================================================================
// === Gating Solver ===============================================
    {"GatingSolver",                 "gmres"},
    {"GatingPreconditioner",         "GaussSeidel"},
    {"GatingEpsilon",                "1e-8"},
    {"GatingReduction",              "1e-12"},

// === Elphy Solver ================================================
    {"ElphySolver",                  "gmres"},
    {"ElphyPreconditioner",          "GaussSeidel"},
    {"ElphyEpsilon",                 "1e-8"},
    {"ElphyReduction",               "1e-12"},

// === Stretch Solver ==============================================
    {"StretchSolver",                "gmres"},
    {"StretchPreconditioner",        "GaussSeidel"},
    {"StretchEpsilon",               "1e-8"},
    {"StretchReduction",             "1e-12"},

// === Mechanics Solver ============================================
    {"MechSolver",                   "gmres"},
    {"MechPreconditioner",           "GaussSeidel"},
    {"MechEpsilon",                  "1e-8"},
    {"MechReduction",                "1e-12"},
    {"MechSteps",                    "10000"},

// === Newton Method === //
    {"NewtonEpsilon",                "1e-7"},
    {"NewtonReduction",              "1e-10"},
    {"NewtonSteps",                  "200"},
    {"NewtonLineSearchSteps",        "50"},
    {"NewtonLinearizationReduction", "1e-12"},

    {"Smoother",                     "GaussSeidel"},

    {"presmoothing",                 "3"},
    {"postsmoothing",                "3"},

    {"Overlap_Distribution",         "0"},

    {"DebugLevel",                   "0"},
    {"TimeLevel",                    "0"},
    {"precision",                    "10"},

    {"MechPlot",                     "0"},
    {"PlotVertexPotential",          "0"},

    {"PlotVTK",                      "0"},
    {"PlottingSteps",                "1"},

    {"h0",                           "1e-4"},
    {"h_min",                        "1e-6"},

    {"PlotStress",                   "0"},

    {"plevel",                       "0"}
};
#endif //TESTELASTICITY_H
