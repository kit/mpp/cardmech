#include "MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"

#include "../TestConfigurations.hpp"

struct TestLaplaceParameter {
  int problemDimension;
  int problemLevel;
  int problemDegree;
};


constexpr double LAPLACE_TEST_TOLERANCE = 1e-6;

class LaplaceTest : public TestWithParam<TestLaplaceParameter> {
protected:
  MainElasticity *cmMain;
  std::string is_tet = "Quad";

  LaplaceTest(const std::string &tet) {
    is_tet = tet;
  }

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Static";
    testConfig["MechDiscretization"] = "Passive";
    testConfig["PressureSteps"] = "1";
    testConfig["Incompressible"] = "false";
    testConfig["QuasiCompressiblePenalty"] = "None";

    testConfig["MechProblem"] = "LaplaceDegree1";
    testConfig["ProblemDimension"] = std::to_string(GetParam().problemDimension);
    testConfig["ProblemGeometry"] = is_tet;
    testConfig["MechLevel"] = std::to_string(GetParam().problemLevel);
    testConfig["MechPolynomialDegree"] = std::to_string(GetParam().problemDegree);
    testConfig["ConfigVerbose"] = "1";
    Config::Initialize(testConfig);
    Config::PrintInfo();

    cmMain = new MainElasticity();
    cmMain->Initialize();
  }

  void TearDown() override {
    Config::Close();
  }

  ~LaplaceTest() override {
    delete cmMain;
  }
};

class TestQuadLaplace : public LaplaceTest {
public:
  TestQuadLaplace() : LaplaceTest("Quad") {};
};

class TestTetLaplace : public LaplaceTest {
public:
  TestTetLaplace() : LaplaceTest("Tet") {};
};


TEST_P(TestQuadLaplace, L2ErrorTest) {
  auto exactSolution = new Vector(cmMain->GetDisplacement());
  cmMain->SetDisplacement(true, *exactSolution);
  Vector &solution = cmMain->Run();


  mout << solution << endl << endl << *exactSolution << endl;
  ASSERT_NEAR(cmMain->EvaluateQuantity(*exactSolution, "L2"), 0.0, tolerance);
  ASSERT_NEAR(cmMain->EvaluateQuantity(solution, "L2"), 0.0, LAPLACE_TEST_TOLERANCE);
}

TEST_P(TestTetLaplace, L2ErrorTest) {
  auto exactSolution = new Vector(cmMain->GetDisplacement());
  cmMain->SetDisplacement(true, *exactSolution);
  Vector &solution = cmMain->Run();

  ASSERT_NEAR(cmMain->EvaluateQuantity(*exactSolution, "L2"), 0.0, tolerance);
  ASSERT_NEAR(cmMain->EvaluateQuantity(solution, "L2"), 0.0, LAPLACE_TEST_TOLERANCE);
}

auto laplace2DValues = Values(
    TestLaplaceParameter({2, 0, 1}),
    TestLaplaceParameter({2, 1, 1}),
    TestLaplaceParameter({2, 0, 2}),
    TestLaplaceParameter({2, 1, 2}),
    TestLaplaceParameter({2, 0, 3}),
    TestLaplaceParameter({2, 1, 3}),
    TestLaplaceParameter({2, 0, 4}),
    TestLaplaceParameter({2, 1, 4}));

auto laplace3DValues = Values(
    TestLaplaceParameter({3, 0, 1}),
    TestLaplaceParameter({3, 1, 1}),
    TestLaplaceParameter({3, 0, 2}),
    TestLaplaceParameter({3, 1, 2})/*,
    TestLaplaceParameter({3, 0, 3}),
    TestLaplaceParameter({3, 1, 3}),
    TestLaplaceParameter({3, 0, 4})*/
    );

INSTANTIATE_TEST_SUITE_P(Test2DLaplace, TestQuadLaplace, laplace2DValues);
INSTANTIATE_TEST_SUITE_P(Test2DLaplace, TestTetLaplace, laplace2DValues);

INSTANTIATE_TEST_SUITE_P(Test3DLaplace, TestQuadLaplace, laplace3DValues);
INSTANTIATE_TEST_SUITE_P(Test3DLaplace, TestTetLaplace, laplace3DValues);

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig().WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}