#ifndef TESTHOLZAPFELMATERIAL_HPP
#define TESTHOLZAPFELMATERIAL_HPP

#include "materials/HolzapfelMaterial.hpp"

INSTANTIATE_TYPED_TEST_SUITE_P(Holzapfel, MaterialTest, HolzapfelMaterial);

std::vector<MaterialTestParameters> holzapfelTestParameters{
    {
        std::vector{1.0, 0.0, 0.0, 0.0, 1.0 / 3.0, 1.0, 1.0, 1.0},
        Tensor{2, 0, 0, 0, 1, 0, 0, 0, 1},
        3.0 / 2.0 * (exp(1.0) - 1.0),
        (2.0 * 2.0) * exp(1.0),
        37.149851 // (2.0 * 1.5 + 4.0 * 3.0) * exp(1.0)
    },
    {
        std::vector{0.0, 1.0, 0.0, 0.0, 1.0, 1.0 / 9.0, 1.0, 1.0},
        Tensor{2, 0, 0, 0, 1, 0, 0, 0, 1},
        9.0 / 2.0 * (exp(1.0) - 1.0),
        (2.0 * 2.0 * 3.0) * exp(1.0),
        (2.0 * 3.0 + 4.0 * 3.0 * 2.0 * 2.0) * exp(1.0)
    },
    {
        std::vector{0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 1.0 / 9.0, 1.0},
        Tensor{1, 0, 0, 0, 2, 0, 0, 0, 1},
        9.0 / 2.0 * (exp(1.0) - 1.0),
        (2.0 * 2.0 * 3.0) * exp(1.0),
        (2.0 * 3.0 + 4.0 * 3.0 * 2.0 * 2.0) * exp(1.0)
    },
    {
        std::vector{0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0},
        Tensor{2, 0, 0, 0, 2, 0, 0, 0, 2},
        0.0,
        0.0,
        0.0
    }
};

class HolzapfelMaterialTest : public TestWithParam<MaterialTestParameters> {
protected:
  HolzapfelMaterial hMat;
public:
  HolzapfelMaterialTest() : hMat(GetParam().matParams) {
  }
};


TEST_P(HolzapfelMaterialTest, AlignedEnergy) {
  Tensor Q = One;

  double e = hMat.IsotropicEnergy(GetParam().deformationGradient)
             + hMat.AnisotropicEnergy(GetParam().deformationGradient, Q);
  EXPECT_NEAR(e, GetParam().energy, 1e-14);
}


TEST_P(HolzapfelMaterialTest, AlignedDerivative) {
  Tensor Q = One;

  double e = hMat.IsotropicDerivative(GetParam().deformationGradient, One)
             + hMat.AnisotropicDerivative(GetParam().deformationGradient, Q, One);
  EXPECT_NEAR(e, GetParam().derivative, 1e-14);
}


TEST_P(HolzapfelMaterialTest, AlignedSecondDerivative) {
  Tensor Q = One;

  double e = hMat.IsotropicSecondDerivative(GetParam().deformationGradient, One, One)
             + hMat.AnisotropicSecondDerivative(GetParam().deformationGradient, Q, One, One);
  //TODO: The bad error is caused by the usage of numerical second derivatives.
  EXPECT_NEAR(e, GetParam().second_derivative, 1e-6);
}

/*
TEST_P(HolzapfelMaterialTest, RotatedEnergy) {
  Tensor Q = FixedRotation(2, 45);
  Tensor F = RotatedDeformationGradient(GetParam().deformationGradient, Q);

  double e = hMat.IsotropicEnergy(Q, F);
  EXPECT_NEAR(e, GetParam().energy, 1e-14);
}

TEST_P(HolzapfelMaterialTest, RotatedDerivative) {
  Tensor Q = FixedRotation(2, 45);
  Tensor F = RotatedDeformationGradient(GetParam().deformationGradient, Q);

  double e = hMat.IsotropicDerivative(Q, F, One);
  EXPECT_NEAR(e, GetParam().derivative, 1e-14);
}


TEST_P(HolzapfelMaterialTest, RotatedSecondDerivative) {
  Tensor Q = FixedRotation(2, 45);
  Tensor F = RotatedDeformationGradient(GetParam().deformationGradient, Q);

  double e = hMat.IsotropicSecondDerivative(Q, F, One, One);
  EXPECT_NEAR(e, GetParam().second_derivative, 1e-12);
}*/


TEST_P(HolzapfelMaterialTest, DerivativeRowConsistency) {
  Tensor F = randomTensor(1.0);

  for (int i = 0; i < 3; ++i) {
    VectorField h = randomVectorField(1.0);
    TensorRow HR(i, h);
    Tensor H(HR);

    EXPECT_DOUBLE_EQ(hMat.IsotropicDerivative(F, H) + hMat.AnisotropicDerivative(F, One, H),
                     hMat.IsotropicDerivative(F,HR)+hMat.AnisotropicDerivative(F, One, HR));
  }
}


TEST_P(HolzapfelMaterialTest, SecondDerivativeRowConsistency) {
  Tensor F = randomTensor(1.0);

  for (int i = 0; i < 3; ++i) {
    VectorField h = randomVectorField(1.0);
    TensorRow HR(i, h);
    Tensor H(HR);
    for (int j = 0; j < 3; ++j) {
      VectorField g = randomVectorField(1.0);
      TensorRow GR(j, g);
      Tensor G(GR);

      EXPECT_DOUBLE_EQ(hMat.IsotropicSecondDerivative(F, H, G) + hMat.AnisotropicSecondDerivative(F, One, H, G),
                       hMat.IsotropicSecondDerivative(F, HR, GR) + hMat.AnisotropicSecondDerivative(F, One, HR, GR));
    }
  }
}

INSTANTIATE_TEST_SUITE_P(HolzapfelMaterial, HolzapfelMaterialTest, ValuesIn(holzapfelTestParameters)
);

#endif //TESTHOLZAPFELMATERIAL_HPP
