#ifndef TESTLINEARMATERIAL_H
#define TESTLINEARMATERIAL_H

#include "materials/LinearMaterial.hpp"
#include "TestDefaultMaterial.hpp"


INSTANTIATE_TYPED_TEST_SUITE_P(Linear, MaterialTest, LinearMaterial);

TEST(LinearMaterialTest, NonDefaultEnergy) {
  const LinearMaterial lMat({1.0, 1.0});
  EXPECT_EQ(lMat.IsotropicEnergy(3 * One), 30.0);
}

TEST(LinearMaterialTest, NonDefaultFirstDerivative) {
  const LinearMaterial lMat({1.0,1.0});
  EXPECT_EQ(lMat.IsotropicDerivative(2 * One, One),
            15.0);
}

TEST(LinearMaterialTest, NonDefaultSecondDerivative) {
  const LinearMaterial lMat({1.0,1.0});
  EXPECT_EQ(lMat.IsotropicSecondDerivative(2 * One, One, Zero),
            0.0);
}

#endif //TESTLINEARMATERIAL_H
