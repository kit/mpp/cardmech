#ifndef TESTGUCCIONEMATERIAL_H
#define TESTGUCCIONEMATERIAL_H

#include "materials/GuccioneMaterial.hpp"
#include "TestDefaultMaterial.hpp"


INSTANTIATE_TYPED_TEST_SUITE_P(Guccione, MaterialTest, GuccioneMaterial);
INSTANTIATE_TYPED_TEST_SUITE_P(Bonet, MaterialTest, GuccioneBonetMaterial);


Tensor randomRotation() {
  int pivot = rand() % 3;
  int angle = rand() % 180 + 1;

  return FixedRotation(pivot, angle);
}

std::vector<MaterialTestParameters> guccioneTestParameters{
    {
        std::vector{2.0, 1.0 / 16.0, 0.0, 0.0},
        Tensor{3, 0, 0, 0, 1, 0, 0, 0, 1},
        exp(1.0) - 1.0,
        exp(1) * 1.5,
        10.533342085278809
    },
    {
        std::vector{2.0, 1.0 / 16.0, 1.0 / 16.0, 1.0 / 16.0},
        Tensor{3, 0, 0, 0, 1, 0, 0, 0, 1},
        exp(1.0) - 1.0,
        exp(1) * 1.5,
        11.212912542393562
    },
    {
        std::vector{2.0, 1.0, 0.0, 0.0},
        Tensor{2, 0, 0, 0, 1, 0, 0, 0, 1},
        exp(1.5 * 1.5) - 1.0,
        exp(1.5 * 1.5) * 6.0,
        exp(1.5 * 1.5) * (3.0 + 2.0*(6.0*3.0+ 4.0))//exp(4*1.5*1.5) * (3.0 * 12.0)
    },
    {
        std::vector{2.0, 0.0, 4.0, 0.0},
        Tensor{2, 0, 0, 0, 1, 0, 0, 0, 1},
        0.0,
        0.0,
        16.0//exp(1) * (0.25 + 81.0 / 16.0)
    },
    {
        std::vector{2.0, 0.0, 0.0, 2.0},
        Tensor{2, 0, 0, 0, 1, 0, 0, 0, 1},
        0.0,
        0.0,
        0.0 //exp(1) * (0.25 + 81.0 / 16.0)
    }
};

class GuccioneMaterialTest : public TestWithParam<MaterialTestParameters> {
protected:
  GuccioneMaterial gMat;
  GuccioneBonetMaterial bMat;
public:
  GuccioneMaterialTest() : gMat(GetParam().matParams), bMat(GetParam().matParams) {

  }
};

TEST_P(GuccioneMaterialTest, EquivalentFormulation) {
  Tensor Q = FixedRotation(2, 45.0);
  Tensor QT = transpose(Q);
  VectorField f = QT[0];

  Tensor F = RotatedDeformationGradient(GetParam().deformationGradient, Q);
  Tensor E = 0.5 * (transpose(F) * F - One);

  Tensor FQ = GetParam().deformationGradient;
  Tensor EQ = 0.5 * (transpose(FQ) * FQ - One);

  VectorField e{1, 0, 0};


  EXPECT_DOUBLE_EQ(e * (EQ * e), f * (E * f));
  EXPECT_DOUBLE_EQ(Frobenius(EQ, EQ), Frobenius(E, E));
  EXPECT_DOUBLE_EQ((EQ * e) * (EQ * e), (E * f) * (E * f));
}

TEST_P(GuccioneMaterialTest, GuccioneEnergy) {
  Tensor Q = FixedRotation(2, 45.0);
  Tensor QT = transpose(Q);
  Tensor F = RotatedDeformationGradient(GetParam().deformationGradient, Q);

  double e = gMat.IsotropicEnergy(F) + gMat.AnisotropicEnergy(F, QT);
  EXPECT_NEAR(e, GetParam().energy, 1e-10);
}


TEST_P(GuccioneMaterialTest, BonetEnergy) {
  Tensor Q = FixedRotation(2, 45.0);
  Tensor QT = transpose(Q);

  Tensor F = RotatedDeformationGradient(GetParam().deformationGradient, Q);

  double e = bMat.IsotropicEnergy(F) + bMat.AnisotropicEnergy(F, QT);
  EXPECT_NEAR(e, GetParam().energy, 1e-10);
}


TEST_P(GuccioneMaterialTest, GuccioneDerivative) {
  Tensor Q = FixedRotation(2, 45.0);
  Tensor QT = transpose(Q);
  Tensor F = RotatedDeformationGradient(GetParam().deformationGradient, Q);
  Tensor H = One;

  double e = gMat.IsotropicDerivative(F, H) + gMat.AnisotropicDerivative(F, QT, H);
  EXPECT_NEAR(e, GetParam().derivative, 1e-8);
}


TEST_P(GuccioneMaterialTest, BonetDerivative) {
  Tensor Q = FixedRotation(2, 45.0);
  Tensor QT = transpose(Q);

  Tensor F = RotatedDeformationGradient(GetParam().deformationGradient, Q);
  Tensor H = One;
  double e = bMat.IsotropicDerivative(F, H) + bMat.AnisotropicDerivative(F, QT, H);
  EXPECT_NEAR(e, GetParam().derivative, 1e-8);
}


TEST_P(GuccioneMaterialTest, GuccioneSecondDerivative) {
  Tensor Q = FixedRotation(2, 45.0);
  Tensor QT = transpose(Q);
  Tensor F = RotatedDeformationGradient(GetParam().deformationGradient, Q);
  Tensor H = One;
  Tensor G = One;

  double e =
      gMat.IsotropicSecondDerivative(F, H, G) + gMat.AnisotropicSecondDerivative(F, QT, H, G);
  EXPECT_NEAR(e, GetParam().second_derivative, 1e-4);
}


TEST_P(GuccioneMaterialTest, BonetSecondDerivative) {
  Tensor Q = FixedRotation(2, 45.0);
  Tensor QT = transpose(Q);

  Tensor F = RotatedDeformationGradient(GetParam().deformationGradient, Q);
  Tensor H = One;
  Tensor G = One;

  double e =
      bMat.IsotropicSecondDerivative(F, H, G) + bMat.AnisotropicSecondDerivative(F, QT, H, G);
  EXPECT_NEAR(e, GetParam().second_derivative, 1e-8);
}



TEST_P(GuccioneMaterialTest, DerivativeRowConsistency) {
  Tensor F = randomTensor(1.0);

  for (int i = 0; i < 3; ++i) {
    VectorField h = randomVectorField(1.0);
    TensorRow HR(i, h);
    Tensor H(HR);

    EXPECT_DOUBLE_EQ(bMat.IsotropicDerivative(F, H) + bMat.AnisotropicDerivative(F, One, H),
                     bMat.IsotropicDerivative(F,HR)+bMat.AnisotropicDerivative(F, One, HR));
  }
}


TEST_P(GuccioneMaterialTest, SecondDerivativeRowConsistency) {
  Tensor F = randomTensor(1.0);

  for (int i = 0; i < 3; ++i) {
    VectorField h = randomVectorField(1.0);
    TensorRow HR(i, h);
    Tensor H(HR);
    for (int j = 0; j < 3; ++j) {
      VectorField g = randomVectorField(1.0);
      TensorRow GR(j, g);
      Tensor G(GR);

      EXPECT_DOUBLE_EQ(bMat.IsotropicSecondDerivative(F, H, G) + bMat.AnisotropicSecondDerivative(F, One, H, G),
                       bMat.IsotropicSecondDerivative(F, HR, GR) + bMat.AnisotropicSecondDerivative(F, One, HR, GR));
    }
  }
}

INSTANTIATE_TEST_SUITE_P(GuccioneMaterial, GuccioneMaterialTest, ValuesIn(guccioneTestParameters)
);

#endif //TESTGUCCIONEMATERIAL_H
