#ifndef TESTNEOHOOKEMATERIAL_HPP
#define TESTNEOHOOKEMATERIAL_HPP

#include "materials/NeoHookeMaterial.hpp"
#include "TestDefaultMaterial.hpp"

INSTANTIATE_TYPED_TEST_SUITE_P(NeoHooke, MaterialTest, NeoHookeMaterial);


std::vector<MaterialTestParameters> neohookeTestParameters{
    {
        std::vector{1.0, 1.0},
        Tensor{3, 0, 0, 0, 1, 0, 0, 0, 1},
        exp(1.0) - 1.0,
        exp(1) * 0.75,
        5.2666710 //exp(1) * (0.25 + 81.0 / 16.0)
    }
};

class NeoHookeMaterialTest : public TestWithParam<MaterialTestParameters> {
protected:
  Material mat;
public:
  NeoHookeMaterialTest() : mat("NeoHooke", GetParam().matParams) {

  }
};

TEST_P(NeoHookeMaterialTest, PenaltyInclusion) {
  double lambda = GetParam().matParams[0];
  double mu = GetParam().matParams[1];

  Tensor F = 2 * One;

  EXPECT_DOUBLE_EQ(lambda * mat.VolEnergy(F), lambda * 0.5 * 49.0 - mu * log(8.0));
}

INSTANTIATE_TEST_SUITE_P(NeoHookeMaterial, NeoHookeMaterialTest, ValuesIn(neohookeTestParameters)
);

#endif //CARDMECH_TESTNEOHOOKEMATERIAL_HPP
