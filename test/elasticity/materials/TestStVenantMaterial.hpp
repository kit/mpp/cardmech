#ifndef CARDMECH_TESTSTVENANTMETERIAL_HPP
#define CARDMECH_TESTSTVENANTMETERIAL_HPP

#include "materials/StVenantKirchhoffMaterial.hpp"
#include "TestDefaultMaterial.hpp"

INSTANTIATE_TYPED_TEST_SUITE_P(StVenantKirchhoff, MaterialTest, StVenantKirchhoffMaterial);
#endif //CARDMECH_TESTNEOHOOKEMATERIAL_HPP
