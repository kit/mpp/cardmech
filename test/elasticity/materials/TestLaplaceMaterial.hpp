#ifndef CARDMECH_TESTLAPLACEMATERIAL_HPP
#define CARDMECH_TESTLAPLACEMATERIAL_HPP

#include "materials/LaplaceMaterial.hpp"
#include "TestDefaultMaterial.hpp"

INSTANTIATE_TYPED_TEST_SUITE_P(Laplace, MaterialTest, LaplaceMaterial);

#endif //CARDMECH_TESTLAPLACEMATERIAL_HPP
