#ifndef TESTDEFAULTMATERIAL_H
#define TESTDEFAULTMATERIAL_H

#include "TestEnvironmentCardMech.hpp"
#include "materials/Material.hpp"
#include "Tensor.hpp"

double test_tolerance = 1e-8;

static const Tensor RotatedDeformationGradient(const Tensor &F, const Tensor &S) {
  return S * F * Invert(S);
}

static
Tensor FixedRotation(int pivot, double angle) {
  switch (pivot) {
    case 0:
      return Tensor{1.0, 0.0, 0.0, 0.0, cos(angle), -sin(angle), 0.0, sin(angle), cos(angle)};
    case 1:
      return Tensor{cos(angle), 0.0, sin(angle), 0.0, 1.0, 0.0, -sin(angle), 0.0, cos(angle)};
    case 2:
      return Tensor{cos(angle), -sin(angle), 0.0, sin(angle), cos(angle), 0.0, 0.0, 0.0, 1.0};
    default:
      return One;
  }
}

struct MaterialTestParameters {
  std::vector<double> matParams;
  Tensor deformationGradient;
  double energy;
  double derivative;
  double second_derivative;
};


static double numMatDerivative(const HyperelasticMaterial &mat, const Tensor &F, const Tensor &Q,
                               const Tensor &H) {
  double h = 1e-8;
  double dwi = mat.IsotropicEnergy(F + h * H) + mat.AnisotropicEnergy(F + h * H, Q)
               - mat.IsotropicEnergy(F - h * H) - mat.AnisotropicEnergy(F - h * H, Q);
  return 0.5 * dwi / h;
}

static double
numMatSecondDerivative(const HyperelasticMaterial &mat, const Tensor &F, const Tensor &Q,
                       const Tensor &H, const Tensor &G) {
  double h = 1e-8;
  double dwi = mat.IsotropicDerivative(F + h * H, G) + mat.AnisotropicDerivative(F + h * H, Q, G)
               - mat.IsotropicDerivative(F - h * H, G) - mat.AnisotropicDerivative(F - h * H, Q, G);
  return 0.5 * dwi / h;
}

template<typename T>
class MaterialTest : public Test {
protected:
public:
  typedef std::list<T> List;
  static const T shared_;
  T value_;
};

std::vector<double> getParameterVector(int N) {
  std::vector<double> params(N);
  std::generate(params.begin(), params.end(), []() { return (double) rand() / RAND_MAX; });
  return params;
}

VectorField randomVectorField(double scale) {
  std::vector<double> entries(3);
  std::generate(entries.begin(), entries.end(), []() { return (double) rand() / RAND_MAX; });
  VectorField V(entries[0], entries[1], entries[2]);
  return scale * V;
}

Tensor randomTensor(double scale) {
  std::vector<double> entries(9);
  std::generate(entries.begin(), entries.end(), []() { return (double) rand() / RAND_MAX; });
  Tensor A(entries[0], entries[1], entries[2], entries[3], entries[4], entries[5], entries[6],
           entries[7], entries[8]);
  return scale * A;
}


TYPED_TEST_SUITE_P(MaterialTest);

TYPED_TEST_P(MaterialTest, DefaultInstantiation) {
  TypeParam mat{};
  for (int i = 0; i < mat.ParameterCount(); ++i)
    EXPECT_EQ(mat.GetParameter(i), 1.0);
}

TYPED_TEST_P(MaterialTest, MoveInstantiation) {
  int n = this->value_.ParameterCount();
  std::vector<double> params(n);
  for (int i = 0; i < n; ++i)
    params[i] = i + 1.5;
  TypeParam mat{move(params)};

  for (int i = 0; i < n; ++i)
    EXPECT_EQ(mat.GetParameter(i), i + 1.5);
}

TYPED_TEST_P(MaterialTest, CopyInstantiation) {
  int n = this->value_.ParameterCount();
  std::vector<double> params(n);
  for (int i = 0; i < n; ++i)
    params[i] = i + 1.5;
  TypeParam mat{params};

  for (int i = 0; i < n; ++i)
    EXPECT_EQ(mat.GetParameter(i), i + 1.5);
}

TYPED_TEST_P(MaterialTest, DefaultEnergy) {
  int n = this->value_.ParameterCount();
  auto paramVector = getParameterVector(n);

  TypeParam mat(move(paramVector));
  EXPECT_EQ(mat.IsotropicEnergy(One) + mat.AnisotropicEnergy(One, One), 0.0);
}

TYPED_TEST_P(MaterialTest, DefaultFirstDerivative) {
  int n = this->value_.ParameterCount();
  auto paramVector = getParameterVector(n);

  TypeParam mat(move(paramVector));
  EXPECT_EQ(mat.IsotropicDerivative(One, Zero) + mat.AnisotropicDerivative(One, One, One),
            0.0);
}

TYPED_TEST_P(MaterialTest, DefaultSecondDerivative) {
  int n = this->value_.ParameterCount();
  auto paramVector = getParameterVector(n);

  TypeParam mat(move(paramVector));
  EXPECT_EQ(mat.IsotropicSecondDerivative(One, One, Zero) +
            mat.AnisotropicSecondDerivative(One, One, One, Zero),
            0.0);
}


TYPED_TEST_P(MaterialTest, DerivativeConsistency) {
  int n = this->value_.ParameterCount();
  auto paramVector = getParameterVector(n);
  Tensor F = randomTensor(0.5) + One;
  Tensor H = randomTensor(1.0);

  TypeParam mat(move(paramVector));

  double isoder = mat.IsotropicDerivative(F, H) + mat.AnisotropicDerivative(F, One, H);
  double numder = numMatDerivative(mat, F, One, H);

  EXPECT_NEAR(isoder, numder,
              1e-6 * std::max(1.0, abs(std::max(isoder, numder))));
}


TYPED_TEST_P(MaterialTest, SecondDerivativeConsistency) {
  int n = this->value_.ParameterCount();
  auto paramVector = getParameterVector(n);
  Tensor F = randomTensor(0.5) + One;
  Tensor H = randomTensor(1.0);
  Tensor G = randomTensor(1.0);

  TypeParam mat(move(paramVector));

  double isoder =
      mat.IsotropicSecondDerivative(F, H, G) + mat.AnisotropicSecondDerivative(F, One, H, G);
  double numder = numMatSecondDerivative(mat, F, One, H, G);

  EXPECT_NEAR(isoder, numder,
              1e-6 * std::max(1.0, abs(std::max(isoder, numder))));
}

REGISTER_TYPED_TEST_SUITE_P(MaterialTest, DefaultInstantiation, MoveInstantiation,
                            CopyInstantiation, DefaultEnergy, DefaultFirstDerivative,
                            DefaultSecondDerivative,
                            DerivativeConsistency, SecondDerivativeConsistency
);

#endif //TESTDEFAULTMATERIAL_H
