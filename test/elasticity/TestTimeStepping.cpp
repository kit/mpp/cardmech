#include "TestEnvironmentCardMech.hpp"
#include "MeshesCreator.hpp"
#include "VectorFieldElement.hpp"
#include "DGVectorFieldElement.hpp"
#include "MixedEGVectorFieldElement.hpp"

#include "LagrangeDiscretization.hpp"
#include "DGDiscretization.hpp"
#include "MixedEGDiscretization.hpp"

constexpr int N = 20;

template<typename ElementType>
class TimeSteppingTest : public Test {
protected:
  std::unique_ptr<Meshes> meshes{};
  std::unique_ptr<IDiscretization> disc{};

  std::unique_ptr<Vector> u{};
  std::unique_ptr<Vector> uNew{};
  std::unique_ptr<Vector> v{};
  std::unique_ptr<Vector> a{};

public:
  TimeSteppingTest() {
    meshes = MeshesCreator("Hexahedron")
        .WithLevel(1)
        .WithPLevel(1)
        .WithCellData(DataContainer(1))
        .WithVertexData(DataContainer(1))
        .CreateUnique();

    if constexpr (std::is_same_v<ElementType, VectorFieldElement>) {
      disc = std::make_unique<LagrangeDiscretization>(*meshes, 1, 3);
    }
    if constexpr (std::is_same_v<ElementType, DGVectorFieldElement>) {
      disc = std::make_unique<DGDiscretization>(*meshes, 1, 3);
    }
    if constexpr (std::is_same_v<ElementType, MixedEGVectorFieldElement>) {
      disc = std::make_unique<MixedEGDiscretization>(*meshes, 1, 3);
    }

    u = std::make_unique<Vector>(0.0, *disc);
    uNew = std::make_unique<Vector>(0.0, *disc);
    v = std::make_unique<Vector>(0.0, *disc);
    a = std::make_unique<Vector>(0.0, *disc);
  }
};


TYPED_TEST_SUITE_P(TimeSteppingTest);

TYPED_TEST_P(TimeSteppingTest, NM_Zero) {
  NewmarkScheme scheme(0.25, 0.5);;

  scheme.Init(1.0, *this->u);

  for (cell c = this->u->cells(); c != this->u->cells_end(); ++c) {
    TypeParam elem(*this->uNew, *c);

    for (int q = 0; q < elem.nQ(); ++q) {
      auto vel = scheme.Velocity<TypeParam>(*this->uNew, elem, q);
      auto acc = scheme.Acceleration<TypeParam>(*this->uNew, elem, q);

      EXPECT_EQ(vel, zero);
      EXPECT_EQ(acc, zero);
    }
  }
}


TYPED_TEST_P(TimeSteppingTest, NM_Static) {
  NewmarkScheme scheme(0.25, 0.5);

  *this->u = 1.0;
  *this->uNew = 1.0;

  scheme.Init(1.0, *this->u);
  scheme._setDisplacement(1.0);

  for (int n = 0; n < N; ++n) {
    for (cell c = this->u->cells(); c != this->u->cells_end(); ++c) {
      TypeParam elem(*this->uNew, *c);

      for (int q = 0; q < elem.nQ(); ++q) {
        auto vel = scheme.Velocity<TypeParam>(*this->uNew, elem, q);
        auto acc = scheme.Acceleration<TypeParam>(*this->uNew, elem, q);

        EXPECT_EQ(vel, zero);
        EXPECT_EQ(acc, zero);
      }
    }
    scheme.Update(1.0, *this->uNew);
  }
}


TYPED_TEST_P(TimeSteppingTest, NM_ConstantVelocity) {
  NewmarkScheme scheme(0.25, 0.5);

  *this->u = 0.0;

  scheme.Init(1.0, *this->u);
  scheme._setVelocity(1.0);

  for (int n = 1; n <= N; ++n) {
    *this->uNew = (double) n;
    for (cell c = this->u->cells(); c != this->u->cells_end(); ++c) {
      TypeParam elem(*this->uNew, *c);

      auto vel = zero;
      auto acc = zero;
      for (int q = 0; q < elem.nQ(); ++q) {
        vel += scheme.Velocity<TypeParam>(*this->uNew, elem, q);
        acc += scheme.Acceleration<TypeParam>(*this->uNew, elem, q);

      }
      vel /= elem.nQ();
      acc /= elem.nQ();

      EXPECT_EQ(vel, VectorField(1.0, 1.0, 1.0));
      EXPECT_EQ(acc, VectorField(0.0, 0.0, 0.0));
    }
    scheme.Update(1.0, *this->uNew);
  }
}

TYPED_TEST_P(TimeSteppingTest, NM_ConstantAcceleration) {
  NewmarkScheme scheme(0.25, 0.5);

  *this->u = 0.0;
  scheme.Init(1.0, *this->u);
  scheme._setAcceleration(1.0);

  int sumN = 0;
  for (int n = 1; n <= N; ++n) {
    *this->uNew = 0.5 * n * n;
    for (cell c = this->u->cells(); c != this->u->cells_end(); ++c) {
      TypeParam elem(*this->uNew, *c);

      auto vel = zero;
      auto acc = zero;
      for (int q = 0; q < elem.nQ(); ++q) {
        vel += scheme.Velocity<TypeParam>(*this->uNew, elem, q);
        acc += scheme.Acceleration<TypeParam>(*this->uNew, elem, q);

      }
      vel /= elem.nQ();
      acc /= elem.nQ();

      EXPECT_EQ(vel, VectorField(n, n, n));
      EXPECT_EQ(acc, VectorField(1.0, 1.0, 1.0));
    }
    scheme.Update(1.0, *this->uNew);
  }
}

TYPED_TEST_P(TimeSteppingTest, FD_Zero) {
  FiniteDifferenceScheme scheme;

  scheme.Init(1.0, *this->u);

  for (cell c = this->u->cells(); c != this->u->cells_end(); ++c) {
    TypeParam elem(*this->uNew, *c);

    for (int q = 0; q < elem.nQ(); ++q) {
      auto vel = scheme.Velocity<TypeParam>(*this->uNew, elem, q);
      auto acc = scheme.Acceleration<TypeParam>(*this->uNew, elem, q);

      EXPECT_EQ(vel, zero);
      EXPECT_EQ(acc, zero);
    }
  }
}


TYPED_TEST_P(TimeSteppingTest, FD_Static) {
  FiniteDifferenceScheme scheme;

  *this->u = 1.0;
  *this->uNew = 1.0;

  scheme.Init(1.0, *this->u);
  scheme._setDisplacement(1.0);

  for (int n = 0; n < N; ++n) {
    for (cell c = this->u->cells(); c != this->u->cells_end(); ++c) {
      TypeParam elem(*this->uNew, *c);

      for (int q = 0; q < elem.nQ(); ++q) {
        auto vel = scheme.Velocity<TypeParam>(*this->uNew, elem, q);
        auto acc = scheme.Acceleration<TypeParam>(*this->uNew, elem, q);

        EXPECT_EQ(vel, zero);
        EXPECT_EQ(acc, zero);
      }
    }
    scheme.Update(1.0, *this->uNew);
  }
}


TYPED_TEST_P(TimeSteppingTest, FD_ConstantVelocity) {
  FiniteDifferenceScheme scheme;

  *this->u = 0.0;

  scheme.Init(1.0, *this->u);
  scheme._setVelocity(1.0);

  for (int n = 1; n <= N; ++n) {
    *this->uNew = (double) n;
    for (cell c = this->u->cells(); c != this->u->cells_end(); ++c) {
      TypeParam elem(*this->uNew, *c);

      auto vel = zero;
      auto acc = zero;
      for (int q = 0; q < elem.nQ(); ++q) {
        vel += scheme.Velocity<TypeParam>(*this->uNew, elem, q);
        acc += scheme.Acceleration<TypeParam>(*this->uNew, elem, q);

      }
      vel /= elem.nQ();
      acc /= elem.nQ();

      EXPECT_EQ(vel, VectorField(1.0, 1.0, 1.0));
      EXPECT_EQ(acc, VectorField(0.0, 0.0, 0.0));
    }
    scheme.Update(1.0, *this->uNew);
  }
}

TYPED_TEST_P(TimeSteppingTest, FD_ConstantAcceleration) {
  FiniteDifferenceScheme scheme;

  *this->u = 0.0;
  scheme.Init(1.0, *this->u);
  scheme._setAcceleration(1.0);

  int sumN = 0;
  for (int n = 1; n <= N; ++n) {
    *this->uNew = 0.5 * n * n;
    for (cell c = this->u->cells(); c != this->u->cells_end(); ++c) {
      TypeParam elem(*this->uNew, *c);

      auto vel = zero;
      auto acc = zero;
      for (int q = 0; q < elem.nQ(); ++q) {
        vel += scheme.Velocity<TypeParam>(*this->uNew, elem, q);
        acc += scheme.Acceleration<TypeParam>(*this->uNew, elem, q);

      }
      vel /= elem.nQ();
      acc /= elem.nQ();

      if(n>1) {
        EXPECT_EQ(vel, VectorField(n-0.5, n-0.5, n-0.5));
        EXPECT_EQ(acc, VectorField(1.0, 1.0, 1.0));
      }
    }
    scheme.Update(1.0, *this->uNew);
  }
}

REGISTER_TYPED_TEST_SUITE_P(TimeSteppingTest,
                            NM_Zero, NM_Static, NM_ConstantVelocity, NM_ConstantAcceleration,
                            FD_Zero, FD_Static, FD_ConstantVelocity, FD_ConstantAcceleration);

INSTANTIATE_TYPED_TEST_SUITE_P(LagrangeTimeStepping, TimeSteppingTest, VectorFieldElement);
INSTANTIATE_TYPED_TEST_SUITE_P(DGTimeStepping, TimeSteppingTest, DGVectorFieldElement);
INSTANTIATE_TYPED_TEST_SUITE_P(EGTimeStepping, TimeSteppingTest, MixedEGVectorFieldElement);


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).
      WithPPM().
      WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}