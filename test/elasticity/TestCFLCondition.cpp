#include "MainElasticity.hpp"

#include "TestEnvironmentCardMech.hpp"
#include "TestConfigurations.hpp"


struct CFLParam {
  std::vector<int> steps;
  std::string bndType;

  int cflSteps() const {
    return steps.size() - 1;
  }

  int maxSteps() const {
    return steps.back();
  }

  double maxStepSize() const { return stepSize(steps.size() - 1, 1.0); }

  double stepSize(int s, double endTime) const {
    return endTime / steps[s];
  }

  std::string problemName() const {
    return "Dynamic" + bndType + "TestPol";
  }

  double tolerance() const {
    return bndType == "Dirichlet" ? 1e-6 : 1e-4;
  }

  friend void PrintTo(const CFLParam &param, std::ostream *os) {
    *os << "CFL" << param.bndType << "TestWith" << std::to_string(param.steps.back()) << "Steps";
  }
};


class CFLTest : public TestWithParam<CFLParam> {
protected:
  MainElasticity *cmMain;

  CFLTest() {
  }

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;
    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Newmark";
    testConfig["MechDiscretization"] = "Lagrange";
    testConfig["PressureSteps"] = "1";
    testConfig["WithPrestress"] = "0";

    testConfig["MechProblem"] = GetParam().problemName();

    testConfig["Incompressible"] = "false";
    testConfig["QuasiCompressiblePenalty"] = "None";

    testConfig["ProblemDimension"] = "3";
    testConfig["ProblemGeometry"] = "Tet";

    testConfig["NewmarkBeta"] = "0.25";
    testConfig["NewmarkGamma"] = "0.5";

    testConfig["MechLevel"] = "1";
    testConfig["MechPolynomialDegree"] = "1";
    testConfig["StartTime"] = "0.0";
    testConfig["EndTime"] = "1.0";
    testConfig["DeltaTime"] = std::to_string(GetParam().maxStepSize());

    testConfig["Density"] = "1.0";
    Config::Initialize(testConfig);
    Config::PrintInfo();

    cmMain = new MainElasticity();
    cmMain->Initialize();

  }

  void TearDown() override {
    Config::Close();
  }

  ~CFLTest() override {
    delete cmMain;
  }
};


TEST_P(CFLTest, ConsistenceTest) {
  Vector reference(cmMain->Run());

  std::vector<double> errors(GetParam().cflSteps());
  for (int i = 0; i < GetParam().cflSteps(); ++i) {
    cmMain->ResetTime(0.0, 1.0, GetParam().steps[i]);
    Vector solution(cmMain->Run());
    solution -= reference;

    errors[i] = solution.norm() / PPM->Sum(solution.size());
    mout << "Error (" << GetParam().steps[i] << " Steps): " << errors[i] << endl;
  }

  // Expect error to become smaller
  for (int i = 0; i < errors.size() - 1; ++i) {
    EXPECT_LE(errors[i + 1], errors[i]);
  }

  // Expect last error to be of certain order
  EXPECT_NEAR(errors.back(), 0.0, GetParam().tolerance());


}


INSTANTIATE_TEST_SUITE_P(TestDirichletCFL, CFLTest, Values(
    CFLParam({{1, 10, 20, 50, 100, 200/*, 500, 1000*/}, "Dirichlet"})
), testing::PrintToStringParamName());


INSTANTIATE_TEST_SUITE_P(TestTractionCFL, CFLTest, Values(
    CFLParam({{1, 10, 20, 50, 100, 200/*, 500, 1000*/}, "Traction"})
), testing::PrintToStringParamName());

INSTANTIATE_TEST_SUITE_P(TestNeumannCFL, CFLTest, Values(
    CFLParam({{1, 10, 20, 50, 100, 200/*, 500, 1000*/}, "Neumann"})
), testing::PrintToStringParamName());


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).
      WithPPM().
      WithScreenLogging().
      WithoutDefaultConfig();
  return mppTest.RUN_ALL_MPP_TESTS();
}