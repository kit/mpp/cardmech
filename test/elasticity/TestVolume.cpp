#include "MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"

#include "../TestConfigurations.hpp"

struct TestVolumeParameter {
  std::string problemGeo;
  int problemDimension;
};

class VolumeTest : public TestWithParam<TestVolumeParameter> {
protected:
  MainElasticity *cmMain;

  VolumeTest() {}

  void SetUp() override {
  }

  void InitMain(const std::string &problemName, const std::string &discretization) {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Static";
    testConfig["MechDiscretization"] = discretization;
    testConfig["PressureSteps"] = "1";
    testConfig["Distribution"] = "RCBz";
    testConfig["WithPrestress"] = "false";
    testConfig["ConfigVerbose"] = "3";
    testConfig["MechVerbose"] = "1";

    if ( discretization == "Passive") {
      testConfig["Overlap"] = "NoOverlap";
      testConfig["Overlap_Distribution"] = "0";
    }
    else if ( discretization == "DG"){
      testConfig["DGSign"] = "-1";
      testConfig["DGPenalty"] = "2.0";
      testConfig["Overlap"] = "dG1";
      testConfig["Overlap_Distribution"] = "1";
    }
    else if ( discretization == "EG"){
      testConfig["DGSign"] = "-1";
      testConfig["DGPenalty"] = "2.0";
      testConfig["Overlap"] = "EG";
      testConfig["Overlap_Distribution"] = "1";
    }

    testConfig["MechProblem"] = problemName;
    testConfig["ProblemDimension"] = std::to_string(GetParam().problemDimension);
    testConfig["ProblemGeometry"] = GetParam().problemGeo;
    testConfig["MechLevel"] = "0";
    testConfig["MechPolynomialDegree"] = "1";

    Config::Initialize(testConfig);

    cmMain = new MainElasticity();
    cmMain->Initialize();

  }

  void TearDown() override {
    Config::Close();
  }

  ~VolumeTest() override {
    delete cmMain;
  }
};


TEST_P(VolumeTest, DirichletX) {
  InitMain("VolumeBlockX", "Passive");

  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto volumes = cmMain->GetVolume(cmMain->GetDisplacement());

  for (auto v: volumes)
    ASSERT_NEAR(v, 2.0 * CUBICMMtoML, 1e-4);
}

TEST_P(VolumeTest, DirichletY) {
  InitMain("VolumeBlockY", "Passive");

  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto volumes = cmMain->GetVolume(cmMain->GetDisplacement());

  for (auto v: volumes)
    ASSERT_NEAR(v, 2.0 * CUBICMMtoML, 1e-4);
}

TEST_P(VolumeTest, DirichletZ) {
  InitMain("VolumeBlockZ", "Passive");

  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto volumes = cmMain->GetVolume(cmMain->GetDisplacement());

  for (auto v: volumes)
    ASSERT_NEAR(v, ((double) GetParam().problemDimension - 1.0) * CUBICMMtoML, 1e-4);
}

TEST_P(VolumeTest, DirichletXYZ) {
  InitMain("VolumeBlockXYZ", "Passive");

  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto volumes = cmMain->GetVolume(cmMain->GetDisplacement());

  for (auto v: volumes)
    ASSERT_NEAR(v, 4.0 * ((double) GetParam().problemDimension - 1.0) * CUBICMMtoML, 1e-4);
}

TEST_P(VolumeTest, DeformedX) {
    InitMain("VolumeBlockX", "Passive");

    auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
    cmMain->SetDisplacement(true, cmMain->GetDisplacement());
    auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

    ASSERT_NEAR(v, 2.0 * vinit, 1e-4);
}

TEST_P(VolumeTest, DGDeformedX) {
  InitMain("VolumeBlockX", "DG");

  auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

  ASSERT_NEAR(v, 2.0 * vinit, 1e-4);
}

TEST_P(VolumeTest, EGDeformedX) {
  InitMain("VolumeBlockX", "EG");

  auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

  ASSERT_NEAR(v, 2.0 * vinit, 1e-4);
}

TEST_P(VolumeTest, DeformedY) {
    InitMain("VolumeBlockY", "Passive");

    auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
    cmMain->SetDisplacement(true, cmMain->GetDisplacement());
    auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

    ASSERT_NEAR(v, 2.0 * vinit, 1e-4);
}

TEST_P(VolumeTest, DGDeformedY) {
  InitMain("VolumeBlockY", "DG");

  auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

  ASSERT_NEAR(v, 2.0 * vinit, 1e-4);
}

TEST_P(VolumeTest, EGDeformedY) {
  InitMain("VolumeBlockY", "EG");

  auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

  ASSERT_NEAR(v, 2.0 * vinit, 1e-4);
}

TEST_P(VolumeTest, DeformedZ) {
    InitMain("VolumeBlockZ", "Passive");

    auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
    cmMain->SetDisplacement(true, cmMain->GetDisplacement());
    auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

    ASSERT_NEAR(v, ((double) GetParam().problemDimension - 1.0)* vinit, 1e-4);
}

TEST_P(VolumeTest, DGDeformedZ) {
  InitMain("VolumeBlockZ", "DG");

  auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

  ASSERT_NEAR(v, ((double) GetParam().problemDimension - 1.0)* vinit, 1e-4);
}

TEST_P(VolumeTest, EGDeformedZ) {
  InitMain("VolumeBlockZ", "EG");

  auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

  ASSERT_NEAR(v, ((double) GetParam().problemDimension - 1.0)* vinit, 1e-4);
}

TEST_P(VolumeTest, DeformedXYZ) {
    InitMain("VolumeBlockXYZ", "Passive");

    auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
    cmMain->SetDisplacement(true, cmMain->GetDisplacement());
    auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

    ASSERT_NEAR(v, 4.0 * ((double) GetParam().problemDimension - 1.0)* vinit, 1e-4);
}

TEST_P(VolumeTest, DGDeformedXYZ) {
  InitMain("VolumeBlockXYZ", "DG");

  auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

  ASSERT_NEAR(v, 4.0 * ((double) GetParam().problemDimension - 1.0)* vinit, 1e-4);
}

TEST_P(VolumeTest, EGDeformedXYZ) {
  InitMain("VolumeBlockXYZ", "EG");

  auto vinit = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;
  cmMain->SetDisplacement(true, cmMain->GetDisplacement());
  auto v = cmMain->GetGeometryVolume(cmMain->GetDisplacement()).second;

  ASSERT_NEAR(v, 4.0 * ((double) GetParam().problemDimension - 1.0)* vinit, 1e-4);
}

INSTANTIATE_TEST_SUITE_P(TestDirichletVolumes, VolumeTest, Values(
    TestVolumeParameter({"Quad", 2}),
    TestVolumeParameter({"Tet", 2}),
    TestVolumeParameter({"Quad", 3}),
    TestVolumeParameter({"Tet", 3})
));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).
      WithPPM().
      WithScreenLogging().
      WithoutDefaultConfig();
  return mppTest.RUN_ALL_MPP_TESTS();
}