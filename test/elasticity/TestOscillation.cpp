#include "TestEnvironmentCardMech.hpp"
#include "MainElasticity.hpp"
#include "TestConfigurations.hpp"

class OscillationTest : public TestWithParam<std::pair<std::string, std::string>> {
protected:
  MainElasticity *cmMain;

  OscillationTest() {}

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["MechRuntype"] = "Default";
    testConfig["MechDynamics"] = GetParam().first;
    testConfig["MechDiscretization"] = GetParam().second;
    testConfig["PressureSteps"] = "1";
    testConfig["Distribution"] = "RCBz";
    testConfig["WithPrestress"] = "false";

    testConfig["MechProblem"] = "Oscillation";
    testConfig["ProblemDimension"] = "3";
    testConfig["ProblemGeometry"] = "Tet";
    testConfig["MechLevel"] = "1";
    testConfig["MechPolynomialDegree"] = "1";

    testConfig["NewmarkBeta"] = "0.25";
    testConfig["NewmarkGamma"] = "0.5";

    testConfig["StartTime"] = "0.0";
    testConfig["EndTime"] = "0.2";
    testConfig["DeltaTime"] = "0.01";

    testConfig["ActiveMaterial"] = "Linear";
    testConfig["QuasiCompressiblePenalty"] = "None";

    testConfig["LinearMat_Lambda"] = "1";
    testConfig["LinearMat_Mu"] = "1";

    testConfig["Density"] = "0.01";


    testConfig["ConstantPressure"] = "0";
    testConfig["LVPressure"] = "0";
    testConfig["RVPressure"] = "0";
    testConfig["LAPressure"] = "0";
    testConfig["RAPressure"] = "0";

    testConfig["DGSign"] = "1.0";
    testConfig["DGPenalty"] = "20.0";


    Config::Initialize(testConfig);

    cmMain = new MainElasticity();
    cmMain->Initialize();
  }

  void TearDown() override {
    Config::Close();
  }

  ~OscillationTest() override {
    delete cmMain;
  }
};

TEST_P(OscillationTest, Run) {
  Vector &solution = cmMain->Run();

  // ToDo: Should actually oscillate
  EXPECT_NEAR(cmMain->EvaluateQuantity(solution, "L2Norm"), 0.0, 1e-8);
}

INSTANTIATE_TEST_SUITE_P(TestOscillationWithDiscs, OscillationTest, ValuesIn(
    std::vector<std::pair<std::string, std::string>>{
        {"FiniteDifference", "Conforming"},
        {"Newmark",          "Conforming"}
        // {"FiniteDifference", "DG"},
        // {"Newmark",          "DG"},
        // {"FiniteDifference", "EG"},
        // {"Newmark",          "EG"}
    }));


int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).
      WithPPM().
      WithScreenLogging().
      WithoutDefaultConfig();
  return mppTest.RUN_ALL_MPP_TESTS();
}