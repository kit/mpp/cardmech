#include "MainElasticity.hpp"
#include "TestEnvironmentCardMech.hpp"
#include "TestConfigurations.hpp"

constexpr double BEAM_TEST_TOLERANCE = 1e-1;

struct TestBeamParameter {
  std::string problemName;
  std::string problemGeo;
  int problemDimension;
  int problemDegree;
  int meshLevel;
};

class ElasticityBeamTest : public TestWithParam<TestBeamParameter> {
protected:
  MainElasticity *cmMain;

  ElasticityBeamTest() {  }

  void SetUp() override {
    std::map<std::string, std::string> testConfig = CARDIAC_TEST_CONFIG;

    testConfig["Model"] = "ActiveStrainElasticity";
    testConfig["MechDynamics"] = "Static";
    testConfig["MechDiscretization"] = "Passive";
    testConfig["PressureSteps"] = "1";
    testConfig["NewtonEpsilon"] = "1e-6";

    testConfig["MechProblem"] = GetParam().problemName;
    testConfig["ProblemDimension"] = std::to_string(GetParam().problemDimension);
    testConfig["ProblemGeometry"] = GetParam().problemGeo;
    testConfig["MechLevel"] = std::to_string(GetParam().meshLevel);
    testConfig["MechPolynomialDegree"] = std::to_string(GetParam().problemDegree);
    Config::Initialize(testConfig);

    cmMain = new MainElasticity();
    cmMain->Initialize();
  }

  void TearDown() override {
    Config::Close();
  }

  ~ElasticityBeamTest() override {
    delete cmMain;
  }
};


TEST_P(ElasticityBeamTest, CheckReferenceValues) {
  Vector &solution = cmMain->Run();
  auto evalResults = cmMain->Evaluate(solution);

  for(auto result : evalResults)
    ASSERT_NEAR(result, 0.0, BEAM_TEST_TOLERANCE);

  //ASSERT_NEAR(cmMain->PressureError(solution), 0.0, BEAM_TEST_TOLERANCE);

}

INSTANTIATE_TEST_SUITE_P(TestUpBeam, ElasticityBeamTest, Values(
    TestBeamParameter({"ElasticityUpBeam", "Quad", 2, 1, 3}),
    TestBeamParameter({"ElasticityUpBeam", "Tet", 2, 1, 3}),
    TestBeamParameter({"ElasticityUpBeam", "Quad", 3, 1, 3}),
    TestBeamParameter({"ElasticityUpBeam", "Tet", 3, 1, 3}),
    TestBeamParameter({"ElasticityUpBeam", "Quad", 2, 2, 1}),
    TestBeamParameter({"ElasticityUpBeam", "Tet", 2, 2, 1}),
    TestBeamParameter({"ElasticityUpBeam", "Quad", 3, 2, 1}),
    TestBeamParameter({"ElasticityUpBeam", "Tet", 3, 2, 1})
    ));

INSTANTIATE_TEST_SUITE_P(TestDownBeam, ElasticityBeamTest, Values(
    TestBeamParameter({"ElasticityDownBeam", "Quad", 2, 1, 3}),
    TestBeamParameter({"ElasticityDownBeam", "Tet", 2, 1, 3}),
    TestBeamParameter({"ElasticityDownBeam", "Quad", 3, 1, 3}),
    TestBeamParameter({"ElasticityDownBeam", "Tet", 3, 1, 3}),
    TestBeamParameter({"ElasticityDownBeam", "Quad", 2, 2, 1}),
    TestBeamParameter({"ElasticityDownBeam", "Tet", 2, 2, 1}),
    TestBeamParameter({"ElasticityDownBeam", "Quad", 3, 2, 1}),
    TestBeamParameter({"ElasticityDownBeam", "Tet", 3, 2, 1})
));

INSTANTIATE_TEST_SUITE_P(TestPushBeam, ElasticityBeamTest, Values(
    TestBeamParameter({"ElasticityPushBeam", "Quad", 2, 1, 3}),
    TestBeamParameter({"ElasticityPushBeam", "Tet", 2, 1, 3}),
    TestBeamParameter({"ElasticityPushBeam", "Quad", 3, 1, 3}),
    TestBeamParameter({"ElasticityPushBeam", "Tet", 3, 1, 3}),
    TestBeamParameter({"ElasticityPushBeam", "Quad", 2, 2, 1}),
    TestBeamParameter({"ElasticityPushBeam", "Tet", 2, 2, 1}),
    TestBeamParameter({"ElasticityPushBeam", "Quad", 3, 2, 1}),
    TestBeamParameter({"ElasticityPushBeam", "Tet", 3, 2, 1})/*,
    TestBeamParameter({"ElasticityPushBeam", "Quad", 2, 3, 1}),
    TestBeamParameter({"ElasticityPushBeam", "Tet", 2, 3, 1}),
    TestBeamParameter({"ElasticityPushBeam", "Quad", 3, 3, 1}),
    TestBeamParameter({"ElasticityPushBeam", "Tet", 3, 3, 1}),
    TestBeamParameter({"ElasticityPushBeam", "Quad", 2, 4, 0}),
    TestBeamParameter({"ElasticityPushBeam", "Tet", 2, 4, 0}),
    TestBeamParameter({"ElasticityPushBeam", "Quad", 3, 4, 0}),
    TestBeamParameter({"ElasticityPushBeam", "Tet", 3, 4, 0})*/
));

INSTANTIATE_TEST_SUITE_P(TestPullBeam, ElasticityBeamTest, Values(
    TestBeamParameter({"ElasticityPullBeam", "Quad", 2, 1, 3}),
    TestBeamParameter({"ElasticityPullBeam", "Tet", 2, 1, 3}),
    TestBeamParameter({"ElasticityPullBeam", "Quad", 3, 1, 3}),
    TestBeamParameter({"ElasticityPullBeam", "Tet", 3, 1, 3}),
    TestBeamParameter({"ElasticityPullBeam", "Quad", 2, 2, 1}),
    TestBeamParameter({"ElasticityPullBeam", "Tet", 2, 2, 1}),
    TestBeamParameter({"ElasticityPullBeam", "Quad", 3, 2, 1}),
    TestBeamParameter({"ElasticityPullBeam", "Tet", 3, 2, 1})
));

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).WithPPM().WithoutDefaultConfig();
  return mppTest.RUN_ALL_MPP_TESTS();
}
