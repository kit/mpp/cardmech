#include <random>
#include "TestEnvironmentCardMech.hpp"
#include "Tensor.hpp"
#include "materials/materials.hpp"

void EXPECT_VECTORFIELD_EQ(const VectorField &v, const VectorField &w) {
  for (int i = 0; i < v.Dim(); ++i)
    EXPECT_NEAR(v[i], w[i], tolerance);
}

void EXPECT_TENSOR_EQ(const Tensor &A, const Tensor &B) {
  for (int i = 0; i < A.Dim(); ++i)
    EXPECT_VECTORFIELD_EQ(A[i], B[i]);
}

Tensor RandomOrth(){
  std::random_device rd;
  std::default_random_engine eng(rd());
  std::uniform_real_distribution<double> distr(1.0, 20.0);

  VectorField u1{distr(eng), distr(eng), distr(eng)};
  VectorField e1{0,1,0};
  VectorField e2{0,0,1};

  u1 = u1/norm(u1);
  VectorField u2 = e1 - (e1*u1) * u1;
  u2 = u2/norm(u2);
  VectorField u3 = e2 - (e2*u1) * u1 - (e2*u2) * u2;
  u3 = u3/norm(u3);

  Tensor Q(u1,u2,u3);
  Q.transpose();
  return Q;
}


TEST(ActiveStrainTest, EquivalentInverseUnit) {

  Tensor Q = One;
  VectorField gamma{-0.2,-0.8,-0.8};
  Tensor Fa = mat::active_deformation(Q, gamma);

  EXPECT_TENSOR_EQ(Invert(Fa), mat::inverse_active_deformation(Q, gamma));
}

TEST(ActiveStrainTest, EquivalentInverseRandom) {

  Tensor Q = RandomOrth();
  VectorField gamma{-0.2,-0.8,-0.8};
  Tensor Fa = mat::active_deformation(Q, gamma);

  EXPECT_TENSOR_EQ(Invert(Fa), mat::inverse_active_deformation(Q, gamma));
}

TEST(ActiveStrainTest, EquivalentInverseOnlyf) {

  Tensor Q = RandomOrth();
  VectorField gamma{-0.2,0.0,0.0};
  Tensor Fa = mat::active_deformation(Q, gamma);

  EXPECT_TENSOR_EQ(Invert(Fa), mat::inverse_active_deformation(Q, gamma));
}

int main(int argc, char **argv) {
  MppTest mppTest = MppTestBuilder(argc, argv).
      WithScreenLogging();
  return mppTest.RUN_ALL_MPP_TESTS();
}
