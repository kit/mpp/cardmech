#include <solvers/DynamicMechanicsSolver.hpp>
#include <solvers/LinearImplicitSolver.hpp>
#include <cellmodels/solvers/MTensionSolver.hpp>
#include <solvers/ElphySolver.hpp>
#include <solvers/SplittingSolver.hpp>
#include "transfers/MultiPartTransfer.hpp"
#include "solver/Newton.hpp"
#include "SegregatedSolver.hpp"

#define v_potential(v) v[0]
#define v_stretch(v) v[1]
#define v_iota(v) v[2]

#define u_displacement(u) u[0]
#define u_stretch(u) u[1]
#define u_iota(u) u[2]
#define u_pot(u) u[3]


std::vector<std::string> metrics = {
    "Time",
    "Step",
    "Geometry-Volume",
    "LV-Volume",
    "RV-Volume",
    "LA-Volume",
    "RA-Volume",
    "FrobeniusNorm",
    "L2Norm(u)",
    "L2AvgNorm(u)",
    "H1Norm(u)",
    "EnergyNorm(u)",
    "FrobeniusNorm(v)",
    "L2Norm(v)",
    "H1Norm(v)",
    "FrobeniusNorm(gamma_v)",
    "L2Norm(gamma_v)",
    "H1Norm(gamma_v)",
    "FrobeniusNorm(gamma_u)",
    "L2Norm(gamma_u)",
    "H1Norm(gamma_u)",
    "NormFa(u)",
    "NormPK(u)"
};

void
SegregatedSolver::EvaluateIteration(const IElphyAssemble &elphyAssemble, const IElasticity &mechAssemble,
                                    Vectors elphyValues, Vectors mechValues) {

  auto volumes = mechAssemble.ChamberVolume(u_displacement(mechValues));

  evaluationMetrics[metrics[0]].emplace_back(mechAssemble.Time());
  evaluationMetrics[metrics[1]].emplace_back(ongoingMechStep);
  evaluationMetrics[metrics[2]].emplace_back(
      mechAssemble.DeformedVolume(u_displacement(mechValues)));
  evaluationMetrics[metrics[3]].emplace_back(volumes[0]);
  evaluationMetrics[metrics[4]].emplace_back(volumes[1]);
  evaluationMetrics[metrics[5]].emplace_back(volumes[2]);
  evaluationMetrics[metrics[6]].emplace_back(volumes[3]);
  evaluationMetrics[metrics[7]].emplace_back(u_displacement(mechValues).ConsistentNorm());
  evaluationMetrics[metrics[8]].emplace_back(mechAssemble.L2Norm(u_displacement(mechValues)));
  evaluationMetrics[metrics[9]].emplace_back(mechAssemble.L2AvgNorm(u_displacement(mechValues)));
  evaluationMetrics[metrics[10]].emplace_back(mechAssemble.H1Norm(u_displacement(mechValues)));
  evaluationMetrics[metrics[11]].emplace_back(mechAssemble.EnergyError(u_displacement(mechValues)));
  evaluationMetrics[metrics[12]].emplace_back(v_potential(elphyValues).ConsistentNorm());
  evaluationMetrics[metrics[13]].emplace_back(elphyAssemble.L2Norm(v_potential(elphyValues)));
  evaluationMetrics[metrics[14]].emplace_back(elphyAssemble.H1Norm(v_potential(elphyValues)));
  evaluationMetrics[metrics[15]].emplace_back(v_stretch(elphyValues).ConsistentNorm());
  evaluationMetrics[metrics[16]].emplace_back(elphyAssemble.L2Norm(v_stretch(elphyValues)));
  evaluationMetrics[metrics[17]].emplace_back(elphyAssemble.H1Norm(v_stretch(elphyValues)));
  evaluationMetrics[metrics[18]].emplace_back(u_stretch(mechValues).ConsistentNorm());
  evaluationMetrics[metrics[19]].emplace_back(elphyAssemble.L2Norm(u_stretch(mechValues)));
  evaluationMetrics[metrics[20]].emplace_back(elphyAssemble.H1Norm(u_stretch(mechValues)));
  evaluationMetrics[metrics[21]].emplace_back(mechAssemble.NormFa(u_displacement(mechValues)));
  evaluationMetrics[metrics[22]].emplace_back(mechAssemble.NormPK(u_displacement(mechValues)));

  std::unordered_map<std::string, double> solverInformation;
  mechSolver->FillInformation(mechAssemble, solverInformation);
  for (auto& [metric, value] : solverInformation){
    evaluationMetrics[metric].emplace_back(value);
  }

  // Get some metrices from src/elasticity/problem/ElasticityProblem.hpp (for the mechanics part)
  auto problemMetrics = mechAssemble.GetElasticityProblem().EvaluationMetrics();
  auto problemEntries = mechAssemble.GetElasticityProblem().EvaluationResults(
      u_displacement(mechValues));
  for (int i = 0; i < problemMetrics.size(); ++i) {
    evaluationMetrics[problemMetrics[i]].emplace_back(problemEntries[i]);
  }

  printStep();
}

void SegregatedSolver::printStep() {
  if (verbose < 1) return;
  mout << "Segregated Solver Step Info:" << endl;
  for (const auto &metric: evaluationMetrics) {
    mout << "\t" << metric.first << " = " << metric.second.back() << endl;
  }
  mout << endl;
}


void SegregatedSolver::initEvaluation(const IElphyAssemble &elphyAssemble,
                                      const IElasticity &elasticityAssemble) {
  for (const auto &metric: metrics) {
    evaluationMetrics[metric] = std::vector<double>{};
  }
  for (const auto &metric: elphyAssemble.GetElphyProblem().EvaluationMetrics()) {
    evaluationMetrics[metric] = std::vector<double>{};
  }
  for (const auto &metric: elasticityAssemble.GetElasticityProblem().EvaluationMetrics()) {
    evaluationMetrics[metric] = std::vector<double>{};
  }
}

void SegregatedSolver::printFullEvaluation() {

  mout << endl << "======= FullCardmechInfo ==========================" << endl;
  for (const auto &metric: evaluationMetrics) {
    auto entries = metric.second;
    mout << metric.first << " = [" << entries[0];
    for (int i = 1; i < entries.size(); ++i) {
      mout << ", " << entries[i];
    }
    mout << "]" << endl;
  }
  mout << "===================================================" << endl;
}

void SegregatedSolver::Initialize(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble,
                                  Vector &potential,
                                  Vector &displacement) {
  elphySolver->Initialize(elphyAssemble, potential);
  mechSolver->Initialize(mechAssemble, displacement);
  initEvaluation(elphyAssemble, mechAssemble);

  elphySolver->SetVtk(-1);

  cardiacTransfer = GetCardiacTransfer(displacement, potential);
  evaluationPoints = elphyAssemble.GetElphyProblem().GetEvaluationPoints();
  int numberBM=elphyAssemble.GetElphyProblem().getNumberEvaluationPointsBesideMesh();
  RemoveEvaluationPointsBesidesMesh(numberBM);
}

bool SegregatedSolver::Method(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble,
                              Vector &potential,
                              Vector &displacement) {

  Initialize(elphyAssemble, mechAssemble, potential, displacement);//, iota_c, gamma_f_c_pot);

  Vectors vNew(3, potential, false);
  v_potential(vNew) = potential;
  v_stretch(vNew) = 0.0;
  v_iota(vNew) = 1.0;

  Vectors uNew(4, displacement, false);
  u_displacement(uNew) = displacement;
  u_stretch(uNew) = 0.0;  // Stretch (vertices)
  u_iota(uNew) = 1.0;  // Iota4
  u_pot(uNew) = 0.0;  // Potential for plotting
  cardiacTransfer->Project(u_pot(uNew), v_potential(vNew));//v_potential

  EvaluateIteration(elphyAssemble, mechAssemble, vNew, uNew);
  PlotIteration(elphyAssemble, mechAssemble, vNew, uNew);
  while (!mechAssemble.IsFinished()) {
    bool converged = Step(elphyAssemble, mechAssemble, vNew, uNew);
    if (converged) {
    EvaluateIteration(elphyAssemble, mechAssemble, vNew, uNew);
    PlotIteration(elphyAssemble, mechAssemble, vNew, uNew);
      mout.PrintInfo("SolverInfo", 1,
                     PrintInfoEntry("Convergence", converged));
    }
    else {
      mout.PrintInfo("SolverInfo", 1,
                     PrintInfoEntry("Convergence", converged));
      return false;
    }
  }

  //mechAssemble.CalculateAndPrintMinMaxGammaF();
  potential = v_potential(vNew);
  displacement = u_displacement(uNew);

  printFullEvaluation();
  return true;
}

bool SegregatedSolver::Step(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble,
                            Vectors &elphyValues,
                            Vectors &mechValues) {
  /* TODO: Implement deformation of conductivity tensor.
   * Is Sigma always diagonal?
   * Is it then possible to assign VectorFields or Scalars instead of using a deformation gradient
   * inside Elphy?
   */
  vout(2) << "Solving MechStep from " << mechAssemble.Time() << " to "
          << mechAssemble.NextTimeStep(false) << endl;

  std::vector<Vector> dyn;
  mechSolver->GetDynamicVectors(dyn);

  elphyAssemble.ResetTime(mechAssemble.Time(), mechAssemble.NextTimeStep(false), elphySteps);
  elphyAssemble.UpdateDeformation(u_displacement(mechValues), dyn[0], v_potential(elphyValues));

  if(plotCa){
    Vector ca(elphyValues[0]);
    elphySolver->GatingVectorAti(ca,1);
    elphyAssemble.PrintVariable(ca,"Ca");
  }

  while (!elphyAssemble.IsFinished()) {
    vout(3) << "Solving ElphyProblem in [" << elphyAssemble.FirstTStep() << ", "
            << elphyAssemble.LastTStep()
            << "] - step " << elphyAssemble.Step() + 1 << " at time " << elphyAssemble.Time() << "."
            << endl;

    elphySolver->printSolverNameInStep(elphyAssemble,ongoingElphyStep);
    elphySolver->Step(elphyAssemble, elphyValues);
    ongoingElphyStep+=1;
  }
  elphyAssemble.GetElphyProblem().PrintMinMaxJacobian(v_potential(elphyValues));
  // Projects stretch from elphy to mech
  cardiacTransfer->Project(u_stretch(mechValues), v_stretch(elphyValues));

  mechAssemble.UpdateStretch(u_stretch(mechValues));

  // Solves one mech step
  int timeStepExtrapolation = 0;
  Config::Get("timeStepExtrapolation", timeStepExtrapolation);
  if (timeStepExtrapolation == 1) {
    u_displacement(mechValues) += mechAssemble.StepSize() * dyn[0];
    u_displacement(mechValues) += 0.5 * mechAssemble.StepSize() * mechAssemble.StepSize() * dyn[1];
  } else if (timeStepExtrapolation == 2) {
    double DampExtrapolation = 1;
    Config::Get("DampExtrapolation", DampExtrapolation);
    u_displacement(mechValues) += DampExtrapolation * mechAssemble.StepSize() * dyn[0];
  }

  bool conv =  mechSolver->Step(mechAssemble, u_displacement(mechValues));
  ongoingMechStep=mechAssemble.Step();


  mechAssemble.PrintPointEvaluation(u_displacement(mechValues), "disp ", evaluationPoints);
  //mechAssemble.PrintPointEvaluation(dyn[0], "vel ", evaluationPoints);
  //mechAssemble.PrintPointEvaluation(dyn[1], "acc ", evaluationPoints);


  // Interpolates iota_4 from mech to elphy
  mechAssemble.GetInvariant(u_displacement(mechValues), u_iota(mechValues));
  cardiacTransfer->Prolongate(u_iota(mechValues), v_iota(elphyValues));
  //mechAssemble.PrintPointEvaluation(u_iota(mechValues), "iota4 ", evaluationPoints);
  //mechAssemble.PointEvaluationGamma( "gamma ", evaluationPoints);

  return conv;
}
double adaptiveSegregatedSolver::getNewMechDeltaTime(int counter){
  double newMechTimeStepSize;
  newMechTimeStepSize= double(counter) *elphyDeltaTime;
  return newMechTimeStepSize;
};
bool adaptiveSegregatedSolver::Step(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble,
                                    Vectors &elphyValues,
                                    Vectors &mechValues) {
  bool conv=false;

  std::vector<Vector> dyn;
  mechSolver->GetDynamicVectors(dyn);
  elphyAssemble.UpdateDeformation(u_displacement(mechValues), dyn[0], v_potential(elphyValues));

  elphyAssemble.ResetTime(mechAssemble.Time(), mechAssemble.LastTStep(), elphyDeltaTime);
  double gammaInfNorm=elphyAssemble.InftyNormOnVertices(v_stretch(elphyValues));
  int elphyStepCounter=0;

  if (gammaInfNorm<=0.0) {
    while (elphyStepCounter != elphySteps && gammaInfNorm <=0.0) {
      //while (!elphyAssemble.IsFinished()) {
      elphySolver->printSolverNameInStep(elphyAssemble, ongoingElphyStep);
      elphySolver->Step(elphyAssemble, elphyValues);

      gammaInfNorm = elphyAssemble.InftyNormOnVertices(v_stretch(elphyValues));
      ongoingElphyStep += 1;
      elphyStepCounter += 1;
    }
  }else if(0.0<gammaInfNorm && gammaInfNorm<factorMaxGammaInfnorm*maxGammaInfNorm) {

    int numberOfInbetweenTimSteps=int(log2(elphySteps))-1;
    //vout(2)<<" numberOfInbetweenTimSteps "<<numberOfInbetweenTimSteps;
    for (int i=0;i<numberOfInbetweenTimSteps;i++){
      double leftboundary= (double(i)/double(numberOfInbetweenTimSteps))*factorMaxGammaInfnorm*maxGammaInfNorm;
      double rightboundary= (double(i+1)/double(numberOfInbetweenTimSteps))*factorMaxGammaInfnorm*maxGammaInfNorm;
      //vout(2)<<" leftboundary "<<leftboundary<<" rightboundary "<<rightboundary;
      if ( leftboundary< gammaInfNorm && gammaInfNorm<= rightboundary){
       // vout(2)<<" i "<<i<<" number steps "<<int(elphySteps/(pow(2,i+1)))<<endl;
        while (elphyStepCounter !=int(elphySteps/(pow(2,i+1)))) {
          elphySolver->printSolverNameInStep(elphyAssemble, ongoingElphyStep);
          elphySolver->Step(elphyAssemble, elphyValues);
          ongoingElphyStep += 1;
          elphyStepCounter += 1;
        }
        break;
      }
    }
  }else{
      elphySolver->printSolverNameInStep(elphyAssemble, ongoingElphyStep);
      elphySolver->Step(elphyAssemble, elphyValues);
      ongoingElphyStep += 1;
      elphyStepCounter=1;
    }
  if(elphyStepCounter != oldEplpyStepCounter){
    mechAssemble.ResetTime(mechAssemble.Time(), mechAssemble.LastTStep(), getNewMechDeltaTime(elphyStepCounter));
    mechSolver->nonlinearSolver->InitializeMatrices(mechAssemble,u_displacement(mechValues));
    mechSolver->nonlinearSolver->updateRHSValues(getNewMechDeltaTime(elphyStepCounter));
  }
  oldEplpyStepCounter=elphyStepCounter;

 // Projects stretch from elphy to mech
  cardiacTransfer->Project(u_stretch(mechValues), v_stretch(elphyValues));
  mechAssemble.UpdateStretch(u_stretch(mechValues));


  vout(2) << "Solving MechStep from " << mechAssemble.Time() << " to "
          << mechAssemble.NextTimeStep(false) << endl;

  conv =  mechSolver->Step(mechAssemble, u_displacement(mechValues));
  ongoingMechStep+=1;


  mechAssemble.PrintPointEvaluation(u_displacement(mechValues), "disp ", evaluationPoints);
  // Interpolates iota_4 from mech to elphy
  mechAssemble.GetInvariant(u_displacement(mechValues), u_iota(mechValues));
  cardiacTransfer->Prolongate(u_iota(mechValues), v_iota(elphyValues));
  return conv;
}
void SegregatedSolver::RemoveEvaluationPointsBesidesMesh(int size){
  for(int i=0;i<size;i++){
    evaluationPoints.pop_back();
  }

}
void SegregatedSolver::PlotIteration(const IElphyAssemble &elphyAssemble,
                                     const IElasticity &mechAssemble, Vectors &elphyValues,
                                     Vectors &mechValues) const {
  cardiacTransfer->Project(u_pot(mechValues), v_potential(elphyValues));

  if (vtk <= 0) return;

  if (elphyVtk > 0) elphyAssemble.PlotIteration(elphyValues, mechAssemble.Step());
  if (mechVtk > 0) mechAssemble.PlotIteration(mechValues);
}

bool SegregatedSolverOnCells::Method(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble,
                              Vector &potential,
                              Vector &displacement) {
  const Meshes &M = displacement.GetMeshes();
  auto disc0 = std::make_shared<LagrangeDiscretization>(M, 0, 1);
  Vector gamma_f_c(disc0);
  gamma_f_c = 0;
  const Meshes &Mpot = potential.GetMeshes();
  auto disc0pot = std::make_shared<LagrangeDiscretization>(Mpot, 0, 1);
  Vector gamma_f_c_pot(disc0pot);
  gamma_f_c_pot = 0;

  Vector iota_c(disc0);
  iota_c = 1;
  Vector iota_c_pot(disc0pot);
  iota_c_pot = 1;

  //vout(0)<<" norm of iota_c "<<iota_c.norm()<<" size " <<iota_c.size()<<endl;
  //vout(0)<<" norm of iota_c_pot "<<iota_c_pot.norm()<<" size " <<iota_c_pot.size()<<endl;
  //vout(0)<<" norm of gamma_f_c "<<gamma_f_c.norm()<<" size " <<gamma_f_c.size()<<endl;
  Initialize(elphyAssemble, mechAssemble, potential, displacement);//, iota_c, gamma_f_c_pot);

  Vectors vNew(3, potential, false);
  v_potential(vNew) = potential;
  v_stretch(vNew) = 0.0;
  v_iota(vNew) = 1.0;

  Vectors uNew(4, displacement, false);
  u_displacement(uNew) = displacement;
  u_stretch(uNew) = 0.0;  // Stretch (vertices)
  u_iota(uNew) = 1.0;  // Iota4
  u_pot(uNew) = 0.0;  // Potential for plotting
  cardiacTransfer->Project(u_pot(uNew), v_potential(vNew));//v_potential

  EvaluateIteration(elphyAssemble, mechAssemble, vNew, uNew);
  PlotIteration(elphyAssemble, mechAssemble, vNew, uNew);
  while (!mechAssemble.IsFinished()) {
    bool converged = Step(elphyAssemble, mechAssemble, vNew, uNew, iota_c, iota_c_pot, gamma_f_c,gamma_f_c_pot);
    if (converged) {
      // mpp::plot("gamma_f_c" + std::to_string(mechAssemble.Time())) << gamma_f_c << mpp::endp;
      // mpp::plot("iota_c" + std::to_string(mechAssemble.Time())) << iota_c << mpp::endp;
      EvaluateIteration(elphyAssemble, mechAssemble, vNew, uNew);
      PlotIteration(elphyAssemble, mechAssemble, vNew, uNew);

      mout.PrintInfo("SolverInfo", 1,
                     PrintInfoEntry("Convergence", converged));
    }
    else {
      mout.PrintInfo("SolverInfo", 1,
                     PrintInfoEntry("Convergence", converged));
      return false;
    }
  }

  //mechAssemble.CalculateAndPrintMinMaxGammaF();
  potential = v_potential(vNew);
  displacement = u_displacement(uNew);

  printFullEvaluation();
  return true;
}
bool SegregatedSolverOnCells::Step(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble,
                            Vectors &elphyValues,
                            Vectors &mechValues,
                            Vector& iota_c,
                            Vector& iota_c_pot,
                            Vector& gamma_f_c,
                            Vector& gamma_f_c_pot) {
  /* TODO: Implement deformation of conductivity tensor.
   * Is Sigma always diagonal?
   * Is it then possible to assign VectorFields or Scalars instead of using a deformation gradient
   * inside Elphy?
   */
  vout(2) << "Solving MechStep from " << mechAssemble.Time() << " to "
          << mechAssemble.NextTimeStep(false) << endl;

  //mechAssemble.PointEvaluationActiveStrain(u_displacement(mechValues));
  std::vector<Vector> dyn;
  mechSolver->GetDynamicVectors(dyn);
  elphyAssemble.ResetTime(mechAssemble.Time(), mechAssemble.NextTimeStep(false), elphySteps);
  elphyAssemble.UpdateDeformation(u_displacement(mechValues), dyn[0], v_potential(elphyValues));

  if(plotCa){
    Vector ca(elphyValues[0]);
    elphySolver->GatingVectorAti(ca,1);
    elphyAssemble.PrintVariable(ca,"Ca");
  }


  while (!elphyAssemble.IsFinished()) {
    vout(3) << "Solving ElphyProblem in [" << elphyAssemble.FirstTStep() << ", "
            << elphyAssemble.LastTStep()
            << "] - step " << elphyAssemble.Step() + 1 << " at time " << elphyAssemble.Time() << "."
            << endl;
    elphySolver->printSolverNameInStep(elphyAssemble,ongoingElphyStep);
    elphySolver->Step(elphyAssemble, elphyValues, iota_c_pot, gamma_f_c_pot);
    ongoingElphyStep+=1;
  }

  elphyAssemble.GetElphyProblem().PrintMinMaxJacobian(v_potential(elphyValues));

  cardiacTransfer->ProjectCells(gamma_f_c,gamma_f_c_pot);
  mechAssemble.UpdateStretch(u_stretch(mechValues),gamma_f_c);

  // Solves one mech step

  int timeStepExtrapolation = 0;
  Config::Get("timeStepExtrapolation", timeStepExtrapolation);
  if (timeStepExtrapolation == 1) {
    u_displacement(mechValues) += mechAssemble.StepSize() * dyn[0];
    u_displacement(mechValues) += 0.5 * mechAssemble.StepSize() * mechAssemble.StepSize() * dyn[1];
  } else if (timeStepExtrapolation == 2) {
    double DampExtrapolation = 1;
    Config::Get("DampExtrapolation", DampExtrapolation);
    u_displacement(mechValues) += DampExtrapolation * mechAssemble.StepSize() * dyn[0];
  }

  bool conv = mechSolver->Step(mechAssemble, u_displacement(mechValues));
  ongoingMechStep=mechAssemble.Step();
  mechAssemble.PrintPointEvaluation(u_displacement(mechValues), "disp ", evaluationPoints);

  mechAssemble.GetInvariant(u_displacement(mechValues), u_iota(mechValues), iota_c);
  cardiacTransfer->ProlongateCell(iota_c,iota_c_pot);
//cardiacTransfer->Prolongate(u_iota(mechValues), v_iota(elphyValues));

  //vout(0)<<" norm of iota_c "<<iota_c.norm()<<endl;
  //vout(0)<<" norm of iota_c_pot "<<iota_c_pot.norm()<<endl;
  //vout(0)<<" norm of gamma_f_c "<<gamma_f_c.norm()<<endl;
  //vout(0)<<" norm of gamma_f_c_pot "<<gamma_f_c_pot.norm()<<endl;



  return conv;
}