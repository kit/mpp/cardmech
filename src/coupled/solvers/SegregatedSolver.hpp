#ifndef SEGREGATEDSOLVER_H
#define SEGREGATEDSOLVER_H


#include "CardiacSolver.hpp"
#include "assemble/IElphyAssemble.hpp"
#include "assemble/IElasticity.hpp"
#include "MCellModel.hpp"

#include "coupled/transfers/MultiPartTransfer.hpp"

#include "solvers/ElphySolver.hpp"
#include "solvers/DynamicMechanicsSolver.hpp"

/**
 * Solves coupled electro-elastodynamical problem with the following scheme:
 *  0. Initialize ELPHY and MECH
 *  1. Solve ELPHY from t_n to t_{n+1} in m steps
 *  2. Project values from ELPHY to MECH
 *  3. Solve MECH from t_n to t_{n+1} in 1 step
 *  4. Interpolate values from MECH to ELPHY
 *  5. If t_{n+1} = t_N STOP, else return to 1.
 */
class SegregatedSolver : public CardiacSolver {
protected:
  int elphyVtk{0};
  int mechVtk{0};
  int elphySteps;
  int ongoingElphyStep;
  int ongoingMechStep;
  bool plotCa{false};

  std::unordered_map<std::string, std::vector<double>> evaluationMetrics{};


  std::unique_ptr<ElphySolver> elphySolver;
  std::unique_ptr<ElastodynamicTimeIntegrator> mechSolver;
  std::vector<Point> evaluationPoints;

  /// Creates some output for each step.
  void printStep();

  void printFullEvaluation();

  void initEvaluation(const IElphyAssemble &elphyAssemble,
                      const IElasticity &elasticityAssemble);


  std::unique_ptr<MultiPartDeformationTransfer> cardiacTransfer;
public:
  explicit SegregatedSolver(
      std::unique_ptr<ElphySolver> &&eSolver,
      std::unique_ptr<ElastodynamicTimeIntegrator> &&mSolver,
      int eSteps = -1) :
      elphySolver(std::move(eSolver)), mechSolver(std::move(mSolver)), elphySteps(eSteps) {
    ongoingElphyStep=1;
    ongoingMechStep=0;
    Config::Get("CoupledSolverVerbose", verbose);
    Config::Get("CoupledSolverVTK", vtk);
    Config::Get("ElphySolverVTK", elphyVtk);
    Config::Get("DynamicSolverVTK", mechVtk);

    if(elphySteps < 0){
      double mechDT{1.0}, elphyDT{1.0};
      Config::Get("MechDeltaTime", mechDT);
      Config::Get("ElphyDeltaTime", elphyDT);
      if (mechDT > elphyDT) {
        elphySteps = (int) (mechDT / elphyDT + 1e-4);
      }

    }
  };

  void EvaluateIteration(const IElphyAssemble &elphyAssemble, const IElasticity &mechAssemble,
                         Vectors elphyValues, Vectors mechValues);

  void PlotIteration(const IElphyAssemble &elphyAssemble, const IElasticity &mechAssemble,
                     Vectors &elphyValues, Vectors &mechValues) const;


  void Initialize(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble, Vector &potential, Vector &displacement);

  //void Initialize(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble, Vector &potential, Vector &displacement, Vector& iota);//, Vector& gamma_f);

  /// Solves coupled equation from mechAssemble.firstTimeStep to mechAssemble.lastTimeStep
  virtual bool Method(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble, Vector &potential, Vector &displacement);

  /// Calculates next timestep for the mechanics as well as for the electrophysiology.


  virtual bool Step(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble, Vectors &elphyValues, Vectors &mechValues);

  void Finalize(Vectors &values) {};
  void RemoveEvaluationPointsBesidesMesh(int size);

};

class adaptiveSegregatedSolver: public SegregatedSolver {
  double elphyDeltaTime{0.0};
  double maxGammaInfNorm{0.0};
  int oldEplpyStepCounter{0};
  double factorMaxGammaInfnorm{0.8};
public:
  explicit adaptiveSegregatedSolver(std::unique_ptr<ElphySolver> &&eSolver,
  std::unique_ptr<ElastodynamicTimeIntegrator> &&mSolver,int eSteps = -1) :
      SegregatedSolver(std::move(eSolver),std::move(mSolver),eSteps) {
    if(elphySteps<=2){
      Exit("The difference between MechDeltaTime and ElphyDeltaTime is to small for the adaptive scheme!")
    }
    Config::Get("ElphyDeltaTime", elphyDeltaTime);
    Config::Get("MaximalGammaInf", maxGammaInfNorm);
    Config::Get("FactorMaximalGammaInf",factorMaxGammaInfnorm);
    oldEplpyStepCounter=elphySteps;

  };
  bool Step(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble, Vectors &elphyValues, Vectors &mechValues) override;
  double getNewMechDeltaTime(int counter);
};
class SegregatedSolverOnCells : public SegregatedSolver {

public:
  explicit SegregatedSolverOnCells(
      std::unique_ptr<ElphySolver> &&eSolver,
  std::unique_ptr<ElastodynamicTimeIntegrator> &&mSolver,int eSteps = -1) : SegregatedSolver(std::move(eSolver),std::move(mSolver),eSteps){};
  bool Method(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble, Vector &potential, Vector &displacement) override;
  bool Step(IElphyAssemble &elphyAssemble, IElasticity &mechAssemble, Vectors &elphyValues, Vectors &mechValues, Vector& iota_c,Vector& iota_c_pot, Vector& gamma_f_c,Vector& gamma_f_c_pot);

};

#endif //M_CARDIACSOLVERS_H
