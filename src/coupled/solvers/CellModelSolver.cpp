#include "CellModelSolver.hpp"


void CellModelSolver::Solve(Vector &potential, Vector &stretch) {
  elphySolver.Initialize(elphyA, potential);
  elphySolver.SetVtk(-1);

  Vectors uNew(3, potential);
  uNew[0] = potential;
  uNew[1] = stretch;
  uNew[2] = 1.0;



  if (verbose > 0) {
    PlotValues(uNew, 0);
  }
  while(!elphyA.IsFinished()){
    int step=elphyA.Step()+1;
    //if (verbose == 1)
      mout << "Solving Step " << step << " at time " << elphyA.Time() << "." << endl;
    elphySolver.Step(elphyA, uNew);

    if (verbose > 0) {
      PlotValues(uNew, step);
    }
    if (verbose > 1) {
      PrintValues(uNew, step, elphyA.Time());
    }


    // Update Invariant
    for (row r = stretch.rows(); r != stretch.rows_end(); ++r) {
      for (int j = 0; j < r.NumberOfDofs(); ++j) {
        uNew[2](r, j) = (1.0 + uNew[1](r, j)) * (1.0 + uNew[1](r, j));
      }
    }

    elphyA.NextTimeStep();

  }
  potential = uNew[0];
  stretch = uNew[1];
}

void CellModelSolver::PlotValues(const Vectors &values, int step) const {
  if (vtk>0 && step % plottingsteps == 0) {
    auto &plot = mpp::plot("Potential" + stepAsString(step));
    plot.AddData("V", values[0]);
    plot.AddData("Stretch", values[1]);
    plot.PlotFile();
  }
}


void CellModelSolver::PrintValues(const Vectors &values, int step, double t) const {
  if (step % 100 != 0) return;

  for (row row = values[0].rows(); row != values[0].rows_end(); ++row) {
    if (row() == Origin) {
      mout.PrintInfo("CellModelValues", 1,
                     PrintInfoEntry("Step", step),
                     PrintInfoEntry("Time", t),
                     PrintInfoEntry("Potential", values[0](row, 0)),
                     PrintInfoEntry("Stretch", values[1](row, 0)),
                     PrintInfoEntry("Invariant", values[2](row, 0))
      );
    }
  }
}
