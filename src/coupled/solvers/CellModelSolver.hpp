#ifndef CELLMODELSOLVER_HPP
#define CELLMODELSOLVER_HPP

#include <CardiacSolver.hpp>
#include <solvers/ElphySolver.hpp>

class CellModelSolver : public CardiacSolver {
  IElphyAssemble &elphyA;
  ElphySolver &elphySolver;

  int plottingsteps;
  void setPlottingSteps(const std::string& configString = "PlottingSteps"){
    Config::Get(configString, plottingsteps);
    plottingsteps = std::min(plottingsteps, 1);
  }
public:
  explicit CellModelSolver(IElphyAssemble &A, ElphySolver &eSolver) : elphyA(A), elphySolver(eSolver) {
    Config::Get("CoupledSolverVTK", vtk);
    Config::Get("CoupledSolverVerbose", verbose);
    setPlottingSteps();
    elphySolver.SetVerbose(-1);
  };


  void Solve(Vector &potential, Vector &stretch);

  void PlotValues(const Vectors &values, int step) const;

  void PrintValues(const Vectors &values, int step, double t) const;
};

#endif //CARDMECH_CELLMODELSOLVER_HPP
