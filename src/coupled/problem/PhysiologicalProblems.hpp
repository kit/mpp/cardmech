#ifndef PHYSIOLOGICALPROBLEMS_HPP
#define PHYSIOLOGICALPROBLEMS_HPP

#include "CoupledProblem.hpp"
#include "problem/VentricleProblems.hpp"

class ContractingVentricleProblem : public CoupledProblem {
  using ElasticityProblem::verbose;
  using ElasticityProblem::meshesName;
  // Robin Boundary
  //std::array<double, 8> robinParams{10.0, 0.0, 5.0, 0.0, 1500.0, 1e-4, 1.0, 0.0};
  std::array<double, 8> robinParams{0.2, 0.0, 5.0e-3, 0.0, 1500.0, 1e-4, 1.0, 0.0};

  double robinParam(int bc, int index) const {
    return robinParams[(bc == ROBIN_BC_EPI) * 4 + index];
  };

  double k{0.1};
  double c{5e-3};

  VentricleProblem ventricleProblem;
public:
  explicit ContractingVentricleProblem(std::string const &defaultMesh = "") :
      ventricleProblem(defaultMesh) {
    Config::Get("TractionK", k);
    Config::Get("TractionC", c);

    ElasticityProblem::meshesName = ventricleProblem.GetMeshName();
    ElphyProblem::meshesName = ventricleProblem.GetMeshName();
  }

  ContractingVentricleProblem(const std::string &matName,
                              const std::vector<double> &matPar)
      : ventricleProblem(matName, matPar) {
    Config::Get("TractionK", k);
    Config::Get("TractionC", c);
    ElasticityProblem::meshesName = ventricleProblem.GetMeshName();
    ElphyProblem::meshesName = ventricleProblem.GetMeshName();
  }

  std::string Name() const override { return "Contracting Ventricle"; }

  const std::string &Model() const override { return ventricleProblem.Model(); }

  bool IsStatic() const override {
    return false;
  }

  double InitialPressure(const Point &x, int bndCondition) const override {
    return ventricleProblem.InitialPressure(x, bndCondition);
  }

  void Initialize(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    ventricleProblem.Initialize(dt, t, mechVolumes);
  }

  void Update(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    ventricleProblem.Update(dt, t, mechVolumes);
  }

  void Finalize(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    ventricleProblem.Finalize(dt, t, mechVolumes);
  }

  void Iterate(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    ventricleProblem.Iterate(dt, t, mechVolumes);
  }

  bool Converged(const std::array<double, 4> &mechVolumes) override {
    return ventricleProblem.Converged(mechVolumes);
  }

  double TractionStiffness(double t, const Point &x, int sd) const override { return k; };

  double TractionViscosity(double t, const Point &x, int sd) const override { return c; };

  std::vector<double> EvaluationResults(const Vector &solution) const;
};

class BiVentricleProblem : public CoupledProblem {
  using ElasticityProblem::verbose;
  // Robin Boundary
  //std::array<double, 8> robinParams{10.0, 0.0, 5.0, 0.0, 1500.0, 1e-4, 1.0, 0.0};
  std::array<double, 8> robinParams{0.2, 0.0, 5.0e-3, 0.0, 1500.0, 1e-4, 1.0, 0.0};

  double robinParam(int bc, int index) const {
    return robinParams[(bc == ROBIN_BC_EPI) * 4 + index];
  };

  double k{0.1};
  double c{5e-3};
protected:
  std::array<double, 2> activation{0.0};
  std::array<double, 2> pressureED{0.5, 0.5};
  std::array<double, 2> pressureES{0.5, 0.5};
  const std::array<double, 2> minVolumeED{1.03, 141.55};
  const std::array<double, 2> maxVolumeED{435.17, 236.55};
  const std::array<double, 2> diffVolumeED{maxVolumeED[0] - minVolumeED[0],
                                           maxVolumeED[1] - minVolumeED[1]};
  const std::array<double, 2> minVolumeES{62.75, 80.50};
  const std::array<double, 2> elastanceES{2.393, 0.457};
  const std::array<double, 2> elastanceED{0.014, -0.005};
  std::array<double, 4> chamberPressure{};

  double calcEDPressure(double vol, int i) const;

  double calcESPressure(double vol, int i) const;

  virtual std::pair<double, double> getPressureVolume(double t, int index) const = 0;

  double pressure(double t, const Point &x, int bndCondition) const override;

public:
  explicit BiVentricleProblem(const std::string &meshName = "",
                              std::vector<double> params = {2, 8, 4, 2})
      : CoupledProblem("Bonet", params) /*TODO: Get correct parameters from Klotz */ , chamberPressure(pressureFromConfig()) {
    defaultMeshName(meshName);
    Config::Get("TractionK", k);
    Config::Get("TractionC", c);
    Config::Get("pressureED_LV", pressureED[0]);
    Config::Get("pressureED_RV", pressureED[1]);
    Config::Get("pressureES_LV", pressureES[0]);
    Config::Get("pressureES_RV", pressureES[1]);
  }

  std::string Name() const override {
    return "BiVentricular Problem on " + ElasticityProblem::GetMeshName();
  }

  bool IsStatic() const override {
    return false;
  }

  double InitialPressure(const Point &x, int bndCondition) const override {
    return 0.0;
  }

  void Initialize(double dt, double t, const std::array<double, 4> &mechVolumes) override {
  }

//  void Update(double dt, double t, const std::array<double, 4> &mechVolumes) override;

  double TractionStiffness(double t, const Point &x, int sd) const override { return k; };

  double TractionViscosity(double t, const Point &x, int sd) const override { return c; };
};


class CoarseBiventricleProblem : public BiVentricleProblem {
protected:

  std::pair<double, double> getPressureVolume(double t, int index) const override;

public:
  explicit CoarseBiventricleProblem(const std::string &meshName = "TestBiventricleInstantActivation")
      : BiVentricleProblem("KovachevaBiventricleInstantActivation",
                           {0.29, 42.0, 16.8, 29.4})  /*TODO: Get correct parameters from Klotz */ {
    evaluationPoints.emplace_back(Point(86.75, -44.2302, 1.74331));// Apex excited left ventricle
    evaluationPoints.emplace_back(Point(48.4428, -35.0794, 33.8927));//midwall middle exitation time !=0
    evaluationPoints.emplace_back(Point(22.5562, -16.1836, 51.2538));// midwall top excitation time =0
    evaluationPoints.emplace_back(Point(55.1275, -62.8303, -2.31392));//right ventricle bottom outer face excitation time =0
    //Todo use meshName = KovachevaBiventricle
  }
};


class FineBiventricleProblem : public BiVentricleProblem {
protected:

  std::pair<double, double> getPressureVolume(double t, int index) const override;

public:
  explicit FineBiventricleProblem(const std::string &meshName = "BiventricleDirichlet")
      : BiVentricleProblem(meshName, {2, 8, 4, 2}) /*TODO: Get correct parameters from Klotz */ {
  }
};


#endif //PHYSIOLOGICALPROBLEMS_HPP
