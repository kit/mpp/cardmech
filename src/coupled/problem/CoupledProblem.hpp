#ifndef ICOUPLEDPROBLEM_HPP
#define ICOUPLEDPROBLEM_HPP

#include "CardiacData.hpp"
#include "problem/ElphyProblem.hpp"
#include "problem/ElasticityProblem.hpp"


class CoupledProblem : public ElasticityProblem, public ElphyProblem {
protected:
  int coupledVerbose() const {
    return std::max(ElasticityProblem::verbose, ElphyProblem::verbose);
  }

  void defaultMeshName(const std::string &name);

public:
  CoupledProblem() : ElasticityProblem(), ElphyProblem() {
    InitializeEvaluationPoints();
  };

  CoupledProblem(bool at, std::array<double, 4> sigmaParams)
      : ElasticityProblem(), ElphyProblem(at, sigmaParams) {
    InitializeEvaluationPoints();
    if (ElphyMeshName() == "TestBiventricleSimpleExcitation_smooth" ||ElphyMeshName() == "TestBiventricle") {
      diffusionValues[0] =0.001 * VectorField(0.28, 0.182, 0.098); // Wie im Paper von Gerach und Loewe
    }
  }

  CoupledProblem(const std::string &matName, const std::vector<double> &matParameters,
                 bool at = true)
      : ElasticityProblem(matName, matParameters), ElphyProblem(at) {
    InitializeEvaluationPoints();
    if (ElphyMeshName() == "TestBiventricleSimpleExcitation_smooth" ||ElphyMeshName() == "TestBiventricle") {
      diffusionValues[0] =0.001 * VectorField(0.28, 0.182, 0.098); // Wie im Paper von Gerach und Loewe
    }
  }

  CoupledProblem(const std::string &matName, const std::vector<double> &matParameters, bool at,
                 std::array<double, 4> sigmaParams)
      : ElasticityProblem(matName, matParameters), ElphyProblem(at, sigmaParams) {
    InitializeEvaluationPoints();
    if (ElphyMeshName() == "TestBiventricleSimpleExcitation_smooth" ||ElphyMeshName() == "TestBiventricle") {
      diffusionValues[0] =0.001 * VectorField(0.28, 0.182, 0.098); // Wie im Paper von Gerach und Loewe
    }
  };

  void CreateMeshes(int elphyLevel, int mechLevel) {
    std::string chamber{"fourchamber"};

    Config::Get("ElphyMeshPart", chamber);
    auto elphyCreator = MeshesCreator().
        WithMeshName(ElphyMeshName()).
        WithLevel(elphyLevel).
        WithIncludeVector(getIncludedChambers(chamber));
    CreateElphyMesh(elphyCreator);

    Config::Get("MechMeshPart", chamber);
    auto mechCreator = MeshesCreator().
        WithMeshName(MechMeshName()).
        WithDistribution(GetElphyMeshes().GetDistribution()).
        WithLevel(mechLevel).
        WithIncludeVector(getIncludedChambers(chamber));
    CreateMechMesh(mechCreator);

    GetElphyMeshes().GetDistribution()->Clear();
  }

  const std::string &ElphyMeshName() const;

  const std::string &MechMeshName() const;

  void CreateElphyMesh(MeshesCreator &creator);

  void CreateMechMesh(MeshesCreator &creator);


  const CoarseGeometry &ElphyDomain() const;;

  const Meshes &GetElphyMeshes() const;

  const Mesh &GetElphyMesh(int level) const;

  const Mesh &GetElphyMesh(MeshIndex level) const;


  const CoarseGeometry &MechDomain() const;;

  const Meshes &GetMechMeshes() const;

  const Mesh &GetMechMesh(int level) const;

  const Mesh &GetMechMesh(MeshIndex level) const;

  virtual std::string Name() const override = 0;
  virtual void InitializeEvaluationPoints() override;
  std::string Evaluate(const Vector &solution) const override {
    return EvaluateElphy(solution) + "\n" + EvaluateMech(solution);
  }

  std::vector<double> EvaluationResults(const Vector &solution) const override {
    auto elphyResults = EvaluationResultsElphy(solution);
    auto mechResults = EvaluationResultsMech(solution);

    return mechResults;
  }

  std::vector<std::string> EvaluationMetrics() const override {
    auto elphyMetrics = EvaluationMetricsElphy();
    auto mechMetrics = EvaluationMetricsMech();

    return mechMetrics;
  }

  virtual std::string EvaluateElphy(const Vector &solution) const { return ""; }

  virtual std::string EvaluateMech(const Vector &solution) const { return ""; }

  virtual std::vector<double> EvaluationResultsElphy(
      const Vector &solution) const {
    return {};
  }

  virtual std::vector<double> EvaluationResultsMech(
      const Vector &solution) const {
    return {};
  }

  virtual std::vector<std::string> EvaluationMetricsElphy() const { return {}; }

  virtual std::vector<std::string> EvaluationMetricsMech() const { return {}; }

  virtual ~CoupledProblem() = default;
};


class CompositeCoupledProblem : public CoupledProblem {
  std::unique_ptr<ElphyProblem> elphyProblem;
  std::unique_ptr<ElasticityProblem> mechProblem;
public:
  CompositeCoupledProblem() : CompositeCoupledProblem(std::move(GetElphyProblem()),
                                                      std::move(GetElasticityProblem())) {}

  CompositeCoupledProblem(std::unique_ptr<ElphyProblem> &&eProblem,
                          std::unique_ptr<ElasticityProblem> &&mProblem)
      : CoupledProblem(), elphyProblem(std::move(eProblem)), mechProblem(std::move(mProblem)) {
    if (elphyProblem->GetMeshName() != mechProblem->GetMeshName()) {
      mout << elphyProblem->GetMeshName() << " - " << mechProblem->GetMeshName() << endl;
      THROW("Meshes of problems don't match")
    }
  }

  std::string Name() const override {
    return "Composite Coupled Problem [" + elphyProblem->Name() + " | " + mechProblem->Name() + "]";
  }

  const std::string &Model() const override { return mechProblem->Model(); }

  bool IsStatic() const override {
    return mechProblem->IsStatic();
  }

  VectorField Load(double t, const Point &x, const Cell &c) const override {
    return mechProblem->Load(t, x, c);
  }

  double ActiveStress(double t, const Point &x) const override {
    return mechProblem->ActiveStress(t, x);
  }

  double ActiveStretch(double t, const Point &x) const override {
    return mechProblem->ActiveStretch(t, x);
  }

  double TractionStiffness(double t, const Point &x, int sd) const override {
    return mechProblem->TractionStiffness(t, x, sd);
  }

  double TractionViscosity(double t, const Point &x, int sd) const override {
    return mechProblem->TractionViscosity(t, x, sd);
  }

  VectorField Deformation(double time, const Point &x) const override {
    return mechProblem->Deformation(time, x);
  }

  VectorField Velocity(double time, const Point &x) const override {
    return mechProblem->Velocity(time, x);
  }

  VectorField Acceleration(double time, const Point &x) const override {
    return mechProblem->Acceleration(time, x);
  }

  Tensor DeformationGradient(double time, const Point &x) const override {
    return mechProblem->DeformationGradient(time, x);
  }

  double InitialPressure(const Point &x, int bndCondition) const override {
    return mechProblem->InitialPressure(x, bndCondition);
  }

  double Scale() const override {
    return mechProblem->Scale();
  }

  void Scale(double s) const override {
    mechProblem->Scale(s);
  }


  double Pressure(double t, const Point &x, int bndCondition) const override{
    return mechProblem-> Pressure(t, x, bndCondition);
  }

  void Initialize(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    mechProblem->Initialize(dt, t, mechVolumes);
  }

  void Update(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    mechProblem->Update(dt, t, mechVolumes);
  }

  void Finalize(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    mechProblem->Finalize(dt, t, mechVolumes);
  }

  void Iterate(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    mechProblem->Iterate(dt, t, mechVolumes);
  }

  bool Converged(const std::array<double, 4> &mechVolumes) override {
    return mechProblem->Converged(mechVolumes);
  }

  void InitializeEvaluationPoints() override {
    elphyProblem->InitializeEvaluationPoints();
  }

  Tensor Conductivity(const Cell &c,const Tensor &F) const override {
    return elphyProblem->Conductivity(c,F);
  }

  Tensor Conductivity(const Cell &c) const override {
    return elphyProblem->Conductivity(c);
  }

  std::vector<double> EvaluationResults(const Vector &solution) const override {
    return elphyProblem->EvaluationResults(solution);
  }

  virtual std::string EvaluateElphy(const Vector &solution) const {
    return elphyProblem->Evaluate(solution);
  }

  virtual std::string EvaluateMech(const Vector &solution) const {
    return mechProblem->Evaluate(solution);
  }

  double Potential(double time, const Point &x) const override {
    return elphyProblem->Potential(time, x);
  }

  double IonCurrent(double time, const Point &x) const override {
    return elphyProblem->IonCurrent(time, x);
  }

  void chamberSpecialization(const std::string &chamberName) const override {
    elphyProblem->chamberSpecialization(chamberName);
  }
};

std::unique_ptr<CoupledProblem> GetCoupledProblem(
    const std::string &problemName);

std::unique_ptr<CoupledProblem> GetCoupledProblem();

#endif //ICOUPLEDPROBLEM_HPP
