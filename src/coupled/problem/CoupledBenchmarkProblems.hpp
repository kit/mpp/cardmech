#ifndef COUPLEDBENCHMARKPROBLEMS_HPP
#define COUPLEDBENCHMARKPROBLEMS_HPP

#include "CoupledProblem.hpp"

class CoupledBeamProblem : public CoupledProblem{
public:
  CoupledBeamProblem() : CoupledProblem(){
    defaultMeshName("BenchmarkBeam3DTet");
  }

  std::string Name() const override { return "Coupled Beam Problem"; }
};
#endif //COUPLEDBENCHMARKPROBLEMS_HPP
