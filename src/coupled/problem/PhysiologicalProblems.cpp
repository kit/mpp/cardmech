#include "PhysiologicalProblems.hpp"
#include "problem/VentriclePoints.hpp"

#include "CardiacData.hpp"

#include "VentriclePVLoops.hpp"

std::vector<double> ContractingVentricleProblem::EvaluationResults(
    const Vector &solution) const {
  auto apexPot = findPotential(solution, {ventricleApex});

  mout.PrintInfo("LeftVentricle", coupledVerbose(),
                 PrintInfoEntry("ApexPotential", apexPot, 1));
  return {};
}

constexpr double An = 28.20232 * MMHGtoKPASCAL;
constexpr double Bn = 2.79;


/*void BiVentricleProblem::Update(double dt, double t, const std::array<double, 4> &mechVolumes) {
  //mout << "Physiological Problem " << endl << endl;
  for (int i = 0; i < 2; ++i) {
    auto vol = mechVolumes[i];
    pressureED[i] = calcEDPressure(vol, i);
    pressureES[i] = calcESPressure(vol, i);
    // Calculate Activation at time t
    auto pvEntry = getPressureVolume(t, i);
    activation[i] = (pvEntry.first - calcEDPressure(pvEntry.second, i)) /
                    (calcESPressure(pvEntry.second, i) - calcEDPressure(pvEntry.second, i));
  }
}*/

double BiVentricleProblem::calcESPressure(double vol, int i) const {
  return elastanceES[i] * (vol - minVolumeES[i]);//
}

double BiVentricleProblem::calcEDPressure(double vol, int i) const {
  mout << "vol " << vol << " minvol " << minVolumeED[i] << endl;
  if (vol < minVolumeED[i]) return 0;

  return An * pow((vol - minVolumeED[i]) / diffVolumeED[i], Bn); //
}

double BiVentricleProblem::pressure(double t, const Point &x, int bndCondition) const {
  return chamberPressure[bndCondition % 230];
//  int i = bndCondition % 230;
//  return (1 - activation[i]) * pressureED[i] + activation[i] * pressureES[i];
}

std::pair<double, double> CoarseBiventricleProblem::getPressureVolume(double t, int index) const {
  //TODO: Should be updated with coarse pV loops
  return pressureVolumeEntry(t, fineVentricleLoops[index]);
}

std::pair<double, double> FineBiventricleProblem::getPressureVolume(double t, int index) const {
  return pressureVolumeEntry(t, fineVentricleLoops[index]);
}
