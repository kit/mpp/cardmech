#ifndef DEFORMEDPROBLEM_HPP
#define DEFORMEDPROBLEM_HPP

#include "coupled/transfers/MultiPartTransfer.hpp"
#include "coupled/problem/CoupledProblem.hpp"


class DeformedCoupledProblem : public CoupledProblem {
  std::unique_ptr<CoupledProblem> originalProblem{nullptr};
  std::unique_ptr<MultiPartDeformationTransfer> transfer;
  std::shared_ptr<IDiscretization> defDisc;
  Vector* displacement; //TODO: has to be more efficient
  Vector* displacement_old;
  std::unique_ptr<Vector> velocity;
  bool withDeformFibersInConductivity=false;

public:
  DeformedCoupledProblem(std::unique_ptr<CoupledProblem> &&problem)
    : originalProblem(std::move(problem)) {
    InitializeEvaluationPoints();
    Config::Get("deformFibersInConductivity", withDeformFibersInConductivity);
  }
  std::string Name() const override {
    return originalProblem->Name() + " (Deformed)";
  }

  std::string Evaluate(const Vector &solution) const override {
    return originalProblem->Evaluate(solution);
  }

  std::vector<double> EvaluationResults(const Vector &solution) const override {
    return originalProblem->EvaluationResults(solution);
  }

  std::vector<std::string> EvaluationMetrics() const override {
    return originalProblem->EvaluationMetrics();
  }

  std::string EvaluateElphy(const Vector &solution) const override {
    return originalProblem->EvaluateElphy(solution);
  }

  std::string EvaluateMech(const Vector &solution) const override {
    return originalProblem->EvaluateMech(solution);
  }

  std::vector<double> EvaluationResultsElphy(
      const Vector &solution) const override {
    return originalProblem->EvaluationResultsElphy(solution);
  }

  std::vector<double> EvaluationResultsMech(
      const Vector &solution) const override {
    return originalProblem->EvaluationResultsMech(solution);
  }

  std::vector<std::string> EvaluationMetricsElphy() const override {
    return originalProblem->EvaluationMetricsElphy();
  }

  std::vector<std::string> EvaluationMetricsMech() const override {
    return originalProblem->EvaluationMetricsMech();
  }

  const std::string &Model() const override { return originalProblem->Model(); }

  bool IsStatic() const override {
    return originalProblem->IsStatic();
  }

  VectorField Load(double t, const Point &x, const Cell &c) const override {
    return originalProblem->Load(t, x, c);
  }

  double ActiveStress(double t, const Point &x) const override {
    return originalProblem->ActiveStress(t, x);
  }

  double ActiveStretch(double t, const Point &x) const override {
    return originalProblem->ActiveStretch(t, x);
  }

  double TractionStiffness(double t, const Point &x, int sd) const override {
    return originalProblem->TractionStiffness(t, x, sd);
  }

  double TractionViscosity(double t, const Point &x, int sd) const override {
    return originalProblem->TractionViscosity(t, x, sd);
  }

  VectorField Deformation(double time, const Point &x) const override {
    return originalProblem->Deformation(time, x);
  }

  VectorField Velocity(double time, const Point &x) const override {
    return originalProblem->Velocity(time, x);
  }

  VectorField Acceleration(double time, const Point &x) const override {
    return originalProblem->Acceleration(time, x);
  }

  Tensor DeformationGradient(double time, const Point &x) const override {
    return originalProblem->DeformationGradient(time, x);
  }

  double InitialPressure(const Point &x, int bndCondition) const override {
    return originalProblem->InitialPressure(x, bndCondition);
  }

  double Scale() const override {
    return originalProblem->Scale();
  }

  void Scale(double s) const override {
    originalProblem->Scale(s);
  }

  double Pressure(double t, const Point &x, int bndCondition) const override{
    return originalProblem-> Pressure(t, x, bndCondition);
  }

  void Initialize(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    originalProblem->Initialize(dt, t, mechVolumes);
  }

  void Update(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    originalProblem->Update(dt, t, mechVolumes);
  }

  void Finalize(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    originalProblem->Finalize(dt, t, mechVolumes);
  }

  void Iterate(double dt, double t, const std::array<double, 4> &mechVolumes) override {
    originalProblem->Iterate(dt, t, mechVolumes);
  }

  bool Converged(const std::array<double, 4> &mechVolumes) override {
    return originalProblem->Converged(mechVolumes);
  }

  void InitializeEvaluationPoints() override {
    originalProblem->InitializeEvaluationPoints();
  }

  double Potential(double time, const Point &x) const override {
    return originalProblem->Potential(time, x);
  }

  double IonCurrent(double time, const Point &x) const override {
    return originalProblem->IonCurrent(time, x);
  }

  void UpdateDeformation(const Vector &u, const Vector &du, const Vector &v) override {
    if (!transfer) {
      if (contains(v.GetDisc().DiscName(), "MultiPart")) {
        const auto &elphyDisc = (const MultiPartDiscretization &) v.GetDisc();
        defDisc = std::make_shared<MultiPartDiscretization>(elphyDisc, u.GetMesh().dim());
      }
      displacement = new Vector(0.0, defDisc);
      displacement_old = new Vector(0.0, defDisc);
      velocity = std::make_unique<Vector>(0.0, defDisc);
      transfer = GetCardiacTransfer(u, *displacement);
      transfer->Prolongate(u, *displacement);
    }

    (*displacement_old) = (*displacement);
    transfer->Prolongate(u, *displacement);
    transfer->Prolongate(du, *velocity);
  }

  double DJacobian(double dt, const Cell &c) const override {
    Tensor F = One + transfer->Gradient(*displacement, c);
    Tensor FInv = Invert(F);
    return dt * Frobenius(det(F) * transpose(FInv), transfer->Gradient(*velocity, c));
  }

  double Jacobian(const Cell &c) const override {
    return det(One + transfer->Gradient(*displacement, c));
  }

  void PrintMinMaxJacobian(const Vector &v) const override {
    double minJac = 1;
    double maxJac = 1;
    for (cell c = v.cells(); c != v.cells_end(); ++c) {
      double J = Jacobian(*c);
      if (minJac > J) minJac = J;
      if (maxJac < J) maxJac = J;
    }
    mout << "Jac in [" << PPM->Min(minJac) << "," << PPM->Max(maxJac) << "]" << endl;
  }

  double Jacobian_old(const Cell &c) const override {
    return det(One + transfer->Gradient(*displacement_old, c));
  }

  double Jacobian_rate(const Cell &c) const override {
    return Jacobian(c) / Jacobian_old(c);
  }

  Tensor Conductivity(const Cell &c) const override {
    Tensor F = One + transfer->Gradient(*displacement, c);
    Tensor D;
    if (withDeformFibersInConductivity){
      D = originalProblem->Conductivity(c,F);
    }else{
      D = originalProblem->Conductivity(c);
    }
    Tensor FInv = Invert(F);
    return det(F) * FInv * D * transpose(FInv);
  }

  void chamberSpecialization(const std::string &chamberName) const override {
    originalProblem->chamberSpecialization(chamberName);
  }
};

#endif //DEFORMEDPROBLEM_HPP
