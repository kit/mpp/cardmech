#include "CoupledProblem.hpp"
#include "PhysiologicalProblems.hpp"
#include "CoupledBenchmarkProblems.hpp"
#include "DeformedProblem.hpp"
#include "CardiacInterpolation.hpp"

std::unique_ptr<CoupledProblem> GetCoupledProblem() {
  std::string problemName{"UNDEFINED"};
  Config::Get("CoupledProblem", problemName);

  return GetCoupledProblem(problemName);
}

std::unique_ptr<CoupledProblem> undeformedCoupledProblem(
    const std::string &problemName) {
  if (contains(problemName, "ContractingBeam")) {
    return std::make_unique<CoupledBeamProblem>();
  }
  if (contains(problemName, "ContractingVentricle")) {
    return std::make_unique<ContractingVentricleProblem>();
  }
  if (contains(problemName, "BiventricleCoarse")) {
    return std::make_unique<CoarseBiventricleProblem>();
  }
  if (contains(problemName, "BiventricleDirichlet")) {
    return std::make_unique<FineBiventricleProblem>("BiventricleDirichlet");
  }
  if (contains(problemName, "BiventricleTraction")) {
    return std::make_unique<FineBiventricleProblem>("BiventricleTraction");
  }

  auto elphyProblem = GetElphyProblem();
  auto mechProblem = GetElasticityProblem();
  return std::make_unique<CompositeCoupledProblem>(std::move(elphyProblem), std::move(mechProblem));
}

std::unique_ptr<CoupledProblem> GetCoupledProblem(
    const std::string &problemName) {
  if (contains(problemName, "Deformed")) {
    return std::make_unique<DeformedCoupledProblem>(undeformedCoupledProblem(problemName));
  }
  return undeformedCoupledProblem(problemName);
}

#define elphyMeshName this->ElphyProblem::meshesName
#define elphyMeshes this->ElphyProblem::meshes
#define elphyGeo this->ElphyProblem::cGeo

#define mechMeshName this->ElasticityProblem::meshesName
#define mechMeshes this->ElasticityProblem::meshes
#define mechGeo this->ElasticityProblem::cGeo

void CoupledProblem::defaultMeshName(const std::string &name) {
  if (!ElasticityProblem::meshFromConfig()) {
    elphyMeshName = name;
    mechMeshName = name;
  }
}

const std::string &CoupledProblem::ElphyMeshName() const {
  return elphyMeshName;
}

const std::string &CoupledProblem::MechMeshName() const {
  return mechMeshName;
}

void CoupledProblem::CreateElphyMesh(MeshesCreator &creator) {
  {
    elphyGeo = CreateCoarseGeometryShared(elphyMeshName);
    elphyMeshes = creator.WithCoarseGeometry(elphyGeo).CreateShared();
  }
}

void CoupledProblem::CreateMechMesh(MeshesCreator &creator) {
  {
    mechGeo = CreateCoarseGeometryShared(mechMeshName);
    mechMeshes = creator.WithCoarseGeometry(mechGeo).CreateShared();
    InterpolateCellData(*mechMeshes);
  }
}

const CoarseGeometry &CoupledProblem::ElphyDomain() const { return *elphyGeo; }

const Meshes &CoupledProblem::GetElphyMeshes() const { return *elphyMeshes; }

const Mesh &CoupledProblem::GetElphyMesh(int level) const { return (*elphyMeshes)[level]; }

const Mesh &CoupledProblem::GetElphyMesh(MeshIndex level) const { return (*elphyMeshes)[level]; }

const CoarseGeometry &CoupledProblem::MechDomain() const { return *mechGeo; }

const Meshes &CoupledProblem::GetMechMeshes() const { return *mechMeshes; }

const Mesh &CoupledProblem::GetMechMesh(int level) const { return (*mechMeshes)[level]; }

const Mesh &CoupledProblem::GetMechMesh(MeshIndex level) const { return (*mechMeshes)[level]; }

void CoupledProblem::InitializeEvaluationPoints() {

  if (ElphyMeshName() == "TestBiventricleSimpleExcitation_smooth" ||
      ElphyMeshName() == "TestBiventricle") {
    if (ElphyMeshName() == "TestBiventricleSimpleExcitation_smooth") {
      diffusionValues[0] =
          0.001 * VectorField(0.28, 0.182, 0.098); // Wie im Paper von Gerach und Loewe
    }
    evaluationPoints.emplace_back(Point(80.70000000, -39.44620000, -4.76400000));// Apex excited left ventricle
    evaluationPoints.emplace_back(Point(22.5562, -16.1836, 51.2538));// midwall top
    evaluationPoints.emplace_back(Point(54.16030000, -53.92580000, -1.27474000));//right ventricle bottom outer face
    evaluationPoints.emplace_back(Point(32.00800000, -21.68290000, 45.87430000));//inner midwall top
    evaluationPoints.emplace_back(Point(41.38990000, -29.85730000, 31.23020000));//inner midwall lower
    evaluationPoints.emplace_back(Point(47.97770000, -36.15050000, 20.41140000));//inner midwall even lower
    evaluationPoints.emplace_back(Point(56.88530000, -42.66120000, 11.04920000));//inner midwall next apex
    evaluationPoints.emplace_back(Point(87.08160000, -4.69526000, 31.59960000));//left ventricle in wall middle
    evaluationPoints.emplace_back(Point(65.18130000, 13.44020000, 63.81600000));//left ventricle in wall top

    //besidesMesh:
    evaluationPoints.emplace_back(Point(80.50000000, -40.44620000, -5.0));// Apex excited left ventricle
    evaluationPoints.emplace_back(Point(22.3, -16.3, 51.4));// midwall top
    evaluationPoints.emplace_back(Point(53.16030000, -54.92580000, -2.27474000));//right ventricle bottom outer face
    evaluationPoints.emplace_back(Point(33.00800000, -22.68290000, 46.87430000));//inner midwall top
    evaluationPoints.emplace_back(Point(42.38990000, -30.85730000, 32.23020000));//inner midwall lower
    evaluationPoints.emplace_back(Point(48.97770000, -37.15050000, 21.41140000));//inner midwall even lower
    evaluationPoints.emplace_back(Point(57.88530000, -43.66120000, 10.04920000));//inner midwall next apex
    evaluationPoints.emplace_back(Point(86.08160000, -5.69526000, 32.59960000));//left ventricle in wall middle
    evaluationPoints.emplace_back(Point(64.18130000, 12.44020000, 64.81600000));//left ventricle in wall top

    SetNumberOfEvaluationPointsBesideMesh(9);


  } else {
    evaluationPoints.emplace_back(Point(86.75, -44.2302, 1.74331));// Apex excited left ventricle
    evaluationPoints.emplace_back(Point(48.4428, -35.0794, 33.8927));//midwall middle exitation time !=0
    evaluationPoints.emplace_back(Point(22.5562, -16.1836, 51.2538));// midwall top excitation time =0
    evaluationPoints.emplace_back(Point(55.1275, -62.8303, -2.31392));//right ventricle bottom outer face excitation time =0
  }
}

