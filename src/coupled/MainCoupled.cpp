#include "MainCoupled.hpp"
#include "solvers/CellModelSolver.hpp"
#include "solvers/SegregatedSolver.hpp"
#include "solvers/IterativePressureSolver.hpp"

void MainCoupled::Initialize() {
  SelectExternalCurrentFunction();

  coupledProblem = GetCoupledProblem();

  coupledProblem->CreateMeshes(elevel, mlevel);
  coupledProblem->GetElphyMeshes().PrintInfo();
  coupledProblem->GetMechMeshes().PrintInfo();

  tensionModel = GetTensionModel();
  initAssemble();
  generateStartVectors();
}

Vector &MainCoupled::Run() {
  std::string eModelName = "";
  Config::Get("ElphyModel", eModelName);

  // Load ElphySolver from src/electrophysiology/solvers/ElphySolver.cpp
  auto elphySolver = GetElphySolver(eModelName, *tensionModel, elphyA->ExcitationData());

  std::string model{"Segregated"};
  Config::Get("CoupledModel", model);
  if (model == "TensionCellmodels") {
    CellModelSolver solver(*elphyA, *elphySolver);
    solver.Solve(*potential, *stretch);
    return *potential;
  }
  if (model == "Segregated") {
    (*elphyA).PlotIteration(*potential);
    bool prestress{false};
    Config::Get("WithPrestress", prestress);
    if (prestress) {

      // Calculates Prestress at src/elasticity/solvers/IterativePressureSolver.cpp
      CalculatePrestress(*mechA, *displacement);
    }

    // Initializes and runs solver at src/coupled/solvers/SegregatedSolver.cpp
    if (eModelName=="SemiImplicitOnCells"|| eModelName =="SemiImplicitOnCellsSVI"|| eModelName=="SemiImplicitOnCellsIextPerCell"){
      (*mechA).SetIfWithCellValues(true);
      SegregatedSolverOnCells solver(std::move(elphySolver),std::make_unique<ElastodynamicTimeIntegrator>());
      solver.Method(*elphyA, *mechA, *potential, *displacement);
    }else{
      bool withAdaptivity=false;
      Config::Get("AdaptiveTimeStepForMech", withAdaptivity);
      if(withAdaptivity){
        adaptiveSegregatedSolver solver(std::move(elphySolver),std::make_unique<ElastodynamicTimeIntegrator>());
        solver.Method(*elphyA, *mechA, *potential, *displacement);
      }else{
        SegregatedSolver solver(std::move(elphySolver),std::make_unique<ElastodynamicTimeIntegrator>());
        solver.Method(*elphyA, *mechA, *potential, *displacement);
      }
    }


  }

  return *displacement;
}

void MainCoupled::generateStartVectors() {
  potential = std::make_unique<Vector>(elphyA->GetSharedDisc());
  stretch = std::make_unique<Vector>(elphyA->GetSharedDisc());
  displacement = std::make_unique<Vector>(0.0, mechA->GetSharedDisc());

  // Gets initial value of cellmodel at src/cellmodesl/*.cpp
  *potential = tensionModel->InitialValue(ELPHY);
  *stretch = tensionModel->InitialValue(TENSION);
  coupledProblem->SetEvaluationCells(*potential);
}

void MainCoupled::SetDisplacement(bool exact, Vector &u) const {
  double d = u.GetMesh().dim();
  if (exact) {
    for (row r = u.rows(); r != u.rows_end(); r++) {
      VectorField phi = coupledProblem->Deformation(mechA->LastTStep(), r());
      for (int i = 0; i < d; i++)
        u(r, i) = phi[i];
    }
  } else {
    for (row r = u.rows(); r != u.rows_end(); r++) {
      for (int i = 0; i < d; i++)
        u(r, i) = (*displacement)(r, i);
    }
  }
}


Vector &MainCoupled::GetPotential() const {
  return *potential;
}

Vector &MainCoupled::GetDisplacement() const {
  return *displacement;
}

void MainCoupled::initAssemble() {
  // Creates Elphy Assemble at src/electrophysiology/assemble/IElphyAssemble.cpp
  elphyA = CreateElphyAssemble(*coupledProblem, *tensionModel, elphyDegree);

  // Creates Mech Assemble at src/elasticity/assemble/IElasticity.cpp
  mechA = CreateFiniteElasticityAssemble(*coupledProblem, mechDegree, false);


  // Creates Mech Assemble at src/MainCardMech.hpp
  auto times = ReadTimeFromConfig("Mech");
  elphyA->ResetTime(times[0], times[1], times[2]);
  mechA->ResetTime(times[0], times[1], times[2]);
}

double MainCoupled::EvaluateQuantity(const Vector &solution, const std::string &quantity) const {
  if (quantity == "L2") { return mechA->L2Error(solution); }
  if (quantity == "H1") { return mechA->H1Error(solution); }
  else { return infty; }
}


std::vector<double> MainCoupled::Evaluate(const Vector &solution) {
  if (verbose > 0) {
  mout << "\n" << "==== Monodomain Information ============== \n\n";

  coupledProblem->GetElphyMeshes().PrintInfo();

  std::string elphyModelClass = "IBTElphyModel";
  Config::Get("ElphyModelClass", elphyModelClass);
  std::string cmName = "FitzHughNagumo";
  if (elphyModelClass == "IBTElphyModel") {
    Config::Get("IBTVentricleModel", cmName);
  } else {
    Config::Get("ElphyModelName", cmName);
  }

  std::string IextSmooth;
  Config::Get("ExternalCurrentSmoothingInTime", IextSmooth);
  std::string IextSpaceSmoothing;
  Config::Get("ExternalCurrentSmoothingInSpace", IextSpaceSmoothing);
  std::string reassembleRHSOnce = "";
  std::string splittingmethod = "";
  std::string order = "";
  Config::Get("OrderStaggeredScheme", order);
  order = " " + order;
  if (modelName == "Splitting") {
    Config::Get("ElphySplittingMethod", splittingmethod);
    splittingmethod = splittingmethod + " ";
    order = "";
  }
  Config::Get("ReassembleRHSonce", reassembleRHSOnce);
  if (reassembleRHSOnce == "1") {
    reassembleRHSOnce = " reassembled once";
  } else {
    reassembleRHSOnce = "";
  }
  std::string elphyModelName;
  Config::Get("ElphyModel",elphyModelName);

    std::string strAtriaDiff = "[" + std::to_string(coupledProblem->getDiffusionValues(1, 0)) +
                               ", " + std::to_string(coupledProblem->getDiffusionValues(1, 1)) +
                               ", " + std::to_string(coupledProblem->getDiffusionValues(1, 2)) +
                               "]";
    std::string strVentricularDiff =
        "[" + std::to_string(coupledProblem->getDiffusionValues(0, 0)) +
        ", " + std::to_string(coupledProblem->getDiffusionValues(0, 1)) +
        ", " + std::to_string(coupledProblem->getDiffusionValues(0, 2)) + "]";
    mout.PrintInfo("Assemble", 1,
                   PrintInfoEntry("Name", std::string("Monodomain").c_str()),
                   PrintInfoEntry("Problem", coupledProblem->Name()),
                   PrintInfoEntry("Cells",
                                  PPM->Sum(coupledProblem->GetElphyMeshes().fine().CellCount())),
                   PrintInfoEntry("Degrees of Freedom:", solution.pSize()));

    mout.PrintInfo("Model and Solver", verbose,
                   PrintInfoEntry("CellModelClass", elphyModelClass, 1),
                   PrintInfoEntry("CellModel", cmName, 1),
                   PrintInfoEntry("dt", elphyA->StepSize(),
                                  1),
                   PrintInfoEntry("T", elphyA->LastTStep(),
                                  1),
                   PrintInfoEntry("Time Integration model", std::string(
                       splittingmethod + elphyModelName + reassembleRHSOnce + order).c_str(), 1));


    std::string inPDE = "true";
    Config::Get("IextInPDE", inPDE);
    mout.PrintInfo("External Stimulus", verbose,
                   PrintInfoEntry("Iext in PDE", inPDE, modelName == "Splitting" ? 1 : 1000),
                   PrintInfoEntry("Iext smooth in time", IextSmooth, 1),
                   PrintInfoEntry("Iext space smoothing", IextSpaceSmoothing,
                                  1));
    mout.PrintInfo("Conductivity", verbose,
                   PrintInfoEntry("atria", strAtriaDiff, 1),
                   PrintInfoEntry("ventricles", strVentricularDiff,
                                  1));

    std::string problemEval = coupledProblem->EvaluateElphy(solution);
    mout << problemEval << endl;
    mout << "===========================================" << endl;
  }



  if (verbose > 0) {
    mout << "\n" << "=================== Elasticity Information ================== \n\n";
    Vector exactSolution(solution);
    SetDisplacement(true, exactSolution);

    solution.GetMesh().PrintInfo();
    std::string m = "Passive";
    Config::Get("MechDiscretization", m);

    mout.PrintInfo("Assemble", verbose,
                   PrintInfoEntry("Name", m + "Elasticity", 1),
                   PrintInfoEntry("ElasticityProblem", coupledProblem->Name(), 1),
                   PrintInfoEntry("Material",
                                  coupledProblem->GetMaterialName(true), 1),
                   PrintInfoEntry("Degrees of Freedom:", solution.pSize(), 1)/*,
                   PrintInfoEntry("Matrix Entries", solution.pMatrixSize(), 2)*/);

    mout.PrintInfo("Solution Metrics", verbose,
                   PrintInfoEntry("Norm", norm(solution), 1),
        //PrintInfoEntry("Pressure Norm", mechA->PressureBndNorm(solution), 1),
                   PrintInfoEntry("L2 Norm", mechA->L2Norm(solution), 1),
                   PrintInfoEntry("L2 Cell-Avg Norm", mechA->L2AvgNorm(solution), 1),
                   PrintInfoEntry("H1 Norm", mechA->H1Norm(solution), 1),
                   PrintInfoEntry("Energy Norm", mechA->EnergyNorm(solution), 1)
    );

    mout.PrintInfo("Exact Solution Metrics", verbose,
        //PrintInfoEntry("Energy", mechA->Energy(solution), 1),
                   PrintInfoEntry("Interpolation Error", mechA->L2Error(exactSolution), 2),
                   PrintInfoEntry("L2 Error", mechA->L2Error(solution), 2),
                   PrintInfoEntry("H1 Error", mechA->H1Error(solution), 2),
                   PrintInfoEntry("Energy Error", mechA->EnergyError(solution), 2)
    );


    auto volume = mechA->ChamberVolume(solution);

    mout.PrintInfo("Volumes", verbose,
                   PrintInfoEntry("Initial Geometry Volume",
                                  mechA->InitialVolume(solution.GetMesh()), 1),
                   PrintInfoEntry("Deformed Geometry Volume", mechA->DeformedVolume(solution), 1),
                   PrintInfoEntry("Left Ventricle Volume", volume[0], 2),
                   PrintInfoEntry("Right Ventricle Volume", volume[1], 2),
                   PrintInfoEntry("Left Atria Volume", volume[2], 2),
                   PrintInfoEntry("Right Atria Volume", volume[3], 2));
  }
  mout << endl << "=========== Problem Information =============================" << endl << endl;
  auto result = coupledProblem->EvaluationResultsMech(solution);
  mout << endl << "=============================================================" << endl << endl;
  return result;

}

void MainCoupled::ResetLevel(int newLevel) {
  elevel = newLevel;
  mlevel = newLevel;
}