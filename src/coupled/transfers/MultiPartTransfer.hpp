#ifndef MULTIPARTTRANSFER_HPP
#define MULTIPARTTRANSFER_HPP

#include "ITransfer.hpp"
#include "Rowlist.hpp"

#include "Tensor.hpp"

#include "MultiPartDiscretization.hpp"
#include "LagrangeDiscretization.hpp"

double checkRefinement(std::pair<int, int> degrees, std::pair<int, int> levels);

class MultiPartDeformationTransfer {
public:
  MultiPartDeformationTransfer() {};

  virtual void Prolongate(const Vector &coarse, Vector &fine) const = 0;
  virtual void ProlongateCell(const Vector &coarse, Vector &fine) const=0;

  virtual void ProlongateTransposed(Vector &coarse, const Vector &fine) const = 0;

  virtual void Restrict(Vector &coarse, const Vector &fine) const = 0;

  virtual void Project(Vector &coarse, const Vector &fine) const = 0;
  virtual void ProjectCells(Vector &coarse, const Vector &fine) const = 0;

  virtual RMatrix AsMatrix() const = 0;

  virtual std::unique_ptr<Vector> CreateFineDeformation() const = 0;

  VectorField Value(const Vector &u, const Cell &c) const {
    return Value(u, c, c());
  };

  VectorField Value(const Vector &u, const Cell &c, const Point &x) const;

  Tensor Gradient(const Vector &u, const Cell &c) const {
    return Gradient(u, c, c());
  };

  Tensor Gradient(const Vector &u, const Cell &c, const Point &x) const;

};

class MultiPartLagrangeTransfer : public MultiPartDeformationTransfer {
  int mechDim{1};
  int elphyDim{1};

  std::pair<std::shared_ptr<LagrangeDiscretization>, std::shared_ptr<MultiPartDiscretization>> discs{};
  RowList rowList;

  void constructRowList(const Vector &coarse, const Vector &fine);

public:
  MultiPartLagrangeTransfer(const Vector &coarse, const Vector &fine);

  void Prolongate(const Vector &coarse, Vector &fine) const override;
  void ProlongateCell(const Vector &coarse, Vector &fine) const override;

  void ProlongateTransposed(Vector &coarse, const Vector &fine) const override;

  void Restrict(Vector &coarse, const Vector &fine) const override;

  void Project(Vector &coarse, const Vector &fine) const override;
  void ProjectCells(Vector &coarse, const Vector &fine) const override;

  [[nodiscard]] RMatrix AsMatrix() const override;

  [[nodiscard]] std::unique_ptr<Vector> CreateFineDeformation() const override;

};

std::unique_ptr<MultiPartDeformationTransfer>
GetCardiacTransfer(const Vector &coarse, const Vector &fine);

#endif //MULTIPARTTRANSFER_HPP
