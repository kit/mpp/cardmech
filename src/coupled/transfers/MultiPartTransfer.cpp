#include "MultiPartTransfer.hpp"
#include "LinearTransfer.hpp"
#include "MultiPartVectorFieldElement.hpp"
#include "CardiacData.hpp"


VectorField MultiPartDeformationTransfer::Value(
    const Vector &u, const Cell &c, const Point &x) const {
  MultiPartVectorFieldElement elem(u, c, DomainPart(c));
  return elem.VectorValue(x, u);
}

Tensor MultiPartDeformationTransfer::Gradient(
    const Vector &u, const Cell &c, const Point &x) const {
  MultiPartVectorFieldElement elem(u, c, DomainPart(c));
  return elem.VectorGradient(x, u);
}

double checkRefinement(std::pair<int, int> degrees,
                       std::pair<int, int> levels) {
  int dlevel = (int) log2(degrees.first);
  if (dlevel < log2(degrees.first)) {
    THROW("degree " + std::to_string(degrees.first) + " transfer not implemented")
  }
  if (((int) log2(degrees.first)) + levels.second < dlevel + levels.first) {
    THROW("Can't interpolate: Coarser mesh is finer than fine mesh.")
  }

  return pow(2, levels.second - (levels.first + dlevel)) * degrees.second;
}


void MultiPartLagrangeTransfer::constructRowList(
    const Vector &coarse, const Vector &fine) {
  int dlevel = (int) log2(discs.first->Degree());
  double lvl = checkRefinement({discs.first->Degree(), discs.second->Degree()},
                               {coarse.SpaceLevel(), fine.SpaceLevel()});

  std::vector<int> coarseSD = coarse.GetMesh().IncludedSubdomains();
  std::vector<int> fineSD = fine.GetMesh().IncludedSubdomains();
  std::vector<int> intersectSD;
  std::set_intersection(coarseSD.begin(), coarseSD.end(), fineSD.begin(), fineSD.end(),
                        std::back_inserter(intersectSD));

  std::vector<Point> childrenVerteces{};
  std::vector<Point> childVerteces{};
  std::vector<Point> coarseNodalPoints{};
  for (cell c = coarse.cells(); c != coarse.cells_end(); ++c) {
    if (std::find(intersectSD.begin(), intersectSD.end(), c.Subdomain()) ==
        intersectSD.end()) {
      continue;
    }

    std::vector<std::vector<std::unique_ptr<Cell>>> recursiveCells(dlevel + 1);
    recursiveCells[0].emplace_back(CreateCell(c.Type(), c.Subdomain(), c.AsVector(), false));
    for (int l = 1; l <= dlevel; ++l) {
      for (const auto &parent: recursiveCells[l - 1]) {
        const std::vector<Rule> &R = parent->Refine(childrenVerteces);
        for (int i = 0; i < R.size(); ++i) {
          R[i](childrenVerteces, childVerteces);
          recursiveCells[l].emplace_back(
              CreateCell(R[i].type(), c.Subdomain(), childVerteces, false));
        }
      }

    }
    for (const auto &child: recursiveCells[dlevel]) {
      LagrangeNodalPoints(*child, coarseNodalPoints, 1);
      std::vector<int> coarseIds = mpp::nodalIds(coarse, coarseNodalPoints);
      mpp::linearTransfer(child->ReferenceType(), coarseNodalPoints, coarseIds, lvl, fine, rowList);
    }
  }
}


MultiPartLagrangeTransfer::MultiPartLagrangeTransfer(const Vector &coarse, const Vector &fine)
    : MultiPartDeformationTransfer(), rowList(coarse, fine) {
  auto &mechDisc = (const LagrangeDiscretization &) coarse.GetDisc();
  auto &elphyDisc = (const MultiPartDiscretization &) fine.GetDisc();

  mechDim = mechDisc.Size();
  elphyDim = elphyDisc.Size();

  discs = std::pair<std::shared_ptr<LagrangeDiscretization>, std::shared_ptr<MultiPartDiscretization>>{
      std::make_shared<LagrangeDiscretization>(mechDisc, mechDisc.Degree(), mechDisc.Size()),
      std::make_shared<MultiPartDiscretization>(elphyDisc, mechDisc.Size()),
  };

  constructRowList(coarse, fine);
}


void MultiPartLagrangeTransfer::Prolongate(const Vector &coarse, Vector &fine) const {
  if (coarse.GetDisc().DiscName() != discs.first->DiscName() ||
      fine.GetDisc().DiscName() != discs.second->DiscName()) {
    THROW("Discretizations of transfer and vectors do not match")
  }

  fine = 0.0;
  for (int i = 0; i < rowList.IdSize(); ++i) {
    int n = fine.Dof(i);
    int scale = n / elphyDim;
    for (int j = 0; j < n; ++j) {
      fine(i, j) = rowList.CalculateFineEntry(i, coarse, j / scale);
    }
  }
  fine.ClearDirichletValues();
}
void MultiPartLagrangeTransfer::ProlongateCell(const Vector &coarse, Vector &fine)const{
  const Meshes &M_fine = fine.GetMeshes();
  const Meshes &M_coarse = coarse.GetMeshes();
  int fineLevel=M_fine.Level();
  int coarseLevel=M_coarse.Level();
  auto discFine = std::make_shared<LagrangeDiscretization>(M_fine, 0, 1);
  fine = 0.0;
  if(fineLevel!=coarseLevel){
    std::unique_ptr<Vector> fineNew;
    std::unique_ptr<Vector> coarseFinePerStep;
    coarseFinePerStep=std::make_unique<Vector>(discFine,coarseLevel);
    for (cell c=(*coarseFinePerStep).cells(); c != (*coarseFinePerStep).cells_end(); ++c) {
      (*coarseFinePerStep)(c(),0) = coarse(c(),0);
    }

    for(int l =coarseLevel; l<fineLevel;l++){
      fineNew=std::make_unique<Vector>(discFine,l+1);
      for (cell c = (*coarseFinePerStep).cells(); c != (*coarseFinePerStep).cells_end(); ++c) {
        for (int j =0;j<c.Children();j++){
          (*fineNew)(c.Child(j),0)=(*coarseFinePerStep)(c(),0);
        }
      }
      coarseFinePerStep=std::make_unique<Vector>(discFine,l+1);
      (*coarseFinePerStep)=(*fineNew);
    }
    fine=(*fineNew);
  }else{
    for(cell c = fine.cells(); c != fine.cells_end(); ++c) {
      fine(c(),0)=coarse(c(),0);
    }
  }

  fine.ClearDirichletValues();
}

void MultiPartLagrangeTransfer::ProlongateTransposed(Vector &coarse, const Vector &fine) const {
  if (coarse.GetDisc().DiscName() != discs.first->DiscName() ||
      fine.GetDisc().DiscName() != discs.second->DiscName()) {
    THROW("Discretizations of transfer and vectors do not match")
  }

  coarse = 0.0;
  for (int i = 0; i < rowList.IdSize(); ++i) {
    for (const auto &cRow: rowList.CoarseRows(i)) {
      for (int j = 0; j < fine.Dof(i); ++j) {
        coarse(cRow.Id, (j * elphyDim) / fine.Dof(i)) += cRow.Weight * fine(i, j) / fine.Dof(i);
      }
    }
  }
  coarse.ClearDirichletValues();
  coarse.Collect();
}


void MultiPartLagrangeTransfer::Restrict(Vector &coarse, const Vector &fine) const {
  if (coarse.GetDisc().DiscName() != discs.first->DiscName() ||
      fine.GetDisc().DiscName() != discs.second->DiscName()) {
    THROW("Discretizations of transfer and vectors do not match")
  }

  coarse = 0.0;
  for (int i = 0; i < rowList.IdSize(); ++i) {
    for (const auto &cRow: rowList.CoarseRows(i)) {
      for (int j = 0; j < fine.Dof(i); ++j) {
        coarse(cRow.Id, (j * elphyDim) / fine.Dof(i)) +=
            cRow.Weight / rowList.CoarseWeight(cRow.Id) * fine(i, j) / fine.Dof(i);
      }
    }
  }
  coarse.ClearDirichletValues();
}


void MultiPartLagrangeTransfer::Project(Vector &coarse, const Vector &fine) const {
  if (coarse.GetDisc().DiscName() != discs.first->DiscName() ||
      fine.GetDisc().DiscName() != discs.second->DiscName()) {
    THROW("Discretizations of transfer and vectors do not match")
  }

  coarse = 0.0;
  for (row r = coarse.rows(); r != coarse.rows_end(); ++r) {
    row fineR = fine.find_row(r());
    if (fineR == fine.rows_end()) continue;

    double scale = ((double) elphyDim) / ((double) fineR.NumberOfDofs());
    for (int j = 0; j < fineR.NumberOfDofs(); ++j) {
      coarse(r, (j * elphyDim) / fineR.NumberOfDofs()) += fine(fineR, j) * scale;
    }
  }
  //coarse.ClearDirichletValues();
}
void MultiPartLagrangeTransfer::ProjectCells(Vector &coarse, const Vector &fine) const {

  /*if (coarse.GetDisc().DiscName() != discs.first->DiscName() ||
      fine.GetDisc().DiscName() != discs.second->DiscName()) {
    THROW("Discretizations of transfer and vectors do not match")
  }*/

  const Meshes &M_fine = fine.GetMeshes();
  const Meshes &M_coarse = coarse.GetMeshes();
  int fineLevel=M_fine.Level();
  int coarseLevel=M_coarse.Level();

  coarse = 0.0;
  if(fineLevel!=coarseLevel){
    auto discFine = std::make_shared<LagrangeDiscretization>(M_fine, 0, 1);
    auto discCoarse = std::make_shared<LagrangeDiscretization>(M_coarse, 0, 1);

    std::unique_ptr<Vector> finePerStep;
    std::unique_ptr<Vector> fineCoarsePerStep;
    finePerStep=std::make_unique<Vector>(discFine,fineLevel);
    (*finePerStep)=fine;
    for(int l =fineLevel; l>coarseLevel;l--){
      fineCoarsePerStep=std::make_unique<Vector>(discFine,l-1);
      for (cell c = (*fineCoarsePerStep).cells(); c != (*fineCoarsePerStep).cells_end(); ++c) {
        for (int i = 0; i < c.Children(); ++i) {
            (*fineCoarsePerStep)(c(),0)+=(*finePerStep)(c.Child(i),0);
        }
        (*fineCoarsePerStep)(c(),0)*=1.0/c.Children();
     }
      finePerStep=std::make_unique<Vector>(discFine,l-1);
      (*finePerStep)=(*fineCoarsePerStep);
   }
    for (cell c = (*fineCoarsePerStep).cells(); c != (*fineCoarsePerStep).cells_end(); ++c) {
      coarse (c(),0)= (*fineCoarsePerStep)(c(),0);
    }
  }else{
    for(cell c = fine.cells(); c != fine.cells_end(); ++c) {
    coarse(c(),0)=fine(c(),0);
    }
  }

  //coarse.ClearDirichletValues();
}

RMatrix MultiPartLagrangeTransfer::AsMatrix() const {
  RMatrix transferMatrix((int) rowList.IdSize(), (int) rowList.WeightSize());
  for (int i = 0; i < rowList.IdSize(); ++i) {
    for (auto r: rowList.CoarseRows(i)) {
      transferMatrix(i, r.Id) = r.Weight;
    }
  }
  return transferMatrix;
}

std::unique_ptr<Vector> MultiPartLagrangeTransfer::CreateFineDeformation() const {
  return std::make_unique<Vector>(0.0, discs.second);
}

std::unique_ptr<MultiPartDeformationTransfer>
GetCardiacTransfer(const Vector &coarse, const Vector &fine) {
  const auto &coarseType = typeid(coarse.GetDisc());
  const auto &fineType = typeid(fine.GetDisc());

  if (fineType != typeid(MultiPartDiscretization)) {
    THROW("Fine vector should be electrophysiology discretization")
  }

  if (coarseType == typeid(LagrangeDiscretization)) {
    return std::make_unique<MultiPartLagrangeTransfer>(coarse, fine);
  }

  THROW("Transfer type not implemented")
}