#ifndef MAINCOUPLED_H
#define MAINCOUPLED_H

#include "MainCardMech.hpp"
#include "problem/CoupledProblem.hpp"

#include "elasticity/assemble/IElasticity.hpp"
#include "electrophysiology/assemble/IElphyAssemble.hpp"

#include "electrophysiology/solvers/ElphySolver.hpp"


#include <cellmodels/solvers/MTensionSolver.hpp>

/**
 * Represents the body of the coupled code, which is called if the coupled/m++.conf is loaded.
 * This combines electrophysiology and elasticity.
 */
class MainCoupled : public MainCardMech {
  int elphyDegree{1};
  int mechDegree{1};

  std::unique_ptr<MTensionModel> tensionModel = nullptr;
  std::unique_ptr<CoupledProblem> coupledProblem = nullptr;

  std::unique_ptr<IElphyAssemble> elphyA = nullptr;
  std::unique_ptr<IElasticity> mechA = nullptr;

  void initAssemble();

  std::unique_ptr<Vector> potential = nullptr;
  std::unique_ptr<Vector> stretch = nullptr;
  std::unique_ptr<Vector> activationTime = nullptr;
  std::unique_ptr<Vector> displacement = nullptr;

  void generateStartVectors();

public:
  int plevel = 0, mlevel = 0, elevel = 0;

  MainCoupled() {
    Config::Get("Model", modelName);
    Config::Get("plevel", plevel);
    Config::Get("ElphyLevel", elevel);
    Config::Get("ElphyPolynomialDegree", elphyDegree);
    Config::Get("MechLevel", mlevel);
    Config::Get("MechPolynomialDegree", mechDegree);
  }

  void ResetLevel(int newLevel);

  void Initialize() override;

  Vector &Run() override;

  std::vector<double> Evaluate(const Vector &solution) override;

  Vector &GetDisplacement() const;

  Vector &GetPotential() const;

  void SetDisplacement(bool exact, Vector &u) const;

  double EvaluateQuantity(const Vector &solution,
                          const std::string &quantity) const;
};

#endif //MAINCOUPLED_H
