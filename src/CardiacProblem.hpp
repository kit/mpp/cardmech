#ifndef CARDIACPROBLEM_HPP
#define CARDIACPROBLEM_HPP

#include <string>

static constexpr int PRESSURE_BC_LV = 230;
static constexpr int PRESSURE_BC_RV = 231;
static constexpr int PRESSURE_BC_LA = 232;
static constexpr int PRESSURE_BC_RA = 233;

static constexpr int ROBIN_BC_EPI = 330;
static constexpr int ROBIN_BC_BASE = 331;

static std::pair<int, bool> isIn(const Point &p,
                                 const std::vector<Point> &pointList) {
  for (int i = 0; i < pointList.size(); ++i) {
    if (p.isNear(pointList[i], 1e-4)) {
      return {i, true};
    }
  }
  return {-1, false};
}

class Vector;
class [[deprecated("Is replaced by IProblem from core")]] CardiacProblem {
  const std::string name{};

protected:
  std::string meshName{""};
  int verbose{-1};

  bool meshFromConfig() const {return meshName != "";}
public:
  virtual ~CardiacProblem() = default;

  explicit CardiacProblem(std::string problemName, const std::string &verbosePrefix = "") : name(
      std::move(problemName)) {
    Config::Get(verbosePrefix + "ProblemVerbose", verbose);
    Config::Get("Mesh", meshName);
  }


  const std::string &Name() const { return name; }

  virtual std::string MeshName() const { return meshName; }

  virtual std::string Evaluate(const Vector &solution) const { return ""; };

  virtual std::vector<std::string> EvaluationMetrics() const {return {};}

  virtual std::vector<double>
  EvaluationResults(const Vector &solution) const { return std::vector<double>{}; };
};

#endif //CARDIACPROBLEM_HPP
