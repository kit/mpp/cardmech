#ifndef CARDIACINTERPOLATION_HPP
#define CARDIACINTERPOLATION_HPP

#include "CardiacData.hpp"

void InterpolateCellData(const Meshes &meshes);

/**
 * Creates a vector containing interpolated data of the coarse
 * mesh on the used discretization
 * @param disc Discretization which is used by the assemble requesting the data
 * @return A vector containing interpolated vertex data
 */
Vector InterpolateMeshData(std::shared_ptr<const IDiscretization> disc);


#endif //CARDIACINTERPOLATION_HPP
