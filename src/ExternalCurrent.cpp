#include "ExternalCurrent.hpp"

#include <numbers>

#include "GlobalDefinitions.hpp"

std::function<double(double,double,double,double)> excitationFunc=IextStepfct;

double IextStepfct(double t, double amp, double exc, double dur) {
  if(exc+dur>=t && t>=exc){
    return amp;
  }else{
    return 0.0;
  }
}

double IextTanh(double t, double amp, double exc, double dur) {
  return ((1+tanh(4.0*(t-exc)))/2) - ((1+tanh(4.0*(t-(exc + dur))))/2)*amp +amp-1;
}
double IextArcTan(double t, double amp, double exc, double dur) {
  double start_t = 4 * (t -  exc);
  double end_t = 4 * (t - (exc + dur));
  return (atan(start_t) - atan(end_t)) / std::numbers::pi * amp;
}

void SelectExternalCurrentFunction(const std::string &externalCurrentFunction) {
  if (externalCurrentFunction == "Arctan") {
    excitationFunc = IextArcTan;
  }else if (externalCurrentFunction=="Tanh"){
    excitationFunc = IextTanh;
  } else {
    excitationFunc =  IextStepfct;
  }
}

void SelectExternalCurrentFunction() {
  std::string s = "Stepfunction";
  Config::Get("ExternalCurrentSmoothingInTime", s);
  return SelectExternalCurrentFunction(s);
}




