#ifndef CARDIACSOLVER_HPP
#define CARDIACSOLVER_HPP

#include <string>

/// Default Wrapper for all solvers to provide verbose and vtk values
class CardiacSolver {
protected:
  int verbose{-1};
  int vtk{-1};
  std::string plotName{"U"};
public:
  void SetVerbose(int v) { verbose = v; }

  int Verbose() const { return verbose; }

  void SetVtk(int v) { vtk = v; }

  int Vtk() const { return vtk; }
};

#endif //CARDIACSOLVER_HPP
