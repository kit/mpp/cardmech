set(CARDMECH
        CardiacInterpolation.cpp
        CardiacData.cpp
        ExternalCurrent.cpp
        )

add_library(CARDMECH_LIBRARIES STATIC ${CARDMECH})
target_link_libraries(CARDMECH_LIBRARIES MPP_LIBRARIES)

include_directories(cellmodels)
add_subdirectory(cellmodels)

include_directories(electrophysiology)
add_subdirectory(electrophysiology)

include_directories(elasticity)
add_subdirectory(elasticity)

include_directories(coupled)
add_subdirectory(coupled)
