#include "ScalarElement.hpp"
#include "MultiPartScalarElement.hpp"
#include "MonodomainSplitting.hpp"
#include "VectorFieldElement.hpp"
#include "ExternalCurrent.hpp"

MonodomainSplitting::MonodomainSplitting(ElphyProblem &elphyProblem, int degree,
                                         int exactUpTo)
    : IElphyAssemble(elphyProblem, degree, exactUpTo), theta(0.5), beta(0.0) {
  Config::Get("IextInPDE", iextInPDE);
  InitializeMonodomainParameters();

}

void MonodomainSplitting::InitializeMonodomainParameters() {
  Config::Get("CrankNicolsonTheta", theta);
  Config::Get("SurfaceToVolumeRatio", beta);
  Config::Get("MembraneCapacitance", C_m);
  //Divide for correct Units from microF to F
  C_m = C_m / 1000000.0;
}
void MonodomainSplitting::setTheta(double value){
  theta=value;

}

void MonodomainSplitting::updateExternalCurrent(const Vector &V) {
  vout(10).StartBlock("update Iext");
  if(!isNormIextZero) {
    double tNew = Time()+timeUpdateIext;
    for (row r = Iext->rows(); r != Iext->rows_end(); ++r) {
      for (int j = 0; j < r.NumberOfDofs(); ++j) {
        (*Iext)(r, j) = (1 - theta) * excitationFunc(1000 * Time(), Amplitude(excData, r, j),
                                                     1000 * Excitation(excData, r, j),
                                                     1000 * Duration(excData, r, j));
        (*Iext)(r, j) += theta * excitationFunc(1000 * tNew, Amplitude(excData, r, j),
                                                1000 * Excitation(excData, r, j),
                                                1000 * Duration(excData, r, j));

      }
    }
    //PrintL2Norm(*Iext, "Iext");
    if ((*Iext).norm() == 0) {
      isNormIextZero = true;
    }
  }
  vout(10).EndBlock();
}


void MonodomainSplitting::SystemMatrix(const cell &c, const Vector &V, Matrix &A) const {
  MultiPartScalarElement E(V, *c,DomainPart(*c));
  PartRowEntries A_c(A, E, DomainPart(*c));
  double J = problem.Jacobian(*c);
  Tensor S = problem.Conductivity(*c);
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      double V_j = E.Value(q, j);
      VectorField DV_j = E.Derivative(q, j);
      for (int i = 0; i < E.size(); ++i) {
        double V_i = E.Value(q, i);
        VectorField DV_i = E.Derivative(q, i);
        // Crank-Nicolson Scheme
        A_c(i, j) += w * (J*V_i * V_j + J*theta * (StepSize() / (beta * C_m)) * DV_i * (S * DV_j));
      }
    }
  }

}

void MonodomainSplitting::RHSMAT(const cell &c, const Vector &V, Matrix &R) const {
  MultiPartScalarElement E(V, *c,DomainPart(*c));
  PartRowEntries R_c(R, E, DomainPart(*c));
  Tensor S = problem.Conductivity(*c);

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      double V_j = E.Value(q, j);
      VectorField DV_j = E.Derivative(q, j);
      for (int i = 0; i < E.size(); ++i) {
        double V_i = E.Value(q, i);
        VectorField DV_i = E.Derivative(q, i);
        // Crank-Nicolson Scheme
        R_c(i, j) +=
            w * (V_i * V_j - (1 - theta) * (StepSize() / (beta * C_m)) * DV_i * (S * DV_j));
      }
    }
  }
}

void MonodomainSplitting::RHS(const cell &c, const Vector &V, Vector &r) const {
  MultiPartScalarElement E(V, *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  double J = problem.Jacobian(*c);
  double J_old = problem.Jacobian_old(*c);
  Tensor S = problem.Conductivity(*c);
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    Point xi = E.QPoint(q);

    for (int j = 0; j < E.size(); ++j) {
      double v = E.Value(q, V);
      VectorField S_Dv = S * E.Derivative(q, V);

      double v_j = E.Value(q, j);
      VectorField Dv_j = E.Derivative(q, j);
      r_c(j) += w * (J_old*v * v_j -J_old* (1.0 / (beta * C_m)) * (1 - theta) * StepSize() * S_Dv * Dv_j);
      if (iextInPDE) {
        double iext=0.0;
        if(!isNormIextZero){
          iext = E.Value(q, *Iext, E.IndexPart(j));
        }
        r_c(j) += w * (J*StepSize() * 1000.0 * iext * v_j);
      }
      r_c(j) -= w * (problem.IonCurrent(Time(), xi) * v_j);

    }
  }
}

void MonodomainSplitting::SetInitialValue(Vector &u) {
  IElphyAssemble::SetInitialValue(u);
  if (iextInPDE) {
    updateExternalCurrent(u);
  }
}



