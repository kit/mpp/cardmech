#include "IElphyAssemble.hpp"
#include <MultiPartScalarElement.hpp>
#include <MultiPartScalarWGElement.hpp>
//#include <VtuPointPlot.hpp>
#include "VectorFieldElement.hpp"
#include "materials/materials.hpp"

#include "CardiacInterpolation.hpp"
#include "MCellModel.hpp"

#include <fstream>
#include <iostream>

IElphyAssemble::IElphyAssemble(ElphyProblem &elphyProblem, int degree, int exactUpTo)
    : INonLinearTimeAssemble(), ICardiacAssemble("ElphyVTK"), problem(elphyProblem), degree(degree),
      disc(std::make_shared<const MultiPartDiscretization>(elphyProblem.GetMeshes(), degree, exactUpTo < 0 ? 2 * degree : exactUpTo, DomainPart, 1)),
      dataDisc(std::make_shared<const MultiPartDiscretization>(elphyProblem.GetMeshes(), degree, exactUpTo < 0 ? 2 * degree : exactUpTo, DomainPart,
               3)),
      excData(std::move(InterpolateMeshData(dataDisc))) {
  Config::Get("ElphyVerbose", verbose);
  Config::Get("IsExternalCurrentSmooth", iextContinuous);
  Config::Get("ThresholdVForActivation", Vthresh);
  Config::Get("PrintVSteps", printVSteps);
  Config::Get("StartTime", startTime);

  timeUpdateIext=StepSize();
  bool newupdateIext=true;
  Config::Get("UseTNewForIextUpdate", newupdateIext);
  if(!newupdateIext){
    timeUpdateIext=0.0;
  }
}

void IElphyAssemble::IExt(const cell &c, const Vector &u, Vector &r) const {
  MultiPartScalarElementWG E(u, *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    for (int j = 0; j < E.size(); ++j) {
      double v_j = E.Value(q, j);
      double iext = E.Value(q, *Iext);
      r_c(j) += w * (StepSize() * 1000.0 * iext * v_j);
    }
  }
}


void IElphyAssemble::updateActivationTime(const Vector &V, double time) {
  //(auto &actTime = *activationTime;
  if (problem.UpdateActivationTime) {
    for (row r = V.rows(); r != V.rows_end(); ++r) {
      if ((*activationTime)(r, 0) < 0.0) {
        if (V(r, 0) >= Vthresh) {
          (*activationTime)(r, 0) = time * 1000.0;
        }
      }
    }
  }
}

void IElphyAssemble::PrintInftyNormOnCells(const Vector &V, const std::string &name) const {

vout(0) << "  ||" << name << "||_infty = " << InftyNormOnCells(V) << endl;
}
void IElphyAssemble::PrintL2Norm(const Vector &V, const std::string &name) const {

  vout(0) << "  ||" << name << "||_L2 = " << L2Norm(V) << endl;
}

void IElphyAssemble::PrintH1Norm(const Vector &V, const std::string &name) const {
  vout(0) << "  ||" << name << "||_H1 = " << H1Norm(V) << endl;
}
void IElphyAssemble::PrintVariable(const Vector &var,const std::string &varname) const {
  int numberBM = problem.getNumberEvaluationPointsBesideMesh();
  int numberOnM = problem.getSizeEvaluationPoints() - numberBM;
  for (int i = 0; i < numberOnM; i++) {
    double value = 0;
    int cnt = 0;
    Point P = problem.getEvaluationPointAt(i);
    if (var.find_row(P) != var.rows_end()) {
      row r = var.find_row(P);
      if (r.NumberOfDofs() == 2) {
        value = var(P, 1);
      } else {
        value = var(P, 0);
      }

      cnt = 1;
    }
    cnt = PPM->Sum(cnt);
    if (cnt) {
      vout(0) << varname << P << " = " << PPM->Sum(value) / cnt << endl;
    }
  }
  vout(0) << endl;
}
void IElphyAssemble::PrintVariableOnCells(const Vector &var,const std::string &varname) const {
  ExchangeBuffer B;
  B.Send(0) << problem.getSizeCellsEvaluationPoints();

  for (int i = 0; i < problem.getSizeCellsEvaluationPoints(); i++) {
    EvaluationPointData data = problem.getCellAt(i);
    cell c = data.c;
    double value = var(c(),0);
    B.Send(0) << data.P << value << data.index ;
  }
  B.Communicate();
  if (PPM->Master()) {
    for (int i = 0; i < PPM->Size(); i++) {
      int sizeFromI;
      B.Receive(i) >> sizeFromI;
      for (int j = 0; j < sizeFromI; j++) {
        Point p;
        double value;
        int index;
        B.Receive(i) >> p >> value>>index ;
        vout(0) << varname <<index << p << " = " << value << endl;
      }
    }
  }
  if(varname=="gamma"){
    PrintInftyNormOnCells(var,varname);
  }else if (varname=="Ca"){
    PrintL2Norm(var,varname);
  }

  vout(0) << endl;
}
void IElphyAssemble::PrintCellMidpointsOfEvaluationPoints(const Vector &var)const{
  int numberBM = problem.getNumberEvaluationPointsBesideMesh();
  int numberOnM = problem.getSizeEvaluationPoints() - numberBM;
  for (int i = 0; i < numberOnM; i++) {
    Point P = problem.getEvaluationPointAt(i);
    if (var.find_row(P) != var.rows_end()) {
      row r = var.find_row(P);
      for (cell c= var.cells(); c != var.cells_end(); ++c) {
       for (int j=0;j<c.Corners();j++){
         if(P==c.Corner(j)){
           std::cout<<" P "<<P<<" c "<<c()<<endl;
         }
       }
      }
    //std::cout<<"     " <<  << P << " = " << value << endl;
    }
  }
}
void IElphyAssemble::PrintV(const Vector &V) const {
  int numberBM = problem.getNumberEvaluationPointsBesideMesh();
  int numberOnM = problem.getSizeEvaluationPoints() - numberBM;
  for (int i = 0; i < numberOnM; i++) {
    double v = 0;
    int cnt = 0;
    Point P = problem.getEvaluationPointAt(i);
    if (V.find_row(P) != V.rows_end()) {
      row r = V.find_row(P);
      if (r.NumberOfDofs() == 2) {
        v = V(P, 1);
      } else {
        v = V(P, 0);
      }

      cnt = 1;
    }
    cnt = PPM->Sum(cnt);
    if (cnt) {
      vout(0) << "  v" <<i<< P << " = " << PPM->Sum(v) / cnt << endl;
    }
  }
  ExchangeBuffer B;
  B.Send(0) << problem.getSizeCellsEvaluationPoints();

  for (int i = 0; i < problem.getSizeCellsEvaluationPoints(); i++) {
    EvaluationPointData data = problem.getCellAt(i);
    cell c = data.c;
    Point rP = c.GlobalToLocal(data.P );//- c.Corner(0)
    ScalarElement E(V, *c);
    double v = E.Value(rP, V);
    B.Send(0) << data.P << v << data.index;
  }
  B.Communicate();
  if (PPM->Master()) {
    for (int i = 0; i < PPM->Size(); i++) {
      int sizeFromI;
      B.Receive(i) >> sizeFromI;
      for (int j = 0; j < sizeFromI; j++) {
        Point p;
        double value;
        int index;
        B.Receive(i) >> p >> value >> index;
        vout(0) << "  v" << index << p << " = " << value << endl;


      }
    }

  }
  PrintL2Norm(V, "v");
  PrintH1Norm(V, "v");
  vout(0) << endl;
}
void IElphyAssemble::PlotExcitation(const std::string &name) const {
  if (vtkplot < 1) return;

  auto &plot = mpp::plot(name);
  plot.AddData("Excitation", excData, 0);
  plot.AddData("Amplitude", excData, 2);
  plot.PlotFile(name);

  std::vector<Point> eP(problem.getSizeEvaluationPoints());
  std::vector<double> data(problem.getSizeEvaluationPoints());
  for (int i = 0; i < problem.getSizeEvaluationPoints(); i++) {
    eP[i] = problem.getEvaluationPointAt(i);
    data[i] = 10.0;
  }
}
void IElphyAssemble::PlotExcitation() const {
  if (vtkplot < 1) return;

  auto &plot = mpp::plot("ExcitationTime");
  plot.AddData("Excitation", excData, 0);
  plot.AddData("Amplitude", excData, 2);
  plot.PlotFile("ExcitationTime");

  std::vector<Point> eP(problem.getSizeEvaluationPoints());
  std::vector<double> data(problem.getSizeEvaluationPoints());
  for (int i = 0; i < problem.getSizeEvaluationPoints(); i++) {
    eP[i] = problem.getEvaluationPointAt(i);
    data[i] = 10.0;
  }

  /*VtuPointPlot pPlot(eP);
  pPlot.AddData("EvaluationPoints", data);
  pPlot.PlotFile("EvaluationPoints");*/
}

void IElphyAssemble::PlotActivation() const {
  if (problem.UpdateActivationTime == false) return;
  writeActivationTimeFile();
  auto &plot = mpp::plot("ActivationTime");
  plot.AddData("Activation", *activationTime);
  plot.PlotFile("ActivationTime");
}
void IElphyAssemble::writeActivationTimeFile() const {
  std::string lLevel;
  Config::Get("ElphyLevel", lLevel);
  std::string tLevel = "0";
  Config::Get("ElphyTimeLevel", tLevel);
  std::string filename("data/AT_l" + lLevel + "j" + tLevel + ".txt");
  std::fstream file_out;
  ExchangeBuffer B;
  for (row r= (*activationTime).rows(); r != (*activationTime).rows_end(); ++r) {
    if((*activationTime).find_procset(r()) == (*activationTime).procsets_end() || (*activationTime).find_procset(r()).master() == PPM->Proc()) {
    //if((*activationTime).find_procset(r()).master()==PPM->Proc()) {
    B.Send(0) << r() << (*activationTime)(r(), 0);
    }
  }
  B.Communicate();

  if (PPM->Master()) {
    file_out.open(filename, std::ios_base::out);
    if (!file_out.is_open()) {
      std::cout << "failed to open " << filename << '\n';
    } else {
      for (int i = 0; i < PPM->Size(); i++) {
          while(B.Receive(i).size()<B.Receive(i).Size()) {
            Point p;
            double actiTime;
            B.Receive(i) >> p >> actiTime;
            file_out << p << " " << actiTime << endl;
          }
      }
    }
    file_out.close();
  }

}
void IElphyAssemble::PrintActivationTime() const {
  auto &AT = *activationTime;
  if (problem.UpdateActivationTime) {
    for (int i = 0; i < problem.getSizeEvaluationPoints(); i++) {
      double aTime = 0;
      int cnt = 0;
      Point P = problem.getEvaluationPointAt(i);

      if (AT.find_row(P) != AT.rows_end()) {
        aTime = AT(P, 0);
        cnt = 1;
      }
      cnt = PPM->Sum(cnt);
      if (cnt)
        vout(1) << "  ActivationTime in " << P << " = " << PPM->Sum(aTime) / cnt << endl;

    }
  }
}

void IElphyAssemble::PlotPotential(Vector &V, const std::string &varname,
                                   int step) const {
  if (vtkplot < 1 || step % vtksteps != 0) return;
  auto &plot = mpp::plot(varname + stepAsString(step));
  plot.AddData("V", V);
  plot.PlotFile();
}

Vector &IElphyAssemble::ActivationTime() const { return *activationTime; }

const Vector &IElphyAssemble::ExcitationData() const { return excData; }

void IElphyAssemble::PrintMesh(const Vector &u) const {
  std::vector<Point> z(u.size());
  for (row r = u.rows(); r != u.rows_end(); ++r) {
    z[r.Id()] = r();
  }
  mout << "POINTS:" << endl;
  for (int i = 0; i < u.nR(); ++i) {
    mout << z[i][0] << " " << z[i][1] << " " << z[i][2] << endl;
  }

  mout << "CELLS:" << endl;
  for (cell c = u.GetMesh().cells(); c != u.GetMesh().cells_end(); ++c) {
    VectorFieldElement R(u, *c);
    mout << "4" << " " << c.Subdomain() << " " << R[0].Id()
         << " " << R[1].Id()
         << " " << R[2].Id()
         << " " << R[3].Id()
         << endl;

  }
  mout << "FACES:" << endl;
  mout << "VDATA:" << endl;
  std::vector<double> excitation(u.size());
  std::vector<double> amplitude(u.size());
  std::vector<double> duration(u.size());
  for (row r = u.rows(); r != u.rows_end(); ++r) {
    amplitude[r.Id()] = Amplitude(excData, r, 0);
    excitation[r.Id()] = Excitation(excData, r, 0);
    duration[r.Id()] = Duration(excData, r, 0);
  }
  for (int i = 0; i < u.nR(); ++i) {
    mout << 3 << " " << excitation[i] << " " << duration[i] << " " << amplitude[i] << endl;
  }

  mout << "CDATA:" << endl;
  std::vector<double> fibre(u.size());
  std::vector<double> sheet(u.size());
  std::vector<double> normal(u.size());
  for (cell c = u.GetMesh().cells(); c != u.GetMesh().cells_end(); ++c) {
    const auto &data = c.GetData();
    VectorField fibre = Direction(data);
    VectorField sheet = Sheet(data);
    VectorField normal = Normal(data);
    mout << 9 << " " << fibre[0] << " " << fibre[1] << " " << fibre[2]
         << " " << sheet[0] << " " << sheet[0] << " " << sheet[0]
         << " " << normal[0] << " " << normal[0] << " " << normal[0] << endl;
  }
}
double IElphyAssemble::InftyNormOnVertices(const Vector &u) const{
  double max=0.0;
  for (row r=u.rows(); r!=u.rows_end();r++){
    if(max<abs(u(r(),0))){
      max=abs(u(r(),0));
    }
  }
  return PPM->Max(max);
}
double IElphyAssemble::InftyNormOnCells(const Vector &V) const{
  double max =0.0;
  for (cell c = V.cells(); c != V.cells_end(); ++c) {
    if(max<abs(V(c(),0))){
      max=abs(V(c(),0));
    }
  }
  return PPM->Max(max);
}
double IElphyAssemble::L2Norm(const Vector &V) const {
  double v_sum = 0;
  for (cell c = V.cells(); c != V.cells_end(); ++c) {
    ScalarElement E(V, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      double V_p = E.Value(q, V, 0);
      double w_q = E.QWeight(q);
      v_sum += w_q * V_p * V_p;

    }
  }
  return sqrt(PPM->Sum(v_sum));
}
double IElphyAssemble::L2Error(const Vector &V) const {
  mout << "Time: " << Time() << endl;
  double v_sum = 0;
  for (cell c = V.cells(); c != V.cells_end(); ++c) {
    ScalarElement E(V, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      double V_p =
          E.Value(q, V, 0) - GetElphyProblem().Potential(Time(), E.QPoint(q));
      double w_q = E.QWeight(q);
      v_sum += w_q * V_p * V_p;

    }
  }
  return sqrt(PPM->Sum(v_sum));
}

double IElphyAssemble::L2AvgNorm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    ScalarElement E(u, *c);

    double w = 0.0;
    double U = 0.0;
    for (int q = 0; q < E.nQ(); ++q) {
      w += E.QWeight(q);
      U += E.Value(q, u);
    }
    err += w * (U * U);

  }
  return sqrt(PPM->Sum(err));
}

double IElphyAssemble::H1Norm(const Vector &V) const {
  double h1 = 0;
  for (cell c = V.cells(); c != V.cells_end(); ++c) {
    ScalarElement E(V, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      double V_p = E.Value(q, V, 0);
      double w_q = E.QWeight(q);
      Tensor DV = E.Derivative(q, V, 0);
      h1 += w_q * (V_p * V_p + Frobenius(DV, DV));
    }
  }

  return sqrt(PPM->Sum(h1));
}

void IElphyAssemble::SetInitialValue(Vector &u) {
  Iext = std::make_unique<Vector>(0.0, u);
  activationTime = std::make_unique<Vector>(-1.0, u);
}

void IElphyAssemble::FinishTimeStep(const Vector &u) {

}

void IElphyAssemble::PrintIteration(const Vector &u) const {
  if (Step() % printVSteps == 0) {
    PrintV(u);
  }
}

void IElphyAssemble::PlotIteration(const Vector &u) const {
  if (Step() == 0)
    PlotExcitation();
  PlotStatic({{"V", u}}, Step(), "V");
  if (IsFinished())
    PlotActivation();
}
void IElphyAssemble::PlotCa(const Vector &Ca)const{
  int currentStep=(Time()-startTime)/StepSize();
  if (vtkplot < 1 ||  currentStep % vtksteps != 0) return;
  auto &plot = mpp::plot("Ca");
  plot.AddData("Ca", Ca);
  plot.PlotFile("Ca" + stepAsString(currentStep));
}
void IElphyAssemble::PlotIteration(const Vectors &elphyValues, int step) const {
  step = step < 0 ? Step() : step;

  if (step == 0)
    PlotExcitation();
  PlotStatic({{"Potential", elphyValues[0]},
              {"Stretch",   elphyValues[1]},
              {"Iota4",     elphyValues[2]}}, step, "V");
  if (IsFinished())
    PlotActivation();
}

#include "Monodomain.hpp"
#include "MonodomainSplitting.hpp"

std::unique_ptr<IElphyAssemble> CreateElphyAssemble(
    ElphyProblem &problem, MCellModel &cellModel, const std::string &modelName,
    int discDegree) {
  int hexaQuadExactupTo = 2 * discDegree;
  if (problem.Name() == "HexaTestProblem") {
    Config::Get("HexaQuadExactupTo", hexaQuadExactupTo);
  }
  if (modelName == "LinearImplicit") {
    return std::make_unique<Monodomain>(problem, cellModel, discDegree, hexaQuadExactupTo);
  } else if (modelName == "LinearImplicitNodes") {
    return std::make_unique<MonodomainLINodes>(problem, cellModel, discDegree, hexaQuadExactupTo);
  }/*else if(modelName =="LinearImplicitQuadrature") {
    return std::make_unique<MonodomainQuadrature>(*problem, *cellModel, *excitationData,*potential, discDegree);
    }*/else if (modelName == "ImplictEuler") {
    return std::make_unique<MonodomainFullNewton>(problem, cellModel, discDegree,
                                                  hexaQuadExactupTo);
  } else if (modelName == "SemiImplicit"   ) {
    return std::make_unique<MonodomainSemiImplicit>(problem, cellModel, discDegree,
                                                    hexaQuadExactupTo);
  } else if (modelName == "SemiImplicitNodes") {
    return std::make_unique<MonodomainSemiImplicitNodes>(problem, cellModel, discDegree,
                                                         hexaQuadExactupTo);
  }else if (modelName == "SemiImplicitOnCells") {
    return std::make_unique<MonodomainSemiImplicitCells>(problem, cellModel, discDegree,
                                                         hexaQuadExactupTo);
    }
    else if (modelName=="SemiImplicitOnCellsIextPerCell"){
      return std::make_unique<MonodomainSemiImplicitCellsIextPerCell>(problem, cellModel, discDegree,
                                                              hexaQuadExactupTo);
    }
  else if (modelName == "SemiImplicitOnCellsSVI") {
    return std::make_unique<MonodomainSemiImplicitCellsSVI>(problem, cellModel, discDegree,
                                                         hexaQuadExactupTo);
  }
  else {
    return std::make_unique<MonodomainSplitting>(problem, discDegree, hexaQuadExactupTo);
  }
}

std::unique_ptr<IElphyAssemble>
CreateElphyAssemble(ElphyProblem &problem, MCellModel &cellModel, int discDegree) {
  std::string modelName{"LinearImplicit"};
  Config::Get("ElphyModel", modelName);
  return CreateElphyAssemble(problem, cellModel, modelName, discDegree);
}

