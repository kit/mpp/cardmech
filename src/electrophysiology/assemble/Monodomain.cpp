#include "ScalarElement.hpp"
#include "Monodomain.hpp"
#include <iostream>
#include <MultiPartScalarElement.hpp>
#include <MultiPartScalarWGElement.hpp>
#include "ExternalCurrent.hpp"

Monodomain::Monodomain(ElphyProblem &elphyProblem, MCellModel &cellModel, int degree,
                       int exactUpTo)
    : IElphyAssemble(elphyProblem, degree, exactUpTo), beta(0.0), elphyModel(cellModel) {
  InitializeMonodomainParameters();
  //vcw = new std::vector<double>(M.ElphySize());


}

void Monodomain::InitializeMonodomainParameters() {
  Config::Get("SurfaceToVolumeRatio", beta);
  Config::Get("MembraneCapacitance", C_m);
  //Divide for correct Units from microF to F
  mircoFToF = (1.0 / 1000000.0);
  cmSquareTommSquare = 100.0;
  // C_m = C_m / 1000000.0;
  conversion = 1 / 100000000.0;

}

void Monodomain::SetInitialValue(Vector &u) {
  IElphyAssemble::SetInitialValue(u);
  u = elphyModel.InitialValue(ELPHY);
  updateExternalCurrent(u);

}

void Monodomain::SetInitialValue(Vectors &values) {
  SetInitialValue(values[0]);
  values[1] = elphyModel.InitialValue(TENSION);

}

void Monodomain::updateExternalCurrent(const Vector &V) {
  if (!isNormIextZero) {
    double tNew = Time() + timeUpdateIext;
    double tHalve = Time() + (StepSize() * 0.5);
    for (row r = V.rows(); r != V.rows_end(); ++r) {
      for (int j = 0; j < r.NumberOfDofs(); ++j) {
        (*Iext)(r, j) = excitationFunc(elphyModel.TimeScale() * tNew, Amplitude(excData, r, j),
                                       elphyModel.TimeScale() * Excitation(excData, r, j),
                                       elphyModel.TimeScale() * Duration(excData, r, j));
        //Interpolation Test:
        //(*Iext)(r, j) = 0.5*getContinuousIext(Time(), Amplitude(excData, r, j), Excitation(excData, r, j),Duration(excData, r, j));
        //(*Iext)(r, j) += 0.5 * getContinuousIext(tNew, Amplitude(excData, r, j),Excitation(excData, r, j),Duration(excData, r, j));
      }
    }
    //PrintL2Norm(*Iext, "Iext");

    /*if((*Iext).norm()==0){
      isNormIextZero=true;
    }*/
  }

}

void Monodomain::SystemMatrix(const cell &c, const Vectors &VCW, Matrix &A) const {
  MultiPartScalarElement E(VCW[0], *c, DomainPart(*c));
  PartRowEntries A_c(A, E, DomainPart(*c));
  double J = problem.Jacobian(*c);
  Tensor S = problem.Conductivity(*c);
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      for (int i = 0; i < vcw.size(); ++i) {
        vcw[i] = E.Value(q, VCW[i], E.IndexPart(j));
      }
      double DVIion = elphyModel.JacobiEntry(0, 0, vcw);
      double V_j = E.Value(q, j);
      VectorField S_DV_j = S * E.Derivative(q, j);
      for (int i = 0; i < E.size(); ++i) {
        double V_i = E.Value(q, i);
        VectorField DV_i = E.Derivative(q, i);
        //Original:
        //A_c(i, j, 0, 0) +=w * (V_i * V_j + StepSize() * ((1 / (beta * C_m)) * (DV_i * S_DV_j)+ M.TimeScale() * DVIion * V_i * V_j));
        A_c(i, j, 0, 0) += w * (V_i * V_j + StepSize() *
                                            ((1 / (beta * C_m * mircoFToF)) * (DV_i * S_DV_j) +
                                             elphyModel.TimeScale() * (1.0 / C_m) *
                                             (1 / cmSquareTommSquare) * DVIion * V_i * V_j));
      }
    }
  }
}

void Monodomain::addReactionSAndRHS(const cell &c, const Vectors &VCW, Matrix &A, Vector &r) const {
  MultiPartScalarElementWG E(VCW[0], *c, DomainPart(*c));
  PartRowEntries A_c(A, E, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      E.Values(q, VCW, vcw, E.IndexPart(j));
      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double V_j = E.Value(q, j);
      double Iion = elphyModel.GetIion(vcw);
      double DVIion = elphyModel.JacobiEntry(0, 0, vcw);
      //Original
      r_c(j) += w * (vcw[0] -
                     elphyModel.TimeScale() * StepSize() * (1.0 / C_m) * (1 / cmSquareTommSquare) *
                     (Iion - DVIion * vcw[0] - iext)) * V_j;

      for (int i = 0; i < E.size(); ++i) {
        double V_i = E.Value(q, i);
        A_c(i, j, 0, 0) +=
            w *
            (StepSize() * elphyModel.TimeScale() * (1.0 / C_m) * (1 / cmSquareTommSquare) * DVIion *
             V_i * V_j);
      }
    }
  }
}

void Monodomain::addReaction(const cell &c, const Vectors &VCW, Matrix &A) const {
  MultiPartScalarElementWG E(VCW[0], *c, DomainPart(*c));
  PartRowEntries A_c(A, E, DomainPart(*c));
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      E.Values(q, VCW, vcw, E.IndexPart(j));
      //for (int i = 0; i < vcw.size(); ++i) {
      //vcw[i] = E.Value(q, VCW[i], E.IndexPart(j));
      //}
      double DVIion = elphyModel.JacobiEntry(0, 0, vcw);
      double V_j = E.Value(q, j);
      for (int i = 0; i < E.size(); ++i) {
        double V_i = E.Value(q, i);
        A_c(i, j, 0, 0) +=
            w *
            (StepSize() * elphyModel.TimeScale() * (1.0 / C_m) * (1 / cmSquareTommSquare) * DVIion *
             V_i * V_j);
      }
    }
  }

}

void Monodomain::MPlusK(const cell &c, const Vector &V, Matrix &A) const {
  MultiPartScalarElement E(V, *c, DomainPart(*c));
  PartRowEntries A_c(A, E, DomainPart(*c));
  double J = problem.Jacobian(*c);
  Tensor S = problem.Conductivity(*c);
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      double V_j = E.Value(q, j);
      VectorField S_DV_j = S * E.Derivative(q, j);
      for (int i = 0; i < E.size(); ++i) {
        double V_i = E.Value(q, i);
        VectorField DV_i = E.Derivative(q, i);
        A_c(i, j, 0, 0) +=
            w * (J * V_i * V_j + StepSize() * (1 / (beta * C_m * mircoFToF)) * (DV_i * S_DV_j));
      }
    }
  }

}

void Monodomain::RHS(const cell &c, const Vectors &VCW, Vector &r) const {
  //MultiPartScalarElement E(VCW[0], *c,DomainPart(*c));
  MultiPartScalarElementWG E(VCW[0], *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      E.Values(q, VCW, vcw, E.IndexPart(j));
      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double v_j = E.Value(q, j);
      double Iion = elphyModel.GetIion(vcw);
      double DVIion = elphyModel.JacobiEntry(0, 0, vcw);
      //Original
      //r_c(j) += w * (vcw[0] - M.TimeScale() * StepSize() * (Iion - DVIion * vcw[0] - iext)) * v_j;
      r_c(j) += w * (vcw[0] -
                     elphyModel.TimeScale() * StepSize() * (1.0 / C_m) * (1 / cmSquareTommSquare) *
                     (Iion - DVIion * vcw[0] - iext)) * v_j;
    }
  }
}

MonodomainLINodes::MonodomainLINodes(ElphyProblem &elphyProblem, MCellModel &cellModel,
                                     int degree, int exactUpTo) : Monodomain(elphyProblem, cellModel,
                                                                         degree, exactUpTo) {
  IionVEC = std::make_unique<Vector>(0.0, GetSharedDisc());
  DVIionVEC = std::make_unique<Vector>(0.0, GetSharedDisc());
}

void MonodomainLINodes::updateIionVecs(const Vectors &VCW) {
  vout(10).StartBlock("Update Iion and DVIion");
  std::vector<double> vcw(elphyModel.ElphySize());
  for (row r = VCW[0].rows(); r != VCW[0].rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < elphyModel.ElphySize(); i++) {
        vcw[i] = VCW[i](r, j);
      }
      (*IionVEC)(r, j) = elphyModel.GetIion(vcw);
      (*DVIionVEC)(r, j) = elphyModel.JacobiEntry(0, 0, vcw);
    }
  }
  vout(10).EndBlock();
}

void MonodomainLINodes::updateIion(const Vectors &VCW) {
  std::vector<double> vcw(elphyModel.ElphySize());
  for (row r = VCW[0].rows(); r != VCW[0].rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < elphyModel.ElphySize(); i++) {
        vcw[i] = VCW[i](r, j);
      }
      (*IionVEC)(r, j) = elphyModel.GetIion(vcw);
    }
  }
  //PrintL2Norm(*Iext, "Iion");
}

void MonodomainLINodes::updateDVIion(const Vectors &VCW) {

  std::vector<double> vcw(elphyModel.ElphySize());
  for (row r = VCW[0].rows(); r != VCW[0].rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < elphyModel.ElphySize(); i++) {
        vcw[i] = VCW[i](r, j);
      }
      (*DVIionVEC)(r, j) = elphyModel.JacobiEntry(0, 0, vcw);
    }
  }
  //PrintL2Norm(*Iext, "DVIion");

}

void
MonodomainLINodes::addReactionSAndRHS(const cell &c, const Vectors &VCW, Matrix &A, Vector &r) const {
  MultiPartScalarElementWG E(VCW[0], *c, DomainPart(*c));
  PartRowEntries A_c(A, E, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      double v_old = E.Value(q, VCW[0], E.IndexPart(j));
      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double V_j = E.Value(q, j);
      double Iion = E.Value(q, *IionVEC, E.IndexPart(j));
      double DVIion = E.Value(q, *DVIionVEC, E.IndexPart(j));
      //Original
      r_c(j) += w * (v_old -
                     elphyModel.TimeScale() * StepSize() * (1.0 / C_m) * (1 / cmSquareTommSquare) *
                     (Iion - DVIion * v_old - iext)) * V_j;

      for (int i = 0; i < E.size(); ++i) {
        double V_i = E.Value(q, i);
        A_c(i, j, 0, 0) +=
            w *
            (StepSize() * elphyModel.TimeScale() * (1.0 / C_m) * (1 / cmSquareTommSquare) * DVIion *
             V_i * V_j);
      }
    }
  }

}

void MonodomainLINodes::SystemMatrix(const cell &c, const Vectors &VCW, Matrix &A) const {
  MultiPartScalarElement E(VCW[0], *c, DomainPart(*c));
  PartRowEntries A_c(A, E, DomainPart(*c));
  double J = problem.Jacobian(*c);
  Tensor S = problem.Conductivity(*c);
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    for (int j = 0; j < E.size(); ++j) {
      E.Values(q, VCW, vcw, E.IndexPart(j));
      double DVIion = E.Value(q, *DVIionVEC, E.IndexPart(j));
      double V_j = E.Value(q, j);
      VectorField S_DV_j = S * E.Derivative(q, j);
      for (int i = 0; i < E.size(); ++i) {
        double V_i = E.Value(q, i);
        VectorField DV_i = E.Derivative(q, i);
        A_c(i, j, 0, 0) +=
            w * (V_i * V_j + StepSize() * ((1 / (beta * C_m)) * (DV_i * S_DV_j)
                                           + elphyModel.TimeScale() * DVIion * V_i * V_j));
      }
    }
  }
}

void MonodomainLINodes::RHS(const cell &c, const Vectors &VCW, Vector &r) const {
  MultiPartScalarElement E(VCW[0], *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      E.Values(q, VCW, vcw, E.IndexPart(j));

      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double v_j = E.Value(q, j);

      double Iion = E.Value(q, *IionVEC, E.IndexPart(j));
      double DVIion = E.Value(q, *DVIionVEC, E.IndexPart(j));
      r_c(j) +=
          w * (vcw[0] - elphyModel.TimeScale() * StepSize() * (Iion - DVIion * vcw[0] - iext)) *
          v_j;
    }
  }
}


MonodomainQuadrature::MonodomainQuadrature(ElphyProblem &elphyProblem, MCellModel &cellModel,
                                           const Vector &u, int degree, int exactUpTo)
    : Monodomain(elphyProblem, cellModel, degree, exactUpTo) {
  int nQ = 0;
  int nJ = 0;
  if (VCWQuadrature == nullptr) {
    //TODO: nQ und nJ schneller bestimmen
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      MultiPartScalarElement E(u, *c, DomainPart(*c));
      nQ = E.nQ();
      nJ = E.size();
    }

    CWdisc = std::make_shared<DoFDiscretization<CellDoF>>(u.GetDisc().GetMeshes(), nQ * nJ);
    CWcells = std::make_unique<Vector>(CWdisc);
    VCWQuadrature = std::make_unique<Vectors>(elphyModel.ElphySize(), *CWcells);
    elphyModel.Initialize(*VCWQuadrature);
  }
};

void MonodomainQuadrature::updateCaAndW(const cell &c, std::vector<double> &vcw, int index) const {
  std::vector<double> G(elphyModel.ElphySize());
  auto yInfty = elphyModel.YInfty(vcw);
  auto tau = elphyModel.YInfty(vcw);
  for (int i = 0; i < elphyModel.GatingSize(); ++i) {
    int iShift = elphyModel.GatingIndex() + i;
    vcw[iShift] =
        yInfty[i] + (vcw[iShift] - yInfty[i]) * exp(-StepSize() * elphyModel.TimeScale() / tau[i]);
    (*VCWQuadrature)[iShift](c(), index) = vcw[iShift];
  }
  elphyModel.ConcentrationUpdate(vcw, G, elphyModel.IExt(Time()));
  for (int i = elphyModel.CalciumIndex(); i < elphyModel.GatingIndex(); ++i) {
    vcw[i] += StepSize() * elphyModel.TimeScale() * G[i];
    (*VCWQuadrature)[i](c(), index) = vcw[i];
  }
}

void MonodomainQuadrature::SystemMatrix(const cell &c, const Vector &V, Matrix &A) const {
  MultiPartScalarElement E(V, *c, DomainPart(*c));
  PartRowEntries A_c(A, E, DomainPart(*c));
  double J = problem.Jacobian(*c);
  Tensor S = problem.Conductivity(*c);
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      vcw[0] = E.Value(q, V, E.IndexPart(j));
      for (int i = 1; i < vcw.size(); ++i) {
        vcw[i] = (*VCWQuadrature)[i](c(), 0);
      }
      updateCaAndW(c, vcw, (q * E.size()) + j);
      double DVIion = elphyModel.JacobiEntry(0, 0, vcw);
      double V_j = E.Value(q, j);
      VectorField S_DV_j = S * E.Derivative(q, j);
      for (int i = 0; i < E.size(); ++i) {
        double V_i = E.Value(q, i);
        VectorField DV_i = E.Derivative(q, i);
        A_c(i, j, 0, 0) +=
            w * (V_i * V_j + StepSize() * ((1 / (beta * C_m)) * (DV_i * S_DV_j)
                                           + elphyModel.TimeScale() * DVIion * V_i * V_j));
      }
    }
  }
}

void MonodomainQuadrature::RHS(const cell &c, const Vectors &VCW, Vector &r) const {
  MultiPartScalarElement E(VCW[0], *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      vcw[0] = E.Value(q, VCW[0], E.IndexPart(j));
      for (int i = 1; i < vcw.size(); ++i) {
        vcw[i] = (*VCWQuadrature)[i](c(), (q * E.size()) + j);
      }
      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double v_j = E.Value(q, j);
      double Iion = elphyModel.GetIion(vcw);
      double DVIion = elphyModel.JacobiEntry(0, 0, vcw);
      r_c(j) +=
          w * (vcw[0] - elphyModel.TimeScale() * StepSize() * (Iion - DVIion * vcw[0] - iext)) *
          v_j;
    }
  }
}

MonodomainFullNewton::MonodomainFullNewton(ElphyProblem &elphyProblem, MCellModel &cellModel,
                                           int degree, int exactUpTo)
    : Monodomain(elphyProblem, cellModel, degree, exactUpTo) {
  VACAW = std::make_unique<Vectors>(elphyModel.ElphySize(), GetSharedDisc());
  elphyModel.Initialize(*VACAW);

  /*if(MK==nullptr){
    MK=std::make_unique<Matrix>( u);
  }*/
}

void MonodomainFullNewton::Residual(const cell &c, const Vector &V, Vector &r) const {
  MultiPartScalarElement E(V, *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  std::vector<double> vcw(elphyModel.ElphySize());
  double J = problem.Jacobian(*c);
  Tensor S = problem.Conductivity(*c);

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    VectorField S_Dv = S * E.Derivative(q, V);
    for (int j = 0; j < E.size(); ++j) {
      E.Values(q, (*VACAW), vcw, E.IndexPart(j));
      vcw[0] = E.Value(q, V, E.IndexPart(j));
      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double v_j = E.Value(q, j);
      VectorField Dv_j = E.Derivative(q, j);
      double Iion = elphyModel.GetIion(vcw);
      double v_old = E.Value(q, (*VACAW)[0], E.IndexPart(j));
      r_c(j) += w * (vcw[0] * v_j - v_old * v_j +
                     StepSize() * (1 / (beta * C_m * mircoFToF)) * (S_Dv * Dv_j) +
                     elphyModel.TimeScale() * StepSize() * (1.0 / C_m) * (1 / cmSquareTommSquare) *
                     (Iion * v_j - iext * v_j));
    }
  }

}

void MonodomainFullNewton::Jacobi(const cell &c, const Vector &V, Matrix &jacobi) const {
  MultiPartScalarElement E(V, *c, DomainPart(*c));
  PartRowEntries A_c(jacobi, E, DomainPart(*c));
  double J = problem.Jacobian(*c);
  Tensor S = problem.Conductivity(*c);
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      E.Values(q, (*VACAW), vcw, E.IndexPart(j));
      vcw[0] = E.Value(q, V, E.IndexPart(j));
      double DVIion = elphyModel.JacobiEntry(0, 0, vcw);
      double V_j = E.Value(q, j);
      VectorField S_DV_j = S * E.Derivative(q, j);
      for (int i = 0; i < E.size(); ++i) {
        double V_i = E.Value(q, i);
        VectorField DV_i = E.Derivative(q, i);
        //A_c(i, j, 0, 0) +=w * (StepSize()* M.TimeScale() * (1.0/C_m)*(1/cmSquareTommSquare)*DVIion * V_i * V_j);
        A_c(i, j, 0, 0) += w * (V_i * V_j +
                                StepSize() * (1 / (beta * C_m * mircoFToF)) * (DV_i * S_DV_j) +
                                StepSize() * elphyModel.TimeScale() * (1.0 / C_m) *
                                (1 / cmSquareTommSquare) * DVIion * V_i * V_j);
      }
    }
  }
}

void MonodomainFullNewton::updateVCW(const Vectors &VCW) {
  for (row r = VCW[0].rows(); r != VCW[0].rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < elphyModel.ElphySize(); i++) {
        (*VACAW)[i](r, j) = VCW[i](r, j);
      }
    }
  }
}


void MonodomainSemiImplicit::RHS(const cell &c, const Vectors &VCW, Vector &r) const {
  //Point P(2.125,    2.75,    2.875);
  Point P(76.734675, -82.053925, -44.18785);

  double J = problem.Jacobian(*c);
  double J_old = problem.Jacobian_old(*c);
  MultiPartScalarElementWG E(VCW[0], *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    for (int j = 0; j < E.size(); ++j) {
      E.Values(q, VCW, vcw, E.IndexPart(j));
      double Iion = elphyModel.GetIion(vcw);
      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double V_j = E.Value(q, j);
      r_c(j) += w * (J_old * vcw[0] -
                     J * elphyModel.TimeScale() * StepSize() * (1.0 / C_m) * (1 / cmSquareTommSquare) *
                     (Iion - iext)) * V_j;
    }
  }
}

void MonodomainSemiImplicitNodes::RHS(const cell &c, const Vectors &VCW, Vector &r) const {
  MultiPartScalarElementWG E(VCW[0], *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      double v = E.Value(q, VCW[0], E.IndexPart(j));
      double Iion = E.Value(q, *IionVEC, E.IndexPart(j));
      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double V_j = E.Value(q, j);
      r_c(j) += w * (v -
                     elphyModel.TimeScale() * StepSize() * (1.0 / C_m) * (1 / cmSquareTommSquare) *
                     (Iion - iext)) * V_j;
    }
  }
}

void MonodomainSemiImplicitNodes::updateIionVecs(const Vectors &VCW) {
  vout(10).StartBlock("Update Iion");
  std::vector<double> vcw(elphyModel.ElphySize());
  for (row r = VCW[0].rows(); r != VCW[0].rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < elphyModel.ElphySize(); i++) {
        vcw[i] = VCW[i](r, j);
      }
      (*IionVEC)(r, j) = elphyModel.GetIion(vcw);
    }
  }
  vout(10).EndBlock();
}

/*void MonodomainSemiImplicitCells::RHS(const cell &c,const Vector &V,const
Vectors &VCW_onCells, Vector &r) const { Point P(2.125,    2.75,    2.875);
  double J = problem.Jacobian(*c);
  double J_old = problem.Jacobian_old(*c);
  MultiPartScalarElementWG E(V, *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int i=0;i<elphyModel.ElphySize();i++){
    vcw[i]=VCW_onCells[i](c(), 0);
  }
  double Iion = elphyModel.GetIion(vcw);
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      double v = E.Value(q, V,E.IndexPart(j));
      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double V_j = E.Value(q, j);
      r_c(j) += w * (J_old * v -
                     J * elphyModel.TimeScale() * StepSize() * (1.0 / C_m) * (1
/ cmSquareTommSquare) * (Iion - iext)) * V_j;
    }
  }
}*/
void MonodomainSemiImplicitCells::RHS(const cell &c,const Vector &V, Vector &r) const {
  Point P(2.125,    2.75,    2.875);
  double J = problem.Jacobian(*c);
  double J_old = problem.Jacobian_old(*c);
  MultiPartScalarElementWG E(V, *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  double Iion = (*IionCell)(c(), E.IndexPart(0));
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      double v = E.Value(q, V,E.IndexPart(j));
      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double V_j = E.Value(q, j);
      r_c(j) += w * (J_old * v -
                     J * elphyModel.TimeScale() * StepSize() * (1.0 / C_m) * (1 / cmSquareTommSquare) *
                     (Iion - iext)) * V_j;
    }
  }
}
void MonodomainSemiImplicitCells::updateIionVecs(const Vectors &VCW) {
  vout(10).StartBlock("Update Iion ");
  std::vector<double> vcw(elphyModel.ElphySize());
  for (row r = VCW[0].rows(); r != VCW[0].rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < elphyModel.ElphySize(); i++) {
        vcw[i] = VCW[i](r, j);
      }
      (*IionCell)(r, j) = elphyModel.GetIion(vcw);
    }
  }
  vout(10).EndBlock();
  //PrintVariable(*IionCell,"ionCell nach update");
}
void MonodomainSemiImplicitCellsIextPerCell::RHS(const cell &c,const Vector &V, Vector &r) const {
  double J = problem.Jacobian(*c);
  double J_old = problem.Jacobian_old(*c);
  MultiPartScalarElementWG E(V, *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  double Iion = (*IionCell)(c(), E.IndexPart(0));
  double iext = 0.0;
  if (!isNormIextZero) {
    iext = (*Iext_cells)(c(),0);
  }
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      double v = E.Value(q, V, E.IndexPart(j));
      double V_j = E.Value(q, j);
      r_c(j) += w * (J_old * v -
                     J * elphyModel.TimeScale() * StepSize() * (1.0 / C_m) * (1 / cmSquareTommSquare) *
                     (Iion - iext)) * V_j;
    }
  }
}
void MonodomainSemiImplicitCellsIextPerCell::updateExternalCurrent(const Vector &V){
  double tNew = Time() + timeUpdateIext;
  double tHalve = Time() + (StepSize() * 0.5);
  for (cell c = (*Iext_cells).cells(); c != (*Iext_cells).cells_end(); ++c) {
    (*Iext_cells)(c(), 0) = excitationFunc(elphyModel.TimeScale() * tNew,
                                           (*externalCurrentOnCells)[2](c(), 0), //amplitude
                                           elphyModel.TimeScale() *
                                           (*externalCurrentOnCells)[0](c(), 0), //excitation time
                                           elphyModel.TimeScale() *
                                           (*externalCurrentOnCells)[1](c(), 0)); //duration
  }
}
void MonodomainSemiImplicitCellsIextPerCell::SetInitialValue(Vector &V) {

  Iext_cells=std::make_unique<Vector>(0.0,disc0);//stimmt das?
  // vorher: Iext_cells=std::make_unique<Vector>(0.0,*disc0);

  Vector amplitude( V);
  Vector excitation(V);
  Vector duration( V);
  for (row r = (V).rows(); r != (V).rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      amplitude(r,j)=Amplitude(ExcitationData(), r, j);
      excitation(r,j)=Excitation(ExcitationData(), r, j);
      duration(r,j)=Duration(ExcitationData(), r, j);
    }
  }
  externalCurrentOnCells=std::make_unique<Vectors>(3, *Iext_cells);
  for (cell c = V.cells(); c != V.cells_end(); ++c) {
    ScalarElement E(V,*c);
    (*externalCurrentOnCells)[0](c(),0) = E.Value(c.LocalCenter(),(excitation));
    (*externalCurrentOnCells)[1](c(),0) = E.Value(c.LocalCenter(),(duration));
    (*externalCurrentOnCells)[2](c(),0) = E.Value(c.LocalCenter(),(amplitude));

  }
  Monodomain::SetInitialValue(V);
}
void MonodomainSemiImplicitCellsSVI::RHS(const cell &c,const Vector &V,const Vectors &VCW_onCells, Vector &r) const {
  Point P(2.125,    2.75,    2.875);
  double J = problem.Jacobian(*c);
  double J_old = problem.Jacobian_old(*c);
  MultiPartScalarElementWG E(V, *c, DomainPart(*c));
  PartRowBndValues r_c(r, E, *c, DomainPart(*c));
  std::vector<double> vcw(elphyModel.ElphySize());
  for (int i=1;i<elphyModel.ElphySize();i++){
    vcw[i]=VCW_onCells[i](c(), 0);
  }
  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    for (int j = 0; j < E.size(); ++j) {
      double v = E.Value(q, V, E.IndexPart(j));
      vcw[0]=v;
      double Iion = elphyModel.GetIion(vcw);
      double iext = 0.0;
      if (!isNormIextZero) {
        iext = E.Value(q, *Iext, E.IndexPart(j));
      }
      double V_j = E.Value(q, j);
      r_c(j) += w * (J_old * v -
                     J * elphyModel.TimeScale() * StepSize() * (1.0 / C_m) * (1 / cmSquareTommSquare) *
                     (Iion - iext)) * V_j;
    }
  }
}
