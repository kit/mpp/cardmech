add_library(ELPHY_ASSEMBLE STATIC
        IElphyAssemble.cpp
        Monodomain.cpp
        MonodomainSplitting.cpp
        )

target_link_libraries(ELPHY_ASSEMBLE ELPHY_PROBLEMS)