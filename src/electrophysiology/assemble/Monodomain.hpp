#ifndef _MONODOMAIN_HPP_
#define _MONODOMAIN_HPP_

#include "IElphyAssemble.hpp"
#include "../problem/ElphyProblem.hpp"
#include "MCellModel.hpp"
#include "DoFDiscretization.hpp"
#include "DGDiscretization.hpp"

class Monodomain : public IElphyAssemble {

protected:
  MCellModel &elphyModel;
  double C_m;
  double beta;
  double mircoFToF;
  double cmSquareTommSquare;
  double conversion;
  //std::vector<double> vcw;
public:
  Monodomain(ElphyProblem &elphyProblem, MCellModel &cellModel, int degree,
             int exactUpTo = -1);

  const char *Name() const override { return "Monodomain"; };

  void InitializeMonodomainParameters();

  virtual void SetInitialValue(Vector &u) override;

  void SetInitialValue(Vectors &values) override;

  virtual void updateExternalCurrent(const Vector &V) override;

  virtual void updateIionVecs(const Vectors &VCW) override {};

  virtual void RHS(const cell &c, const Vectors &u, Vector &r) const;

  virtual void RHS(const cell &c, const Vector &u, Vector &r) const{
    THROW("Not implemented in the basis Monodomain")
  };
  virtual void RHS(const cell &c, const Vector &u, const Vectors &g,Vector &r) const{
    THROW("Not implemented in the basis Monodomain")
  };


  void RHS(const Vectors &u, Vector &r) const override {
    r = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      RHS(c, u, r);
    //r.ClearDirichletValues();
    r.Collect();
  }

  void RHS(const Vector &u, Vector &r) const override {
    r = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      RHS(c, u, r);
    //r.ClearDirichletValues();
    r.Collect();
  };
  void RHS(const Vector &u,const Vectors &g, Vector &r) const override {
    r = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      RHS(c, u, g,r);
    //r.ClearDirichletValues();
    r.Collect();
  };

  virtual void addReactionSAndRHS(const cell &c, const Vectors &u, Matrix &S, Vector &r) const;

  void addReactionSAndRHS(const Vectors &u, Matrix &S, Vector &r) const override {
    r = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      addReactionSAndRHS(c, u, S, r);
    r.Collect();
  };

  void addReaction(const cell &c, const Vectors &u, Matrix &S) const;

  void addReaction(const Vectors &u, Matrix &S) const override {
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      addReaction(c, u, S);
    S.ClearDirichletValues();
  };

  void MPlusK(const cell &c, const Vector &V, Matrix &A) const;

  void MPlusK(const Vector &V, Matrix &A) const override {
    A = 0;
    for (cell c = V.cells(); c != V.cells_end(); ++c)
      MPlusK(c, V, A);
  }

  virtual void SystemMatrix(const cell &c, const Vectors &u, Matrix &A) const;

  virtual void SystemMatrix(const Vectors &u, Matrix &A) const {
    A = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      SystemMatrix(c, u, A);
    A.ClearDirichletValues();
  }

  void SystemMatrix(const Vector &u, Matrix &A) const override {
    A = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      SystemMatrix(c, u, A);
    A.ClearDirichletValues();
  };

  virtual void SystemMatrix(const cell &c, const Vector &u, Matrix &A) const {
    THROW("Not implemented for this assemble class!")
  };

};


class MonodomainLINodes : public Monodomain {
  std::unique_ptr<Vector> IionVEC;
  std::unique_ptr<Vector> DVIionVEC;
public:
  MonodomainLINodes(ElphyProblem &elphyProblem, MCellModel &cellModel,
                    int degree, int exactUpTo = -1);

  void addReactionSAndRHS(const cell &c, const Vectors &u, Matrix &S, Vector &r) const override;

  void RHS(const cell &c, const Vectors &u, Vector &r) const override;

  void SystemMatrix(const cell &c, const Vectors &u, Matrix &A) const override;

  void updateIionVecs(const Vectors &VCW) override;

  void updateIion(const Vectors &VCW);

  void updateDVIion(const Vectors &VCW);
};

class MonodomainQuadrature : public Monodomain {
  std::unique_ptr<Vectors> VCWQuadrature;
  std::unique_ptr<Vector> CWcells;
  std::shared_ptr<DoFDiscretization<CellDoF>> CWdisc;
public:
  MonodomainQuadrature(ElphyProblem &elphyProblem, MCellModel &cellModel, const Vector &u,
                       int degree, int exactUpTo = -1);

  void RHS(const cell &c, const Vectors &u, Vector &r) const override;

  void SystemMatrix(const Vectors &u, Matrix &A) const override {
    Monodomain::SystemMatrix(u[0], A);
  }

  void SystemMatrix(const cell &c, const Vector &u, Matrix &A) const override;

  void updateCaAndW(const cell &c, std::vector<double> &vcw, int index) const;
};

//TODO: FullNewton access should be handled in the solver class
class MonodomainFullNewton : public Monodomain {
  std::unique_ptr<Vectors> VACAW;
  //std::unique_ptr<Matrix>  MK;
  //bool assembled=false;
public:
  MonodomainFullNewton(ElphyProblem &elphyProblem,
                       MCellModel &cellModel, int degree, int exactUpTo = -1);

  /* void Initialize(Vector &u, double t, double dt) override{
     IElphyAssemble::Initialize(u,t,dt);
     if(!assembled){
       MPlusK(u,(*MK));
       assembled=true;
     }
   };*/
  void Residual(const cell &c, const Vector &u, Vector &defect) const override;

  //void Jacobi(const Vector &u, Matrix &jacobi) const override;
  void Jacobi(const cell &c, const Vector &u, Matrix &jacobi) const override;

  void updateVCW(const Vectors &VCW) override;

  void Energy(const cell &c, const Vector &u, double &energy) const override {};


  const char *Name() const override { return "FullNewton Monodomain"; };
};

class MonodomainSemiImplicit : public Monodomain {

public:
  MonodomainSemiImplicit(ElphyProblem &elphyProblem, MCellModel &cellModel, int degree,
                         int exactUpTo = -1)
      : Monodomain(
      elphyProblem, cellModel, degree, exactUpTo) {};

  void RHS(const cell &c, const Vectors &u, Vector &r) const override;
};

class MonodomainSemiImplicitNodes : public Monodomain {
  std::unique_ptr<Vector> IionVEC;
public:
  MonodomainSemiImplicitNodes(ElphyProblem &elphyProblem, MCellModel &cellModel, int degree,
                              int exactUpTo = -1)
      : Monodomain(elphyProblem, cellModel, degree, exactUpTo) {
    IionVEC = std::make_unique<Vector>(0.0, GetSharedDisc());

  };

  void RHS(const cell &c, const Vectors &u, Vector &r) const override;

  void updateIionVecs(const Vectors &VCW) override;

};
class MonodomainSemiImplicitCells : public Monodomain {
protected:
  std::shared_ptr<LagrangeDiscretization> disc0;
  std::unique_ptr<Vector> IionCell;
public:
  MonodomainSemiImplicitCells(ElphyProblem &elphyProblem, MCellModel &cellModel, int degree,
                              int exactUpTo = -1)
      : Monodomain(elphyProblem, cellModel, degree, exactUpTo) {
    const Meshes &mesh = GetDisc().GetMeshes();
    disc0 = std::make_shared<LagrangeDiscretization >(mesh, 0, 1);
    IionCell = std::make_unique<Vector>(0.0, disc0);;

  };

  void RHS(const cell &c, const Vector &u,const Vectors &g, Vector &r) const override{
    RHS(c, u, r);
  };
  virtual void RHS(const cell &c, const Vector &u, Vector &r) const override;
  void updateIionVecs(const Vectors &VCW) override;

};
class MonodomainSemiImplicitCellsIextPerCell : public MonodomainSemiImplicitCells {
  std::unique_ptr<Vectors> externalCurrentOnCells;;
  std::unique_ptr<Vector> Iext_cells;
public:
  MonodomainSemiImplicitCellsIextPerCell(ElphyProblem &elphyProblem, MCellModel &cellModel, int degree,
                                 int exactUpTo = -1)
      : MonodomainSemiImplicitCells(elphyProblem, cellModel, degree, exactUpTo) {

  };

  void RHS(const cell &c, const Vector &u, Vector &r) const override;
  void updateExternalCurrent(const Vector &V) override;
  void SetInitialValue(Vector &V) override;

};
class MonodomainSemiImplicitCellsSVI : public Monodomain {
public:
  MonodomainSemiImplicitCellsSVI(ElphyProblem &elphyProblem, MCellModel &cellModel, int degree,
                              int exactUpTo = -1)
      : Monodomain(elphyProblem, cellModel, degree, exactUpTo) {};

  void RHS(const cell &c, const Vector &u,const Vectors &g, Vector &r) const override;

};


#endif