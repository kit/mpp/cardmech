#ifndef IELPHYASSEMBLE_H
#define IELPHYASSEMBLE_H

#include <iostream>
#include "Assemble.hpp"
#include "CardiacAssemble.hpp"
#include "MCellModel.hpp"
#include "problem/ElphyProblem.hpp"
#include "MultiPartDiscretization.hpp"

// Is only Nonlinear to make usage of MonodomainFullNewton possible
class IElphyAssemble : public INonLinearTimeAssemble, public ICardiacAssemble {
protected:
  using IAssemble::verbose;

  ElphyProblem &problem;

  std::unique_ptr<Vector> activationTime;
  std::unique_ptr<Vector> Iext;
  /*std::unique_ptr<Vector> IionVEC;
  std::unique_ptr<Vector> DVIionVEC;*/
  int iextContinuous{0};
  bool isNormIextZero{false};
  double Vthresh{0.0};

  //Discretization
  int degree{1};
  std::shared_ptr<const MultiPartDiscretization> disc;
  std::shared_ptr<const MultiPartDiscretization> dataDisc;
  Vector excData;

  int printVSteps{1};
  double startTime{0.0};
  double timeUpdateIext;
public:
  IElphyAssemble(ElphyProblem &elphyProblem, int degree, int exactUpTo = -1);

  const IDiscretization &GetDisc() const {
    return *disc;
  }

  std::shared_ptr<const IDiscretization> GetSharedDisc() const { return disc; }

  virtual void Initialize(Vector &u) const {};

  virtual void SetInitialValue(Vector &u) override;

  virtual void SetInitialValue(Vectors &values) { SetInitialValue(values[0]); };

  void FinishTimeStep(const Vector &u) override;

  virtual void RHS(const Vector &u, Vector &r) const  {
    THROW("Not implemented in the basis class")
  };

  virtual void RHS(const Vectors &u, Vector &r) const {
    THROW("Not implemented in the basis class")
  };
  virtual void RHS(const Vector &u,const Vectors &g, Vector &r) const {
    THROW("Not implemented in the basis class")
  };

  virtual void RHSMAT(const Vector &u, Matrix &A) const {
    THROW("Not implemented in the basis class")
  };

  virtual void setTheta(double value) { THROW("Not implemented in the basis class") };

  void IExt(const cell &c, const Vector &u, Vector &r) const;

  void IExt(const Vector &u, Vector &r) const {
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      IExt(c, u, r);
    r.Collect();
  };

  virtual void addReaction(const Vectors &u, Matrix &S) const {
    THROW("Not implemented in the basis class")
  };

  virtual void addReactionSAndRHS(const Vectors &u, Matrix &S, Vector &r) const {
    THROW("Not implemented in the basis class")
  };

  virtual void MPlusK(const Vector &V, Matrix &A) const {
    THROW("Not implemented in the basis class")
  };

  virtual void SystemMatrix(const Vector &u, Matrix &A) const = 0;

  virtual void SystemMatrix(const Vectors &u, Matrix &A) const {
    THROW("Not implemented in the basis class")
  };

  void UpdateDeformation(const Vector &u, const Vector &du, const Vector &v) {
    problem.UpdateDeformation(u, du, v);
  }

  virtual void updateExternalCurrent(const Vector &V) = 0;

  virtual void updateIionVecs(const Vectors &VCW) = 0;

  virtual void updateVCW(const Vectors &vcw) {
    THROW("Not implemented in the basis class")
  };

  void PrintInftyNormOnCells(const Vector &V, const std::string &name ) const;
  void PrintL2Norm(const Vector &V, const std::string &name) const;

  void PrintH1Norm(const Vector &V, const std::string &name) const;

  void updateActivationTime(const Vector &V, double time);

  void PrintV(const Vector &V) const;

  void PrintActivationTime() const;

  void PrintMesh(const Vector &u) const;

  void PlotExcitation() const;
  void PlotExcitation(const std::string &name) const;

  void PlotActivation() const;

  void writeActivationTimeFile() const;

  void PlotPotential(Vector &V, const std::string &varname, int step) const;

  Vector &ActivationTime() const;

  const Vector &ExcitationData() const;

  const Vector &ExternalCurrent() const { return *Iext; };

  virtual double  InftyNormOnVertices(const Vector &u ) const;
  virtual double InftyNormOnCells(const Vector &u) const;
  virtual double L2Norm(const Vector &u) const;

  virtual double L2AvgNorm(const Vector &u) const;

  virtual double H1Norm(const Vector &u) const;
  double L2Error(const Vector &V) const;

  void PrintIteration(const Vector &u) const override;

  void PrintVariable(const Vector &var,const std::string &varname) const ;
  void PrintVariableOnCells(const Vector &var,const std::string &varname) const ;
  void PrintCellMidpointsOfEvaluationPoints(const Vector &var)const;

  void PlotIteration(const Vector &u) const override;
  void PlotCa(const Vector &Ca)const;

  void PlotIteration(const Vectors &u, int step=-1) const;
 double getexternalCurrentTime()const {return Time() + timeUpdateIext;}
  const ElphyProblem &GetElphyProblem() const {
    return problem;
  }
};


std::unique_ptr<IElphyAssemble>
CreateElphyAssemble(ElphyProblem &problem, MCellModel &cellModel,
                    const std::string &modelName,
                    int discDegree);

std::unique_ptr<IElphyAssemble>
CreateElphyAssemble(ElphyProblem &problem, MCellModel &cellModel, int discDegree);

#endif