#ifndef _MONODOMAINSPLITTING_HPP_
#define _MONODOMAINSPLITTING_HPP_

#include "IElphyAssemble.hpp"
#include "../problem/ElphyProblem.hpp"
#include "MultiPartDiscretization.hpp"


class MonodomainSplitting : public IElphyAssemble {

  double theta;
protected:
  double C_m;
  double beta;
  bool iextInPDE{false};


public:
  MonodomainSplitting(ElphyProblem &elphyProblem, int degree, int exactUpTo = -1);

  const char *Name() const override { return "MonodomainSplitting"; };

  void InitializeMonodomainParameters();

  void updateExternalCurrent(const Vector &V) override;
  void updateIionVecs(const Vectors &VCW) override {};

  void setTheta(double value) override;
  void RHS(const cell &c, const Vector &u, Vector &r) const;

  void RHS(const Vector &u, Vector &r) const override {
    r = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      RHS(c, u, r);
    //r.ClearDirichletValues();
    r.Collect();
  };

  void RHSMAT(const cell &c, const Vector &u, Matrix &R) const;

  void RHSMAT(const Vector &u, Matrix &R) const override {
    R = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      RHSMAT(c, u, R);
    R.ClearDirichletValues();
  };

  void SystemMatrix(const cell &c, const Vector &u, Matrix &A) const;

  void SystemMatrix(const Vector &u, Matrix &A) const override {
    A = 0;
    for (cell c = u.cells(); c != u.cells_end(); ++c)
      SystemMatrix(c, u, A);;
    //A.ClearDirichletValues();
  };

  void SetInitialValue(Vector &u) override;
};

#endif

