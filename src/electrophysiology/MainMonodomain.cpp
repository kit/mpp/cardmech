#include <discretization/MultiPartDiscretization.hpp>
#include <solvers/ElphySolver.hpp>
#include <solvers/DiffusionSolver.hpp>
#include <solvers/MElphySolver.hpp>
#include <solvers/SplittingSolver.hpp>
#include "MainMonodomain.hpp"
#include "ExternalCurrent.hpp"


MainMonodomain::MainMonodomain() {
  Config::Get("AssembleVerbose", verbose);
  Config::Get("ElphyModel", modelName);
  Config::Get("plevel", plevel);
  Config::Get("ElphyLevel", elevel);
  Config::Get("ElphyPolynomialDegree", discDegree);

  Config::Get("ElphyMeshPart", chamber);

  SelectExternalCurrentFunction();
}

void MainMonodomain::Initialize() {
  // Load Problem
  problem = GetElphyProblem();
  // Initialize elphy specific mesh
  problem->chamberSpecialization(chamber);
  std::vector<int> chambers = getIncludedChambers(chamber);
  auto mCreator = MeshesCreator().
      WithMeshName(problem->GetMeshName()).
      WithIncludeVector(chambers).
      WithLevel(elevel);
  problem->CreateMeshes(mCreator);

  // Initialize evaluation
  problem->InitializeEvaluationPoints();

  // Initialize Assemble
  cellModel = GetElphyModel();
  elphyA = CreateElphyAssemble(*problem, *cellModel, modelName, discDegree);
  auto times = ReadTimeFromConfig();
  elphyA->ResetTime(times[0], times[1], times[2]);

  // Initialize Vectors
  potential = std::make_unique<Vector>(0.0, elphyA->GetSharedDisc());
  problem->SetEvaluationCells(*potential);

  bool printMeshOnly = false;
  Config::Get("PrintMeshOnly", printMeshOnly);
  if (printMeshOnly) {
    elphyA->PrintMesh(*potential);
  }
}

Vector &MainMonodomain::Run() {
  auto elphySolver = GetElphySolver(modelName, *cellModel, elphyA->ExcitationData());
  elphySolver->Method(*elphyA, *potential);

  elphyA->PrintActivationTime();

  if (problem->UpdateActivationTime) {
    return elphyA->ActivationTime();
  } else {
    return *potential;
  }
}

std::vector<double> MainMonodomain::Evaluate(const Vector &solution) {
  mout << "\n" << "==== Monodomain Information ============== \n\n";
  std::string elphyModelClass = "IBTElphyModel";
  Config::Get("ElphyModelClass", elphyModelClass);
  std::string cmName = "FitzHughNagumo";
  if (elphyModelClass == "IBTElphyModel") {
    Config::Get("IBTVentricleModel", cmName);
  } else {
    Config::Get("ElphyModelName", cmName);
  }

  std::string IextSmooth;
  Config::Get("ExternalCurrentSmoothingInTime", IextSmooth);
  std::string IextSpaceSmoothing;
  Config::Get("ExternalCurrentSmoothingInSpace", IextSpaceSmoothing);
  std::string reassembleRHSOnce = "";
  std::string splittingmethod = "";
  std::string order = "";
  Config::Get("OrderStaggeredScheme", order);
  std::string problemEval = problem->Evaluate(solution);
  order = " " + order;
  if (modelName == "Splitting") {
    Config::Get("ElphySplittingMethod", splittingmethod);
    splittingmethod = splittingmethod + " ";
    order = "";
  }
  Config::Get("ReassembleRHSonce", reassembleRHSOnce);
  if (reassembleRHSOnce == "1") {
    reassembleRHSOnce = " reassembled once";
  } else {
    reassembleRHSOnce = "";
  }

  if (verbose > 0) {
    std::string strAtriaDiff = "[" + std::to_string(problem->getDiffusionValues(1,0)) +
                               ", " + std::to_string(problem->getDiffusionValues(1,1))+
                                      ", " + std::to_string(problem->getDiffusionValues(1,2)) + "]";
    std::string strVentricularDiff = "[" + std::to_string(problem->getDiffusionValues(0,0)) +
                               ", " + std::to_string(problem->getDiffusionValues(0,1))+
                               ", " + std::to_string(problem->getDiffusionValues(0,2)) + "]";
    problem->GetMeshes().PrintInfo();
    mout.PrintInfo("Assemble", verbose,
                   PrintInfoEntry("Name", std::string("Monodomain").c_str(), 1),
                   PrintInfoEntry("Problem", problem->Name(), 1),
                   PrintInfoEntry("Cells", PPM->Sum(problem->GetMeshes().fine().CellCount()), 2),
                   PrintInfoEntry("Degrees of Freedom:", solution.pSize(), 2)/*,
                 PrintInfoEntry("Matrix Entries", solution.pMatrixSize())*/);

    mout.PrintInfo("Model and Solver", verbose,
                   PrintInfoEntry("CellModelClass", elphyModelClass, 1),
                   PrintInfoEntry("CellModel", cmName, 1),
                   PrintInfoEntry("dt", elphyA->StepSize(),
                                  1),
                   PrintInfoEntry("T", elphyA->LastTStep(),
                                  1),
                   PrintInfoEntry("Time Integration model", std::string(
                       splittingmethod + modelName + reassembleRHSOnce + order).c_str(), 1));


    std::string inPDE = "true";
    Config::Get("IextInPDE", inPDE);
    mout.PrintInfo("External Stimulus", verbose,
                   PrintInfoEntry("Iext in PDE", inPDE, modelName == "Splitting" ? 1 : 1000),
                   PrintInfoEntry("Iext smooth in time", IextSmooth, 1),
                   PrintInfoEntry("Iext space smoothing", IextSpaceSmoothing,
                                  1));
    mout.PrintInfo("External Stimulus", verbose,
                   PrintInfoEntry("Iext smooth in time", std::string(IextSmooth).c_str(), 1),
                   PrintInfoEntry("Iext space smoothing", std::string(IextSpaceSmoothing).c_str(),
                                  1));
    mout.PrintInfo("Conductivity", verbose,
                   PrintInfoEntry("atria", strAtriaDiff, 1),
                   PrintInfoEntry("ventricles", strVentricularDiff,
                                  1));
    mout << problemEval << endl;
    mout << "===========================================" << endl;
  }
  return problem->EvaluationResults(solution);
}

void MainMonodomain::ResetLevel(int newLevel) {
  elevel = newLevel;
}

double MainMonodomain::EvaluateQuantity(const Vector &solution,
                                        const std::string &quantity) const {
  if (quantity == "L2Error") { return elphyA->L2Error(solution); }
  if (quantity == "L2Norm") { return elphyA->L2Norm(solution); }
  if (quantity == "H1Norm") { return elphyA->H1Norm(solution); }
  else { return infty; }
}
