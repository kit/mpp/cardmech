#ifndef MAINMONECELL_H
#define MAINMONECELL_H

#include "MainCardMech.hpp"
#include "solvers/LinearImplicitSolver.hpp"


class MainOneCell : public MainCardMech {
  std::unique_ptr<ElphyProblem> problem = nullptr;
  std::unique_ptr<TimeSeries> tSeries = nullptr;
  std::unique_ptr<Meshes> mesh = nullptr;
  std::unique_ptr<IDiscretization> elphyDisc = nullptr;
  std::unique_ptr<IElphyAssemble> elphyA = nullptr;
  std::unique_ptr<VtuPlot> plot;

  std::unique_ptr<Vector> potential = nullptr;
  std::unique_ptr<Vector> activationTime = nullptr;


public:

  bool plotSolution = false;
  bool dualPrimalError = false;


  MainOneCell() {
    Config::Get("Model", modelName);

  }

  void Initialize() override;

  Vector &Run() override;

  std::vector<double> Evaluate(const Vector &) override;
};


#endif
