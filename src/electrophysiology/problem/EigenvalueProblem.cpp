#include "EigenvalueProblem.hpp"

constexpr std::array<double, 1> potentialValues{-84.584430};


std::string EigenvalueProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, {Point(0.0, 0.0)});

  evaluation += "V(0.0,0.0) = " + std::to_string(values[0]) + "\n";

  return evaluation;
}

std::vector<double> EigenvalueProblem::EvaluationResults(const Vector &solution) const {
  auto value = getEvaluationAt(solution, {Point(0.0, 0.0)});
  value[0] -= potentialValues[0];
  return value;
}

void EigenvalueProblem::InitializeEvaluationPoints() {
  evaluationPoints.emplace_back(Point(0.0, 0.0));//p1
}
