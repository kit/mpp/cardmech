#include "DiffusionProblem.hpp"

#include <numbers>
#include <utility>

DiffusionProblem::DiffusionProblem(std::array<double, 9> entries) :
    ElphyProblem(false),
    conductivity(entries[0], entries[1], entries[2],
                 entries[3], entries[4], entries[5],
                 entries[6], entries[7], entries[8]) {
  Config::Get("ProblemDimension", spaceDim);
  Config::Get("ProblemGeometry", isTet);

  if (!meshFromConfig()) {
    meshesName = "UnitBlock" + std::to_string(spaceDim) + "D" + (isTet ? "Tet" : "Quad");
  }
}

std::vector<double> DiffusionProblem::EvaluationResults(
    const Vector &solution) const {
  std::vector<double> eval{};

  Vector error(solution);
  for (row r = solution.rows(); r != solution.rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      error(r, j) -= Potential(0.5, r());
    }
  }
  eval.emplace_back(error.norm() / error.rowsize());

  return eval;
}

Tensor DiffusionProblem::Conductivity(const Cell &c) const {
  return conductivity;
}

DiffusionExampleOne::DiffusionExampleOne() : DiffusionProblem(
    {1, 0.5, 0.5, 0.5, 1.0, 0.5, 0.5, 0.5, 1.0}) {}

double DiffusionExampleOne::Potential(double time, const Point &x) const {
  using std::numbers::pi;
  return 0.25 * (1 - sin(time) * sin(time)) * (1 - cos(2 * pi * x[0])) *
         (1 - cos(2 * pi * x[1]));
}

double DiffusionExampleOne::IonCurrent(double time, const Point &x) const {
  return timeDerivative(time, x)
         - (1 - sin(time) * sin(time))
           * (divergence(0, x) + divergence(1, x) + divergence(2, x));
}

double DiffusionExampleOne::timeDerivative(double time, const Point &x) const {
  using std::numbers::pi;
  return -0.5 * (sin(time) * cos(time)) * (1 - cos(2 * pi * x[0])) *
         (1 - cos(2 * pi * x[1]));
}

double DiffusionExampleOne::divergence(int row, const Point &x) const {
  using std::numbers::pi;
  int i = row;
  int j = (row + 1) % SpaceDimension;
  int k = (row + 2) % SpaceDimension;
  return pi * pi *
         (conductivity[i][i] * cos(2 * pi * x[i]) * (1 - cos(2 * pi * x[j])) *
          (1 - cos(2 * pi * x[k]))) *
         (conductivity[i][j] * sin(2 * pi * x[j]) * (1 + sin(2 * pi * x[i])) *
          (1 - cos(2 * pi * x[k]))) *
         (conductivity[i][k] * sin(2 * pi * x[k]) * (1 + sin(2 * pi * x[i])) *
          (1 - cos(2 * pi * x[j])));
}

DiffusionExampleTwo::DiffusionExampleTwo() : DiffusionProblem(
    {1, 0.5, 0.5, 0.5, 1.0, 0.5, 0.5, 0.5, 1.0}) {}

double DiffusionExampleTwo::Potential(double time, const Point &x) const {
  double v =
      256 * (1 - sin(time) * sin(time)) * x[0] * x[0] * (x[0] - 1) * (x[0] - 1) * x[1] * x[1] *
      (x[1] - 1) * (x[1] - 1) * x[2] * x[2] * (x[2] - 1) * (x[2] - 1);
  double g = 0.5 * (1 + cos(std::numbers::pi * (x[1] - 0.5)));
  return v * g;
}

double DiffusionExampleTwo::IonCurrent(double time, const Point &x) const {
  return ElphyProblem::IonCurrent(time, x);
}

double DiffusionExampleTwo::timeDerivative(double time, const Point &x) const {
  return 0;
}

double DiffusionExampleTwo::divergence(int row, const Point &x) const {
  return 0;
}
