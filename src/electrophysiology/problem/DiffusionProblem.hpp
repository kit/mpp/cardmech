#ifndef DIFFUSIONPROBLEM_HPP
#define DIFFUSIONPROBLEM_HPP

#include "ElphyProblem.hpp"

class DiffusionProblem : public ElphyProblem {
protected:
  Tensor conductivity{};

  int spaceDim{2};
  bool isTet{true};
public:
  explicit DiffusionProblem(std::array<double, 9> entries);

  std::string Name() const override {
    return "Diffusion";
  }

  std::vector<double> EvaluationResults(const Vector &solution) const override;

  Tensor Conductivity(const Cell &c) const override;
};

class DiffusionExampleOne : public DiffusionProblem {
  double timeDerivative(double time, const Point &x) const;

  double divergence(int row, const Point &x) const;

public:
  DiffusionExampleOne();

  double Potential(double time, const Point &x) const override;

  double IonCurrent(double time, const Point &x) const override;
};

class DiffusionExampleTwo : public DiffusionProblem {
  double timeDerivative(double time, const Point &x) const;

  double divergence(int row, const Point &x) const;

public:
  DiffusionExampleTwo();

  double Potential(double time, const Point &x) const override;

  double IonCurrent(double time, const Point &x) const override;
};

#endif //DIFFUSIONPROBLEM_HPP
