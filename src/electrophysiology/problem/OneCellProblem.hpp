#ifndef ONECELLPROBLEM_HPP
#define ONECELLPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class OneCellProblem : public ElphyProblem {
public:
  explicit OneCellProblem() : ElphyProblem(false) {
    //SetSigma(0,0.0);
    //SetSigma(1,0.0);
    if (!meshFromConfig()) {
      meshesName = "HexaTestCube025";
    }
  }

  std::string Name() const override{
    return "OneCellProblem";
  }

  void InitializeEvaluationPoints() override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;

  std::string Evaluate(const Vector &solution) const override;

};


#endif //ELPHYNODIFFUSIONPROBLEM_HPP
