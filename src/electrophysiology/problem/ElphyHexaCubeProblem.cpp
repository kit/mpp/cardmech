#include "ElphyHexaCubeProblem.hpp"


constexpr std::array<double, 2> potentialValues{-84.584430, -85.298003};


std::string ElphyHexaCubeProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, {Point(0.0, 0.0, 0.0), Point(5.0, 3.0, 3.0)});

  evaluation += "V(0.0,0.0,0.0) = " + std::to_string(values[0]) + "\n";
  evaluation += "V(5.0,3.0,3.0) = " + std::to_string(values[1]) + "\n";

  return evaluation;
}

std::vector<double> ElphyHexaCubeProblem::EvaluationResults(const Vector &solution) const {
  auto values = getEvaluationAt(solution, {Point(0.0, 0.0, 0.0), Point(5.0, 3.0, 3.0)});

  for (int i = 0; i < values.size(); ++i) {
    values[i] -= potentialValues[i];
  }
  return values;
}

void ElphyHexaCubeProblem::InitializeEvaluationPoints() {
  evaluationPoints.emplace_back(Point(0.0, 0.0, 0.0));//p1
  evaluationPoints.emplace_back((Point(1.0, 0.5, 0.5))); //p3
  evaluationPoints.emplace_back((Point(1.75, 1.0, 1.0)));//p5
  evaluationPoints.emplace_back(Point(2.5, 1.5, 1.5));//p7-Mittelpunkt
  evaluationPoints.emplace_back(Point(3.25, 2.0, 2.0));//p9
  evaluationPoints.emplace_back(Point(4.25, 2.5, 2.5));//p11
  evaluationPoints.emplace_back(Point(5.0, 3.0, 3.0));//p13
  evaluationPoints.emplace_back(Point(10.0/3.0,7.0/3.0,7.0/3.0));//besides grid

}



/*, p1(0.0, 0.0, 0.0), p2(0.5, 0.25, 0.25 ),
p3( 1.0,0.5, 0.5 ), p4(1.25,0.75, 0.75), p5( 1.75,1.0,1.0), p6(2.0, 1.25,1.25),
p7( 2.5,1.5,1.5), p8( 3.0,1.75,1.75), p9( 3.25,2.0,2.0), p10(3.75,2.25,2,25 ),
p11(4.25,2.5,2.5), p12(4.5,2.75,2.75), p13(5.0,3.0,3.0), p14(0.0,3.0,0.0), p15(5.0,0.0,0.0),
p16(5.0,3.0,0.0), p17(0.0,0.0,3.0), p18(0.0,3.0,3.0), p19(5.0,0.0,3.0), p20(5.0,1.5,1.5)*/