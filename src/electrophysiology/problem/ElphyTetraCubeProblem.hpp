#ifndef ELPHYTETRACUBEPROBLEM_HPP
#define ELPHYTETRACUBEPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class ElphyTetraCubeProblem : public ElphyProblem {

public:
  explicit ElphyTetraCubeProblem() : ElphyProblem(false) {
    if (!meshFromConfig()) {
      meshesName = "TestCube0253DTet";
    }
  }

  std::string Name() const override{
    return "TetraTestProblem";
  }

  void InitializeEvaluationPoints() override;

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;


};


#endif //ELPHYNODIFFUSIONPROBLEM_HPP
