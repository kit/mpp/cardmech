#ifndef ELPHYNODIFFUSIONPROBLEM_HPP
#define ELPHYNODIFFUSIONPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class ElphyNoDiffusionProblem : public ElphyProblem {
public:
  ElphyNoDiffusionProblem() : ElphyProblem(false, {0.0, 0.0, 1.0, 1.0}) {

    if (!meshFromConfig()) {
      meshesName = "TestCube0253DQuad";
    }
  }

  std::string Name() const override{
    return "WithoutDiffusion";
  }

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;

  void InitializeEvaluationPoints() override;


};

#endif //ELPHYNODIFFUSIONPROBLEM_HPP
