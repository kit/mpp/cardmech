//
// Created by laurapfeiffer on 09.02.21.
//

#include "ElphyBenchmarkProblem.hpp"

void ElphyBenchmarkProblem::InitializeEvaluationPoints() {
  evaluationPoints.emplace_back(Point(0.0, 0.0, 0.0));
  evaluationPoints.emplace_back(Point(0.0, 7.0, 0.0));
  evaluationPoints.emplace_back(Point(20.0, 0.0, 0.0));
  evaluationPoints.emplace_back(Point(20.0, 7.0, 0.0));
  evaluationPoints.emplace_back(Point(0.0, 0.0, 3.0));
  evaluationPoints.emplace_back(Point(0.0, 7.0, 3.0));
  evaluationPoints.emplace_back(Point(20.0, 0.0, 3.0));
  evaluationPoints.emplace_back(Point(20.0, 7.0, 3.0));
  evaluationPoints.emplace_back(Point(10.0, 3.5, 1.5));
  //Eckpunkte der Zelle P(2.125,    2.75,    2.875)
  /*evaluationPoints.emplace_back(Point(2, 2.5, 2.5));
  evaluationPoints.emplace_back(Point(2, 3, 3));
  evaluationPoints.emplace_back(Point(2, 2.5, 3));
  evaluationPoints.emplace_back(Point(2.5, 3, 3));*/

  evaluationPoints.emplace_back(Point(0.125, 0.75, 0.875));
  evaluationPoints.emplace_back(Point(2.125,    2.75,    2.875));
  evaluationPoints.emplace_back(Point(10.125,    2.75,    1.875));
  evaluationPoints.emplace_back(Point(19.75, 6.625, 2.875));

}
