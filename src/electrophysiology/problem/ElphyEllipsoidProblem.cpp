#include "ElphyEllipsoidProblem.hpp"


constexpr std::array<double, 2> potentialValues{-84.584430, -85.298003};


std::string ElphyEllipsoidProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, {Point(0.0, 0.0, -17.0),
                                           Point(0.8833796382, 3.280038834, -16.95954514)});

  evaluation +=
      "V(0.0, 0.0, -17.0) = " + std::to_string(values[0]) + "\n";
  evaluation +=
      "V(0.8833796382, 3.280038834, -16.95954514) = " + std::to_string(values[1]) +
      "\n";

  return evaluation;
}

std::vector<double> ElphyEllipsoidProblem::EvaluationResults(const Vector &solution) const {
  auto values = getEvaluationAt(solution, {Point(9.831800928949974e-07, -2.8855842715280744e-07,
                                                 -16.998758539557457)});

  for (int i = 0; i < values.size(); ++i) {
    values[i] -= potentialValues[i];
  }
  return values;
}

void ElphyEllipsoidProblem::InitializeEvaluationPoints() {

  if (meshesName == "EllipsoidExcitationApex") {
    evaluationPoints.emplace_back(Point(9.831800928949974e-07, -2.8855842715280744e-07,
                                        -16.998758539557457));//Apex endocard excited
    evaluationPoints.emplace_back(Point(-2.2469761315733194, -6.578279193490744,
                                        -14.376446604728699));//Punkt relativ weit unten am epicard
    //evaluationPoints.emplace_back(Point(5.845959298312664, 4.74184425547719, -8.508824743330479));//Punkt innenwand angeregt wenn 0.03s
    evaluationPoints.emplace_back(Point(4.36374160926789, -6.76172971725464,
                                        -5.95058128237724));//Punkt in Innenwand erst ab Level 1;
    evaluationPoints.emplace_back(Point(-3.0740033835172653, 7.421300746500492, 4.999999888241291));
    evaluationPoints.emplace_back(
        Point(8.945425041019917, 3.7053164560347795, 4.999999888241291)); //Oben Rand exc=-10
    evaluationPoints.emplace_back(Point(9.831800928949974e-07, -2.8855842715280744e-07,
                                        -17.998758539557457));//besides grid next to P0
    evaluationPoints.emplace_back(Point(-2.2469761315733194, -5.578279193490744,
                                        -14.376446604728699));// besides grid next to P1
    evaluationPoints.emplace_back(Point(3.77902, -4.31901, -13.5944));//besides Grid in Wall
  }
  else if (meshesName == "Ellipsoid2Layer"|| meshesName=="Ellipsoid2LayerOriented") {
    evaluationPoints.emplace_back(Point(0.0, 0.0, -17.0));//Apex endocard excited
    evaluationPoints.emplace_back(Point(0.0, 0.0,-20.0));//Apex epicard not excited
    evaluationPoints.emplace_back(Point(-0.8833796509616111, -3.280038756274156, -16.95954518286376));//Point in wall  not excited
    evaluationPoints.emplace_back(Point(0.2, 0.2, -17.4));//besides grid Apex endocard excited
    evaluationPoints.emplace_back(Point(0.1, 0.1, -19.8));//besides grid Apex epicard not excited
    evaluationPoints.emplace_back(Point(-0.9833, -3.3, -16.2));//besides grid Point in wall  not excited
  }
  else if (meshesName=="CuttedEllipsoid2LayerOriented"||meshesName=="TestEllipsoid3dExcitation") {
    evaluationPoints.emplace_back(Point(0.0, 0.0, -17.0));//Apex endocard excited
    evaluationPoints.emplace_back(Point(0.0, 0.0,-20.0));//Apex epicard not excited
    //evaluationPoints.emplace_back(Point(-0.8833796381950378, -3.280038833618164, -16.959545135498047));//Point in wall  not excited
    evaluationPoints.emplace_back(Point(0.8833796382, 3.280038834, -16.95954514));
    evaluationPoints.emplace_back(Point(0.2, 0.2, -17.4));//besides grid Apex endocard excited
    evaluationPoints.emplace_back(Point(0.1, 0.1, -19.8));//besides grid Apex epicard not excited
    evaluationPoints.emplace_back(Point(-0.9833, -3.3, -16.2));//besides grid Point in wall  not excited
    evaluationPoints.emplace_back(Point(-1.6, 4.5,-15.8));
  }else {
    Warning("No evaluation points for this mesh")
  }
}



