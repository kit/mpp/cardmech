#ifndef CARDMECH_ELPHYELLIPSOIDPROBLEM_HPP
#define CARDMECH_ELPHYELLIPSOIDPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class ElphyEllipsoidProblem : public ElphyProblem {
public:
  explicit ElphyEllipsoidProblem() : ElphyEllipsoidProblem(false) {}

  explicit ElphyEllipsoidProblem(bool plotActivation) : ElphyProblem(plotActivation) {
    if (!meshFromConfig()) {
      meshesName = "TestEllipsoid3dExcitation";
    }

    if (meshesName == "CuttedEllipsoid2LayerOriented" || meshesName == "TestEllipsoid3dExcitation")
      SetNumberOfEvaluationPointsBesideMesh(4);
    else
      SetNumberOfEvaluationPointsBesideMesh(3);


  }

  std::string Name() const override {
    return "Ellipsoid";
  }

  void InitializeEvaluationPoints() override;

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;


};

class ElphyEllipsoidWAProblem : public ElphyEllipsoidProblem {
public:
  explicit ElphyEllipsoidWAProblem() : ElphyEllipsoidProblem(true) {};
};

#endif //CARDMECH_ELPHYNODIFFUSIONPROBLEM_HPP
