#ifndef EIGENVALUEPROBLEM_HPP
#define EIGENVALUEPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class EigenvalueProblem : public ElphyProblem {
public:
  explicit EigenvalueProblem() : ElphyProblem(false) {
    //SetSigma(0,0.0);
    //SetSigma(1,0.0);

    if (!meshFromConfig()) {
      meshesName = "HexaTestCube025";
    }
  }

  std::string Name() const override{
    return "EigenvalueProblem";
  }

  void InitializeEvaluationPoints() override;

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;

};


#endif //ELPHYNODIFFUSIONPROBLEM_HPP
