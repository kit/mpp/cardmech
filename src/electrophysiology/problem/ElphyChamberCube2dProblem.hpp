#ifndef ELPHYCHAMBERCUBE2DPROBLEM_HPP
#define ELPHYCHAMBERCUBE2DPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class ElphyChamberCube2dProblem : public ElphyProblem {
public:
  explicit ElphyChamberCube2dProblem() : ElphyProblem(false) {
    diffusionValues[1] = diffusionValues[0];

    if (!meshFromConfig()) {
      meshesName = "ChamberCube0252DQuad";
    }
  }

  std::string Name() const override{
    return "ChamberCube2dProblem";
  }

  void InitializeEvaluationPoints() override;

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;


};


#endif //ELPHYNODIFFUSIONPROBLEM_HPP
