//
// Created by laurapfeiffer on 09.02.21.
//
#include "ElphyCubeWithActivationProblem.hpp"

constexpr std::array<double, 2> activationTimes{1.88, 5.32};

std::string ElphyCubeWithActivationTimeProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, {Point(0.0, 0.0, 0.0), Point(2.5, 1.5, 0.0)});

  evaluation += "ActivationTime(0.0,0.0,0.0) = " + std::to_string(values[0]) + "\n";
  //evaluation += "ActivationTime(2.5,1.5,1.5) = " + std::to_string(values[1]) + "\n";
  evaluation += "ActivationTime(2.5,1.5,0.0) = " + std::to_string(values[1]) + "\n";

  return evaluation;
}

std::vector<double>
ElphyCubeWithActivationTimeProblem::EvaluationResults(const Vector &solution) const {
  auto values = getEvaluationAt(solution, {Point(0.0, 0.0, 0.0), Point(2.5, 1.5,0.0)});

  for (int i = 0; i < values.size(); ++i) {
    //std::cout<<"value of "<<i<<": "<<values[i] <<" activation time: "<<activationTimes[i]<<endl;
    values[i] -= activationTimes[i];
  }
  return values;
}

void ElphyCubeWithActivationTimeProblem::InitializeEvaluationPoints() {
  evaluationPoints.emplace_back(Point(0.0, 0.0, 0.0));
  evaluationPoints.emplace_back(Point(2.5, 1.5, 0.0));
}