#include "VariableMeshProblem.hpp"


constexpr std::array<double, 2> potentialValues{-84.584430, -85.298003};


std::string VariableMeshProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, {Point(0.0, 0.0,0.0),
                                           Point(1.5,1.0,0.0)});

 // evaluation += "V(-31.7229, -18.4416, 31.5543) = " + std::to_string(values[0]) + "\n";
  //evaluation += "V(-29.4811, 15.0629, 18.6768) = " + std::to_string(values[1]) + "\n";
  evaluation += "V(0.0, 0.0) = " + std::to_string(values[0]) + "\n";
  evaluation += "V(1.5,1.0) = " + std::to_string(values[1]) + "\n";

  return evaluation;
}

std::vector<double> VariableMeshProblem::EvaluationResults(const Vector &solution) const {
  auto values = getEvaluationAt(solution, {Point(-31.7229, -18.4416, 31.5543),
                                           Point(-29.4811, 15.0629, 18.6768)});

  for (int i = 0; i < values.size(); ++i) {
    values[i] -= potentialValues[i];
  }
  return values;
}

void VariableMeshProblem::InitializeEvaluationPoints() {
  evaluationPoints.emplace_back(Point(1.5,1.0,0.0));
  evaluationPoints.emplace_back(Point(3.0,1.5,0.0));
  evaluationPoints.emplace_back(Point(3.25,1.5));
  evaluationPoints.emplace_back(Point(0.0, 0.0));//p1
  evaluationPoints.emplace_back(Point(1.0,0.5)); //p3
  evaluationPoints.emplace_back(Point(4.25,2.5));//p11
  evaluationPoints.emplace_back(Point(5.0,3.0));//p13
  evaluationPoints.emplace_back(Point(-29.6084, -66.4513, 29.9516));
  evaluationPoints.emplace_back(Point(78.444, -57.4689, -46.2186));//in Wand linkes Ventrikel
  evaluationPoints.emplace_back(Point(-29.4811, 15.0629, 18.6768));
}


/*, p1(0.0, 0.0, 0.0), p2(0.5, 0.25, 0.25 ),
p3( 1.0,0.5, 0.5 ), p4(1.25,0.75, 0.75), p5( 1.75,1.0,1.0), p6(2.0, 1.25,1.25),
p7( 2.5,1.5,1.5), p8( 3.0,1.75,1.75), p9( 3.25,2.0,2.0), p10(3.75,2.25,2,25 ),
p11(4.25,2.5,2.5), p12(4.5,2.75,2.75), p13(5.0,3.0,3.0), p14(0.0,3.0,0.0), p15(5.0,0.0,0.0),
p16(5.0,3.0,0.0), p17(0.0,0.0,3.0), p18(0.0,3.0,3.0), p19(5.0,0.0,3.0), p20(5.0,1.5,1.5)*/

