#ifndef ELPHYPROBLEM_H
#define ELPHYPROBLEM_H

#include "utility/Config.hpp"

#include "CardMechIProblem.hpp"
#include "CardiacProblem.hpp"

#include "CardiacData.hpp"


static std::array<double, 4> ConductivityFromConfig() {
  std::array<double, 4> sigma{0.17, 0.019, 0.62, 0.24};
  Config::Get("IntraLongitudinal", sigma[0]);
  Config::Get("IntraTransversal", sigma[1]);
  Config::Get("ExtraLongitudinal", sigma[2]);
  Config::Get("ExtraTransversal", sigma[3]);
  return sigma;
}

struct EvaluationPointData {
  const cell c;
  const Point P;
  const int index;
};

static std::vector<double> findPotential(const Vector &u,
                                         const std::vector<Point> &points) {
  std::vector<double> potentials(points.size());
  for (row r = u.rows(); r != u.rows_end(); ++r) {
    auto [i, isNear] = isIn(r(), points);
    if (isNear) {
      auto p = u.find_procset(r());
      if (p != u.procsets_end()) {
        potentials[i] = u(r, 0) / p.size();
      } else {
        potentials[i] = u(r, 0);
      }

    }
  }
  for (int i = 0; i < potentials.size(); ++i) {
    potentials[i] = PPM->Sum(potentials[i]);
  }
  return potentials;
}

class IElphyProblem {
protected:
  std::vector<Point> evaluationPoints{};
  int numberEvaluationPointsBesideMesh = 0;
  std::vector<EvaluationPointData> cellsBesideMeshPoints{};
  std::array<double, 4> sigma{};
  /// Stores the diagonal entries of the diffusion tensor for the ventricles (0) and atria (1).
  std::array<VectorField, 2> diffusionValues{};
  bool withGerachDiffusion=false;
public:
  bool UpdateActivationTime{true};

  virtual void InitializeEvaluationPoints() {}

  virtual const std::vector<Point> GetEvaluationPoints() const { return evaluationPoints;};

  explicit IElphyProblem(bool at = true) : IElphyProblem(at, ConductivityFromConfig()) {};

  IElphyProblem(bool at, std::array<double, 4> sigmaParams)
      : sigma(sigmaParams), UpdateActivationTime(at) {

    double sigma_l = (sigma[0] * sigma[2]) / (sigma[0] + sigma[2]);
    double sigma_t = (sigma[1] * sigma[3]) / (sigma[1] + sigma[3]);
    //Divide for correct Units from 1/m to 1/mm
    diffusionValues[0] = (1.0 / 1000.0) * VectorField(sigma_l, sigma_t, sigma_t);
    diffusionValues[1] = (2.0 / 1000.0) * VectorField(2.2188 * 0.251, 0.251,0.251); //TODO scaling mit 2 automatisieren

    Config::Get("WithGerachDiffsion",withGerachDiffusion);  //aktuell hier nur für Kopplung verwendet. Wird die Elektrophysiologie alleine verwendet, sind die Werte für die Diffusion in den Problems festgehalten
    if(withGerachDiffusion){
      diffusionValues[0] =0.001 * VectorField(0.28, 0.182, 0.098); // Wie im Paper von Gerach und Loewe
    }
  }


  bool Includes(const cell &c, const Point P);

  virtual double Potential(double time, const Point &x) const { return 0.0; }

  virtual double IonCurrent(double time, const Point &x) const { return 0.0; }

  virtual void UpdateDeformation(const Vector &u, const Vector &du, const Vector &v) {}

  virtual double Jacobian(const Cell &c) const { return 1.0; }

  virtual double Jacobian_old(const Cell &c) const { return 1.0; }

  virtual double Jacobian_rate(const Cell &c) const { return 1.0; }

  virtual double DJacobian(double dt, const Cell &c) const { return 1.0; }

  virtual void PrintMinMaxJacobian(const Vector &v) const {}
  virtual double getDiffusionValues(int i,int j) const{return diffusionValues[i][j];}
  virtual Tensor Conductivity(const Cell &c, const Tensor &F) const {

    int domainPart = DomainPart(c);
    auto fibre = F * Direction(c);
    auto sheet = F * Sheet(c);
    auto normal = F * Normal(c);
    auto S_f = Product(fibre, fibre);
    auto S_s = Product(sheet, sheet);
    auto S_n = Product(normal, normal);
    auto nS_f = fibre * fibre;
    auto nS_s = sheet * sheet;
    auto nS_n = normal * normal;
    auto sigma_f = diffusionValues[domainPart][0];
    auto sigma_s = diffusionValues[domainPart][1];
    auto sigma_n = sigma_s;
    return (sigma_f/nS_f) * S_f + (sigma_s/nS_s) * S_s + (sigma_n/nS_n) * S_n;

    //alte Version
    /*int domainPart = DomainPart(c);
    auto fibre = F*Direction(c);
    auto S = Product(fibre, fibre);
    double normFfsquared=norm(fibre)*norm(fibre);
    return (1/normFfsquared)*(diffusionValues[domainPart][1] * One
           + (diffusionValues[domainPart][0] - diffusionValues[domainPart][1]) * S);*/

  }
  virtual Tensor Conductivity(const Cell &c) const {
    int domainPart = DomainPart(c);
    auto fibre = Direction(c);
    auto S = Product(fibre, fibre);
    return diffusionValues[domainPart][1] * One
           + (diffusionValues[domainPart][0] - diffusionValues[domainPart][1]) * S;

  }

  std::vector<double>
  getEvaluationAt(const Vector &solution, const std::vector<Point> points) const;

  const int getSizeEvaluationPoints() const { return evaluationPoints.size(); };

  const Point &getEvaluationPointAt(int i) const { return evaluationPoints.at(i); }

  virtual void chamberSpecialization(const std::string &chamberName) const {}

  void
  SetNumberOfEvaluationPointsBesideMesh(int number) { numberEvaluationPointsBesideMesh = number; };

  const int
  getNumberEvaluationPointsBesideMesh() const { return numberEvaluationPointsBesideMesh; };

  const int getSizeCellsEvaluationPoints() const { return cellsBesideMeshPoints.size(); };

  void SetEvaluationCells(const Vector &V);

  const EvaluationPointData &getCellAt(int i) const { return cellsBesideMeshPoints.at(i); };
};

class ElphyProblem : public IElphyProblem, public CardMechIProblem {
protected:
  bool meshFromConfig() const { return meshesName != ""; }

public:
  ElphyProblem() : CardMechIProblem(), IElphyProblem() {}

  explicit ElphyProblem(bool at) : CardMechIProblem(), IElphyProblem(at) {}

  ElphyProblem(bool at, std::array<double, 4> sigmaParams)
      : CardMechIProblem(), IElphyProblem(at, sigmaParams) {}

  [[nodiscard]] const std::string &GetMeshName() const { return meshesName; }

  std::string Name() const override { return "Default Elphy Problem"; }

  [[nodiscard]] virtual std::string Evaluate(const Vector &solution) const { return ""; };

  [[nodiscard]] virtual std::vector<std::string> EvaluationMetrics() const { return {}; }

  [[nodiscard]] virtual std::vector<double>
  EvaluationResults(const Vector &solution) const { return std::vector<double>{}; }
};


class MonoDomainProblem : public ElphyProblem {
public:
  MonoDomainProblem(const std::string &meshName) : ElphyProblem(false) {
    if (!meshFromConfig()) {
      meshesName = meshName;
    }
  };

  void InitializeEvaluationPoints() override;

  std::string Name() const override { return "Monodomain"; };
};


std::unique_ptr<ElphyProblem> GetElphyProblem();

std::unique_ptr<ElphyProblem> GetElphyProblem(const std::string &problemName);

#endif //ELPHYPROBLEM_H
