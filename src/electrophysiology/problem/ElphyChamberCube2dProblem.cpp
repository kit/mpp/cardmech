#include "ElphyChamberCube2dProblem.hpp"

constexpr std::array<double, 2> potentialValues{12.52738402, -84.57368826};

std::string ElphyChamberCube2dProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, {Point(0.0, 0.0), Point(5.0, 3.0)});

  evaluation += "V(0.0,0.0) = " + std::to_string(values[0]) + "\n";
  evaluation += "V(5.0,3.0) = " + std::to_string(values[1]) + "\n";

  return evaluation;
}

std::vector<double> ElphyChamberCube2dProblem::EvaluationResults(const Vector &solution) const {
  auto values = getEvaluationAt(solution, {Point(0.0, 0.0), Point(5.0, 3.0)});

  for (int i = 0; i < values.size(); ++i) {
    values[i] -= potentialValues[i];
  }
  return values;
}

void ElphyChamberCube2dProblem::InitializeEvaluationPoints() {
// evaluationPoints.emplace_back(Point(0.0, 0.0));//p1
  //evaluationPoints.emplace_back(Point(1.0, 0.5)); //p3
  evaluationPoints.emplace_back(Point(3.0, 1.5));//p5
  //evaluationPoints.emplace_back(Point(3.0, 2.5));//p7-Mittelpunkt
  //evaluationPoints.emplace_back(Point(3.25, 2.0));//p9
  //evaluationPoints.emplace_back(Point(4.25, 2.5));//p11
  //evaluationPoints.emplace_back(Point(5.0, 3.0));//p13
}

