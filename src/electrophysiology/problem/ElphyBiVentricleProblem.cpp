#include "ElphyBiVentricleProblem.hpp"


constexpr std::array<double, 2> potentialValues{-84.584430, -85.298003};


std::string ElphyBiVentricleProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, {Point(74.837097, -81.447998, -44.074902)});

  evaluation += "V(74.837097, -81.447998, -44.074902) = " + std::to_string(values[0]) + "\n";
  //evaluation += "V(26.297, 10.486, 7.529) = " + std::to_string(values[1]) + "\n";

  return evaluation;
}

std::vector<double> ElphyBiVentricleProblem::EvaluationResults(const Vector &solution) const {
  auto values = getEvaluationAt(solution, {Point(74.837097, -81.447998, -44.074902)});

  for (int i = 0; i < values.size(); ++i) {
    values[i] -= potentialValues[i];
  }
  return values;
}

void ElphyBiVentricleProblem::InitializeEvaluationPoints() {
  if (meshesName == "BiVentricle_smoothed" || meshesName =="BiVentricle_unsmoothed "){
    evaluationPoints.emplace_back(Point(74.8371, -81.448, -44.0749));// Apex excited left ventricle
    evaluationPoints.emplace_back(Point(74.3271, -86.7963 ,-49.0176));//Apex midwall left ventricle
    //all in one plane:
    evaluationPoints.emplace_back(Point(39.6117, -83.0804, -34.9477));// Innenwand  between ventricles bottom
    evaluationPoints.emplace_back(Point(18.0078, -63.9593, -19.1492));// Innenwand  between ventricles middle
    evaluationPoints.emplace_back(Point(-0.33377, -44.2081, -7.27446));// Innenwand  between ventricles top
    evaluationPoints.emplace_back(Point(64.5504, -27.8744, -25.5657));// Innenwand Mitte linkes Ventrikel
    evaluationPoints.emplace_back(Point(-8.8439, -95.5179, -19.522)); //Innenwand Mitte rechtes Ventrikel
    evaluationPoints.emplace_back(Point(-16.0075, -47.811, 4.33503)); //on top of right ventricle with late activation
    //cell evaluation points
    evaluationPoints.emplace_back(Point(76.73467500, -82.05392500, -44.18785000));//l=0

    //besides Mesh evaluation:
    //evaluationPoints.emplace_back(Point(76.7346, -82.0539, -44.1879));







  }
  if (meshesName == "KovachevaBiventricle") {
    evaluationPoints.emplace_back(Point(86.75, -44.2302, 1.74331));// Apex excited left ventricle
    evaluationPoints.emplace_back(Point(22.5562, -16.1836, 51.2538));// midwall top excitation time =0
    evaluationPoints.emplace_back(Point(55.1275, -62.8303, -2.31392));//right ventricle bottom outer face excitation time =0
//on level1:
    evaluationPoints.emplace_back(Point(31.66030000, -22.52370000, 43.47850000));//inner midwall top
    evaluationPoints.emplace_back(Point(40.97000000, -27.82665000, 32.66370000));//inner midwall lower
    evaluationPoints.emplace_back(Point(49.55725000, -35.89105000, 20.56925000));//inner midwall even lower
    evaluationPoints.emplace_back(Point(57.53240000, -41.66090000, 10.48984000));//inner midwall next apex
    evaluationPoints.emplace_back(Point(90.67820000, -6.75337500, 35.58070000));//left ventricle in wall middle
    evaluationPoints.emplace_back(Point(68.47675000, 10.11180000, 65.68930000));//left ventricle in wall top
  }


}

void ElphyKovachevaBiventricleProblem::InitializeEvaluationPoints() {

    evaluationPoints.emplace_back(Point(86.75, -44.2302, 1.74331));// Apex excited left ventricle
    evaluationPoints.emplace_back(Point(22.5562, -16.1836, 51.2538));// midwall top excitation time =0
    evaluationPoints.emplace_back(Point(55.1275, -62.8303, -2.31392));//right ventricle bottom outer face excitation time =0
  //on level1:
  evaluationPoints.emplace_back(Point(31.66030000, -22.52370000, 43.47850000));//inner midwall top
  evaluationPoints.emplace_back(Point(40.97000000, -27.82665000, 32.66370000));//inner midwall lower
  evaluationPoints.emplace_back(Point(49.55725000, -35.89105000, 20.56925000));//inner midwall even lower
  evaluationPoints.emplace_back(Point(57.53240000, -41.66090000, 10.48984000));//inner midwall next apex
  evaluationPoints.emplace_back(Point(90.67820000, -6.75337500, 35.58070000));//left ventricle in wall middle
  evaluationPoints.emplace_back(Point(68.47675000, 10.11180000, 65.68930000));//left ventricle in wall top
}
std::string ElphyKovachevaBiventricleProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, {Point(86.75, -44.2302, 1.74331)});

  evaluation += "V(86.75, -44.2302, 1.74331) = " + std::to_string(values[0]) + "\n";
  //evaluation += "V(26.297, 10.486, 7.529) = " + std::to_string(values[1]) + "\n";

  return evaluation;
}

std::vector<double> ElphyKovachevaBiventricleProblem::EvaluationResults(const Vector &solution) const {
  auto values = getEvaluationAt(solution, {Point(86.75, -44.2302, 1.74331)});

  for (int i = 0; i < values.size(); ++i) {
    values[i] -= potentialValues[i];
  }
  return values;
}
void ElphyKovachevaBiventricleSimpleExcitationProblem::InitializeEvaluationPoints() {
 evaluationPoints.emplace_back(Point(80.2251,-39.2528,-8.70475));// Apex excited left ventricle
  evaluationPoints.emplace_back(Point(22.5562, -16.1836, 51.2538));// midwall top
  evaluationPoints.emplace_back(Point(55.1275, -62.8303, -2.31392));//right ventricle bottom outer face 
  //on level1:
  evaluationPoints.emplace_back(Point(31.66030000, -22.52370000, 43.47850000));//inner midwall top
  evaluationPoints.emplace_back(Point(40.97000000, -27.82665000, 32.66370000));//inner midwall lower
  evaluationPoints.emplace_back(Point(49.55725000, -35.89105000, 20.56925000));//inner midwall even lower
  evaluationPoints.emplace_back(Point(57.53240000, -41.66090000, 10.48984000));//inner midwall next apex
  evaluationPoints.emplace_back(Point(90.67820000, -6.75337500, 35.58070000));//left ventricle in wall middle
  evaluationPoints.emplace_back(Point(68.47675000, 10.11180000, 65.68930000));//left ventricle in wall top





}