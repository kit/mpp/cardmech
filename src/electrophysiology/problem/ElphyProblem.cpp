#include "ElphyProblem.hpp"

#include "ElphyNoDiffusionProblem.hpp"
#include "ElphyHexaCubeProblem.hpp"
#include "ElphyCubeWithActivationProblem.hpp"
#include "ElphyHexaCube2dProblem.hpp"
#include "ElphyBenchmarkProblem.hpp"
#include "ElphyTetraCubeProblem.hpp"
#include "OneCellProblem.hpp"
#include "EigenvalueProblem.hpp"
#include "VariableMeshProblem.hpp"
#include "ElphyFullHeartProblem.hpp"
#include "ElphyEllipsoidProblem.hpp"
#include "DiffusionProblem.hpp"
#include "ElphyChamberCube2dProblem.hpp"
#include "ElphyBiVentricleProblem.hpp"


std::unique_ptr<ElphyProblem>
GetElphyProblem() {
  std::string problemName{""};
  Config::Get("ElphyProblem", problemName);
  return GetElphyProblem(problemName);
}

std::unique_ptr<ElphyProblem> GetElphyProblem(const std::string &problemName) {
  if (problemName == "VariableMesh")
    return std::make_unique<VariableMeshProblem>();
  if (problemName == "DiffusionProblem1")
    return std::make_unique<DiffusionExampleOne>();
  if (problemName == "DiffusionProblem2")
    return std::make_unique<DiffusionExampleTwo>();
  if (problemName == "ElphyBenchmarkNiederer")
    return std::make_unique<ElphyBenchmarkProblem>();
  if (problemName == "Monodomain")
    return std::make_unique<MonoDomainProblem>("NiedererCube");
  if (problemName == "WithoutDiffusion")
    return std::make_unique<ElphyNoDiffusionProblem>();
  if (problemName == "HexaTestProblem")
    return std::make_unique<ElphyHexaCubeProblem>();
  if (problemName == "HexaTestProblemWA")
    return std::make_unique<ElphyCubeWithActivationTimeProblem>();
  if (problemName == "Hexa2dTestProblem")
    return std::make_unique<ElphyHexaCube2dProblem>();
  if (problemName == "HexaFineTestProblem")
    return std::make_unique<ElphyHexaCubeProblem>("HexaTestCube003125", "HexaFineTestProblem");
  if (problemName == "HexaLinExTestProblem")
    return std::make_unique<ElphyHexaCubeProblem>("NiedererBlock3DTet", "HexaLinExTestProblem");
  if (problemName == "TetraTestProblem")
    return std::make_unique<ElphyTetraCubeProblem>();
  if (problemName == "OneCellProblem")
    return std::make_unique<OneCellProblem>();
  if (problemName == "EigenvalueProblem")
    return std::make_unique<EigenvalueProblem>();
  if (problemName == "EllipsoidProblem")
    return std::make_unique<ElphyEllipsoidProblem>();
  if (problemName == "EllipsoidWAProblem")
    return std::make_unique<ElphyEllipsoidWAProblem>();
  if (problemName == "BiVentricleProblem")
    return std::make_unique<ElphyBiVentricleProblem>();
  if (problemName == "BiVentricleWAProblem")
    return std::make_unique<ElphyBiVentricleWAProblem>();
  if (problemName == "KovachevaBiventricleProblem")
    return std::make_unique<ElphyKovachevaBiventricleProblem>();
  if (problemName == "KovachevaBiventricleWAProblem")
    return std::make_unique<ElphyKovachevaBiventricleWAProblem>();
  if (problemName == "KovachevaBiventricleSimpleExcitationProblem")
    return std::make_unique<ElphyKovachevaBiventricleSimpleExcitationProblem>();
  if (problemName == "KovachevaBiventricleSimpleExcitationWAProblem")
    return std::make_unique<ElphyKovachevaBiventricleSimpleExcitationWAProblem>();
  if (problemName == "ChamberCube2d")
    return std::make_unique<ElphyChamberCube2dProblem>();
  if (problemName == "FullHeartProblem")
    return std::make_unique<ElphyFullHeartProblem>();

  if (problemName != "Default") Warning("Problem name unknown")
  return std::make_unique<ElphyProblem>();
}

std::vector<double>
IElphyProblem::getEvaluationAt(const Vector &solution, const std::vector<Point> points) const {
  std::vector<double> values(points.size());
  for (row r = solution.rows(); r != solution.rows_end(); ++r) {
    for (int i = 0; i < points.size(); ++i) {
      if (r() == points[i]) {
        auto p = solution.find_procset(r());
        if (p != solution.procsets_end()) {
          values[i] = solution(r, 0) / p.size();
        } else {
          values[i] = solution(r, 0);
        }
      }
    }
  }

  for (int i = 0; i < values.size(); ++i) {
    values[i] = PPM->Sum(values[i]);
  }

  return values;
}

void IElphyProblem::SetEvaluationCells(const Vector &V) {
  int numberBM = getNumberEvaluationPointsBesideMesh();
  int numberOnM = getSizeEvaluationPoints() - numberBM;
  for (int i = 0; i < numberBM; i++) {
    Point P = getEvaluationPointAt(numberOnM + i);
    for (cell c = V.cells(); c != V.cells_end(); ++c) {
      if (Includes(c, P)) {
        EvaluationPointData data{c, P, numberOnM + i};
        cellsBesideMeshPoints.push_back(data);
      }
    }
  }
}

bool IElphyProblem::Includes(const cell &c, const Point P) {
  if (c.Type() == HEXAHEDRON) {
    double mx = 100, Mx = -100;
    double my = 100, My = -100;
    double mz = 100, Mz = -100;
    for (int i = 0; i < 8; i++) {
      mx = std::min(mx, c.Corner(i)[0]);
      Mx = std::max(Mx, c.Corner(i)[0]);
      my = std::min(my, c.Corner(i)[1]);
      My = std::max(My, c.Corner(i)[1]);
      mz = std::min(mz, c.Corner(i)[2]);
      Mz = std::max(Mz, c.Corner(i)[2]);
    }
    return mx < P[0] && P[0] < Mx && my < P[1] && P[1] < My && mz < P[2] && P[2] < Mz;
  } else if (c.Type() == TETRAHEDRON) {
    Point LocalP = c.GlobalToLocal(P); //-c.Corner(0)
    return LocalP[0] >= 0.0 && LocalP[1] >= 0.0 && LocalP[2] >= 0.0 &&
           LocalP[0] + LocalP[1] + LocalP[2] < 1.0;
  }
  THROW("Includes only implemented for hexahedral and tetrahedral cells.")

}

void MonoDomainProblem::InitializeEvaluationPoints() {
  evaluationPoints.emplace_back(Point(0.0, 0.0, 0.0));
}

