#ifndef ELPHYHEXACUBE2DPROBLEM_HPP
#define ELPHYHEXACUBE2DPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class ElphyHexaCube2dProblem : public ElphyProblem {
public:
  explicit ElphyHexaCube2dProblem() : ElphyProblem(false){
    if (!meshFromConfig()) {
      meshesName = "TestCube0252DQuad";
    }
  }

  std::string Name() const override{
    return "Hexa2dTestProblem";
  }
  explicit ElphyHexaCube2dProblem(std::string mName) : ElphyProblem(false){
    if (!meshFromConfig()) {
      meshesName = mName;
    }}

  void InitializeEvaluationPoints() override;

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;


};


#endif //ELPHYNODIFFUSIONPROBLEM_HPP
