#include "ElphyNoDiffusionProblem.hpp"

constexpr std::array<double, 2> noDiffusionValues{-84.94104211, -85.29800166};


std::string ElphyNoDiffusionProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, evaluationPoints);

  evaluation += "V(0.0,0.0,0.0) = " + std::to_string(values[0]) + "\n";
  evaluation += "V(5.0,3.0,3.0) = " + std::to_string(values[1]) + "\n";

  return evaluation;
}

std::vector<double> ElphyNoDiffusionProblem::EvaluationResults(const Vector &solution) const {
  auto values = getEvaluationAt(solution, evaluationPoints);

  for (int i = 0; i < values.size(); ++i) {
    values[i] -= noDiffusionValues[i];
  }
  return values;
}

void ElphyNoDiffusionProblem::InitializeEvaluationPoints() {
  evaluationPoints.emplace_back(Point(0.0, 0.0, 0.0));
  evaluationPoints.emplace_back(Point(5.0, 3.0, 3.0));
}