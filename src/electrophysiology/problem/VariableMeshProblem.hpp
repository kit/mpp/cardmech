#ifndef VARIABLEMESHPROBLEM_HPP
#define VARIABLEMESHPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class VariableMeshProblem : public ElphyProblem {
public:
  VariableMeshProblem() : ElphyProblem(false) {
    diffusionValues[1] = diffusionValues[0];


    if (!meshFromConfig()) {
      meshesName = "HexaTestCube025";
    }
  }

  std::string Name() const override{
    return "VariableMeshProblem";
  }

  void InitializeEvaluationPoints() override;

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;
};


#endif //ELPHYNODIFFUSIONPROBLEM_HPP
