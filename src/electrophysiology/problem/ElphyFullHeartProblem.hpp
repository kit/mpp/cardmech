#ifndef ELPHYFULLHEARTPROBLEM_HPP
#define ELPHYFULLHEARTPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class ElphyFullHeartProblem : public ElphyProblem {
public:
  explicit ElphyFullHeartProblem() : ElphyProblem(false) {
    if (!meshFromConfig()) {
      meshesName = "HeartT4";
    }
  }

  std::string Name() const override{
    return "FullHeart";
  }

  void InitializeEvaluationPoints() override;

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;


};


#endif //ELPHYNODIFFUSIONPROBLEM_HPP
