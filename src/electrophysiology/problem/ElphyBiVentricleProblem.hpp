#ifndef ELPHYBIVENTRICLEPROBLEM_HPP
#define ELPHYBIVENTRICLEPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class ElphyBiVentricleProblem : public ElphyProblem {
public:
  explicit ElphyBiVentricleProblem() : ElphyBiVentricleProblem(false) {}

  explicit ElphyBiVentricleProblem(bool plotActivation) : ElphyProblem(plotActivation) {
    std::string spaceSmoothing;
    Config::Get("ExternalCurrentSmoothingInSpace", spaceSmoothing);
    SetNumberOfEvaluationPointsBesideMesh(1);
    if (spaceSmoothing == "discrete") {
      meshesName = "BiVentricle_unsmoothed";
    } else {
      meshesName = "BiVentricle_smoothed";
    }
    std::string cellModel;
    Config::Get("ElphyModelName", cellModel);
    //if(cellModel=="BeelerReuter"){
      diffusionValues[0] = 0.001 * VectorField(0.2073, 0.0921, 0.023);
    //}
    diffusionValues[1] = diffusionValues[0];
  }

  std::string Name() const override {

    if (UpdateActivationTime)
      return "Biventricle with activation";
    return "Biventricle";
  }

  void InitializeEvaluationPoints() override;

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;


};

class ElphyBiVentricleWAProblem : public ElphyBiVentricleProblem {
public:
  explicit ElphyBiVentricleWAProblem() : ElphyBiVentricleProblem(true) {
  }
};




class ElphyKovachevaBiventricleProblem : public ElphyProblem {
public:
  explicit ElphyKovachevaBiventricleProblem() : ElphyKovachevaBiventricleProblem(false) {}

  explicit ElphyKovachevaBiventricleProblem(bool plotActivation) : ElphyProblem(plotActivation) {
    std::string spaceSmoothing;
    Config::Get("ExternalCurrentSmoothingInSpace", spaceSmoothing);
    diffusionValues[0] = 0.001 * VectorField(0.2073, 0.0921, 0.023);
    if (spaceSmoothing == "discrete") {
      Exit("This option is not possible for the chosen mesh.");
    } else {
      meshesName = "KovachevaBiventricle";
    }
  }

  std::string Name() const override {

      if (UpdateActivationTime)
        return "KovachevaBiventricle with activation";
      else
        return "KovachevaBiventricle";
  }


  void InitializeEvaluationPoints() override;

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;


};
class ElphyKovachevaBiventricleWAProblem : public ElphyKovachevaBiventricleProblem {
public:
  explicit ElphyKovachevaBiventricleWAProblem() : ElphyKovachevaBiventricleProblem(true) {
  }
};
class ElphyKovachevaBiventricleSimpleExcitationProblem : public ElphyProblem {
public:
  explicit ElphyKovachevaBiventricleSimpleExcitationProblem() : ElphyKovachevaBiventricleSimpleExcitationProblem(false) {}

  explicit ElphyKovachevaBiventricleSimpleExcitationProblem(bool plotActivation) : ElphyProblem(plotActivation) {
    std::string spaceSmoothing;
    Config::Get("ExternalCurrentSmoothingInSpace", spaceSmoothing);
    diffusionValues[0] = 0.001 * VectorField(0.28, 0.182, 0.098);
    if (spaceSmoothing == "discrete") {
      meshesName="KovacheveBiventricleSimpleExcitation";
    } else {
      meshesName = "KovacheveBiventricleSimpleExcitation_smooth";
    }
  }
  void InitializeEvaluationPoints() override;
  std::string Name() const override {

    if (UpdateActivationTime)
      return "KovachevaBiventricle simple excitation with activation";
    else
      return "KovachevaBiventricle simple excitation";
  }



};
class ElphyKovachevaBiventricleSimpleExcitationWAProblem : public ElphyKovachevaBiventricleSimpleExcitationProblem {
public:
  explicit ElphyKovachevaBiventricleSimpleExcitationWAProblem() : ElphyKovachevaBiventricleSimpleExcitationProblem(true) {
  }
};

#endif //ELPHYVENTRICLEPROBLEM_HPP;