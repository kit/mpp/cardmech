#include "ElphyTetraCubeProblem.hpp"


constexpr std::array<double, 2> potentialValues{23.2810756, -84.572363};


std::string ElphyTetraCubeProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, {Point(0.0, 0.0, 0.0), Point(5.0, 3.0, 3.0)});
  evaluation += "V(0.0,0.0,0.0) = " + std::to_string(values[0]) + "\n";
  evaluation += "V(5.0,3.0,3.0) = " + std::to_string(values[1]) + "\n";

  return evaluation;
}

std::vector<double> ElphyTetraCubeProblem::EvaluationResults(const Vector &solution) const {
  auto values = getEvaluationAt(solution, {Point(0.0, 0.0, 0.0), Point(5.0, 3.0, 3.0)});

  for (int i = 0; i < values.size(); ++i) {
    values[i] -= potentialValues[i];
  }
  return values;
}

void ElphyTetraCubeProblem::InitializeEvaluationPoints() {
  evaluationPoints.emplace_back(Point(0.0, 0.0, 0.0));
  evaluationPoints.emplace_back((Point(1.0, 0.5, 0.5)));
  evaluationPoints.emplace_back((Point(1.75, 1.0, 1.0)));
  evaluationPoints.emplace_back(Point(2.5, 1.5, 1.5));
  evaluationPoints.emplace_back(Point(3.25, 2.0, 2.0));
  evaluationPoints.emplace_back(Point(4.25, 2.5, 2.5));
  evaluationPoints.emplace_back(Point(5.0, 3.0, 3.0));
}

