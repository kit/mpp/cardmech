//
// Created by laurapfeiffer on 09.02.21.
//

#ifndef ELPHYBENCHMARKPROBLEM_HPP
#define ELPHYBENCHMARKPROBLEM_HPP

#include "ElphyProblem.hpp"

class ElphyBenchmarkProblem : public ElphyProblem {
public:
  explicit ElphyBenchmarkProblem() : ElphyProblem(true) {
    if (!meshFromConfig()) {
      meshesName = "NiedererBM05_3DTet";
    }
  }

  std::string Name() const override{
    return "Electrophysiology Benchmark";
  }
  void InitializeEvaluationPoints() override;
};


#endif //ELPHYBENCHMARKPROBLEM_HPP
