#include "ElphyFullHeartProblem.hpp"


constexpr std::array<double, 2> potentialValues{-84.584430, -85.298003};


std::string ElphyFullHeartProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};

  auto values = getEvaluationAt(solution, {Point(-33.47779846191406, -27.064899444580078, 44.79759979248047),
                                           Point(26.29669952392578, 10.485799789428711, 7.529429912567139)});

  evaluation += "V(-33.4778, -27.0649, 44.7976) = " + std::to_string(values[0]) + "\n";
  evaluation += "V(26.297, 10.486, 7.529) = " + std::to_string(values[1]) + "\n";

  return evaluation;
}

std::vector<double> ElphyFullHeartProblem::EvaluationResults(const Vector &solution) const {
  auto values = getEvaluationAt(solution, {Point(-33.47779846191406, -27.064899444580078, 44.79759979248047),
                                           Point(82.53800201416016, -88.54129791259766, -55.96120071411133)});

  for (int i = 0; i < values.size(); ++i) {
    values[i] -= potentialValues[i];
  }
  return values;
}

void ElphyFullHeartProblem::InitializeEvaluationPoints() {
  //atria
  evaluationPoints.emplace_back(Point(-33.47779846191406, -27.064899444580078, 44.79759979248047));//Sinus Node
  evaluationPoints.emplace_back(Point(-56.90629959106445, -45.7943000793457, -19.526899337768555));//right atria inner side bottom
  evaluationPoints.emplace_back(Point(-62.139198303222656, -28.6466007232666, -1.0233700275421143));// right Atria outer side middle
  evaluationPoints.emplace_back(Point(-9.397879600524902, 13.068099975585938, 34.92649841308594));// left Atria inner side middle
  evaluationPoints.emplace_back(Point(26.29669952392578, 10.485799789428711, 7.529429912567139));//left atria outer side bottom

  //ventricle
  evaluationPoints.emplace_back(Point(82.53800201416016, -88.54129791259766, -55.96120071411133));// Apex
  //evaluationPoints.emplace_back(Point(82.538, -88.5413, -55.9612));
  evaluationPoints.emplace_back(Point(19.492599487304688, -74.51229858398438, -31.519500732421875));// Innenwand Ventricle angeregt
  //evaluationPoints.emplace_back(Point(19.4926, -74.5123, -31.5195));


}

