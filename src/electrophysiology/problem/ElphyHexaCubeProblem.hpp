#ifndef ELPHYHEXACUBEPROBLEM_HPP
#define ELPHYHEXACUBEPROBLEM_HPP


#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class ElphyHexaCubeProblem : public ElphyProblem {
  std::string problemName{"HexaTestProblem"};
public:
  explicit ElphyHexaCubeProblem() : ElphyProblem(false) {
    SetNumberOfEvaluationPointsBesideMesh(1);
    if (!meshFromConfig()) {
      meshesName = "TestCube0253DQuad";
    }
  }

  explicit ElphyHexaCubeProblem(std::string mName) : ElphyProblem(false) {
    if (!meshFromConfig()) {
      meshesName = mName;
    }
  }

  explicit ElphyHexaCubeProblem(std::string mName, std::string pName) : ElphyProblem(false), problemName(pName) {
    if (!meshFromConfig()) {
      meshesName = mName;
    }
  }

  std::string Name() const override{
    return problemName;
  }



  void InitializeEvaluationPoints() override;

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;


};


#endif //ELPHYNODIFFUSIONPROBLEM_HPP
