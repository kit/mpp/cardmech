//
// Created by laurapfeiffer on 09.02.21.
//

#ifndef ELPHYCUBEWITHACTIVATIONPROBLEM_HPP
#define ELPHYCUBEWITHACTIVATIONPROBLEM_HPP

#include "Algebra.hpp"
#include "ElphyProblem.hpp"

class ElphyCubeWithActivationTimeProblem : public ElphyProblem {
public:
  explicit ElphyCubeWithActivationTimeProblem() : ElphyProblem(true) {

    if (!meshFromConfig()) {
      meshesName = "TestCube0252DQuad";
    }
  }

  std::string Name() const override{
    return "HexaTestProblemWA";
  }

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;

  void InitializeEvaluationPoints() override;
};

#include "Algebra.hpp"
#include "ElphyProblem.hpp"

#endif //ELPHYCUBEWITHACTIVATIONPROBLEM_HPP
