#ifndef MAINMONODOMAIN_H
#define MAINMONODOMAIN_H

#include "MainCardMech.hpp"
#include "solvers/LinearImplicitSolver.hpp"


class MainMonodomain : public MainCardMech {
  std::unique_ptr<ElphyProblem> problem = nullptr;
  std::unique_ptr<MElphyModel> cellModel = nullptr;
  std::unique_ptr<IElphyAssemble> elphyA = nullptr;

  std::unique_ptr<Vector> potential = nullptr;

  int plevel = 0, elevel = 0, discDegree = 1;

  std::string elphyModelClass{"MElphyModel"};
public:
  MainMonodomain();

  void ResetLevel(int newLevel);

  void Initialize() override;

  Vector &Run() override;

  std::vector<double> Evaluate(const Vector &) override;
  double EvaluateQuantity(const Vector &solution, const std::string &quantity) const;
};


#endif //MAINMONODOMAIN_H
