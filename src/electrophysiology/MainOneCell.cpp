#include <discretization/MultiPartDiscretization.hpp>
#include <solvers/SplittingSolver.hpp>
#include "MainOneCell.hpp"
#include"MeshesCreator.hpp"
#include "cellmodels/solvers/MElphySolver.hpp"


void MainOneCell::Initialize() {

  problem = GetElphyProblem();
  problem->InitializeEvaluationPoints();
  mesh = std::unique_ptr<Meshes>(MeshesCreator("Interval").Create());
  int discDegree = 1;
  Config::Get("ElphyPolynomialDegree", discDegree);
}

Vector &MainOneCell::Run() {
  std::string elphyModelClass = "IBTElphyModel";
  Config::Get("ElphyModelClass", elphyModelClass);

  auto times = ReadTimeFromConfig();
  if (elphyModelClass == "MElphyModel") {
    auto cellModel = GetElphyModel();
    elphyA = CreateElphyAssemble(*problem, *cellModel, 0);
    elphyA->ResetTime(times[0], times[1], times[2]);
    potential = std::make_unique<Vector>(cellModel->InitialValue(ELPHY), elphyA->GetSharedDisc());
    if (problem->Name() == "EigenvalueProblem") {
      MElphySolver solver(*cellModel, elphyA->ExcitationData(), "Eigenvalue");
      solver.SetVerbose(15);
      solver.Solve(*tSeries, *potential);
    } else {
      MElphySolver solver(*cellModel, elphyA->ExcitationData(), "ExponentialIntegrator");
      solver.SetVerbose(20);
      solver.Solve(*tSeries, *potential);
    }

  } else {
    elphyA = std::make_unique<MonodomainSplitting>(*problem, 0);
    IBTSplittingSolver eSolver(ONLYODE);
    eSolver.Method(*elphyA, *potential);

  }

  return *potential;


}

std::vector<double> MainOneCell::Evaluate(const Vector &solution) {
  mout << "\n" << "==== Monodomain Information ============== \n\n";

  mesh->PrintInfo();

  mout.PrintInfo("Assemble", 1,
                 PrintInfoEntry("Name", std::string("Monodomain").c_str()),
                 PrintInfoEntry("Problem", problem->Name()),
                 PrintInfoEntry("Cells", PPM->Sum(mesh->fine().CellCount())),
                 PrintInfoEntry("Degrees of Freedom:", solution.pSize()),
                 PrintInfoEntry("Matrix Entries", solution.pMatrixSize()));


  std::string problemEval = problem->Evaluate(solution);
  mout << problemEval << endl;
  mout << "===========================================" << endl;

  return problem->EvaluationResults(solution);
}





