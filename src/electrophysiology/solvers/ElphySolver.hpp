#ifndef ELPHYSOLVER_HPP
#define ELPHYSOLVER_HPP


#include <CardiacSolver.hpp>
#include <LinearSolver.hpp>
#include <cellmodels/solvers/MCellSolver.hpp>
#include "assemble/IElphyAssemble.hpp"


class ElphySolver : public CardiacSolver {
protected:
  bool iextInPDE{false};
public:
  explicit ElphySolver();

  virtual void Initialize(IElphyAssemble &A, Vector &potential) {};

  //virtual void Initialize(IElphyAssemble &A, Vector &potential, Vector &gamma_f_c) {
    //Initialize(A,potential);
  //}
  
  /**
   * Solves purely electrophysiolical problem over the course of the timeSeries
   * @param potential
   * @param timeSeries
   * The solution is stored in potential
   */
  virtual void Method(IElphyAssemble &A, Vector &potential);

  /**
   * Solves a single electrophysiolgical time step with the given parameters
   * @param values  - Must contain (in this order) potential as first Vector
   *                - possibly Stretch and 4th Invariant as second and third entry.
   * @param t
   * @param dt
   */
  virtual void Step(IElphyAssemble &A, Vectors &values) = 0;
  virtual  void printSolverNameInStep(IElphyAssemble &A) {vout(1) << "Solving Step " << A.Step() + 1 << " at time " << A.Time() << "." << endl;};
  virtual  void printSolverNameInStep(IElphyAssemble &A, int step) {vout(1) << "Solving Step " << step << " at time " << A.Time() << "." << endl;};
  virtual void Step(IElphyAssemble &A, Vectors &values, Vector &iota_c, Vector& gamma_f_c) {
    Exit("This step is only defined for semi-implicit on cells")
    //Step(A,values);
    };

  virtual void Finalize(Vectors &values) {};
  virtual void GatingVectorAti( Vector &values, int i) const {Exit("Gating vector not defined here");};
  virtual void printVariableOnCells(IElphyAssemble &A,const Vector &var,const std::string &varname)const ;
};
std::unique_ptr<ElphySolver>
GetElphySolver(const std::string &solverName, MCellModel &cellModel, const Vector &excitationData);
std::unique_ptr<ElphySolver> GetElphySolver(MCellModel &cellModel, const Vector &excitationData);


#endif //ELPHYSOLVER_HPP