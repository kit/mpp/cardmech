#include "ElphySolver.hpp"
#include "SplittingSolver.hpp"
#include "LinearImplicitSolver.hpp"
#include <MultiPartScalarWGElement.hpp>

ElphySolver::ElphySolver() {
  Config::Get("ElphySolverVerbose", verbose);
  Config::Get("ElphySolverVTK", vtk);
  Config::Get("IextInPDE", iextInPDE);
}

void ElphySolver::Method(IElphyAssemble &A, Vector &potential) {
  //mout.StartBlock("Elphy");
  Initialize(A, potential);
  Vectors uNew(1, potential);

  while (!A.IsFinished()) {
    A.PlotIteration(uNew[0]);


    printSolverNameInStep(A);
    vout(10).StartBlock("Step Time");
    Step(A, uNew);
    vout(10).EndBlock();
    //A.NextTimeStep();


  }
  Finalize(uNew);

  A.PlotIteration(uNew[0]);
 // A.PrintIteration(uNew[0]);

  //mout.EndBlock();
  potential = uNew[0];
  A.PlotActivation();

}

std::unique_ptr<ElphySolver> GetElphySolver(const std::string &solverName,
                                            MCellModel &cellModel,
                                            const Vector &excitationData) {
  std::string cmClass = "MElphyModel";
  Config::Get("ElphyModelClass", cmClass);
  if (contains(cmClass, "IBT")) {
    return std::make_unique<IBTSplittingSolver>();
  } else {
    if (solverName == "Splitting") {
      auto cellSolver = std::make_unique<MElphySolver>(cellModel, excitationData);
      return std::make_unique<MSplittingSolver>(std::move(cellSolver));
    } else if (solverName == "Diffusion") {
      return std::make_unique<DiffusionSolver>();
    } else if (solverName == "LinearImplicit" || solverName == "LinearImplicitNodes") {
      return std::make_unique<LinearImplicitSolver>(cellModel);
    }/* else if(modelName =="LinearImplicitQuadrature") {
      LiniearImplicitSolverQuad eSolver(*elphyA, *cellModel);
      eSolver.Solve(*potential, *tSeries);
    }*/else if (solverName == "ImplictEuler") {
      return std::make_unique<ImplicitSolver>(cellModel);
    } else if (solverName == "SemiImplicit" || solverName == "SemiImplicitNodes") {
      return std::make_unique<SemiImplicitSolver>(cellModel);
    }else if (solverName=="SemiImplicitOnCells"|| solverName =="SemiImplicitOnCellsSVI"|| solverName=="SemiImplicitOnCellsIextPerCell")
      return std::make_unique<SemiImplicitSolverOnCells>(cellModel);
  }
  THROW(solverName + " No Solver for this Model implemented.")
}

std::unique_ptr<ElphySolver>
GetElphySolver(MCellModel &cellModel, const Vector &excitationData) {
  std::string solverName{"SemiImplicit"};
  Config::Get("ElphyModel", solverName);
  return GetElphySolver(solverName, cellModel, excitationData);
}

void ElphySolver::printVariableOnCells(IElphyAssemble &A,const Vector &var,const std::string &varname) const{
  const Meshes &mesh = var.GetMeshes();
  auto disc0_var = std::make_shared<LagrangeDiscretization>(mesh, 0, 1);
  Vector variable(disc0_var);
  variable=0;
  for(cell c= var.cells(); c != var.cells_end(); ++c){
    MultiPartScalarElementWG E(var, *c, DomainPart(*c));
    variable(c(),0)=E.Value(c.LocalCenter(),var,0);
  }

  A.PrintVariableOnCells(variable,varname);
}