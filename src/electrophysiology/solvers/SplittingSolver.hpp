#ifndef SPLITTINGSOLVER_HPP
#define SPLITTINGSOLVER_HPP

#include <cellmodels/ibt/ICellParameters.hpp>
#include <cellmodels/ibt/ICellModel.hpp>
#include "MCellModel.hpp"
#include "cellmodels/solvers/MElphySolver.hpp"
#include "assemble/IElphyAssemble.hpp"
#include "assemble/MonodomainSplitting.hpp"
#include "assemble/Monodomain.hpp"
#include "LinearSolver.hpp"
#include "CardiacSolver.hpp"
#include "DiffusionSolver.hpp"

enum SPLITTING {
  ONLYODE = 0,
  GODUNOV = 1,
  STRANG = 2,
  STEIN = 3
};

static SPLITTING getSplitting(const std::string &prefix = "Elphy") {
  std::string s = "Godunov";
  Config::Get(prefix + "SplittingMethod", s);
  if (s == "OnlyODE") return ONLYODE;
  if (s == "Godunov") return GODUNOV;
  if (s == "Strang") return STRANG;
  if (s == "Stein") return STEIN;
  return GODUNOV;
}

class ElphySplittingSolver : public ElphySolver {

protected:
  DiffusionSolver DS;
  bool iextInPDE{false};


  virtual void solveCellModels(Vectors &values, double t, double dt) = 0;

  virtual void solveDiffusion(IElphyAssemble &A, Vector &potential) {
    A.updateExternalCurrent(potential);
    DS.SolveStep(A, potential);
  };

  std::function<void(IElphyAssemble &, Vectors &)> solveSplittingStep;

  void selectScheme(SPLITTING splitting) {
    switch (splitting) {
      case ONLYODE:
        solveSplittingStep = [this](IElphyAssemble &A, Vectors &V) {
          solveCellModels(V, A.Time(), A.StepSize());
        };
        break;
      case GODUNOV:
        solveSplittingStep = [this](IElphyAssemble &A, Vectors &V) {
          vout(15).StartBlock("Solve Splittingstep");
          double t = A.Time();
          double dt = A.StepSize();

          solveCellModels(V, t, dt);
          //A.PrintV(V[0]);
          vout(15).StartBlock("Solve PDE");
          solveDiffusion(A, V[0]);
          vout(15).EndBlock();
          vout(15).EndBlock();
          A.updateActivationTime(V[0], t + dt);
        };
        break;
      case STRANG:
        solveSplittingStep = [this](IElphyAssemble &A, Vectors &V) {
          double t = A.Time();
          double dt = A.StepSize();
          //double tNew = t + dt;
          solveCellModels(V, t, dt / 2.0);
          solveDiffusion(A, V[0]);
          solveCellModels(V, t + dt / 2.0, dt / 2.0);
          A.updateActivationTime(V[0], t + dt);
        };
        break;
      case STEIN:
        solveSplittingStep = [this](IElphyAssemble &A, Vectors &V) {
          double t = A.Time();
          double dt = A.StepSize();

          A.setTheta(0.0);
          solveDiffusion(A, V[0]);
          solveCellModels(V, t, dt);
          A.setTheta(1.0);
          solveDiffusion(A, V[0]);
          A.updateActivationTime(V[0], t + dt);
        };
        break;
      default:
        THROW("Unknown splitting Scheme")
    }
  }

  int subSteps{1};

public:
  explicit ElphySplittingSolver() : ElphySolver(), DS() {
    Config::Get("IextInPDE", iextInPDE);

    std::string schemeName{"Undefined"};
    Config::Get("ElphyScheme", schemeName);
    selectScheme(getSplitting());
    //if(iextInPDE && DS.AssemblesOnce()){
    //THROW("RHS must be assembled in every step if Iext is  in PDE")
    //}
    if (getSplitting() == STEIN) {
      DS.setAssembleOptions(false, true);
    }
  }

  ElphySplittingSolver(SPLITTING splitting) :
      ElphySolver(), DS() {
    Config::Get("IextInPDE", iextInPDE);
    selectScheme(splitting);
  }
  virtual void Step(IElphyAssemble &A, Vectors &values) override {
    solveSplittingStep(A, values);
    A.NextTimeStep();
    A.PrintIteration(values[0]);
  }

  void Initialize(IElphyAssemble &A, Vector &potential) override;
};

class IBTSplittingSolver : public ElphySplittingSolver {
  void solveCellModels(Vectors &values, double t, double dt) override;

protected:
  EMP_IBT ventricleParameters{0};
  EMP_IBT atriaParameters{1};
  std::unordered_map<Point, std::unique_ptr<IElphyModel>> cellModel{};

  int odeSteps{1};
public:
  explicit IBTSplittingSolver() :
      ElphySplittingSolver() {
    Config::Get("ElphySubsteps", subSteps);
    if (getSplitting() == STRANG) odeSteps = 2;
  }

  IBTSplittingSolver(SPLITTING scheme) :
      ElphySplittingSolver(scheme) {
    Config::Get("ElphySubsteps", subSteps);
    if (scheme == STRANG) odeSteps = 2;
  }

  void Initialize(IElphyAssemble &A, Vector &potential) override;
};


class MSplittingSolver : public ElphySplittingSolver {
  std::unique_ptr<MElphySolver> cS;

  void solveCellModels(Vectors &values, double t, double dt) override;

protected:
  TimeSeries microTS{};
  int subSteps{1};

public:
  explicit MSplittingSolver(std::unique_ptr<MElphySolver> &&cellS) :
      ElphySplittingSolver(), cS(std::move(cellS)), microTS(0.0, 1.0, 1) {
  }

  MSplittingSolver(std::unique_ptr<MElphySolver> &&cellS, SPLITTING scheme)
      : ElphySplittingSolver(scheme), cS(std::move(cellS)), microTS(0.0, 1.0, 1) {
    Config::Get("ElphySubsteps", subSteps);
  }

  void Initialize(IElphyAssemble &A, Vector &potential) override;
  void Step(IElphyAssemble &A, Vectors &values) override {
    ElphySplittingSolver::Step(A,values);
    printVariableOnCells(A,cS->GetCa(),"Ca");
    if (cS->GetModel().Type() == TENSION) {
      printVariableOnCells(A, values[1], "gamma");
    }

  }
  void Finalize(Vectors &values) override;

};

#endif //SPLITTINGSOLVER_HPP
