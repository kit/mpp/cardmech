#include <ibt/ElphyIBT.hpp>
#include <MultiPartScalarElement.hpp>
#include "SplittingSolver.hpp"

void ElphySplittingSolver::Initialize(IElphyAssemble &A, Vector &potential) {
  DS.Initialize(A, potential);
}
void IBTSplittingSolver::Initialize(IElphyAssemble &A, Vector &V) {
  ElphySplittingSolver::Initialize(A, V);
  bool withAtria(false);
  Vector DP(0.0, V);
  for (cell c = V.cells(); c != V.cells_end(); ++c) {
    MultiPartScalarElement E(V, *c);
    auto z = V.GetNodalPoints(*c);

    for (int i = 0; i < E.size(); ++i) {
      DP(E[i], E.IndexPart(i)) = DomainPart(*c);
      if (DomainPart(*c) == 1) withAtria = true;
    }
  }
  double scaleamplitude = 1.0;
  Config::Get("ScaleExcitationAmplitude", scaleamplitude);
  double overwriteamplitude = -1.0;
  Config::Get("OverwriteExcitationAmplitude", overwriteamplitude);
  double scaleduration = 1.0;
  Config::Get("ScaleExcitationDuration", scaleduration);
  double overwriteduration = -1.0;
  Config::Get("OverwriteExcitationDuration", overwriteduration);
  //double starttime = 0.0;
  //Config::Get("StartTime", starttime);

  bool isExCurSmooth(false);
  Config::Get("IsExternalCurrentSmooth", isExCurSmooth);

  ventricleParameters.initParameters(A.StepSize() / (odeSteps * subSteps));


  if (withAtria) {
    cout << "With Atria " << withAtria << endl;
    atriaParameters.initParameters(A.StepSize() / (odeSteps * subSteps));
    for (row r = V.rows(); r != V.rows_end(); ++r) {

      for (int j = 0; j < r.NumberOfDofs(); ++j) {
        if (DP(r, j) == 1) {
          cellModel[r() + j * shift2] = std::make_unique<ElphyIBT>(atriaParameters,
                                                                   Excitation(A.ExcitationData(), r, j),
                                                                   Duration(A.ExcitationData(), r, j), Amplitude(A.ExcitationData(), r, j));//, isExCurSmooth, nummethod);
          V(r, j) = cellModel.find(r() + j * shift2)->second->getPotential();
        } else {
          cellModel[r() + j * shift2] = std::make_unique<ElphyIBT>(ventricleParameters,
                                                                   Excitation(A.ExcitationData(), r, j),
                                                                   Duration(A.ExcitationData(), r, j),
                                                                   scaleamplitude * Amplitude(A.ExcitationData(), r, j)
          );//, isExCurSmooth, nummethod);
          V(r, j) = cellModel.find(r() + j * shift2)->second->getPotential();
        }
      }
    }
  } else {
    for (row r = V.rows(); r != V.rows_end(); ++r) {
      for (int j = 0; j < r.NumberOfDofs(); ++j) {
        cellModel[r() + j * shift2] = std::make_unique<ElphyIBT>(ventricleParameters,
                                                                 Excitation(A.ExcitationData(), r, j),
                                                                 Duration(A.ExcitationData(), r, j),
                                                                 scaleamplitude * Amplitude(A.ExcitationData(), r, j));//, isExCurSmooth, nummethod);
        V(r, j) = cellModel.find(r() + j * shift2)->second->getPotential();
      }
    }
  }
  vout(2) << "Finished Initializing" << endl;
}

void IBTSplittingSolver::solveCellModels(Vectors &values, double t, double dt) {
  Vector &V = values[0];
  double e_dt = dt / subSteps;

  for (row r = V.rows(); r != V.rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); j++) {
      for (int k = 0; k < subSteps; ++k) {
        V(r, j) = cellModel.find(r() + j * shift2)->second->update(e_dt, V(r, j));
      }
    }
  }
}


void MSplittingSolver::solveCellModels(Vectors &values, double t, double dt) {
  vout(11).StartBlock("Solve ODE");
  microTS.Reset(t * cS->TimeScale(), (t + dt) * cS->TimeScale(), subSteps);
  cS->Solve(microTS, values);
  vout(11).EndBlock();

}

void MSplittingSolver::Initialize(IElphyAssemble &A, Vector &potential) {
  ElphySplittingSolver::Initialize(A, potential);
  potential = cS->GetModel().InitialValue(ELPHY);
  cS->initialize(potential);
  //cS.SetVerbose(-1);

  if (iextInPDE) {
    cS->SetExternal({-10.0, 0.0, 0.0});
  }
  A.PrintIteration(potential);
  printVariableOnCells(A,cS->GetCa(),"Ca");
}

void MSplittingSolver::Finalize(Vectors &values) {
  cS->PrintMaxMinGating();
}