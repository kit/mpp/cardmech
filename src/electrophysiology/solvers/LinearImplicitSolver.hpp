#ifndef ELPHYSOLVERS_H
#define ELPHYSOLVERS_H

#include "ElphySolver.hpp"
#include <LinearSolver.hpp>
#include <assemble/IElphyAssemble.hpp>
#include <assemble/Monodomain.hpp>
#include <MCellModel.hpp>
#include "Newton.hpp"

void QuadraticIonStep(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                      std::vector<std::vector<double>> &g);

void ExplicitEulerIonStep(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                          std::vector<std::vector<double>> &g);

void RungeKutta2IonStep(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                        std::vector<std::vector<double>> &g);

void RungeKutta4IonStep(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                        std::vector<std::vector<double>> &g);

class LinearImplicitSolver : public ElphySolver {
  int ReferenceCalculation;
  int max_estep;
  bool check_conv;
  int linearSolverVerbose{0};
  bool deformConcentrationStretch = false;

protected:
  std::unique_ptr<Matrix> K;
  std::unique_ptr<Vector> b;
  std::unique_ptr<Matrix> MK;
  std::unique_ptr<Vector> potential;
  std::unique_ptr<Vectors> gating;
  std::unique_ptr<Vectors> vcw_c;
  std::unique_ptr<Vectors> maxgating;
  std::unique_ptr<Vectors> mingating;

  std::unique_ptr<LinearSolver> S;
  //IElphyAssemble &A;
  bool assembleOnce{true};
  bool assembled{false};
  MCellModel &M;
  int subSteps{1};
  bool iextInPDE{false};
  bool plotVTK{false};
  int ionSteps{1};
  bool plotCa{false};
  std::unique_ptr<Vector> cellSizeV;
  std::shared_ptr<LagrangeDiscretization> disc0;
  std::unique_ptr<Vectors> externalCurrentOnCells;

  void selectIonScheme(const std::string &solvingIonScheme);

  std::function<void(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                     std::vector<std::vector<double>> &g)> SolveConcentrationStep;
public:
  explicit LinearImplicitSolver(MCellModel &cellModel) :
      ElphySolver(),
      //A(assemble),
      ReferenceCalculation(0), S(std::unique_ptr<LinearSolver>(GetLinearSolverByPrefix("Elphy"))),
      max_estep(100000), check_conv(true), M(cellModel)//, M(cellModel.ElphyModel())
  {
    M.UseExpInt(true);
    std::string solvingIonScheme{"ExplicitEuler"};
    Config::Get("IonScheme", solvingIonScheme);
    selectIonScheme(solvingIonScheme);

    Config::Get("PlotVTK", plotVTK);
    Config::Get("PlotCalcium", plotCa);
    Config::Get("LinearImplicitVerbose", verbose);
    Config::Get("ReferenceCalculation", ReferenceCalculation);
    Config::Get("EulerSteps", max_estep);
    Config::Get("EulerConvergence", check_conv);
    Config::Get("IextInPDE", iextInPDE);
    Config::Get("ElphyLinearSolverVerbose", linearSolverVerbose);
    Config::Get("ReassembleRHSonce", assembleOnce);
    //if (ReferenceCalculation) NoDate();
    (*S).SetVerbose(linearSolverVerbose);

    std::string order = "wcV";
    Config::Get("OrderStaggeredScheme", order);
    selectOrder(order);
    // include deformation of cell when using quasi-incompressibility in mechanics
    Config::Get("deformConcentrationStretch", deformConcentrationStretch);
  }

  virtual void Initialize(IElphyAssemble &A, Vector &potential);

  //virtual void Initialize(IElphyAssemble &A, Vector &potential, Vector &gamma_f_c);

  virtual void Method(IElphyAssemble &A, Vector &V);

  void SolveConcentration(IElphyAssemble &A, double dt, const Vectors &values);

  void SolveConcentrationOnCells(IElphyAssemble &A, const Vectors &values, Vector &iota_c);

  void SolveGating(double t, double dt);
  void SolveGatingOnCells(double t,double dt);

  virtual void SolvePDE(IElphyAssemble &A);
  virtual void SolvePDEOnCells(IElphyAssemble &A){
    Exit("only defined for semi-implicit solver");
  };

  void Step(IElphyAssemble &A, Vectors &values) override;

  std::function<void(IElphyAssemble &A, const Vectors &values)> StaggeredStep;
  void StaggeredStepOnCells(IElphyAssemble &A, const Vectors &values,Vector &iota_c);

  void selectOrder(std::string o);

  void updateMaxMin();

  void PrintMaxMinGating();
  virtual void printSolverNameInStep(IElphyAssemble &A);
  virtual void printSolverNameInStep(IElphyAssemble &A,int step);

  virtual void updateValues(Vectors &values);

  virtual void updateValues(Vectors &values, Vector &gamma_f_c){Exit("only defined for semi-implicit solver");};

  void GatingVectorAti( Vector &values, int i) const override;
};

/*class LiniearImplicitSolverQuad: public LinearImplicitSolver{
public:
  LiniearImplicitSolverQuad(IElphyAssemble &assemble, MElphyModel &cellModel):LinearImplicitSolver(assemble,cellModel){};
  void SolveStep( double t, double dt) override;

};*/
class ImplicitSolver : public LinearImplicitSolver {
  NewtonT<IElphyAssemble> newton;
public:
  ImplicitSolver(MCellModel &cellModel) : LinearImplicitSolver(cellModel), newton(
      std::unique_ptr<LinearSolver>(GetLinearSolverByPrefix("Elphy"))
  ) {};

  void Method(IElphyAssemble &A, Vector &V) override;

  void Step(IElphyAssemble &A, Vectors &values) override;

  bool SolvePDE(IElphyAssemble &A, Vectors &values);
  void printSolverNameInStep(IElphyAssemble &A) override;
  void printSolverNameInStep(IElphyAssemble &A,int step) override;
};


class SemiImplicitSolver : public LinearImplicitSolver {
public:
  SemiImplicitSolver(MCellModel &cellModel) : LinearImplicitSolver(cellModel) {};

  void SolvePDE(IElphyAssemble &A) override;
  void printSolverNameInStep(IElphyAssemble &A,int step) override;
  void printSolverNameInStep(IElphyAssemble &A) override;
};

class SemiImplicitSolverOnCells : public LinearImplicitSolver {

public:
  SemiImplicitSolverOnCells(MCellModel &cellModel) : LinearImplicitSolver(cellModel){};


  void Initialize(IElphyAssemble &A, Vector &potential) override;
  void printSolverNameInStep(IElphyAssemble &A) override;
  void printSolverNameInStep(IElphyAssemble &A,int step) override;
  void Method(IElphyAssemble &A, Vector &V) override;
  void Step(IElphyAssemble &A, Vectors &values, Vector &iota_c, Vector &gamma_f_c) override;
  void Step(IElphyAssemble &A, Vectors &values) override;
  void SolvePDE(IElphyAssemble &A) override;
  void SolvePDEOnCells(IElphyAssemble &A) override;
  void updateValues(Vectors &values) override;
  void updateValues(Vectors &values, Vector &gamma_f_c) override;

  void printVariableOnCells(IElphyAssemble &A,const Vector &var,const std::string &varname)const override;

};

#endif //ELPHYSOLVERS_H