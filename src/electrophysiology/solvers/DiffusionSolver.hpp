#ifndef DIFFUSIONSOLVER_HPP
#define DIFFUSIONSOLVER_HPP

#include "ElphySolver.hpp"

class DiffusionSolver : public ElphySolver {
  std::unique_ptr<Matrix> K;
  std::unique_ptr<Matrix> R;
  std::unique_ptr<Vector> b;
  std::unique_ptr<LinearSolver> S;
  bool assembleOnce{false};
  bool reassemble{false};
  bool assembled{false};
  int linearSolverVerbose;
public:
  explicit DiffusionSolver();

  bool AssemblesOnce() const;

  void Initialize(IElphyAssemble &A, Vector &v);

  void SolveStep(IElphyAssemble &A, Vector &potential);

  void Step(IElphyAssemble &A, Vectors &values) override;

  void Method(IElphyAssemble &A, Vector &potential);

  void assembleStep(IElphyAssemble &A, Vector &v);

  void assembleOnceStep(IElphyAssemble &A, Vector &v);
  void setAssembleOptions(bool splittingAssemble, bool matrixAssemble);

};

#endif //DIFFUSIONSOLVER_HPP
