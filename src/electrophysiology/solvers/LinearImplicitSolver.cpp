#include "TimeSeries.hpp"
#include "LinearImplicitSolver.hpp"

#include "cellmodels/ibt/ElphyIBT.hpp"



void LinearImplicitSolver::Initialize(IElphyAssemble &A, Vector &V) {
  gating = std::make_unique<Vectors>(M.Size(), V);
  M.Initialize(*gating);
  mingating = std::make_unique<Vectors>(M.Size(), V);
  maxgating = std::make_unique<Vectors>(M.Size(), V);
  M.Initialize(*maxgating);
  M.Initialize(*mingating);
  K = std::make_unique<Matrix>(V);
  b = std::make_unique<Vector>(V);
  if (assembleOnce) {
    MK = std::make_unique<Matrix>(V);
  }

  A.SetInitialValue(V);
  A.PrintIteration((*gating)[0]);
  printVariableOnCells(A,(*gating)[1],"Ca");

}

/*void LinearImplicitSolver::Initialize(IElphyAssemble &A, Vector &V, Vector &gamma_f_c) {
  //Initialize(A,V);
  potential=std::make_unique<Vector>(V);
  (*potential) = M.InitialValue(ELPHY);
  if ((vcw_c) == nullptr)
  {
    const Meshes &mesh = (*potential).GetMeshes();
    disc0=std::make_shared<LagrangeDiscretization >(mesh, 0, 1);
    cellSizeV=std::make_unique<Vector>(disc0);
  }
  vcw_c = std::make_unique<Vectors>(M.Size(), *cellSizeV);
  //gating_c = std::make_unique<Vectors>(M.Size(), gamma_f_c);
  M.Initialize(*vcw_c);
  K = std::make_unique<Matrix>(V);
  b = std::make_unique<Vector>(V);
  if (assembleOnce) {
    MK = std::make_unique<Matrix>(V);
  }
  A.SetInitialValue(V);
}*/

void LinearImplicitSolver::Method(IElphyAssemble &A, Vector &V) {
  Vectors vNew(M.Type() == TENSION ? 3 : 1, V);
  Initialize(A, vNew[0]);
  if (M.Type() == TENSION) {
    vNew[1] = 0.0;
    vNew[2] = 1.0;
  }
  if(plotCa){
    A.PlotCa((*gating)[1]);
  }

  while (!A.IsFinished()) {
    A.PlotIteration(vNew[0]);
    printSolverNameInStep(A);
    vout(10).StartBlock("Step Time");
    Step(A, vNew);
    vout(10).EndBlock();


  }
  V = vNew[0];
  PrintMaxMinGating();

  A.PlotIteration(V);
}

void LinearImplicitSolver::Step(IElphyAssemble &A, Vectors &values) {
  A.Initialize(values[0]);
  A.updateExternalCurrent(values[0]);

  StaggeredStep(A, values);
  updateMaxMin();
  updateValues(values);
  if (M.Type() == TENSION) {
    printVariableOnCells(A,values[1],"gamma");
  }

  A.NextTimeStep();
  A.updateActivationTime(values[0], A.Time());
  A.PrintIteration(values[0]);
}


void LinearImplicitSolver::SolveGating(double t, double dt) {
  Point P(0.0, 0.0, 0.0);
  std::vector<double> vcw(M.Size());
  int position = M.GatingIndex();
  for (row r = (*gating)[0].rows(); r != (*gating)[0].rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < M.Size(); ++i) {
        vcw[i] = (*gating)[i](r, j);
      }

      M.ExpIntegratorUpdate(dt * M.TimeScale(), vcw, vcw);

      for (int i = position; i < M.ElphySize(); ++i) {
        (*gating)[i](r, j) = vcw[i];
        if (vcw[i] < 0.0 || vcw[i] > 1.0) {
          mout << " gating " << i << " out of interval [0,1] with value " << vcw[i] << endl;
        }
      }
    }
  }

}
void LinearImplicitSolver::SolveGatingOnCells(double t,double dt){
  std::vector<double> vcwPerCell(M.Size());
  int position = M.GatingIndex();
  for (row r = (*vcw_c)[0].rows(); r != (*vcw_c)[0].rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < M.Size(); ++i) {
        vcwPerCell[i] = (*vcw_c)[i](r(), j);
      }
      M.ExpIntegratorUpdate(dt * M.TimeScale(), vcwPerCell, vcwPerCell);
      for (int i = position; i < M.ElphySize(); ++i) {
        (*vcw_c)[i](r, j) = vcwPerCell[i];
      }
    }
  }
}
void LinearImplicitSolver::SolvePDE(IElphyAssemble &A) {
  if(contains(A.GetElphyProblem().Name(),"Deformed")){
    Warning("Is the Systemmatrix correct and correctly updated? Has to be checked...")
  }
  A.updateIionVecs(*gating);
  if (assembleOnce) {
    if (!assembled) {
      vout(15).StartBlock("Assemble MPlusK");
      A.MPlusK((*gating)[0], *MK);
      vout(15).EndBlock();
      assembled = true;
    }
    //Update von K
    vout(15).StartBlock("Assemble Systemmatrix");
    *K = *MK;

    A.addReactionSAndRHS((*gating), *K, *b);
    vout(15).EndBlock();
    (*S)(*K);

  } else {
    vout(15).StartBlock("Assemble Systemmatrix");
    A.SystemMatrix((*gating), *K);
    vout(15).EndBlock();

    vout(15).StartBlock("Invert Systemmatrix");
    (*S)(*K);
    vout(15).EndBlock();
    vout(15).StartBlock("Assemble RHS");
    A.RHS((*gating), *b);
    vout(15).EndBlock();
  }

  (*gating)[0] = (*S) * (*b);
}

void LinearImplicitSolver::updateValues(Vectors &values) {
  values[0] = (*gating)[0];
  if (M.Type() == TENSION) {
    values[1] = (*gating)[M.ElphySize()];
  }
}



void LinearImplicitSolver::updateMaxMin() {
  for (row r = (*maxgating).rows(); r != (*maxgating).rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < M.Size(); i++) {
        if ((*gating)[i](r, j) < (*mingating)[i](r, j)) {
          (*mingating)[i](r, j) = (*gating)[i](r, j);
        } else if ((*gating)[i](r, j) > (*maxgating)[i](r, j)) {
          (*maxgating)[i](r, j) = (*gating)[i](r, j);
        }
      }
    }
  }
}
void LinearImplicitSolver::printSolverNameInStep(IElphyAssemble &A,int step){
  vout(1) << "# Linear implicit step " << step << " from " << A.Time() << " to "
          << A.NextTimeStep(false) << " (" << A.StepSize() << ")" << endl;
}
void LinearImplicitSolver::printSolverNameInStep(IElphyAssemble &A){
  vout(1) << "# Linear implicit step " << A.Step() + 1 << " from " << A.Time() << " to "
          << A.NextTimeStep(false) << " (" << A.StepSize() << ")" << endl;
}
void LinearImplicitSolver::PrintMaxMinGating() {
  std::vector<double> maxGatingOverVertices(M.Size());
  std::vector<double> minGatingOverVertices(M.Size());
  for (int i = 0; i < M.Size(); i++) {
    maxGatingOverVertices[i] = -infty;
    minGatingOverVertices[i] = 10000000;
  }
  for (row r = (*maxgating).rows(); r != (*maxgating).rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < M.Size(); i++) {
        if ((*mingating)[i](r, j) < minGatingOverVertices[i]) {
          minGatingOverVertices[i] = (*mingating)[i](r, j);
        } else if ((*maxgating)[i](r, j) > maxGatingOverVertices[i]) {
          maxGatingOverVertices[i] = (*maxgating)[i](r, j);
        }
      }

    }
  }
  vout(5) << " " << endl;
  vout(5) << "# Finished: " << endl;
  vout(5) << " V  Ca  d  f  h  j  m  x_1" << endl;
  vout(5) << "Max values: ";
  for (int i = 0; i < M.Size(); i++) {
    vout(5) << PPM->Max(maxGatingOverVertices[i]) << " ";
  }
  vout(5) << endl;
  vout(5) << "Min values: ";
  for (int i = 0; i < M.Size(); i++) {
    vout(5) << PPM->Min(minGatingOverVertices[i]) << " ";
  }
  vout(5) << endl;
}

void LinearImplicitSolver::SolveConcentration(IElphyAssemble &A, double dt, const Vectors &values) {
  std::vector<double> vcw(M.Size() + (M.Type() == TENSION));
  //std::cout<<"M.Size() "<<M.Size()<<" ElphySize<"<<M.ElphySize()<< " size gating "<<(*gating).size()<<endl;
  std::vector<std::vector<double>> G(ionSteps);
  for (int i = 0; i < ionSteps; ++i) {
    G[i] = std::vector<double>(M.Size());
  }
  for (row r = (*gating)[0].rows(); r != (*gating)[0].rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < M.Size(); ++i) {
        vcw[i] = (*gating)[i](r, j);
      }
      if (M.Type() == TENSION) {
        vcw[M.Size()] = values[2](r, j);
      }
      M.SetExternal(Excitation(A.ExcitationData(), r, j), Duration(A.ExcitationData(), r, j),
                            Amplitude(A.ExcitationData(), r, j));
      SolveConcentrationStep(M, A.Time(), M.TimeScale() * dt, vcw, G);
      for (int i: ConcentrationIndices(M)) {
        (*gating)[i](r, j) = vcw[i];
      }
      M.SetExternal(infty, 0.0,0.0);
      if (vcw[1] < 0.0) {
        mout << "concentration of Ca is less than zero " << vcw[1] << endl;
      }
    }
  }
}

void LinearImplicitSolver::SolveConcentrationOnCells(IElphyAssemble &A, const Vectors &values, Vector &iota_c_pot) {
  double t = A.Time();
  double dt = A.StepSize();

  std::vector<double> vcwPerCell(M.Size() + (M.Type() == TENSION));
  std::vector<std::vector<double>> G(ionSteps);
  for (int i = 0; i < ionSteps; ++i) {
    G[i] = std::vector<double>(M.Size());
  }

  for (cell c = (*vcw_c)[0].cells(); c != (*vcw_c)[0].cells_end(); ++c) {
    if (values[0].find_cell(c()) == values[0].cells_end()) continue;
    double rate=1.0;
    if (deformConcentrationStretch){
      rate = 1 / A.GetElphyProblem().Jacobian_rate(*c);
    }
    //row r = (*vcw_c)[0].find_row(c());
    //for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < M.Size(); ++i) {
        vcwPerCell[i] = (*vcw_c)[i](c(), 0);
      }
      if (M.Type() == TENSION) {
        vcwPerCell[M.Size()] = iota_c_pot(c(), 0);
      }
      M.SetExternal((*externalCurrentOnCells)[0](c(),0), (*externalCurrentOnCells)[1](c(),0),
                    (*externalCurrentOnCells)[2](c(),0));
      SolveConcentrationStep(M, t, M.TimeScale() * dt, vcwPerCell, G);
      for (int i: ConcentrationIndices(M)) {
        (*vcw_c)[i](c(), 0) = vcwPerCell[i];
        if (deformConcentrationStretch) (*vcw_c)[i](c(), 0) *= rate;
      }

    }
  //}
  M.SetExternal(infty, 0.0,0.0);
}
void LinearImplicitSolver::selectOrder(std::string o) {
  if (o == "Vcw") {
    StaggeredStep = [this](IElphyAssemble &A, const Vectors &values) {
      SolvePDE(A);
      SolveConcentration(A, A.StepSize(), values);
      SolveGating(A.Time(), A.StepSize());
    };
  } else if (o == "Vwc") {
    StaggeredStep = [this](IElphyAssemble &A, const Vectors &values) {
      SolvePDE(A);
      SolveGating(A.Time(), A.StepSize());
      SolveConcentration(A, A.StepSize(), values);
    };
  } else if (o == "cVw") {
    StaggeredStep = [this](IElphyAssemble &A, const Vectors &values) {
      SolveConcentration(A, A.StepSize(), values);
      SolvePDE(A);
      SolveGating(A.Time(), A.StepSize());
    };
  } else if (o == "cwV") {
    StaggeredStep = [this](IElphyAssemble &A, const Vectors &values) {
      SolveConcentration(A, A.StepSize(), values);
      SolveGating(A.Time(), A.StepSize());
      SolvePDE(A);
    };
  } else if (o == "wVc") {
    StaggeredStep = [this](IElphyAssemble &A, const Vectors &values) {
      SolveGating(A.Time(), A.StepSize());
      SolvePDE(A);
      SolveConcentration(A, A.StepSize(), values);
    };
  } else { //wcV
    StaggeredStep = [this](IElphyAssemble &A, const Vectors &values) {
      vout(11).StartBlock("Solve Gating");
      SolveGating(A.Time(), A.StepSize());
      vout(11).EndBlock();
      //printVariableOnCells(A,(*gating)[6],"m");
      vout(11).StartBlock("Solve Concentration");
      SolveConcentration(A, A.StepSize(), values);
      vout(11).EndBlock();
      printVariableOnCells(A,(*gating)[1],"Ca");
      vout(11).StartBlock("Solve PDE");
      SolvePDE(A);
      vout(11).EndBlock();
      //printVariableOnCells(A,(*gating)[0],"V_c");
      if (plotCa) A.PlotCa((*gating)[1]);

    };
  }
};

void LinearImplicitSolver::StaggeredStepOnCells(IElphyAssemble &A, const Vectors &values,Vector &iota_c_pot){
  SolveGatingOnCells(A.Time(), A.StepSize());
  SolveConcentrationOnCells(A,values, iota_c_pot);
  A.updateIionVecs(*vcw_c);
  SolvePDEOnCells(A);
  printVariableOnCells(A,(*vcw_c)[1],"Ca");
  //if (plotCa) A.PlotCa((*vcw_c)[1]);
}
void LinearImplicitSolver::GatingVectorAti( Vector &values, int i)const{
      values=(*gating)[i];

}
void LinearImplicitSolver::selectIonScheme(
    const std::string &solvingIonScheme) {
  if (solvingIonScheme == "ExplicitEuler") {
    ionSteps = 1;
    SolveConcentrationStep = ExplicitEulerIonStep;
  } else if (solvingIonScheme == "RungeKutta2") {
    ionSteps = 2;
    SolveConcentrationStep = RungeKutta2IonStep;
  } else if (solvingIonScheme == "RungeKutta4") {
    ionSteps = 4;
    SolveConcentrationStep = RungeKutta4IonStep;
  } else if (solvingIonScheme == "Quadratic") {
    SolveConcentrationStep = QuadraticIonStep;
  } else {
    THROW("CellModelScheme \"" + solvingIonScheme + "\" unknown.")
  }
}
void SemiImplicitSolver::printSolverNameInStep(IElphyAssemble &A,int step){
  vout(1) << "# Semi implicit step " << step << " from " << A.Time() << " to "
          << A.NextTimeStep(false) << " (" << A.StepSize() << ")" << endl;
}
void SemiImplicitSolver::printSolverNameInStep(IElphyAssemble &A){
  vout(1) << "# Semi implicit step " << A.Step() + 1 << " from " << A.Time() << " to "
          << A.NextTimeStep(false) << " (" << A.StepSize() << ")" << endl;
}

void SemiImplicitSolver::SolvePDE(IElphyAssemble &A) {
  A.updateIionVecs(*gating);
  if (!assembled || contains(A.GetElphyProblem().Name(),"Deformed")) {
    A.MPlusK((*gating)[0], *K);
    (*S)(*K);
    assembled = true;
  }
  vout(15).StartBlock("AssembleRHS ");
  A.RHS((*gating), *b);
  vout(15).EndBlock();
  (*b) -= (*K) * (*gating)[0];
  (*gating)[0] += (*S) * (*b);

}

void SemiImplicitSolverOnCells:: Initialize(IElphyAssemble &A, Vector &V){

  potential=std::make_unique<Vector>(V);
  (*potential) = M.InitialValue(ELPHY);
  if ((vcw_c) == nullptr)
  {
    const Meshes &mesh = (*potential).GetMeshes();
    disc0=std::make_unique<LagrangeDiscretization >(mesh, 0, 1);
    cellSizeV=std::make_unique<Vector>(disc0);
  }
  vcw_c = std::make_unique<Vectors>(M.Size(), *cellSizeV);


  Vector amplitude( *potential);
  Vector excitation( *potential);
  Vector duration( *potential);
  for (row r = (*potential).rows(); r != (*potential).rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
       amplitude(r,j)=Amplitude(A.ExcitationData(), r, j);
       excitation(r,j)=Excitation(A.ExcitationData(), r, j);
       duration(r,j)=Duration(A.ExcitationData(), r, j);
     }
  }
  externalCurrentOnCells=std::make_unique<Vectors>(3, *cellSizeV);
  for (cell c = (*potential).cells(); c != (*potential).cells_end(); ++c) {
    ScalarElement E((*potential),*c);
    (*externalCurrentOnCells)[0](c(),0) = E.Value(c.LocalCenter(),(excitation));
    (*externalCurrentOnCells)[1](c(),0) = E.Value(c.LocalCenter(),(duration));
    (*externalCurrentOnCells)[2](c(),0) = E.Value(c.LocalCenter(),(amplitude));

  }
  M.Initialize(*vcw_c);
  K = std::make_unique<Matrix>(V);
  b = std::make_unique<Vector>(V);

  if (assembleOnce) {
    MK = std::make_unique<Matrix>(V);
  }

  A.SetInitialValue(V);
  A.PrintIteration((*potential));
  A.PrintVariableOnCells((*vcw_c)[1],"Ca");

}
void SemiImplicitSolverOnCells::printSolverNameInStep(IElphyAssemble &A,int step) {
  vout(1) << "# Semi implicit on cells step " << step << " from " << A.Time() << " to "
          << A.NextTimeStep(false) << " (" << A.StepSize() << ")" << endl;
}
void SemiImplicitSolverOnCells::printSolverNameInStep(IElphyAssemble &A) {
  vout(1) << "# Semi implicit on cells step " << A.Step() + 1 << " from " << A.Time() << " to "
          << A.NextTimeStep(false) << " (" << A.StepSize() << ")" << endl;
}
void SemiImplicitSolverOnCells::updateValues(Vectors &values) {
  values[0] = *potential;
  if (M.Type() == TENSION) {
    THROW("This method should not be used!!!");
  }
}

void SemiImplicitSolverOnCells::updateValues(Vectors &values, Vector &gamma_f_c_pot) {
  values[0] = *potential;
  if (M.Type() == TENSION){
      for (row r = (*vcw_c)[0].rows(); r != (*vcw_c)[0].rows_end(); ++r) {
        for (int j = 0; j < r.NumberOfDofs(); ++j) {
          gamma_f_c_pot(r(),j) = (*vcw_c)[M.ElphySize()](r(),j);
        }
      }

    }
}
void SemiImplicitSolverOnCells::Method(IElphyAssemble &A, Vector &V) {
  Vectors vNew(M.Type() == TENSION ? 3 : 1, V);
  Initialize(A, vNew[0]);
  if (M.Type() == TENSION) {
    vNew[1] = 0.0;
    vNew[2] = 1.0;
  }
  while (!A.IsFinished()) {
    A.PlotIteration(vNew[0]);
    printSolverNameInStep(A);
    vout(10).StartBlock("Step Time");
    Step(A, vNew);
    vout(10).EndBlock();



  }
  V = vNew[0];
  A.PlotIteration(V);
}
void SemiImplicitSolverOnCells::Step(IElphyAssemble &A, Vectors &values) {
  A.Initialize(values[0]);
  A.updateExternalCurrent(values[0]);
  vout(11).StartBlock("Solve Gating");
  SolveGatingOnCells(A.Time(), A.StepSize());
  //printVariableOnCells(A,(*vcw_c)[6],"m");
  vout(11).EndBlock();
  vout(11).StartBlock("Solve Concentration");
  SolveConcentrationOnCells(A,values, values[0]);
  //printVariableOnCells(A,(*vcw_c)[1],"Ca");
  vout(11).EndBlock();
  vout(11).StartBlock("Solve PDE");
  A.updateIionVecs(*vcw_c);
  SolvePDEOnCells(A);
  //printVariableOnCells(A,(*vcw_c)[1],"Ca");
  vout(11).EndBlock();
  updateValues(values);
  A.NextTimeStep();
  A.updateActivationTime(values[0], A.Time());
  A.PrintIteration(values[0]);
}

void SemiImplicitSolverOnCells::Step(IElphyAssemble &A, Vectors &values, Vector &iota_c_pot, Vector &gamma_f_c_pot) {
  A.Initialize(values[0]);
  A.updateExternalCurrent(values[0]);
  StaggeredStepOnCells(A, values, iota_c_pot);

  updateValues(values,gamma_f_c_pot);
  A.PrintVariableOnCells(gamma_f_c_pot,"gamma");
  A.NextTimeStep();
  A.updateActivationTime(values[0], A.Time());
  A.PrintIteration(values[0]);
}
void SemiImplicitSolverOnCells::SolvePDEOnCells(IElphyAssemble &A){
  if (!assembled || contains(A.GetElphyProblem().Name(),"Deformed")) {
    A.MPlusK((*potential), *K);
    (*S)(*K);
    assembled = true;
  }
  A.RHS(*potential,(*vcw_c), *b);

  (*b) -= (*K) * (*potential);
  (*potential) += (*S) * (*b);

  for (cell c = (*potential).cells(); c != (*potential).cells_end(); ++c) {
    ScalarElement E((*potential),*c);
    /*
    double A = 0;
    double V = 0;
    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      A += w;
      V += w * E.Value(q, *potential, 0);
      (*vcw_c)[0](c(),0) = V / A; //
    }*/
    (*vcw_c)[0](c(),0) = E.Value(c.LocalCenter(),(*potential));
  }
}

void SemiImplicitSolverOnCells::SolvePDE(IElphyAssemble &A){
  SolvePDEOnCells(A);
}
void SemiImplicitSolverOnCells::printVariableOnCells(IElphyAssemble &A,const Vector &var,const std::string &varname) const{
  A.PrintVariableOnCells(var,varname);
}
void ImplicitSolver::printSolverNameInStep(IElphyAssemble &A,int step){
  vout(1) << "# Implicit step " << step << " from " << A.Time() << " to "
          << A.NextTimeStep(false) << " (" << A.StepSize() << ")" << endl;
}
void ImplicitSolver::printSolverNameInStep(IElphyAssemble &A) {
  vout(1) << "# Implicit step " << A.Step()+1 << " from " << A.Time() << " to "
          << A.NextTimeStep(false) << " (" << A.StepSize() << ")" << endl;
}
void ImplicitSolver::Method(IElphyAssemble &A, Vector &V) {
  Vectors vNew(M.Type() == TENSION ? 3 : 1, V);
  Initialize(A, vNew[0]);
  if (M.Type() == TENSION) {
    vNew[1] = 0.0;
    vNew[2] = 1.0;
  }

  double t = A.FirstTStep();

  A.PlotIteration(V);
  A.PrintIteration(V);

  while (!A.IsFinished()) {
    printSolverNameInStep(A);
    vout(10).StartBlock("Implicit step");

    Step(A, vNew);

    vout(10).EndBlock();


    A.PlotIteration(vNew[0]);
    A.PrintIteration(vNew[0]);
  }
  V = vNew[0];
  PrintMaxMinGating();

}

void ImplicitSolver::Step(IElphyAssemble &A, Vectors &values) {
  A.NextTimeStep();
  A.Initialize(values[0]);
  A.updateExternalCurrent(values[0]);

  SolveGating(A.Time(), A.StepSize());
  SolveConcentration(A, A.StepSize(), values);
  A.updateVCW(*gating);
  vout(15).StartBlock("Solve PDE");
  bool conv = SolvePDE(A, values);
  vout(15).EndBlock();
  if (!conv) {
    THROW("Newton method did not converge")
  }
  updateMaxMin();
  updateValues(values);

  A.updateActivationTime(values[0], A.Time());
}

bool ImplicitSolver::SolvePDE(IElphyAssemble &A, Vectors &values) {
  newton(A, values[0]);
  return newton.converged();
}


void QuadraticIonStep(MCellModel &cellModel, double t, double dt, std::vector<double> &vcw,
                      std::vector<std::vector<double>> &g) {
  cellModel.ConcentrationUpdate(vcw, g[0], cellModel.IExt(t));
  cellModel.UpdateValues(dt,vcw, g[0]);
}


void ExplicitEulerIonStep(MCellModel &cellModel, double t, double dt, std::vector<double> &vcw,
                          std::vector<std::vector<double>> &g) {
  cellModel.ConcentrationUpdate(vcw, g[0], cellModel.IExt(t));
  for (int i: ConcentrationIndices(cellModel)) {
    vcw[i] += dt * g[0][i];
  }
}

void RungeKutta2IonStep(MCellModel &cellModel, double t, double dt, std::vector<double> &vcw,
                        std::vector<std::vector<double>> &g) {
  std::vector<double> k(vcw.size());
  std::uninitialized_copy(vcw.begin(), vcw.end(), k.begin());
  auto concentrationIndices = ConcentrationIndices(cellModel);
  cellModel.ConcentrationUpdate(vcw, g[0], cellModel.IExt(t));
  for (int i: concentrationIndices) {
    k[i] = vcw[i] + 0.5 * dt * g[0][i];
  }
  cellModel.ConcentrationUpdate(k, g[1], cellModel.IExt(t + 0.5 * dt));
  for (int i: concentrationIndices) {
    vcw[i] += dt * g[1][i];
  }
}

void RungeKutta4IonStep(MCellModel &cellModel, double t, double dt, std::vector<double> &vcw,
                        std::vector<std::vector<double>> &g) {
  std::vector<double> k(vcw.size());
  std::uninitialized_copy(vcw.begin(), vcw.end(), k.begin());
  auto concentrationIndices = ConcentrationIndices(cellModel);

  cellModel.ConcentrationUpdate(k, g[0], cellModel.IExt(t));
  for (int i: concentrationIndices) {
    k[i] = vcw[i] + 0.5 * dt * g[0][i];
  }
  cellModel.ConcentrationUpdate(k, g[1], cellModel.IExt(t + 0.5 * dt));
  for (int i: concentrationIndices) {
    k[i] = vcw[i] + 0.5 * dt * g[1][i];
  }

  cellModel.ConcentrationUpdate(k, g[2], cellModel.IExt(t + 0.5 * dt));
  for (int i: concentrationIndices) {
    k[i] = vcw[i] + dt * g[2][i];
  }
  cellModel.ConcentrationUpdate(k, g[3], cellModel.IExt(t + dt));
  for (int i: concentrationIndices) {
    vcw[i] += (dt / 6.0) * (g[0][i] + 2.0 * g[1][i] + 2.0 * g[2][i] + g[3][i]);
  }
}

