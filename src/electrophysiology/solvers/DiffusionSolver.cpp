#include "DiffusionSolver.hpp"


DiffusionSolver::DiffusionSolver()
    : S(std::unique_ptr<LinearSolver>(GetLinearSolverByPrefix("Elphy"))) {
  Config::Get("ElphySolverVerbose", verbose);
  Config::Get("ReassembleRHSonce", assembleOnce);
  Config::Get("EulerReAssemble", reassemble);
  Config::Get("IextInPDE", iextInPDE);
  Config::Get("ElphyLinearSolverVerbose", linearSolverVerbose);
  S->SetVerbose(linearSolverVerbose);
}

void DiffusionSolver::setAssembleOptions(bool splittingAssemble, bool matrixAssemble) {
  assembleOnce = splittingAssemble;
  reassemble = matrixAssemble;
}

void DiffusionSolver::Initialize(IElphyAssemble &A, Vector &v) {

  K = std::make_unique<Matrix>(v);
  b = std::make_unique<Vector>(v);
  if (assembleOnce) {
    R = std::make_unique<Matrix>(v);
  }
  A.SetInitialValue(v);
}

void DiffusionSolver::Method(IElphyAssemble &A, Vector &potential) {
  //mout.StartBlock("Elphy");
  Initialize(A, potential);

  Vector vNew(potential);


  double dt = A.StepSize();
  double t = A.FirstTStep();
  while (!A.IsFinished()) {
    A.PlotIteration(vNew);
    int step = A.Step() + 1;
    vout(1) << "Solving Step " << step << " from " << t << " to "
                               << t + dt << " (" << dt << ")" << endl;
    SolveStep(A, vNew);

    A.NextTimeStep();
    t = A.Time();
    dt = A.StepSize();


  }
  //mout.EndBlock();
  A.PlotIteration(vNew);
  potential = vNew;
}

void DiffusionSolver::SolveStep(IElphyAssemble &A, Vector &potential) {
  A.Initialize(potential);
  if (assembleOnce && !contains(A.GetElphyProblem().Name(),"Deformed")) {
    assembleOnceStep(A, potential);
  } else {
    assembleStep(A, potential);
  }
}

void DiffusionSolver::assembleStep(IElphyAssemble &A, Vector &v) {
  if (!assembled || reassemble|| contains(A.GetElphyProblem().Name(),"Deformed")) {
    vout(15).StartBlock("Assemble Systemmatrix");
    A.SystemMatrix(v, *K);
    vout(15).EndBlock();

    (*S)(*K);
    assembled = true;
  }
  vout(15).StartBlock("Assemble RHS");
  A.RHS(v, *b);
  vout(15).EndBlock();
  (*b)-=(*K)*v;
  v += (*S) * (*b);
}

void DiffusionSolver::assembleOnceStep(IElphyAssemble &A, Vector &v) {
  if (!assembled || reassemble) {
    vout(15).StartBlock("Assemble Systemmatrix");
    A.SystemMatrix(v, *K);
    vout(15).EndBlock();
    (*S)(*K);

    A.RHSMAT(v, *R);
    assembled = true;
  }
  vout(15).StartBlock("Assemble RHS");
  *b = ((*R) * v); // müsste man danach accumulate aufrufen?
  if (iextInPDE) {
    vout(15).StartBlock("Add Iext");
    A.IExt(v, *b);
    vout(15).EndBlock();
  }
  vout(15).EndBlock();
  (*b)-=(*K)*v;
  v += (*S) * (*b);
}


bool DiffusionSolver::AssemblesOnce() const {
  return assembleOnce;
}

void DiffusionSolver::Step(IElphyAssemble &A, Vectors &values) {
  SolveStep(A, values[0]);
}
