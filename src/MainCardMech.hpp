#ifndef MAINH
#define MAINH

#include "m++.hpp"

#include "VtuPlot.hpp"
#include "TimeSeries.hpp"
#include "Config.hpp"
#include "CardiacInterpolation.hpp"
#include "MeshesCreator.hpp"


static std::array<double, 3> ReadTimeFromConfig(const std::string &prefix = "") {
  std::array<double, 3> times{};
  times[0] = 0.0;
  times[1] = 1.0;
  times[2] = 0.001;

  Config::Get("StartTime", times[0]);
  Config::Get("EndTime", times[1]);
  Config::Get(prefix + "DeltaTime", times[2]);
  return times;
}

/**
 * Every run of M++ creates an instance of MainCardMech,
 * then calls Initialize(), Run() and Evaluatue(*) with the solution * of Run().
 */
class MainCardMech {

protected:
  int verbose{1};
  std::string modelName{};
  std::string chamber{};
public:
  /// Resets the calculation to be run again with the given time specs
  virtual void ResetTime(double start, double end, int steps) {}

  /// Initializes all objects needed to run a calculation
  virtual void Initialize() = 0;

  /// Runs a calculation and returns the result
  virtual Vector &Run() = 0;

  /// Evaluates the result with default and problem specific metrics
  virtual std::vector<double> Evaluate(const Vector &result) = 0;

  virtual ~MainCardMech() = default;
};

#endif //MAINH
