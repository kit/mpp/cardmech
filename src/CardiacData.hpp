#ifndef CARDIACDATA_HPP
#define CARDIACDATA_HPP

#include "DataSet.hpp"
#include "dof/MultiPartDoF.hpp"
#include "Vector.hpp"

constexpr double MLtoCUBICMM = 1000.0;
constexpr double CUBICMMtoML = 1.0 / MLtoCUBICMM;

constexpr double MMHGtoPASCAL{133.3223684};
constexpr double MMHGtoKPASCAL{MMHGtoPASCAL / 1000.0};
constexpr double PASCALtoMMHG{1.0 / MMHGtoPASCAL};
constexpr double KPASCALtoMMHG{1.0 / MMHGtoKPASCAL};

// subdomains
enum CHAMBER {
  LEFT_VENTRICLE = 0,
  RIGHT_VENTRICLE = 1000,
  LEFT_ATRIUM = 2000,
  RIGHT_ATRIUM = 3000,
  LTISSUE = 4000,
  LVALVES = 5000,
  RTISSUE = 6000,
  RVALVES = 7000,
  VESSELBASE = 8000,
  VESSELS = 9000,
  PERICARD = 11000
};

std::vector<int> getIncludedChambers(const std::string &chamberName = "");

static bool inChamber(int domain, int chamber) {
  switch (chamber) {
    case 0:
      return domain == 0;
    case 1:
      return domain == 0 || domain == 1;
    case 2:
      return domain == 2 || domain == 3;
    case 3:
      return domain < 4;
    case 4:
      return domain < 10;
    case 5:
      return domain != 9;
    case 6:
      return domain == 0 || domain == 1 || domain == 8 || domain == 9;
    case 7:
      return domain == 2 || domain == 3 || domain == 8 || domain == 9;
    default:
      return true;
  }
}

static int getCellMaterial(int subdomain) {
  return subdomain % 1000;
}

static int getCellChamber(int subdomain) {
  return subdomain / 1000;
}

// === Used for MultiPart DoFs ===
// Three Parts: Atria, Ventricles and other
static int DomainPart(const Cell &c) {
  double d = c.Subdomain() / 1000.0;
  if (d < 2) return 0;
  if (d < 4) return 1;
  return 2;
}

static bool Contracts(const cell &c) {
  if (c.Subdomain()== LEFT_VENTRICLE || c.Subdomain()==  RIGHT_VENTRICLE ||
  c.Subdomain()== LEFT_ATRIUM ||c.Subdomain()==  RIGHT_ATRIUM){
    return true;
  }
  else{
    return false;
  }
}

static VectorField Direction(const DataContainer &data) {
  return VectorField(data[0], data[1], data[2]);
}

static VectorField Sheet(const DataContainer &data) {
  return VectorField(data[3], data[4], data[5]);
}

static VectorField Normal(const DataContainer &data) {
  return VectorField(data[6], data[7], data[8]);
}


static VectorField Direction(const Cell &c) {
  return Direction(c.GetData());
}

static VectorField Sheet(const Cell &c) {
  return Sheet(c.GetData());
}

static VectorField Normal(const Cell &c) {
  return Normal(c.GetData());
}

class VtuPlot;

void PlotFibreOrientation(const Vector &U, VtuPlot &plot);

void PlotFibreOrientation(const Vector &U);

static double Amplitude(const DataContainer &data) { return data[2]; }

static double Amplitude(const Vector &vec, row &r, int j = 0) { return vec(r, 2 * r.NumberOfDofs() + j); }

static double VectorAmplitude(const Vector &vec, row &r, int j = 0) {
  return vec(r,
             2 * (r.NumberOfDofs() / 3) +
             j);
}

static double Duration(const DataContainer &data) { return data[1]; }

static double Duration(const Vector &vec, row &r, int j = 0) { return vec(r, r.NumberOfDofs() + j); }

static double VectorDuration(const Vector &vec, row &r, int j = 0) { return vec(r, r.NumberOfDofs() / 3 + j); }

static double Excitation(const DataContainer &data) { return data[0]; }

static double Excitation(const Vector &vec, const row &r, int j = 0) { return vec(r, j); }

static double VectorExcitation(const Vector &vec, const row &r, int j = 0) { return vec(r, j); }


static Tensor RotationMatrix(const DataContainer &cellData) {
  VectorField f = Direction(cellData);
  VectorField s = Sheet(cellData);
  VectorField n = Normal(cellData);

  return Tensor(f, s, n).transpose();
}

static Tensor OrientationMatrix(const DataContainer &cellData) {
  VectorField f = Direction(cellData);
  VectorField s = Sheet(cellData);
  VectorField n = Normal(cellData);

  return Tensor(f, s, n);
}

static std::string FilenameTime(const std::string &name, double time) {
  int timeInMS = (int) (time * 1000.0);
  return name + std::to_string(timeInMS);
}

template<typename ElemenT>
static Tensor DeformationGradient(const ElemenT &E, int q, const Vector &u) {
  return One + E.VectorGradient(q, u);
}

template<typename ElemenT>
static Tensor DeformationGradient(const ElemenT &E, const Point &z, const Vector &u) {
  return One + E.VectorGradient(z, u);
}

double Volume(CELLTYPE type, const std::vector<Point> &corners);

static std::string stepAsString(int step) {
  char buffer[256];
  sprintf(buffer, "%04d", step);
  std::string str(buffer);
  return std::string("-") + str;
}
#endif //CARDIACDATA_HPP
