#ifndef CARDIACASSEMBLE_H
#define CARDIACASSEMBLE_H


#include <Plotting.hpp>
#include "plot/VtuPlot.hpp"
#include "discretization/IDiscretization.hpp"
#include "CardiacData.hpp"
#include "ScalarElement.hpp"
#include "LagrangeDiscretization.hpp"


const Point shift2(-exp(1.3), 0.0);

class ICardiacAssemble {
  int plotFibres{1};
protected:
  int vtkplot{0};
  int vtksteps{1};

  void setVtkSteps(const std::string& configString){
    Config::Get(configString, vtksteps);
    vtksteps = std::max(vtksteps, 1);
  }
public:
  explicit ICardiacAssemble(const std::string& plotEntry= "PlotVTK", const std::string& stepEntry = "PlottingSteps") {
    Config::Get(plotEntry, vtkplot);
    Config::Get("PlotFibres", plotFibres);

    setVtkSteps(stepEntry);
  }

  // ===== Plotting Utility ===================
  void PlotDeformed(const Vector &U, std::vector<std::pair<std::string, const Vector&>> &&values, int step, const std::string& plotName="U") const {
    if (vtkplot < 1 || step % vtksteps != 0) return;
    mout.StartBlock("PlotDeformed");
    PlotConfig plotConfig;
    plotConfig.plotBackendCreator = std::make_unique<DisplacementBackend>;
    Config::Get("ParallelPlotting", plotConfig.parallelPlotting);
    Config::Get("Compression", plotConfig.compression);

    auto &plot = mpp::deformed_plot(plotName, U, plotConfig);
    for(const auto& value : values){
      plot.AddData(value.first, value.second);
    }
    if(plotFibres){
      PlotFibreOrientation(U, plot);
    }
    plot.PlotFile(plotName + stepAsString(step));
    mout.EndBlock();
  }

  void PlotStatic( std::vector<std::pair<std::string, const Vector&>> &&values, int step, const std::string& plotName="V") const {
    if (vtkplot < 1 || step % vtksteps != 0) return;

    auto &plot = mpp::plot(plotName);
    for(int i=0;i<values.size();++i){
      const auto& value = values[i];
      plot.AddData(value.first, value.second);
      if(i==values.size()-1 && plotFibres){
        PlotFibreOrientation(value.second, plot);
      }
    }
    plot.PlotFile(plotName + stepAsString(step));
  }

  virtual void PlotBoundary(const Vector &U) const {
    Warning("Nothing to Plot")
  };

  void ToggleFibrePlotting(bool toggle){
    plotFibres = toggle;
  }
};

#endif //M_CARDIACASSEMBLE_H
