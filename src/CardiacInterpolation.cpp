#include <DGDiscretization.hpp>
#include "LagrangeDiscretization.hpp"
#include "MixedEGDiscretization.hpp"
#include "MultiPartDiscretization.hpp"
#include "CardiacInterpolation.hpp"

double linearInterpolation(double value, double x) {
  return x * value;
}


bool compare_excitation(const DataContainer &a, const DataContainer &b) {
  return Excitation(a) < Excitation(b);
}


void fillLagrangeData(Vector &vec, row r, DataContainer d) {
  int singleSize = r.NumberOfDofs() / d.size();
  if (singleSize==1){
    for (int i = 0; i < d.size(); ++i) {
      vec(r, i) = d[i];
    }
  }else if (singleSize==2){
    vec(r,0)=d[0];
    vec(r,1) =-10.0;
    vec(r,2)=d[1];
    vec(r,3) =-0.0;
    vec(r,4)=d[2];
    vec(r,5) =0.0;

  }else{
    for(int i = 0;i<r.NumberOfDofs();++i){
      vec(r,i) = d[i%d.size()];
    }

  }
  /*int singleSize = r.NumberOfDofs() / d.size();
  for (int i = 0; i < d.size(); ++i) {
    for (int j = 0; j < singleSize; ++j)
      if (j == 1 && i == 0) {
        vec(r, i + j) = -10.0;
      } else {
        vec(r, i + j) = d[i];
      }

  }*/
}

void fillLagrangeCellData(const std::vector<Point> &corners,
                          std::vector<DataContainer> data,
                          std::vector<Point> &cells, Vector &vec) {
  auto min_index = std::distance(data.begin(),
                                 std::min_element(data.begin(), data.end(), compare_excitation));
  auto max_index = std::distance(data.begin(),
                                 std::max_element(data.begin(), data.end(), compare_excitation));

  for (const auto &c : cells) {
    row fineRow = vec.find_row(c);
    if (fineRow == vec.rows_end()) {
      THROW("Couldn't find interpolated row")
    }
    if (Excitation(data[max_index]) < 0.0) {
      fillLagrangeData(vec, fineRow, data[min_index]);
    } else {
      // Calculate sum of all corner distances to cell midpoint
      double N = 0.0;
      for (const auto &x : corners) {
        N += 1.0 / dist(x, c);
      }

      // Calculate interpolated Amplitude and Excitation
      double multi_amplitude = 0.0;
      double multi_excitation = 0.0;
      for (int i = 0; i < data.size(); ++i) {
        multi_amplitude += linearInterpolation(Amplitude(data[i]), (1.0 / dist(corners[i], c)) / N);
        multi_excitation += linearInterpolation(Excitation(data[i]),
                                                (1.0 / dist(corners[i], c)) / N);
      }

      DataContainer exc_data(3);
      if (multi_excitation >= 0.0) {
        exc_data.Set(0, multi_excitation);
        exc_data.Set(1, Duration(data[max_index]));
        exc_data.Set(2, multi_amplitude);
      } else {
        exc_data.Set(0, -10.0);
        exc_data.Set(1, 0.0);
        exc_data.Set(2, 0.0);
      }

      double exc = VectorExcitation(vec, fineRow);
      if (exc < 0.0) {
        fillLagrangeData(vec, fineRow, exc_data);
      }
    }
  }
}

void interpolateTriangle(const std::vector<Point> &corners,
                         const std::vector<DataContainer> &data, Vector &vec,
                         int lvl) {


 // auto min_index = std::distance(data.begin(),
                                 //std::min_element(data.begin(), data.end(), compare_excitation));
  auto max_index = std::distance(data.begin(),
                                 std::max_element(data.begin(), data.end(), compare_excitation));

  for (int n = 0; n <= lvl; ++n) {
    for (int m = 0; m <= lvl - n; ++m) {
      Point finePoint = ((lvl - m - n) * corners[0]
                         + m * corners[1]
                         + n * corners[2]) / double(lvl);
      row fineRow = vec.find_row(finePoint);
      if (fineRow == vec.rows_end()) {
        THROW("Couldn't find interpolated row")
      }

      // ===================================================================
      if (Excitation(data[max_index]) < 0.0) {
        fillLagrangeData(vec, fineRow, data[max_index]);
      } else {
        double multi_amplitude = (
            linearInterpolation(Amplitude(data[0]), (lvl - m - n) / ((double) lvl))
            + linearInterpolation(Amplitude(data[1]), m / ((double) lvl))
            + linearInterpolation(Amplitude(data[2]), n / ((double) lvl)));
        double multi_excitation = (
            linearInterpolation(Excitation(data[0]), (lvl - m - n) / ((double) lvl))
            + linearInterpolation(Excitation(data[1]), m / ((double) lvl))
            + linearInterpolation(Excitation(data[2]), n / ((double) lvl)));
        DataContainer exc_data(3);
        if (multi_excitation >= 0.0) {
          exc_data.Set(0, multi_excitation);
          exc_data.Set(1, Duration(data[max_index]));
          exc_data.Set(2, multi_amplitude);
        } else {
          exc_data.Set(0, -10.0);
          exc_data.Set(1, 0.0);
          exc_data.Set(2, 0.0);
        }

        double exc = VectorExcitation(vec, fineRow);
        if (exc < 0.0) {
          fillLagrangeData(vec, fineRow, exc_data);
        }
      }
    }
  }
}

void interpolateQuadrilateral(
    const std::vector<Point> &corners, const std::vector<DataContainer> &data, Vector &vec,
    int lvl) {
  auto max_index = std::distance(data.begin(),
                                 std::max_element(data.begin(), data.end(), compare_excitation));
  for (int i = 0; i <= lvl; i++) {
    for (int j = 0; j <= lvl; j++) {
      auto v = corners[0] +
               j / double(lvl) * (corners[1] - corners[0]);
      auto w = corners[3] +
               j / double(lvl) * (corners[2] - corners[3]);
      Point finePoint = v + i / double(lvl) * (w - v);
      row fineRow = vec.find_row(finePoint);
      if (fineRow == vec.rows_end()) {
        THROW("Couldn't find interpolated row")
      }


      // ===================================================================
      if (Excitation(data[max_index]) < 0.0) {
        fillLagrangeData(vec, fineRow, data[max_index]);
      } else {
        auto av = linearInterpolation(Amplitude(data[0]), 1.0 - j / ((double) lvl)) +
                  linearInterpolation(Amplitude(data[1]), j / ((double) lvl));
        auto aw = linearInterpolation(Amplitude(data[3]), 1.0 - j / ((double) lvl)) +
                  linearInterpolation(Amplitude(data[2]), j / ((double) lvl));
        double multi_amplitude = av + i / ((double) lvl) * (aw - av);

        auto ev = linearInterpolation(Excitation(data[0]), 1.0 - j / ((double) lvl)) +
                  linearInterpolation(Excitation(data[1]), j / ((double) lvl));
        auto ew = linearInterpolation(Excitation(data[3]), 1.0 - j / ((double) lvl)) +
                  linearInterpolation(Excitation(data[2]), j / ((double) lvl));
        double multi_excitation = ev + i / ((double) lvl) * (ew - ev);

        DataContainer exc_data(3);
        if (multi_excitation >= 0.0) {
          exc_data.Set(0, multi_excitation);
          exc_data.Set(1, Duration(data[max_index]));
          exc_data.Set(2, multi_amplitude);
        } else {
          exc_data.Set(0, -10.0);
          exc_data.Set(1, 0.0);
          exc_data.Set(2, 0.0);
        }


        double exc = VectorExcitation(vec, fineRow);
        if (exc < 0.0) {
          fillLagrangeData(vec, fineRow, exc_data);
        }
      }
    }
  }
}


void interpolateTetrahedron(const std::vector<Point> &corners,
                            const std::vector<DataContainer> &data, Vector &vec,
                            int lvl) {

  auto max_index = std::distance(data.begin(),
                                 std::max_element(data.begin(), data.end(), compare_excitation));
  for (int k = 0; k <= lvl; ++k) {
    for (int n = 0; n <= lvl - k; ++n) {
      for (int m = 0; m <= lvl - k - n; ++m) {
        Point finePoint = ((lvl - m - n - k) * corners[0]
                           + m * corners[1]
                           + n * corners[2]
                           + k * corners[3]) / double(lvl);
        row fineRow = vec.find_row(finePoint);
        if (fineRow == vec.rows_end()) {
          THROW("Couldn't find interpolated row")
        }

        // ===================================================================
        if (Excitation(data[max_index]) < 0.0) {
          fillLagrangeData(vec, fineRow, data[max_index]);
        } else {
          double multi_amplitude = (
              linearInterpolation(Amplitude(data[0]), (lvl - m - n - k) / ((double) lvl))
              + linearInterpolation(Amplitude(data[1]), m / ((double) lvl))
              + linearInterpolation(Amplitude(data[2]), n / ((double) lvl))
              + linearInterpolation(Amplitude(data[3]), k / ((double) lvl)));
          double multi_excitation = (
              linearInterpolation(Excitation(data[0]), (lvl - m - n - k) / ((double) lvl))
              + linearInterpolation(Excitation(data[1]), m / ((double) lvl))
              + linearInterpolation(Excitation(data[2]), n / ((double) lvl))
              + linearInterpolation(Excitation(data[3]), k / ((double) lvl)));
          DataContainer exc_data(3);
          if (multi_excitation >= 0.0) {
            exc_data.Set(0, multi_excitation);
            exc_data.Set(1, Duration(data[max_index]));
            exc_data.Set(2, multi_amplitude);
          } else {
            exc_data.Set(0, -10.0);
            exc_data.Set(1, 0.0);
            exc_data.Set(2, 0.0);
          }

          double exc = VectorExcitation(vec, fineRow);
          if (exc < 0.0) {
            fillLagrangeData(vec, fineRow, exc_data);
          }
        }
      }
    }
  }
}


void interpolateHexahedron(const std::vector<Point> &corners,
                           const std::vector<DataContainer> &data, Vector &vec,
                           int lvl) {
  auto max_index = std::distance(data.begin(),
                                 std::max_element(data.begin(), data.end(), compare_excitation));


  for (int i = 0; i <= lvl; i++) {
    for (int j = 0; j <= lvl; j++) {
      for (int k = 0; k <= lvl; ++k) {
        auto v = corners[0] +
                 k / double(lvl) * (corners[1] - corners[0]);
        auto w = corners[3] +
                 k / double(lvl) * (corners[2] - corners[3]);
        auto x = corners[4] +
                 k / double(lvl) * (corners[5] - corners[4]);
        auto y = corners[7] +
                 k / double(lvl) * (corners[6] - corners[7]);
        auto r = v + j / double(lvl) * (w - v);
        auto s = x + j / double(lvl) * (y - x);
        Point finePoint = r + i / double(lvl) * (s - r);
        row fineRow = vec.find_row(finePoint);
        if (fineRow == vec.rows_end()) {
          THROW("Couldn't find interpolated row")
        }

        // ===================================================================
        if (Excitation(data[max_index]) < 0.0) {
          fillLagrangeData(vec, fineRow, data[max_index]);
        } else {
          auto av = linearInterpolation(Amplitude(data[0]), 1.0 - k / ((double) lvl)) +
                    linearInterpolation(Amplitude(data[1]), k / ((double) lvl));
          auto aw = linearInterpolation(Amplitude(data[3]), 1.0 - k / ((double) lvl)) +
                    linearInterpolation(Amplitude(data[2]), k / ((double) lvl));
          auto ax = linearInterpolation(Amplitude(data[4]), 1.0 - k / ((double) lvl)) +
                    linearInterpolation(Amplitude(data[5]), k / ((double) lvl));
          auto ay = linearInterpolation(Amplitude(data[7]), 1.0 - k / ((double) lvl)) +
                    linearInterpolation(Amplitude(data[6]), k / ((double) lvl));

          auto ar = av + j / double(lvl) * (aw - av);
          auto as = ax + j / double(lvl) * (ay - ax);
          double multi_amplitude = ar + i / double(lvl) * (as - ar);


          auto ev = linearInterpolation(Excitation(data[0]), 1.0 - k / ((double) lvl)) +
                    linearInterpolation(Excitation(data[1]), k / ((double) lvl));
          auto ew = linearInterpolation(Excitation(data[3]), 1.0 - k / ((double) lvl)) +
                    linearInterpolation(Excitation(data[2]), k / ((double) lvl));
          auto ex = linearInterpolation(Excitation(data[4]), 1.0 - k / ((double) lvl)) +
                    linearInterpolation(Excitation(data[5]), k / ((double) lvl));
          auto ey = linearInterpolation(Excitation(data[7]), 1.0 - k / ((double) lvl)) +
                    linearInterpolation(Excitation(data[6]), k / ((double) lvl));

          auto er = ev + j / double(lvl) * (ew - ev);
          auto es = ex + j / double(lvl) * (ey - ex);
          double multi_excitation = er + i / double(lvl) * (es - er);

          DataContainer exc_data(3);
          if (multi_excitation >= 0.0) {
            exc_data.Set(0, multi_excitation);
            exc_data.Set(1, Duration(data[max_index]));
            exc_data.Set(2, multi_amplitude);
          } else {
            exc_data.Set(0, -10.0);
            exc_data.Set(1, 0.0);
            exc_data.Set(2, 0.0);
          }

          double exc = VectorExcitation(vec, fineRow);
          if (exc < 0.0) {
            fillLagrangeData(vec, fineRow, exc_data);
          }
        }
      }
    }
  }
}


void interpolateLagrangeVertexData(const Mesh &coarse, Vector &vec, int lvl) {
  for (cell c = coarse.cells(); c != coarse.cells_end(); ++c) {
    std::vector<DataContainer> cornerData(c.Corners());
    for (int i = 0; i < c.Corners(); ++i) {
      cornerData[i] = coarse.find_vertex(c.Corner(i)).GetData();
    }
    switch (c.ReferenceType()) {
      case TRIANGLE:
        interpolateTriangle(c.AsVector(), cornerData, vec, lvl);
        break;
      case QUADRILATERAL:
        interpolateQuadrilateral(c.AsVector(), cornerData, vec, lvl);
        break;
      case TETRAHEDRON:
        interpolateTetrahedron(c.AsVector(), cornerData, vec, lvl);
        break;
      case HEXAHEDRON:
        interpolateHexahedron(c.AsVector(), cornerData, vec, lvl);
        break;
      default:
        THROW("Celltype not implemented")
    }
  }
}

cell findCellLocal(const Mesh &fine, Point c){
  cell child = fine.find_cell(c);
  if (child == fine.cells_end()) {
    for (cell cf = fine.cells(); cf != fine.cells_end(); ++cf) {
      Point difference = cf()-c;
      if (norm(difference) < GeometricTolerance){
        return cf;
      }
    }
  }
  return child;
}

void interpolateCellData(const Mesh &coarse, const Mesh &fine) {
  for (cell c = coarse.cells(); c != coarse.cells_end(); ++c) {
    const auto &cellData = c.GetData();
    for (int i = 0; i < c.Children(); ++i) {
      findCellLocal(fine, c.Child(i)).SetData(cellData);
    }
  }
}


void smoothMeshData(const Mesh &mesh, Vector &vec) {
  for (cell c = mesh.cells(); c != mesh.cells_end(); ++c) {
    double max_exc_time = -10.0;
    double max_amplitude = 0.0;
    double max_duration = 0.0;

    for (int i = 0; i < c.Corners(); ++i) {
      max_exc_time = std::max(max_exc_time, mesh.find_vertex(c.Corner(i)).GetData()[0]);
      max_duration = std::max(max_duration, mesh.find_vertex(c.Corner(i)).GetData()[1]);
      max_amplitude = std::max(max_amplitude, mesh.find_vertex(c.Corner(i)).GetData()[2]);
    }
    if (max_exc_time >= 0.0 && max_amplitude > 0.0) {
      for (int i = 0; i < c.Corners(); ++i) {
        double amplitude =
            std::min(mesh.find_vertex(c.Corner(i)).GetData()[2], max_amplitude);
        DataContainer exc_data({max_exc_time, max_duration, amplitude});
        row cornerRow = vec.find_row(c.Corner(i));
        mesh.find_vertex(c.Corner(i)).SetData(exc_data);
      }
    }
  }
}


void interpolateLagrangeCellData(const Meshes &meshes, Vector &vec) {
  const auto &coarse = meshes.coarse();

  for (cell c = coarse.cells(); c != coarse.cells_end(); ++c) {
    // Fetch coarse cell corners and data
    const auto &cellCorners = c.AsVector();
    std::vector<DataContainer> cellData{};
    for (const auto &x : cellCorners) {
      cellData.emplace_back(coarse.find_vertex(x).GetData());
    }

    // Find all children of the finest level of this cell
    std::vector<std::vector<Point>> children;
    children.emplace_back(std::vector<Point>{c()});
    for (int l = 1; l <= meshes.Level(); ++l) {
      std::vector<Point> nextChildren{};
      for (const auto &x : children[l - 1]) {
        cell parent = meshes[l - 1].find_cell(x);
        for (int i = 0; i < parent.Children(); ++i) {
          nextChildren.emplace_back(parent.Child(i));
        }
      }
      children.emplace_back(nextChildren);
    }

    // Fill child cell rows with interpolated data
    fillLagrangeCellData(cellCorners, cellData, children[children.size() - 1], vec);
  }
}


void interpolateLagrangeData(Vector &vec, int degree) {
  const auto &meshes = vec.GetDisc().GetMeshes();
  std::string interpolation{};
  Config::Get("ExternalCurrentSmoothingInSpace", interpolation);
  if (interpolation != "discrete" && meshes[0].Name() != "BiVentricle_smoothed" && meshes[0].Name() != "KovachevaBiventricle"&& meshes[0].Name() != "BiventricleDirichlet" && meshes[0].Name()!="KovacheveBiventricleSimpleExcitation_smooth") {
    smoothMeshData(meshes.coarse(), vec);
  }

    InterpolateCellData(meshes);

  if (degree > 0) {
    interpolateLagrangeVertexData(meshes.coarse(), vec, pow(2, meshes.Level()) * degree);
  } else {
    interpolateLagrangeCellData(meshes, vec);
  }
}

void InterpolateCellData(const Meshes &meshes) {
  for (int i = 1; i <= meshes.Level(); ++i) {
    interpolateCellData(meshes[i - 1], meshes[i]);
  }
}

Vector InterpolateMeshData(std::shared_ptr<const IDiscretization> disc){
  Vector interpolated(0.0, disc);
  const auto &mesh = disc->GetMeshes().coarse();

  if (typeid(*disc) == typeid(LagrangeDiscretization)) {
    const auto &lagrangeDisc = std::dynamic_pointer_cast<const LagrangeDiscretization>(disc);
    int d = lagrangeDisc->Degree();
    interpolated = -100.0;
    interpolateLagrangeData(interpolated, d);
  } else if (typeid(*disc) == typeid(MultiPartDiscretization)) {
    const auto &multiPartDisc = std::dynamic_pointer_cast<const MultiPartDiscretization>(disc);
    int d = multiPartDisc->Degree();
    interpolated = -100.0;
    interpolateLagrangeData(interpolated, d);
  } else if(typeid(*disc) == typeid(DGDiscretization)) {
    interpolated = -100.0;
    interpolateLagrangeData(interpolated, 0);
  } else if(typeid(*disc) == typeid(MixedEGDiscretization)) {
    interpolated = -100.0;
    interpolateLagrangeData(interpolated, 0);
  } else {
    THROW("Can't interpolate discretization type")
  }

  return interpolated;
}
