#include "fenv.h"

#include "electrophysiology/MainMonodomain.hpp"
#include "elasticity/MultiElasticity.hpp"
#include "elasticity/MultiDiscElasticity.hpp"
#include "coupled/MainCoupled.hpp"


int main(int argc, char **argv) {
  Config::SetGeoPath(std::string(ProjectSourceDir) + "/geo/");
  Config::SetConfPath(std::string(ProjectSourceDir) + "/conf/");
  Config::SetConfigFileName("m++.conf");
  Mpp::initialize(&argc, argv);
  Config::PrintInfo();
  bool clear = false;
  Config::Get("ClearData", clear);
  if (clear && PPM->master()) {
    int cleared = system("exec rm -rf data/vtu/*");
  }

  Date Start;

  std::string model = "";
  Config::Get("Model", model);

  bool reference{false};
  bool multiDiscs{false};
  Config::Get("CalculateReference", reference);
  Config::Get("multiDiscs", multiDiscs);

  MainCardMech *cmMain;
  if (model == "Monodomain") {
    mout << "Starting Electrophysiology Suite of M++." << endl;
    cmMain = new MainMonodomain();
  } else if (model.find("Elasticity") != std::string::npos) {
    mout << "Starting Elasticity Suite of M++." << endl;
    if (reference)
      cmMain = new MultiElasticity();
    else if(multiDiscs)
      cmMain = new MultiDiscElasticity();
    else
      cmMain = new MainElasticity();
  } else if (model.find("Coupled") != std::string::npos) {
    mout << "Starting Coupled Suite of M++." << endl;
    cmMain = new MainCoupled();
  } else {
    THROW("No Model named " + model)
  }

  cmMain->Initialize();
  feenableexcept(FE_INVALID | FE_OVERFLOW);
  auto solution = cmMain->Run();
  cmMain->Evaluate(solution);

  mout << "Finished Calculations after " << Date() - Start << " seconds" << endl;

  delete cmMain;
  return 0;
}
