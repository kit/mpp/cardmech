#include "CardiacData.hpp"

double triangleVolume(const std::vector<Point> &corners) {
  // Stable Herons formula
  std::array<double, 3> sides{dist(corners[0], corners[1]), dist(corners[1], corners[2]),
                              dist(corners[2], corners[0])};
  std::sort(sides.begin(), sides.end());
  double a = sides[0], b = sides[1], c = sides[2];

  return 0.25 * sqrt((a + (b + c)) * (c - (a - b)) * (c + (a - b)) * (a + (b - c)));
}

double tetrahedronVolume(const std::vector<Point> &corners) {
  Tensor vertices(VectorField(corners[0] - corners[3]), VectorField(corners[1] - corners[3]),
                  VectorField(corners[2] - corners[3]));
  return 1.0 / 6.0 * abs(det(vertices));
}

double quadrilateralVolume(const std::vector<Point> &corners) {
  return triangleVolume({corners[0], corners[1], corners[3]}) +
         triangleVolume({corners[1], corners[2], corners[3]});
}

double hexahedronVolume(const std::vector<Point> &corners) {
  // Split arbitrary into 6 hexahedrons
  return tetrahedronVolume({corners[0], corners[1], corners[5], corners[6]})
         + tetrahedronVolume({corners[0], corners[1], corners[2], corners[6]})
         + tetrahedronVolume({corners[0], corners[3], corners[2], corners[6]})
         + tetrahedronVolume({corners[0], corners[3], corners[7], corners[6]})
         + tetrahedronVolume({corners[0], corners[4], corners[7], corners[6]})
         + tetrahedronVolume({corners[0], corners[4], corners[5], corners[6]});
}

double Volume(CELLTYPE type, const std::vector<Point> &corners) {
  switch (type) {
    case INTERVAL:
      return dist(corners[0], corners[1]);
    case TRIANGLE:
      return triangleVolume(corners);
    case TETRAHEDRON:
      return tetrahedronVolume(corners);
    case QUADRILATERAL:
      return quadrilateralVolume(corners);
    case HEXAHEDRON:
      return hexahedronVolume(corners);
    default:
      THROW("No Volume for CELLTYPE " + std::to_string(type) + "defined");
  }
}

void emplace_chamber(CHAMBER c, std::vector<int> & chambers){
  for (int i = 0; i < 18; i++) {
    chambers.emplace_back(c + i);
    chambers.emplace_back(c + 100 + i);
  }
}
std::vector<int> getIncludedChambers(const std::string &chamberName) {
  std::vector<int> chambers{};
  if (chamberName == "leftventricle") {
    emplace_chamber(LEFT_VENTRICLE, chambers);
  } else if (chamberName == "ventriclesonly") {
    emplace_chamber(LEFT_VENTRICLE, chambers);
    emplace_chamber(RIGHT_VENTRICLE, chambers);
  } else if (chamberName == "atriaonly") {
    emplace_chamber(LEFT_ATRIUM, chambers);
    emplace_chamber(RIGHT_ATRIUM, chambers);
  } else if (chamberName == "fourchamber") {
    emplace_chamber(LEFT_VENTRICLE, chambers);
    emplace_chamber(RIGHT_VENTRICLE, chambers);
    emplace_chamber(LEFT_ATRIUM, chambers);
    emplace_chamber(RIGHT_ATRIUM, chambers);
  } else if (chamberName == "vessel_base") {
    emplace_chamber(LEFT_VENTRICLE, chambers);
    emplace_chamber(RIGHT_VENTRICLE, chambers);
    emplace_chamber(LEFT_ATRIUM, chambers);
    emplace_chamber(RIGHT_ATRIUM, chambers);
    emplace_chamber(LTISSUE, chambers);
    emplace_chamber(LVALVES, chambers);
    emplace_chamber(RTISSUE, chambers);
    emplace_chamber(RVALVES, chambers);
    emplace_chamber(VESSELBASE, chambers);
  } else if (chamberName == "with_vessels") {
    emplace_chamber(LEFT_VENTRICLE, chambers);
    emplace_chamber(RIGHT_VENTRICLE, chambers);
    emplace_chamber(LEFT_ATRIUM, chambers);
    emplace_chamber(RIGHT_ATRIUM, chambers);
    emplace_chamber(LTISSUE, chambers);
    emplace_chamber(LVALVES, chambers);
    emplace_chamber(RTISSUE, chambers);
    emplace_chamber(RVALVES, chambers);
    emplace_chamber(VESSELBASE, chambers);
    emplace_chamber(VESSELS, chambers);
  }

  return chambers;
}

#include "Plotting.hpp"
#include "LagrangeDiscretization.hpp"

void PlotFibreOrientation(const Vector &U, VtuPlot& plot) {
  auto fibreDisc = std::make_shared<LagrangeDiscretization>(U.GetDisc().GetMeshes(), 0, SpaceDimension);
  Vector f(0.0, fibreDisc);
  Vector s(0.0, fibreDisc);
  Vector n(0.0, fibreDisc);

  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    const auto &data = c.GetData();
    VectorField fibre = Direction(data);
    VectorField sheet = Sheet(data);
    VectorField normal = Normal(data);

    for (int d = 0; d < SpaceDimension; ++d) {
      f(c(), d) = fibre[d];
      s(c(), d) = sheet[d];
      n(c(), d) = normal[d];
    }

  }
  plot.AddData("Fibre", f);
  plot.AddData("Sheet", s);
  plot.AddData("Normal", n);
}

void PlotFibreOrientation(const Vector &U) {
  auto &plot = mpp::plot("Orientation");
  PlotFibreOrientation(U, plot);
  plot.PlotFile("Orientation");
}
