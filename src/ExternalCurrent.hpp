#ifndef EXTERNALCURRENT_HPP
#define EXTERNALCURRENT_HPP


#include <functional>
#include "Config.hpp"

/**
 * Globally accessible variant of the excitation method.
 * Has to be initialized by either a custom method or calling @SelectExternalCurrentFunction()
 */
extern std::function<double(double, double, double, double)> excitationFunc;

/// Unsmoothed external current function
double IextStepfct(double t, double amp, double exc, double dur);

/// Smoothes the external current by applying an arctan-sigmoid function
double IextArcTan(double t, double amp, double exc, double dur);

/// Smoothes the external current by applying an tanh-sigmoid function
double IextTanh(double t, double amp, double exc, double dur);

/// Initializes @excitationFunc with the specified method
void SelectExternalCurrentFunction(const std::string &externalCurrentFunction);

/// Initializes @excitationFunc with the method specified in elphy.conf:
/// ExternalCurrentSmoothingInTime = Stepfunction,Arctan,Tanh
void SelectExternalCurrentFunction();

#endif //EXTERNALCURRENT_HPP
