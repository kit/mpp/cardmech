#ifndef MTENTUSSCHER_HPP
#define MTENTUSSCHER_HPP


#include "MCellModel.hpp"

// Variable indexing
#define TT_Pot(v) v[0]
#define TT_Ca(v) v[1]
#define TT_Ca_SS(v) v[2]
#define TT_Ca_SR(v) v[3]
#define TT_Na(v) v[4]
#define TT_K(v) v[5]
#define TT_m(v) v[6]
#define TT_h(v) v[7]
#define TT_j(v) v[8]
#define TT_x_r1(v) v[9]
#define TT_x_r2(v) v[10]
#define TT_x_s(v) v[11]
#define TT_s(v) v[12]
#define TT_r(v) v[13]
#define TT_d(v) v[14]
#define TT_f(v) v[15]
#define TT_f_2(v) v[16]
#define TT_f_Ca(v) v[17]
#define TT_R_quer(v) v[18]

// Ion current indexing
#define TT_I_Na(currents) currents[0]
#define TT_I_CaL(currents) currents[1]
#define TT_I_Ks(currents) currents[2]
#define TT_I_to(currents) currents[3]
#define TT_I_Kr(currents) currents[4]
#define TT_I_K1(currents) currents[5]
#define TT_I_NaCa(currents) currents[6]
#define TT_I_NaK(currents) currents[7]
#define TT_I_pCa(currents) currents[8]
#define TT_I_pK(currents) currents[9]
#define TT_I_bCa(currents) currents[10]
#define TT_I_bNa(currents) currents[11]

class MTenTusscher : public MElphyModel {
  double R = 8314.472;
  double T = 310;
  double F = 96485.3415;
  double V_mCaL= 15.0; // x
  double V_mpK1 = 25.0;
  double V_mpK2 = 5.98;
  double K_e = 5.4;
  double Na_e = 140.0;
  double Ca_e = 2.0;
  double V_c = 0.016404;
  double G_Na = 14.838;
  double G_CaL = 0.0000398;
  double G_Ks = 0.098;
  double G_to = 0.294;
  double G_Kr = 0.153;
  double G_K1 = 5.405;
  double G_pK = 0.0146;
  double G_bNa = 0.00029;
  double G_bCa = 0.000592;
  double G_pCa = 0.1238;
  double k_NaCa = 1000;
  double gamma = 0.35;
  double alpha = 2.5;
  double k_sat = 0.1;
  double K_mNai = 87.5;
  double K_mCa = 1.38;
  double K_mK = 1.0;
  double K_mNa = 40; //knak=2.724 direkt in Formel eingesetzt
  double K_pCa = 0.0005;
  double p_KNa = 0.03;
  double V_SS = 0.00005468;
  double V_SR = 0.001094;
  double V_leak = 0.00036; // x
  double V_maxup = 0.006375;
  double K_up = 0.00025;
  double V_rel = 0.102;
  double V_xfer = 0.0038;
  double k_1prime = 0.15;
  double k_2prime = 0.045;
  double k_3 = 0.06;
  double k_4 = 0.005;
  double max_sr = 2.5;
  double min_sr = 1.0;
  double EC = 1.5;

  //Calcium buffering dynamics
  double Bufc = 0.2;
  double Kbufc = 0.001;
  double Bufsr = 10.;
  double Kbufsr = 0.3;
  double Bufss = 0.4;
  double Kbufss = 0.00025;

  double Capacitance = 0.185;

  double initialPotential() const override { return -85.23; } //-86.2//-85.423

  double oQuer(const std::vector<double> &var) const;

  std::vector<double> calculateCurrents(const std::vector<double> &var) const;

public:
  explicit MTenTusscher() : MElphyModel(19, 1000.0, 1e3) {
    amplitude = 20;
  }

  explicit MTenTusscher(double exc, double dur) : MTenTusscher() {
    SetExternal(exc, dur, 20.0);
  }

  int CalciumIndex() const override { return 1; }

  int GatingIndex() const override { return 6; }

  void Initialize(Vectors &VW) override;

  void Initialize(std::vector<double> &vw) override;

  void IIonUpdate(const std::vector<double> &var, std::vector<double> &g,
                  double iExt) const override;

  void ConcentrationUpdate(const std::vector<double> &var,
                           std::vector<double> &g, double iExt) const override;

  void GatingUpdate(const std::vector<double> &var,
                    std::vector<double> &g) const override;

  void ExpIntegratorUpdate(double dt, const std::vector<double> &var,
                           std::vector<double> &g) const override;

  std::vector<double> Tau(const std::vector<double> &var) const override;

  std::vector<double> YInfty(const std::vector<double> &var) const override;

  void UpdateValues(double dt, std::vector<double> &var,
                    const std::vector<double> &g) const;

  double GetIion(const std::vector<double> &var) const override;
};

/*
class MTenTusscherEpi:public MTenTusscher{

public:
  explicit MTenTusscherEpi(): MTenTusscher(){
    setGto(0.294);
    setGks(0.392);
  }

};
class MTenTusscherEndo:public MTenTusscher{
public:
  explicit MTenTusscherEndo(): MTenTusscher(){
    setGto(0.073);
    setGks(0.392);
    setTauC(9,0.0,1.0,1.0,8.0);
    setTauC1(0,1.0,67.0,1.0);
    setYinfty(6,1.0, 28.0, -1.0, 5.0, 1.0, 0.0);
  }

};
*/
#endif //MTENTUSSCHER_HPP
