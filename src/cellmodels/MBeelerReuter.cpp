#include "MBeelerReuter.hpp"


#define BR_tau_d(V) (1./(alpha(0,V)  + beta(0,V) ))
#define BR_tau_f(V) (1./(alpha(1,V)  + beta(1,V) ))
#define BR_tau_h(V) (1./(alpha(2,V)  + beta(2,V) ))
#define BR_tau_j(V) (1./(alpha(3,V)  + beta(3,V) ))
#define BR_tau_m(V) (1./(alpha(4,V)  + beta(4,V) ))
#define BR_tau_x_1(V) (1./(alpha(5,V)  + beta(5,V) ))

#define BR_yinfty_d(V)   (alpha(0,V) * BR_tau_d(V)  )
#define BR_yinfty_f(V)   (alpha(1,V) * BR_tau_f(V)  )
#define BR_yinfty_h(V)   (alpha(2,V) * BR_tau_h(V)  )
#define BR_yinfty_j(V)   (alpha(3,V) * BR_tau_j(V)  )
#define BR_yinfty_m(V)   (alpha(4,V) * BR_tau_m(V)  )
#define BR_yinfty_x_1(V) (alpha(5,V) * BR_tau_x_1(V))

double MBeelerReuter::GetIion(const std::vector<double> &var) const {
  double mVolt = var[0];
  double Ca = var[1];
  double d = var[2];
  double f = var[3];
  double h = var[4];
  double j = var[5];
  double m = var[6];
  double x1 = var[7];

  double I_Na = (g_Na * m * m * m * h * j + g_NaC) * (mVolt - E_Na);
  double I_s = g_s * d * f * (mVolt - (-82.3 - 13.0287 * log(Ca)));
  double I_x1 = x1 * ((0.8 * (exp(0.04 * (mVolt + 77)) - 1)) / (exp(0.04 * (mVolt + 35))));
  double I_K = 0.35 * (((4 * (exp(0.04 * (mVolt + 85)) - 1)) /
                        (exp(0.08 * (mVolt + 53)) + exp(0.04 * (mVolt + 53)))) +
                       ((0.2 * (mVolt + 23)) / (1 - exp(-0.04 * (mVolt + 23)))));
  return (I_Na + I_s + I_x1 + I_K);
}

void MBeelerReuter::Initialize(Vectors &VW) {
  BR_Pot(VW) = initialPotential(); //V
  BR_Ca(VW) = 0.0000002;  // Ca
  BR_d(VW) = 0.002980; //d
  BR_f(VW) = 1.0; //f
  BR_h(VW) = 0.9877; //h
  BR_j(VW) = 0.975; //j
  BR_m(VW) = 0.011; //m
  BR_x_1(VW) = 0.00565;//x_1
}

void MBeelerReuter::Initialize(std::vector<double> &vw) {
  BR_Pot(vw) = initialPotential(); //V
  BR_Ca(vw) = 0.0000002;  // Ca
  BR_d(vw) = 0.002980; //d
  BR_f(vw) = 1.0; //f
  BR_h(vw) = 0.9877; //h
  BR_j(vw) = 0.975; //j
  BR_m(vw) = 0.011; //m
  BR_x_1(vw) = 0.00565;//x_1
}

double MBeelerReuter::alpha(int i, double V) const {

  return
      (alphaC[i][0] * exp(alphaC[i][1] * (V + alphaC[i][2])) + alphaC[i][3] * (V + alphaC[i][4])) /
      (exp(alphaC[i][5] * (V + alphaC[i][2])) + alphaC[i][6]);

}

double MBeelerReuter::beta(int i, double V) const {
  return (betaC[i][0] * exp(betaC[i][1] * (V + betaC[i][2])) + betaC[i][3] * (V + betaC[i][4])) /
         (exp(betaC[i][5] * (V + betaC[i][2])) + betaC[i][6]);
}

double MBeelerReuter::dAlpha(int i, double V) const {
  double a =
      alphaC[i][0] * exp(alphaC[i][1] * (V + alphaC[i][2])) + alphaC[i][3] * (V + alphaC[i][4]);
  double da = alphaC[i][1] * alphaC[i][0] * exp(alphaC[i][1] * (V + alphaC[i][2])) + alphaC[i][3];
  double b = exp(alphaC[i][5] * (V + alphaC[i][2])) + alphaC[i][6];
  double db = alphaC[i][5] * exp(alphaC[i][5] * (V + alphaC[i][2]));
  return (b * da - a * db) / (b * b);
}

double MBeelerReuter::dBeta(int i, double V) const {
  double a = betaC[i][0] * exp(betaC[i][1] * (V + betaC[i][2])) + betaC[i][3] * (V + betaC[i][4]);
  double da = betaC[i][1] * betaC[i][0] * exp(betaC[i][1] * (V + betaC[i][2])) + betaC[i][3];
  double b = exp(betaC[i][5] * (V + betaC[i][2])) + betaC[i][6];
  double db = betaC[i][5] * exp(betaC[i][5] * (V + betaC[i][2]));
  return (b * da - a * db) / (b * b);
}

double MBeelerReuter::JacobiEntry(int i, int j, const std::vector<double> &VW) const {
  switch (i) {
    case 0:
      return dIion(j, VW);
    case 1:
      return dGc(j, VW);
    default:
      return dGw(i, j, VW);
  }
}

double MBeelerReuter::dIiondV(const std::vector<double> &VW) const {
  double V = BR_Pot(VW);
  double dI_NaToV = g_Na * BR_m(VW) * BR_m(VW) * BR_m(VW) * BR_h(VW) * BR_j(VW) + g_NaC;
  double dI_sToV = g_s * BR_d(VW) * BR_f(VW);
  double a = 1.4 * exp(0.04 * (V + 85)) - 1.4;
  double b = exp(0.08 * (V + 53)) + exp(0.04 * (V + 53));
  double dA = ((0.04 * 1.4 * exp(0.04 * (V + 85))) * b -
               a * (0.08 * exp(0.08 * (V + 53)) + 0.04 * exp(0.04 * (V + 53)))) / (b * b);
  double dB =
      (0.07 * (1 - exp(-0.04 * (V + 23))) - 0.07 * (V + 23) * 0.04 * exp(-0.04 * (V + 23))) /
      ((1 - exp(-0.04 * (V + 23))) * (1 - exp(-0.04 * (V + 23))));
  double dI_KToV = dA + dB;
  double dI_x1ToV = 0.0078911 * BR_x_1(VW) * exp(-0.04 * V);//oder *-1?
  return -1. * (-dI_NaToV - dI_sToV - dI_KToV - dI_x1ToV);
}

double MBeelerReuter::dIion(int j, const std::vector<double> &VW) const {
  switch (j) {
    case 0:
      return dIiondV(VW);
    case 1:
      return g_s * BR_d(VW) * BR_f(VW) * 13.0287 * (1 / BR_Ca(VW));
    case 2:
      return g_s * BR_f(VW) * (BR_Pot(VW) + 82.3 + 13.0287 * log(BR_Ca(VW)));
    case 3:
      return g_s * BR_d(VW) * (BR_Pot(VW) + 82.3 + 13.0287 * log(BR_Ca(VW)));
    case 4:
      return g_Na * BR_m(VW) * BR_m(VW) * BR_m(VW) * BR_j(VW) * (BR_Pot(VW) - E_Na);
    case 5:
      return g_Na * BR_m(VW) * BR_m(VW) * BR_m(VW) * BR_h(VW) * (BR_Pot(VW) - E_Na);
    case 6:
      return 3 * g_Na * BR_m(VW) * BR_m(VW) * BR_h(VW) * BR_j(VW) * (BR_Pot(VW) - E_Na);
    case 7:
      return ((0.8 * (exp(0.04 * (BR_Pot(VW) + 77)) - 1)) / (exp(0.04 * (BR_Pot(VW) + 35))));
    default:
      THROW("Index j is too large")
  }
}

double MBeelerReuter::dGc(int j, const std::vector<double> &VW) const {
  switch (j) {
    case 0:
      return -0.0000001 * g_s * BR_d(VW) * BR_f(VW);
    case 1:
      return -0.07 - 0.0000001 * g_s * BR_d(VW) * BR_f(VW) * 13.0287 * (1 / BR_Ca(VW));
    case 2:
      return -0.0000001 * g_s * BR_f(VW) * (BR_Pot(VW) + 82.3 + 13.0287 * log(BR_Ca(VW)));
    case 3:
      return -0.0000001 * g_s * BR_d(VW) * (BR_Pot(VW) + 82.3 + 13.0287 * log(BR_Ca(VW)));
    default:
      return 0.0;
  }
}

double MBeelerReuter::dGw(int i, int j, const std::vector<double> &VW) const {
  double V = BR_Pot(VW);
  switch (j) {
    case 0 :
      return dAlpha(i - 2, V) - VW[i] * (dAlpha(i - 2, V) + dBeta(i - 2, V));
    case 1 :
      return 0.0;
    default:
      return (i == j) * -1. * (alpha(i - 2, V) + beta(i - 2, V));
  }
}

std::vector<double> MBeelerReuter::Tau(const std::vector<double> &var) const {
  auto V = BR_Pot(var);
  return {
      BR_tau_d(V),
      BR_tau_f(V),
      BR_tau_h(V),
      BR_tau_j(V),
      BR_tau_m(V),
      BR_tau_x_1(V)};
}

std::vector<double> MBeelerReuter::YInfty(
    const std::vector<double> &var) const {
  auto V = BR_Pot(var);
  return {
      BR_yinfty_d(V),
      BR_yinfty_f(V),
      BR_yinfty_h(V),
      BR_yinfty_j(V),
      BR_yinfty_m(V),
      BR_yinfty_x_1(V)
  };
}

void MBeelerReuter::ExpIntegratorUpdate(double dt,
                                        const std::vector<double> &var,
                                        std::vector<double> &g) const {
  if (!useExpInt) return;

  double V = BR_Pot(var);
  BR_d(g) = (BR_yinfty_d(V) - (BR_yinfty_d(V) - BR_d(var)) * exp(-dt / BR_tau_d(V)));
  BR_f(g) = (BR_yinfty_f(V) - (BR_yinfty_f(V) - BR_f(var)) * exp(-dt / BR_tau_f(V)));
  BR_h(g) =(BR_yinfty_h(V) - (BR_yinfty_h(V) - BR_h(var)) * exp(-dt / BR_tau_h(V)));
  BR_j(g) = (BR_yinfty_j(V) - (BR_yinfty_j(V) - BR_j(var)) * exp(-dt / BR_tau_j(V)));
  BR_m(g) = (BR_yinfty_m(V) - (BR_yinfty_m(V) - BR_m(var)) * exp(-dt / BR_tau_m(V)));
  BR_x_1(g) =  (BR_yinfty_x_1(V) - (BR_yinfty_x_1(V) - BR_x_1(var)) * exp(-dt / BR_tau_x_1(V)));
}

void
MBeelerReuter::IIonUpdate(const std::vector<double> &var, std::vector<double> &g, double iExt) const {

  auto V = BR_Pot(var);

  double iNa =
      (g_Na * BR_m(var) * BR_m(var) * BR_m(var) * BR_h(var) * BR_j(var) + g_NaC) * (V - E_Na);
  double iS = g_s * BR_d(var) * BR_f(var) *
              (V - (-82.3 - 13.0287 * log(BR_Ca(var))));
  double iX1 = BR_x_1(var) * ((0.8 * (exp(0.04 * (V + 77)) - 1)) / (exp(0.04 * (V + 35))));
  double iK = 0.35 * (((4 * (exp(0.04 * (V + 85)) - 1)) /
                       (exp(0.08 * (V + 53)) + exp(0.04 * (V + 53)))) +
                      ((0.2 * (V + 23)) / (1 - exp(-0.04 * (V + 23)))));

  BR_Pot(g) = -1.0 * (iNa + iS + iX1 + iK );//*0.001
  if(!iextInPDE){
    BR_Pot(g)+=iExt;
  }
}

void MBeelerReuter::ConcentrationUpdate(const std::vector<double> &var,
                                        std::vector<double> &g,
                                        double iExt) const {
  double I_s = g_s * BR_d(var) * BR_f(var) *
               (BR_Pot(var) - (-82.3 - 13.0287 * log(BR_Ca(var))));
  BR_Ca(g) = -0.0000001 * I_s + 0.07 * (0.0000001 - BR_Ca(var));
}

void MBeelerReuter::GatingUpdate(const std::vector<double> &var,
                                 std::vector<double> &g) const {
  if (useExpInt) return;
  auto V = BR_Pot(var);

  BR_d(g) = (BR_yinfty_d(V) - BR_d(var)) / BR_tau_d(V);
  BR_f(g) = (BR_yinfty_f(V) - BR_f(var)) / BR_tau_f(V);
  BR_h(g) = (BR_yinfty_h(V) - BR_h(var)) / BR_tau_h(V);
  BR_j(g) = (BR_yinfty_j(V) - BR_j(var)) / BR_tau_j(V);
  BR_m(g) = (BR_yinfty_m(V) - BR_m(var)) / BR_tau_m(V);
  BR_x_1(g) = (BR_yinfty_x_1(V) - BR_x_1(var)) / BR_tau_x_1(V);
}

void MBeelerReuter::UpdateValues(double dt, std::vector<double> &var,
                                 const std::vector<double> &g) const {
  BR_Pot(var) += dt * BR_Pot(g);
  BR_Ca(var) += dt * BR_Ca(g);
  if (useExpInt) {
    BR_d(var) = BR_d(g);
    BR_f(var) = BR_f(g);
    BR_h(var) = BR_h(g);
    BR_j(var) = BR_j(g);
    BR_m(var) = BR_m(g);
    BR_x_1(var) = BR_x_1(g);
  }else{
    BR_d(var) += dt * BR_d(g);
    BR_f(var) += dt * BR_f(g);
    BR_h(var) += dt * BR_h(g);
    BR_j(var) += dt * BR_j(g);
    BR_m(var) += dt * BR_m(g);
    BR_x_1(var) += dt * BR_x_1(g);
  }

}
