#ifndef MBEELERREUTER_HPP
#define MBEELERREUTER_HPP

#include "MCellModel.hpp"

#define BR_Pot(v) v[0]
#define BR_Ca(v) v[1]
#define BR_d(v) v[2]
#define BR_f(v) v[3]
#define BR_h(v) v[4]
#define BR_j(v) v[5]
#define BR_m(v) v[6]
#define BR_x_1(v) v[7]


//w=(d,f,h,j,m,x_1)
//c=(Ca)
class MBeelerReuter : public MElphyModel {
  double initialPotential() const override { return -84.57; }
  double C_m = 1.0;
  double g_Na = 4.;
  double g_NaC = 0.003;
  double g_s = 0.09;
  double E_Na = 50.;
  double scaleIext = 1.0;

  std::array<double, 7> IxC{0.8, 0.04, 77., 1., 0.04, 35., 0.};

  std::array<std::array<double, 7>, 6> alphaC = {
      std::array<double, 7>{0.095, -0.01, -5., 0., 0., -0.072, 1.},
      std::array<double, 7>{0.012, -0.008, 28., 0., 0., 0.15, 1.},
      std::array<double, 7>{0.126, -0.25, 77, 0, 0, 0, 0},
      std::array<double, 7>{0.055, -0.25, 78, 0, 0, -0.2, 1},
      std::array<double, 7>{0, 0, 47, -1, 47, -0.1, -1},
      std::array<double, 7>{0.0005, 0.083, 50, 0, 0, 0.057, 1}
  };
  std::array<std::array<double, 7>, 6> betaC = {
      std::array<double, 7>{0.07, -0.017, 44, 0, 0, 0.05, 1},
      std::array<double, 7>{0.0065, -0.02, 30, 0, 0, -0.2, 1},
      std::array<double, 7>{1.7, 0, 22.5, 0, 0, -0.082, 1},
      std::array<double, 7>{0.3, 0, 32, 0, 0, -0.1, 1},
      std::array<double, 7>{40, -0.056, 72, 0, 0, 0, 0},
      std::array<double, 7>{0.0013, -0.06, 20, 0, 0, -0.04, 1}
  };


  [[nodiscard]] double alpha(int i, double V) const;

  [[nodiscard]] double beta(int i, double V) const;

  [[nodiscard]] double dAlpha(int i, double V) const;

  [[nodiscard]] double dBeta(int i, double V) const;

  [[nodiscard]] double dIiondV(const std::vector<double> &VW) const;

  [[nodiscard]] double dIion(int j, const std::vector<double> &VW) const;

  [[nodiscard]] double dGc(int j, const std::vector<double> &VW) const;

  [[nodiscard]] double dGw(int i, int j, const std::vector<double> &VW) const;

public:
  explicit MBeelerReuter() : MElphyModel(8, 1000.0, 1e6) {
    amplitude = 20;
    Config::Get("ScaleExcitationAmplitude", scaleIext);
  }

  explicit MBeelerReuter(double exc, double dur) : MBeelerReuter() {
    SetExternal(exc, dur, 20.0);
  }

  int CalciumIndex() const override { return 1; }

  int GatingIndex() const override { return 2; }

  void Initialize(Vectors &VW) override;

  void Initialize(std::vector<double> &vw) override;

  void IIonUpdate(const std::vector<double> &var, std::vector<double> &g,
                  double iExt) const override;

  void ConcentrationUpdate(const std::vector<double> &var,
                           std::vector<double> &g, double iExt) const override;

  void GatingUpdate(const std::vector<double> &var,
                    std::vector<double> &g) const override;

  void UpdateValues(double dt, std::vector<double> &var,
                    const std::vector<double> &g) const override;

  void ExpIntegratorUpdate(double dt, const std::vector<double> &var,
                           std::vector<double> &g) const override;

  std::vector<double> Tau(const std::vector<double> &var) const override;

  std::vector<double> YInfty(const std::vector<double> &var) const override;

  double GetIion(const std::vector<double> &var) const override;

  double JacobiEntry(int i, int j, const std::vector<double> &VW) const override;
};

#endif //MBEELERREUTER_HPP
