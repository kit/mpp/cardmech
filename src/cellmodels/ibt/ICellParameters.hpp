#ifndef ICELLPARAMETERS_H
#define ICELLPARAMETERS_H

#include <string>
#include <fstream>
#include "Config.hpp"
#include "Point.hpp"


static const int pFirst = 0;

class CellModelParameter {
public:
  std::string name;
  double value;
  bool readFromFile;
  int dynamicVar;

  CellModelParameter() : name(""), value(0.0), readFromFile(true), dynamicVar(-1) {};

  void read(std::istream &fp) {
    fp >> name >> value;
    while (!fp.eof()) // read over potential comment
      if (fp.get() == '\n')
        break;
  }
};

/*
 * Stores Parameters for Hodgkin-Huxley type equations. These values are usually computed once at the beginning
 * for an array of voltages so we don't have to calculate them during runtime.
 */
class HodgkinHuxleyParameters : public std::unordered_map<Point, double> {
public:
  double alpha(int V, double gating) const {
    return find(Point(V, gating, 0))->second;
  }

  double beta(int V, double gating) const {
    return find(Point(V, gating, 1))->second;
  }

  double tau(int V, double gating) const {
    return find(Point(V, gating, 2))->second;
  }

  double g_inf(int V, double gating) const {
    return find(Point(V, gating, 3))->second;
  }
};

class ICellParameters {
protected:
  bool isExternalCurrentSmooth = false;
  std::string numMethod = "ExplicitEuler";
public:
  ICellParameters() {
    Config::Get("NumericalMethodForElphy", numMethod);
    Config::Get("isExternalCurrentSmooth", isExternalCurrentSmooth);
  }

  virtual double Get(int) const = 0;

  bool isExtCurrentSmooth() {
    return isExternalCurrentSmooth;
  }

  std::string getNumMethod() {
    return numMethod;
  }
};

enum TenTusscherVar {
  TT_R = pFirst,
  TT_Tx,
  TT_F,
  TT_m_init,
  TT_h_init,
  TT_j_init,
  TT_xr1_init,
  TT_xr2_init,
  TT_xs_init,
  TT_r_init,
  TT_s_init,
  TT_d_init,
  TT_f_init,
  TT_f2_init,
  TT_fCa_init,
  TT_Rq_init,
  TT_O_init,
  TT_K_o,
  TT_Ca_o,
  TT_Na_o,
  TT_Vc,
  TT_Vsr,
  TT_Vss,
  TT_Vrel,
  TT_Vcell,
  TT_ks1,
  TT_ks2,
  TT_k3,
  TT_k4,
  TT_EC,
  TT_max_sr,
  TT_min_sr,
  TT_Vxfer,
  TT_Bufc,
  TT_Kbufc,
  TT_Bufsr,
  TT_Kbufsr,
  TT_Bufss,
  TT_Kbufss,
  TT_taug,
  TT_Vmaxup,
  TT_Kup,
  TT_C,
  TT_g_Kr,
  TT_pKNa,
  TT_g_Ks,
  TT_g_K1,
  TT_g_to,
  TT_g_Na,
  TT_g_bNa,
  TT_KmK,
  TT_KmNa,
  TT_knak,
  TT_g_CaL,
  TT_g_bCa,
  TT_kNaCa,
  TT_KmNai,
  TT_KmCa,
  TT_ksat,
  TT_n,
  TT_g_pCa,
  TT_KpCa,
  TT_g_pK,
  TT_V_init,
  TT_Cai_init,
  TT_CaSR_init,
  TT_CaSS_init,
  TT_Nai_init,
  TT_Ki_init,
  TT_lenght,
  TT_radius,
  TT_pi,
  TT_CaiMin,
  TT_CaiMax,
  TT_s_inf_vHalf,
  TT_tau_s_f1,
  TT_tau_s_slope1,
  TT_tau_s_vHalf1,
  TT_tau_s_enable,
  TT_vol,
  TT_vi,
  TT_inverseviF,
  TT_inverseviF2,
  TT_inversevssF2,
  TT_volforCall,
  TT_INVERSECAPACITANCE,
  TT_VcdVsr,
  TT_Kupsquare,
  TT_BufcPKbufc,
  TT_Kbufcsquare,
  TT_Kbufc2,
  TT_BufsrPKbufsr,
  TT_Kbufsrsquare,
  TT_Kbufsr2,
  TT_KmNai3,
  TT_Nao3,
  TT_StepCai,
  TT_inverseRTONF,
  TT_RTONF,
  TT_tau_s_add,
  TT_m_Xr1_1,
  TT_m_Xr1_2,
  TT_a_Xr1_1,
  TT_a_Xr1_2,
  TT_b_Xr1_1,
  TT_b_Xr1_2,
  TT_K_Q10Xr1,
  TT_m_Xr2_1,
  TT_m_Xr2_2,
  TT_a_Xr2_1,
  TT_a_Xr2_2,
  TT_b_Xr2_1,
  TT_b_Xr2_2,
  TT_K_Q10Xr2,
  TT_m_Xs_1,
  TT_m_Xs_2,
  TT_a_Xs_1,
  TT_a_Xs_2,
  TT_b_Xs_1,
  TT_b_Xs_2,
  TT_tau_Xs_add,
  TT_K_Q10Xs,
  TT_stim_duration,
  TT_Amp,
  TT_alpha3_div,
  TT_alpha5_div,
  TT_beta5_div,
  TT_alpha6,
  TT_beta6,
  TT_initMINALC3,
  TT_initMINALC2,
  TT_initMINALC1,
  TT_initMINALO,
  TT_initMINAUC3,
  TT_initMINAUC2,
  TT_initMINAUC1,
  TT_initMINAUO,
  TT_initMINAUIC3,
  TT_initMINAUIC2,
  TT_initMINAUIF,
  TT_initMINAUIM1,
  TT_initMINAUIM2,
  TT_last,
  TT_m,
  TT_h,
  TT_j,
  TT_xr1,
  TT_xr2,
  TT_xs,
  TT_s,
  TT_r,
  TT_d,
  TT_f,
  TT_f2,
};

class EMP_TenTusscher : public ICellParameters {
  const double minVTable;
  const double maxVTable;
  const double Vdt;

  double Cm;
public:
  CellModelParameter *P[TT_last]{};
  HodgkinHuxleyParameters HHP;

  EMP_TenTusscher() : minVTable(-100.0), maxVTable(100.0), Vdt(0.001), Cm(0.01) {

    Config::Get("MembranCapacitance", Cm);
    Cm = Cm / 1000000.0;

    for (auto &i : P) {
      i = new CellModelParameter();
    }

    // === Assign all Names ===
    P[TT_R]->name = "R";
    P[TT_Tx]->name = "Tx";
    P[TT_F]->name = "F";
    P[TT_m_init]->name = "init_m";
    P[TT_h_init]->name = "init_h";
    P[TT_j_init]->name = "init_j";
    P[TT_xr1_init]->name = "init_Xr1";
    P[TT_xr2_init]->name = "init_Xr2";
    P[TT_xs_init]->name = "init_Xs";
    P[TT_r_init]->name = "init_r";
    P[TT_s_init]->name = "init_s";
    P[TT_d_init]->name = "init_d";
    P[TT_f_init]->name = "init_f";
    P[TT_f2_init]->name = "init_f2";
    P[TT_fCa_init]->name = "init_fCa";
    P[TT_Rq_init]->name = "init_Rq";
    P[TT_O_init]->name = "init_O";
    P[TT_K_o]->name = "K_o";
    P[TT_Ca_o]->name = "Ca_o";
    P[TT_Na_o]->name = "Na_o";
    P[TT_Vc]->name = "Vc";
    P[TT_Vsr]->name = "Vsr";
    P[TT_Vss]->name = "Vss";
    P[TT_Vrel]->name = "Vrel";
    P[TT_Vcell]->name = "Vcell";
    P[TT_ks1]->name = "ks1";
    P[TT_ks2]->name = "ks2";
    P[TT_k3]->name = "k3";
    P[TT_k4]->name = "k4";
    P[TT_EC]->name = "EC";
    P[TT_max_sr]->name = "max_sr";
    P[TT_min_sr]->name = "min_sr";
    P[TT_Vxfer]->name = "Vxfer";
    P[TT_Bufc]->name = "Bufc";
    P[TT_Kbufc]->name = "Kbufc";
    P[TT_Bufsr]->name = "Bufsr";
    P[TT_Kbufsr]->name = "Kbufsr";
    P[TT_Bufss]->name = "Bufss";
    P[TT_Kbufss]->name = "Kbufss";
    P[TT_taug]->name = "taug";
    P[TT_Vmaxup]->name = "Vmaxup";
    P[TT_Kup]->name = "Kup";
    P[TT_C]->name = "C";
    P[TT_g_Kr]->name = "g_Kr";
    P[TT_pKNa]->name = "pKNa";
    P[TT_g_Ks]->name = "g_Ks";
    P[TT_g_K1]->name = "g_K1";
    P[TT_g_to]->name = "g_to";
    P[TT_g_Na]->name = "g_Na";
    P[TT_g_bNa]->name = "g_bNa";
    P[TT_KmK]->name = "KmK";
    P[TT_KmNa]->name = "KmNa";
    P[TT_knak]->name = "knak";
    P[TT_g_CaL]->name = "g_CaL";
    P[TT_g_bCa]->name = "g_bCa";
    P[TT_kNaCa]->name = "kNaCa";
    P[TT_KmNai]->name = "KmNai";
    P[TT_KmCa]->name = "KmCa";
    P[TT_ksat]->name = "ksat";
    P[TT_n]->name = "n";
    P[TT_g_pCa]->name = "g_pCa";
    P[TT_KpCa]->name = "KpCa";
    P[TT_g_pK]->name = "g_pK";
    P[TT_V_init]->name = "init_V_m";
    P[TT_Cai_init]->name = "init_Cai";
    P[TT_CaSR_init]->name = "init_CaSR";
    P[TT_CaSS_init]->name = "init_CaSS";
    P[TT_Nai_init]->name = "init_Nai";
    P[TT_Ki_init]->name = "init_Ki";
    P[TT_lenght]->name = "lenght";
    P[TT_radius]->name = "radius";
    P[TT_pi]->name = "pi";
    P[TT_CaiMin]->name = "CaiMin";
    P[TT_CaiMax]->name = "CaiMax";
    P[TT_s_inf_vHalf]->name = "s_inf_vHalf";
    P[TT_tau_s_f1]->name = "tau_s_f1";
    P[TT_tau_s_slope1]->name = "tau_s_slope1";
    P[TT_tau_s_vHalf1]->name = "tau_s_vHalf1";
    P[TT_tau_s_enable]->name = "tau_s_enable";
    P[TT_tau_s_add]->name = "tau_s_add";
    P[TT_m_Xr1_1]->name = "m_Xr1_1";
    P[TT_m_Xr1_2]->name = "m_Xr1_2";
    P[TT_a_Xr1_1]->name = "a_Xr1_1";
    P[TT_a_Xr1_2]->name = "a_Xr1_2";
    P[TT_b_Xr1_1]->name = "b_Xr1_1";
    P[TT_b_Xr1_2]->name = "b_Xr1_2";
    P[TT_K_Q10Xr1]->name = "K_Q10Xr1";
    P[TT_m_Xr2_1]->name = "m_Xr2_1";
    P[TT_m_Xr2_2]->name = "m_Xr2_2";
    P[TT_a_Xr2_1]->name = "a_Xr2_1";
    P[TT_a_Xr2_2]->name = "a_Xr2_2";
    P[TT_b_Xr2_1]->name = "b_Xr2_1";
    P[TT_b_Xr2_2]->name = "b_Xr2_2";
    P[TT_K_Q10Xr2]->name = "K_Q10Xr2";
    P[TT_m_Xs_1]->name = "m_Xs_1";
    P[TT_m_Xs_2]->name = "m_Xs_2";
    P[TT_a_Xs_1]->name = "a_Xs_1";
    P[TT_a_Xs_2]->name = "a_Xs_2";
    P[TT_b_Xs_1]->name = "b_Xs_1";
    P[TT_b_Xs_2]->name = "b_Xs_2";
    P[TT_tau_Xs_add]->name = "tau_Xs_add";
    P[TT_K_Q10Xs]->name = "K_Q10Xs";
    P[TT_vol]->name = "vol";
    P[TT_vi]->name = "vi";
    P[TT_inverseviF]->name = "inverseviF";
    P[TT_inverseviF2]->name = "inverseviF2";
    P[TT_inversevssF2]->name = "inversevssF2";
    P[TT_volforCall]->name = "volforCall";
    P[TT_INVERSECAPACITANCE]->name = "INVERSECAPACITANCE";
    P[TT_VcdVsr]->name = "VcdVsr";
    P[TT_Kupsquare]->name = "Kupsquare";
    P[TT_BufcPKbufc]->name = "BufcPKbufc";
    P[TT_Kbufcsquare]->name = "Kbufcsquare";
    P[TT_Kbufc2]->name = "Kbufc2";
    P[TT_BufsrPKbufsr]->name = "BufsrPKbufsr";
    P[TT_Kbufsrsquare]->name = "Kbufsrsquare";
    P[TT_Kbufsr2]->name = "Kbufsr2";
    P[TT_KmNai3]->name = "KmNai3";
    P[TT_Nao3]->name = "Nao3";
    P[TT_StepCai]->name = "StepCai";
    P[TT_inverseRTONF]->name = "inverseRTONF";
    P[TT_RTONF]->name = "RTONF";
    P[TT_stim_duration]->name = "stim_duration";
    P[TT_Amp]->name = "Amp";
    P[TT_alpha3_div]->name = "alpha3_div";
    P[TT_alpha5_div]->name = "alpha5_div";
    P[TT_beta5_div]->name = "beta5_div";
    P[TT_alpha6]->name = "alpha6";
    P[TT_beta6]->name = "beta6";
    P[TT_initMINALC3]->name = "initMINALC3";
    P[TT_initMINALC2]->name = "initMINALC2";
    P[TT_initMINALC1]->name = "initMINALC1";
    P[TT_initMINALO]->name = "initMINALO";
    P[TT_initMINAUC3]->name = "initMINAUC3";
    P[TT_initMINAUC2]->name = "initMINAUC2";
    P[TT_initMINAUC1]->name = "initMINAUC1";
    P[TT_initMINAUO]->name = "initMINAUO";
    P[TT_initMINAUIC3]->name = "initMINAUIC3";
    P[TT_initMINAUIC2]->name = "initMINAUIC2";
    P[TT_initMINAUIF]->name = "initMINAUIF";
    P[TT_initMINAUIM1]->name = "initMINAUIM1";
    P[TT_initMINAUIM2]->name = "initMINAUIM2";

    // === Check File Readability
    P[TT_RTONF]->readFromFile = false;
    P[TT_inverseRTONF]->readFromFile = false;
    P[TT_vol]->readFromFile = false;
    P[TT_vi]->readFromFile = false;
    P[TT_vi]->readFromFile = false;
    P[TT_inverseviF]->readFromFile = false;
    P[TT_inverseviF2]->readFromFile = false;
    P[TT_inversevssF2]->readFromFile = false;
    P[TT_volforCall]->readFromFile = false;
    P[TT_INVERSECAPACITANCE]->readFromFile = false;
    P[TT_VcdVsr]->readFromFile = false;
    P[TT_Kupsquare]->readFromFile = false;
    P[TT_BufcPKbufc]->readFromFile = false;
    P[TT_Kbufcsquare]->readFromFile = false;
    P[TT_Kbufc2]->readFromFile = false;
    P[TT_BufsrPKbufsr]->readFromFile = false;
    P[TT_Kbufsrsquare]->readFromFile = false;
    P[TT_Kbufsr2]->readFromFile = false;
    P[TT_KmNai3]->readFromFile = false;
    P[TT_Nao3]->readFromFile = false;
    P[TT_StepCai]->readFromFile = false;
    P[TT_pi]->readFromFile = false;

    // === Load Values from File
    std::string paramfilename;
    Config::Get("ElphyParameters", paramfilename);
    std::string paramnames[256];
    double paramvals[256];

    std::ifstream paramfile(paramfilename);

    int paramN = 0;
    while (!paramfile.eof()) {
      paramfile >> paramnames[paramN];
      paramfile >> paramvals[paramN++];
    }
    paramN -= 1;

    for (auto &x : P)
      if (x->readFromFile) {
        std::string Pname = x->name;
        for (int n = 0; n < paramN; n++) {
          if (Pname == paramnames[n])
            x->value = paramvals[n];
        }
      }

    InitializeValues();
    InitializeGatingParameters();

    //for ( int Pntr = 0; Pntr < vtLast; Pntr++ )
    //    cout << "  " << Pntr+1 << "  " << P[Pntr]->name << ": "<< P[Pntr]->value << endl;
  };

  void InitializeValues() {
    P[TT_pi]->value = 3.141592;
    P[TT_RTONF]->value = (P[TT_R]->value) * (P[TT_Tx]->value) / (P[TT_F]->value);
    P[TT_inverseRTONF]->value = 1 / (P[TT_RTONF]->value);
    P[TT_vol]->value =
        (P[TT_pi]->value) * (P[TT_lenght]->value) * (P[TT_radius]->value) * (P[TT_radius]->value);
    //P[TT_vi]->value                 = 0.49*(P[TT_vol]->value);
    P[TT_vi]->value = 0.016404;
    P[TT_inverseviF]->value = 1 / ((P[TT_vi]->value) * (P[TT_F]->value));
    P[TT_inverseviF2]->value = 1. / (2 * (P[TT_vi]->value) * (P[TT_F]->value));
    P[TT_inversevssF2]->value = 1 / (2 * ((P[TT_vi]->value) / 300.) * (P[TT_F]->value));
    P[TT_volforCall]->value = P[TT_vol]->value / 1000000000;
    P[TT_INVERSECAPACITANCE]->value = 1. / (P[TT_C]->value);
    P[TT_VcdVsr]->value = (P[TT_Vc]->value) / (P[TT_Vsr]->value);
    P[TT_Kupsquare]->value = (P[TT_Kup]->value) * (P[TT_Kup]->value);
    P[TT_BufcPKbufc]->value = (P[TT_Bufc]->value) + (P[TT_Kbufc]->value);
    P[TT_Kbufcsquare]->value = (P[TT_Kbufc]->value) * (P[TT_Kbufc]->value);
    P[TT_Kbufc2]->value = 2 * (P[TT_Kbufc]->value);
    P[TT_BufsrPKbufsr]->value = (P[TT_Bufsr]->value) + (P[TT_Kbufsr]->value);
    P[TT_Kbufsrsquare]->value = (P[TT_Kbufsr]->value) * (P[TT_Kbufsr]->value);
    P[TT_Kbufsr2]->value = 2 * (P[TT_Kbufsr]->value);
    P[TT_KmNai3]->value = (P[TT_KmNai]->value) * (P[TT_KmNai]->value) * (P[TT_KmNai]->value);
    P[TT_Nao3]->value = (P[TT_Na_o]->value) * (P[TT_Na_o]->value) * (P[TT_Na_o]->value);
    P[TT_C]->value = Cm;
    //P[TT_StepCai]->value            = (P[TT_CaiMax]->value-P[TT_CaiMin]->value)/RTDT;
  }

  void InitializeGatingParameters() {
    /*double Vd = minVTable;
    while(Vd < maxVTable){
        double m_inf = 1./((1.+exp((-56.86-Vd)/9.03))*(1.+exp((-56.86-Vd)/9.03)));
        const double a_m        = 1./(1.+exp((-60.-Vd)/5.));
        const double b_m        = 0.1/(1.+exp((Vd+35.)/5.))+0.10/(1.+exp((Vd-50.)/200.));
        const double tau_m = a_m*b_m;

        double tau_h, tau_j;
        double h_inf = 1./((1.+exp((Vd+71.55)/7.43))*(1.+exp((Vd+71.55)/7.43)));
        if (Vd >= -40.) {
            const double AH_1 = 0.;
            const double BH_1 = (0.77/(0.13*(1.+exp(-(Vd+10.66)/11.1))));
            tau_h = 1.0/(AH_1+BH_1);
        } else {
            const double AH_2 = (0.057*exp(-(Vd+80.)/6.8));
            const double BH_2 = (2.7*exp(0.079*Vd)+(3.1e5)*exp(0.3485*Vd));
            tau_h = 1.0/(AH_2+BH_2);
        }

        double j_inf   = h_inf;
        if (Vd >= -40.) {
            const double AJ_1 = 0.;
            const double BJ_1 = (0.6*exp((0.057)*Vd)/(1.+exp(-0.1*(Vd+32.))));
            tau_j = 1.0/(AJ_1+BJ_1);
        } else {
            const double AJ_2 = (((-2.5428e4)*exp(0.2444*Vd)-(6.948e-6)*exp(-0.04391*Vd))*(Vd+37.78)/(1.+exp(0.311*(Vd+79.23))));
            const double BJ_2 = (0.02424*exp(-0.01052*Vd)/(1.+exp(-0.1378*(Vd+40.14))));
            tau_j = 1.0/(AJ_2+BJ_2);
        }

        double xr1_inf = 1./(1.+exp((P[TT_m_Xr1_1]->value-Vd)/(P[TT_m_Xr1_2]->value)));
        const double a_Xr1   = 450./(1.+exp((P[TT_a_Xr1_1]->value-Vd)/(P[TT_a_Xr1_2]->value)));
        const double b_Xr1   = 6./(1.+exp((Vd-(P[TT_b_Xr1_1]->value))/(P[TT_b_Xr1_2]->value)));
        const double tau_Xr1 = a_Xr1*b_Xr1*fabs(P[TT_K_Q10Xr1]->value);

        double xr2_inf = 1./(1.+exp((Vd-(P[TT_m_Xr2_1]->value))/(P[TT_m_Xr2_2]->value)));
        const double a_Xr2   = 3./(1.+exp((P[TT_a_Xr2_1]->value-Vd)/(P[TT_a_Xr2_2]->value)));
        const double b_Xr2   = 1.12/(1.+exp((Vd-(P[TT_b_Xr2_1]->value))/(P[TT_b_Xr2_2]->value)));
        const double tau_Xr2 = a_Xr2*b_Xr2*fabs(P[TT_K_Q10Xr2]->value);

        double xs_inf = 1./(1.+exp((P[TT_m_Xs_1]->value-Vd)/P[TT_m_Xs_2]->value));
        const double a_Xs   = (1400./(sqrt(1.+exp((P[TT_a_Xs_1]->value-Vd)/P[TT_a_Xs_2]->value))));
        const double b_Xs   = (1./(1.+exp((Vd-P[TT_b_Xs_1]->value)/P[TT_b_Xs_2]->value)));
        const double tau_Xs = (a_Xs*b_Xs+P[TT_tau_Xs_add]->value)*fabs(P[TT_K_Q10Xs]->value);


        double s_inf = 1./(1.+exp((Vd+(P[TT_s_inf_vHalf]->value))/5.));     // =1./(1.+exp((Vd+20)/5.));  //58
        const double tau_s = P[TT_tau_s_f1]->value*
                             exp(-(Vd+(P[TT_tau_s_vHalf1]->value))*(Vd+(P[TT_tau_s_vHalf1]->value))/P[TT_tau_s_slope1]->value)+
                             P[TT_tau_s_enable]->value*5./(1.+exp((Vd-20.)/5.))+P[TT_tau_s_add]->value;

        double r_inf = 1./(1.+exp((20-Vd)/6.));                            // 56
        const double tau_r = 9.5*exp(-(Vd+40.)*(Vd+40.)/1800.)+0.8; // 57

        double d_inf = 1./(1.+exp((-8.-Vd)/7.5));  // 41 n7
        const double a_d   = 1.4/(1.+exp((-35-Vd)/13))+0.25;
        const double b_d   = 1.4/(1.+exp((Vd+5)/5));
        const double c_d   = 1./(1.+exp((50-Vd)/20));
        const double tau_d = a_d*b_d+c_d;

        double f_inf = 1./(1.+exp((Vd+20)/7));  // 46 n12
        const double Vi27square = (Vd+27)*(Vd+27);                   // new
        const double exsquare  = Vi27square/(15*15);               // new
        const double a_f       = 1102.5*exp(-exsquare);           // new n13
        const double b_f       = 200./(1.+exp((13-Vd)/10.));       // new n14
        const double g_f       = (180./(1.+exp((Vd+30)/10.)))+20.; // new n15
        const double tau_f     = a_f+b_f+g_f;                     // new n16

        double f2_inf= (.67/(1.+exp((Vd+35.)/7.)))+.33;                   // new n17
        const double a_f2   = 600.*exp(-(Vd+25)*(Vd+25)/170.);      // new n18
        const double b_f2   = 31./(1.+exp((25-Vd)/10.));           // new n19
        const double c_f2   = 16./(1.+exp((Vd+30)/10.));           // new n20
        const double tau_f2 = a_f2+b_f2+c_f2;                     // new n21

        int Vi = (int) (Vd/Vdt + min_EPS);
        // Add alpha/beta to parameter table
        SetHHP(Vi, TT_m, tau_m, m_inf);
        SetHHP(Vi, TT_h, tau_h, h_inf);
        SetHHP(Vi, TT_j, tau_j, j_inf);
        SetHHP(Vi, TT_xr1, tau_Xr1, xr1_inf);
        SetHHP(Vi, TT_xr2, tau_Xr2, xr2_inf);
        SetHHP(Vi, TT_xs, tau_Xs, xs_inf);
        SetHHP(Vi, TT_s, tau_s, s_inf);
        SetHHP(Vi, TT_r, tau_r, r_inf);
        SetHHP(Vi, TT_d, tau_d, d_inf);
        SetHHP(Vi, TT_f, tau_f, f_inf);
        SetHHP(Vi, TT_f2, tau_f2, f2_inf);

        Vd += Vdt;
    }*/

  }

  ~EMP_TenTusscher() {};

  double Get(int pos) const override { return P[pos]->value; };

  double AlphaHH(double tau_g, double g_inf) { return g_inf / tau_g; }

  double BetaHH(double tau_g, double g_inf) { return (1 - g_inf) / tau_g; }

  void SetHHP(double Vd, int gating, double tau_g, double g_inf) {
    int Vi = (int) (Vd / Vdt + 1e-8);
    HHP[Point(Vi, gating, 0)] = AlphaHH(tau_g, g_inf);
    HHP[Point(Vi, gating, 1)] = BetaHH(tau_g, g_inf);
    HHP[Point(Vi, gating, 2)] = tau_g;
    HHP[Point(Vi, gating, 3)] = g_inf;
  }

  double GetHHP(double Vd, int gating, int i) const {
    int Vi = (int) (Vd / Vdt + 1e-8);
    switch (i) {
      case 0:
        return HHP.alpha(Vi, gating);
      case 1:
        return HHP.beta(Vi, gating);
      case 2:
        return HHP.tau(Vi, gating);
      case 3:
        return HHP.g_inf(Vi, gating);
      default:
        return 0.0;
    }
  }
};

enum BeelerReuterVar {
  BR_C_m = pFirst,
  BR_g_Na,
  BR_g_NaC,
  BR_E_Na,
  BR_g_s,
  BR_Vol,
  BR_Amp,
  BR_dC_m,
  BR_Init_Ca_i,
  BR_Init_m,
  BR_Init_h,
  BR_Init_j,
  BR_Init_d,
  BR_Init_f,
  BR_Init_x1,
  BR_Init_Vm,
  BR_RC_ax1C1,
  BR_RC_ax1C2,
  BR_RC_ax1C3,
  BR_RC_ax1C4,
  BR_RC_ax1C5,
  BR_RC_ax1C6,
  BR_RC_ax1C7,
  BR_RC_bx1C1,
  BR_RC_bx1C2,
  BR_RC_bx1C3,
  BR_RC_bx1C4,
  BR_RC_bx1C5,
  BR_RC_bx1C6,
  BR_RC_bx1C7,
  BR_RC_amC1,
  BR_RC_amC2,
  BR_RC_amC3,
  BR_RC_amC4,
  BR_RC_amC5,
  BR_RC_amC6,
  BR_RC_amC7,
  BR_RC_bmC1,
  BR_RC_bmC2,
  BR_RC_bmC3,
  BR_RC_bmC4,
  BR_RC_bmC5,
  BR_RC_bmC6,
  BR_RC_bmC7,
  BR_RC_ahC1,
  BR_RC_ahC2,
  BR_RC_ahC3,
  BR_RC_ahC4,
  BR_RC_ahC5,
  BR_RC_ahC6,
  BR_RC_ahC7,
  BR_RC_bhC1,
  BR_RC_bhC2,
  BR_RC_bhC3,
  BR_RC_bhC4,
  BR_RC_bhC5,
  BR_RC_bhC6,
  BR_RC_bhC7,
  BR_RC_ajC1,
  BR_RC_ajC2,
  BR_RC_ajC3,
  BR_RC_ajC4,
  BR_RC_ajC5,
  BR_RC_ajC6,
  BR_RC_ajC7,
  BR_RC_bjC1,
  BR_RC_bjC2,
  BR_RC_bjC3,
  BR_RC_bjC4,
  BR_RC_bjC5,
  BR_RC_bjC6,
  BR_RC_bjC7,
  BR_RC_adC1,
  BR_RC_adC2,
  BR_RC_adC3,
  BR_RC_adC4,
  BR_RC_adC5,
  BR_RC_adC6,
  BR_RC_adC7,
  BR_RC_bdC1,
  BR_RC_bdC2,
  BR_RC_bdC3,
  BR_RC_bdC4,
  BR_RC_bdC5,
  BR_RC_bdC6,
  BR_RC_bdC7,
  BR_RC_afC1,
  BR_RC_afC2,
  BR_RC_afC3,
  BR_RC_afC4,
  BR_RC_afC5,
  BR_RC_afC6,
  BR_RC_afC7,
  BR_RC_bfC1,
  BR_RC_bfC2,
  BR_RC_bfC3,
  BR_RC_bfC4,
  BR_RC_bfC5,
  BR_RC_bfC6,
  BR_RC_bfC7,
  BR_RC_ix1C1,
  BR_RC_ix1C2,
  BR_RC_ix1C3,
  BR_RC_ix1C4,
  BR_RC_ix1C5,
  BR_RC_ix1C6,
  BR_RC_ix1C7,
  BR_last
};

class EMP_BeelerReuter : public ICellParameters {
  double minVTable = -200.0;
  double maxVTable = 200.0;
  double Vdt = 0.001;

public:
  CellModelParameter P[BR_last];
  HodgkinHuxleyParameters HHP;

  EMP_BeelerReuter() {
    for (auto &i : P) {
      i = CellModelParameter();
    }

    // === Assign all Names ===
    P[BR_C_m].name = "C_m";
    P[BR_g_Na].name = "g_Na";
    P[BR_g_NaC].name = "g_NaC";
    P[BR_E_Na].name = "E_Na";
    P[BR_g_s].name = "g_s";
    P[BR_Vol].name = "Vol";
    P[BR_Amp].name = "Amp";
    P[BR_dC_m].name = "dC_m";
    P[BR_Init_Ca_i].name = "init_Ca_i";
    P[BR_Init_m].name = "init_m";
    P[BR_Init_h].name = "init_h";
    P[BR_Init_j].name = "init_j";
    P[BR_Init_d].name = "init_d";
    P[BR_Init_f].name = "init_f";
    P[BR_Init_x1].name = "init_x1";
    P[BR_Init_Vm].name = "init_Vm";
    P[BR_RC_ax1C1].name = "RC_ax1C1";
    P[BR_RC_ax1C2].name = "RC_ax1C2";
    P[BR_RC_ax1C3].name = "RC_ax1C3";
    P[BR_RC_ax1C4].name = "RC_ax1C4";
    P[BR_RC_ax1C5].name = "RC_ax1C5";
    P[BR_RC_ax1C6].name = "RC_ax1C6";
    P[BR_RC_ax1C7].name = "RC_ax1C7";
    P[BR_RC_bx1C1].name = "RC_bx1C1";
    P[BR_RC_bx1C2].name = "RC_bx1C2";
    P[BR_RC_bx1C3].name = "RC_bx1C3";
    P[BR_RC_bx1C4].name = "RC_bx1C4";
    P[BR_RC_bx1C5].name = "RC_bx1C5";
    P[BR_RC_bx1C6].name = "RC_bx1C6";
    P[BR_RC_bx1C7].name = "RC_bx1C7";
    P[BR_RC_amC1].name = "RC_amC1";
    P[BR_RC_amC2].name = "RC_amC2";
    P[BR_RC_amC3].name = "RC_amC3";
    P[BR_RC_amC4].name = "RC_amC4";
    P[BR_RC_amC5].name = "RC_amC5";
    P[BR_RC_amC6].name = "RC_amC6";
    P[BR_RC_amC7].name = "RC_amC7";
    P[BR_RC_bmC1].name = "RC_bmC1";
    P[BR_RC_bmC2].name = "RC_bmC2";
    P[BR_RC_bmC3].name = "RC_bmC3";
    P[BR_RC_bmC4].name = "RC_bmC4";
    P[BR_RC_bmC5].name = "RC_bmC5";
    P[BR_RC_bmC6].name = "RC_bmC6";
    P[BR_RC_bmC7].name = "RC_bmC7";
    P[BR_RC_ahC1].name = "RC_ahC1";
    P[BR_RC_ahC2].name = "RC_ahC2";
    P[BR_RC_ahC3].name = "RC_ahC3";
    P[BR_RC_ahC4].name = "RC_ahC4";
    P[BR_RC_ahC5].name = "RC_ahC5";
    P[BR_RC_ahC6].name = "RC_ahC6";
    P[BR_RC_ahC7].name = "RC_ahC7";
    P[BR_RC_bhC1].name = "RC_bhC1";
    P[BR_RC_bhC2].name = "RC_bhC2";
    P[BR_RC_bhC3].name = "RC_bhC3";
    P[BR_RC_bhC4].name = "RC_bhC4";
    P[BR_RC_bhC5].name = "RC_bhC5";
    P[BR_RC_bhC6].name = "RC_bhC6";
    P[BR_RC_bhC7].name = "RC_bhC7";
    P[BR_RC_ajC1].name = "RC_ajC1";
    P[BR_RC_ajC2].name = "RC_ajC2";
    P[BR_RC_ajC3].name = "RC_ajC3";
    P[BR_RC_ajC4].name = "RC_ajC4";
    P[BR_RC_ajC5].name = "RC_ajC5";
    P[BR_RC_ajC6].name = "RC_ajC6";
    P[BR_RC_ajC7].name = "RC_ajC7";
    P[BR_RC_bjC1].name = "RC_bjC1";
    P[BR_RC_bjC2].name = "RC_bjC2";
    P[BR_RC_bjC3].name = "RC_bjC3";
    P[BR_RC_bjC4].name = "RC_bjC4";
    P[BR_RC_bjC5].name = "RC_bjC5";
    P[BR_RC_bjC6].name = "RC_bjC6";
    P[BR_RC_bjC7].name = "RC_bjC7";
    P[BR_RC_adC1].name = "RC_adC1";
    P[BR_RC_adC2].name = "RC_adC2";
    P[BR_RC_adC3].name = "RC_adC3";
    P[BR_RC_adC4].name = "RC_adC4";
    P[BR_RC_adC5].name = "RC_adC5";
    P[BR_RC_adC6].name = "RC_adC6";
    P[BR_RC_adC7].name = "RC_adC7";
    P[BR_RC_bdC1].name = "RC_bdC1";
    P[BR_RC_bdC2].name = "RC_bdC2";
    P[BR_RC_bdC3].name = "RC_bdC3";
    P[BR_RC_bdC4].name = "RC_bdC4";
    P[BR_RC_bdC5].name = "RC_bdC5";
    P[BR_RC_bdC6].name = "RC_bdC6";
    P[BR_RC_bdC7].name = "RC_bdC7";
    P[BR_RC_afC1].name = "RC_afC1";
    P[BR_RC_afC2].name = "RC_afC2";
    P[BR_RC_afC3].name = "RC_afC3";
    P[BR_RC_afC4].name = "RC_afC4";
    P[BR_RC_afC5].name = "RC_afC5";
    P[BR_RC_afC6].name = "RC_afC6";
    P[BR_RC_afC7].name = "RC_afC7";
    P[BR_RC_bfC1].name = "RC_bfC1";
    P[BR_RC_bfC2].name = "RC_bfC2";
    P[BR_RC_bfC3].name = "RC_bfC3";
    P[BR_RC_bfC4].name = "RC_bfC4";
    P[BR_RC_bfC5].name = "RC_bfC5";
    P[BR_RC_bfC6].name = "RC_bfC6";
    P[BR_RC_bfC7].name = "RC_bfC7";
    P[BR_RC_ix1C1].name = "RC_ix1C1";
    P[BR_RC_ix1C2].name = "RC_ix1C2";
    P[BR_RC_ix1C3].name = "RC_ix1C3";
    P[BR_RC_ix1C4].name = "RC_ix1C4";
    P[BR_RC_ix1C5].name = "RC_ix1C5";
    P[BR_RC_ix1C6].name = "RC_ix1C6";
    P[BR_RC_ix1C7].name = "RC_ix1C7";

    P[BR_dC_m].readFromFile = false;

    // === Load Values from File
    std::string paramfilename{};
    Config::Get("ElphyParameters", paramfilename);
    std::string paramnames[256];
    double paramvals[256];

    std::ifstream paramfile(paramfilename.c_str());

    int paramN = 0;
    while (!paramfile.eof()) {
      paramfile >> paramnames[paramN];
      paramfile >> paramvals[paramN++];
    }
    paramN -= 1;

    for (auto &x : P)
      if (x.readFromFile) {
        std::string Pname = x.name;
        for (int n = 0; n < paramN; n++) {
          if (Pname == paramnames[n])
            x.value = paramvals[n];
        }
      }
  };

  void InitializeValues() {
    P[BR_dC_m].value = .001 / (P[BR_C_m].value);
  };

  ~EMP_BeelerReuter() = default;;

  double Get(int pos) const override { return P[pos].value; };

  double AlphaHH(double tau_g, double g_inf) { return g_inf / tau_g; }

  double BetaHH(double tau_g, double g_inf) { return (1 - g_inf) / tau_g; }

  void SetHHP(double Vd, int gating, double tau_g, double g_inf) {
    int Vi = (int) (Vd / Vdt + 1e-8);
    HHP[Point(Vi, gating, 0)] = AlphaHH(tau_g, g_inf);
    HHP[Point(Vi, gating, 1)] = BetaHH(tau_g, g_inf);
    HHP[Point(Vi, gating, 2)] = exp(-Vdt / tau_g);
    HHP[Point(Vi, gating, 3)] = g_inf;
  }

  double GetHHP(double Vd, int gating, int i) const {
    int Vi = (int) (Vd / Vdt + 1e-8);
    switch (i) {
      case 0:
        return HHP.alpha(Vi, gating);
      case 1:
        return HHP.beta(Vi, gating);
      case 2:
        return HHP.tau(Vi, gating);
      case 3:
        return HHP.g_inf(Vi, gating);
      default:
        return 0.0;
    }
  }
};

#include "ElphyModelBasis.h"
#include "ForceModelBasis.h"
#include "CellModelLayer.h"


class EMP_IBT : public ICellParameters {
  ElphyModelType CMVentricle;
  vbElphyParameters<double> *ElphyParameters = NULL;

  std::string parameterfilename;
public:
  EMP_IBT(int chamber = -1) {
    std::string nameCellModelType = "ElphyDummy";
    switch (chamber) {
      case -1:
        Config::Get("IBTElphyModel", nameCellModelType);
        break;
      case 0:
        Config::Get("IBTVentricleModel", nameCellModelType);
        break;
      case 1:
        Config::Get("IBTAtriaModel", nameCellModelType);
        break;
    }
    CMVentricle = FindElphyModelType(nameCellModelType);

    parameterfilename = FindElphyModelFileName(CMVentricle);
    switch (chamber) {
      case -1:
        Config::Get("IBTElphyParameters", parameterfilename);
        break;
      case 0:
        Config::Get("IBTVentricleParameters", parameterfilename);
        break;
      case 1:
        Config::Get("IBTAtriaParameters", parameterfilename);
        break;
    }
  };

  void initParameters(double min_dt) {
    initElphyParameters<double>(&ElphyParameters, (std::string(ProjectSourceDir) + "/" + parameterfilename).c_str(), CMVentricle, min_dt);
  }

  ElphyModelType &getModel() { return CMVentricle; };

  vbElphyParameters<double> *getParameters() { return ElphyParameters; };

  double Get(int i) const {
    return i;
  }

  ~EMP_IBT() {
    delete ElphyParameters;
  };
};

class TMP_IBT : public ICellParameters {
  ForceModelType forceModel;
  vbForceParameters<double> *forceParameters = nullptr;

  std::string parameterfilename;

public:
  double scaling = 0.0115;

  TMP_IBT() {
    Config::Get("MechScaling", scaling);

    std::string tmName = "ForceDummy";
    Config::Get("IBTTensionModel", tmName);
    forceModel = FindForceModelType(tmName);

    Config::Get("IBTTensionParameters", parameterfilename);
    initForceParameters<double>(&forceParameters, parameterfilename.c_str(), forceModel);
  }

  ForceModelType &GetModel() { return forceModel; };

  vbForceParameters<double> *GetParameters() { return forceParameters; };

  double Get(int i) const {
    return i;
  }

  ~TMP_IBT() {
    delete forceParameters;
  };
};

#endif //ICELLPARAMETERS_H
