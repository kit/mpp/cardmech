#ifndef M_EM_IBT_H
#define M_EM_IBT_H

#include "ICellModel.hpp"
#include "ICellParameters.hpp"


class ElphyIBT : public IElphyModel {
  EMP_IBT &ibtP;
  vbElphyModel<double> *ElphyModel = nullptr;

public:
  ElphyIBT(EMP_IBT &ibtP_, double e_, double d_, double a_);

  int gatingDim() const { return 1; };

  double getPotential() const override {
    return ElphyModel->GetVm() * 1000.;
  };

  double getGating() const override { return ElphyModel->GetCai(); };

  double updateStep(int, double, double) override;

  double updateModel(int, double, double) override;

  double updateHodgkinHuxley(double, double, double, double, double);

  double updateConcentration(double, double, double);

  double getExternalCurrent() const;
};

#endif //M_EM_IBT_H
