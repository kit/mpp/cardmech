#include "ICellModel.hpp"


double ICellModel::update(double dt, double var) {
  switch (RK_Order) {
    case 1:
      return update_EE(dt, var);
    case 2:
      return update_RK2(dt, var);
    case 4:
      return update_RK4(dt, var);
    default:
      return var;
  }
}

double ICellModel::update_EE(double dt, double var) {
  updateStep(0, 0.0, var);

  return updateModel(1, dt, var);
}

double ICellModel::update_RK2(double dt, double var) {
  double oVar(var);
  double dVar = updateStep(0, 0.0, var);
  updateStep(1, dt / 2, var + dt / 2 * dVar);

  return updateModel(2, dt, oVar);
}

double ICellModel::update_RK4(double dt, double var) {
  double oVar(var);

  double dVar = updateStep(0, 0.0, var);
  dVar = updateStep(1, dt / 2., var + dt / 2 * dVar);
  dVar = updateStep(2, dt / 2., var + dt / 2 * dVar);
  updateStep(3, dt, var + dt * dVar);

  return updateModel(4, dt, oVar);
}

double ICellModel::updateVariable(int N, double dt, double var, const TIdouble dVar) {
  switch (N) {
    case 1:
      return var + dt * dVar[0];
    case 2:
      return var + dt * dVar[1];
    case 4:
      return var + dt / 6. * (dVar[0] + 2 * dVar[1] + 2 * dVar[2] + dVar[3]);
  }
  return var;
}
