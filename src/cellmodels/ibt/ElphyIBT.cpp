#include "ElphyIBT.hpp"


ElphyIBT::ElphyIBT(EMP_IBT &ibtP_, double e_, double d_, double a_) : IElphyModel(e_, d_,
                                                                                  a_),
                                                                      ibtP(ibtP_) {
  initElphyModel<double>(&ElphyModel, ibtP.getParameters(), ibtP.getModel());
  V = ElphyModel->GetVm() * 1000.;
}

double ElphyIBT::updateStep(int, double, double) {
  return 0.0;
}

double ElphyIBT::updateModel(int N, double dt, double mVolt) {
  V_passed += dt;
  is_excited = (V_excitation >= 0. && V_passed >= V_excitation);
  double volt = mVolt / 1000.;
  double iTot = ElphyModel->Calc(dt, volt, getExternalCurrent(), 1, 1);
  V = (volt + iTot) * 1000.;
  return V;
}

double ElphyIBT::getExternalCurrent() const {
  if (is_excited && V_passed <= V_stimulation){
    return V_amplitude;
  }
  else
    return 0.0;
}