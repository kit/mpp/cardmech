#ifndef TENSIONIBT_H
#define TENSIONIBT_H

#include "ICellModel.hpp"
#include "ICellParameters.hpp"


class TensionIBT : public ITensionModel {
  TMP_IBT &ibtP;
  vbForceModel<double> *forceModel = NULL;

  double minLength = 1.7, maxLength = 2.6, startLength = 1.95;
  double baseCalcium = 0.2155;

  double alpha = -0.004;
  double mu = 5.0;

  //double ca0 = 0.2155;
  double ibtScaling;

  //Parameters for Rossi
  double c[4] = {-4333.618335582119, 2570.395355352195, 1329.53611689133, 104.943770305116};
  double d[3] = {-2051.827278991976, 302.216784558222, 218.375174229422};
public:
  TensionIBT(TMP_IBT &, double);

  int gatingDim() const override { return 1; };

  double updateStep(int, double, double) override;

  double updateModel(int, double, double) override;

  double UpdateForce(double, double, double) override;

  double UpdateLength(double, double, double) override;

  double ForceLengthRelation(double);

  double ActiveStressEvolution(double);

  double HeavisideCalcium(double);
};

#endif //TENSIONIBT_H
