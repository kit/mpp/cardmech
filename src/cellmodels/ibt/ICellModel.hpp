#ifndef ICELLMODEL_H
#define ICELLMODEL_H

#include <cmath>


// TIdouble is an array holding temporary values used in time integration.
typedef double TIdouble[5];

class ICellModel {
protected:
  int RK_Order = 1;

  double currentForce;
public:
  ICellModel() {
  }

  double update(double, double);

  double update_EE(double, double);

  double update_RK2(double, double);

  double update_RK4(double, double);

  virtual int gatingDim() const = 0;

  virtual double getVariable() const = 0;

  virtual double getGating() const = 0;

  virtual double updateStep(int, double, double) = 0;

  virtual double updateModel(int, double, double) = 0;

  double updateVariable(int, double, double, const TIdouble);
};

enum class IonConcentration {
  Ca_In = 0,
  Ca_Out = 1,
  Na_In = 2,
  Na_Out = 3,
  K_In = 4,
  K_Out = 5
};

class IElphyModel : public ICellModel {
protected:
  double V;

  double V_excitation;
  double V_stimulation;
  double V_amplitude;
  double V_passed;
  bool is_excited;
public:
  IElphyModel(double e, double d, double a) : V_excitation(e), V_stimulation(e + d),
                                              V_amplitude(a), V_passed(0.0) {
  }

  virtual double getVariable() const { return V; }

  virtual double getPotential() const { return getVariable(); };

  virtual double getExternalCurrent() const = 0;
};

class ITensionModel : public ICellModel {
protected:
  double force;
  double length;
  double velocity;
  double cCalcium;
  double invariant4f;
public:
  ITensionModel(double ca) : force(0.0), length(0.0), velocity(0.0), cCalcium(ca), invariant4f(1.) {
  }

  virtual double getVariable() const { return length; };

  virtual double getGating() const { return force; };

  virtual double update(double dt, double length_, double calcium_, double iota4_) {
    velocity = (length_ - length) / dt;
    length = length_;
    cCalcium = calcium_;
    invariant4f = iota4_;
    return ICellModel::update(dt, length);
  }

  virtual double UpdateForce(double, double, double) = 0;

  virtual double UpdateLength(double, double, double) = 0;
};

static double sigmoid(double t, double scale, double shift) {
  return (1 + tanh(scale * (t - shift))) / 2;
}

#endif //ICELLMODEL_H
