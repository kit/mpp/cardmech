#include "TensionIBT.hpp"


TensionIBT::TensionIBT(TMP_IBT &ibtP_, double baseCa) : ITensionModel(baseCa * 1000.), ibtP(ibtP_),
                                                        baseCalcium(baseCa * 1000.),
                                                        ibtScaling(ibtP_.scaling) {
  initForceModel<double>(&forceModel, ibtP.GetParameters(), ibtP.GetModel());
}

double TensionIBT::updateStep(int n, double dt, double l) {
  return 0.0;
}

double TensionIBT::updateModel(int N, double dt, double l) {
  double mcCalcium = cCalcium * 1000.;

  double dLength = UpdateLength(dt, mcCalcium, invariant4f);
  force = UpdateForce(dt, mcCalcium, l);

  velocity = dLength;
  length = l + dt * dLength;

  //2nd order Adams-Bashforth
  //length = length + dt*(1.5*dLength-0.5*dLength_old);
  //velocity = dLength;

  return length;
}

double TensionIBT::UpdateForce(double dt, double ca, double length) {
  double l = (startLength + length) / startLength;
  //mout << "Update Force" << endl;
  force = 1000 * forceModel->Calc(dt, l, velocity, ca); // Tension in Pa
  //if(force > 0.001)
  return force;
}

double TensionIBT::UpdateLength(double dt, double ca, double iota4) {
  double fA = HeavisideCalcium(ca) * alpha * ibtScaling * force * ForceLengthRelation(iota4);
  if (fA > 0) fA = -fA;

  double lA = iota4 * ActiveStressEvolution(length);
  /*double dl = 1.0/(mu*ca*ca)*(fA + lA);
  if(dl>0)
      mout << OUT(fA) << OUT(lA) << OUT(ca) << OUT(dl) << OUT(length) << endl;*/
  return 1.0 / (mu * ca * ca) * (fA + lA);
}

double TensionIBT::ForceLengthRelation(double iota4) {
  double rfl = 0.0;
  double l = length + startLength;
  //mout << OUT(l) << OUT(length) << OUT(l0) << endl;
  //double l = l0 * (1.0+length);
  if (l >= minLength && l <= maxLength) {
    rfl = c[0] / 2.0;
    for (int i = 1; i < 4; i++) {
      rfl += c[i] * sin(i * iota4 * startLength);
      rfl += d[i - 1] * cos(i * iota4 * startLength);
    }
  }

  return abs(rfl);
}

double TensionIBT::ActiveStressEvolution(double l) {
  double taylorSum = 0.0;
  if (l != 0.0) {

    for (int k = 5; k > 0; k--) {
      taylorSum = (taylorSum + (2.0 * (0.5 - k % 2)) * (k + 1) * (k + 2)) * l;
    }

    /*for(int k=1;k<6;k++){
        taylorSum += (2.0 * (0.5-k%2)) * (k+1) * (k+2) * pow(l, float(k));
    }*/
  }
  return taylorSum;
}

double TensionIBT::HeavisideCalcium(double ca) {
  if (ca - baseCalcium > 0) return 1.;
  else return 0.;
}