#ifndef MCELLMODELL_HPP
#define MCELLMODELL_HPP


#include "Algebra.hpp"
#include "CardiacData.hpp"
#include <functional>
#include "ExternalCurrent.hpp"

enum CellModelType {
  ELPHY, TENSION
};

class MCellModel {
  int size{1};
protected:
  bool useExpInt{false};

  double timeScaling;

  double amplitude{0.0};
  double excitation{infty};
  double duration{0.0};
public:
  explicit MCellModel(int N, double tS) : size(N), timeScaling(tS) {}

  virtual CellModelType Type() const = 0;

  int GatingSize() const { return Size() - GatingIndex(); }

  int ConcentrationSize() const { return GatingIndex() - CalciumIndex(); }

  virtual int Size() const { return size; }

  double TimeScale() const { return timeScaling; }

  virtual int CalciumIndex() const = 0;

  virtual int GatingIndex() const = 0;

  virtual int TensionIndex() const { return Size(); };

  virtual int ElphySize() const { THROW("Not implemented in the basis class") };

  virtual double IExt(double t) const {
    return excitationFunc(t, amplitude, excitation, duration);
  }

  virtual void SetExternal(double exc, double dur, double amp) {
    excitation = exc * TimeScale();
    duration = dur * TimeScale();
    amplitude = amp;
  }

  virtual void UseExpInt(bool expInt = true) { useExpInt = expInt; }

  virtual double GetAmplitude() const { return amplitude; };

  [[nodiscard]] virtual double GetIion(const std::vector<double> &var) const {
    THROW("Not implemented in the basis class")
  };

  virtual void Initialize(Vectors &vectors) = 0;

  virtual void Initialize(std::vector<double> &vector) = 0;

  /**
   * Updates algebraic equations within the cellmodel
   * @param dt timestep size
   * @param[out] var current values which may be updated
   */
  virtual void AlgebraicUpdate(double dt, std::vector<double> &var) const {};

  /**
   * Calculates exact values var(t+dt) and stores them in g.
   * Depending on the cell model, some of the entries of g may not be filled.
   * @param dt timestep size
   * @param var current values at time t
   * @param[out] g stores the next value var(t+dt)
   */
  virtual void ExpIntegratorUpdate(double dt, const std::vector<double> &var,
                                   std::vector<double> &g) const {};

  /**
   * Calculates the change rates of concentration-type variables and stores the results in g.
   * Depending on the cell model, some of the entries of g may not be filled.
   * @param var current values at time t
   * @param[out] g change rates
   * @param iExt external Current
   */
  virtual void ConcentrationUpdate(const std::vector<double> &var,
                                   std::vector<double> &g,
                                   double iExt) const {};

  /**
   * Calculates the change rates of all gating-type variables and stores the results in g.
   * Depending on the cell model, some of the entries of g may not be filled.
   * @param var current values at time t
   * @param[out] g change rates
   */
  virtual void GatingUpdate(const std::vector<double> &var,
                            std::vector<double> &g) const {};

  /**
   * Calculates the change rates of all variables and stores the results in g.
   * @param var urrent values at time t
   * @param[out] g change rates
   * @param iExt external current
   */
  virtual void RHSUpdate(const std::vector<double> &var, std::vector<double> &g,
                         double iExt) const = 0;

  /**
   * Updates the values with the given timestep and change rates
   * @param dt timestep size (varies depending on the method
   * @param[out] var values which are updated
   * @param g change rates (time derivatives of the correspondig ODE, depending on the method)
   */
  virtual void UpdateValues(double dt, std::vector<double> &var,
                            const std::vector<double> &g) const = 0;

  /**
   * Returns a value to initialize external components
   * @param valueType used to determine which initial value should be returned
   * @return Initial value of the variable which we want to solve
   */
  virtual double InitialValue(CellModelType valueType) const {
    THROW("Not implemented in the basis class")
  };


  /**
   * Generates a list of size GatingSize() containing all tau values of the correspondig gating variables
   * for the given cell model.
   * @param var current values to calculate tau
   */
  virtual std::vector<double> Tau(const std::vector<double> &var) const {
    THROW("Not implemented in base class")
  }

  /**
   * Generates a list of size GatingSize() containing all y_infty values of the correspondig gating
   * variables for the given cell model.
   * @param var current values to calculate y_infty
   */
  virtual std::vector<double> YInfty(const std::vector<double> &var) const {
    THROW("Not implemented in base class")
  }

  /**
   * Calculates the entry dVW[i]/dVW[j] of the Jacobi Matrix
   * @param i row index
   * @param j column index
   * @param VW current values
   */
  virtual double JacobiEntry(int i, int j, const std::vector<double> &VW) const {
    THROW("Not implemented for this cellmodel")
  };

};

class MElphyModel : public MCellModel {
protected:
  double CAScaling;
  bool iextInPDE = false;

  virtual double initialPotential() const = 0;

public:
  explicit MElphyModel(int N, double timeScale, double cS) : MCellModel(N, timeScale), CAScaling(cS) {
    Config::Get("IextInPDE", iextInPDE);
  }

  CellModelType Type() const override { return ELPHY; }

  int ElphySize() const { return Size(); }

  double InitialValue(CellModelType valueType) const override {
    return initialPotential();
  };

  virtual void IIonUpdate(const std::vector<double> &var,
                          std::vector<double> &g, double iExt) const {};

  void RHSUpdate(const std::vector<double> &var, std::vector<double> &g,
                 double iExt) const override {
    IIonUpdate(var, g, iExt);
    ConcentrationUpdate(var, g, iExt);
    GatingUpdate(var, g);
  };

  double CAScale() const { return CAScaling; }

};


class MTensionModel : public MCellModel {
protected:
  std::unique_ptr<MElphyModel> elphy;

  virtual double initialStretch() const = 0;

public:
  explicit MTensionModel(std::unique_ptr<MElphyModel> &&eModel, int N) : MCellModel(
      eModel->Size() + N, eModel->TimeScale()), elphy(std::move(eModel)) {
    amplitude = elphy->GetAmplitude();
  }

  CellModelType Type() const override {
    return TENSION;
  }

  double InitialValue(CellModelType valueType) const override {
    if (valueType == ELPHY) return elphy->InitialValue(valueType);
    else return initialStretch();
  }

  int ElphySize() const override { return elphy->Size(); }

  MElphyModel &ElphyModel() const { return *elphy; }

  double GetIion(const std::vector<double> &var) const override { return elphy->GetIion(var); };

  int CalciumIndex() const override { return elphy->CalciumIndex(); };

  int GatingIndex() const override { return elphy->GatingIndex(); };

  void SetExternal(double exc, double dur, double amp) override {
    MCellModel::SetExternal(exc, dur, amp);
    elphy->SetExternal(exc, dur, amp);
  }

  void UseExpInt(bool expInt) override {
    MCellModel::UseExpInt(expInt);
    elphy->UseExpInt(expInt);
  }

  std::vector<double> Tau(const std::vector<double> &var) const override {
    return elphy->Tau(var);
  }

  std::vector<double> YInfty(const std::vector<double> &var) const override {
    return elphy->YInfty(var);
  }

  double JacobiEntry(int i, int j,
                     const std::vector<double> &VW) const override {
    return elphy->JacobiEntry(i, j, VW);
  };

  void AlgebraicUpdate(double dt, std::vector<double> &var) const override {
    elphy->AlgebraicUpdate(dt, var);
  };

  void ExpIntegratorUpdate(double dt, const std::vector<double> &var,
                           std::vector<double> &g) const override {
    elphy->ExpIntegratorUpdate(dt, var, g);
  };

  void ConcentrationUpdate(const std::vector<double> &var,
                           std::vector<double> &g, double iExt) const override {
    elphy->ConcentrationUpdate(var, g, iExt);
    TensionUpdate(var, g, iExt);
  };

  void GatingUpdate(const std::vector<double> &var,
                    std::vector<double> &g) const override {
    elphy->GatingUpdate(var, g);
  };

  void RHSUpdate(const std::vector<double> &var, std::vector<double> &g,
                 double iExt) const override {
    elphy->RHSUpdate(var, g, iExt);
    TensionUpdate(var, g, iExt);
  };

  virtual void TensionUpdate(const std::vector<double> &var,
                             std::vector<double> &g, double iExt) const = 0;
};

static std::vector<int> ConcentrationIndices(const MCellModel &M) {
  std::vector<int> concentrationIndices{};
  for (int i = M.CalciumIndex(); i < M.GatingIndex(); ++i) {
    concentrationIndices.emplace_back(i);
  }
  for (int i = M.ElphySize(); i < M.Size(); ++i) {
    concentrationIndices.emplace_back(i);
  }
  return concentrationIndices;
}

std::unique_ptr<MElphyModel> GetElphyModel();

std::unique_ptr<MElphyModel> GetElphyModel(const std::string &name);

std::unique_ptr<MTensionModel> GetTensionModel(const std::string &name,
                                               std::unique_ptr<MElphyModel> &&);

std::unique_ptr<MTensionModel> GetTensionModel(std::unique_ptr<MElphyModel> &&);

std::unique_ptr<MTensionModel> GetTensionModel();

#endif //MCELLMODELL_HPP
