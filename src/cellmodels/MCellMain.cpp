#include <memory>

#include "m++.hpp"
#include "MeshesCreator.hpp"
#include "MultiPartDiscretization.hpp"
#include "CardiacData.hpp"
#include "MFitzHughNagumo.hpp"

#include "solvers/MElphySolver.hpp"

int main(int argc, char **argv) {
  Config::SetConfPath("../");
  Config::SetConfigFileName("m++.conf");
  Mpp::initialize(&argc, argv);

  Config::PrintInfo();

  auto meshes = MeshesCreator("FitzHughNagumoCube").CreateUnique();
  auto disc = std::make_unique<MultiPartDiscretization>(*meshes, 1, DomainPart, 1);
  auto V = std::make_unique<Vectors>(1, 0.0, *disc);
  auto dataDisc = std::make_unique<MultiPartDiscretization>(*meshes, 1, DomainPart, 3);
  auto excData = std::make_unique<Vector>(-100.0, *dataDisc);
  InterpolateMeshData((*V)[0], *excData);
  MFitzHughNagumo cellModel;

  double startTime{0.0};
  Config::Get("StartTime", startTime);
  double endTime{0.1};
  Config::Get("EndTime", endTime);
  double deltaTime{0.001};
  Config::Get("DeltaTime", deltaTime);
  int subSteps = 1;
  Config::Get("SubSteps", subSteps);

  TimeSeries TS(startTime, endTime, deltaTime / subSteps);
  MElphySolver solver(cellModel, *excData, "RungeKutta4");

  solver.Solve(TS, *V);
  return 0;
}
