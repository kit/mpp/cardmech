#include <ScalarElement.hpp>
#include "MFitzHughNagumo.hpp"


void MFitzHughNagumo::Initialize(Vectors &var) {
  var[0] = initialPotential();
  var[1] = 0.0;
}

void MFitzHughNagumo::Initialize(std::vector<double> &var) {
  var[0] = initialPotential();
  var[1] = 0.0;
}

std::vector<double> MFitzHughNagumo::Tau(const std::vector<double> &var) const {
  return {};
}

std::vector<double> MFitzHughNagumo::YInfty(
    const std::vector<double> &var) const {
  return {};
}

void MFitzHughNagumo::IIonUpdate(const std::vector<double> &var,
                                 std::vector<double> &g, double iExt) const {
  double mVolt = var[0];
  g[0] = c1 * mVolt * (mVolt - a) * (1 - mVolt) - c2 * var[1] + iExt;
}

void MFitzHughNagumo::ConcentrationUpdate(const std::vector<double> &var,
                                          std::vector<double> &g,
                                          double iExt) const {
  double mVolt = var[0];
  g[1] = b * (mVolt - c3 * var[1]);
}

void MFitzHughNagumo::UpdateValues(double dt, std::vector<double> &var,
                                   const std::vector<double> &g) const {
  var[0] += dt * g[0];
  var[1] += dt * g[1];
}

int MFitzHughNagumo::CalciumIndex() const {
  return 1;
}

int MFitzHughNagumo::GatingIndex() const {
  return -1;
}

