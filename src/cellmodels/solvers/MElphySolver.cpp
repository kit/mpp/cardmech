#include "MElphySolver.hpp"
#include "Logging.hpp"
#include "Spectrum.hpp"
#include <solvers/SplittingSolver.hpp>


void MElphySolver::initialize(Vector &u) {
  gating = std::make_unique<Vectors>(cellModel.Size(), u);
  mingating = std::make_unique<Vectors>(cellModel.Size(), u);
  maxgating = std::make_unique<Vectors>(cellModel.Size(), u);
  cellModel.Initialize(*gating);
  cellModel.Initialize(*maxgating);
  cellModel.Initialize(*mingating);
  u = (*gating)[0];
  init = true;
}

void MElphySolver::Solve(TimeSeries &microTS, Vectors &u) {
  double t = microTS.FirstTStep();
  double tNew = microTS.NextTimeStep();
  double T = microTS.LastTStep();

  if (!init) {
    initialize(u[0]);
  }

  Point P(0.0, 0.0, 0.0);
  //update von V aus Diffusion wird in gating geschrieben
  (*gating)[0] = u[0];
  std::vector<double> vw(cellModel.Size()+(cellModel.Type() == TENSION));
  std::vector<std::vector<double>> g(steps);
  for (int i = 0; i < steps; ++i)
    g[i] = std::vector<double>(cellModel.Size());//vorher: size

  for (row r = u[0].rows(); r != u[0].rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < cellModel.Size(); ++i) {//stimmt hier der max-WErt?
        vw[i] = (*gating)[i](r, j);
      }
      if (cellModel.Type() == TENSION) {
        vw[cellModel.Size()] = u[2](r, j);
      }
      fillTimeIndependant(vw, u, r, j);
      t = microTS.FirstTStep();
      tNew = microTS.NextTimeStep();
      T = microTS.LastTStep();
      while (t < T) {
        if (r() == P) {
          vout (15) << endl
                    << "################################################################################"
                    << endl
                    << "# From " << t << " to " << tNew << " (" << tNew - t << ") "
                    << endl;
        }

        //fillTimeDependant(vw, u, t, r);
        SolveStep(cellModel, t, tNew - t, vw, g);
        t = tNew;
        tNew = microTS.NextTimeStep();
      }
      microTS.Reset();

      //u[0](r, j) = vw[0];

      for (int i = 0; i < gating->size(); ++i) {
        (*gating)[i](r, j) = vw[i];
        if ((*gating)[i](r, j) > (*maxgating)[i](r, j)) {
          (*maxgating)[i](r, j) = (*gating)[i](r, j);
        } else if ((*gating)[i](r, j) < (*mingating)[i](r, j)) {
          (*mingating)[i](r, j) = (*gating)[i](r, j);
        }

      }
    }

  }
  u = (*gating)[0];
  if (cellModel.Type() == TENSION) {
    u[1] = (*gating)[cellModel.ElphySize()];
  }
  vout(20) << "Finished Solve ODE " << endl;
}

void MElphySolver::ComputeAndPrintEigenvalues(const std::vector<double> &VW) {
  int n = cellModel.GatingSize() + 1;
  std::vector<std::vector<double>> J(n);
  for (int i = 0; i < n; ++i) {
    J[i] = std::vector<double>(n);
    for (int j = 0; j < n; j++) {
      J[i][j] = cellModel.JacobiEntry(i, j, VW);
    }
  }
  RMatrix A(J);
  CEigenvalues lambda;
  EVreal(A, lambda);
  for (int i = 0; i < n; i++) {
    vout(15) << "EV" << i << ": " << lambda[i] << endl;
  }
}

void MElphySolver::fillTimeIndependant(std::vector<double> vector, Vectors &vectors, row row, int j) {

  cellModel.SetExternal(Excitation(excData, row, j), Duration(excData, row, j),
                          Amplitude(excData, row, j));

  //das war wohl früher die Stelle, jetzt sind die Werte anders angeordnet
  /*if (cellModel.Type() != ELPHY) {
    vector[size - 2] = vectors[1](row, 0);    // 4th Invariant
  }
  if (cellModel.Type() == TENSION) {
    vector[size - 1] = vectors[2](row, 0);  // Calcium
  }*/
}

void MElphySolver::Solve(TimeSeries &microTS, Vector &u) {
  Vectors V(1, u);
  Solve(microTS, V);
  u = V[0];
}

void MElphySolver::Solve(TimeSeries &microTS, std::vector<double> &u) {
  u.resize(microTS.Steps() + 1);

  std::vector<double> vw(size);
  std::vector<std::vector<double>> g(steps);
  for (int i = 0; i < steps; ++i) {
    g[i] = std::vector<double>(cellModel.Size());
  }
  cellModel.Initialize(vw);
  u[0] = vw[0];

  double t = 0.0;
  for (int step = 0; step < microTS.Steps(); ++step) {
    SolveStep(cellModel, t, microTS.StepSize(), vw, g);
    u[step + 1] = vw[0];
    t = microTS.NextTimeStep();
  }
}

void MElphySolver::PrintMaxMinGating() const {
  std::vector<double> maxGatingOverVertices(cellModel.Size());
  std::vector<double> minGatingOverVertices(cellModel.Size());
  for (int i = 0; i < cellModel.Size(); i++) {
    maxGatingOverVertices[i] = -infty;
    minGatingOverVertices[i] = 10000000;
  }
  for (row r = (*maxgating).rows(); r != (*maxgating).rows_end(); ++r) {
    for (int j = 0; j < r.NumberOfDofs(); ++j) {
      for (int i = 0; i < cellModel.Size(); i++) {
        if ((*mingating)[i](r, j) < minGatingOverVertices[i]) {
          minGatingOverVertices[i] = (*mingating)[i](r, j);
        } else if ((*maxgating)[i](r, j) > maxGatingOverVertices[i]) {
          maxGatingOverVertices[i] = (*maxgating)[i](r, j);
        }
      }

    }
  }

  vout(1) << " " << endl;
  vout(1) << "# Finished: " << endl;
  vout(5) << " V  Ca  d  f  h  j  m  x_1" << endl;
  vout(1) << "Max values: ";
  for (int i = 0; i < cellModel.Size(); i++) {
    vout(1) << PPM->Max(maxGatingOverVertices[i]) << " ";
  }
  vout(1) << endl;
  vout(1) << "Min values: ";
  for (int i = 0; i < cellModel.Size(); i++) {
    vout(1) << PPM->Min(minGatingOverVertices[i]) << " ";
  }
  vout(1) << endl;
}

void MElphySolver::selectScheme(const std::string &solvingScheme) {
  if (getSplitting() == STEIN) {
    steps = 2;
    SolveStep = HeunStep;
  } else {
    if (solvingScheme == "ExplicitEuler") {
      steps = 1;
      SolveStep = ExplicitEulerStep;
    } else if (solvingScheme == "RungeKutta2") {
      steps = 2;
      SolveStep = RungeKutte2Step;
    } else if (solvingScheme == "Heun") {
      steps = 2;
      SolveStep = HeunStep;
    } else if (solvingScheme == "RungeKutta4") {
      steps = 4;
      SolveStep = RungeKutta4Step;
    } else if (solvingScheme == "ExponentialIntegrator") {
      cellModel.UseExpInt();
      steps = 1;
      SolveStep = ExponentialIntegratorStep;
    } else if (solvingScheme == "EIwithNewtonStep") {
      cellModel.UseExpInt();
      steps = 1;
      SolveStep = EIWithNewtonStep;
    } else if (solvingScheme == "Eigenvalue") {
      steps = 1;
      SolveStep = EigenvalueStep;
    } else {
      THROW("CellModelScheme \"" + solvingScheme + "\" unknown.")
    }
  }
}

double MElphySolver::TimeScale() const {
  return cellModel.TimeScale();
}

void MElphySolver::SetExternal(std::vector<double> values) {
  cellModel.SetExternal(values[0], values[1], values[2]);
}

void MElphySolver::Solve(TimeSeries &microTS,
                         std::vector<std::vector<double>> &u) {
  u.resize(microTS.Steps() + 1);

  std::vector<double> vw(cellModel.Size() + (cellModel.Type() == TENSION));
  std::vector<std::vector<double>> g(steps);
  for (int i = 0; i < steps; ++i) {
    g[i] = std::vector<double>(cellModel.Size());
  }
  cellModel.Initialize(vw);
  if (cellModel.Type() == TENSION) {
    vw[cellModel.Size()] =
        (1.0 + vw[cellModel.ElphySize()]) * (1.0 + vw[cellModel.ElphySize()]);
  }

  u[0] = vw;

  while (!microTS.IsFinished()) {
    SolveStep(cellModel, microTS.Time(), microTS.StepSize(), vw, g);
    if (cellModel.Type() == TENSION) {
      vw[cellModel.Size()] = 1.0;
    }
    microTS.NextTimeStep();
    u[microTS.Step()] = vw;
  }
}

void ExponentialIntegratorStep(MCellModel &cellModel, double t, double dt,
                               std::vector<double> &VW,
                               std::vector<std::vector<double>> &g) {
  int position = cellModel.GatingIndex();

  cellModel.ExpIntegratorUpdate(dt, VW, g[0]);
  cellModel.RHSUpdate(VW, g[0], cellModel.IExt(t));
  cellModel.UpdateValues(dt, VW, g[0]);
  /*
  VW[0] += dt * g[0][0];
  cellModel.EvaluateConcentration(t, VW, g[0]);
  for (int i =1;i<position;i++){
    VW[i] += dt * g[0][i];
  }*/
}

void EIWithNewtonStep(MCellModel &cellModel, double t, double dt,
                      std::vector<double> &VW,
                      std::vector<std::vector<double>> &g) {
  double mVolt = VW[0];
  int position = cellModel.GatingIndex();

  cellModel.ExpIntegratorUpdate(dt, VW, g[0]);
  cellModel.RHSUpdate(VW, g[0], cellModel.IExt(t));
  double DVIion = cellModel.JacobiEntry(0, 0, VW);
  VW[0] = (1 / (1 + dt * DVIion)) * ((1 + dt * DVIion) * mVolt + dt * g[0][0]);
  cellModel.ConcentrationUpdate(VW, g[0], cellModel.IExt(t));
  for (int i = 1; i < position; i++) {
    VW[i] += dt * g[0][i];
  }
}

void EigenvalueStep(MCellModel &cellModel, double t, double dt,
                    std::vector<double> &VW,
                    std::vector<std::vector<double>> &g) {
  double mVolt = VW[0];
  double volt = mVolt / 1000.;

  auto yInfty = cellModel.YInfty(VW);
  auto tau = cellModel.YInfty(VW);
  for (int i = 0; i < cellModel.GatingSize(); ++i) {
    int iShift = cellModel.GatingIndex() + i;

    VW[iShift] = yInfty[i] + (VW[iShift] - yInfty[i]) * exp(-1000 * dt / tau[i]);
  }

  cellModel.RHSUpdate(VW, g[0], cellModel.IExt(t));
  for (int i = 0; i < cellModel.GatingIndex(); ++i) {
    VW[i] += dt * 1000.0 * g[0][i];
  }
}

/*void MEigenvalueSolver::ComputeAndPrintEigenvalues(std::vector<double> &VW) {
  int n = cellModel.GatingDim()+1;
  std::vector<std::vector<double>> J(n);
  for(int i = 0; i<n; ++i) {
    J[i] = std::vector<double>(n);
    for (int j =0; j<n;j++){
      J[i][j]=cellModel.JacobiEntry(i, j, VW);
    }
  }
  for (int i =0; i<n; ++i) {
    vout(15)<<"J("<<i<<") = ";
    for (int j =0; j<n;j++) {
      vout(15) << J[i][j] <<" ";
    }
    vout(15)<<endl;
  }

  RMatrix A(J);
  CEigenvalues lambda;
  EVreal(A, lambda);
  for (int i=0;i<n;i++){
    vout(15) <<"EV"<<i<<": "<< lambda[i] << endl;
  }




}*/
