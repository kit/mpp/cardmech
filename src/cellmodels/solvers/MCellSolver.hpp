#ifndef MCELLSOLVER_HPP
#define MCELLSOLVER_HPP

#include "CardiacSolver.hpp"
#include "TimeSeries.hpp"
#include "MCellModel.hpp"
#include <functional>

void ExplicitEulerStep(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                       std::vector<std::vector<double>> &g);

void RungeKutte2Step(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                     std::vector<std::vector<double>> &g);
void HeunStep(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                     std::vector<std::vector<double>> &g);

void RungeKutta4Step(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                     std::vector<std::vector<double>> &g);


class MCellSolver : public CardiacSolver {
protected:
  bool init = false;
  int size{1};

  std::unique_ptr<Vectors> gating{};
  std::unique_ptr<std::vector<Vectors>> intermediateGating{};
  int steps{1};


public:
  explicit MCellSolver(int N) : size(N) {
    Config::Get("CellModelVerbose", verbose);
    Config::Get("CellModelVtk", verbose);
  }

  virtual void Solve(TimeSeries &microTS, Vector &u) = 0;

  virtual void Solve(TimeSeries &microTS, Vectors &u) = 0;

  virtual void Solve(TimeSeries &microTS, std::vector<double> &u) = 0;

  virtual double TimeScale() const { return 1.0; }

  virtual void PrintMaxMinGating() const {}

  virtual void SetExternal(std::vector<double> values) {};
};


/*
 *

  };
 */

#endif //MCELLSOLVER_HPP
