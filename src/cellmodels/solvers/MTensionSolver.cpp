#include <Plotting.hpp>
#include "MTensionSolver.hpp"

void MTensionSolver::Solve(TimeSeries &microTS, Vectors &vecs) {
  if (!init) {
    initialize(vecs[0]);
  } else {
    (*gating)[0] = vecs[0];
    (*gating)[cellModel.ElphySize()] = vecs[1];
  }

  std::vector<double> vw(size);
  std::vector<std::vector<double>> g(steps);
  for (int i = 0; i < steps; ++i)
    g[i] = std::vector<double>(cellModel.Size());

  for (row r = vecs[0].rows(); r != vecs[0].rows_end(); ++r) {
    for (int j = 0; j < r.n(); ++j) {
      for (int i = 0; i < cellModel.Size(); ++i) {
        vw[i] = (*gating)[i](r, j);
      }
      vw[invariantIndex()] = vecs[2](r, j);
      if (!iextInPDE) {
        cellModel.SetExternal(Excitation(excData, r, j), Duration(excData, r, j),
                              Amplitude(excData, r, j));
      }

      int step = 1;
      for (double t = microTS.NextTimeStep();; t = microTS.NextTimeStep(), ++step) {
        SolveStep(cellModel, t, microTS.StepSize(), vw, g);

        if (Verbose() > 1) {
          PlotEntries(vw, r, j, step);
        }
        if (t >= microTS.LastTStep()) break;
      }

      microTS.Reset();
/*
      if(r() == Origin){
        for (int i = 0;i<gating->size();++i)
          mout << i << ": " << vw[i]<< endl;
      }
*/
      for (int i = 0; i < gating->size(); ++i) {
        (*gating)[i](r, j) = vw[i];
      }
    }

  }
  vecs[0] = (*gating)[0];
  vecs[1] = (*gating)[cellModel.ElphySize()];

  if (Vtk() > 0) {
    PlotResult(*gating, microTS.FirstTStep(), microTS.LastTStep());
  }
}

void MTensionSolver::Solve(TimeSeries &microTS, std::vector<double> &u) {}

void MTensionSolver::initialize(Vector &u) {
  gating = std::make_unique<Vectors>(cellModel.Size(), u);
  cellModel.Initialize(*gating);
  u = (*gating)[0];
  init = true;
}

void MTensionSolver::selectScheme(const std::string &solvingScheme) {
  if (solvingScheme == "ExplicitEuler") {
    steps = 1;
    SolveStep = ExplicitEulerStep;
  } else if (solvingScheme == "RungeKutta2") {
    steps = 2;
    SolveStep = RungeKutte2Step;
  } else if (solvingScheme == "RungeKutta4") {
    steps = 4;
    SolveStep = RungeKutta4Step;
  } else {
    THROW(("Scheme \"" + solvingScheme + "\" not implemented").c_str())
  }
}

void MTensionSolver::PlotEntries(const std::vector<double> &entries, row row,
                                 int j, int step) const {
  if (row() == Origin) {
    mout.PrintInfo("CellModelValues", 1,
                   PrintInfoEntry("Calcium", entries[cellModel.ElphyModel().CalciumIndex()]),
                   PrintInfoEntry("Stretch", entries[cellModel.ElphySize()])
    );
  }
}

void MTensionSolver::PlotResult(const Vectors &vectors, double beginT, double endT) const {
  auto &plot = mpp::plot("Stretch");
  plot.AddData("Potential", vectors[0]);
  plot.AddData("Calcium", vectors[cellModel.ElphyModel().CalciumIndex()]);
  plot.AddData("Stretch", vectors[cellModel.ElphySize()]);
  plot.AddData("Tension", vectors[cellModel.TensionIndex()]);
  plot.PlotFile(FilenameTime("StretchData", endT));
}

double MTensionSolver::TimeScale() const {
  return cellModel.TimeScale();
}

void MTensionSolver::SetExternal(std::vector<double> values) {
  cellModel.ElphyModel().SetExternal(values[0], values[1], values[2]);
}
