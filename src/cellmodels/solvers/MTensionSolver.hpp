#ifndef MTENSIONSOLVER_HPP
#define MTENSIONSOLVER_HPP

#include "MCellModel.hpp"
#include "MCellSolver.hpp"

class MTensionSolver : public MCellSolver {
  MTensionModel &cellModel;
  const Vector &excData;
  bool iextInPDE = true;

  void initialize(Vector &u);

  void selectScheme(const std::string &solvingScheme);

  std::function<void(MTensionModel &, double, double, std::vector<double> &,
                     std::vector<std::vector<double>> &)> SolveStep;


  int invariantIndex() const { return size - 1; }

public :
  explicit MTensionSolver(MTensionModel &M, const Vector &data) : MCellSolver(M.Size() + 1),
                                                                  cellModel(M), excData(data) {
    std::string solvingScheme{"ExplicitEuler"};
    Config::Get("CellModelScheme", solvingScheme);
    selectScheme(solvingScheme);
    Config::Get("IextInPDE", iextInPDE);
  }

  explicit MTensionSolver(MTensionModel &M, const Vector &data,
                          const std::string &solvingScheme)
      : MCellSolver(M.Size() + 1), cellModel(M), excData(data) {
    selectScheme(solvingScheme);
    Config::Get("IextInPDE", iextInPDE);
  }

  void Solve(TimeSeries &microTS, Vector &vec) override {
    THROW("Not enough components in Vector")
  };

  void Solve(TimeSeries &microTS, Vectors &vecs) override;

  void Solve(TimeSeries &microTS, std::vector<double> &u) override;

  void PlotEntries(const std::vector<double> &vector, row row, int j,
                   int step) const;

  void PlotResult(const Vectors &vectors, double beginT, double endT) const;

  double TimeScale() const override;

  void SetExternal(std::vector<double> values) override;
};


#endif //MTENSIONSOLVER_HPP
