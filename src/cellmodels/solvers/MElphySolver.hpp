#ifndef MELPHYSOLVER_HPP
#define MELPHYSOLVER_HPP

#include "MCellSolver.hpp"
#include "TimeSeries.hpp"

void ExponentialIntegratorStep(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                               std::vector<std::vector<double>> &g);

void EigenvalueStep(MCellModel &cellModel, double t, double dt, std::vector<double> &VW,
                    std::vector<std::vector<double>> &g);

void EIWithNewtonStep(MCellModel &cellModel, double t, double dt,
                      std::vector<double> &VW,
                      std::vector<std::vector<double>> &g);

class MElphySolver : public MCellSolver {
  MCellModel &cellModel;
  const Vector &excData;
  std::unique_ptr<Vectors> maxgating;
  std::unique_ptr<Vectors> mingating;





  void selectScheme(const std::string &solvingScheme);

  std::function<void(MCellModel &, double, double, std::vector<double> &,
                     std::vector<std::vector<double>> &)> SolveStep;

  void fillTimeIndependant(std::vector<double> vector, Vectors &vectors, row row, int j = 0);
public :
  explicit MElphySolver(MCellModel &M, const Vector &data) : MCellSolver(M.Size()),
                                                             cellModel(M), excData(data) {
    std::string solvingScheme{"ExplicitEuler"};
    Config::Get("CellModelScheme", solvingScheme);
    selectScheme(solvingScheme);

  }

  explicit MElphySolver(MCellModel &M, const Vector &data,
                        const std::string &solvingScheme)
      : MCellSolver(M.Size() + 1), cellModel(M), excData(data) {
    selectScheme(solvingScheme);
  }

  void initialize(Vector &u);
  void Solve(TimeSeries &microTS, Vector &u) override;

  void Solve(TimeSeries &microTS, Vectors &u) override;

  void Solve(TimeSeries &microTS, std::vector<double> &u) override;
  void Solve(TimeSeries &microTS, std::vector<std::vector<double>> &u);


  void ComputeAndPrintEigenvalues(const std::vector<double> &VW);

  double TimeScale() const override;

  void PrintMaxMinGating() const override;

  void SetExternal(std::vector<double> values) override;

  MCellModel &GetModel() const {
    return cellModel;
  }
  Vector &GetCa() const {return (*gating)[1];};
};

#endif //MELPHYSOLVER_HPP
