#include "MCellSolver.hpp"

void ExplicitEulerStep(MCellModel &cellModel, double t, double dt,
                       std::vector<double> &VW,
                       std::vector<std::vector<double>> &g) {
  cellModel.RHSUpdate(VW, g[0], cellModel.IExt(t));
  cellModel.UpdateValues(dt, VW, g[0]);
}

void RungeKutte2Step(MCellModel &cellModel, double t, double dt,
                     std::vector<double> &VW,
                     std::vector<std::vector<double>> &g) {
  std::vector<double> k(VW.size());
  std::uninitialized_copy(VW.begin(), VW.end(), k.begin());

  cellModel.RHSUpdate(VW, g[0], cellModel.IExt(t));
  cellModel.UpdateValues(0.5 * dt, k, g[0]);

  cellModel.RHSUpdate(k, g[1], cellModel.IExt(t+0.5*dt));
  cellModel.UpdateValues(dt, VW, g[1]);
}
void HeunStep(MCellModel &cellModel, double t, double dt,
              std::vector<double> &VW, std::vector<std::vector<double>> &g) {
  std::vector<double> k(VW.size());
  std::uninitialized_copy(VW.begin(), VW.end(), k.begin());

  cellModel.RHSUpdate(VW, g[0], cellModel.IExt(t));
  cellModel.UpdateValues(dt, k, g[0]);

  cellModel.RHSUpdate(k, g[1], cellModel.IExt(t+dt));
  for (int i = 0; i < g[0].size(); ++i) {
    g[0][i] += g[1][i];
  }
  cellModel.UpdateValues(0.5*dt, VW, g[0]);
}

void RungeKutta4Step(MCellModel &cellModel, double t, double dt,
                     std::vector<double> &VW,
                     std::vector<std::vector<double>> &g) {
  std::vector<double> k(VW.size());
  std::uninitialized_copy(VW.begin(), VW.end(), k.begin());

  cellModel.RHSUpdate(VW, g[0], cellModel.IExt(t));
  cellModel.UpdateValues(0.5 * dt, k, g[0]);

  cellModel.RHSUpdate(k, g[1], cellModel.IExt(t+0.5*dt));
  std::copy(VW.begin(), VW.end(), k.begin());
  cellModel.UpdateValues(0.5 * dt, k, g[1]);

  cellModel.RHSUpdate(k, g[2], cellModel.IExt(t+0.5*dt));
  std::copy(VW.begin(), VW.end(), k.begin());
  cellModel.UpdateValues(dt, k, g[2]);

  cellModel.RHSUpdate(k, g[3], cellModel.IExt(t+dt));
  for (int i = 0; i < g[0].size(); ++i) {
    g[0][i] += 2.0 * g[1][i] + 2.0 * g[2][i] + g[3][i];
  }
  cellModel.UpdateValues(dt/6.0, VW, g[0]);
}

/*void MEigenvalueSolver::ComputeAndPrintEigenvalues(std::vector<double> &VW) {
  int n = cellModel.GatingDim()+1;
  std::vector<std::vector<double>> J(n);
  for(int i = 0; i<n; ++i) {
    J[i] = std::vector<double>(n);
    for (int j =0; j<n;j++){
      J[i][j]=cellModel.JacobiEntry(i, j, VW);
    }
  }
  for (int i =0; i<n; ++i) {
    vout(15)<<"J("<<i<<") = ";
    for (int j =0; j<n;j++) {
      vout(15) << J[i][j] <<" ";
    }
    vout(15)<<endl;
  }

  RMatrix A(J);
  CEigenvalues lambda;
  EVreal(A, lambda);
  for (int i=0;i<n;i++){
    vout(15) <<"EV"<<i<<": "<< lambda[i] << endl;
  }




}*/
