#ifndef MROSSI_HPP
#define MROSSI_HPP


#include "MCellModel.hpp"

// Variable indexing
#define R_gamma(v) v[elphy->Size()]
#define R_force(v) v[elphy->Size()+1]
#define R_iota(v) v[elphy->Size()+2]

class MRossi : public MTensionModel {

  double restingCA{0.0};        // mM
  double viscosity{12.0};       // s / M^2
  double sarcomereForce{6};    // 1/muM^2
  double initialLength{1.95};   // mu m
  double minLength{1.7};
  double maxLength{2.6};

  double alpha = -0.004;
  double mu = 5.0;

  //Parameters for Rossi
  std::array<double, 4> c{-4333.618335582119, 2570.395355352195, 1329.53611689133,
                          104.943770305116};
  std::array<double, 3> d{-2051.827278991976, 302.216784558222, 218.375174229422};


  double heavyside(double calcium);

  double forceLengthRelation(double iota4) const;

  double activeStressEvolution(double l, int N = 5) const;

protected:
  double initialStretch() const override;

public:
  explicit MRossi(std::unique_ptr<MElphyModel> &&M, double initCA = 0.0) : MTensionModel(
      std::move(M), 2),
                                                                           restingCA(initCA) {
    Config::Get("RossiViscosity", viscosity);
    Config::Get("RossiForce", sarcomereForce);
  }

  int Size() const override { return elphy->Size() + 2; }

  int TensionIndex() const override { return elphy->Size() + 1; }

  void Initialize(Vectors &VW) override;

  void Initialize(std::vector<double> &vw) override;

  /*
   * The vector var contains all intermediate variables in the following order:
   * 0. stretch
   * 1. calcium
   * 2. I4 (macroscopic second material invariant)
   */
  void TensionUpdate(const std::vector<double> &var, std::vector<double> &g,
                     double iExt) const override;

  void UpdateValues(double dt, std::vector<double> &var,
                    const std::vector<double> &g) const override;
};

#endif //MROSSI_HPP
