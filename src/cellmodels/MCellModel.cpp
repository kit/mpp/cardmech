#include "MCellModel.hpp"
#include "MFitzHughNagumo.hpp"
#include "MBeelerReuter.hpp"
#include "MRossi.hpp"
#include "MTenTusscher.hpp"



std::unique_ptr<MElphyModel> GetElphyModel() {
  std::string cmClass = "MElphyModel";
  Config::Get("ElphyModelClass", cmClass);

  if(contains(cmClass,"IBT"))
    return GetElphyModel("IBTCellModel");

  std::string cmName = "FitzHughNagumo";
  Config::Get("ElphyModelName", cmName);
  return GetElphyModel(cmName);
}

std::unique_ptr<MElphyModel> GetElphyModel(const std::string &name) {
  if (name == "FitzHughNagumo") {
    return std::make_unique<MFitzHughNagumo>();
  }
  if (name == "BeelerReuter") {
    return std::make_unique<MBeelerReuter>();
  }
  if (name == "TenTusscher") {
    return std::make_unique<MTenTusscher>();
  }
  if (contains(name, "IBT")) {
    return nullptr;
  }

  THROW(name + " cellmodel is not implemented.")
}

std::unique_ptr<MTensionModel> GetTensionModel(
    const std::string &name, std::unique_ptr<MElphyModel> &&M) {
  if (name == "Rossi") {
    return std::make_unique<MRossi>(std::move(M));
  }

  THROW(name + " cellmodel is not implemented.")
}

std::unique_ptr<MTensionModel> GetTensionModel(std::unique_ptr<MElphyModel> &&M) {
  std::string cmName = "Rossi";
  Config::Get("TensionModelName", cmName);
  return GetTensionModel(cmName, std::move(M));
}

std::unique_ptr<MTensionModel> GetTensionModel() {
  return GetTensionModel(GetElphyModel());
}

