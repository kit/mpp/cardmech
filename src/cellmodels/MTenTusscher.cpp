#include "MTenTusscher.hpp"

void MTenTusscher::Initialize(Vectors &VW) {
  TT_Pot(VW) = initialPotential(); //V // x
  TT_Ca(VW) = 0.000126;//0.00007//0.000153;
  TT_Ca_SS(VW) = 0.00036;//0.00007//0.00042;
  TT_Ca_SR(VW) = 3.64;//1.3//4.272;
  TT_Na(VW) = 8.604;//7.67//10.132;
  TT_K(VW) = 136.89;//38.3//138.52;
  TT_m(VW) = 0.00172; //0.0//0.00165;
  TT_h(VW) = 0.7444; // 0.75//0.749;
  TT_j(VW) = 0.7045; // 0.75//0.6788;
  TT_x_r1(VW) = 0.00621; //0.0// 0.0165;
  TT_x_r2(VW) = 0.4712; //1.0// 0.473;
  TT_x_s(VW) = 0.0095; //  0.0//0.0174;
  TT_s(VW) = 0.999998; //1.0// 0.999998;
  TT_r(VW) = 2.42e-8; //0.0// 2.347e-8;
  TT_d(VW) = 3.373e-5; // 0.0//3.288e-5;
  TT_f(VW) = 0.7888; //1.0// 0.7026;
  TT_f_2(VW) = 0.9755; // 1.0//0.9526;
  TT_f_Ca(VW) = 0.9953; // 1.0// 0.9942;
  TT_R_quer(VW) = 0.9073; // 1.0//0.8978;

}

void MTenTusscher::Initialize(std::vector<double> &vw) {
  TT_Pot(vw) = initialPotential();
  TT_Ca(vw) = 0.000126;//0.00007//0.000153;
  TT_Ca_SS(vw) = 0.00036;//0.00007//0.00042;
  TT_Ca_SR(vw) = 3.64;//1.3//4.272;
  TT_Na(vw) = 8.604;//7.67//10.132;
  TT_K(vw) = 136.89;//38.3//138.52;
  TT_m(vw) = 0.00172; //0.0//0.00165;
  TT_h(vw) = 0.7444; // 0.75//0.749;
  TT_j(vw) = 0.7045; // 0.75//0.6788;
  TT_x_r1(vw) = 0.00621; //0.0// 0.0165;
  TT_x_r2(vw) = 0.4712; //1.0// 0.473;
  TT_x_s(vw) = 0.0095; //  0.0//0.0174;
  TT_s(vw) = 0.999998; //1.0// 0.999998;
  TT_r(vw) = 2.42e-8; //0.0// 2.347e-8;
  TT_d(vw) = 3.373e-5; // 0.0//3.288e-5;
  TT_f(vw) = 0.7888; //1.0// 0.7026;
  TT_f_2(vw) = 0.9755; // 1.0//0.9526;
  TT_f_Ca(vw) = 0.9953; // 1.0// 0.9942;
  TT_R_quer(vw) = 0.9073; // 1.0//0.8978;
}

double tauFormula_h(double V) {
  if (V >= -40.) {
    return (0.13 * (1. + exp(-(V + 10.66) / 11.1))) / 0.77;
  } else {
    return 1.0 /
           ((0.057 * exp(-(V + 80.) / 6.8)) + (2.7 * exp(0.079 * V) + (3.1e5) * exp(0.3485 * V)));
  }
}

double tauFormula_j(double V) {
  if (V >= -40.) {
    return (1. + exp(-0.1 * (V + 32.))) / (0.6 * exp((0.057) * V));
  } else {
    double aj = (((-2.5428e4) * exp(0.2444 * V) - (6.948e-6) *
                                                  exp(-0.04391 * V)) * (V + 37.78) /
                 (1. + exp(0.311 * (V + 79.23))));
    double bj = (0.02424 * exp(-0.01052 * V) / (1. + exp(-0.1378 * (V + 40.14))));
    return 1.0 / (aj + bj);
  }
}

#define TT_tau_m(V)    (1./(1.+exp((-60.-V)/5.)) * (0.1/(1.+exp((V+35.)/5.))+0.10/(1.+exp((V-50.)/200.)))            )
#define TT_tau_h(V)    (tauFormula_h(V)                                                                           )
#define TT_tau_j(V)    (tauFormula_j(V)                                                                           )
#define TT_tau_x_r1(V) ((450. / (1. + exp((-45. - V) / 10.))) * (6. / (1. + exp((V - (-30.)) / 11.5)))                                          )
#define TT_tau_x_r2(V) ((3. / (1. + exp((-60. - V) / 20.))) *       (1.12 / (1. + exp((V - 60.) / 20.)))                                    )
#define TT_tau_x_s(V)  ((1400. / (sqrt(1. + exp((5. - V) / 6)))) * (1. / (1. + exp((V - 35.) / 15.))) + 80. )
#define TT_tau_s(V)    (85. * exp(-(V + 45.) * (V + 45.) / 320.) + 5. / (1. + exp((V - 20.) / 5.)) + 3.                                           )
#define TT_tau_r(V)    (9.5 * exp(-(V + 40.) * (V + 40.) / 1800.) + 0.8                                                               )
#define TT_tau_d(V)    (( 1.4 / (1. + exp((-35 - V) / 13)) + 0.25) * (1.4 / (1. + exp((V + 5) / 5))) + (1. / (1. + exp((50 - V) / 20)))              )
#define TT_tau_f(V)    ((1102.5 * exp(-(V + 27) * (V + 27) / 225)) + (200. / (1 + exp((13 - V) / 10.))) + (180. / (1 + exp((V + 30) / 10))) + 20 )
#define TT_tau_f_2(V)  ((600 * exp(-(V + 25) * (V + 25) / 170))+(31 / (1. + exp((25 - V) / 10)))+(16 / (1. + exp((V + 30) / 10)))                )
#define TT_tau_f_Ca(CaSS)  (80. / (1 + (CaSS / 0.05) * (CaSS / 0.05)) + 2.                                                                  )


#define TT_yinfty_m(V)     (1./((1.+exp((-56.86-V)/9.03))*(1.+exp((-56.86-V)/9.03))))
#define TT_yinfty_h(V)     (1. / ((1. + exp((V + 71.55) / 7.43)) * (1. + exp((V + 71.55) / 7.43))))
#define TT_yinfty_j(V)     (1. / ((1. + exp((V + 71.55) / 7.43)) * (1. + exp((V + 71.55) / 7.43))))
#define TT_yinfty_x_r1(V)  (1. / (1. + exp((-26. - V) / 7.)))
#define TT_yinfty_x_r2(V)  (1. / (1. + exp((V - (-88.)) / 24.)))
#define TT_yinfty_x_s(V)   (1. / (1. + exp((-5. - V) / 14.)))
#define TT_yinfty_s(V)     (1. / (1. + exp((V + 20.) / 5.)))
#define TT_yinfty_r(V)     (1. / (1. + exp((20. - V) / 6.)))
#define TT_yinfty_d(V)     (1. / (1. + exp((-8. - V) / 7.5)))
#define TT_yinfty_f(V)     (1. / (1. + exp((V + 20.) / 7)))
#define TT_yinfty_f_2(V)  (0.67 / (1. + exp((V + 35.) / 7)) + 0.33)
#define TT_yinfty_f_Ca(CaSS)  (0.6 / (1 + (CaSS / 0.05) * (CaSS / 0.05)) + 0.4)

void MTenTusscher::ExpIntegratorUpdate(double dt,
                                       const std::vector<double> &var,
                                       std::vector<double> &g) const {
  if (!useExpInt)
    return;

  // This should not be here!
  double kcasr =
      max_sr - ((max_sr - min_sr) /
                (1.00000 + (EC / TT_Ca_SR(var)) * (EC / TT_Ca_SR(var))));  // x
  double k_2 = k_2prime * kcasr;                                           // x
  TT_R_quer(g) = TT_R_quer(var) + dt * (k_4 * (1 - TT_R_quer(var)) -
                                        k_2 * TT_Ca_SS(var) * TT_R_quer(var));

  double V = TT_Pot(var);
  TT_m(g) =
      (TT_yinfty_m(V) - (TT_yinfty_m(V) - TT_m(var)) * exp(-dt / TT_tau_m(V)));
  TT_h(g) =
      (TT_yinfty_h(V) - (TT_yinfty_h(V) - TT_h(var)) * exp(-dt / TT_tau_h(V)));
  TT_j(g) =
      (TT_yinfty_j(V) - (TT_yinfty_j(V) - TT_j(var)) * exp(-dt / TT_tau_j(V)));
  TT_x_r1(g) = (TT_yinfty_x_r1(V) -
                (TT_yinfty_x_r1(V) - TT_x_r1(var)) * exp(-dt / TT_tau_x_r1(V)));
  TT_x_r2(g) = (TT_yinfty_x_r2(V) -
                (TT_yinfty_x_r2(V) - TT_x_r2(var)) * exp(-dt / TT_tau_x_r2(V)));
  TT_x_s(g) = (TT_yinfty_x_s(V) -
               (TT_yinfty_x_s(V) - TT_x_s(var)) * exp(-dt / TT_tau_x_s(V)));
  TT_s(g) =
      (TT_yinfty_s(V) - (TT_yinfty_s(V) - TT_s(var)) * exp(-dt / TT_tau_s(V)));
  TT_r(g) =
      (TT_yinfty_r(V) - (TT_yinfty_r(V) - TT_r(var)) * exp(-dt / TT_tau_r(V)));
  TT_d(g) =
      (TT_yinfty_d(V) - (TT_yinfty_d(V) - TT_d(var)) * exp(-dt / TT_tau_d(V)));
  TT_f(g) =
      (TT_yinfty_f(V) - (TT_yinfty_f(V) - TT_f(var)) * exp(-dt / TT_tau_f(V)));
  TT_f_2(g) = (TT_yinfty_f_2(V) -
               (TT_yinfty_f_2(V) - TT_f_2(var)) * exp(-dt / TT_tau_f_2(V)));

  auto caSS = TT_Ca_SS(var);
  TT_f_Ca(g) = TT_yinfty_f_Ca(caSS) - (TT_yinfty_f_Ca(caSS) - TT_f_Ca(var)) *
                                          exp(-dt / TT_tau_f_Ca(caSS));
}

std::vector<double> MTenTusscher::calculateCurrents(
    const std::vector<double> &var) const {
  double V = TT_Pot(var);

  // reverse potentials
  double E_Ca = ((0.500000 * R * T) / F) * log(Ca_e / TT_Ca(var)); // x
  double E_Na = ((R * T) / F) * log(Na_e / TT_Na(var)); // x
  double E_K = ((R * T) / F) * log(K_e / TT_K(var)); // x
  double E_Ks = ((R * T / F) * log((K_e + p_KNa * Na_e) / (TT_K(var) + p_KNa * TT_Na(var)))); // x

  double alpha_K1 = 0.100000 / (1.00000 + exp(0.0600000 * (V - E_K - 200.000)));//x
  double beta_K1 = (3.00000 * exp(0.000200000 * (V - E_K + 100.000)) +
                    exp(0.100000 * (V - E_K - 10.0000))) /
                   (1.00000 + exp(-0.500000 * (V - E_K)));//x
  double x_K1infty = alpha_K1 / (alpha_K1 + beta_K1);//x
  double rec_iNaK = (1.0 / (1.0 + 0.1245 * exp(-0.1 * V * F / (R * T)) +
                            0.0353 * exp(-V * F / (R * T)))); // x

  double i_Na =
      (G_Na * TT_m(var) * TT_m(var) * TT_m(var) * TT_h(var) * TT_j(var)) * (V - E_Na);  //x
  double i_CaL = G_CaL * TT_d(var) * TT_f(var) * TT_f_2(var) * TT_f_Ca(var) * 4 * (V - V_mCaL) *
                 (F * F / (R * T)) *
                 (0.25 * TT_Ca_SS(var) * exp(2.0 * (V - V_mCaL) * F / (R * T)) - Ca_e) /
                 (exp(2.0 * (V - V_mCaL) * F / (R * T)) - 1.);
  double i_Ks = G_Ks * TT_x_s(var) * TT_x_s(var) * (V - E_Ks);//x
  double i_to = G_to * TT_r(var) * TT_s(var) * (V - E_K);//x
  double i_Kr = G_Kr * sqrt(K_e / 5.4) * TT_x_r1(var) * TT_x_r2(var) * (V - E_K);//x
  double i_K1 = G_K1 * x_K1infty * (V - E_K)*sqrt(K_e/5.4);//x
  double i_NaCa =
      k_NaCa * (1.0 / (K_mNai * K_mNai * K_mNai + Na_e * Na_e * Na_e)) * (1.0 / (K_mCa + Ca_e)) *
      (1.0 / (1 + k_sat * exp((gamma - 1) * V * F / (R * T)))) *
      (exp(gamma * V * F / (R * T)) * TT_Na(var) * TT_Na(var) * TT_Na(var) * Ca_e -
       exp((gamma - 1.0) * V * F / (R * T)) * Na_e * Na_e * Na_e * TT_Ca(var) * alpha); // x
  double i_NaK = 2.724 * (K_e / (K_e + K_mK)) * (TT_Na(var) / (TT_Na(var) + K_mNa)) * rec_iNaK; // x
  double i_pCa = (G_pCa * TT_Ca(var)) / (TT_Ca(var) + K_pCa);//x
  double i_pK = (G_pK * (V - E_K)) / (1.00000 + exp((V_mpK1 - V) / V_mpK2));//x
  double i_bCa = G_bCa * (V - E_Ca);//x
  double i_bNa = G_bNa * (V - E_Na);//x
  // hier fehlt noch die Definition von I_SAC

  return {i_Na, i_CaL, i_Ks, i_to, i_Kr, i_K1, i_NaCa, i_NaK, i_pCa, i_pK, i_bCa,
          i_bNa};  // hier fehlt noch I_SAC
}

double MTenTusscher::oQuer(const std::vector<double> &var) const {
  double kcasr =
      max_sr - ((max_sr - min_sr) / (1.00000 + (EC / TT_Ca_SR(var)) * (EC / TT_Ca_SR(var)))); // x
  double k_1 = k_1prime / kcasr; // x
  return k_1 * TT_Ca_SS(var) * TT_Ca_SS(var) * TT_R_quer(var) /
         (k_3 + k_1 * TT_Ca_SS(var) * TT_Ca_SS(var));
}

std::vector<double> MTenTusscher::Tau(const std::vector<double> &var) const {
  auto V = TT_Pot(var);
  return {
      TT_tau_m(V),
      TT_tau_h(V),
      TT_tau_j(V),
      TT_tau_x_r1(V),
      TT_tau_x_r2(V),
      TT_tau_x_s(V),
      TT_tau_s(V),
      TT_tau_r(V),
      TT_tau_d(V),
      TT_tau_f(V),
      TT_tau_f_2(V)  /*,
TT_tau_f_Ca(CaSS)*/
  };
}

std::vector<double> MTenTusscher::YInfty(const std::vector<double> &var) const {
  auto V = TT_Pot(var);
  return {
      TT_yinfty_m(V),
      TT_yinfty_h(V),
      TT_yinfty_j(V),
      TT_yinfty_x_r1(V),
      TT_yinfty_x_r2(V),
      TT_yinfty_x_s(V),
      TT_yinfty_s(V),
      TT_yinfty_r(V),
      TT_yinfty_d(V),
      TT_yinfty_f(V),
      TT_yinfty_f_2(V) /*,
TT_yinfty_f_Ca(CaSS)*/ };
}

void MTenTusscher::IIonUpdate(const std::vector<double> &var,
                              std::vector<double> &g, double iExt) const {
  auto currents = calculateCurrents(var);
  TT_Pot(g)=- TT_I_Na(currents)
              - TT_I_CaL(currents)
              - TT_I_Ks(currents)
              - TT_I_to(currents)
              - TT_I_Kr(currents)
              - TT_I_K1(currents)
              - TT_I_NaCa(currents)
              - TT_I_NaK(currents)
              - TT_I_pCa(currents)
              - TT_I_pK(currents)
              - TT_I_bCa(currents)
              - TT_I_bNa(currents);
  if(!iextInPDE){
    TT_Pot(g)+=iExt;
  }
}

void MTenTusscher::ConcentrationUpdate(const std::vector<double> &var,
                                       std::vector<double> &g,
                                       double iExt) const {
  auto ion = calculateCurrents(var);
  double I_leak = V_leak * (TT_Ca_SR(var) - TT_Ca(var)); // x
  double I_up = V_maxup / (1.00000 + ((K_up * K_up) / (TT_Ca(var) * TT_Ca(var)))); //x
  double I_rel = V_rel * oQuer(var) * (TT_Ca_SR(var) - TT_Ca_SS(var)); // x
  double I_xfer = V_xfer * (TT_Ca_SS(var) - TT_Ca(var)); // x


  TT_Ca(g) =
      ((-(TT_I_bCa(ion) + TT_I_pCa(ion) - 2 * TT_I_NaCa(ion)) * (1. / (2. * V_c * F)) *
        Capacitance) - (I_up - I_leak) * (V_SR / V_c) +
       I_xfer); // x
  TT_Ca_SS(g) = (-I_xfer * (V_c / V_SS) + I_rel * (V_SR / V_SS) +
                 (-TT_I_CaL(ion) * (1. / (2. * V_SS * F)) * Capacitance));
  // x
  TT_Ca_SR(g) = (I_up - I_rel - I_leak);
  TT_Na(g) =
      -(TT_I_Na(ion) + TT_I_bNa(ion) + 3. * TT_I_NaK(ion) + 3. * TT_I_NaCa(ion)) * 1. / (F * V_c) *
      Capacitance; // x
  TT_K(g) = -(TT_I_K1(ion) + TT_I_to(ion) + TT_I_Kr(ion) + TT_I_Ks(ion) - 2. * TT_I_NaK(ion) +
              TT_I_pK(ion) - iExt) * 1. /
            (F * V_c) * Capacitance; // x
}

void MTenTusscher::GatingUpdate(const std::vector<double> &var,
                                std::vector<double> &g) const {
  if (useExpInt)
    return;

  double V = TT_Pot(var);
  TT_m(g) = (TT_yinfty_m(V) - TT_m(var)) / TT_tau_m(V);
  TT_h(g) = (TT_yinfty_h(V) - TT_h(var)) / TT_tau_h(V);
  TT_j(g) = (TT_yinfty_j(V) - TT_j(var)) / TT_tau_j(V);
  TT_x_r1(g) = (TT_yinfty_x_r1(V) - TT_x_r1(var)) / TT_tau_x_r1(V);
  TT_x_r2(g) = (TT_yinfty_x_r2(V) - TT_x_r2(var)) / TT_tau_x_r2(V);
  TT_x_s(g) = (TT_yinfty_x_s(V) - TT_x_s(var)) / TT_tau_x_s(V);
  TT_s(g) = (TT_yinfty_s(V) - TT_s(var)) / TT_tau_s(V);
  TT_r(g) = (TT_yinfty_r(V) - TT_r(var)) / TT_tau_r(V);
  TT_d(g) = (TT_yinfty_d(V) - TT_d(var)) / TT_tau_d(V);
  TT_f(g) = (TT_yinfty_f(V) - TT_f(var)) / TT_tau_f(V);
  TT_f_2(g) = (TT_yinfty_f_2(V) - TT_f_2(var)) / TT_tau_f_2(V);
  TT_f_Ca(g) = (TT_yinfty_f_Ca(TT_Ca_SS(var)) - TT_f_Ca(var)) /
               TT_tau_f_Ca(TT_Ca_SS(var));

  double kcasr =
      max_sr - ((max_sr - min_sr) /
                (1.00000 + (EC / TT_Ca_SR(var)) * (EC / TT_Ca_SR(var))));  // x
  double k_2 = k_2prime * kcasr;                                           // x
  TT_R_quer(g) = k_4 * (1 - TT_R_quer(var)) -
                 k_2 * TT_Ca_SS(var) * TT_R_quer(var);  // von tentusscher // x

  /* Vorher:
   * double alpha_quer = k_4;
   * double beta_quer = k_2 * Ca_SS(var);
   * double y_infty_R = alpha_quer / (alpha_quer + beta_quer);
   * double TT_tau_R = 1 / (alpha_quer + beta_quer);
   * return y_infty_R + (R_quer - y_infty_R) * exp (-dt* TimeScale() *
   * 1/TT_tau_R);
   */
}

void MTenTusscher::UpdateValues(double dt, std::vector<double> &var,
                                const std::vector<double> &g) const {
  if (useExpInt) {
    TT_R_quer(var) = TT_R_quer(g);
  } else {
    TT_R_quer(var) += dt * TT_R_quer(g);
  }

  //Update concentrations
  double CaBuf = Bufc * TT_Ca(var) / (TT_Ca(var) + Kbufc);
  double dCa = dt * TT_Ca(g);
  double bc = Bufc - CaBuf - dCa - TT_Ca(var) + Kbufc;
  double cc = Kbufc * (CaBuf + dCa + TT_Ca(var));
  TT_Ca(var) = 0.5 * (sqrt(bc * bc + 4 * cc) - bc); //Ca

  double CaSSBuf = Bufss * TT_Ca_SS(var) / (TT_Ca_SS(var) + Kbufss);
  double dCaSS = dt * TT_Ca_SS(g);
  double bcss = Bufss - CaSSBuf - dCaSS - TT_Ca_SS(var) + Kbufss;
  double ccss = Kbufss * (CaSSBuf + dCaSS + TT_Ca_SS(var));
  TT_Ca_SS(var) = 0.5 * (sqrt(bcss * bcss + 4 * ccss) - bcss); //Ca_ss

  double CaCSQN = Bufsr * TT_Ca_SR(var) / (TT_Ca_SR(var) + Kbufsr);
  double dCaSR = dt * TT_Ca_SR(g);
  double bjsr = Bufsr - CaCSQN - dCaSR - TT_Ca_SR(var) + Kbufsr;
  double cjsr = Kbufsr * (CaCSQN + dCaSR + TT_Ca_SR(var));
  TT_Ca_SR(var) = 0.5 * (sqrt(bjsr * bjsr + 4 * cjsr) - bjsr);//Ca_SR

  TT_Na(var) += dt * TT_Na(g);
  TT_K(var) += dt * TT_K(g);

  // Update Hodgkin Huxley type equations
  if (useExpInt) {
    TT_m(var) = TT_m(g);
    TT_h(var) = TT_h(g);
    TT_j(var) = TT_j(g);
    TT_x_r1(var) = TT_x_r1(g);
    TT_x_r2(var) = TT_x_r2(g);
    TT_x_s(var) = TT_x_s(g);
    TT_s(var) = TT_s(g);
    TT_r(var) = TT_r(g);
    TT_d(var) = TT_d(g);
    TT_f(var) = TT_f(g);
    TT_f_2(var) = TT_f_2(g);
    TT_f_Ca(var) = TT_f_Ca(g);
  } else {
    TT_m(var) += dt * TT_m(g);
    TT_h(var) += dt * TT_h(g);
    TT_j(var) += dt * TT_j(g);
    TT_x_r1(var) += dt * TT_x_r1(g);
    TT_x_r2(var) += dt * TT_x_r2(g);
    TT_x_s(var) += dt * TT_x_s(g);
    TT_s(var) += dt * TT_s(g);
    TT_r(var) += dt * TT_r(g);
    TT_d(var) += dt * TT_d(g);
    TT_f(var) += dt * TT_f(g);
    TT_f_2(var) += dt * TT_f_2(g);
    TT_f_Ca(var) += dt * TT_f_Ca(g);
  }


  TT_Pot(var) += dt * TT_Pot(g);
}

double MTenTusscher::GetIion(const std::vector<double> &var) const {
  auto ionCurrents = calculateCurrents(var);
  return std::accumulate(ionCurrents.begin(), ionCurrents.end(), 0.0);
}
