#include <Vector.hpp>
#include "MRossi.hpp"

double MRossi::activeStressEvolution(double l, int N) const {
  double taylorSum = 0.0;
  if (l != 0.0) {
    for (int k = N; k > 0; k--) {
      taylorSum = (taylorSum + (2.0 * (0.5 - k % 2)) * (k + 1) * (k + 2)) * l;
    }

    /*for(int k=1;k<6;k++){
        taylorSum += (2.0 * (0.5-k%2)) * (k+1) * (k+2) * pow(l, float(k));
    }*/
  }
  return taylorSum;
}

double MRossi::forceLengthRelation(double stretch) const {
  //return (1-exp(-0.8*stretch));

  double l = stretch*initialLength;
  double flr = 0.0;
  if (l >= minLength && l <= maxLength) {
    flr = c[0] / 2.0;
    for (int i = 1; i < 4; i++) {
      flr += c[i] * sin(i * l);
      flr += d[i - 1] * cos(i * l);
    }
  }

  return flr;
}

void MRossi::Initialize(Vectors &vectors) {
  elphy->Initialize(vectors);
  R_gamma(vectors) = 0.0;
  R_force(vectors) = 0.0;

  std::vector<double> elphyValues(vectors.size());
  elphy->Initialize(elphyValues);
  restingCA = elphyValues[elphy->CalciumIndex()] * elphy->CAScale();
}

void MRossi::Initialize(std::vector<double> &vw) {
  elphy->Initialize(vw);
  R_gamma(vw) = 0.0;
  R_force(vw) = 0.0;

  restingCA = vw[elphy->CalciumIndex()] * elphy->CAScale();
}

void MRossi::TensionUpdate(const std::vector<double> &var,
                           std::vector<double> &g, double iExt) const {
  double gamma = R_gamma(var);
  double ca = var[elphy->CalciumIndex()] * elphy->CAScale();
  double iota = R_iota(var);

  double activeForce =
      -abs((ca > restingCA) * //-abs?
           sarcomereForce * (ca - restingCA) * (ca - restingCA) *
           forceLengthRelation(sqrt(iota)));


  double initialStress = iota * activeStressEvolution(gamma);

  //mout << OUT(ca) << endl << activeForce + initialStress << endl;
  double cubicLength = (1.0 + gamma) * (1.0 + gamma) * (1.0 + gamma);
  double gshift = (1 / (viscosity * ca * ca)) *
                  (activeForce + initialStress);
      //(0.001 / (viscosity * ca * ca)) *
        //          (activeForce + initialStress);

  R_gamma(g) = gshift;// (2. *iota / cubicLength) - 2. * iota);
  R_force(g) = activeForce;
}

void MRossi::UpdateValues(double dt, std::vector<double> &var,
                          const std::vector<double> &g) const {
  elphy->UpdateValues(dt, var, g);
  R_gamma(var) += dt * R_gamma(g);
  R_force(var) += dt * R_force(g);
}

double MRossi::initialStretch() const {
  return 0.0;
}
