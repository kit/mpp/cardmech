#ifndef MFITZHUGHNAGUMO_HPP
#define MFITZHUGHNAGUMO_HPP

#include "MCellModel.hpp"

class MFitzHughNagumo : public MElphyModel {

  double a = 0.13;
  double b = 0.013;
  double c1 = 0.26;
  double c2 = 0.1;
  double c3 = 1.0;
protected:
  double initialPotential() const override {
    return 0;
  };

public:
  explicit MFitzHughNagumo(std::array<double, 5> params = {0.13, 0.013, 0.26, 0.1, 1.0})
      : MElphyModel(2, 1000.0, 1), a(params[0]), b(params[1]), c1(params[2]), c2(params[3]),
        c3(params[4]) {
    amplitude = 325;
  }

  int CalciumIndex() const override;

  int GatingIndex() const override;

  void Initialize(Vectors &VW) override;

  void Initialize(std::vector<double> &vw) override;

  void IIonUpdate(const std::vector<double> &var, std::vector<double> &g,
                  double iExt) const override;

  void ConcentrationUpdate(const std::vector<double> &var,
                           std::vector<double> &g, double iExt) const override;

  void UpdateValues(double dt, std::vector<double> &var,
                    const std::vector<double> &g) const override;

  std::vector<double> Tau(const std::vector<double> &var) const override;

  std::vector<double> YInfty(const std::vector<double> &var) const override;
};

#endif //MFITZHUGHNAGUMO_HPP
