//#include "Parameter.hpp"
//PrintParameterModus PrintParameterMode = PrintParameterModeOff;
/*
#include <iostream>
#include "Parameter.hpp"


int main(int argc, char **argv) {
    Parameter myParam{};
    std::cout << myParam.name<< std::endl;
}
*/
#include "electrophysiology/MainMonodomain.hpp"
#include "electrophysiology/MainOneCell.hpp"


int main(int argc, char **argv) {
  Config::SetGeoPath(std::string(ProjectSourceDir) + "/geo/");
  Config::SetConfPath(std::string(ProjectSourceDir) + "/conf/");
  Config::SetConfigFileName("m++.conf");
  Mpp::initialize(&argc, argv);

  Config::PrintInfo();

  mout << "Starting Electrophysiology Suite of M++." << endl;

  bool clear = false;
  Config::Get("ClearData", clear);
  if (clear && PPM->master()) {
    int cleared = system("exec rm -rf data/vtu/*");
  }

  Date Start;

  std::string model = "";
  Config::Get("Model", model);

  MainCardMech *cmMain;
  if (model == "") {
    THROW("No Model found")
  } else if (model == "OneCell") {
    cmMain = new MainOneCell();
  } else {
    cmMain = new MainMonodomain();
  }
  //alter Code:
  /*if (model == "MonoDomainBM" || model == "MonoDomain" || model == "MonodomainGodunovReassembleOnce") {
    cmMain = new MainMonodomain();
  } else {
    THROW("No Model named " + model)
  }
*/
  cmMain->Initialize();
  auto solution = cmMain->Run();
  cmMain->Evaluate(solution);

  mout << "Finished Calculations after " << Date() - Start << " seconds" << endl;

  delete cmMain;
  return 0;
}
