#ifndef CARDMECH_CARDMECHIPROBLEM_H
#define CARDMECH_CARDMECHIPROBLEM_H

#include "Config.hpp"
#include "Meshes.hpp"
#include "MeshesCreator.hpp"
#include <memory>

class CardMechIProblem {
protected:
  int verbose = 1;

  std::string meshesName = "";

  std::shared_ptr<Meshes> meshes = nullptr;

  std::shared_ptr<CoarseGeometry> cGeo = nullptr;

public:
  explicit CardMechIProblem(std::string meshName = "") : meshesName(meshName) {
    if (meshesName.empty())
      Config::Get("Mesh", meshesName);
    if (!meshesName.empty()) {
      CreateMeshes(MeshesCreator(meshesName));
    }
    Config::Get("ProblemVerbose", verbose);
  }

  explicit CardMechIProblem(std::shared_ptr<Meshes> meshes)
      : meshes(std::move(meshes)) {
    Config::Get("ProblemVerbose", verbose);
  }

  explicit CardMechIProblem(CardMechIProblem &otherProblem)
      : meshes(otherProblem.meshes), verbose(otherProblem.verbose) {}

  virtual ~CardMechIProblem() = default;

  virtual bool HasExactSolution() const { return false; }

  void CreateMeshes(MeshesCreator meshesCreator) {
    std::string injectedMeshName = meshesCreator.GetMeshName();
    cGeo = CreateCoarseGeometryShared(
        injectedMeshName.empty() ? meshesName : injectedMeshName);
    meshes = meshesCreator.WithCoarseGeometry(cGeo).CreateShared();
  }

  const CoarseGeometry &Domain() const { return *cGeo; };

  const Meshes &GetMeshes() const {
    if (meshes == nullptr)
      Exit("Meshes not initialized") return *meshes;
  }

  bool IsMeshInitialized() const { return meshes != nullptr; }

  const Mesh &GetMesh(int level) const { return (*meshes)[level]; }

  const Mesh &GetMesh(MeshIndex level) const { return (*meshes)[level]; }

  virtual std::string Name() const = 0;
};

#endif // CARDMECH_CARDMECHIPROBLEM_H
