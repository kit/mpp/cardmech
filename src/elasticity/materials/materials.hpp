#ifndef MATERIALS_HPP
#define MATERIALS_HPP

#include "Tensor.hpp"

namespace mat {
  constexpr double cutexp(double argument, double max = 35) {
    if (argument<max) return exp(argument);
    return exp(max) * (1 + argument-max);
  }

  static Tensor cofactor(const Tensor &F) {
    return 0.5 * Cross(F, F);
  }

  static Tensor d_cofactor(const Tensor &F, const Tensor &Du) {
    return Cross(F, Du);
  }

  static Tensor dd_cofactor(const Tensor &F, const Tensor &Du, const Tensor &Dv) {
    return Cross(Du, Dv);
  }

  static double d_det(const Tensor &F, const Tensor &Du) {
    return Frobenius(cofactor(F), Du);
  }

  static double dd_det(const Tensor &F, const Tensor &Du, const Tensor &Dv) {
    return Frobenius(F, Cross(Du, Dv));
  }

  static double macaulay(double value) {
    return (value > 0) * value;
  }

  static double
  invariant(int k, const Tensor &F, const VectorField &f = zero, const VectorField &l = zero) {
    switch (k) {
      case 1:
        return Frobenius(F, F);
      case 2:
        return Frobenius(cofactor(F), cofactor(F));
      case 3:
        return det(F);
      case 4:
        return (F * f) * (F * f);
      case 8:
        return abs((F * f) * (F * l));
      default:
        return 1.0;
    }
  }

  static Tensor
  dinvariant(int k, const Tensor &F, const VectorField &f = zero, const VectorField &l = zero) {
    switch (k) {
      case 1:
        return 2.0 * F;
      case 2:
        return invariant(1, F) * dinvariant(1, F) - 4.0 * F * transpose(F) * F;
      case 3:
        return cofactor(F);
      case 4:
        return 2.0 * F * Product(f, f);
      case 8:
        return F * (Product(f, l) + Product(l, f));
      default:
        return 1.0;
    }
  }

  static Tensor
  ddinvariant(int k, const Tensor &F, const Tensor &H, const VectorField &f = zero,
              const VectorField &l = zero) {
    switch (k) {
      case 1:
        return 2.0 * H;
      case 2:
        return dinvariant(1, F) * dinvariant(1, F) + invariant(1, F) * ddinvariant(1, F, H)
               - 4.0 * (F * transpose(F) * H + F * transpose(H) * F + H * transpose(F) * F);
      case 3:
        // https://math.stackexchange.com/questions/2136747/derivate-of-the-cofactor-and-the-determinant
        return transposeInvert(F) * (Frobenius(cofactor(F), H) * One - transpose(H) * cofactor(F));
      case 4:
        return 2.0 * H * Product(f, f);
      case 8:
        return H * (Product(f, l) + Product(l, f));
      default:
        return 1.0;
    }
  }

  //decides which type for the active deformation in the active strain approach will be used
  static Tensor active_deformation(const Tensor &Q, const VectorField &gamma) {
    std::string name;
    Config::Get("activeDeformation", name, true);
    Tensor AD{};
    if (gamma[0] == 0.0)
      return One;

    if (name == "Quarteroni") {
      AD = gamma[0] * Product(Q[0], Q[0])
           + 1 / sqrt(gamma[0]) * (One - Product(Q[0], Q[0]));
      return AD;
    } else if (name == "") {
      AD = One + gamma[0] * Product(Q[0], Q[0])
           + gamma[1] * Product(Q[1], Q[1])
           + gamma[2] * Product(Q[2], Q[2]);
      return AD;
    }
    return AD;
  }


  static Tensor inverse_active_deformation(const Tensor &Q, const VectorField &gamma) {
    std::string name;
    Config::Get("activeDeformation", name, true);
    double scale_gamma = 1;
    Config::Get("scale_gamma", scale_gamma, true);

    Tensor AD{};
    if (gamma[0] == 0.0)
      return One;

    if (name == "Quarteroni") {
      AD = 1 / gamma[0] * Product(Q[0], Q[0])
           + sqrt(gamma[0]) * (One - Product(Q[0], Q[0]));
      return AD;
    } else if (name == "Quarteroni2") {
      if (1+gamma[0] < 0)
	return One;
      AD = (1 / (1+gamma[0])) * Product(Q[0], Q[0])
           + scale_gamma * sqrt(1+gamma[0]) * Product(Q[1], Q[1])
           + (sqrt(1+gamma[0]) / scale_gamma) * Product(Q[2], Q[2]);
      return AD;
    } else if (name == "") {
      AD = One - gamma[0] / (1 + gamma[0]) * Product(Q[0], Q[0])
           - gamma[1] / (1 + gamma[1]) * Product(Q[1], Q[1])
           - gamma[2] / (1 + gamma[2]) * Product(Q[2], Q[2]);
      return AD;
    }
    return AD;
  }
}


#endif //MATERIALS_HPP
