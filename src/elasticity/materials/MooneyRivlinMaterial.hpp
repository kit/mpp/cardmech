#ifndef MOONEYRIVLINMATERIAL_HPP
#define MOONEYRIVLINMATERIAL_HPP

#include "Material.hpp"

class MooneyRivlinMaterial : public HyperelasticMaterial {
  double lambda() const { return GetParameter(0); }

  double alpha() const { return GetParameter(1); }

  double beta() const { return GetParameter(2); }


  double gammaFunction(double J) const;;

  double gammaDerivative(double J) const;;

  double gammaSecondDerivative(double J) const;;

public:
  MooneyRivlinMaterial() noexcept: HyperelasticMaterial("MooneyRivlin", 2) {
    Config::Get("MooneyRivlin_Alpha", parameters[0]);
    Config::Get("MooneyRivlin_Beta", parameters[1]);
    Config::Get("MooneyRivlin_Lambda", parameters[1]);
  }

  explicit MooneyRivlinMaterial(const std::vector<double> &params) : HyperelasticMaterial("MooneyRivlin",
                                                                                          params) {};

  explicit MooneyRivlinMaterial(std::vector<double> &&params) : HyperelasticMaterial("MooneyRivlin",
                                                                                     std::move(params)) {};


  Tensor FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const override;

  double IsotropicEnergy(const Tensor &F) const override;

  double IsotropicDerivative(const Tensor &F, const Tensor &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const override;

  int ParameterCount() const override { return 2; }
};

#endif //CARDMECH_MOONEYRIVLINMATERIAL_HPP
