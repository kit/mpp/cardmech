#include "MooneyRivlinMaterial.hpp"


double MooneyRivlinMaterial::gammaFunction(double J) const {
  return -4 * beta() * log(J) - 2 * alpha() * log(J) + lambda() / 40.0 * (pow(J, 20) - pow(J, -20));
  return 0.5 * lambda() * (J - 1) * (J - 1);
}

double MooneyRivlinMaterial::gammaDerivative(double J) const {
  return -4 * beta() / J - 2 * alpha() / J + lambda() / 40.0 * (20 * pow(J, 19) + 0.05 * pow(J, -21));
  return lambda() * (J - 1);
}

double MooneyRivlinMaterial::gammaSecondDerivative(double J) const {
  double JJ = J * J;
  return 4 * beta() / JJ + 2 * alpha() / JJ +
         lambda() / 40.0 * (20 * 19 * pow(J, 18) - 0.05 * 21 * pow(J, -22));
  return lambda();
}

Tensor MooneyRivlinMaterial::FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso,
                                                 const Tensor &Q) const {
  return Zero;
}

double MooneyRivlinMaterial::IsotropicEnergy(const Tensor &F) const {
  Tensor H = mat::cofactor(F);
  return alpha() * (Frobenius(F, F) - 3) + beta() * Frobenius(H, H) +
         lambda() * gammaFunction(det(F));

  //return EnergyFunctional(F);
}

double
MooneyRivlinMaterial::IsotropicDerivative(const Tensor &F, const Tensor &Du) const {
  Tensor H = mat::cofactor(F);
  return 2.0 * alpha() * Frobenius(F, Du)
         + 2.0 * beta() * Frobenius(Cross(H, F), Du)
         + gammaDerivative(det(F)) * mat::d_det(F, Du);
}

double
MooneyRivlinMaterial::IsotropicSecondDerivative(const Tensor &F, const Tensor &Du,
                                                const Tensor &Dv) const {
  Tensor H = mat::cofactor(F);
  return 2.0 * alpha() * Frobenius(Du, Dv)
         + 2.0 * beta() * Frobenius(Cross(Du, F), Cross(Dv, F))
         + 2.0 * beta() * Frobenius(H, Cross(Du, Dv))
         + gammaSecondDerivative(det(F)) * mat::d_det(F, Du) * mat::d_det(F, Dv)
         + gammaDerivative(det(F)) * Frobenius(F, Cross(Du, Dv));
}
