add_library(MATERIALS STATIC
        Material.cpp
        LaplaceMaterial.cpp
        LinearMaterial.cpp
        NeoHookeMaterial.cpp
        GuccioneMaterial.cpp
        HolzapfelMaterial.cpp
        StVenantKirchhoffMaterial.cpp
        MooneyRivlinMaterial.cpp)

target_link_libraries(MATERIALS CARDMECH_LIBRARIES)