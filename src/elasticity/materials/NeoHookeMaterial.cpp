#include "NeoHookeMaterial.hpp"


Tensor
NeoHookeMaterial::FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const {
  return mu() * sym(Fiso);
}

Tensor
NeoHookeMaterial::constitutiveMatrix(const Tensor &F, const Tensor &Q, const Tensor &H) const {
  return FirstPiolaKirchhoff(H, H, Q);
}

double NeoHookeMaterial::IsotropicEnergy(const Tensor &F) const {
  return mu() * 0.5 * (Frobenius(F, F) - 3);
}

double NeoHookeMaterial::IsotropicDerivative(const Tensor &F, const Tensor &H) const {
  return mu() * Frobenius(F, H);
}

double
NeoHookeMaterial::IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const {
  return mu() * Frobenius(H, G);
}


