#ifndef LINEARMATERIAL_H
#define LINEARMATERIAL_H

#include "Material.hpp"


class LinearMaterial : public HyperelasticMaterial {
  double lambda() const { return GetParameter(0); }

  double mu() const { return GetParameter(1); }

  std::pair<size_t, size_t> GetMaxParameterRange() const override {
      // start, length
      return {0, 2};
  }


  double energy(const Tensor &e_u) const;

  Tensor firstPK(const Tensor &e_u) const;

  Tensor constitutiveMatrix(const Tensor &e_u, const Tensor &e_v) const;


public:
  LinearMaterial() noexcept: HyperelasticMaterial("Linear", 2) {
    Config::Get("LinearMat_Lambda", parameters[0]);
    Config::Get("LinearMat_Mu", parameters[1]);
  }

  explicit LinearMaterial(const std::vector<double> &params) : HyperelasticMaterial("Linear", params) {};

  explicit LinearMaterial(std::vector<double> &&params) : HyperelasticMaterial("Linear",
                                                                               std::move(params)) {};

  Tensor FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const override;

  double IsotropicEnergy(const Tensor &F) const override;

  double IsotropicDerivative(const Tensor &F, const Tensor &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const override;

  double IsotropicDerivative(const Tensor &F, const TensorRow &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const TensorRow &H, const TensorRow &G) const override;

  int ParameterCount() const override { return 2; }
};

#endif //LINEARMATERIAL_H
