#include "LaplaceMaterial.hpp"
#include "LinearMaterial.hpp"


double LaplaceMaterial::energy(const Tensor &Du) const {
  return 0.5 * Frobenius(permeability * Du, Du);
}

Tensor LaplaceMaterial::firstPK(const Tensor &Du) const {
  return permeability * Du;
}

Tensor
LaplaceMaterial::constitutiveMatrix(const Tensor &Du, const Tensor &H) const {
  return permeability * H;
}

Tensor
LaplaceMaterial::constitutiveMatrix(const Tensor &Du, const TensorRow &H) const {
  return permeability * H;
}


Tensor
LaplaceMaterial::FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const {
  return firstPK(Fiso - One);
}

double LaplaceMaterial::IsotropicEnergy(const Tensor &F) const {
  return energy(F - One);
}

double LaplaceMaterial::IsotropicDerivative(const Tensor &F, const Tensor &H) const {
  return Frobenius(firstPK(F - One), H);
}

double
LaplaceMaterial::IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const {
  return Frobenius(constitutiveMatrix(F - One, H), G);
}

double LaplaceMaterial::IsotropicDerivative(const Tensor &F, const TensorRow &H) const {
  return Frobenius(firstPK(F - One), H);
}

double LaplaceMaterial::IsotropicSecondDerivative(const Tensor &F, const TensorRow &H,
                                                  const TensorRow &G) const {
  return Frobenius(constitutiveMatrix(F - One, H), G);
}


