#include "LinearMaterial.hpp"


double LinearMaterial::energy(const Tensor &e_u) const {
  Tensor S = firstPK(e_u);
  return 0.5 * Frobenius(S, e_u);
}

Tensor LinearMaterial::firstPK(const Tensor &e_u) const {
  return 2.0* mu() * e_u + lambda() * trace(e_u) * One;
}

Tensor
LinearMaterial::constitutiveMatrix(const Tensor &e_u, const Tensor &e_v) const {
  return firstPK(e_v);
}


Tensor
LinearMaterial::FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const {
  return firstPK(sym(Fiso - One));
}

double LinearMaterial::IsotropicEnergy(const Tensor &F) const {
  return energy(sym(F - One));
}

double LinearMaterial::IsotropicDerivative(const Tensor &F, const Tensor &H) const {
  return Frobenius(firstPK(sym(F - One)), sym(H));
}

double
LinearMaterial::IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const {
  return Frobenius(constitutiveMatrix(sym(F - One), sym(H)), sym(G));
}

double LinearMaterial::IsotropicDerivative(const Tensor &F, const TensorRow &H) const {
  return Frobenius(firstPK(sym(F - One)), sym(H));
}

double LinearMaterial::IsotropicSecondDerivative(const Tensor &F, const TensorRow &H,
                                                 const TensorRow &G) const {
  return Frobenius(constitutiveMatrix(sym(F - One), sym(H)), sym(G));
}
