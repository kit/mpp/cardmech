#ifndef STVENANTKIRCHHOFFMATERIAL_H
#define STVENANTKIRCHHOFFMATERIAL_H

#include "Material.hpp"


class StVenantKirchhoffMaterial : public HyperelasticMaterial {
  double lambda() const { return GetParameter(0); }

  double mu() const { return GetParameter(1); }

  double energy(const Tensor &E) const;

  Tensor firstPK(const Tensor &E) const;

  Tensor constitutiveMatrix(const Tensor &E, const Tensor &H) const;

public:
  StVenantKirchhoffMaterial() noexcept: HyperelasticMaterial("StVenantKirchhoff", 2) {
    Config::Get("StVenantMat_Lambda", parameters[0]);
    Config::Get("StVenantMat_Mu", parameters[1]);
  }

  explicit StVenantKirchhoffMaterial(const std::vector<double> &params) : HyperelasticMaterial("StVenantKirchhoff",
                                                                                               params) {};

  explicit StVenantKirchhoffMaterial(std::vector<double> &&params) : HyperelasticMaterial("StVenantKirchhoff",
                                                                                          std::move(params)) {};


  Tensor FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const override;

  double IsotropicEnergy(const Tensor &F) const override;

  double IsotropicDerivative(const Tensor &F, const Tensor &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const override;

  int ParameterCount() const override { return 2; }
};

#endif //STVENANTKIRCHHOFFMATERIAL_H
