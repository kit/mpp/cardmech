#ifndef OGDENMATERIAL_H
#define OGDENMATERIAL_H

#include "Material.hpp"

/*
 * This Holzapfel Model disregards I_8, as we only consider orthotropic materials, i.e. f*s = 0 for all x.
 * Literature suggests to use I_8 = f * C * s insted of (f*s) * (f*C*s).
 * However, as for now, we don't want to handle the following loss of ellipticity of DDW.
 * See i.e. doi:10.1016/j.ijnonlinmec.2006.02.001
 */
class HolzapfelMaterial : public HyperelasticMaterial {
  double exp_max = 10;

  double a() const { return GetParameter(0); }

  double af() const { return GetParameter(1); }

  double as() const { return GetParameter(2); }

  double afs() const { return GetParameter(3); }

  double b() const { return GetParameter(4); }

  double bf() const { return GetParameter(5); }

  double bs() const { return GetParameter(6); }

  double bfs() const { return GetParameter(7); }

  std::pair<size_t, size_t> GetMaxParameterRange() const override{
    // start, length
    return {0, 4};
  }

  /// Returns d if d is positive and 0 instead.
  double macaulay(double d) const { return (d > 0 ? d : 0.0); };


  double energyIso(const Tensor &C) const;

  Tensor firstPKIso(const Tensor &C) const;

  Tensor constitutiveMatrixIso(const Tensor &C, const Tensor &H) const;

  double energyAniso(const Tensor &C, const Tensor &Q) const;

  Tensor firstPKAniso(const Tensor &C, const Tensor &Q) const;

  Tensor constitutiveMatrixAniso(const Tensor &C, const Tensor &Q, const Tensor &H) const;

public:
  HolzapfelMaterial() noexcept: HyperelasticMaterial("Holzapfel", 8) {
    Config::Get("HolzapfelMat_a", parameters[0]);
    Config::Get("HolzapfelMat_af", parameters[1]);
    Config::Get("HolzapfelMat_as", parameters[2]);
    Config::Get("HolzapfelMat_afs", parameters[3]);
    Config::Get("HolzapfelMat_b", parameters[4]);
    Config::Get("HolzapfelMat_bf", parameters[5]);
    Config::Get("HolzapfelMat_bs", parameters[6]);
    Config::Get("HolzapfelMat_bfs", parameters[7]);
    Config::Get("exp_max", exp_max);
  }

  explicit HolzapfelMaterial(const std::vector<double> &params) : HyperelasticMaterial("Holzapfel", params) {
    Config::Get("exp_max", exp_max);
  };

  explicit HolzapfelMaterial(std::vector<double> &&params) : HyperelasticMaterial("Holzapfel",
                                                                                  std::move(params)) {
    Config::Get("exp_max", exp_max);
  };


  Tensor FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const override;

  double IsotropicEnergy(const Tensor &F) const override;

  double IsotropicDerivative(const Tensor &F, const Tensor &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const override;


  double AnisotropicEnergy(const Tensor &F, const Tensor &Q) const override;

  double AnisotropicDerivative(const Tensor &F, const Tensor &Q, const Tensor &H) const override;

  double
  AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const Tensor &H,
                            const Tensor &G) const override;



  double IsotropicDerivative(const Tensor &F, const TensorRow &H) const override;
  double IsotropicSecondDerivative(const Tensor &F,  const TensorRow &H, const TensorRow &G) const override;



  double AnisotropicDerivative(const Tensor &F, const Tensor &Q, const TensorRow &H) const override;

  double AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const TensorRow &H, const TensorRow &G) const override;

  int ParameterCount() const override { return 8; }
};

#endif //OGDENMATERIAL_H
