#include "HolzapfelMaterial.hpp"

double HolzapfelMaterial::energyIso(const Tensor &C) const {
  double I_1 = trace(C);
  return 0.5 * a() / b() * (mat::cutexp(b() * (I_1 - 3),exp_max) - 1);
}

Tensor HolzapfelMaterial::firstPKIso(const Tensor &C) const {
  double I_1 = trace(C);
  Tensor Psi = 0.5 * a() * (mat::cutexp(b() * (I_1 - 3),exp_max)) * One;
  return Psi;
}

Tensor
HolzapfelMaterial::constitutiveMatrixIso(const Tensor &C, const Tensor &H) const {
  double I_1 = trace(C);
  Tensor Psi = 0.5 * a() * b()* (mat::cutexp(b() * (I_1 - 3),exp_max)) * trace(H) * One;
  return Psi;
}

double HolzapfelMaterial::energyAniso(const Tensor &C, const Tensor &Q) const {
  double I_4f = macaulay(Q[0] * (C * Q[0]) - 1.0);
  double energy = af() / (2.0 * bf()) * (mat::cutexp(bf() * I_4f * I_4f,exp_max) - 1);

  double I_4s = macaulay(Q[1] * (C * Q[1]) - 1.0);
  energy += as() / (2.0 * bs()) * (mat::cutexp(bs() * I_4s * I_4s,exp_max) - 1);

  double I_8fs = Q[0] * (C * Q[1]);
  energy += afs() / (2.0 * bfs()) * (mat::cutexp(bfs() * I_8fs * I_8fs,exp_max) - 1);

  return energy;
}

Tensor HolzapfelMaterial::firstPKAniso(const Tensor &C, const Tensor &Q) const {
  double I_4f = macaulay(Q[0] * (C * Q[0]) - 1.0);
  Tensor Psi = af() * mat::cutexp(bf() * I_4f * I_4f,exp_max) * I_4f * Product(Q[0], Q[0]);

  double I_4s = macaulay(Q[1] * (C * Q[1]) - 1.0);
  Psi += as() * mat::cutexp(bs() * I_4s * I_4s,exp_max) * I_4s * Product(Q[1], Q[1]);

  double I_8fs = Q[0] * (C * Q[1]);
  Psi += afs() * mat::cutexp(bfs() * I_8fs * I_8fs,exp_max) * I_8fs * sym(Product(Q[0], Q[1]));

  return Psi;
}

Tensor
HolzapfelMaterial::constitutiveMatrixAniso(const Tensor &C, const Tensor &Q, const Tensor &H) const {
  double I_4f = macaulay(Q[0] * (C * Q[0]) - 1.0);
  double Hf = Q[0] * (H * Q[0]);
  Tensor Psi = af() * mat::cutexp(bf() * I_4f * I_4f,exp_max) * Hf * (2 * bf() * I_4f * I_4f + 1) * Product(Q[0], Q[0]);

  double I_4s = macaulay(Q[1] * (C * Q[1]) - 1.0);
  double Hs = Q[1] * (H * Q[1]);
  Psi += as() * mat::cutexp(bs() * I_4s * I_4s,exp_max) * Hs * (2 * bs() * I_4s * I_4s + 1) * Product(Q[1], Q[1]);

  double I_8fs = Q[0] * (C * Q[1]);
  double Hfs = 0.5 * (Q[0] * (H * Q[1]) + Q[1] * (H * Q[0]));
  Psi += afs() * mat::cutexp(bfs() * I_8fs * I_8fs,exp_max) * Hfs * (2 * bfs() * I_8fs * I_8fs + 1) * sym(Product(Q[0], Q[1]));

  return Psi;
}


Tensor
HolzapfelMaterial::FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const {
  Tensor Ciso = transpose(Fiso) * Fiso;
  Tensor C = transpose(F) * F;
  return 2 * (Fiso * firstPKIso(Ciso) + F * firstPKAniso(C, Q));
}


double HolzapfelMaterial::IsotropicEnergy(const Tensor &F) const {
  Tensor C = transpose(F) * F;
  return energyIso(C);
}

double
HolzapfelMaterial::IsotropicDerivative(const Tensor &F, const Tensor &H) const {
  Tensor C = transpose(F) * F;
  return 2 * Frobenius(F * firstPKIso(C), H);
}

double
HolzapfelMaterial::IsotropicSecondDerivative(const Tensor &F, const Tensor &H,
                                             const Tensor &G) const {
  Tensor FT = transpose(F);
  Tensor C = FT * F;

  return 2 * Frobenius(firstPKIso(C), sym(transpose(G) * H)) +
         4 * Frobenius(constitutiveMatrixIso(C, sym(FT*H)), sym(FT * G));
}


double HolzapfelMaterial::AnisotropicEnergy(const Tensor &F, const Tensor &Q) const {
  Tensor C = transpose(F) * F;
  return energyAniso(C, Q);
}

double HolzapfelMaterial::AnisotropicDerivative(const Tensor &F, const Tensor &Q,
                                                    const Tensor &H) const {
  Tensor C = transpose(F) * F;
  return 2 * Frobenius(F * firstPKAniso(C, Q), H);
}

double HolzapfelMaterial::AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q,
                                                          const Tensor &H, const Tensor &G) const {
  Tensor FT = transpose(F);
  Tensor C = FT * F;

  return 2 * Frobenius(firstPKAniso(C, Q), sym(transpose(G) * H)) +
         4 * Frobenius(constitutiveMatrixAniso(C, Q, sym(FT*H)), sym(FT * G));
}


double HolzapfelMaterial::IsotropicDerivative(const Tensor &F, const TensorRow &H) const {
  Tensor C = transpose(F) * F;
  return 2 * Frobenius(F * firstPKIso(C), H);
}

double HolzapfelMaterial::IsotropicSecondDerivative(const Tensor &F, const TensorRow &H,
                                                    const TensorRow &G) const {
  Tensor FT = transpose(F);
  Tensor C = FT * F;

  return 2 * Frobenius(firstPKIso(C), sym(ColRowProduct(G, H))) +
         4 * Frobenius(constitutiveMatrixIso(C, sym(FT*H)), sym(FT * G));
}
double HolzapfelMaterial::AnisotropicDerivative(const Tensor &F, const Tensor &Q,
                                                const TensorRow &H) const {
  Tensor C = transpose(F) * F;
  return 2 * Frobenius(F * firstPKAniso(C, Q), H);
}


double
HolzapfelMaterial::AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const TensorRow &H,
                                               const TensorRow &G) const {
  Tensor FT = transpose(F);
  Tensor C = FT * F;

  return 2 * Frobenius(firstPKAniso(C, Q), sym(ColRowProduct(G, H))) +
         4 * Frobenius(constitutiveMatrixAniso(C, Q, sym(FT*H)), sym(FT * G));
}

