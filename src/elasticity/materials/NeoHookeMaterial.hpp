#ifndef NEOHOOKEMATERIAL_HPP
#define NEOHOOKEMATERIAL_HPP

#include "Material.hpp"

class NeoHookeMaterial : public HyperelasticMaterial {
  double lambda() const { return GetParameter(0); }

  double mu() const { return GetParameter(1); }

  Tensor constitutiveMatrix(const Tensor &F, const Tensor &Q, const Tensor &H) const;

public:
  NeoHookeMaterial() noexcept: HyperelasticMaterial("NeoHooke", 2) {
    Config::Get("NeoHooke_Lambda", parameters[0]);
    Config::Get("NeoHooke_Mu", parameters[1]);
  }

  explicit NeoHookeMaterial(const std::vector<double> &params) : HyperelasticMaterial("NeoHooke",
                                                                                      params) {};

  explicit NeoHookeMaterial(std::vector<double> &&params) : HyperelasticMaterial("NeoHooke",
                                                                                 std::move(params)) {};


  Tensor FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const override;

  double IsotropicEnergy(const Tensor &F) const override;

  double IsotropicDerivative(const Tensor &F, const Tensor &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const override;

  int ParameterCount() const override { return 2; }

};

#include "Material.hpp"


#endif //NEOHOOKEMATERIAL_HPP
