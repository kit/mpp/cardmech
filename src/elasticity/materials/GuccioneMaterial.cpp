#include "GuccioneMaterial.hpp"


double GuccioneMaterial::energy(const Tensor &E) const {
  Tensor G = fiberWeights(E);
  return 0.5 * C() * (exp(Frobenius(G, E)) - 1);
}


Tensor GuccioneMaterial::firstPK(const Tensor &E) const {
  Tensor G = fiberWeights(E);
  Tensor DPsi = C() * exp(Frobenius(G, E)) * G;
  return DPsi;
}

Tensor
GuccioneMaterial::constitutiveMatrix(const Tensor &E, const Tensor &H) const {
  Tensor G = fiberWeights(E);
  double expQ = exp(Frobenius(G, E));

  Tensor DDPsi = trace(G * H) * G;
  return 0.5 * C() * expQ * DDPsi;
}

double
GuccioneMaterial::secondDerivative(const Tensor &E, const Tensor &H, const Tensor& K) const {
  Tensor G = fiberWeights(E);
  double expQ = exp(Frobenius(G, E));

  double ddPsi = Frobenius(G, H) * Frobenius(G, K) + Frobenius(H, fiberWeights(K));
  return 0.5 * C() * expQ * ddPsi;
}


Tensor
GuccioneMaterial::FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const {
  Tensor FQ = Q * F * transpose(Q);
  Tensor FT = transpose(FQ);
  Tensor E = 0.5 * (FT * FQ - One);
  return F * firstPK(E);
}

double GuccioneMaterial::IsotropicEnergy(const Tensor &F) const {
  return 0.0;
}

double GuccioneMaterial::IsotropicDerivative(const Tensor &F, const Tensor &H) const {
  return 0.0;
}

double
GuccioneMaterial::IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const {
  return 0.0;
}

double GuccioneMaterial::AnisotropicEnergy(const Tensor &F, const Tensor &Q) const {
  // Q ist the matrix of fibre directions, i.e. f=Q[0].
  // The Rotation Matrix must then be R=Q^T.
  Tensor E = 0.5 * (Q * transpose(F) * F * transpose(Q) - One);
  return energy(E);
}

double
GuccioneMaterial::AnisotropicDerivative(const Tensor &F, const Tensor &Q, const Tensor &H) const {
  // Q ist the matrix of fibre directions, i.e. f=Q[0].
  // The Rotation Matrix must then be R=Q^T.
  Tensor R = transpose(Q);
  Tensor FQ = Q * F * R;
  Tensor FT = transpose(FQ);
  Tensor E = 0.5 * (FT * FQ - One);
  Tensor HQ = Q * H * R;
  return Frobenius(FQ * firstPK(E), HQ);
}

double
GuccioneMaterial::AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const Tensor &H,
                                              const Tensor &G) const {
  // Using numerical derivative
  using namespace ::std::placeholders;
  auto derivativeFunc = std::bind(&GuccioneMaterial::AnisotropicDerivative, this, _1, Q, G);
  return numericalDerivative(derivativeFunc, F, H);

  // Q ist the matrix of fibre directions, i.e. f=Q[0].
  // The Rotation Matrix must then be R=Q^T.
  Tensor R = transpose(Q);
  Tensor FQ = Q * F * R;
  Tensor HQ = Q * H * R;
  Tensor GQ = Q * G * R;
  Tensor FT = transpose(FQ);
  Tensor E = 0.5 * (FT * FQ - One);

  return Frobenius(firstPK(E), sym(transpose(GQ) * HQ)) +
         secondDerivative(E, sym(FT * HQ), sym(FT * GQ));
}

double GuccioneMaterial::IsotropicSecondDerivative(const Tensor &F, const TensorRow &H,
                                                   const TensorRow &G) const {
  return 0.0;
}

double
GuccioneMaterial::AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const TensorRow &H,
                                              const TensorRow &G) const {
  return AnisotropicSecondDerivative(F, Q, Tensor(H), Tensor(G));
}


/*
quad GuccioneBonetMaterial::expQ(const Tensor &E, const VectorField &f) const {
  VectorField Ef = E * f;
  quad eQ = exp(4 * a * (f * Ef) * (f * Ef));
  eQ *= exp(4 * b * (Ef * Ef));
  eQ *= exp(4 * c * Frobenius(E, E));
  return eQ;
}*/

double GuccioneBonetMaterial::q(const Tensor &E, const VectorField &f) const {
  VectorField Ef = E * f;
  double QE = 4 * a * (f * Ef) * (f * Ef);
  QE += 4 * b * (Ef * Ef);
  QE += 4 * c * Frobenius(E, E);;
  return QE;
}

Tensor GuccioneBonetMaterial::dq(const Tensor &E, const VectorField &f) const {
  VectorField Ef = E * f;
  Tensor QE =  a * (f * Ef) * Product(f, f);
  QE += b * sym(Product(Ef, f));
  QE += c * E;
  return 8.0*QE;
}

Tensor GuccioneBonetMaterial::ddq(const Tensor &E, const VectorField &f, const Tensor &H) const {
  VectorField Ef = E * f;
  Tensor ff = Product(f, f);
  double Hf = f* (H*f);
  Tensor QE = 4.0 * a * Hf * ff;
  QE += 4 * b * sym(Product(H * f, f));
  QE += 4 * c * H;
  return 2*QE;
}

double GuccioneBonetMaterial::energy(const Tensor &E, const VectorField &f) const {
  return 0.5 * C() * (mat::cutexp(q(E, f),exp_max) - 1);
}


Tensor GuccioneBonetMaterial::firstPK(const Tensor &E, const VectorField &f) const {
  Tensor DPsi = 0.5 * C() * mat::cutexp(q(E, f),exp_max) * dq(E, f) + activeStress * Product(f,f);
  return DPsi;
}

Tensor
GuccioneBonetMaterial::constitutiveMatrix(const Tensor &E, const VectorField &f,
                                          const Tensor &H) const {
  double expQ = mat::cutexp(q(E, f),exp_max);
  Tensor DDPsi = Frobenius(dq(E, f), H) * dq(E, f);
  DDPsi += ddq(E, f, H);
  return 0.5 * C() * expQ * DDPsi;
}


Tensor GuccioneBonetMaterial::FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso,
                                                  const Tensor &Q) const {
  VectorField f = Q[0];
  Tensor E = 0.5 * (transpose(F) * F - One);
  return F * (firstPK(E, f));
}

double GuccioneBonetMaterial::IsotropicEnergy(const Tensor &F) const {
  return 0.0;
}

double
GuccioneBonetMaterial::IsotropicDerivative(const Tensor &F, const Tensor &H) const {
  return 0.0;
}

double
GuccioneBonetMaterial::IsotropicSecondDerivative(const Tensor &F, const Tensor &H,
                                                 const Tensor &G) const {
  return 0.0;
}


double
GuccioneBonetMaterial::IsotropicDerivative(const Tensor &F, const TensorRow &H) const {
  return 0.0;
}

double
GuccioneBonetMaterial::IsotropicSecondDerivative(const Tensor &F, const TensorRow &H,
                                                 const TensorRow &G) const {
  return 0.0;
}

double GuccioneBonetMaterial::AnisotropicEnergy(const Tensor &F, const Tensor &Q) const {
  Tensor E = 0.5 * (transpose(F) * F - One);
  return energy(E, Q[0]);
}

double GuccioneBonetMaterial::AnisotropicDerivative(const Tensor &F, const Tensor &Q,
                                                    const Tensor &H) const {
  Tensor FT = transpose(F);
  Tensor E = 0.5 * (FT * F - One);
  return Frobenius(firstPK(E, Q[0]), FT * H);
}

double GuccioneBonetMaterial::AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q,
                                                          const Tensor &H, const Tensor &G) const {
  // Using numerical derivative
  /*using namespace ::std::placeholders;
  auto derivativeFunc = std::bind(&GuccioneBonetMaterial::AnisotropicDerivative, this, _1, Q, G);
  return numericalDerivative(derivativeFunc, F, H);*/

  Tensor FT = transpose(F);
  Tensor E = 0.5 * (FT * F - One);

  return Frobenius(firstPK(E, Q[0]), sym(transpose(G) * H)) +
         Frobenius(constitutiveMatrix(E, Q[0], sym(FT * H)), sym(FT * G));
}


double GuccioneBonetMaterial::AnisotropicDerivative(const Tensor &F, const Tensor &Q,
                                                    const TensorRow &H) const {
  Tensor FT = transpose(F);
  Tensor E = 0.5 * (FT * F - One);
  return Frobenius(F * firstPK(E, Q[0]), H);
}

double GuccioneBonetMaterial::AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q,
                                                          const TensorRow &H, const TensorRow &G) const {
  // Using numerical derivative
  /*using namespace ::std::placeholders;
  auto derivativeFunc = std::bind(&GuccioneBonetMaterial::AnisotropicDerivative, this, _1, Q, G);
  return numericalDerivative(derivativeFunc, F, H);*/

  Tensor FT = transpose(F);
  Tensor E = 0.5 * (FT * F - One);

  return Frobenius(firstPK(E, Q[0]), sym(ColRowProduct(G, H))) +
         Frobenius(constitutiveMatrix(E, Q[0], sym(FT * H)), sym(FT * G));
}
