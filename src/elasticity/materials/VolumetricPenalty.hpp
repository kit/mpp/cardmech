#ifndef VOLUMETRICPENALTY_H
#define VOLUMETRICPENALTY_H

class VolumetricPenalty {
protected:
  double penalty = 0.0;
public:
  VolumetricPenalty() : penalty(20000.0) {
    Config::Get("VolumetricPenalty", penalty);
  };

  explicit VolumetricPenalty(double lambda) : penalty(lambda) {};

  virtual double Functional(double J) const = 0;

  virtual double Derivative(double J) const = 0;

  virtual double SecondDerivative(double J) const = 0;

  bool IsIncompressible() const { return penalty > 0; }

  double Penalty() const { return penalty; }
};

class NoPenalty : public VolumetricPenalty {
public:
  NoPenalty() : VolumetricPenalty(0.0) {};

  double Functional(double J) const override { return 0.0; };

  double Derivative(double J) const override { return 0.0; };

  double SecondDerivative(double J) const override { return 0.0; };
};

class CiarletPenalty : public VolumetricPenalty {
  double mu{0.0};
public:
  CiarletPenalty(bool mixed = false) : VolumetricPenalty() {
    if (mixed && penalty > 0.0) {
      Config::Get("PermeabilityPenalty", mu);
      mu /= penalty;
    }
  }

  CiarletPenalty(double p) : VolumetricPenalty(p) {}

  CiarletPenalty(double p, double m) : VolumetricPenalty(p), mu(p > 0.0 ? m / p : 1.0) {}

  double Functional(double J) const override {
    if (J <= 0) {
      return 0;
    }
    return 0.5 * (J - 1.0) * (J - 1.0) - mu * log(J);
  }

  double Derivative(double J) const override {
    return (J - 1.0) - mu / J;
  }

  double SecondDerivative(double J) const override {
    return 1.0 + mu / (J * J);
  }
};

class LogarithmicCompressiblePenalty : public VolumetricPenalty {
public:
  LogarithmicCompressiblePenalty() : VolumetricPenalty() {}

  LogarithmicCompressiblePenalty(double p) : VolumetricPenalty(p) {}

  double Functional(double J) const override { return 0.5 * (J - 1.0) * log(J);
  }

  double Derivative(double J) const override { return 0.5 * (log(J) + (J - 1.0) / J); };

  double SecondDerivative(double J) const override { return 0.5 * (1.0 / J + 1.0 / (J * J)); };
};

class MixedCompressiblePenalty : public VolumetricPenalty {
  double mu{0.0};
public:
  MixedCompressiblePenalty() : VolumetricPenalty() {
    if (penalty > 0.0) {
      Config::Get("PermeabilityPenalty", mu);
      mu /= penalty;
    }

  }

  MixedCompressiblePenalty(double l, double m) : VolumetricPenalty(l), mu(l > 0.0 ? m / l : 0.0) {
  }

  double Functional(double J) const override {
    return 0.25 * ((J - 1.0) * (J - 1.0) + mu * log(J) * log(J));
  };

  double Derivative(double J) const override { return 0.5 * ((J - 1.0) + mu * log(J) / J); };

  double SecondDerivative(double J) const override { return 0.5 * (1.0 - mu * log(J)) / J / J; };

};

#endif //VOLUMETRICPENALTY_H
