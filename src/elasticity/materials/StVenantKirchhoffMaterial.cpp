#include "StVenantKirchhoffMaterial.hpp"


double StVenantKirchhoffMaterial::energy(const Tensor &E) const {
  return 0.5 * lambda() * trace(E) * trace(E) + mu() * Frobenius(E, E);
}

Tensor StVenantKirchhoffMaterial::firstPK(const Tensor &E) const {
  return lambda() * trace(E) * One + 2 * mu() * E;
}

Tensor StVenantKirchhoffMaterial::constitutiveMatrix(const Tensor &E, const Tensor &H) const {
  return lambda() * trace(H) * One + 2 * mu() * H;
}


Tensor StVenantKirchhoffMaterial::FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso,
                                                      const Tensor &Q) const {
  Tensor E = 0.5 * (transpose(Fiso) * Fiso - One);
  return Fiso * firstPK(E);
}

double StVenantKirchhoffMaterial::IsotropicEnergy(const Tensor &F) const {
  Tensor E = 0.5 * (transpose(F) * F - One);
  return energy(E);
}

double
StVenantKirchhoffMaterial::IsotropicDerivative(const Tensor &F, const Tensor &H) const {
  Tensor FT = transpose(F);
  Tensor E = 0.5 * (FT * F - One);
  return Frobenius(firstPK(E), FT * H);
}

double
StVenantKirchhoffMaterial::IsotropicSecondDerivative(const Tensor &F, const Tensor &H,
                                                     const Tensor &G) const {
  Tensor FT = transpose(F);
  Tensor E = 0.5 * (FT * F - One);

  return Frobenius(firstPK(E), sym(transpose(G) * H)) +
         Frobenius(constitutiveMatrix(E, sym(FT * H)), sym(FT * G));
}
