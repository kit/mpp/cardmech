#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include "utility/Config.hpp"
#include "VolumetricPenalty.hpp"
#include "ViscoelasticDamping.hpp"
#include "materials.hpp"
#include <Tensor.hpp>
#include <functional>



static Tensor DeformationGradient(const Tensor &Du) { return One + Du; }

class HyperelasticMaterial {
protected:
  std::string name{};
  std::vector<double> parameters{};
public:
  HyperelasticMaterial(std::string &&name, int size = 1) noexcept: name(std::move(name)), parameters(size, 1.0) {}

  HyperelasticMaterial(std::string &&name, const std::vector<double> &params) noexcept: name(std::move(name)), parameters(params) {}

  HyperelasticMaterial(std::string &&name, std::vector<double> &&params) noexcept: name(std::move(name)), parameters(
      std::move(params)) {}

  virtual double q(const Tensor &E, const VectorField &f) const { return 0; }

  
  /**
   * Returns the strain energy of the material given the deformation Gradient F
   * @param F = (Du + One) for the derivative Du of a given displacement u.
   */
  virtual double IsotropicEnergy(const Tensor &F) const = 0;

  /**
   * Returns the strain derivate of the material given the deformation Gradient F
   * @param F = (Du + One) for the derivative Du of a given displacement u.
   */
  virtual double IsotropicDerivative(const Tensor &F, const Tensor &H) const = 0;
  virtual double IsotropicDerivative(const Tensor &F, const TensorRow &H) const {
    return IsotropicDerivative(F, Tensor(H));
  }

  /**
   * Returns the second strain derivative of the material given the deformation Gradient F
   * @param F = (Du + One) for the derivative Du of a given displacement u.
   */
  virtual double
  IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const = 0;

  virtual double IsotropicSecondDerivative(const Tensor &F, const TensorRow &H, const TensorRow &G) const{
    return IsotropicSecondDerivative(F, Tensor(H), Tensor(G));
  }


  /**
   * Returns the strain energy of the material given the deformation Gradient F
   * @param F = (Du + One) for the derivative Du of a given displacement u.
   */
  virtual double AnisotropicEnergy(const Tensor &F, const Tensor &Q) const {return 0.0;};

  /**
   * Returns the strain derivate of the material given the deformation Gradient F
   * @param F = (Du + One) for the derivative Du of a given displacement u.
   */
  virtual double AnisotropicDerivative(const Tensor &F, const Tensor &Q, const Tensor &H) const {return 0.0; };
  virtual double AnisotropicDerivative(const Tensor &F, const Tensor &Q, const TensorRow &H) const{return AnisotropicDerivative(F, Q,Tensor(H));}

  /**
   * Returns the second strain derivative of the material given the deformation Gradient F
   * @param F = (Du + One) for the derivative Du of a given displacement u.
   */
  virtual double
  AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const Tensor &H,
                            const Tensor &G) const {return 0.0;};
  virtual double AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const TensorRow &H, const TensorRow &G) const{return AnisotropicSecondDerivative(F, Q, Tensor(H), Tensor(G));}

  /**
   * Returns the derivative of the strain energy given the appropriate tensor.
   * For Hyperelastic materials, this is equal to the first Piola Kirchhoff tensor.
   * @param F = (Du + One) for the derivative Du of a given displacement u.
   */
  virtual Tensor
  FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const = 0;

  virtual int ParameterCount() const = 0;

  double GetParameter(int i) const { return parameters[i]; }

  virtual std::pair<size_t, size_t> GetMaxParameterRange() const {
      // start, length
      return {0, 1};
  }

  double GetMaxParameter() const{
      auto range = GetMaxParameterRange();
      double maxParameter = parameters[range.first];
      for (size_t i = range.first; i < range.first + range.second; ++i){
          if(parameters[i] > maxParameter){
              maxParameter = parameters[i];
          }
      }
      return maxParameter;
  }

  void UpdateParameters(const std::vector<double> &params) {
    std::copy(params.begin(), params.end(), parameters.begin());
  }

  void UpdateParameters(std::vector<double> &&params) {
    std::move(params.begin(), params.end(), parameters.begin());
  }

  const std::string& Name() const { return name; }
};

static double numericalDerivative(const std::function<double(const Tensor &)> &func,
                                  const Tensor &F, const Tensor &H) {
  double h = 1e-8;
  double dwi =
      func(F + h * H) - func(F - h * H);
  return 0.5 * dwi / h;
}

static double numericalSecondDerivative(
    const HyperelasticMaterial &mat,
    const Tensor &F, const Tensor &Q, const Tensor &H, const Tensor &G) {
  double h = 1e-8;
  double ddwi =
      mat.IsotropicDerivative(F + h * H, G) - mat.IsotropicDerivative(F - h * H, G);
  return 0.5 * ddwi / h;
}

class Material {
  double scalePenalty = 1;
  
  std::unique_ptr<HyperelasticMaterial> hyperelMat = nullptr;
  std::unique_ptr<ViscoelasticDamping> viscoMat = nullptr;
  std::unique_ptr<VolumetricPenalty> volPenalty = nullptr;

  std::unique_ptr<VolumetricPenalty> GetVolumetricPenalty();

  std::unique_ptr<VolumetricPenalty> GetVolumetricPenalty(
      const std::string &penaltyType);

  std::unique_ptr<ViscoelasticDamping> GetDamping();

  std::unique_ptr<ViscoelasticDamping> GetDamping(
      const std::string &dampingType);

public:
  Material() = default;

  explicit Material(const std::string &materialName);

  Material(const std::string &materialName,
           const std::vector<double> &materialParameters);

  double IsoEnergy(const Tensor &F) const {
    return hyperelMat->IsotropicEnergy(F);
  }

  template<typename T>
  double IsoDerivative(const Tensor &F, const T &H) const {
    return hyperelMat->IsotropicDerivative(F, H);
  }

  template<typename T>
  double
  IsoSecondDerivative(const Tensor &F, const T &H, const T &G) const {
    return hyperelMat->IsotropicSecondDerivative(F, H, G);
  }
  
  double q(const Tensor &E, const VectorField &f) const { 
    return hyperelMat->q(E,f);
  }

  double AnisoEnergy(const Tensor &F, const Tensor &Q) const {
    return hyperelMat->AnisotropicEnergy(F, Q);
  }

  template<typename T>
  double AnisoDerivative(const Tensor &F, const Tensor &Q, const T &H) const {
    return hyperelMat->AnisotropicDerivative(F, Q, H);
  }

  template<typename T>
  double
  AnisoSecondDerivative(const Tensor &F, const Tensor &Q, const T &H, const T &G) const {
    return hyperelMat->AnisotropicSecondDerivative(F, Q, H, G);
  }

  Tensor FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const {
    return hyperelMat->FirstPiolaKirchhoff(F, Fiso, Q);
  }

  double Penalty() const {
    return scalePenalty * volPenalty->Penalty();
  }

  double VolEnergy(const Tensor &F) const {
    return volPenalty->Penalty() > 0 ? volPenalty->Functional(det(F)) : 0.0;
  }

  double VolDerivative(const Tensor &F, const Tensor &H) const;

  double  VolSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const;

  double VolDerivative(const Tensor &F, const TensorRow &H) const {return VolDerivative(F, Tensor(H));}

  double  VolSecondDerivative(const Tensor &F, const TensorRow &H, const TensorRow &G) const{return VolSecondDerivative(F, Tensor(H), Tensor(G));};

  Tensor VolFPK(double J) const {
    if (volPenalty->Penalty() > 0) {
      return volPenalty->Functional(J) * volPenalty->Derivative(J) * J * One;
    }
    return Zero;
  }

  Tensor VolConstitutive(double J, const Tensor &H) const {
    if (volPenalty->Penalty() > 0) {
      Tensor volPart = Zero;
      double dwj = volPenalty->Derivative(J) * J;
      double ddwj = volPenalty->SecondDerivative(J) * J;
      if (dwj != 0.0 || ddwj != 0.0) {
        volPart += (ddwj + dwj) * trace(H) * One;
        volPart -= dwj * H;
      }
      return volPart;
    }
    return Zero;
  }

  template<typename T>
  double TotalDerivative(const Tensor &Fiso, const Tensor &F, const Tensor &Q, const T &H) const{
    return IsoDerivative(Fiso, H) + AnisoDerivative(F, Q, H) + Penalty() * VolDerivative(F, H);
  }

  template<typename T>
  double TotalSecondDerivative(const Tensor &Fiso, const Tensor &F, const Tensor &Q, const T &H, const T &G) const{
    return
        IsoSecondDerivative(Fiso, H, G)
        + AnisoSecondDerivative(F, Q, H, G)
        + Penalty() * VolSecondDerivative(F, H, G);
  }

  double TotalSecondDerivative(const Tensor &Fiso, const Tensor &F, const Tensor &Q, const TensorRow &H, const Tensor &G) const{
    return
        IsoSecondDerivative(Fiso, Tensor(H), G)
        + AnisoSecondDerivative(F, Q, Tensor(H), G)
        + Penalty() * VolSecondDerivative(F, Tensor(H), G);
  }

  double TotalSecondDerivative(const Tensor &Fiso, const Tensor &F, const Tensor &Q, const Tensor &H, const TensorRow &G) const{
    return
        IsoSecondDerivative(Fiso, H, Tensor(G))
        + AnisoSecondDerivative(F, Q, H, Tensor(G))
        + Penalty() * VolSecondDerivative(F, H, Tensor(G));
  }

  const HyperelasticMaterial &Isochoric() const { return *hyperelMat; }

  const VolumetricPenalty &Volumetric() const { return *volPenalty; }

  double TotalEnergy(const Tensor &F, const Tensor &Q) const {
    return IsoEnergy(F) + AnisoEnergy(F, Q) + Penalty() * VolEnergy(F);
  }


  double ViscoEnergy(const Tensor &F, const Tensor &L) const {
    return viscoMat->Functional(F, L);
  }

  double ViscoDerivative(const Tensor &F, const Tensor &L, const Tensor &H) const {
    return viscoMat->Derivative(F, L, H);
  }

  double
  ViscoSecondDerivative(const Tensor &F, const Tensor &L, const Tensor &H, const Tensor &G) const {
    return viscoMat->SecondDerivative(F, L, H, G);
  }

  double ViscoDerivative(const Tensor &F, const Tensor &L, const TensorRow &H) const {
    return viscoMat->Derivative(F, L, H);
  }

  double ViscoSecondDerivative(const Tensor &F, const Tensor &L, const TensorRow &H,
                               const TensorRow &G) const {
    return viscoMat->SecondDerivative(F, L, H, G);
  }

  const std::string &Name() const { return hyperelMat->Name(); }

  double GetParameter(int i) const {
    return hyperelMat->GetParameter(i);
  }

  std::string ParameterInfo() const;

  void ScalePenalty (double s) {
    scalePenalty = ((s>1.0) + (0.0<s<=1.0) * s);
  }
};


namespace material {
    static double poissonInvariant(const Material &mat){
        if (mat.Name() == "Linear"){
            return mat.GetParameter(0)/(2*(mat.GetParameter(0) + mat.GetParameter(1)));
        }
        else{
            return 0;
        }
    }
}

#endif
