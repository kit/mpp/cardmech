#include "LinearMaterial.hpp"
#include "NeoHookeMaterial.hpp"
#include "GuccioneMaterial.hpp"
#include "HolzapfelMaterial.hpp"
#include "StVenantKirchhoffMaterial.hpp"
#include "Material.hpp"
#include "LaplaceMaterial.hpp"
#include "MooneyRivlinMaterial.hpp"

Material::Material(const std::string &materialName) {
  // === Create HyperElasticMaterial ==== //
  if (materialName == "Laplace") hyperelMat = std::make_unique<LaplaceMaterial>();
  else if (materialName == "Linear") hyperelMat = std::make_unique<LinearMaterial>();
  else if (materialName == "NeoHooke") {
    hyperelMat = std::make_unique<NeoHookeMaterial>();
    volPenalty = std::make_unique<CiarletPenalty>(hyperelMat->GetParameter(0),
                                                  hyperelMat->GetParameter(1));
    return;
  } else if (materialName == "MooneyRivlin") hyperelMat = std::make_unique<MooneyRivlinMaterial>();
  else if (materialName == "Guccione") hyperelMat = std::make_unique<GuccioneMaterial>();
  else if (materialName == "Bonet") hyperelMat = std::make_unique<GuccioneBonetMaterial>();
  else if (materialName == "Holzapfel") hyperelMat = std::make_unique<HolzapfelMaterial>();
  else if (materialName == "StVenantKirchhoff")
    hyperelMat = std::make_unique<StVenantKirchhoffMaterial>();
  else
    THROW("Abort: Material not defined")

  volPenalty = GetVolumetricPenalty();
  viscoMat = GetDamping();
}

Material::Material(const std::string &materialName,
                   const std::vector<double> &materialParameters) {
  if (materialName == "Laplace")
    hyperelMat = std::make_unique<LaplaceMaterial>(materialParameters);
  else if (materialName == "Linear")
    hyperelMat = std::make_unique<LinearMaterial>(materialParameters);
  else if (materialName == "NeoHooke") {
    hyperelMat = std::make_unique<NeoHookeMaterial>(materialParameters);
    volPenalty = std::make_unique<CiarletPenalty>(materialParameters[0], materialParameters[1]);
    return;
  } else if (materialName == "MooneyRivlin")
    hyperelMat = std::make_unique<MooneyRivlinMaterial>(materialParameters);
  else if (materialName == "Guccione")
    hyperelMat = std::make_unique<GuccioneMaterial>(materialParameters);
  else if (materialName == "Bonet")
    hyperelMat = std::make_unique<GuccioneBonetMaterial>(materialParameters);
  else if (materialName == "Holzapfel")
    hyperelMat = std::make_unique<HolzapfelMaterial>(materialParameters);
  else if (materialName == "StVenantKirchhoff")
    hyperelMat = std::make_unique<StVenantKirchhoffMaterial>(materialParameters);
  else if (materialName == "Laplace")
    hyperelMat = std::make_unique<LaplaceMaterial>(materialParameters);
  else
    THROW("Abort: Material not defined")

  volPenalty = GetVolumetricPenalty();
  viscoMat = GetDamping();
}

std::unique_ptr<VolumetricPenalty> Material::GetVolumetricPenalty(const std::string &penaltyType) {
  if (penaltyType == "Ciarlet")
    return std::make_unique<CiarletPenalty>();
  else if (penaltyType == "Logarithmic")
    return std::make_unique<LogarithmicCompressiblePenalty>();
  else if (penaltyType == "Mixed") volPenalty = std::make_unique<MixedCompressiblePenalty>();

  if (penaltyType != "None") Warning(
      "Volumetric Penalty Type " + penaltyType + "unkown. Continuing without penalty")
  return std::make_unique<NoPenalty>();
}

std::unique_ptr<ViscoelasticDamping> Material::GetDamping(const std::string &dampingType) {
  if (dampingType == "KelvinVoigt")
    return std::make_unique<KelvinVoigtDamping>();

  if (dampingType != "None") Warning(
      "Damping Type " + dampingType + " unkown. Continuing without damping")
  return std::make_unique<ViscoelasticDamping>();
}

std::unique_ptr<VolumetricPenalty> Material::GetVolumetricPenalty() {
  std::string penaltyType = "None";
  Config::Get("QuasiCompressiblePenalty", penaltyType);
  return GetVolumetricPenalty(penaltyType);
}

std::unique_ptr<ViscoelasticDamping> Material::GetDamping() {
  std::string dampingType = "None";
  Config::Get("ViscoelasticDamping", dampingType);
  return GetDamping(dampingType);
}

double Material::VolDerivative(const Tensor &F, const Tensor &H) const {
  double J = det(F);
  if (volPenalty->Penalty() == 0.0 || J <= 0.0) return 0.0;

  double dwj = volPenalty->Derivative(J);
  return dwj != 0.0 ? dwj * Frobenius(mat::cofactor(F), H) : 0.0;
}

double Material::VolSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const {
  double J = det(F);
  if (volPenalty->Penalty() == 0.0 || J<=0.0) return 0.0;

  Tensor CofF = mat::cofactor(F);
  double ddwj = volPenalty->SecondDerivative(J) * Frobenius(CofF, H) * Frobenius(CofF, G);
  ddwj += volPenalty->Derivative(J) * Frobenius(mat::d_cofactor(F, H), G);
  return ddwj;
}

std::string Material::ParameterInfo() const{
  std::string paramInfo{"("};
  for(int i =0;i<hyperelMat->ParameterCount();++i){
    if(i>0) paramInfo += ", ";
    paramInfo += std::to_string(hyperelMat->GetParameter(i));
  }
  paramInfo += ")";
  return paramInfo;
}
