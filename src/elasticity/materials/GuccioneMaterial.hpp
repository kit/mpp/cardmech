#ifndef GUCCIONEMATERIAL_H
#define GUCCIONEMATERIAL_H

#include "Material.hpp"

#ifdef __INTEL_COMPILER
typedef _Quad quad;
#else

#include <quadmath.h>

typedef __float128 quad;
#endif

class GuccioneMaterial : public HyperelasticMaterial {
  double C() const { return GetParameter(0); }

  double bf() const { return GetParameter(1); }

  double bs() const { return GetParameter(2); }

  double bfs() const { return GetParameter(3); }

  std::pair<size_t, size_t> GetMaxParameterRange() const override {
    // start, length
    return {0, 1};
  }

  Tensor fiberWeights(const Tensor &E) const {
    return {bf() * E[0][0], bfs() * E[0][1], bfs() * E[0][2],
            bfs() * E[1][0], bs() * E[1][1], bs() * E[1][2],
            bfs() * E[2][0], bs() * E[2][1], bs() * E[2][2]};
  }


  double energy(const Tensor &E) const;

  Tensor firstPK(const Tensor &E) const;

  Tensor constitutiveMatrix(const Tensor &E, const Tensor &H) const;

public:
  GuccioneMaterial() noexcept: HyperelasticMaterial("Guccione", 4) {
    Config::Get("GuccioneMat_C", parameters[0]);
    Config::Get("GuccioneMat_bf", parameters[1]);
    Config::Get("GuccioneMat_bs", parameters[2]);
    Config::Get("GuccioneMat_bfs", parameters[3]);
  }

  explicit GuccioneMaterial(const std::vector<double> &params) : HyperelasticMaterial("Guccione", params) {};

  explicit GuccioneMaterial(std::vector<double> &&params) : HyperelasticMaterial("Guccione",
                                                                                 std::move(params)) {};

  Tensor FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const override;

  double IsotropicEnergy(const Tensor &F) const override;

  double IsotropicDerivative(const Tensor &F, const Tensor &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const override;
  double
  IsotropicSecondDerivative(const Tensor &F, const TensorRow &H, const TensorRow &G) const override;

  double AnisotropicEnergy(const Tensor &F, const Tensor &Q) const override;

  double AnisotropicDerivative(const Tensor &F, const Tensor &Q, const Tensor &H) const override;

  double AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const Tensor &H,
                                     const Tensor &G) const override;

  double AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const TensorRow &H,
                                     const TensorRow &G) const override;

  int ParameterCount() const override { return 4; }

  double secondDerivative(const Tensor &E, const Tensor &H, const Tensor &K) const;
};

/*
 *  This formulation was derived in
 *  https://doi.org/10.1016/j.cma.2019.01.042
 *
 *  The corresponding invariant formulation is to convoluted to implement within this code.
 *  May only work when completely chaning how we handle tensor derivatives and invariants in M++.
 */
class GuccioneBonetMaterial : public HyperelasticMaterial {
  double a{1.0};
  double b{1.0};
  double c{1.0};
  double activeStress{0.0};
  double exp_max = 10;
  
  void calculateParams() {
    a = 0.25 * (bf() - 2.0 * bfs() + bs());
    b = 0.5 * (bfs() - bs());
    c = 0.25 * bs();
  }

  double C() const { return GetParameter(0); }

  double bf() const { return GetParameter(1); }

  double bs() const { return GetParameter(2); }

  double bfs() const { return GetParameter(3); }

  std::pair<size_t, size_t> GetMaxParameterRange() const override {
    // start, length
    return {0, 1};
  }


  double q(const Tensor &E, const VectorField &f) const;

  Tensor dq(const Tensor &E, const VectorField &f) const;

  Tensor ddq(const Tensor &E, const VectorField &f, const Tensor &H) const;

  double energy(const Tensor &E, const VectorField &f) const;

  Tensor firstPK(const Tensor &E, const VectorField &f) const;

  Tensor
  constitutiveMatrix(const Tensor &E, const VectorField &f, const Tensor &H) const;

public:
  GuccioneBonetMaterial() noexcept: HyperelasticMaterial("Bonet", 4) {
    Config::Get("GuccioneMat_C", parameters[0]);
    Config::Get("GuccioneMat_bf", parameters[1]);
    Config::Get("GuccioneMat_bs", parameters[2]);
    Config::Get("GuccioneMat_bfs", parameters[3]);
    Config::Get("exp_max", exp_max);
    calculateParams();
    Config::Get("activeStress", activeStress);
  }

  explicit GuccioneBonetMaterial(const std::vector<double> &params) : HyperelasticMaterial("Bonet",
                                                                                           params) {
    Config::Get("exp_max", exp_max);
    calculateParams();
    Config::Get("activeStress", activeStress);
  };

  explicit GuccioneBonetMaterial(std::vector<double> &&params) : HyperelasticMaterial("Bonet",
                                                                                      std::move(params)) {
    Config::Get("exp_max", exp_max);
    calculateParams();
    Config::Get("activeStress", activeStress);
  };

  Tensor FirstPiolaKirchhoff(const Tensor &E, const Tensor &Fiso, const Tensor &Q) const override;

  double IsotropicEnergy(const Tensor &F) const override;

  double IsotropicDerivative(const Tensor &F, const Tensor &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const override;

  double IsotropicDerivative(const Tensor &F, const TensorRow &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const TensorRow &H, const TensorRow &G) const override;

  double AnisotropicEnergy(const Tensor &F, const Tensor &Q) const override;

  double AnisotropicDerivative(const Tensor &F, const Tensor &Q, const Tensor &H) const override;

  double AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const Tensor &H,
                                     const Tensor &G) const override;


  double AnisotropicDerivative(const Tensor &F, const Tensor &Q, const TensorRow &H) const override;

  double AnisotropicSecondDerivative(const Tensor &F, const Tensor &Q, const TensorRow &H,
                                     const TensorRow &G) const override;


  int ParameterCount() const override { return 4; }
};

#endif //GUCCIONEMATERIAL_H
