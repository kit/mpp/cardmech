#ifndef LAPLACEMATERIAL_HPP
#define LAPLACEMATERIAL_HPP

#include "Material.hpp"

class LaplaceMaterial : public HyperelasticMaterial {
  Tensor permeability{One};

  double energy(const Tensor &Du) const;

  Tensor firstPK(const Tensor &Du) const;

  Tensor constitutiveMatrix(const Tensor &Du, const Tensor &H) const;

  Tensor constitutiveMatrix(const Tensor &Du, const TensorRow &H) const;

public:
  LaplaceMaterial() noexcept: HyperelasticMaterial("Laplace", 1) {
    Config::Get("LaplaceMat_kappa", parameters[0]);
    permeability *= parameters[0];
  }

  explicit LaplaceMaterial(const std::vector<double> &params)
      : HyperelasticMaterial("Laplace", params) {
    permeability *= parameters[0];
  };

  explicit LaplaceMaterial(std::vector<double> &&params)
      : HyperelasticMaterial("Laplace", std::move(params)) {
    permeability *= parameters[0];
  };

  Tensor FirstPiolaKirchhoff(const Tensor &F, const Tensor &Fiso, const Tensor &Q) const override;

  double IsotropicEnergy(const Tensor &F) const override;

  double IsotropicDerivative(const Tensor &F, const Tensor &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const Tensor &H, const Tensor &G) const override;

  double IsotropicDerivative(const Tensor &F, const TensorRow &H) const override;

  double
  IsotropicSecondDerivative(const Tensor &F, const TensorRow &H, const TensorRow &G) const override;

  int ParameterCount() const override { return 1; }

};

#endif //LAPLACEMATERIAL_HPP
