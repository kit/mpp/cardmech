#ifndef VISCOELASTICDAMPING_HPP
#define VISCOELASTICDAMPING_HPP

#include "Tensor.hpp"

class ViscoelasticDamping {
public:
  ViscoelasticDamping() = default;

  virtual double Functional(const Tensor &F, const Tensor &L) const { return 0.0; };

  virtual double
  Derivative(const Tensor &F, const Tensor &L, const Tensor &H) const { return 0.0; };

  virtual double SecondDerivative(const Tensor &F, const Tensor &L, const Tensor &H,
                                  const Tensor &G) const { return 0.0; };

  virtual double Derivative(const Tensor &F, const Tensor &L, const TensorRow &H) const {
    return Derivative(F, L, Tensor(H));
  };

  virtual double
  SecondDerivative(const Tensor &F, const Tensor &L, const TensorRow &H, const TensorRow &G) const {
    return SecondDerivative(F, L, Tensor(H), Tensor(G));
  };
};

class KelvinVoigtDamping : public ViscoelasticDamping {
  double eta{0.0};
public:
  KelvinVoigtDamping() : ViscoelasticDamping() {
    Config::Get("KelvinVoigtEta", eta);
  };

  KelvinVoigtDamping(double e) : ViscoelasticDamping(), eta(e) {
  };

  double Functional(const Tensor &F, const Tensor &L) const override {
    auto traceEDot = trace(sym(transpose(F) * L));
    return 0.5 * eta * Frobenius(F, L) * Frobenius(F, L);
  }

  double Derivative(const Tensor &F, const Tensor &L, const Tensor &H) const override {
    return eta * Frobenius(F, L) * Frobenius(H, L);
  }

  double SecondDerivative(const Tensor &F, const Tensor &L, const Tensor &H,
                          const Tensor &G) const override {
    return eta * Frobenius(H, L) * Frobenius(G, L);
  }

  double Derivative(const Tensor &F, const Tensor &L, const TensorRow &H) const override {
    return eta * Frobenius(F, L) * Frobenius(H, L);
  }

  double SecondDerivative(const Tensor &F, const Tensor &L, const TensorRow &H,
                          const TensorRow &G) const override {
    return eta * Frobenius(H, L) * Frobenius(G, L);
  }
};

#endif //VISCOELASTICDAMPING_HPP
