#include "solvers/IterativePressureSolver.hpp"
#include "solvers/DynamicMechanicsSolver.hpp"

#include "CardiacInterpolation.hpp"
#include "MainElasticity.hpp"

MainElasticity::MainElasticity() {
  Config::Get("Model", modelName);
  Config::Get("plevel", plevel);
  Config::Get("MainMechVerbose", verbose);
  Config::Get("MechLevel", mlevel);
  Config::Get("MechPolynomialDegree", discDegree);
  Config::Get("MechMeshPart", chamber);
  Config::Get("PressureSteps", pressureSteps);
  Config::Get("PrestressSteps", prestressSteps);
}

void MainElasticity::Initialize() {
  // load ElasticityProblem from src/elasticity/problem/ElasticityProblem.cpp
  elasticityProblem = GetElasticityProblem();

  // get discretization model for elasticity problem from src/elasticity/problem/ElasticityProblem.hpp
  auto elasticityModel = elasticityProblem->Model();
  if (elasticityModel.empty()) Config::Get("MechDiscretization", elasticityModel);

  generateMesh(elasticityModel);
  initAssemble();
  generateStartVectors();
}

void MainElasticity::Initialize(std::unique_ptr<ElasticityProblem> problem) {
  elasticityProblem = std::move(problem);
  generateMesh(elasticityProblem->Model());
  initAssemble();
  generateStartVectors();
}

Vector &MainElasticity::Run(const Vector &start) {
  if (start.GetDisc().DiscName() == (*displacement).GetDisc().DiscName() && start.Level() < (*displacement).Level()){
    // load Interpolation from used discretization model from src/elasticity/assemble/IElasticity.hpp
    *displacement = mechA->Interpolation(start, *displacement);
  }
  else {
    if(start.GetDisc().DiscName()==(*displacement).GetDisc().DiscName())
      *displacement = start;
    else {
      *displacement = mechA->UseLagrangeAsStartVector(start, *displacement);
    }
  }
  return Run();
}

Vector &MainElasticity::Run() {

  std::string runtype{"Default"};
  Config::Get("MechRuntype", runtype);
  std::string dyn = "Static";
  Config::Get("MechDynamics", dyn);

  mout << "=== Running MainElasticity with parameters: ===============" << endl;
  mout.PrintInfo("Assemble", verbose,
                 PrintInfoEntry("Name", mechA->Name(), 1),
                 PrintInfoEntry("Runtype", runtype, 1),
                 PrintInfoEntry("Dynamics", dyn, 1),
                 PrintInfoEntry("ElasticityProblem", elasticityProblem->Name(), 1),
                 PrintInfoEntry("Material",
                                elasticityProblem->GetMaterialName(true), 1),
                 PrintInfoEntry("Material Parameters",
                                elasticityProblem->GetMaterials().first.ParameterInfo(), 1),
                 PrintInfoEntry("Poisson's Ratio",
                                material::poissonInvariant(elasticityProblem->GetMaterials().first),
                                1),
                 PrintInfoEntry("MechPolynomialDegree:", discDegree, 1),
                 PrintInfoEntry("Degrees of Freedom:", displacement->pSize(), 1)/*,
                   PrintInfoEntry("Matrix Entries", solution.pMatrixSize(), 2)*/);
  mout << "===========================================================" << endl << endl;
//  displacement->GetMatrixGraph().PrintMatrixMemoryInfo();
//  displacement->GetMatrixGraph().PrintVectorMemoryInfo();
  displacement->GetMesh().PrintInfo();

  mout << endl << "===========================================================" << endl << endl;

  if (runtype == "Exact") {
    SetDisplacement(true, *displacement);
    mechA->SetInitialValue(*displacement);
    if (verbose > 0) mechA->PlotDisplacement(*displacement, 0, "exactSolution");
  } else if (runtype == "Klotz") {
    // Generate KlotzPressureSolver in src/elasticity/solvers/IterativePressureSolver.hpp
    KlotzPressureSolver mSolver(*elasticityProblem, pressureSteps);
    mSolver.Method(*mechA, *displacement);
  } else if (runtype == "Default") {
    bool prestress{false};
    Config::Get("WithPrestress", prestress);
    if (prestress) {
      // Calculates Prestress in src/elasticity/solvers/IterativePressureSolver.cpp
      CalculatePrestress(*mechA, *displacement);
    }
    if (dyn == "Static") {
      elasticityProblem->EvaluationResults(*displacement);
      // Generate IterativePressureSolver in src/elasticity/solvers/IterativePressureSolver.hpp
      IterativePressureSolver mSolver(*elasticityProblem, pressureSteps);
      mSolver.Method(*mechA, *displacement);
    } else {
      // Generate ElastodynamicTimeIntegrator in src/elasticity/solvers/DynamicMechanicsSolver.hpp
      ElastodynamicTimeIntegrator dynamicSolver(dyn);
      dynamicSolver.Method(*mechA, *displacement);
    }
  }

  return *displacement;
}

void MainElasticity::generateMesh(const std::string &model) const {
  // search for included chambers (src/CardiacData.cpp)
  std::vector<int> chambers = getIncludedChambers(chamber);
  auto mCreator = MeshesCreator().
      WithMeshName(elasticityProblem->GetMeshName()).
      WithIncludeVector(chambers).
      WithLevel(mlevel);

  elasticityProblem->CreateMeshes(mCreator);
  InterpolateCellData(elasticityProblem->GetMeshes());
}

void MainElasticity::generateStartVectors() {
  // Create discretizations in src/elasticity/assemble/IElasticity.hpp.
  vectorDisc = mechA->CreateDisc(3);
  scalarDisc = mechA->CreateDisc(1);

  displacement = std::make_unique<Vector>(vectorDisc);
  *displacement = 0.0;
  //displacement->Clear();
}


void MainElasticity::SetDisplacement(bool exact, Vector &u) const {
  double d = u.GetMesh().dim();
  if (exact) {
    mechA->SetDisplacement(u);
  } else {
    for (row r = u.rows(); r != u.rows_end(); r++) {
      for (int i = 0; i < d; i++)
        u(r, i) = (*displacement)(r, i);
    }
  }
}

Vector &MainElasticity::GetDisplacement() const {
  return *displacement;
}

void MainElasticity::initAssemble() {
  bool isStatic = false;
  // Create assemble for used discretization in src/elasticity/assemble/IElasticity.cpp
  mechA = CreateFiniteElasticityAssemble(*elasticityProblem, discDegree, isStatic);
  auto times = ReadTimeFromConfig();
  mechA->ResetTime(times[0], times[1], times[2]);
}

double MainElasticity::EvaluateQuantity(const Vector &solution, const std::string &quantity) const {
  if (quantity == "L2Norm") { return mechA->L2Norm(solution); }
  if (quantity == "L2") { return mechA->L2Error(solution); }
  if (quantity == "H1") { return mechA->H1Error(solution); }
  if (quantity == "Energy") { return mechA->EnergyError(solution); }
  if (quantity == "PressureBND") { return mechA->PressureError(solution); }
  else { return infty; }
}


std::array<double, 4> MainElasticity::GetVolume(const Vector &solution) const {
  return mechA->ChamberVolume(solution);
}

std::pair<double, double> MainElasticity::GetGeometryVolume(const Vector &solution) const {
  return {mechA->InitialVolume(solution.GetMesh()), mechA->DeformedVolume(solution)};
}

std::vector<double> MainElasticity::Evaluate(const Vector &solution) {
  Vector exactSolution(solution);
  SetDisplacement(true, exactSolution);

  if (verbose > 0) {
    mout << "\n" << "=================== Elasticity Information ================== \n\n";
    mout.PrintInfo("Solution Metrics", verbose,
                   PrintInfoEntry("Norm", norm(solution), 1),
        //PrintInfoEntry("Pressure Norm", mechA->PressureBndNorm(solution), 1),
                   PrintInfoEntry("L2 Norm", mechA->L2Norm(solution), 1),
                   PrintInfoEntry("L2 Cell-Avg Norm", mechA->L2AvgNorm(solution), 1),
                   PrintInfoEntry("H1 Norm", mechA->H1Norm(solution), 1),
                   PrintInfoEntry("StrainEnergy Norm", mechA->StrainEnergyNorm(solution), 1),
                   PrintInfoEntry("Energy Norm", mechA->EnergyNorm(solution), 2),
                   PrintInfoEntry("Energy Value", mechA->EnergyValue(solution), 2),
                   PrintInfoEntry("min(J)", mechA->detF(solution).first, 1),
                   PrintInfoEntry("max(J)", mechA->detF(solution).second, 1)
    );

    mout.PrintInfo("Exact Solution Metrics", verbose,
        //PrintInfoEntry("Energy", mechA->Energy(solution), 1),
                   PrintInfoEntry("Interpolation Error", mechA->L2Error(exactSolution), 2),
                   PrintInfoEntry("L2 Error", mechA->L2Error(solution), 2),
                   PrintInfoEntry("H1 Error", mechA->H1Error(solution), 2),
                   PrintInfoEntry("StrainEnergy Error", mechA->StrainEnergyNorm(solution), 2),
                   PrintInfoEntry("Energy Error", mechA->EnergyNorm(solution), 2)
    );


    auto volume = GetVolume(solution);

    mout.PrintInfo("Volumes", verbose,
                   PrintInfoEntry("Initial Geometry Volume (mm^3)",
                                  mechA->InitialVolume(solution.GetMesh()), 1),
                   PrintInfoEntry("Deformed Geometry Volume (mm^3)", mechA->DeformedVolume(solution), 1),
                   PrintInfoEntry("Left Ventricle Volume (ml)", volume[0], 2),
                   PrintInfoEntry("Right Ventricle Volume (ml)", volume[1], 2),
                   PrintInfoEntry("Left Atria Volume (ml)", volume[2], 2),
                   PrintInfoEntry("Right Atria Volume (ml)", volume[3], 2));
  }
  mout << endl << "=========== Problem Information =============================" << endl << endl;
  auto result = elasticityProblem->EvaluationResults(solution);
  mout << endl << "=============================================================" << endl << endl;
  mechA->ProblemEvaluation(solution);
  mout << endl << "=============================================================" << endl << endl;
  return result;
}

void MainElasticity::ResetTime(double start, double end, int steps) {
  mechA->ResetTime(start, end, steps);
}

void MainElasticity::ResetLevel(int newLevel) {
  mlevel = newLevel;
}


void MainElasticity::SetDegree(int degree){
  discDegree = degree;
}

void MainElasticity::EvaluateReferece(const Vector &solution, const Vector &reference) {
  mout << "============== Comparing grid level " << solution.GetMesh().Level() << " with "
       << reference.GetMesh().Level() << " =========" << endl << endl;

  Vector projection2(0.0, solution);
  /*mout << projection2 << endl;
  mout << "reference in Main before everything" << reference << endl;*/
  Vector projection = mechA->Projection(solution, reference);
  /*mout << "solution " << solution << endl;
  mout << "reference " << reference << endl;
  mout << "projection MainElasticity " << projection << endl; // für DG noch 0, da noch nicht implementiert*/
  projection -= solution;

  mout.PrintInfo("Convergence Results", 10,
                 PrintInfoEntry("L2 Error", mechA->L2Norm(projection), 1),
                 PrintInfoEntry("L2 CellAvg Error", mechA->L2AvgNorm(projection), 1),
                 PrintInfoEntry("H1 Error", mechA->H1Norm(projection), 1),
                 PrintInfoEntry("StrainEnergy Error", mechA->StrainEnergyNorm(projection), 1),
                 PrintInfoEntry("Energy Error", mechA->EnergyNorm(projection), 1)/*,
                 PrintInfoEntry("Pressure Error", mechA->PressureBndNorm(projection), 1)*/
  );
//  mout << "=============================================================" << endl << endl;
//  mechA->ProblemEvaluation(solution);
//  mout << endl << "=============================================================" << endl << endl;

}

VectorStruct MainElasticity::MoveVector() {
  return VectorStruct{std::move(elasticityProblem), std::move(vectorDisc), std::move(displacement)};
}

