#ifndef MULTIELASTICITY_HPP
#define MULTIELASTICITY_HPP


#include "MainCardMech.hpp"
#include "MainElasticity.hpp"

/**
 * To run MainElasticity multiple times with different MechLevels one after the other.
 */
class MultiElasticity : public MainCardMech {
  MainElasticity main;

  int initialLevel{0};
  int referenceLevel{1};
  bool useReference{true};
  std::vector<VectorStruct> solutions{};


public:
  MultiElasticity();

  void Initialize() override;

  Vector &Run() override;

  std::vector<double> Evaluate(const Vector &solution) override;
};

#endif //MULTIELASTICITY_HPP
