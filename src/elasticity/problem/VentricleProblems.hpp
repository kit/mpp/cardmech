#ifndef CARDMECH_VENTRICLEPROBLEMS_H
#define CARDMECH_VENTRICLEPROBLEMS_H

#include "ElasticityProblem.hpp"

class VentricleProblem : public ElasticityProblem {
  double ventriclePressure{0.0};
protected:
  double pressure(double t, const Point &x, int bndCondition) const override {
    return (bndCondition % 230 == 0) * ventriclePressure;
  }

public:
  explicit VentricleProblem(std::string const &defaultMesh = "")
      : ElasticityProblem(), ventriclePressure(pressureFromConfig(0)) {
    if (!meshFromConfig()) {
      meshesName = defaultMesh.empty() ? "LeftVentricle" : defaultMesh;
    }
  }

  VentricleProblem(const std::string &matName,
                   const std::vector<double> &matPar,
                   std::string const &defaultMesh = "")
      : VentricleProblem(matName, matPar, pressureFromConfig(0), defaultMesh) {}

  VentricleProblem(const std::string &matName,
                   const std::vector<double> &matPar, int pressure,
                   std::string const &defaultMesh = "")
      : ElasticityProblem(matName, matPar), ventriclePressure(pressure) {
    if (!meshFromConfig()) {
      meshesName = defaultMesh.empty() ? "LeftVentricle" : defaultMesh;
    }
  }

  std::vector<double> EvaluationResults(const Vector &solution) const override;

  std::vector<std::string> EvaluationMetrics() const override;

  std::pair<double, double> printMyocard(const Vector &u) const;

  std::string Name() const override { return "Ventricle Problem"; }
};


#endif //CARDMECH_VENTRICLEPROBLEMS_H






