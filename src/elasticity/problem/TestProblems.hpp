#ifndef TESTPROBLEMS_HPP
#define TESTPROBLEMS_HPP

#include <utility>

#include "ElasticityProblem.hpp"

template<int degree>
class LaplaceDirichletProblem : public ElasticityProblem {
  int spaceDim{3};
  std::string is_tet{"Tet"};
public:
  explicit LaplaceDirichletProblem() : ElasticityProblem("Laplace", {1.0}) {
    Config::Get("ProblemDimension", spaceDim);
    Config::Get("ProblemGeometry", is_tet);

    if (!meshFromConfig())
      meshesName = "UnitBlock" + std::to_string(spaceDim) + "D" + is_tet;
  }

  VectorField Deformation(double time, const Point &x) const override {
    return {pow(x.Sum(), degree), spaceDim > 1 ? pow(x.Sum(), degree) : 0.0,
            spaceDim > 2 ? pow(x.Sum(), degree) : 0.0};
  };


  Tensor DeformationGradient(double time, const Point &x) const override {
    double val = degree * pow(x.Sum(), degree - 1);
    return Tensor(
        val, spaceDim > 1 ? val : 0.0, spaceDim > 2 ? val : 0.0,
        spaceDim > 1 ? val : 0.0, spaceDim > 1 ? val : 0.0, spaceDim > 2 ? val : 0.0,
        spaceDim > 2 ? val : 0.0, spaceDim > 2 ? val : 0.0,
        spaceDim > 2 ? val : 0.0);
  }

  VectorField Load(double time, const Point &x, const Cell &c) const override {
    double val = -spaceDim * degree * (degree - 1) * pow(x.Sum(), degree - 2);
    return {val, spaceDim > 1 ? val : 0.0, spaceDim > 2 ? val : 0.0};
  }

  bool HasExactSolution() const override {
    return true;
  }

  std::string Name() const override {
    return "LaplaceProblemDegree" + std::to_string(degree);
  }
};


class VolumeBlockProblem : public ElasticityProblem {
  int spaceDim{3};
  std::string is_tet{"Tet"};

  std::array<double, 3> scale;
public :
  VolumeBlockProblem(double xScale, double yScale, double zScale) :
      ElasticityProblem("Linear", {1.0, 1.0}),
      scale({xScale, yScale, zScale}) {
    Config::Get("ProblemDimension", spaceDim);
    Config::Get("ProblemGeometry", is_tet);

    if (!meshFromConfig())
      meshesName = "VolumeBlock" + std::to_string(spaceDim) + "D" + is_tet;

  }

  VectorField Deformation(double time, const Point &x) const override {
    return VectorField(scale[0] * x[0], scale[1] * x[1], spaceDim > 2 ? scale[2] * x[2] : 0.0);
  }

  Tensor DeformationGradient(double time, const Point &x) const override {
    return Tensor(scale[0], 0.0, 0.0,
                  0.0, scale[1], 0.0,
                  0.0, 0.0, spaceDim > 2 ? scale[2] : 0.0);
  }

  bool HasExactSolution() const override {
    return true;
  }

  std::string Name() const override {
    return "Volume Block Problem";
  }
};


class PrestressProblem : public ElasticityProblem {
  std::array<double, 4> chamberPressure{0.0, 0.0, 0.0, 0.0};
protected:
  double pressure(double t, const Point &x, int bndCondition) const override {
    return chamberPressure[bndCondition%230];
  }
public:
  PrestressProblem(std::string meshName, std::array<double, 4> pressure) :
      ElasticityProblem("Linear", {1.0, 1.0}), chamberPressure(pressure) {
    if (!meshFromConfig())
      meshesName = std::move(meshName);

    if(!meshFromConfig()){
      THROW("No Meshname was defined")
    }
  }

  PrestressProblem(std::string meshName, double p) :
      PrestressProblem(std::move(meshName), {p, p, p, p}) {}

  explicit PrestressProblem(std::string meshName="") : PrestressProblem(std::move(meshName), 0.0) {
    double p = 0.0;
    Config::Get("ConstantPressure", p);
    std::fill(chamberPressure.begin(), chamberPressure.end(), p);

    Config::Get("LVPressure", chamberPressure[0]);
    Config::Get("RVPressure", chamberPressure[1]);
    Config::Get("LAPressure", chamberPressure[2]);
    Config::Get("RAPressure", chamberPressure[3]);
  }


  VectorField Deformation(double time, const Point &x) const override { return zero; }

  Tensor DeformationGradient(double time, const Point &x) const override { return Zero; }


  bool HasExactSolution() const override { return true; }

  std::string Name() const override { return "Prestress Problem"; }
};

#endif //TESTPROBLEMS_HPP
