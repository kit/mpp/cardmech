#include "DynamicBoundaryProblems.hpp"

#include <numbers>

VectorField DExponentialStiffnessProblem::Deformation(double time, const Point &x) const {
    VectorField  u(
            exp(x[0]), exp(x[1]), spaceDim > 2 ? exp(x[2]) : 0.0
            );
    return time * time * u;
}

VectorField DExponentialStiffnessProblem::Velocity(double time, const Point &x) const {
  VectorField  u(
      exp(x[0]), exp(x[1]), spaceDim > 2 ? exp(x[2]) : 0.0
  );
    return 2 * time * u;
}

VectorField DExponentialStiffnessProblem::Acceleration(double time, const Point &x) const {
  VectorField  u(
      exp(x[0]), exp(x[1]), spaceDim > 2 ? exp(x[2]) : 0.0
  );
    return 2 *u;
}

Tensor DExponentialStiffnessProblem::DeformationGradient(double time, const Point &x) const {
    Tensor Du(
            exp(x[0]), 0.0, 0.0,
            0.0, exp(x[1]), 0.0,
            0.0, 0.0, spaceDim > 2 ? exp(x[2]) : 0.0
            );
    return time * time * Du;
}


VectorField DExponentialStiffnessProblem::Load(double t, const Point &x, const Cell &c) const {
    VectorField a = Acceleration(t, x);

    VectorField l(
            exp(x[0]), exp(x[1]), spaceDim > 2 ? exp(x[2]) : 0.0
            );
    return a - t * t * l;
}

double DExponentialStiffnessProblem::TractionStiffness(double t, const Point &x, int sd) const {
    return -1.0;
}

double DExponentialStiffnessProblem::TractionViscosity(double t, const Point &x, int sd) const {
    return 0.0;
}


VectorField DZeroDirichletProblem::Deformation(double time, const Point &x) const {
  VectorField u(
      x[0] * (x[0] - 1.0) * x[1] * (spaceDim > 2 ? x[2] : 1.0),
      x[0] * x[1] * (x[1] - 1.0) * (spaceDim > 2 ? x[2] : 1.0),
      x[0] * x[1] * (spaceDim > 2 ? x[2] * (x[2] - 1.0) : 0.0)
  );
  return time * time * u;
}

VectorField DZeroDirichletProblem::Velocity(double time, const Point &x) const {
  VectorField u(
      x[0] * (x[0] - 1.0) * x[1] * (spaceDim > 2 ? x[2] : 1.0),
      x[0] * x[1] * (x[1] - 1.0) * (spaceDim > 2 ? x[2] : 1.0),
      x[0] * x[1] * (spaceDim > 2 ? x[2] * (x[2] - 1.0) : 0.0)
  );
  return 2.0 * time * u;
}

VectorField DZeroDirichletProblem::Acceleration(double time, const Point &x) const {
  VectorField u(
      x[0] * (x[0] - 1.0) * x[1] * (spaceDim > 2 ? x[2] : 1.0),
      x[0] * x[1] * (x[1] - 1.0) * (spaceDim > 2 ? x[2] : 1.0),
      x[0] * x[1] * (spaceDim > 2 ? x[2] * (x[2] - 1.0) : 0.0)
  );
  return 2.0 * u;
}

Tensor DZeroDirichletProblem::DeformationGradient(double time, const Point &x) const {
  Tensor Du(
      (2*x[0] - 1) * x[1] * (spaceDim > 2 ? x[2] : 1.0), (x[0] - 1)*x[0]* (spaceDim > 2 ? x[2] : 1.0), spaceDim > 2 ? (x[0] - 1) * x[0]*x[1] : 0.0,
      (x[1]-1)*x[1]* (spaceDim > 2 ? x[2] : 1.0), x[0]*(2*x[1]-1)* (spaceDim > 2 ? x[2] : 1.0), spaceDim > 2 ? x[0]*(x[1]-1)*x[1] : 0.0,
      spaceDim > 2 ? x[1]*(x[2]-1)*x[2] : 0.0, spaceDim > 2 ? x[0]*(x[2]-1)*x[2] : 0.0, spaceDim > 2 ? x[0]*x[1]*(2*x[2]-1) : 0.0
  );
  return time * time * Du;
}

VectorField DZeroDirichletProblem::Load(double t, const Point &x, const Cell &c) const {
  VectorField a = Acceleration(t, x);

  VectorField u(2.0 * x[1] * (spaceDim > 2 ? x[2] : 1.0),
                2.0 * x[0] * (spaceDim > 2 ? x[2] : 1.0),
                x[0] * x[1] * (spaceDim > 2 ? 2.0 : 0.0));

  return a - t * t * u;
}

VectorField DPolynomialDirichletProblem::Deformation(double time, const Point &x) const {
  VectorField u(x[0] * x[0],
                x[1] * x[1],
                (spaceDim > 2 ? x[2] * x[2] : 0.0));
  return time * time * u;
}

VectorField DPolynomialDirichletProblem::Velocity(double time, const Point &x) const {
  VectorField u(x[0] * x[0],
                x[1] * x[1],
                (spaceDim > 2 ? x[2] * x[2] : 0.0));
  return 2.0 * time * u;
}

VectorField DPolynomialDirichletProblem::Acceleration(double time, const Point &x) const {
  VectorField u(x[0] * x[0],
                x[1] * x[1],
                (spaceDim > 2 ? x[2] * x[2] : 0.0));
  return 2.0 * u;
}


Tensor DPolynomialDirichletProblem::DeformationGradient(double time, const Point &x) const {
  Tensor Du(
      2*x[0],0,0,
      0,2 * x[1],0,
      0,0,(spaceDim > 2 ? 2 * x[2] : 0.0)
  );
  return time * time * Du;
}

VectorField DPolynomialDirichletProblem::Load(double time, const Point &x, const Cell &c) const {
  VectorField a = Acceleration(time, x);
  VectorField u(2.0,
                2.0,
                (spaceDim > 2 ? 2.0 : 0.0));

  return a - time * time * u;
}


VectorField DExponentialDirichletProblem::Deformation(double time, const Point &x) const {
  VectorField u(cos(x[0]), sin( x[1]), spaceDim > 2 ? sin(x[2]) : 0.0);
  return (time * time) * u;
}

VectorField DExponentialDirichletProblem::Velocity(double time, const Point &x) const {
  VectorField u(cos(x[0]), sin(x[1]), spaceDim > 2 ? sin(x[2]) : 0.0);
  return 2 * time * u;
}

VectorField DExponentialDirichletProblem::Acceleration(double time, const Point &x) const {
  VectorField u(cos(x[0]), sin(x[1]), spaceDim > 2 ? sin(x[2]) : 0.0);
  return 2 * u;
}


Tensor DExponentialDirichletProblem::DeformationGradient(double time, const Point &x) const {
  Tensor Du(
      -sin(x[0]), 0.0, 0.0,
      0.0, cos(x[1]), 0.0,
      0.0, 0.0, spaceDim > 2 ? cos(x[2]) : 0.0
  );
  return (time * time) * Du;
}

VectorField DExponentialDirichletProblem::Load(double time, const Point &x, const Cell &c) const {
  VectorField a = Acceleration(time, x);
  VectorField u(-cos(x[0]), -sin(x[1]), spaceDim > 2 ? -sin(x[2]) : 0.0);

  return a - (time * time) * u;
}


VectorField DPolynomialNeumannProblem::Deformation(double time, const Point &x) const {
  VectorField u(0.5 * x[0] * x[0], 0.5 * x[1] * x[1], 0.5 * x[2] * x[2]);
  return time * time * u;
}

VectorField DPolynomialNeumannProblem::Velocity(double time, const Point &x) const {
  VectorField u(0.5 * x[0] * x[0], 0.5 * x[1] * x[1], 0.5 * x[2] * x[2]);
  return 2.0 * time * u;
}

VectorField DPolynomialNeumannProblem::Acceleration(double time, const Point &x) const {
  VectorField u(0.5 * x[0] * x[0], 0.5 * x[1] * x[1], 0.5 * x[2] * x[2] );
  return 2.0 * u;
}


Tensor DPolynomialNeumannProblem::DeformationGradient(double time, const Point &x) const {
  Tensor Du(x[0], 0.0, 0.0,
            0.0, x[1], 0.0,
            0.0, 0.0, x[2]);
  return time * time * Du;
}

VectorField DPolynomialNeumannProblem::Load(double time, const Point &x, const Cell &c) const {
  VectorField a = Acceleration(time, x);
  VectorField u(1.0, 1.0, 1.0);

  return a - time * time * u;
}

double DPolynomialNeumannProblem::pressure(double t, const Point &x, int bndCondition) const {
  if (bndCondition == 230) {
    return - (t*t*x[0]);
  } else if (bndCondition == 231) {
    return -(t*t*x[1]);
  } else if (bndCondition == 232) {
    return - (t*t*x[2]);
  } else {
    return 0;
  }
}

VectorField DExponentialNeumannProblem::Deformation(double time,
                                                    const Point &x) const {
  using std::numbers::pi;
  VectorField u(cos(0.5 * pi * x[0]), cos(0.5 * pi * x[1]),
                cos(0.5 * pi * x[2]));
  return -2.0 / pi * sin(0.5 * pi * time) * u;
}

VectorField DExponentialNeumannProblem::Velocity(double time,
                                                 const Point &x) const {
  using std::numbers::pi;
  VectorField u(cos(0.5 * pi * x[0]), cos(0.5 * pi * x[1]),
                cos(0.5 * pi * x[2]));
  return -cos(0.5 * pi * time) * u;
}

VectorField DExponentialNeumannProblem::Acceleration(double time,
                                                     const Point &x) const {
  using std::numbers::pi;
  VectorField u(cos(0.5 * pi * x[0]), cos(0.5 * pi * x[1]),
                cos(0.5 * pi * x[2]));
  return 0.5 * pi * sin(0.5 * pi * time) * u;
}

Tensor DExponentialNeumannProblem::DeformationGradient(double time,
                                                       const Point &x) const {
  using std::numbers::pi;
  Tensor Du(-0.5 * pi * sin(0.5 * pi * x[0]), 0.0, 0.0, 0.0,
            -0.5 * pi * sin(0.5 * pi * x[1]), 0.0, 0.0, 0.0,
            -0.5 * pi * sin(0.5 * pi * x[2]));
  return -2.0 / pi * sin(0.5 * pi * time) * Du;
}

VectorField DExponentialNeumannProblem::Load(double time, const Point &x,
                                             const Cell &c) const {
  using std::numbers::pi;
  VectorField a = Acceleration(time, x);
  VectorField u(-0.25 * pi * pi * cos(0.5 * pi * x[0]),
                -0.25 * pi * pi * cos(0.5 * pi * x[1]),
                -0.25 * pi * pi * cos(0.5 * pi * x[2]));

  return a - ((-2.0 / pi * sin(0.5 * pi * time)) * u);
}

double DExponentialNeumannProblem::pressure(double t, const Point &x,
                                            int bndCondition) const {
  using std::numbers::pi;
  if (bndCondition == 230) {
    return -sin(0.5 * pi * t) * sin(0.5 * pi * x[0]);
  } else if (bndCondition == 231) {
    return -sin(0.5 * pi * t) * sin(0.5 * pi * x[1]);
  } else if (bndCondition == 232) {
    return -sin(0.5 * pi * t) * sin(0.5 * pi * x[2]);
  } else {
    return 0;
  }
}
