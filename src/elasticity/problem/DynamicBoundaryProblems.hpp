#ifndef DYNAMICBOUNDARYPROBLEMS_HPP
#define DYNAMICBOUNDARYPROBLEMS_HPP

#include "ElasticityProblem.hpp"

class DynamicBoundaryProblem : public ElasticityProblem {
protected:
  int spaceDim{3};
  std::string is_tet{"Tet"};

public:
  explicit DynamicBoundaryProblem(std::string mName) :
      ElasticityProblem("Laplace", {1.0}) {
    Config::Get("ProblemDimension", spaceDim);
    Config::Get("ProblemGeometry", is_tet);

    if (!meshFromConfig())
      meshesName = mName + std::to_string(spaceDim) + "D" + is_tet;
  }

  bool HasExactSolution() const override {
    return true;
  }
};

class DZeroDirichletProblem : public DynamicBoundaryProblem {
public:
  DZeroDirichletProblem() : DynamicBoundaryProblem("DirichletBlock") {

  }

  std::string Evaluate(const Vector &solution) const override { return ""; };

  VectorField Load(double t, const Point &x, const Cell &c) const override;

  VectorField Deformation(double time, const Point &x) const override;

  VectorField Velocity(double time, const Point &x) const override;

  VectorField Acceleration(double time, const Point &x) const override;

  Tensor DeformationGradient(double time, const Point &x) const override;

  std::string Name() const override {
    return "Dynamic Zero Dirichlet Problem";
  }
};

class DPolynomialDirichletProblem : public DynamicBoundaryProblem {
public:
  DPolynomialDirichletProblem() : DynamicBoundaryProblem("DirichletBlock") {

  }

  std::string Evaluate(const Vector &solution) const override { return ""; };

  VectorField Load(double t, const Point &x, const Cell &c) const override;

  VectorField Deformation(double time, const Point &x) const override;

  VectorField Velocity(double time, const Point &x) const override;

  VectorField Acceleration(double time, const Point &x) const override;

  Tensor DeformationGradient(double time, const Point &x) const override;

  std::string Name() const override {
    return "Dynamic Polynomial Dirichlet Problem";
  }
};

class DExponentialDirichletProblem : public DynamicBoundaryProblem {
public:
  DExponentialDirichletProblem() : DynamicBoundaryProblem("DirichletBlock") {

  }

  std::string Evaluate(const Vector &solution) const override { return ""; };

  VectorField Load(double t, const Point &x, const Cell &c) const override;

  VectorField Deformation(double time, const Point &x) const override;

  VectorField Velocity(double time, const Point &x) const override;

  VectorField Acceleration(double time, const Point &x) const override;

  Tensor DeformationGradient(double time, const Point &x) const override;

  std::string Name() const override {
    return "Dynamic Exponential Dirichlet Problem";
  }
};


class DPolynomialNeumannProblem : public DynamicBoundaryProblem {
protected:
  double pressure(double t, const Point &x, int bndCondition) const override;
public:
  DPolynomialNeumannProblem() : DynamicBoundaryProblem("NeumannBlock") {

  }

  std::string Evaluate(const Vector &solution) const override { return ""; };

  VectorField Load(double t, const Point &x, const Cell &c) const override;

  VectorField Deformation(double time, const Point &x) const override;

  VectorField Velocity(double time, const Point &x) const override;

  VectorField Acceleration(double time, const Point &x) const override;

  Tensor DeformationGradient(double time, const Point &x) const override;


  std::string Name() const override {
    return "Dynamic Polynomial Neumann Problem";
  }
};

class DExponentialNeumannProblem : public DynamicBoundaryProblem {
protected:
  double pressure(double t, const Point &x, int bndCondition) const override;
public:
  DExponentialNeumannProblem() : DynamicBoundaryProblem("NeumannBlock") {

  }

  std::string Evaluate(const Vector &solution) const override { return ""; };

  VectorField Load(double t, const Point &x, const Cell &c) const override;

  VectorField Deformation(double time, const Point &x) const override;

  VectorField Velocity(double time, const Point &x) const override;

  VectorField Acceleration(double time, const Point &x) const override;

  Tensor DeformationGradient(double time, const Point &x) const override;

  std::string Name() const override {
    return "Dynamic Exponential Neumann Problem";
  }
};

class DExponentialStiffnessProblem : public DynamicBoundaryProblem {
    double alpha{0.1};
    double beta{0.1};
    double omega{0.1};

    double factors{0.11};
public:
    DExponentialStiffnessProblem() : DynamicBoundaryProblem("TractionBlock") {
        factors = alpha + beta * omega;
    }


    VectorField Load(double t, const Point &x, const Cell &c) const override;

    double TractionStiffness(double t, const Point &x, int sd) const override;

    double TractionViscosity(double t, const Point &x, int sd) const override;

    VectorField Deformation(double time, const Point &x) const override;

    VectorField Velocity(double time, const Point &x) const override;

    VectorField Acceleration(double time, const Point &x) const override;

    Tensor DeformationGradient(double time, const Point &x) const override;

    std::string Name() const override {
        return "Dynamic Exponential Stiffness Problem";
    }
};


class OscillationProblem : public ElasticityProblem {
protected:
  int spaceDim{3};
  std::string is_tet{"Tet"};

public:
  explicit OscillationProblem(std::string mName = "BenchmarkBeam") :
      ElasticityProblem() {
    Config::Get("ProblemDimension", spaceDim);
    Config::Get("ProblemGeometry", is_tet);

    if (!meshFromConfig())
      meshesName = mName + std::to_string(spaceDim) + "D" + is_tet;
  }

  VectorField Deformation(double time, const Point &x) const override {
    return {0.0, (3-spaceDim) * 0.02 * x[0]* x[0], (spaceDim-2) * 0.02 * x[0]* x[0]};
  }

  bool IsStatic() const override{return false;}

  std::string Name() const override { return "Oscillation Problem"; }
};

#endif //DYNAMICBOUNDARYPROBLEMS_HPP
