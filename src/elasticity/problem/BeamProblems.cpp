#include "BeamProblems.hpp"
#include "DGVectorFieldElement.hpp"

std::array<VectorField, 11> BeamProblem::atEvaluationPoints(const Vector &solution) const {
  std::array<VectorField, 11> values{};
  std::array<int, 11> cellCount{};

  if (contains(elasticityModel, "DG")) {
    for (cell c = solution.cells(); c != solution.cells_end(); ++c) {
      for (int i = 0; i < c.Corners(); ++i) {
        for (int j = 0; j < 11; ++j) {
          if (c.Corner(i) == Point(j, 0.5, 0.5)) {
            DGVectorFieldElement E(solution, *c);
            values[j] += E.VectorValue(c.LocalCorner(i), solution);
            cellCount[j]++;
          }
        }
      }
    }

    for (int j = 0; j < 11; ++j) {
      cellCount[j] = PPM->Sum(cellCount[j]);
      if (cellCount[j] > 0) {
          values[j] = PPM->Sum(values[j]) / cellCount[j];
      }
    }
  } else {
    for (row r = solution.rows(); r != solution.rows_end(); ++r) {
      for (int x = 0; x < 11; ++x) {
        if (r() == Point(x, 0.5, spaceDim > 2 ? 0.5 : 0.0)) {
          auto p = solution.find_procset(r());
          Point value{};
          for (int i = 0; i < spaceDim; ++i) {
            value[i] = solution(r, i) / (p != solution.procsets_end() ? p.size() : 1);
          }
          values[x] = value;
        }
      }
    }
    for (int x = 0; x < values.size(); ++x) {
      values[x] = PPM->Sum(values[x]);
    }
  }

  return values;
}


std::string BeamProblem::Evaluate(const Vector &solution) const {
  std::string evaluation{};
  // ====================================================
  // Evaluate Deformation at (x,0.5,0.5) for x = 0,...,10
  // ====================================================
  auto pointValues = atEvaluationPoints(solution);


  auto values = atEvaluationPoints(solution);


  evaluation += "u_x={";
  for (int x = 0; x < 11; ++x) {
    evaluation += std::to_string(values[x][0]);
    if (x < 10) evaluation += ", ";
  }
  evaluation += "}\n";

  evaluation += "u_y={";
  for (int x = 0; x < 11; ++x) {
    evaluation += std::to_string(values[x][1]);
    if (x < 10) evaluation += ", ";
  }
  evaluation += "}\n";

  evaluation += "u_z={";
  for (int x = 0; x < 11; ++x) {
    evaluation += std::to_string(values[x][2]);
    if (x < 10) evaluation += ", ";
  }
  evaluation += "}\n\n";
  return evaluation;
}

std::vector<double> BeamProblem::EvaluationResults(
    const Vector &solution) const {
  bool test = false;
  auto displacements = atEvaluationPoints(solution);

    for (int x = 0; x < displacements.size() - 1; ++x) {
      if (displacements[x + 1] == (0, 0, 0)) {
        test = true;
      } else {
        if (x == 0) {
          mout << "U(x=" << 0 << ") = " << displacements[0] << endl;
          mout << "U(x=" << x + 1 << ") = " << displacements[x + 1] << endl;
        } else {
          mout << "U(x=" << x + 1 << ") = " << displacements[x + 1] << endl;
        }
      }
    }
  if (test) {
    mout << "There are no suitable points to (x, 0.5, 0.5) found! " << endl << endl;
  }
  return {};
}

std::string BeamProblem::Name() const {
  return "Default Beam Problem";
}

VectorField PressurePullBeamProblem::Deformation(double time, const Point &x) const {
  return VectorField(x[0], 0, 0);
}

Tensor PressurePullBeamProblem::DeformationGradient(double time, const Point &x) const {
  return Tensor(1.0, 0.0, 0.0,
                0.0, 0, 0.0,
                0.0, 0.0, 0);

}

VectorField PressurePushBeamProblem::Deformation(double time, const Point &x) const {
    return VectorField(-2 * x[0], 0, 0);
}

Tensor PressurePushBeamProblem::DeformationGradient(double time, const Point &x) const {
  return Tensor(-2.0, 0.0, 0.0,
                0.0, 0, 0.0,
                0.0, 0.0, 0);

}
