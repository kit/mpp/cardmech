#ifndef BEAMPROBLEMS_HPP
#define BEAMPROBLEMS_HPP

#include <utility>

#include "CardiacData.hpp"
#include "ElasticityProblem.hpp"


class BeamProblem : public ElasticityProblem {
protected:
  int spaceDim{3};
  std::string is_tet{"Tet"};

  std::array<VectorField, 11> atEvaluationPoints(const Vector &solution) const;

  std::string defaultMeshName(std::string meshName = "BenchmarkBeam") const {
    return meshName + std::to_string(spaceDim) + "D" + is_tet;
  };

public:
  explicit BeamProblem(std::string defaultMesh = "") : ElasticityProblem() {
    Config::Get("ProblemDimension", spaceDim);
    Config::Get("ProblemGeometry", is_tet);

    if (!meshFromConfig()) {
      meshesName = defaultMesh.empty() ? defaultMeshName() : defaultMeshName(defaultMesh);
    }
  }

  BeamProblem(const std::string &matName,
              const std::vector<double> &matParameters,
              std::string defaultMesh = "")
      : ElasticityProblem(matName, matParameters) {
    Config::Get("ProblemDimension", spaceDim);
    Config::Get("ProblemGeometry", is_tet);

    if (!meshFromConfig()) {
      meshesName = defaultMesh.empty() ? defaultMeshName() : defaultMeshName(defaultMesh);
    }
  }

  std::string Evaluate(const Vector &solution) const override;

  std::vector<double> EvaluationResults(const Vector &solution) const override;

  std::string Name() const override;
};

template<int degree>
class DirichletBeamProblem : public BeamProblem {
  double lambda{};
  double mu{};

public:
  explicit DirichletBeamProblem(double l = 4.0, double m = 2.0) :
      BeamProblem("Linear", {l, m}, "DirichletBeam"), lambda(l), mu(m) {}

  VectorField Deformation(double time, const Point &x) const override {
    return {
        pow(x[0], degree),
        spaceDim > 1 ? pow(x[1], degree) : 0.0,
        spaceDim > 2 ? pow(x[2], degree) : 0.0
    };
  }

  Tensor DeformationGradient(double time, const Point &x) const override {
    if constexpr (degree < 1) { return Zero; }
    return Tensor(degree * pow(x[0], degree - 1), 0.0, 0.0,
                  0.0, spaceDim > 1 ? degree * pow(x[1], degree - 1) : 0.0, 0.0,
                  0.0, 0.0, spaceDim > 1 ? degree * pow(x[2], degree - 1) : 0.0);
  }

  VectorField Load(double time, const Point &x, const Cell &c) const override {
    if constexpr (degree <= 1) { return zero; }
    double val = (lambda + 2.0 * mu) * degree * (degree - 1);

    return -VectorField(val * pow(x[0], degree - 2),
                        spaceDim > 1 ? val * pow(x[1], degree - 2) : 0.0,
                        spaceDim > 2 ? val * pow(x[2], degree - 2) : 0.0);
  }

  bool HasExactSolution() const override {
    return true;
  }

  std::string Name() const override {
    return "DirichletBeamDegree" + std::to_string(degree);
  }
};

class PressureBeamProblem : public BeamProblem {
  std::array<double, 4> chamberPressure{};
protected:
  double pressure(double t, const Point &x, int bndCondition) const override {
    return chamberPressure[bndCondition % 230];
  }
public:
  PressureBeamProblem() : BeamProblem(), chamberPressure(pressureFromConfig()) {}


  std::string Name() const override {
    return "Cardiac Beam Problem";
  }
};

class PressurePullBeamProblem : public BeamProblem {
  double lambda{};
  double mu{};
protected:
  double pressure(double t, const Point &x, int bndCondition) const override {
    if (bndCondition == 230) {
      return -(2 * mu + lambda);
    } else if (bndCondition == 231 || bndCondition == 232) {
      return -lambda;
    } else {
      return 0;
    }
  }

public :
  explicit PressurePullBeamProblem(double l = 1.0, double m = 1.0) :
      BeamProblem("Linear", {l, m}, "BenchmarkPressureBeam"), lambda(l), mu(m) {}

  VectorField Deformation(double time, const Point &x) const override;

  Tensor DeformationGradient(double time, const Point &x) const override;

  VectorField Load(double time, const Point &z, const Cell &c) const override {
    return VectorField(0, 0, 0);
  }


  bool HasExactSolution() const override {
    return true;
  }

  std::string Name() const override {
    return "Pull Beam Problem";
  }
};

class PressurePushBeamProblem : public PressurePullBeamProblem {
  double lambda{};
  double mu{};
protected:
  double pressure(double t, const Point &x, int bndCondition) const override {
    if (bndCondition == 230) {
      return (4 * mu + 2 * lambda);
    } else if (bndCondition == 231 || bndCondition == 232) {
      return 2 * lambda;
    } else {
      return 0;
    }
  }

public :
  explicit PressurePushBeamProblem(double l = 1.0, double m = 1.0) :
      PressurePullBeamProblem(l, m), lambda(l), mu(m) {}

  VectorField Deformation(double time, const Point &x) const override;

  Tensor DeformationGradient(double time, const Point &x) const override;


  std::string Name() const override {
    return "Push Beam Problem";
  }
};

#endif //BEAMPROBLEMS_HPP
