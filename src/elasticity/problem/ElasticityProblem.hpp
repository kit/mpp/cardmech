#ifndef ELASTICITYPROBLEM_HPP
#define ELASTICITYPROBLEM_HPP

#include "CardMechIProblem.hpp"
#include "Vector.hpp"

#include "CardiacProblem.hpp"
#include "materials/Material.hpp"

namespace mpp {
template <class T, class Alloc, class Pred>
constexpr typename std::vector<T, Alloc>::size_type
erase_if(std::vector<T, Alloc> &c, Pred pred) {
  auto it = std::remove_if(c.begin(), c.end(), pred);
  auto r = std::distance(it, c.end());
  c.erase(it, c.end());
  return r;
}
} // namespace mpp

std::array<double, 4> pressureFromConfig();
double pressureFromConfig(short chamber);

std::vector<double>
findDisplacements(const Vector &u, const std::vector<Point> &points, int index);

std::vector<VectorField> findDisplacements(const Vector &u,
                                           const std::vector<Point> &points);

class IElasticityProblem {
  bool useFlorySplit{false};
  bool isStatic{false};

  std::string elasticityModel{"Conforming"};

public:
  IElasticityProblem() {
    Config::Get("MechDiscretization", elasticityModel);

    Config::Get("Static", isStatic);
    Config::Get("VolumetricSplit", useFlorySplit);
  }

  [[nodiscard]] virtual const std::string &Model() const {
    return elasticityModel;
  }

  virtual const void SetModel(std::string modelName) {
    elasticityModel = modelName;
  }

  [[nodiscard]] virtual bool IsStatic() const { return isStatic; }

  [[nodiscard]] virtual VectorField Load(double t, const Point &x,
                                         const Cell &c) const {
    return zero;
  };

  [[nodiscard]] virtual double ActiveStress(double t, const Point &x) const {
    return 0.0;
  };

  [[nodiscard]] virtual double ActiveStretch(double t, const Point &x) const {
    return 0.0;
  };

  [[nodiscard]] virtual double TractionStiffness(double t, const Point &x,
                                                 int sd) const {
    return 0.0;
  };

  [[nodiscard]] virtual double TractionViscosity(double t, const Point &x,
                                                 int sd) const {
    return 0.0;
  };

  [[nodiscard]] Tensor IsochoricPart(const Tensor &F) const {
    return useFlorySplit ? pow(det(F), -1.0 / 3.0) * F : F;
  }

  [[nodiscard]] virtual VectorField Deformation(double time,
                                                const Point &x) const {
    return zero;
  };

  [[nodiscard]] virtual VectorField Velocity(double time,
                                             const Point &x) const {
    return zero;
  };

  [[nodiscard]] virtual VectorField Acceleration(double time,
                                                 const Point &x) const {
    return zero;
  };

  [[nodiscard]] virtual Tensor DeformationGradient(double time,
                                                   const Point &x) const {
    return Zero;
  };
};

class IPressureProblem {
  mutable double pressureScale{1.0};

protected:
  virtual double pressure(double t, const Point &x, int bndCondition) const {
    return 0.0;
  }

public:
  IPressureProblem() = default;

  [[nodiscard]] virtual double Scale() const { return pressureScale; }

  virtual void Scale(double s) const {
    pressureScale = ((s > 1.0) + (0.0 < s <= 1.0) * s);
  }

  virtual double Pressure(double t, const Point &x, int bndCondition) const {
    return Scale() * pressure(t, x, bndCondition);
  }

  virtual double InitialPressure(const Point &x, int bndCondition) const {
    return 0.0;
  }

  virtual void Initialize(double dt, double t,
                          const std::array<double, 4> &mechVolumes) {}

  virtual void Update(double dt, double t,
                      const std::array<double, 4> &mechVolumes) {}

  virtual void Finalize(double dt, double t,
                        const std::array<double, 4> &mechVolumes) {}

  virtual void Iterate(double dt, double t,
                       const std::array<double, 4> &mechVolumes) {}

  virtual bool Converged(const std::array<double, 4> &mechVolumes) {
    return true;
  }
};

class ElasticityProblem : public CardMechIProblem,
                          public IElasticityProblem,
                          public IPressureProblem {
protected:
  std::string elasticityModel{"Conforming"};

  Material activeMaterial;
  Material passiveMaterial;

  double DGpenalty = 0;

  bool meshFromConfig() const { return meshesName != ""; }

public:
  ElasticityProblem();

  ElasticityProblem(const std::string &matName,
                    const std::vector<double> &matParameters);

  [[nodiscard]] const std::string &GetMeshName() const { return meshesName; }

  virtual const void SetMeshName(std::string meshName) {
    meshesName = meshName;
  }

  [[nodiscard]] const std::string &GetMaterialName(bool active) const {
    return (active ? activeMaterial.Name() : passiveMaterial.Name());
  };

  [[nodiscard]] const Material &GetMaterial(const cell &c) const {
    // TODO: enable different materials
    bool isActive = true;
    return (isActive ? activeMaterial : passiveMaterial);
  }

  [[nodiscard]] Material &GetMaterial() { return activeMaterial; }

  [[nodiscard]] std::pair<const Material &, const Material &>
  GetMaterials() const {
    return std::pair<const Material &, const Material &>{activeMaterial,
                                                         passiveMaterial};
  }

  [[nodiscard]] virtual std::string Evaluate(const Vector &solution) const {
    return "";
  };

  [[nodiscard]] virtual std::vector<std::string> EvaluationMetrics() const {
    return {};
  }

  [[nodiscard]] virtual std::vector<double>
  EvaluationResults(const Vector &solution) const {
    return std::vector<double>{};
  }

  [[nodiscard]] virtual const double &GetDGPenalty() const { return DGpenalty; }

  virtual const void SetDGPenalty(double p) { DGpenalty = p; }
};

class DefaultElasticityProblem : public ElasticityProblem {
  std::array<double, 4> chamberPressure{0.0, 0.0, 0.0, 0.0};

protected:
  [[nodiscard]] double pressure(double t, const Point &x,
                                int bndCondition) const override {
    return chamberPressure[bndCondition % 230];
  }

public:
  DefaultElasticityProblem()
      : ElasticityProblem(), chamberPressure(pressureFromConfig()) {}

  explicit DefaultElasticityProblem(double p)
      : ElasticityProblem(), chamberPressure({p, p, p, p}) {}

  explicit DefaultElasticityProblem(std::array<double, 4> pressure)
      : ElasticityProblem(), chamberPressure(pressure) {}

  [[nodiscard]] std::string Name() const override {
    return "Default Elasticity Problem";
  }
};

class InitialPrestressProblem : public ElasticityProblem {
  const ElasticityProblem &refProblem;

protected:
  [[nodiscard]] double pressure(double t, const Point &x,
                                int bndCondition) const override {
    return refProblem.Pressure(0.0, x, bndCondition);
  }

public:
  InitialPrestressProblem(const ElasticityProblem &reference)
      : refProblem(reference) {}

  [[nodiscard]] VectorField Load(double t, const Point &x,
                                 const Cell &c) const override {
    return refProblem.Load(0.0, x, c);
  }

  [[nodiscard]] double ActiveStretch(double t, const Point &x) const override {
    return refProblem.ActiveStretch(0.0, x);
  }

  [[nodiscard]] double TractionStiffness(double t, const Point &x,
                                         int sd) const override {
    return refProblem.TractionStiffness(t, x, sd);
  }

  [[nodiscard]] double TractionViscosity(double t, const Point &x,
                                         int sd) const override {
    return refProblem.TractionViscosity(t, x, sd);
  }

  [[nodiscard]] VectorField Deformation(double time,
                                        const Point &x) const override {
    return refProblem.Deformation(0.0, x);
  }

  [[nodiscard]] VectorField Velocity(double time,
                                     const Point &x) const override {
    return refProblem.Velocity(0.0, x);
  }

  [[nodiscard]] VectorField Acceleration(double time,
                                         const Point &x) const override {
    return refProblem.Acceleration(0.0, x);
  }

  [[nodiscard]] Tensor DeformationGradient(double time,
                                           const Point &x) const override {
    return refProblem.DeformationGradient(0.0, x);
  }

  [[nodiscard]] const std::string &Model() const override {
    return refProblem.Model();
  }

  [[nodiscard]] bool IsStatic() const override { return refProblem.IsStatic(); }

  [[nodiscard]] std::string Name() const override {
    return "Initial Prestress Problem";
  }
};

std::unique_ptr<ElasticityProblem> GetElasticityProblem();

std::unique_ptr<ElasticityProblem>
GetElasticityProblem(const std::string &problemName);

#endif // ELASTICITYPROBLEM_HPP
