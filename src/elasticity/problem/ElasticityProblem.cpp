#include "ElasticityProblem.hpp"
#include "EllipsoidProblems.hpp"
#include "BeamProblems.hpp"
#include "TestProblems.hpp"
#include "DynamicBoundaryProblems.hpp"
#include "VentricleProblems.hpp"
#include "DGVectorFieldElement.hpp"

std::vector<double> findDisplacements(const Vector &u,
                                      const std::vector<Point> &points,
                                      int index) {
  std::vector<double> displacements(points.size());

  if (!contains(u.GetDisc().DiscName(),"DG")) {
    for (row r = u.rows(); r != u.rows_end(); ++r) {
      auto [i, isNear] = isIn(r(), points);
      if (isNear) {
        auto p = u.find_procset(r());
        if (p != u.procsets_end()) {
          displacements[i] = u(r, index) / p.size();
        } else {
          displacements[i] = u(r, index);
        }
      }
    }
    for (int i = 0; i < displacements.size(); ++i) {
      displacements[i] = PPM->Sum(displacements[i]);
    }
  }
  else {
    std::vector<VectorField> disp(points.size());
    disp = findDisplacements(u,points);
    for (int l = 0; l < displacements.size(); ++l) {
      displacements[l] = disp[l][index] ;
    }
  }
  return displacements;
}

std::vector<VectorField> findDisplacements(const Vector &u,
                                           const std::vector<Point> &points) {
  std::vector<VectorField> displacements(points.size());
  if (!contains(u.GetDisc().DiscName(),"DG")) {
    for (row r = u.rows(); r != u.rows_end(); ++r) {
      auto [i, isNear] = isIn(r(), points);
      if (isNear) {
        auto p = u.find_procset(r());
        for (int j = 0; j < SpaceDimension; ++j) {
          if (p != u.procsets_end()) {
            displacements[i][j] = u(r, j) / p.size();
          } else {
            displacements[i][j] = u(r, j);
          }
        }
      }
    }
    for (int i = 0; i < displacements.size(); ++i) {
      displacements[i] = PPM->Sum(displacements[i]);
    }
  }
  else {
    std::vector<int> cellCount(points.size());

    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      for (int i = 0; i < c.Corners(); ++i) {
        auto [l, isNear] = isIn(c.Corner(i), points);
        if (isNear) {
          DGVectorFieldElement E(u, *c);
          displacements[l] += E.VectorValue(c.LocalCorner(i), u);
          cellCount[l]++;
        }
      }
    }

    for (int l = 0; l < displacements.size(); ++l) {
      int cellCountForL = PPM->Sum(cellCount[l]);
      if (cellCountForL > 0) {
        displacements[l] = PPM->Sum(displacements[l]) / cellCountForL;
      }
    }
  }

  return displacements;
}

ElasticityProblem::ElasticityProblem() : CardMechIProblem(), IElasticityProblem(), IPressureProblem() {
  Config::Get("MechProblemVerbose", verbose);

  std::string aMatName{"Linear"};
  std::string pMatName{"Linear"};
  Config::Get("ActiveMaterial", aMatName);
  Config::Get("PassiveMaterial", pMatName);

  activeMaterial = Material(aMatName);
  passiveMaterial = Material(pMatName);
}

ElasticityProblem::ElasticityProblem(const std::string &matName,
                                     const std::vector<double> &matParameters)
    : CardMechIProblem(), IElasticityProblem(), IPressureProblem() {
  Config::Get("MechProblemVerbose", verbose);
  activeMaterial = Material(matName, matParameters);
  passiveMaterial = Material(matName, matParameters);

}


std::unique_ptr<ElasticityProblem> GetElasticityProblem() {
  std::string problemName{"UNDEFINED"};
  Config::Get("MechProblem", problemName);

  return GetElasticityProblem(problemName);
}

std::unique_ptr<ElasticityProblem> GetElasticityProblem(
    const std::string &problemName) {
  if (problemName == "Default" || problemName == "ReadFromConfig") {
    return std::make_unique<DefaultElasticityProblem>();
  }
  if (problemName == "DirichletBeamDegree1") { return std::make_unique<DirichletBeamProblem<1>>(); }
  if (problemName == "DirichletBeamDegree2") { return std::make_unique<DirichletBeamProblem<2>>(); }
  if (problemName == "DirichletBeamDegree3") { return std::make_unique<DirichletBeamProblem<3>>(); }
  if (problemName == "DirichletBeamDegree4") { return std::make_unique<DirichletBeamProblem<4>>(); }

  if (problemName == "LaplaceDegree1") { return std::make_unique<LaplaceDirichletProblem<1>>(); }
  if (problemName == "LaplaceDegree2") { return std::make_unique<LaplaceDirichletProblem<2>>(); }
  if (problemName == "LaplaceDegree3") { return std::make_unique<LaplaceDirichletProblem<3>>(); }
  if (problemName == "LaplaceDegree4") { return std::make_unique<LaplaceDirichletProblem<4>>(); }

  if (problemName ==
      "Oscillation") { return std::make_unique<OscillationProblem>(); }
  if (problemName ==
      "DynamicDirichletTestZero") { return std::make_unique<DZeroDirichletProblem>(); }
  if (problemName ==
      "DynamicDirichletTestPol") { return std::make_unique<DPolynomialDirichletProblem>(); }
  if (problemName ==
      "DynamicDirichletTestExp") { return std::make_unique<DExponentialDirichletProblem>(); }
  if (problemName ==
      "DynamicNeumannTestPol") { return std::make_unique<DPolynomialNeumannProblem>(); }
  if (problemName ==
      "DynamicNeumannTestExp") { return std::make_unique<DExponentialNeumannProblem>(); }
  if (problemName ==
      "DynamicStiffnessTestExp") { return std::make_unique<DExponentialStiffnessProblem>(); }

  if (problemName == "VolumeBlockX") { return std::make_unique<VolumeBlockProblem>(1.0, 0.0, 0.0); }
  if (problemName == "VolumeBlockY") { return std::make_unique<VolumeBlockProblem>(0.0, 1.0, 0.0); }
  if (problemName == "VolumeBlockZ") { return std::make_unique<VolumeBlockProblem>(0.0, 0.0, 1.0); }
  if (problemName == "VolumeBlockXYZ") {
    return std::make_unique<VolumeBlockProblem>(1.0, 1.0, 1.0);
  }

  if (problemName == "Prestress") { return std::make_unique<PrestressProblem>(); }
  if (problemName == "PrestressBeam") {
    return std::make_unique<PrestressProblem>("BenchmarkBeam3DTet");
  }
  if (problemName == "PrestressEllipsoid") {
    return std::make_unique<PrestressProblem>("FullEllipsoid");
  }

  if (problemName == "ElasticityPullBeam") { return std::make_unique<PressurePullBeamProblem>(); }
  if (problemName == "ElasticityPushBeam") { return std::make_unique<PressurePushBeamProblem>(); }

  if (problemName == "Beam") { return std::make_unique<BeamProblem>(); }
  if (problemName == "CardiacBeam") { return std::make_unique<PressureBeamProblem>(); }
  if (problemName == "Klotz") {
    return std::make_unique<DefaultElasticityProblem>(
        std::array<double, 4>{30.0 * MMHGtoKPASCAL, 0.0, 0.0, 0.0}
    );
  }
  if (problemName == "FullEllipsoid") {
    return std::make_unique<EllipsoidProblems>(FULL, "FullEllipsoid");
  }
  if (problemName == "QuarterEllipsoid") { return std::make_unique<EllipsoidProblems>(QUARTER); }
  if (problemName == "UnorientedEllipsoid") {
    return std::make_unique<EllipsoidProblems>(UNORIENTED);
  }
  if (problemName == "RobinEllipsoid") { return std::make_unique<EllipsoidProblems>(TRACTION); }

  if (problemName == "LeftVentricle") { return std::make_unique<VentricleProblem>(); }
  /*if (problemName == "CirculatoryVentricle") { return std::make_unique<ContractingVentricleProblem>();}*/

  if (problemName == "LandEllipsoid") { return std::make_unique<LandEllipsoidProblem>(); }

  if (problemName == "LandBiventricle") { return std::make_unique<LandBiventricleProblem>(); }

  THROW("Problem with name '" + problemName + "' is unknown.")
}

std::array<double, 4> pressureFromConfig() {
  std::array<double, 4> chamberPressure{};

  double p = 0.0;
  Config::Get("ConstantPressure", p);
  std::fill(chamberPressure.begin(), chamberPressure.end(), p);

  Config::Get("LVPressure", chamberPressure[0]);
  Config::Get("RVPressure", chamberPressure[1]);
  Config::Get("LAPressure", chamberPressure[2]);
  Config::Get("RAPressure", chamberPressure[3]);

  return chamberPressure;
}

double pressureFromConfig(short chamber) {
  double p = 0.0;
  Config::Get("ConstantPressure", p);

  switch (chamber) {
    case 0:
      Config::Get("LVPressure", p);
      break;
    case 1:
      Config::Get("RVPressure", p);
      break;
    case 2:
      Config::Get("LAPressure", p);
      break;
    case 3:
      Config::Get("RAPressure", p);
      break;
    default: Warning("Chamber with index " + std::to_string(chamber) + " not implemented.")
  }
  return p;
}
