#include "VentricleProblems.hpp"
#include "VentriclePoints.hpp"

std::pair<double, double> VentricleProblem::printMyocard(const Vector &u) const {
    std::vector<VectorField> endoDisplacements = findDisplacements(u, ventricleEndocard);
    std::vector<VectorField> epiDisplacements = findDisplacements(u, ventricleEpicard);

    mpp::erase_if(endoDisplacements, [](const VectorField &vf) { return vf == zero; });
    mpp::erase_if(epiDisplacements, [](const VectorField &vf) { return vf == zero; });

    double meanStrain = 0.0;
    double meanThickness = 0.0;
    for (int i = 0; i < ventricleEndocard.size(); ++i) {
        VectorField endoPoint(ventricleEndocard[i]);
        VectorField epiPoint(ventricleEpicard[i]);

        meanStrain += norm(endoPoint + endoDisplacements[i] - epiPoint - epiDisplacements[i]) /
                      norm(endoPoint - epiPoint) - 1.0;
        meanThickness += norm(endoPoint + endoDisplacements[i] - epiPoint - epiDisplacements[i]);
    }

    return {PPM->Sum(meanStrain) / ventricleEndocard.size(),
            PPM->Sum(meanThickness) / ventricleEndocard.size()};
}

std::vector<std::string> VentricleProblem::EvaluationMetrics() const {
  return {"MyocardStrain", "MyocardThickness", "ApexX", "ApexY", "ApexZ"};
}

std::vector<double> VentricleProblem::EvaluationResults(
    const Vector &solution) const {
  auto myocardValues = printMyocard(solution);

  auto apexDisplacement = findDisplacements(solution, {ventricleApex});
  std::vector<double> apexDisplacement0 = {apexDisplacement[0][0],
                                           apexDisplacement[0][1],
                                           apexDisplacement[0][2]};
  std::vector<double> apexDisplacement0AndApex =
      {ventricleApex[0] + apexDisplacement0[0],
       ventricleApex[1] + apexDisplacement0[1],
       ventricleApex[2] + apexDisplacement0[2]};
  mout.PrintInfo("LeftVentricle", verbose,
                 /*PrintInfoEntry("MyocardStrain", meanStrain, 1),*/
                 PrintInfoEntry("ApexLocation", apexDisplacement0AndApex, 1),
                 PrintInfoEntry("ApexDisplacement", apexDisplacement0, 1));

  return {myocardValues.first, myocardValues.second, apexDisplacement[0][0],
          apexDisplacement[0][1], apexDisplacement[0][2]};
}
