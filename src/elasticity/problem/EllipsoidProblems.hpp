#ifndef CARDMECH_ELLIPSOIDPROBLEMS_HPP
#define CARDMECH_ELLIPSOIDPROBLEMS_HPP

#include "ElasticityProblem.hpp"

enum EllipsoidType{
    QUARTER, UNORIENTED, FULL, TRACTION, ORIENTED
};

static std::string meshType(enum EllipsoidType tp){
    switch (tp) {
        case QUARTER: return "QuarterEllipsoid";
        case UNORIENTED: return "UnorientedEllipsoid";
        case ORIENTED: return "tetrahedral/OrientedEllipsoid";
        case TRACTION: return "RobinEllipsoid";
        default: return "FullEllipsoid";
    }
}


class EllipsoidProblems : public ElasticityProblem {
  EllipsoidType type;
  std::vector<Point> innerEvaluationPoints{};
  std::vector<Point> midEvaluationPoints{};
  std::vector<Point> outerEvaluationPoints{};
protected:
  int spaceDim{3};
  std::string is_tet{"Tet"};
  // std::string meshName{};
  std::array<double, 4> chamberPressure{};


  double pressure(double t, const Point &x, int bndCondition) const override {
    return chamberPressure[bndCondition % 230];
  }

public:
    explicit EllipsoidProblems(EllipsoidType eType = FULL, std::string const &defaultMesh="") :
        ElasticityProblem(), type(eType), chamberPressure(pressureFromConfig()) {
        Config::Get("ProblemDimension", spaceDim);
        Config::Get("ProblemGeometry", is_tet);

        if(!meshFromConfig()){
            meshesName =  defaultMesh.empty() ? meshType(eType) : defaultMesh;
        }
      FillEvaluationPoints();
    }

    EllipsoidProblems(const std::string &matName,
                      const std::vector<double> &matPar,
                      EllipsoidType eType = FULL,
                      std::string const &defaultMesh = "")
        : ElasticityProblem(matName, matPar),
          type(eType),
          chamberPressure(pressureFromConfig()) {
      Config::Get("ProblemDimension", spaceDim);
      Config::Get("ProblemGeometry", is_tet);

      if (!meshFromConfig()) {
        meshesName = defaultMesh.empty() ? meshType(eType) : defaultMesh;
      }
      FillEvaluationPoints();
    }

  void FillEvaluationPoints();

  std::vector<double> EvaluationResults(const Vector &solution) const override;

  std::vector<std::string> EvaluationMetrics() const override;

  std::pair<double, double> printMyocard(const Vector &u) const;

  void printLineDisplacements(const Vector &u) const;

  double ActiveStretch(double t, const Point &x) const override;

  std::string Name() const override { return "Ellipsoid Problem"; }
};

class LandEllipsoidProblem : public EllipsoidProblems {
public:
    LandEllipsoidProblem() : EllipsoidProblems("Bonet", {2, 8, 2, 4}, ORIENTED) {
    }

    std::string Evaluate(const Vector &solution) const override { return ""; };

    std::string Name() const override { return "LandEllipsoid Problem"; }
};

class LandBiventricleProblem : public EllipsoidProblems {
public:
  LandBiventricleProblem() : EllipsoidProblems("Bonet", {0.29, 42.0, 16.8, 29.4}) {
  }

  std::vector<double> EvaluationResults(const Vector &solution) const override;

  std::string Name() const override { return "LandBiventricle Problem"; }
};

#endif //CARDMECH_ELLIPSOIDPROBLEMS_HPP
