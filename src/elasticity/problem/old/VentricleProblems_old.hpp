#pragma clang diagnostic push
#pragma ide diagnostic ignored "modernize-use-nodiscard"
#ifndef VENTRICLEPROBLEMS_HPP
#define VENTRICLEPROBLEMS_HPP


#include "ElasticityProblemOld.hpp"

class VentricleProblem : public ElasticityProblemOld {
public:
  explicit VentricleProblem() :
      ElasticityProblemOld("Ventricle Problem") {

  }

  VentricleProblem(const std::string &matName,
                   const std::vector<double> &matPar)
      : ElasticityProblemOld("Ventricle Problem", matName, matPar) {}

  std::string MeshName() const override {
    return meshFromConfig() ? meshName : "LeftVentricle";
  }

  std::vector<double> EvaluationResults(const Vector &solution) const override;

  VectorField Traction(double t, const Point &x, const VectorField &u, const VectorField &v,
                       const VectorField &n, int sd) const override;

  std::vector<std::string> EvaluationMetrics() const override;

  std::pair<double, double> printMyocard(const Vector &u) const;
};


class ContractingVentricleProblem : public VentricleProblem {
  // Robin Boundary
  //std::array<double, 8> robinParams{10.0, 0.0, 5.0, 0.0, 1500.0, 1e-4, 1.0, 0.0};
  std::array<double, 8> robinParams{0.2, 0.0, 5.0e-3, 0.0, 1500.0, 1e-4, 1.0, 0.0};

  double robinParam(int bc, int index) const {
    return robinParams[(bc == ROBIN_BC_EPI) * 4 + index];
  };

  double k{0.1};
  double c{5e-3};
public:
  explicit ContractingVentricleProblem() :
      VentricleProblem() {
    Config::Get("TractionK", k);
    Config::Get("TractionC", c);

  }

  ContractingVentricleProblem(const std::string &matName,
                              const std::vector<double> &matPar)
      : VentricleProblem(matName, matPar) {
    Config::Get("TractionK", k);
    Config::Get("TractionC", c);
  }

  VectorField Traction(double t, const Point &x, const VectorField &u, const VectorField &v,
                       const VectorField &n, int bc) const override;
};


#endif //VENTRICLEPROBLEMS_HPP

#pragma clang diagnostic pop