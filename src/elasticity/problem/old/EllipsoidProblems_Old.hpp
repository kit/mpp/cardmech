#ifndef ELLIPSOIDPROBLEMS_HPP
#define ELLIPSOIDPROBLEMS_HPP


#include "ElasticityProblemOld.hpp"


enum EllipsoidType{
  QUARTER, UNORIENTED, FULL, TRACTION
};

static std::string meshType(enum EllipsoidType tp){
  switch (tp) {
    case QUARTER: return "QuarterEllipsoid";
    case UNORIENTED: return "UnorientedEllipsoid";
    case TRACTION: return "RobinEllipsoid";
    default: return "FullEllipsoid";
  }
}

class EllipsoidProblem : public ElasticityProblemOld {
  EllipsoidType type;
protected:
  int spaceDim{3};
  std::string meshName{};

public:
  explicit EllipsoidProblem(EllipsoidType eType = FULL) :
      ElasticityProblemOld("Ellipsoid Problem"), type(eType){

  }

  EllipsoidProblem(const std::string &matName,
                   const std::vector<double> &matPar,
                   EllipsoidType eType = FULL)
      : ElasticityProblemOld("Ellipsoid Problem", matName, matPar),
        type(eType) {}

  std::string MeshName() const override {
    return meshFromConfig() ? meshName : meshType(type);
  }

  std::vector<std::string> EvaluationMetrics() const override;
  std::vector<double> EvaluationResults(const Vector &solution) const override;

  std::pair<double, double> printMyocard(const Vector &u) const;

  void printLineDisplacements(const Vector &u) const;

  double ActiveStretch(double t, const Point &x) const override;
};

class LandEllipsoidProblem : public EllipsoidProblem {
public:
  LandEllipsoidProblem() : EllipsoidProblem("Bonet", {2000, 8, 4, 2}) {

  }

  std::string Evaluate(const Vector &solution) const override { return ""; };
};


#endif //ELLIPSOIDPROBLEMS_HPP
