#include "EllipsoidProblems_Old.hpp"
#include "EllipsoidPoints.hpp"

std::vector<std::string> EllipsoidProblem::EvaluationMetrics() const {
  return {"MyocardStrain", "MyocardThickness", "ApexX", "ApexY", "ApexZ"};
}

std::vector<double> EllipsoidProblem::EvaluationResults(
    const Vector &solution) const {
  printLineDisplacements(solution);
  auto myoCardvalues = printMyocard(solution);

  Point apex = outer_line_x[0];
  auto z_displacement = findDisplacements(solution, {apex});

  /*vout(1).PrintInfo("Ellipsoid", verbose,
                    PrintInfoEntry("MyocardStrain", myoCardvalues.first, 1),
                    PrintInfoEntry("MyocardThickness", myoCardvalues.second, 1),
                    PrintInfoEntry("ApexLocation", apex_zpos, 1),
                    PrintInfoEntry("ApexDisplacement", z_displacement[0], 1));*/

  return {myoCardvalues.first, myoCardvalues.second, z_displacement[0][0], z_displacement[0][1], z_displacement[0][2]};
}

std::pair<double, double> EllipsoidProblem::printMyocard(const Vector &u) const {
  std::vector<VectorField> endoDisplacements = findDisplacements(u, ellipsoidEndocard);
  std::vector<VectorField> epiDisplacements = findDisplacements(u, ellipsoidEpicard);

  mpp::erase_if(endoDisplacements, [](const VectorField &vf) { return vf == zero; });
  mpp::erase_if(epiDisplacements, [](const VectorField &vf) { return vf == zero; });

  double meanStrain = 0.0;
  double meanThickness = 0.0;
  for (int i = 0; i < ellipsoidEndocard.size(); ++i) {
    VectorField endoPoint(ellipsoidEndocard[i]);
    VectorField epiPoint(ellipsoidEpicard[i]);

    meanStrain += norm(endoPoint + endoDisplacements[i] - epiPoint - epiDisplacements[i]) /
                  norm(endoPoint - epiPoint) - 1.0;
    meanThickness += norm(endoPoint + endoDisplacements[i] - epiPoint - epiDisplacements[i]);
  }

  return {PPM->Sum(meanStrain) / ellipsoidEndocard.size(), PPM->Sum(meanThickness) / ellipsoidEndocard.size()};


/*
  for (int i = 0; i < 20; ++i) {
    VectorField endoPoint(ellipsoidEndocard[i]);
    VectorField epiPoint(ellipsoidEpicard[i]);
    vout(1) << "Reference Thickness=" << norm(endoPoint - epiPoint)
            << " - Deformed Thickness="
            << norm(endoPoint + endoDisplacements[i] - epiPoint - epiDisplacements[i])
            << endl;
  }
*/
}

void EllipsoidProblem::printLineDisplacements(const Vector &u) const {
  auto x_displacements = findDisplacements(u, outer_line_x, 1);
  auto y_displacements = findDisplacements(u, outer_line_y, 0);
  auto z_displacements = findDisplacements(u, outer_line_y, 2);
  vout(1) << endl << endl << "outer z values: " << endl;
  std::string outer_z_values{};
  for (int i = 0; i < x_displacements.size(); ++i) {
    vout(1) << outer_line_x[i][2] << "\t";
    outer_z_values += std::to_string(outer_line_x[i][2]) + "\t" + "\t";
  }

  std::string outer_y_displacement{};
  vout(1) << endl << "outer Y Displacement: " << endl;
  for (int i = 0; i < x_displacements.size(); ++i) {
    vout(1) << x_displacements[i] << "\t";
    outer_y_displacement += std::to_string(x_displacements[i]) + "\t" + "\t";
  }

  std::string outer_x_displacements{};
  vout(1) << endl << "outer X Displacement: " << endl;
  for (int i = 0; i < y_displacements.size(); ++i) {
    vout(1) << y_displacements[i] << "\t";
    outer_x_displacements += std::to_string(y_displacements[i]) + "\t" + "\t";
  }

  std::string outer_z_displacement{};
  vout(1) << endl << "outer Z Displacement: " << endl;
  for (int i = 0; i < z_displacements.size(); ++i) {
    vout(1) << z_displacements[i] << "\t";
    outer_z_displacement += std::to_string(z_displacements[i]) + "\t" + "\t";
  }

  x_displacements = findDisplacements(u, mid_line_x, 1);
  y_displacements = findDisplacements(u, mid_line_y, 0);
  z_displacements = findDisplacements(u, mid_line_y, 2);
  std::string mid_z_values{};
  std::string mid_Y_displacements{};
  std::string mid_X_displacements{};
  std::string mid_z_displacements{};
  vout(1) << endl << endl << "mid z values: " << endl;
  for (int i = 0; i < x_displacements.size(); ++i) {
    vout(1) << mid_line_x[i][2] << "\t";
    mid_z_values += std::to_string(mid_line_x[i][2]) + "\t" + "\t";
  }
  vout(1) << endl << "mid Y Displacement: " << endl;
  for (int i = 0; i < x_displacements.size(); ++i) {
    vout(1) << x_displacements[i] << "\t";
    mid_Y_displacements += std::to_string(x_displacements[i]) + "\t" + "\t";
  }
  vout(1) << endl << "mid X Displacement: " << endl;
  for (int i = 0; i < y_displacements.size(); ++i) {
    vout(1) << y_displacements[i] << "\t";
    mid_X_displacements += std::to_string(y_displacements[i]) + "\t" + "\t";
  }
  vout(1) << endl << "mid z Displacement: " << endl;
  for (int i = 0; i < z_displacements.size(); ++i) {
    vout(1) << z_displacements[i] << "\t";
    mid_z_displacements += std::to_string(z_displacements[i]) + "\t" + "\t";
  }

  x_displacements = findDisplacements(u, inner_line_x, 1);
  y_displacements = findDisplacements(u, inner_line_y, 0);
  z_displacements = findDisplacements(u, inner_line_y, 2);
  std::string inner_z_values{};
  std::string inner_Y_displacement{};
  std::string inner_X_displacement{};
  std::string inner_z_displacements{};
  vout(1) << endl << endl << "inner z values: " << endl;
  for (int i = 0; i < x_displacements.size(); ++i) {
    vout(1) << inner_line_x[i][2] << "\t";
    inner_z_values += std::to_string(inner_line_x[i][2]) + "\t" + "\t";
  }
  vout(1) << endl << "inner Y Displacement: " << endl;
  for (int i = 0; i < x_displacements.size(); ++i) {
    vout(1) << x_displacements[i] << "\t";
    inner_Y_displacement += std::to_string(x_displacements[i]) + "\t" + "\t";
  }
  vout(1) << endl << "inner X Displacement: " << endl;
  for (int i = 0; i < y_displacements.size(); ++i) {
    vout(1) << y_displacements[i] << "\t";
    inner_X_displacement += std::to_string(y_displacements[i]) + "\t" + "\t";
  }
  vout(1) << endl << "inner z Displacement: " << endl;
  for (int i = 0; i < z_displacements.size(); ++i) {
    vout(1) << z_displacements[i] << "\t";
    inner_z_displacements += std::to_string(z_displacements[i]) + "\t" + "\t";
  }

  vout(1) << endl << endl << "Apex z Movement: ";
  std::vector<Point> apex{outer_line_x[0]};
  auto z_displacement = findDisplacements(u, apex, 2);
  vout(1) << z_displacement[0] << endl;
  vout(1).PrintInfo("Ellipsoid Data", 1,
                    PrintInfoEntry("outer z values", "[" + outer_z_values + "]"),
                    PrintInfoEntry("outer Y Displacement", "[" + outer_y_displacement + "]"),
                    PrintInfoEntry("outer X Displacement", "[" + outer_x_displacements + "]"),
                    PrintInfoEntry("mid z values", "[" + mid_z_values + "]"),
                    PrintInfoEntry("mid Y Displacement", "[" + mid_Y_displacements + "]"),
                    PrintInfoEntry("mid X Displacement", "[" + mid_X_displacements + "]"),
                    PrintInfoEntry("inner z values", "[" + inner_z_values + "]"),
                    PrintInfoEntry("inner Y Displacement", "[" + inner_Y_displacement + "]"),
                    PrintInfoEntry("inner X Displacement", "[" + inner_X_displacement + "]"),
                    PrintInfoEntry("Apex z Movement", z_displacement[0]));

}

double EllipsoidProblem::ActiveStretch(double t, const Point &x) const {
  double z = -(x[0] + 20.0) / 100.0 + t;

  return z < 0.0 ? 0.0 : -0.03 * (exp(-(z - 0.15) / 0.02) / (0.02 * (1 + exp(-(z - 0.15) / 0.02)) *
                                                             (1 + exp(-(z - 0.15) / 0.02))));
}

