#ifndef ELASTICITYPROBLEMOLD_H
#define ELASTICITYPROBLEMOLD_H

#include <utility>

#include "Algebra.hpp"
#include "materials/Material.hpp"
#include "CardiacData.hpp"
#include "CardiacProblem.hpp"


class cell;

// TODO: This should probably belong in the MPP core
template<typename T>
int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

// This function is only in the std:: library since C++20
namespace mpp {
  template<class T, class Alloc, class Pred>
  constexpr typename std::vector<T, Alloc>::size_type
  erase_if(std::vector<T, Alloc> &c, Pred pred) {
    auto it = std::remove_if(c.begin(), c.end(), pred);
    auto r = std::distance(it, c.end());
    c.erase(it, c.end());
    return r;
  }
}

std::vector<double>
findDisplacements(const Vector &u, const std::vector<Point> &points, int index);

std::vector<VectorField>
findDisplacements(const Vector &u, const std::vector<Point> &points);


class ElasticityProblemOld : public CardiacProblem {
  bool useFlorySplit{false};
  bool isStatic{false};
protected:
  std::string elasticityModel{"Passive"};

  Material activeMaterial;
  Material passiveMaterial;

  std::string aMatName = "Linear";
  std::string pMatName = "Linear";

public:
  explicit ElasticityProblemOld(std::string pName) : CardiacProblem(std::move(pName), "Mech") {
    Config::Get("MechDiscretization", elasticityModel);

    Config::Get("Static", isStatic);
    Config::Get("ActiveMaterial", aMatName);
    Config::Get("PassiveMaterial", pMatName);
    Config::Get("VolumetricSplit", useFlorySplit);

    activeMaterial = Material(aMatName);
    passiveMaterial = Material(pMatName);
  };

  ElasticityProblemOld(std::string pName, const std::string &aMat, const std::string &pMat)
      : CardiacProblem(std::move(pName), "Mech"), aMatName(aMat), pMatName(pMat),
        activeMaterial(aMat), passiveMaterial(pMat) {
    Config::Get("MechDiscretization", elasticityModel);
    Config::Get("VolumetricSplit", useFlorySplit);
  }

  ElasticityProblemOld(std::string pName, const std::string &aMat,
                       const std::vector<double> &aPar, const std::string &pMat,
                       const std::vector<double> &pPar)
      : CardiacProblem(std::move(pName), "Mech"),
        aMatName(aMat),
        pMatName(pMat),
        activeMaterial(aMat, aPar),
        passiveMaterial(pMat, pPar) {
    Config::Get("MechDiscretization", elasticityModel);
    Config::Get("VolumetricSplit", useFlorySplit);
  }

  ElasticityProblemOld(const std::string &pName, const std::string &matName,
                       const std::vector<double> &matPar)
      : ElasticityProblemOld(pName, matName, matPar, matName, matPar) {}

  /// Right hand side in domain
  virtual VectorField Load(double t, const Point &x, const Cell &c) const { return zero; };

  virtual double ActiveStress(double t, const Point &x) const { return 0.0; };

  virtual double ActiveStretch(double t, const Point &x) const { return 0.0; };

  /// Boundary conditions
  virtual VectorField Traction(double t, const Point &x, const VectorField &u, const VectorField &v,
                               const VectorField &n, int sd) const { return 0.0; };


  Tensor IsochoricPart(const Tensor &F) const {
    return useFlorySplit ? pow(det(F), -1.0 / 3.0) * F : F;
  }

  /**
   * Returns non-zero dirichlet boundary deformation.
   */
  virtual VectorField Deformation(double time, const Point &x) const { return zero; };

  virtual VectorField Velocity(double time, const Point &x) const { return zero; };

  virtual VectorField Acceleration(double time, const Point &x) const { return zero; };

  virtual Tensor DeformationGradient(double time, const Point &x) const { return Zero; };

  const std::string &GetMaterialName(bool active) const {
    return (active ? aMatName : pMatName);
  };

  const Material &GetMaterial(const cell &c) const {
    //TODO: enable different materials
    bool isActive = true;
    return (isActive ? activeMaterial : passiveMaterial);
  }

  /// Used to decide which discretization to use
  virtual const std::string &Model() const { return elasticityModel; }

  virtual bool IsStatic() const {return isStatic;}

};


std::unique_ptr<ElasticityProblemOld> getElasticityProblemOld();

#endif //ELASTICITYPROBLEM_H
