#include "WindkesselProblems.hpp"

#include <algorithm>

SingleChamberWindkesselProblem::SingleChamberWindkesselProblem(double lv, double rv, double la,
                                                               double ra)
    : PressureProblem("Windkessel Pressure", {lv, rv, la, ra}),
      pInit(lv) {
}

SingleChamberWindkesselProblem::SingleChamberWindkesselProblem(std::array<double, 4> p) :
    PressureProblem("Windkessel Pressure", p),
    pInit(p[0]) {

}

SingleChamberWindkesselProblem::SingleChamberWindkesselProblem() :
    PressureProblem("Windkessel Pressure") {
  // Default to a constant pressure Problem
  double p = 0.0;
  Config::Get("ConstantPressure", p);
  std::fill(pressure.begin(), pressure.end(), p);

  Config::Get("LVPressure", pressure[0]);
  Config::Get("RVPressure", pressure[1]);
  Config::Get("LAPressure", pressure[2]);
  Config::Get("RAPressure", pressure[3]);

  pInit = pressure[0];
}

double SingleChamberWindkesselProblem::Pressure(double t, const Point &x, int bndCondition) const {
  return pressure[bndCondition % 230];
}

void SingleChamberWindkesselProblem::Initialize(double dt, double t,
                                                const std::array<double, 4> &mechVolumes) {
  pPrev = pressure[0] * KPASCALtoMMHG;
  vPrev = mechVolumes[0];
}

double SingleChamberWindkesselProblem::ejection(double dv, double pressure) const {
  return (-pressure / rEjection - dv) / cEjection;
}

void SingleChamberWindkesselProblem::Update(double dt, double t,
                                            const std::array<double, 4> &mechVolumes) {
  if (currentPhase == CONTRACTION) {
    pressure[0] = pPrev - (mechVolumes[0] - vPrev) / cContraction;
  } else if (currentPhase == EJECTION) {
    // Using RK2
    double dv = (mechVolumes[0] - pPrev) / dt;
    double k = pPrev + 0.5 * dt * ejection(dv, pPrev);
    pressure[0] = pPrev + dt * ejection(dv, k);
    nextPhase = dv >= 0;
  } else if (currentPhase == RELAXATION) {
    pressure[0] = pPrev - (mechVolumes[0] - vPrev) / cContraction;
  } else if (currentPhase == FILLING) {
    pressure[0] = pPrev + dt * dpFilling;
  }

  pressure[0] *= MMHGtoKPASCAL;
}

void SingleChamberWindkesselProblem::Finalize(double dt, double t,
                                              const std::array<double, 4> &mechVolumes) {
  if (currentPhase == CONTRACTION) {
    if (pressure[0] >= 95.0 * MMHGtoKPASCAL) {
      currentPhase = EJECTION;
    }
  } else if (currentPhase == EJECTION) {
    if (nextPhase) currentPhase = RELAXATION;
  } else if (currentPhase == RELAXATION) {
    if (pressure[0] <= 5.0 * MMHGtoKPASCAL) {
      currentPhase = FILLING;
      double tFilling = tCycle - t;
      double pFilling = pInit - pressure[0] * KPASCALtoMMHG;
      dpFilling = pFilling / tFilling;
    }
  } else if (currentPhase == FILLING) {
    if (t >= tCycle) {
      currentPhase = CONTRACTION;
    }
  }
}

bool SingleChamberWindkesselProblem::Converged(const std::array<double, 4> &mechVolumes) {
  switch (currentPhase) {
    case CONTRACTION:
      return abs(vPrev - mechVolumes[0]) / mechVolumes[0] < volEps;
    case RELAXATION:
      return abs(vPrev - mechVolumes[0]) / mechVolumes[0] < volEps;
    default:
      return true;
  }
}


FourChamberWindkesselProblem::FourChamberWindkesselProblem(double lv, double rv, double la,
                                                           double ra)
    : PressureProblem("Scaled Pressure", {lv, rv, la, ra}),
      pVec(4), pPrev(4), rVec(4), rPrev(4), cMat(4, 4) {
}

FourChamberWindkesselProblem::FourChamberWindkesselProblem(std::array<double, 4> p) :
    PressureProblem("Scaled Pressure", p),
    pVec(4), pPrev(4), rVec(4), rPrev(4), cMat(4, 4) {

}

FourChamberWindkesselProblem::FourChamberWindkesselProblem() :
    PressureProblem("Windkessel Pressure"),
    pVec(4), pPrev(4), rVec(4), rPrev(4), cMat(4, 4) {
  // Default to a constant pressure Problem
  double p = 0.0;
  Config::Get("ConstantPressure", p);
  std::fill(pressure.begin(), pressure.end(), p);

  Config::Get("LVPressure", pressure[0]);
  Config::Get("RVPressure", pressure[1]);
  Config::Get("LAPressure", pressure[2]);
  Config::Get("RAPressure", pressure[3]);


}

double FourChamberWindkesselProblem::Pressure(double t, const Point &x, int bndCondition) const {
  return pVec[bndCondition % 230];
}

void FourChamberWindkesselProblem::Initialize(double dt, double t,
                                              const std::array<double, 4> &mechVolumes) {
  cMat.Identity();

  if (prevPressures.size() < 4) {
    for (int i = 0; i < 4; ++i)
      pVec[i] = pressure[i] + (prevPressures.empty() ? 1 : 0);
  } else {
    // Assumes equidistant time steps dt
    pVec = pVec + dt * (
        55.0 / 24.0 * prevPressures[3]
        - 59.0 / 24.0 * prevPressures[2]
        + 37.0 / 24.0 * prevPressures[1]
        - 9.0 / 24.0 * prevPressures[0]);
  }

  pPrev = pVec;
  rPrev = rVec;
}

void FourChamberWindkesselProblem::Update(double dt, double t,
                                          const std::array<double, 4> &mechVolumes) {

  RVector dP = pVec - pPrev;
  RVector dR = rVec - rPrev;

  cMat += product(dP - cMat * dR, dP * cMat) / (dP * (cMat * dR));

  pPrev = pVec;
  rPrev = rVec;

  pVec -= cMat * rVec;

  circModel.SolveStep(dt, t, circVolumes, pressure);
}

void FourChamberWindkesselProblem::Finalize(double dt, double t,
                                            const std::array<double, 4> &mechVolumes) {
  RVector dP(4);
  for (int i = 0; i < 4; ++i) {
    dP[i] = (pVec[i] - pressure[i]) / dt;
    pressure[i] = pVec[i];
  }

  if (prevPressures.size() < 4) {
    prevPressures.emplace_back(std::move(dP));
  } else {
    // Shifts elements in prevPressures to the left
    std::rotate(prevPressures.begin(), prevPressures.begin() + 1, prevPressures.end());
    prevPressures[3] = std::move(dP);
  }
}

bool FourChamberWindkesselProblem::Converged(const std::array<double, 4> &mechVolumes) {
  for (int i = 0; i < 4; ++i) {
    rVec[i] = mechVolumes[i] - volumes[i];
    if (abs(rVec[i]) > volEps) {
      return false;
    }
  }
  return true;
}

