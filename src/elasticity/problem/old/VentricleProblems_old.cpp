#include "VentricleProblems_old.hpp"
#include "VentriclePoints.hpp"


std::pair<double, double> VentricleProblem::printMyocard(const Vector &u) const {
  std::vector<VectorField> endoDisplacements = findDisplacements(u, ventricleEndocard);
  std::vector<VectorField> epiDisplacements = findDisplacements(u, ventricleEpicard);

  mpp::erase_if(endoDisplacements, [](const VectorField &vf) { return vf == zero; });
  mpp::erase_if(epiDisplacements, [](const VectorField &vf) { return vf == zero; });

  double meanStrain = 0.0;
  double meanThickness = 0.0;
  for (int i = 0; i < ventricleEndocard.size(); ++i) {
    VectorField endoPoint(ventricleEndocard[i]);
    VectorField epiPoint(ventricleEpicard[i]);

    meanStrain += norm(endoPoint + endoDisplacements[i] - epiPoint - epiDisplacements[i]) /
                  norm(endoPoint - epiPoint) - 1.0;
    meanThickness += norm(endoPoint + endoDisplacements[i] - epiPoint - epiDisplacements[i]);
  }

  return {PPM->Sum(meanStrain) / ventricleEndocard.size(),
          PPM->Sum(meanThickness) / ventricleEndocard.size()};
}

std::vector<std::string> VentricleProblem::EvaluationMetrics() const {
  return {"MyocardStrain", "MyocardThickness", "ApexX", "ApexY", "ApexZ"};
}
std::vector<double> VentricleProblem::EvaluationResults(
    const Vector &solution) const {
  auto myocardValues = printMyocard(solution);

  auto apexDisplacement = findDisplacements(solution, {ventricleApex});

  mout.PrintInfo("LeftVentricle", verbose,
                 /*PrintInfoEntry("MyocardStrain", meanStrain, 1),*/
                 PrintInfoEntry("ApexLocation", ventricleApex + apexDisplacement[0], 1),
                 PrintInfoEntry("ApexDisplacement", apexDisplacement[0], 1));

  return {myocardValues.first, myocardValues.second, apexDisplacement[0][0], apexDisplacement[0][1], apexDisplacement[0][2]};
}

VectorField
VentricleProblem::Traction(double t, const Point &x, const VectorField &u, const VectorField &v,
                           const VectorField &n, int bc) const {

  return zero;

}

VectorField
ContractingVentricleProblem::Traction(double t, const Point &x, const VectorField &u, const VectorField &v,
                               const VectorField &n, int bc) const {
  /*Tensor N = Product(n, n);
  VectorField orth = robinParam(bc, 0) * u + robinParam(bc, 2) * v;
  VectorField parallel = robinParam(bc, 1) * u + robinParam(bc, 3) * v;*/

  return n*(k*(u*n) + c * (v*n));
}
