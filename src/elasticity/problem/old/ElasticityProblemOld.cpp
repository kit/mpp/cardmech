#include "ElasticityProblemOld.hpp"
#include "../ElasticityBenchmarkProblem.hpp"
#include "EllipsoidProblems_Old.hpp"
#include "VentricleProblems_old.hpp"
#include "../DynamicProblems.hpp"

std::vector<double> findDisplacements(const Vector &u,
                                      const std::vector<Point> &points,
                                      int index) {
  std::vector<double> displacements(points.size());
  for (row r = u.rows(); r != u.rows_end(); ++r) {
    auto[i, isNear] = isIn(r(), points);
    if (isNear) {
      auto p = u.find_procset(r());
      if (p != u.procsets_end()) {
        displacements[i] = u(r, index) / p.size();
      } else {
        displacements[i] = u(r, index);
      }
    }
  }
  for (int i = 0; i < displacements.size(); ++i) {
    displacements[i] = PPM->Sum(displacements[i]);
  }
  return displacements;
}

std::vector<VectorField> findDisplacements(const Vector &u,
                                           const std::vector<Point> &points) {
  std::vector<VectorField> displacements(points.size());
  for (row r = u.rows(); r != u.rows_end(); ++r) {
    auto[i, isNear] = isIn(r(), points);
    if (isNear) {
      auto p = u.find_procset(r());
      for (int j = 0; j < SpaceDimension; ++j) {
        if (p != u.procsets_end()) {
          displacements[i][j] = u(r, j) / p.size();
        } else {
          displacements[i][j] = u(r, j);
        }
      }
    }
  }
  for (int i = 0; i < displacements.size(); ++i) {
    displacements[i] = PPM->Sum(displacements[i]);
  }
  return displacements;
}

std::unique_ptr<ElasticityProblemOld> getElasticityProblemOld() {
  return getElasticityProblemOld(
      problemIndex[Config::GetWithoutCheck<std::string>("MechProblem")]);
}

std::unique_ptr<ElasticityProblemOld> getElasticityProblemOld(const ProblemType cProblem) {
  std::string meshName = "UnitBlock3DQuad";
  Config::Get("Mesh", meshName);

  switch (cProblem) {
    case ElasticityBenchmarkLinear:
      return std::make_unique<LinearBenchmarkProblem>(false,
                                                      std::vector<double>{1,
                                                                          1});
    case ElasticityBenchmarkQuadratic:
      return std::make_unique<
          QuadraticBenchmarkProblem>(false, std::vector<double>{1, 1});
    case FullEllipsoid:
      return std::make_unique<EllipsoidProblem>(FULL);
    case QuarterEllipsoid:
      return std::make_unique<EllipsoidProblem>(QUARTER);
    case UnorientedEllipsoid:
      return std::make_unique<EllipsoidProblem>(UNORIENTED);
    case RobinEllipsoid:
      return std::make_unique<EllipsoidProblem>(TRACTION);
    case LeftVentricle:
      return std::make_unique<VentricleProblem>();
    case ContractingVentricle:
      return std::make_unique<ContractingVentricleProblem>();
    case CirculatoryVentricle:
      return std::make_unique<ContractingVentricleProblem>();
    default:
      THROW("Problem name " + std::to_string(cProblem) + " unknown.")
  }
}
