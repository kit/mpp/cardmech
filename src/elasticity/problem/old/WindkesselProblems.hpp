#ifndef WINDKESSELPROBLEMS_HPP
#define WINDKESSELPROBLEMS_HPP

#include "PressureProblem.hpp"
#include "pressure_bnd/MCircModel.hpp"

enum circPhase {
  CONTRACTION, EJECTION, RELAXATION, FILLING
};

/// Adapated from Dede et al. (2018)
class SingleChamberWindkesselProblem : public PressureProblem {
  double volEps{1e-2};
  circPhase currentPhase{CONTRACTION};

  double pInit{8.0246 * MMHGtoKPASCAL};
  double vPrev{0.0};
  double pPrev{0.0};

  double tCycle{1.0};

  double cContraction{900 * MMHGtoKPASCAL};
  double cEjection{1.0};
  double rEjection{1.0};

  double dpFilling{0.0};

  bool nextPhase{false};

  double ejection(double dv, double pressure) const;

public:
  SingleChamberWindkesselProblem(double lv, double rv, double la, double ra);

  explicit SingleChamberWindkesselProblem(std::array<double, 4> p);

  SingleChamberWindkesselProblem();

  double Pressure(double t, const Point &x, int bndCondition) const override;

  void Initialize(double dt, double ts, const std::array<double, 4> &mechVolumes) override;

  void Update(double dt, double t, const std::array<double, 4> &mechVolumes) override;

  void Finalize(double dt, double t, const std::array<double, 4> &mechVolumes) override;

  bool Converged(const std::array<double, 4> &mechVolumes) override;

};

class FourChamberWindkesselProblem : public PressureProblem {
  double volEps{1e-4};

  std::array<double, 4> volumes{};
  // LV, RV, LA, RA, SysArt, SysVen, PulArt, PulVen
  std::array<double, 8> circVolumes{};
  std::array<std::array<double, 8>, 4> rkVolumes{};
  // SysArt, SysVen, PulArt, PulVen
  std::array<double, 4> circPressures{};
  // SysArt, SysPer, SysVen, Rav, PulArt, PulPer, PulVen, Lav
  std::array<double, 8> flows{};

  std::vector<RVector> prevPressures{};
  RVector pVec;
  RVector rVec;

  RVector rPrev;
  RVector pPrev;

  RMatrix cMat;

  MCircModel circModel{};
public:
  FourChamberWindkesselProblem(double lv, double rv, double la, double ra);

  explicit FourChamberWindkesselProblem(std::array<double, 4> p);

  FourChamberWindkesselProblem();

  double Pressure(double t, const Point &x, int bndCondition) const override;

  void Initialize(double dt, double ts, const std::array<double, 4> &mechVolumes) override;

  void Update(double dt, double t, const std::array<double, 4> &mechVolumes) override;

  void Finalize(double dt, double t, const std::array<double, 4> &mechVolumes) override;

  bool Converged(const std::array<double, 4> &mechVolumes) override;
};

#endif //WINDKESSELPROBLEMS_HPP
