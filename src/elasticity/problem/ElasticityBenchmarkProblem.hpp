#ifndef ELASTICITYBENCHMARKPROBLEM_H
#define ELASTICITYBENCHMARKPROBLEM_H

#include "old/ElasticityProblemOld.hpp"


class LinearBenchmarkProblem : public ElasticityProblemOld {
  bool isPlanar;
public:
  LinearBenchmarkProblem(bool planar, std::vector<double> linearMatParameters)
      : ElasticityProblemOld("LinearBenchmarkProblem: u(x,y) = (-y,x)",
                             "Linear", linearMatParameters),
        isPlanar(planar) {}

  std::string MeshName() const override {
    return (meshFromConfig()?  meshName : (isPlanar ? "UnitBlock2DQuad" : "UnitBlock3DQuad"));
  }

  VectorField Deformation(const Point &z) const {
    return VectorField(-z[1], z[0], 0.0);
  }

  Tensor DeformationGradient(const Point &z) const {
    return Tensor(0.0, -1.0, 0.0,
                  1.0, 0.0, 0.0,
                  0.0, 0.0, 0.0);
  }
};

class QuadraticBenchmarkProblem : public ElasticityProblemOld {
  bool isPlanar;
public:
  QuadraticBenchmarkProblem(bool planar,
                            std::vector<double> linearMatParameters)
      : ElasticityProblemOld("QuadraticBenchmarkProblem: u(x,y) = (x*y, 0)",
                             "Linear", linearMatParameters),
        isPlanar(planar) {}

  std::string MeshName() const override {
    return (meshFromConfig()?  meshName : (isPlanar ?"UnitSquare" : "UnitCube"));
  }

  VectorField Deformation(const Point &z) const {
    return VectorField(z[0] * z[1], 0.0, 0.0);
  }

  Tensor DeformationGradient(const Point &z) const {
    return Tensor(z[1], 0.0, 0.0,
                  z[0], 0.0, 0.0,
                  0.0, 0.0, 0.0);
  }

  VectorField Load(double time, const Point &z, const Cell &c) const override {
    return VectorField(0.0, -2.0, 0.0);
  }
};

#endif //ELASTICITYBENCHMARKPROBLEM_H
