//
// Created by laura on 17.11.22.
//

#include "EllipsoidProblems.hpp"
#include "EllipsoidPoints.hpp"

std::vector<std::string> EllipsoidProblems::EvaluationMetrics() const {
  return {"MyocardStrain", "MyocardThickness", "ApexX", "ApexY", "ApexZ"};
}

std::vector<double> EllipsoidProblems::EvaluationResults(const Vector &solution) const {
    printLineDisplacements(solution);
    auto myoCardvalues = printMyocard(solution);

    Point apex = outerEvaluationPoints[2];
  auto z_displacement = findDisplacements(solution, {apex});

    /*vout(1).PrintInfo("Ellipsoid", verbose,
                      PrintInfoEntry("MyocardStrain", myoCardvalues.first, 1),
                      PrintInfoEntry("MyocardThickness", myoCardvalues.second, 1),
                      PrintInfoEntry("ApexLocation", apex_zpos, 1),
                      PrintInfoEntry("ApexDisplacement", z_displacement[0], 1));*/

    return {myoCardvalues.first, myoCardvalues.second, z_displacement[0][0], z_displacement[0][1], z_displacement[0][2]};
}

std::pair<double, double> EllipsoidProblems::printMyocard(const Vector &u) const {
    std::vector<VectorField> endoDisplacements = findDisplacements(u, ellipsoidEndocard);
    std::vector<VectorField> epiDisplacements = findDisplacements(u, ellipsoidEpicard);

    mpp::erase_if(endoDisplacements, [](const VectorField &vf) { return vf == zero; });
    mpp::erase_if(epiDisplacements, [](const VectorField &vf) { return vf == zero; });

    double meanStrain = 0.0;
    double meanThickness = 0.0;
    for (int i = 0; i < ellipsoidEndocard.size(); ++i) {
        VectorField endoPoint(ellipsoidEndocard[i]);
        VectorField epiPoint(ellipsoidEpicard[i]);

        meanStrain += norm(endoPoint + endoDisplacements[i] - epiPoint - epiDisplacements[i]) /
                      norm(endoPoint - epiPoint) - 1.0;
        meanThickness += norm(endoPoint + endoDisplacements[i] - epiPoint - epiDisplacements[i]);
    }

    return {PPM->Sum(meanStrain) / ellipsoidEndocard.size(), PPM->Sum(meanThickness) / ellipsoidEndocard.size()};


/*
  for (int i = 0; i < 20; ++i) {
    VectorField endoPoint(ellipsoidEndocard[i]);
    VectorField epiPoint(ellipsoidEpicard[i]);
    vout(1) << "Reference Thickness=" << norm(endoPoint - epiPoint)
            << " - Deformed Thickness="
            << norm(endoPoint + endoDisplacements[i] - epiPoint - epiDisplacements[i])
            << endl;
  }
*/
}

void EllipsoidProblems::FillEvaluationPoints() {
  if (contains(ElasticityProblem::GetMeshName(), "1")) {
    innerEvaluationPoints = inner_line_y_1;
    midEvaluationPoints = mid_line_y_2;
    outerEvaluationPoints = outer_line_y_1;
  } else if (contains(ElasticityProblem::GetMeshName(), "2")) {
    innerEvaluationPoints = inner_line_y_2;
    midEvaluationPoints = mid_line_y_2;
    outerEvaluationPoints = outer_line_y_2;
  } else if (contains(ElasticityProblem::GetMeshName(), "3")) {
    innerEvaluationPoints = inner_line_y_3;
    midEvaluationPoints = mid_line_y_2;
    outerEvaluationPoints = outer_line_y_3;
  } else if (contains(ElasticityProblem::GetMeshName(), "4")) {
    innerEvaluationPoints = inner_line_y_4;
    midEvaluationPoints = mid_line_y_4;
    outerEvaluationPoints = outer_line_y_4;
  } else if (contains(ElasticityProblem::GetMeshName(), "5")) {
    innerEvaluationPoints = inner_line_y_5;
    midEvaluationPoints = mid_line_y_4;
    outerEvaluationPoints = outer_line_y_5;
  } else if (contains(ElasticityProblem::GetMeshName(), "Full")) {
    innerEvaluationPoints = inner_line_y_full;
    midEvaluationPoints = mid_line_y_full;
    outerEvaluationPoints = outer_line_y_full;
  }
  else{
    Warning("you have no eval points.")
    //TODO find a better solution for this
    innerEvaluationPoints = {};
    midEvaluationPoints = {};
    outerEvaluationPoints = {};
  }
}

void EllipsoidProblems::printLineDisplacements(const Vector &u) const {
  auto x_displacements = findDisplacements(u, outerEvaluationPoints, 0);
  auto y_displacements = findDisplacements(u, outerEvaluationPoints, 1);
  auto z_displacements = findDisplacements(u, outerEvaluationPoints, 2);
  //vout(1) << endl << endl << "outer z values: " << endl;

  std::string outer_x_values{};
  for (int i = 0; i < y_displacements.size(); ++i) {
    //vout(1) << outer_line_x[i][2] << "\t";
    if (i != x_displacements.size() - 1) {
      outer_x_values += std::to_string(outerEvaluationPoints[i][0]) + ", ";
    } else {
      outer_x_values += std::to_string(outerEvaluationPoints[i][0]);
    }
  }


  std::string outer_z_values{};
  for (int i = 0; i < x_displacements.size(); ++i) {
    //vout(1) << outer_line_x[i][2] << "\t";
    if (i != x_displacements.size() - 1) {
      outer_z_values += std::to_string(outerEvaluationPoints[i][2]) + ", ";
    } else {
      outer_z_values += std::to_string(outerEvaluationPoints[i][2]);
    }
  }

  std::string outer_y_displacement{};
  //vout(1) << endl << "outer Y Displacement: " << endl;
  for (int i = 0; i < y_displacements.size(); ++i) {
    if (i != y_displacements.size() - 1) {
      //vout(1) << x_displacements[i] << "\t";
      outer_y_displacement +=
          std::to_string(y_displacements[i] + outerEvaluationPoints[i][1]) + ", ";
    } else {
      outer_y_displacement += std::to_string(y_displacements[i] + outerEvaluationPoints[i][1]);
    }
  }

  std::string outer_x_displacements{};
  //vout(1) << endl << "outer X Displacement: " << endl;
  for (int i = 0; i < x_displacements.size(); ++i) {
    //vout(1) << y_displacements[i] << "\t";
    if (i != x_displacements.size() - 1) {
      outer_x_displacements +=
          std::to_string(x_displacements[i] + outerEvaluationPoints[i][0]) + ", ";
    } else {
      outer_x_displacements += std::to_string(x_displacements[i] + outerEvaluationPoints[i][0]);
    }
  }

  std::string outer_z_displacement{};
  //vout(1) << endl << "outer Z Displacement: " << endl;
  for (int i = 0; i < z_displacements.size(); ++i) {
    //vout(1) << z_displacements[i] << "\t";
    if (i != z_displacements.size() - 1) {
      outer_z_displacement +=
          std::to_string(z_displacements[i] + outerEvaluationPoints[i][2]) + ", ";
    } else {
      outer_z_displacement += std::to_string(z_displacements[i] + outerEvaluationPoints[i][2]);
    }
  }

  x_displacements = findDisplacements(u, midEvaluationPoints, 0);
  y_displacements = findDisplacements(u, midEvaluationPoints, 1);
  z_displacements = findDisplacements(u, midEvaluationPoints, 2);
  std::string mid_z_values{};
  std::string mid_Y_displacements{};
  std::string mid_X_displacements{};
  std::string mid_z_displacements{};
  //vout(1) << endl << endl << "mid z values: " << endl;
  for (int i = 0; i < x_displacements.size(); ++i) {
    if (i != x_displacements.size() - 1) {
      //vout(1) << mid_line_x[i][2] << "\t";
      mid_z_values += std::to_string(midEvaluationPoints[i][2]) + ",";
    } else {
      mid_z_values += std::to_string(midEvaluationPoints[i][2]);
    }
  }
  //vout(1) << endl << "mid Y Displacement: " << endl;
  for (int i = 0; i < x_displacements.size(); ++i) {
    if (i != x_displacements.size() - 1) {
      //vout(1) << x_displacements[i] << "\t";
      mid_Y_displacements += std::to_string(x_displacements[i] + midEvaluationPoints[i][0]) + ",";
    } else {
      mid_Y_displacements += std::to_string(x_displacements[i] + midEvaluationPoints[i][0]);
    }
  }

  //vout(1) << endl << "mid X Displacement: " << endl;
  for (int i = 0; i < y_displacements.size(); ++i) {
    if (i != y_displacements.size() - 1) {
      //vout(1) << y_displacements[i] << "\t";
      mid_X_displacements += std::to_string(y_displacements[i] + midEvaluationPoints[i][1]) + ",";
    } else {
      mid_X_displacements += std::to_string(y_displacements[i] + midEvaluationPoints[i][1]);
    }
  }
  //vout(1) << endl << "mid z Displacement: " << endl;
  for (int i = 0; i < z_displacements.size(); ++i) {
    if (i != z_displacements.size() - 1) {
      //vout(1) << z_displacements[i] << "\t";
      mid_z_displacements += std::to_string(z_displacements[i] + midEvaluationPoints[i][2]) + ", ";
    } else {
      mid_z_displacements += std::to_string(z_displacements[i] + midEvaluationPoints[i][2]);
    }
  }

  x_displacements = findDisplacements(u, innerEvaluationPoints, 0);
  y_displacements = findDisplacements(u, innerEvaluationPoints, 1);
  z_displacements = findDisplacements(u, innerEvaluationPoints, 2);
  std::string inner_z_values{};
  std::string inner_Y_displacement{};
  std::string inner_X_displacement{};
  std::string inner_z_displacements{};

  std::string inner_x_values{};
  for (int i = 0; i < y_displacements.size(); ++i) {
    //vout(1) << outer_line_x[i][2] << "\t";
    if (i != x_displacements.size() - 1) {
      inner_x_values += std::to_string(innerEvaluationPoints[i][0]) + ", ";
    } else {
      inner_x_values += std::to_string(innerEvaluationPoints[i][0]);
    }
  }


  //vout(1) << endl << endl << "inner z values: " << endl;
  for (int i = 0; i < x_displacements.size(); ++i) {
    if (i != x_displacements.size() - 1) {
      //vout(1) << inner_line_x[i][2] << "\t";
      inner_z_values += std::to_string(innerEvaluationPoints[i][2]) + ", ";
    } else {
      inner_z_values += std::to_string(innerEvaluationPoints[i][2]);
    }
  }

  //vout(1) << endl << "inner Y Displacement: " << endl;
  for (int i = 0; i < y_displacements.size(); ++i) {
    if (i != y_displacements.size() - 1) {
      //vout(1) << x_displacements[i] << "\t";
      inner_Y_displacement +=
          std::to_string(y_displacements[i] + innerEvaluationPoints[i][1]) + ", ";
    } else {
      inner_Y_displacement += std::to_string(y_displacements[i] + innerEvaluationPoints[i][1]);
    }
  }
  //vout(1) << endl << "inner X Displacement: " << endl;
  for (int i = 0; i < x_displacements.size(); ++i) {
    //vout(1) << y_displacements[i] << "\t";
    if (i != x_displacements.size() - 1) {
      inner_X_displacement +=
          std::to_string(x_displacements[i] + innerEvaluationPoints[i][0]) + ", ";
    } else {
      inner_X_displacement += std::to_string(x_displacements[i] + innerEvaluationPoints[i][0]);
    }
  }
  //vout(1) << endl << "inner z Displacement: " << endl;
  for (int i = 0; i < z_displacements.size(); ++i) {
    if (i != z_displacements.size() - 1) {
      //vout(1) << z_displacements[i] << "\t";
      inner_z_displacements +=
          std::to_string(z_displacements[i] + innerEvaluationPoints[i][2]) + ", ";
    } else {
      inner_z_displacements += std::to_string(z_displacements[i] + innerEvaluationPoints[i][2]);
    }
  }

  //vout(1) << endl << endl << "Apex z Movement: ";
  std::vector<Point> apex{outerEvaluationPoints[0]};
  mout << "apex in Ellipsoid problems" << apex << endl << endl;
  auto z_displacement = findDisplacements(u, apex, 2);
  //vout(1) << z_displacement[0] << endl;
  vout(1).PrintInfo("Ellipsoid Data", 1,
                    PrintInfoEntry("outer x values", "[" + outer_x_values + "]"),
                    PrintInfoEntry("outer z values", "[" + outer_z_values + "]"),
                    PrintInfoEntry("outer Y Location", "[" + outer_y_displacement + "]"),
                    PrintInfoEntry("outer X Location", "[" + outer_x_displacements + "]"),
                    PrintInfoEntry("outer Z Location", "[" + outer_z_displacement + "]"),
                    PrintInfoEntry("mid z Location", "[" + mid_z_values + "]"),
                    PrintInfoEntry("mid X Location", "[" + mid_Y_displacements + "]"),
                    PrintInfoEntry("mid Y Location", "[" + mid_X_displacements + "]"),
                    PrintInfoEntry("mid Z Location", "[" + mid_z_displacements + "]"),
                    PrintInfoEntry("inner x values", "[" + inner_x_values + "]"),
                    PrintInfoEntry("inner z values", "[" + inner_z_values + "]"),
                    PrintInfoEntry("inner Y Location", "[" + inner_Y_displacement + "]"),
                    PrintInfoEntry("inner X Location", "[" + inner_X_displacement + "]"),
                    PrintInfoEntry("inner Z Location", "[" + inner_z_displacements + "]"),
                    PrintInfoEntry("Apex z Movement", z_displacement[0]));

}

double EllipsoidProblems::ActiveStretch(double t, const Point &x) const {
    double z = -(x[0] + 20.0) / 100.0 + t;

    return z < 0.0 ? 0.0 : -0.03 * (exp(-(z - 0.15) / 0.02) / (0.02 * (1 + exp(-(z - 0.15) / 0.02)) *
                                                               (1 + exp(-(z - 0.15) / 0.02))));
}

std::vector<double> LandBiventricleProblem::EvaluationResults(const Vector &solution) const {
  return {};
}
