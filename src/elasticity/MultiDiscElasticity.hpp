//
// Created by laura on 30.01.24.
//

#ifndef CARDMECH_MULTIDISCELASTICITY_HPP
#define CARDMECH_MULTIDISCELASTICITY_HPP

#endif //CARDMECH_MULTIDISCELASTICITY_HPP

#include "MainCardMech.hpp"
#include "MainElasticity.hpp"

/**
 * Running different discretizations one after the other.
 * Homotopy (for different discs or e.g. to test different penalty parameters for DG/EG faster).
 * Possibilty to choose different meshes and degrees is not implemented yet!
 * "meshesList" and "degreeList": if there is only one entry in the config, it uses the same entries
 * for all discretizations in "discs"
 * homotopy possibilities: CG -> DG, CG -> EG, DG -> DG, EG -> EG
 */
class MultiDiscElasticity : public MainCardMech {
  MainElasticity main;

  bool multiDiscs{false};
  bool useDifferentMeshes{false};
  bool useDifferentDegs{false};
  std::vector<VectorStruct> solutions{};
  std::vector<std::string> discs{};
  std::vector<std::string> meshes;
  std::vector<std::string> meshesList;
  std::vector<int>  degrees;
  std::vector<int>  degreeList;
  std::vector<int>  DGPenalties;
  std::vector<int>  DGPenaltyList;

public:
  MultiDiscElasticity();

  void Initialize() override;

  Vector &Run() override;

  std::vector<double> Evaluate(const Vector &solution) override;
};

