#ifndef DISPLACEMENTLOGGER_H
#define DISPLACEMENTLOGGER_H

#include "CardiacLogger.hpp"


class DisplacementLogger : CardiacLogger {
  const std::string &dataFileName;

  ///
  std::vector <Point> *trackedPoints = new std::vector<Point>;

  ///
  unordered_map<double, unordered_map < Point, Point> *> *
  data = new unordered_map < double, unordered_map<Point, Point>
  *>;

  int size() {
    return trackedPoints->size();
  }

public:
  DisplacementLogger(const std::string &logName, const std::string &dataName);

  /**
   * Calculates the distances between the paired points under the given displacement.
   * The values are stored in "data" until everything is logged into a file.
   *
   * @param displacement
   * @param time
   */
  void Log(const Vector &displacement, double time);

  void LogToFile() override;

  ~DisplacementLogger() {
    trackedPoints->clear();
    delete trackedPoints;

    data->clear();
    delete data;
  }
};

#endif //DISPLACEMENTLOGGER_H
