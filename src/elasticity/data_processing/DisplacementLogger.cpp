#include "DisplacementLogger.hpp"

DisplacementLogger::DisplacementLogger(const std::string &logName,
                                       const std::string &dataName)
    : CardiacLogger(logName), dataFileName(dataName) {
  std::ifstream dataFile("../cardiacmechanics/data/" + dataFileName);
  if (dataFile.is_open()) {
    std::string line;
    while (getline(dataFile, line)) {
      // Expects to points written as six consecutive doubles
      double pointsInLine[3];
      std::string value;
      std::istringstream linestream(line);
      int j = 0;
      while (getline(linestream, value, ' ')) {
        pointsInLine[j++] = std::stod(value);
      }
      Point p(pointsInLine[0], pointsInLine[1], pointsInLine[2]);

      trackedPoints->push_back(p);
    }
    dataFile.close();
  } else THROW("File for logging not found.")
}

void DisplacementLogger::Log(const Vector &displacement, double time) {
  ExchangeBuffer E;

  for (row r = displacement.rows(); r != displacement.rows_end(); r++) {
    if (std::find(trackedPoints->begin(), trackedPoints->end(), r()) != trackedPoints->end()) {
      E.Send(0) << (Point) r() << (Point) displacement(r());
    }
  }

  E.Communicate();

  if (PPM->master()) {
    auto *snapshot = new unordered_map<Point, Point>;

    for (short q = 0; q < PPM->size(); ++q) {
      while (E.Receive(q).size() < E.ReceiveSize(q)) {
        Point P;
        E.Receive(q) >> P;
        Point phi;
        E.Receive(q) >> phi;

        if (snapshot->find(P) != snapshot->end())
          continue;

        (*snapshot)[P] = phi;
      }
    }

    (*data)[time] = snapshot;
  }
}

void DisplacementLogger::LogToFile() {
  int j = 0;
  Point trackedPointsFixed[size()];

  for (const auto &p : *trackedPoints) {
    trackedPointsFixed[j++] = p;
  }

  if (PPM->master()) {
    mout << "Writing Data Files..." << endl;
    std::ofstream file;
    file.open("data/" + logFileName + ".dat");

    file << "time";
    for (int i = 0; i < size(); i++)
      file << " x_" << i << " y_" << i << " z_" << i;
    file << endl;

    for (auto snapshot = data->begin(); snapshot != data->end(); ++snapshot) {
      file << snapshot->first;
      for (int i = 0; i < size(); i++) {
        Point p = snapshot->second->at(trackedPointsFixed[i]);
        for (int j = 0; j < 3; j++)
          file << " " << p[j];
      }
      file << endl;
    }
    file.close();
  }
}
