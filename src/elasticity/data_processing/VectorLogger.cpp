#include "VectorLogger.hpp"


void VectorLogger::Log(double time) {
  if (Vs.size() != size) THROW("Logging Error: number of arguments does not match!")
  ExchangeBuffer E;

  Point P(0.0, 0.0, 0.0);
  P.t() = time;

  std::vector<double> valuesToSend;
  for (auto V : Vs) {
    for (row r = V.rows(); r != V.rows_end(); r++) {
      if (dist(r(), P) < Eps) {
        valuesToSend.push_back(V(r, 0));
      }
    }
  }

  E.Send(0) << (Point) P << valuesToSend.size();
  for (auto v : valuesToSend) {
    E.Send(0) << (double) v;
  }

  E.Communicate();

  if (PPM->master()) {
    auto *snapshot = new unordered_map<Point, std::vector<double> *>;

    for (short q = 0; q < PPM->size(); ++q) {
      while (E.Receive(q).size() < E.ReceiveSize(q)) {
        Point P;
        E.Receive(q) >> P;
        size_t count = 0;
        E.Receive(q) >> count;

        auto valuesRecieved = new std::vector<double>;
        for (int i = 0; i < count; ++i) {
          double tmp = 0.0;
          E.Receive(q) >> tmp;
          valuesRecieved->push_back(tmp);
        }

        if (snapshot->find(P) != snapshot->end())
          continue;

        (*snapshot)[P] = valuesRecieved;
      }
    }

    data->push_back(snapshot);
  }
}

void VectorLogger::LogToFile() {
  if (PPM->master()) {
    mout << "Writing Data Files..." << endl;
    std::ofstream file;
    file.open("data/" + logFileName + ".dat");

    file << "time";
    if (size > 0) file << " potential";
    if (size > 1) file << " calcium";
    if (size > 2) file << " stretch";
    if (size > 3) file << " force";
    file << endl;

    for (auto i = data->begin(); i != data->end(); ++i) {
      for (auto j = (*i)->begin(); j != (*i)->end(); ++j) {
        Point P = j->first;
        file << P.t();
        for (auto k = j->second->begin(); k != j->second->end(); ++k) {
          file << " " << *k;
        }
        file << endl;
      }
    }
    file.close();
  }
}
