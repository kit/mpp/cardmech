#ifndef VECTORLOGGER_H
#define VECTORLOGGER_H

#include "Algebra.hpp"

#include "CardiacLogger.hpp"


class VectorLogger : CardiacLogger {
  list<unordered_map<Point, std::vector<double> *> *> *data =
      new list<unordered_map<Point, std::vector<double> *> *>;

  std::vector<Vector> Vs;
  int size;
public:
  VectorLogger(const std::string &filename, int s, Vector &ref)
      : CardiacLogger(filename), size(s), Vs(s, ref) {}

  void Set(int i, Vector &v) {
    Vs[i] = v;
  }

  void Log(double time);

  void LogToFile();
};

#endif //VECTORLOGGER_H
