#ifndef CARDIACLOGGER_H
#define CARDIACLOGGER_H

#include "m++.hpp"


class CardiacLogger {
protected:
  const std::string &logFileName;

public:
  CardiacLogger(const std::string &fileName) : logFileName(fileName) {};

  /**
   * Writes all internally stored logging arguments into the file @logFileName.
   * To increase performance, it is advised to only call this method exactly
   * once - at the end of calculations
   */
  virtual void LogToFile() = 0;
};

#endif //CARDIACLOGGER_H
