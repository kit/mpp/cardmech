#ifndef THICKNESSLOGGER_H
#define THICKNESSLOGGER_H

#include "CardiacLogger.hpp"


class ThicknessLogger : CardiacLogger {
  const std::string &dataFileName;

  ///
  unordered_map <Point, Point> *pairedPoints = new unordered_map<Point, Point>;

  ///
  unordered_map<double, unordered_map < Point, double> *> *
  data = new unordered_map < double, unordered_map<Point, double>
  *>;

  int size() {
    return pairedPoints->size();
  }

public:
  ThicknessLogger(const std::string &logName, const std::string &dataName);

  /**
   * Calculates the distances between the paired points under the given displacement.
   * The values are stored in "data" until everything is logged into a file.
   *
   * @param displacement
   * @param time
   */
  void Log(const Vector &displacement, double time);

  void LogToFile() override;

  ~ThicknessLogger() {
    pairedPoints->clear();
    delete pairedPoints;

    data->clear();
    delete data;
  }
};

class MultiThicknessLogger : CardiacLogger {
  std::vector<ThicknessLogger *> loggers;

public:
  MultiThicknessLogger(int loggerSize, const std::string &logName,
                       const std::string &dataName)
      : CardiacLogger(logName), loggers(loggerSize) {
    for (int l = 0; l < loggerSize; l++) {
      loggers[l] = new ThicknessLogger(logName + std::to_string(l), dataName + std::to_string(l));
    }
  }

  void Log(const Vector &displacement, double time) {
    for (auto logger : loggers) {
      logger->Log(displacement, time);
    }
  };

  void LogToFile() override {
    for (auto logger : loggers) {
      logger->LogToFile();
    }
  };

  ~MultiThicknessLogger() {
    loggers.clear();
  }
};

#endif //THICKNESSLOGGER_H
