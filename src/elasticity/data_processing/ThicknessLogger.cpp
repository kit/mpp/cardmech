#include "ThicknessLogger.hpp"

ThicknessLogger::ThicknessLogger(const std::string &logName,
                                 const std::string &dataName)
    : CardiacLogger(logName), dataFileName(dataName) {
  std::ifstream dataFile("../geo/" + dataFileName);
  if (dataFile.is_open()) {
    std::string line;
    while (getline(dataFile, line)) {
      // Expects to points written as six consecutive doubles
      double pointsInLine[6];
      std::string value;
      std::istringstream linestream(line);
      int j = 0;
      while (getline(linestream, value, ' ')) {
        pointsInLine[j++] = std::stod(value);
      }
      Point pInner(pointsInLine[0], pointsInLine[1], pointsInLine[2]);
      Point pOuter(pointsInLine[3], pointsInLine[4], pointsInLine[5]);

      (*pairedPoints)[pInner] = pOuter;
    }
    dataFile.close();
  }
}

void ThicknessLogger::Log(const Vector &displacement, double time) {
  ExchangeBuffer E;

  for (row r = displacement.rows(); r != displacement.rows_end(); r++) {
    try {
      Point innerP = r();
      Point outerP = pairedPoints->at(innerP);

      double thickness = dist(innerP + Point(displacement(innerP)),
                              outerP + Point(displacement(outerP)));
      E.Send(0) << (Point) innerP << thickness;
    }
      // If r is not a paired point, move on.
    catch (const std::out_of_range &oor) {
      continue;
    }
  }

  E.Communicate();

  if (PPM->master()) {
    auto *snapshot = new unordered_map<Point, double>;

    for (short q = 0; q < PPM->size(); ++q) {
      while (E.Receive(q).size() < E.ReceiveSize(q)) {
        Point P;
        E.Receive(q) >> P;
        double rThickness = 0;
        E.Receive(q) >> rThickness;

        if (snapshot->find(P) != snapshot->end())
          continue;

        (*snapshot)[P] = rThickness;
      }
    }

    (*data)[time] = snapshot;
  }
}

void ThicknessLogger::LogToFile() {
  int j = 0;
  Point pairedPointsFixed[size()];

  for (const auto &p : *pairedPoints) {
    pairedPointsFixed[j++] = p.first;
  }

  if (PPM->master()) {
    mout << "Writing Data Files..." << endl;
    std::ofstream file;
    file.open("data/" + logFileName + ".dat");

    file << "time";
    for (int i = 0; i < size(); i++)
      file << " diam_" << i;
    file << endl;

    for (auto snapshot = data->begin(); snapshot != data->end(); ++snapshot) {
      file << (*snapshot).first;
      for (int i = 0; i < size(); i++)
        file << " " << (*snapshot).second->at(pairedPointsFixed[i]);

      file << endl;
    }
    file.close();
  }
}
