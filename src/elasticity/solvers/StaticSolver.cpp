#include "StaticSolver.hpp"

void StaticSolver::Initialize(const IElasticity &assemble, Vector &u) {
  assemble.Initialize(u);

  residualMatrix = std::make_unique<Matrix>(u);
  assemble.TractionStiffness(*residualMatrix);
}

double StaticSolver::calculateResidualUpdate(const IElasticity &assemble, const Vector &u,
                                             Vector &defect) const {
  assemble.Residual(u, defect);
  defect += *residualMatrix * u;

  defect.ClearDirichletValues();
  defect.Collect();

  return defect.norm();
}

void StaticSolver::calculateJacobiUpdate(const IElasticity &assemble, const Vector &u,
                                         Matrix &J) const {
  assemble.Jacobi(u, J);
  J += *residualMatrix;
}

