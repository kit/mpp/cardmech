#include "ElasticityNewmark.hpp"

void NewmarkSolver::Initialize(const IElasticity &assemble, Vector &u) {
  if (solveViscoelastic || useRayleigh) {
    velocity = std::make_unique<Vector>(0.0, u);
  }

  oldValues.Init(u);
  assemble.Initialize(*(oldValues.values));
  u = oldValues.Displacement();

  double dt = assemble.StepSize();
  updateRHSValues(dt);
  InitializeMatrices(assemble,u);

}

void NewmarkSolver::InitializeMatrices(const IElasticity &assemble, Vector &u){
  double dt = assemble.StepSize();
  Matrix massMatrix(u);
  assemble.MassMatrix(massMatrix);

  Matrix systemMatrix(u);
  assemble.TractionViscosity(systemMatrix);

  if (useRayleigh) {
    rayleighMatrix = std::make_unique<Matrix>(massMatrix);
    *rayleighMatrix *= rAlpha;
    // *rayleighMatrix += rBeta * RESIDUAL
  }

  displacementMatrix = std::make_unique<Matrix>(massMatrix);
  *displacementMatrix *= (1.0 / (beta * dt * dt));
  *displacementMatrix += (gamma / (beta * dt)) * systemMatrix;

  velocityMatrix = std::make_unique<Matrix>(massMatrix);
  *velocityMatrix *= (1.0 / (beta * dt));
  *velocityMatrix += (gamma / beta - 1.0) * systemMatrix;

  accelerationMatrix = std::make_unique<Matrix>(massMatrix);
  *accelerationMatrix *= ((0.5 - beta) / beta);
  *accelerationMatrix += (gamma * (1 + (0.5 - beta) / beta)*dt - dt) * systemMatrix;

  residualMatrix = std::make_unique<Matrix>(u);
  assemble.TractionStiffness(*residualMatrix);
  *residualMatrix += *displacementMatrix;
}
void NewmarkSolver::operator()(const IElasticity &assemble, Vector &u) {
  double t = assemble.Time();
  NewtonT<IElasticity>::operator()(assemble, u);
  updateOldValues(assemble.StepSize(), u);

  assemble.EvaluateTractionStiffness(u);
  
  //PlotIteration(t);
}

std::string NewmarkSolver::Name() const {
  return "Newmark";
}

double
NewmarkSolver::calculateResidualUpdate(const IElasticity &assemble, const Vector &u,
                                       Vector &defect) const {
  if (solveViscoelastic || useRayleigh) {
    *velocity = (gamma / (beta * assemble.StepSize())) * (u - oldValues.Displacement());
    *velocity += (1. - gamma / beta) * oldValues.Velocity();
    *velocity +=
        assemble.StepSize() * (1. - gamma * (1. + (0.5 - beta) / beta)) * oldValues.Acceleration();
  }
  if (solveViscoelastic) {
    assemble.ViscoResidual(u, *velocity, defect);
  } else {
    assemble.Residual(u, defect);
  }

  if (useRayleigh) {
    defect += *rayleighMatrix * *velocity;
  }

  defect += *residualMatrix * u;
  defect -= *displacementMatrix * oldValues.Displacement();
  defect -= *velocityMatrix * oldValues.Velocity();
  defect -= *accelerationMatrix * oldValues.Acceleration();

  defect.ClearDirichletValues();
  defect.Collect();

  return defect.norm();
}

void NewmarkSolver::calculateJacobiUpdate(const IElasticity &assemble, const Vector &u,
                                          Matrix &J) const {
  if (solveViscoelastic) {
    assemble.ViscoJacobi(u, *velocity, J);
  } else {
    assemble.Jacobi(u, J);
  }
  J += *residualMatrix;
  J.ClearDirichletValues();
}


void NewmarkSolver::updateRHSValues(double dt) {

  oldValues.VelTmp() = oldValues.Displacement(); // u^n
  oldValues.VelTmp() -= (beta / gamma - 1.0) * dt * oldValues.Velocity();
  oldValues.VelTmp() -= (1.0 - gamma * (1.0 + ((0.5 - beta) / beta)) ) * dt * dt * beta/gamma * oldValues.Acceleration();

  oldValues.AccTmp() = oldValues.Displacement(); // u^n
  oldValues.AccTmp() += dt * oldValues.Velocity();
  oldValues.AccTmp() += dt * dt * (0.5 - beta) * oldValues.Acceleration();
}

void NewmarkSolver::updateOldValues(double dt, const Vector &u) {

  oldValues.Displacement() = u; // u^{n+1}
  oldValues.Velocity() = (gamma / (dt * beta)) * (u - oldValues.VelTmp()); //v^{n+1}
  oldValues.Acceleration() = 1.0 / (dt * dt * beta) * (u - oldValues.AccTmp()); //a^{n+1}

  updateRHSValues(dt);
}

void NewmarkSolver::PlotIteration(double time) const {
  auto &plot_vel = mpp::plot("Velocity");
  plot_vel.AddData("Velocity", oldValues.Velocity());
  plot_vel.PlotFile("Velocity" + std::to_string(time));
  auto &plot_acc = mpp::plot("Acceleration");
  plot_acc.AddData("Acceleration", oldValues.Acceleration());
  plot_acc.PlotFile("Acceleration" + std::to_string(time));
}