#include "ElasticityEuler.hpp"

void DynamicEulerSolver::Initialize(const IElasticity &assemble, Vector &u) {
  if (solveViscoelastic || useRayleigh) {
    velocity = std::make_unique<Vector>(0.0, u);
  }

  oldValues.Init(u);
  assemble.Initialize(*(oldValues.values));
  u = oldValues.Displacement();

  double dt = assemble.StepSize();

  Matrix massMatrix(u);
  assemble.MassMatrix(massMatrix);

  Matrix systemMatrix(u);
  assemble.TractionViscosity(systemMatrix);

  if (useRayleigh) {
    rayleighMatrix = std::make_unique<Matrix>(massMatrix);
    *rayleighMatrix *= rAlpha;
    // *rayleighMatrix += rBeta * RESIDUAL
  }

  displacementMatrix = std::make_unique<Matrix>(massMatrix);
  *displacementMatrix *= (1.0 / (dt * dt));
  *displacementMatrix += (1.0 / (dt)) * systemMatrix;

  velocityMatrix = std::make_unique<Matrix>(massMatrix);
  *velocityMatrix *= (1.0 / (dt));

  residualMatrix = std::make_unique<Matrix>(u);
  assemble.TractionStiffness(*residualMatrix);
  *residualMatrix += *displacementMatrix;

}

void DynamicEulerSolver::operator()(const IElasticity &assemble, Vector &u) {
  NewtonT<IElasticity>::operator()(assemble, u);
  updateOldValues(assemble.StepSize(), u);
}

std::string DynamicEulerSolver::Name() const {
  return "Dynamic Euler";
}

double
DynamicEulerSolver::calculateResidualUpdate(const IElasticity &assemble, const Vector &u,
                                            Vector &defect) const {
  if (solveViscoelastic) {
    *velocity = (1. / assemble.StepSize()) * (u - oldValues.Displacement());
    assemble.ViscoResidual(u, *velocity, defect);
  } else {
    assemble.Residual(u, defect);
  }

  if (useRayleigh) {
    defect += *rayleighMatrix * *velocity;
  }

  defect += *residualMatrix * u;
  defect -= *displacementMatrix * oldValues.Displacement();
  defect -= *velocityMatrix * oldValues.Velocity();

  defect.ClearDirichletValues();
  defect.Collect();

  return defect.norm();
}

void DynamicEulerSolver::calculateJacobiUpdate(const IElasticity &assemble, const Vector &u,
                                               Matrix &J) const {
  if (solveViscoelastic) {
    *velocity = (1. / assemble.StepSize()) * (u - oldValues.Displacement());
    assemble.ViscoJacobi(u, *velocity, J);
  } else {
    assemble.Jacobi(u, J);
  }

  J += *residualMatrix;
  J.ClearDirichletValues();
}

void DynamicEulerSolver::updateOldValues(double dt, const Vector &u) {
  oldValues.Acceleration() = 1.0 / (dt * dt) * (u - oldValues.Displacement());
  oldValues.Acceleration() -= (1.0/dt) * oldValues.Velocity();
  oldValues.Velocity() = (1.0 / dt) * (u - oldValues.Displacement());
  oldValues.Displacement() = u;
}