#ifndef ITERATIVEPRESSURESOLVER_HPP
#define ITERATIVEPRESSURESOLVER_HPP

#include <utility>

#include "Newton.hpp"
#include "CardiacSolver.hpp"
#include "assemble/IElasticity.hpp"
#include "TimeSeries.hpp"
#include "StaticSolver.hpp"

class IterativePressureSolver : public CardiacSolver {
  int pressureSteps{1};
  int pressureDepth{0};
  int maxPressureDepth{0};
  int maxPressureIterations{10};
  TimeSeries iteration{};

  bool nextPressureIteration(int currentStep){
    if(pressureDepth++>=maxPressureDepth) return false;

    pressureSteps = (pressureSteps-currentStep+1) * 2;
    iteration.Reset(iteration.PreviousTStep(),1.0,pressureSteps);

    return true;
  }

protected:
  const IPressureProblem& problem;
    StaticSolver solver{};
public:
  IterativePressureSolver(const IPressureProblem &p, int pSteps)
  : problem(p), pressureSteps(pSteps), iteration(0.0,1.0,pSteps) {
    Config::Get("PressureSolverVerbose", verbose);
    Config::Get("PressureSolverVTK", vtk);
    Config::Get("PressureIterations", maxPressureIterations);
    Config::Get("PressureDepth", maxPressureDepth);
  }

  explicit IterativePressureSolver(const IPressureProblem &p) : problem(p) {
    Config::Get("PressureSolverVerbose", verbose);
    Config::Get("PressureSolverVTK", vtk);
    Config::Get("PressureSteps", pressureSteps);
    Config::Get("PressureIterations", maxPressureIterations);
    Config::Get("PressureDepth", maxPressureDepth);

    iteration.Reset(0.0,1.0,pressureSteps);
  }

  void Initialize(IElasticity &assemble, Vector &u);

  bool Method(IElasticity &assemble, Vector &u);

  virtual bool Step(IElasticity &assemble, Vector &u);
};

class KlotzPressureSolver : public IterativePressureSolver {

public:
  explicit KlotzPressureSolver(const IPressureProblem &p, int pSteps)
      : IterativePressureSolver(p, pSteps) {}

  explicit KlotzPressureSolver(const IPressureProblem &p)
      : IterativePressureSolver(p) {};


  bool Step(IElasticity &assemble, Vector &u) override;
};

void CalculatePrestress(IElasticity &assemble, Vector &u);

#endif //ITERATIVEPRESSURESOLVER_HPP
