#ifndef ELASTICITYNEWMARK_HPP
#define ELASTICITYNEWMARK_HPP

#include "Newton.hpp"

#include "assemble/IElasticity.hpp"

class NewmarkSolver : public NewtonT<IElasticity> {
  double beta{0.25};
  double gamma{0.5};

  bool solveViscoelastic{false};

  bool useRayleigh{false};
  double rAlpha{0.0};
  double rBeta{0.0};

  std::unique_ptr<Matrix> rayleighMatrix = nullptr;
  std::unique_ptr<Matrix> residualMatrix = nullptr;
  std::unique_ptr<Matrix> displacementMatrix = nullptr;
  std::unique_ptr<Matrix> velocityMatrix = nullptr;
  std::unique_ptr<Matrix> accelerationMatrix = nullptr;

  std::unique_ptr<Vector> velocity = nullptr;

  struct NewmarkValues {
    std::unique_ptr<Vectors> values{};
  public:
    void Init(const Vector &u) {
      values = std::make_unique<Vectors>(5, u);
      (*values)[1] = 0.0;
      (*values)[2] = 0.0;
      (*values)[3] = 0.0;
      (*values)[4] = 0.0;
    }

    Vector &Displacement() const {
      return (*values)[0];
    }

    Vector &Velocity() const {
      return (*values)[1];
    }

    Vector &Acceleration() const {
      return (*values)[2];
    }

    Vector &VelTmp() const {
      return (*values)[3];
    }

    Vector &AccTmp() const {
      return (*values)[4];
    }
  };

  NewmarkValues oldValues{};

  void updateRHSValues(double dt) override;

  void updateOldValues(double dt, const Vector &u);

protected:
  double calculateResidualUpdate(const IElasticity &assemble, const Vector &u,
                                 Vector &r) const override;

  void calculateJacobiUpdate(const IElasticity &assemble, const Vector &u,
                             Matrix &J) const override;

public:
  NewmarkSolver(bool viscoElastic, bool rayleigh = false) : NewtonT<IElasticity>(
      std::unique_ptr<LinearSolver>(GetLinearSolverByPrefix("Mech"))
  ), solveViscoelastic(viscoElastic), useRayleigh(rayleigh) {
    Config::Get("NewmarkBeta", beta);
    Config::Get("NewmarkGamma", gamma);

    Config::Get("RayleighAlpha", rAlpha);
    Config::Get("RayleighBeta", rBeta);
  }

  NewmarkSolver(double b, double g, bool viscoElastic) : NewtonT<IElasticity>(
      std::unique_ptr<LinearSolver>(GetLinearSolverByPrefix("Mech"))
  ), beta(b), gamma(g), solveViscoelastic(viscoElastic) {}

  void Initialize(const IElasticity &assemble, Vector &u) override;
  void InitializeMatrices(const IElasticity &assemble, Vector &u) override;

  void operator()(const IElasticity &assemble, Vector &u) override;

  std::string Name() const override;

  void FillInformation(const IElasticity& assemble,
                       std::unordered_map<std::string, double> &info) const override {
    info["L2Norm(vel)"] = assemble.L2Norm(oldValues.Velocity());
    info["H1(vel)"] = assemble.H1Norm(oldValues.Velocity());
    info["Frobenius(vel)"] = oldValues.Velocity().ConsistentNorm();
    info["L2Norm(acc)"] = assemble.L2Norm(oldValues.Acceleration());
    info["H1(acc)"] = assemble.H1Norm(oldValues.Acceleration());
    info["Frobenius(acc)"] = oldValues.Acceleration().ConsistentNorm();
  }

  void PlotIteration(double time) const;

  void GetDynamicVectors( std::vector<Vector> &values) const override {
    values = {oldValues.Velocity(),oldValues.Acceleration()};
  };
};

#endif //ELASTICITYNEWMARK_HPP
