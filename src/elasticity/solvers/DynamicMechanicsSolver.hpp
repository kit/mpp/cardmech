#ifndef MECHANICSSOLVERS_H
#define MECHANICSSOLVERS_H

#include "Newton.hpp"

#include "assemble/IElasticity.hpp"


std::unique_ptr<NonLinearSolverT<IElasticity>> GetDynamicSolver();

std::unique_ptr<NonLinearSolverT<IElasticity>> GetDynamicSolver(
    const std::string &solverName);

class ElastodynamicTimeIntegrator {
  int verbose{1};


public:

  std::unique_ptr<NonLinearSolverT<IElasticity>> nonlinearSolver{};


  explicit ElastodynamicTimeIntegrator() : ElastodynamicTimeIntegrator(
      GetDynamicSolver()) {}

  explicit ElastodynamicTimeIntegrator(const std::string &solverName)
      : ElastodynamicTimeIntegrator(GetDynamicSolver(solverName)) {}

  explicit ElastodynamicTimeIntegrator(
      std::unique_ptr<NonLinearSolverT<IElasticity>> &&nlSolver) : nonlinearSolver(
      std::move(nlSolver)) {}

  void Initialize(IElasticity &assemble, Vector &u) const;

  bool Method(IElasticity &assemble, Vector &u) const;

  bool Step(IElasticity &assemble, Vector &u) const;

  void FillInformation(const IElasticity& assemble,
                       std::unordered_map<std::string, double> &info) const{
    nonlinearSolver->FillInformation(assemble, info);
  }
   virtual void GetDynamicVectors( std::vector<Vector> &values) const {
    nonlinearSolver->GetDynamicVectors(values);
  };
};

#endif //MECHANICSSOLVERS_H
