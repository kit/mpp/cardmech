#include "DynamicMechanicsSolver.hpp"

#include "TimeIntegrator.hpp"

#include "ElasticityEuler.hpp"
#include "ElasticityNewmark.hpp"


std::unique_ptr<NonLinearSolverT<IElasticity>> GetDynamicSolver() {
  std::string dynamics = "Newmark";
  Config::Get("MechDynamics", dynamics);
  return GetDynamicSolver(dynamics);
}

std::unique_ptr<NonLinearSolverT<IElasticity>> GetDynamicSolver(
    const std::string &solverName) {
  if (contains(solverName, "Euler"))
    return std::make_unique<DynamicEulerSolver>(contains(solverName, "Visco"),
                                                contains(solverName, "Rayleigh"));
  if (contains(solverName, "Newmark"))
    return std::make_unique<NewmarkSolver>(contains(solverName, "Visco"),
                                           contains(solverName, "Rayleigh"));
  THROW("Solver with name " + solverName + " not found.")
}

void ElastodynamicTimeIntegrator::Initialize(IElasticity &assemble, Vector &u) const {
  assemble.SetInitialValue(u);
  nonlinearSolver->Initialize(assemble, u);

  assemble.PrintIteration(u);
  assemble.PlotIteration(u);
}

bool ElastodynamicTimeIntegrator::Method(IElasticity &assemble, Vector &u) const {
  mout.StartBlock("Dynamics");
  vout(1) << "Starting " << nonlinearSolver->Name() << " Method for dynamic elasticity problem"
          << endl;

  Vector uNew(u);

  Initialize(assemble, uNew);
  while (!assemble.IsFinished()) {
    Step(assemble, uNew);
  }
  u = uNew;
  mout.EndBlock();
  return true;
}

bool ElastodynamicTimeIntegrator::Step(IElasticity &assemble, Vector &u) const {
  assemble.NextTimeStep();
  nonlinearSolver->operator()(assemble, u);
  if (!nonlinearSolver->converged()) {
    return false;
  }
  assemble.FinishTimeStep(u);

  assemble.PrintIteration(u);
  assemble.PlotIteration(u);

  return true;
}

