#ifndef ELASTICITYEULER_HPP
#define ELASTICITYEULER_HPP

#include "Newton.hpp"

#include "assemble/IElasticity.hpp"

class DynamicEulerSolver : public NewtonT<IElasticity> {
  bool solveViscoelastic{false};
  bool useRayleigh{false};
  double rAlpha{0.0};
  double rBeta{0.0};

  std::unique_ptr<Matrix> rayleighMatrix = nullptr;
  std::unique_ptr<Matrix> residualMatrix = nullptr;
  std::unique_ptr<Matrix> displacementMatrix = nullptr;
  std::unique_ptr<Matrix> velocityMatrix = nullptr;

  std::unique_ptr<Vector> velocity = nullptr;

  struct EulerValues {
    std::unique_ptr<Vectors> values{};
  public:
    void Init(const Vector &u) {
      values = std::make_unique<Vectors>(3, u);
      (*values)[1] = 0.0;
      (*values)[2] = 0.0;
    }

    Vector &Displacement() const {
      return (*values)[0];
    }

    Vector &Velocity() const {
      return (*values)[1];
    }

    Vector &Acceleration() const {
      return (*values)[2];
    }
  };

  EulerValues oldValues{};

  void updateOldValues(double dt, const Vector &u);
protected:
  double calculateResidualUpdate(const IElasticity &assemble, const Vector &u,
                                 Vector &r) const override;

  void calculateJacobiUpdate(const IElasticity &assemble, const Vector &u,
                             Matrix &J) const override;
public:
  explicit DynamicEulerSolver(bool viscoElastic = false, bool rayleigh = false)
      : NewtonT<IElasticity>(
      std::unique_ptr<LinearSolver>(GetLinearSolverByPrefix("Mech"))
  ), solveViscoelastic(viscoElastic), useRayleigh(rayleigh) {
    Config::Get("RayleighAlpha", rAlpha);
    Config::Get("RayleighBeta", rBeta);
  }

  void Initialize(const IElasticity &assemble, Vector &u) override;

  void operator()(const IElasticity &assemble, Vector &u) override;

  void GetDynamicVectors( std::vector<Vector> &values) const override {
    values = {oldValues.Velocity(),oldValues.Acceleration()};
  };

  std::string Name() const override;
};

#endif //ELASTICITYEULER_HPP
