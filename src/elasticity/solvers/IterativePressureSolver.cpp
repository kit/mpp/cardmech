#include <LagrangeDiscretization.hpp>
#include "TimeSeries.hpp"
#include "IterativePressureSolver.hpp"
#include <chrono>
#include "LagrangeTransfer.hpp"

void IterativePressureSolver::Initialize(IElasticity &assemble, Vector &u) {
  problem.Scale(0.0);
  assemble.SetInitialValue(u);
  solver.Initialize(assemble, u);

  if(vtk>0){
    assemble.PlotPressureIteration(u, 0);
  }
  assemble.PrintPressureIteration(u);
}

bool IterativePressureSolver::Method(IElasticity &assemble, Vector &u) {
  mout.StartBlock("Static");
  vout(1) << "Starting " << solver.Name() << " Method for static elasticity problem"
          << endl;

  Vector uNew(u);

  Initialize(assemble, uNew);
  while (!iteration.IsFinished()) {
    bool converged = Step(assemble, uNew);

    if(converged) {
      //mapping von corse to finer
      assemble.PrintPressureIteration(uNew);
      assemble.PlotPressureIteration(uNew, iteration.Step());
      u = uNew;
      mout.PrintInfo("SolverInfo", 1,
                     PrintInfoEntry("Convergence", converged));
    }
    else {
      mout.PrintInfo("SolverInfo", 1,
                     PrintInfoEntry("Convergence", converged));
      bool nextIter = nextPressureIteration(iteration.Step());
      if (nextIter)
        return Method(assemble, u);
      return false;
    }
  }
  mout.EndBlock();
  return true;
}

bool IterativePressureSolver::Step(IElasticity &assemble, Vector &u) {
  bool prestress{false};
  Config::Get("WithPrestress", prestress);
  std::string activeDeformation;
  Config::Get("activeDeformation", activeDeformation);
  if (activeDeformation != "" && prestress == false) {
    assemble.UpdateStretch(u);
  }
  iteration.NextTimeStep();
  assemble.Scale(iteration.Time());
  problem.Scale(iteration.Time());
  solver(assemble, u);
  return solver.converged();
}

bool KlotzPressureSolver::Step(IElasticity &assemble, Vector &u) {
  auto start = std::chrono::steady_clock::now();
  bool conv = IterativePressureSolver::Step(assemble, u);
  auto end = std::chrono::steady_clock::now();
  std::chrono::duration<double> duration = end - start;
  //std::cout << "elapsed time: " << duration.count() << "s\n";
  //double vol = mechA.CalculateVolume(uNew)[0];

  solver.PrintIterationInfo();
  mout.PrintInfo("KlotzCurve", verbose,
                 PrintInfoEntry("LV Pressure (in Pa)", problem.Pressure(assemble.Time(),Origin, PRESSURE_BC_LV),1),//(problem.Pressure(t, Origin, PRESSURE_BC_LV)/133.3223684)*1000
                 PrintInfoEntry("RV Pressure (in Pa)", problem.Pressure(assemble.Time(),Origin, PRESSURE_BC_RV), 1),
                 PrintInfoEntry("LA Pressure (in Pa)", problem.Pressure(assemble.Time(),Origin, PRESSURE_BC_LA), 1),
                 PrintInfoEntry("RA Pressure (in Pa)", problem.Pressure(assemble.Time(),Origin, PRESSURE_BC_RA), 1)
  );
  mout.PrintInfo("SolverInfo", 1,
                 PrintInfoEntry("Duration", duration.count()),
                 PrintInfoEntry("Convergence", conv)
  );
  return conv;
}

void CalculatePrestress(IElasticity &assemble, Vector &u) {
  int prestressSteps{1};
  Config::Get("PrestressSteps", prestressSteps);
  int prestressLevel=0;
  Config::Get("PrestressLevel", prestressLevel);
  Vector uPrestressLevel(assemble.GetSharedDisc(),prestressLevel);
  uPrestressLevel.Accumulate();
  IterativePressureSolver prestressSolver(assemble.GetElasticityProblem(), prestressSteps);
  prestressSolver.Method(assemble, uPrestressLevel);
  LagrangeTransfer lagrangeT(uPrestressLevel,u);
  lagrangeT.Prolongate(uPrestressLevel,u);
  assemble.InitializePrestress(u);
  //u.Clear();
}
