#ifndef STATICSOLVER_HPP
#define STATICSOLVER_HPP

#include "Newton.hpp"

#include "assemble/IElasticity.hpp"

class StaticSolver : public NewtonT<IElasticity>{
  std::unique_ptr<Matrix> residualMatrix = nullptr;
protected:
  double calculateResidualUpdate(const IElasticity &assemble, const Vector &u,
                                 Vector &r) const override;

  void calculateJacobiUpdate(const IElasticity &assemble, const Vector &u,
                             Matrix &J) const override;
public:
  StaticSolver() : NewtonT<IElasticity>(
      std::unique_ptr<LinearSolver>(GetLinearSolverByPrefix("Mech"))
          ) { }

  void Initialize(const IElasticity &assemble, Vector &u) override;

  std::string Name() const override {return "Static Mechanics Solver";}
};

#endif //STATICSOLVER_HPP
