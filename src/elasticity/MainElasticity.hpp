#ifndef MAINELASTICITY_H
#define MAINELASTICITY_H

#include "assemble/IElasticity.hpp"
#include "problem/ElasticityProblem.hpp"
#include "MainCardMech.hpp"
#include <plot/VtuPlot.hpp>

/// Is used to move ElasticityProblem, Discretization and displacement in MoveVector() function.
struct VectorStruct {
  std::unique_ptr<ElasticityProblem> problem{nullptr};
  std::shared_ptr<IDiscretization> disc{nullptr};
  std::unique_ptr<Vector> vec{nullptr};

  Vector &operator()() {
    return *vec;
  }

  bool Filled() const {
    return vec != nullptr;
  }
};

/**
 * Represents the body of the elasticity code, which is called if the elasticity/m++.conf is loaded.
 */
class MainElasticity : public MainCardMech {
  friend class MultiElasticity;
  friend class MultiDiscElasticity;

  void generateMesh(const std::string &model) const;

  int discDegree{1};

  std::unique_ptr<ElasticityProblem> elasticityProblem = nullptr;

  std::shared_ptr<IDiscretization> vectorDisc = nullptr;
  std::shared_ptr<IDiscretization> scalarDisc = nullptr;

  std::unique_ptr<IElasticity> mechA = nullptr;
  std::unique_ptr<IElasticity> prestressedEla = nullptr;

  std::unique_ptr<TimeSeries> tSeries = nullptr;

  std::unique_ptr<VtuPlot> plot;

  std::unique_ptr<Vector> displacement = nullptr;
  std::unique_ptr<Vector> stretch = nullptr;

  /**
   *  Creates a FiniteElasticityAssemble with MechPolynomialDegree and Time from config.
   */
  void initAssemble();

public:

  bool plotSolution = false;
  bool dualPrimalError = false;

  int plevel = 0, mlevel = 0;
  int pressureSteps{1};
  int prestressSteps{1};
  /// Constructor gets soms config entries like "Model", "MechLevel",...
  MainElasticity();

  void Initialize() override;

  void Initialize(std::unique_ptr<ElasticityProblem> problem);

  void ResetTime(double start, double end, int steps) override;

  void ResetLevel(int newLevel);

  void SetDegree(int degree);

  Vector &Run() override;

  std::vector<double> Evaluate(const Vector &solution) override;

  void EvaluateReferece(const Vector &solution, const Vector &reference);

  Vector &GetDisplacement() const;

  /// Gets corresponding ElasticityProblem.
  const ElasticityProblem &GetProblem() const { return *elasticityProblem; }
  /**
   *   If problem has an exact solution the displacement of the corresponding problem is used.
       Otherwise it is using the internal value of the displacement.
   */
  void SetDisplacement(bool exact, Vector &u) const;
  /// Creates vector- and scalar-valued discretizations and generates a displacement vector.
  void generateStartVectors();
  /// Evaluates some error measures for the solution.
  double EvaluateQuantity(const Vector &solution,
                          const std::string &quantity) const;

  std::array<double, 4> GetVolume(const Vector &solution) const;

  std::pair<double, double> GetGeometryVolume(const Vector &solution) const;


  Vector &Run(const Vector &start);
  /// VectorStruct moves ElasticityProblem, Discretization and displacement.
  VectorStruct MoveVector();

  [[nodiscard]] IElasticity &GetAssemble() const { return *mechA; }
};

#endif //MAINELASTICITY_H
