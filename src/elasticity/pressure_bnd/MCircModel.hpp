#ifndef _MCIRC_MODEL_H_
#define _MCIRC_MODEL_H_

#include <iostream>
#include <array>
#include <memory>
#include <functional>

class MCircModel {
  double cSysArt{1.0};
  double cSysVen{1.0};
  double cPulArt{1.0};
  double cPulVen{1.0};

  double rSysArt{1.0};
  double rSysArtValve{1.0};
  double rSysPer{1.0};
  double rSysVen{1.0};
  double rRavValve{1.0};

  double rPulArt{1.0};
  double rPulArtValve{1.0};
  double rPulPer{1.0};
  double rPulVen{1.0};
  double rLavValve{1.0};

  // LV, RV, LA, RA, SysArt, SysVen, PulArt, PulVen
  std::array<double, 8> circVolumes{};

  std::array<double, 8> solveAlgebraic(std::array<double, 8>& volumes, const std::array<double, 4>& pressure) const;


  void rk4Step(double dt, double t, std::array<double, 8>& volumes, const std::array<double, 4>& pressure) const {
    std::array<double, 8> k{};
    std::array<std::array<double, 8>, 4> rkVolumes{};
    std::uninitialized_copy(volumes.begin(), volumes.end(), k.begin());


    rkVolumes[0] = solveAlgebraic(k, pressure);

    for (int i = 0; i < 8; ++i) {
      k[i] = volumes[i] + 0.5 * dt * rkVolumes[0][i];
    }
    rkVolumes[1] = solveAlgebraic(k, pressure);

    for (int i = 0; i < 8; ++i) {
      k[i] = volumes[i] + 0.5 * dt * rkVolumes[1][i];
    }
    rkVolumes[2] = solveAlgebraic(k, pressure);

    for (int i = 0; i < 8; ++i) {
      k[i] = volumes[i] + dt * rkVolumes[2][i];
    }
    rkVolumes[3] = solveAlgebraic(k, pressure);


    for (int i = 0; i < 8; ++i) {
      volumes[i] += (dt / 6.0) *
                        (rkVolumes[0][i] + 2.0 * rkVolumes[1][i] + 2.0 * rkVolumes[2][i] +
                         rkVolumes[3][i]);
    }

  }

public:
  MCircModel() :
      cSysArt(3.0), cSysVen(150.0), cPulArt(10.0), cPulVen(15.0),
      rSysArt(0.03), rSysArtValve(0.006), rSysPer(0.6), rSysVen(0.03), rRavValve(0.003),
      rPulArt(0.02), rPulArtValve(0.003), rPulPer(0.07), rPulVen(0.03), rLavValve(0.003) {

    SolveStep = [*this](double dt, double t, std::array<double, 8>& volumes, const std::array<double, 4>& pressure){
      this->rk4Step(dt, t, volumes, pressure);
    };
  };

  std::function<void(double, double, std::array<double, 8>&, const std::array<double, 4>&)> SolveStep;
  

};



#endif