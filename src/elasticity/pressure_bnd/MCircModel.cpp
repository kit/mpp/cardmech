#include "MCircModel.hpp"


std::array<double, 8> MCircModel::solveAlgebraic(std::array<double, 8>& circV, const std::array<double, 4>& pressure) const {
  // SysArt, SysVen, PulArt, PulVen
  std::array<double, 4> circPressures{};
  // SysArt, SysPer, SysVen, Rav, PulArt, PulPer, PulVen, Lav
  std::array<double, 8> flows{};

  flows[0] = std::max((pressure[0] - circV[4] / cSysArt) / (rSysArt + rSysArtValve), 0.0);
  flows[1] = (circV[4] / cSysArt - circPressures[1]) / rSysPer;
  flows[2] = (circPressures[1] - pressure[3]) / rSysVen;
  flows[3] = std::max((pressure[3] - pressure[1]) / rRavValve, 0.0);

  flows[4] = std::max((pressure[1] - circV[6] / cPulArt) / (rPulArt + rPulArtValve), 0.0);
  flows[5] = (circV[6] / cPulArt - circPressures[3]) / rPulPer;
  flows[6] = (circPressures[3] - pressure[2]) / rPulVen;
  flows[7] = std::max((pressure[2] - pressure[0]) / rLavValve, 0.0);

  circPressures[0] = pressure[0] - rSysArtValve * flows[0];
  circPressures[1] = circV[2] / cSysVen;
  circPressures[2] = pressure[1] - rPulArtValve * flows[4];
  circPressures[3] = circV[7] / cPulVen;

  return std::array<double, 8>{
      flows[7] - flows[0],
      flows[3] - flows[5],
      flows[6] - flows[7],
      flows[2] - flows[4],
      flows[0] - flows[1],
      flows[1] - flows[2],
      flows[4] - flows[5],
      flows[5] - flows[6],
  };
}