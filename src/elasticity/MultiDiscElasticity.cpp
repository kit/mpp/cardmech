#include "MultiDiscElasticity.hpp"



MultiDiscElasticity::MultiDiscElasticity() : main() {
  Config::Get("multiDiscs", multiDiscs);
  Config::Get("useDifferentMeshes", useDifferentMeshes);
  Config::Get("useDifferentDegs", useDifferentDegs);
  Config::Get("MechDiscretizationList", discs);
  solutions.resize(discs.size());
  meshes.resize(discs.size());
  degrees.resize(discs.size());
  DGPenalties.resize(discs.size());

  Config::Get("MechPolynomialDegreeList", degreeList);
  Config::Get("MeshList", meshesList);
  Config::Get("DGPenaltyList", DGPenaltyList);
}


void MultiDiscElasticity::Initialize() {

  if (degreeList.size() == 1){
    for(int i = 0; i <= discs.size()-1; ++i) {
      degrees[i] = degreeList[0];
    }
  } else {
    if (degreeList.size() != degrees.size()) THROW("List of MechPolynomialDegrees is not matching!")
    degrees = degreeList;
  }


  if (meshesList.size() == 1){
    for(int i = 0; i <= discs.size()-1; ++i) {
      meshes[i] = meshesList[0];
    }
  } else {
    if (meshesList.size() != meshes.size()) THROW("List of MechPolynomialDegrees is not matching!")
    meshes = meshesList;
  }

  DGPenalties = DGPenaltyList;

  for (int j = 0; j < degrees.size(); ++j){
    if (degrees[j] == degrees[j+1]){
      useDifferentDegs = false;
    }
  }
  auto elasticityProblemLocal = GetElasticityProblem();
  elasticityProblemLocal->SetModel(discs[0]); // sets disc
  elasticityProblemLocal->SetDGPenalty(DGPenalties[0]);
  if (useDifferentMeshes) {
    elasticityProblemLocal->SetMeshName(meshes[0]);
  }
  main.SetDegree(degrees[0]);
  main.Initialize(std::move(elasticityProblemLocal));
}

Vector &MultiDiscElasticity::Run() {
  for (int i = 0; i <= discs.size()-1; ++i) {
    mout << "\n=================== Running discretization " << discs[i] << " ================ " << endl;
    auto elasticityProblemLocal = GetElasticityProblem();
    elasticityProblemLocal->SetModel(discs[i]);

    elasticityProblemLocal->SetDGPenalty(DGPenalties[i]);

    if (useDifferentMeshes) {
      elasticityProblemLocal->SetMeshName(meshes[i]);
    }
    main.SetDegree(degrees[i]);
    main.Initialize(std::move(elasticityProblemLocal));

    bool usePrevDisc =  (multiDiscs && i > 0 && !useDifferentMeshes && !useDifferentDegs);

    if ((usePrevDisc && useDifferentMeshes) || (usePrevDisc && useDifferentDegs)){
      THROW("Not implemented right now!")
    }

    main.pressureSteps = 1;
    const auto &solution = usePrevDisc ? main.Run(solutions[i - 1]()) : main.Run();

    main.Evaluate(solution);
    solutions[i] = main.MoveVector();
  }

  return solutions[discs.size() - 1]();
}

std::vector<double> MultiDiscElasticity::Evaluate(const Vector &solution) {
  mout << endl << "=============================================================" << endl << endl;
  return {};
}
