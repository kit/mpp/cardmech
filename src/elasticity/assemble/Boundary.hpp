#ifndef BOUNDARY_H
#define BOUNDARY_H

const int FREE_BC = 0;

const int DIRICHLET_BC = 1; //Dirichlet b.c. for x, y, and z-direction
const int NEUMANN_BC = 2;

const int CONTACT_BC = 7;

const int DIRICHLET_X_BC = 100; //Dirichlet b.c. for x-direction
const int DIRICHLET_Y_BC = 101; //Dirichlet b.c. for y-direction
const int DIRICHLET_Z_BC = 102; //Dirichlet b.c. for z-direction
const int DIRICHLET_SYM_X_BC = 110; //Dirichlet symmetry b.c. for x-dir (u_x=0)
const int DIRICHLET_SYM_Y_BC = 111; //Dirichlet symmetry b.c. for y-dir (u_y=0)
const int DIRICHLET_SYM_Z_BC = 112; //Dirichlet symmetry b.c. for z-dir (u_z=0)
const int DIRICHLET_FREE_X_BC = 131; //Dirichlet b.c. for y- and z-direction
const int DIRICHLET_FREE_Y_BC = 132; //Dirichlet b.c. for x- and z-direction
const int DIRICHLET_FREE_Z_BC = 133; //Dirichlet b.c. for x- and y-direction
const int DIRICHLET_FIX_BC = 199; //Dirichlet b.c. all dir (u_x= u_y= u_z =0)
const int HOLE_BC = 333; //This is a hole

static void Dirichlet(int dim,
                      int k, RowBndValues &u_c, const VectorField &D) {
    for (int d = 0; d < dim; ++d) {
        u_c(k, d) = D[d];
        u_c.D(k, d) = true;
    }
}

static void Dirichlet_X(int k, RowBndValues &u_c, const VectorField &D) {
    u_c(k, 0) = D[0];
    u_c.D(k, 0) = true;
}

static void Dirichlet_Y(int k, RowBndValues &u_c, const VectorField &D) {
    u_c(k, 1) = D[1];
    u_c.D(k, 1) = true;
}

static void Dirichlet_Z(int k, RowBndValues &u_c, const VectorField &D) {
    u_c(k, 2) = D[2];
    u_c.D(k, 2) = true;
}

static void Dirichlet_FIX(int dim, int k, RowBndValues &u_c) {
    for (int d = 0; d < dim; ++d) {
        u_c(k, d) = 0.0;
        u_c.D(k, d) = true;
    }
}

static void Dirichlet_SYM_X(int k, RowBndValues &u_c) {
    u_c(k, 0) = 0.0;
    u_c.D(k, 0) = true;
}

static void Dirichlet_SYM_Y(int k, RowBndValues &u_c) {
    u_c(k, 1) = 0.0;
    u_c.D(k, 1) = true;
}

static void Dirichlet_SYM_Z(int k, RowBndValues &u_c) {
    u_c(k, 2) = 0.0;
    u_c.D(k, 2) = true;
}

static bool SetDirichletValues(int bc,
                               int dim,
                               int k,
                               RowBndValues &u_c,
                               const VectorField &D) {
    switch (bc) {
        case FREE_BC:
            return true;
        case DIRICHLET_BC:
            Dirichlet(dim, k, u_c, D);
            return true;
        case DIRICHLET_X_BC:
            Dirichlet_X(k, u_c, D);
            return true;
        case DIRICHLET_Y_BC:
            Dirichlet_Y(k, u_c, D);
            return true;
        case DIRICHLET_Z_BC:
            Dirichlet_Z(k, u_c, D);
            return true;
        case DIRICHLET_FIX_BC:
            Dirichlet_FIX(dim, k, u_c);
            return true;
        case DIRICHLET_SYM_X_BC:
            Dirichlet_SYM_X(k, u_c);
            return true;
        case DIRICHLET_SYM_Y_BC:
            Dirichlet_SYM_Y(k, u_c);
            return true;
        case DIRICHLET_SYM_Z_BC:
            Dirichlet_SYM_Z(k, u_c);
            return true;
    }
    return false;
}

static bool Dirichlet_PrescribedDisplacement_BC(int bc) {
    switch (bc) {
        case DIRICHLET_BC:
            return true;
        case DIRICHLET_X_BC:
            return true;
        case DIRICHLET_Y_BC:
            return true;
        case DIRICHLET_Z_BC:
            return true;
    }
    return false;
}

static bool Dirichlet_Fixed_BC(int bc) {
    switch (bc) {
        case DIRICHLET_FIX_BC:
            return true;
        case DIRICHLET_SYM_X_BC:
            return true;
        case DIRICHLET_SYM_Y_BC:
            return true;
        case DIRICHLET_SYM_Z_BC:
            return true;
    }
    return false;
}

#endif //BOUNDARY_H

