#include "DGElasticity.hpp"
#include "DGDoF.hpp"
#include "DGVectorFieldElement.hpp"
#include "VectorAccess.hpp"
#include "VectorFieldElement.hpp"

DGElasticity::DGElasticity(ElasticityProblem &eP, int degree, bool isStatic)
    : IElasticity(eP), disc(std::make_shared<const DGDiscretization>(eP.GetMeshes(), degree, dim())) {
  Config::Get("DGSign", sign);

  penalty = eP.GetDGPenalty();
  if (penalty == 0)   Config::Get("DGPenalty", penalty);

  this->degree = degree;
}

double DGElasticity::Energy(const Vector &u) const {
  double energy = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    Energy(c, u, energy);
  }
  return PPM->Sum(energy);
}


void DGElasticity::Energy(const cell &c, const Vector &U, double &energy) const {
  DGVectorFieldElement E(U, *c);
  const auto &cellMat = eProblem.GetMaterial(c);

  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);


    Tensor F = DeformationGradient(E, q, U);

//    VectorField v = timeScheme->Velocity<DGVectorFieldElement>(U, E, q);
//    energy += w * rho * (v * v);
    energy += w * cellMat.TotalEnergy(F, Q);
  }

  BFParts bnd(U.GetMesh(), *c);
  if (!bnd.onBnd()) return;

  if (bnd.onBnd()) {
    for (int face = 0; face < c.Faces(); ++face) {
      int bc = bnd[face];
      if (bc == 2) {
        energy -= TractionEnergy(c, face, bc, U);
      }

      if ((bc > 229) && (bc < 234)) {
        energy += PressureEnergy(c, face, bc, U);
      }
    }
  }
}


double DGElasticity::TractionEnergy(const cell &c, int face, int bc, const Vector &U) const {
  double W_Neumann = 0.0;
  DGVectorFieldFaceElement E(U, *c, face);

  Tensor Q = RotationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    VectorField n = E.QNormal(q);
    VectorField TN = eProblem.TractionStiffness(Time(), E.QPoint(q), c.Subdomain()) *
                     (Product(n, n) * E.VectorValue(q, U));
    W_Neumann += w * TN * TN;
  }
  return W_Neumann;
}


double DGElasticity::PressureEnergy(const cell &c, int face, int bc, const Vector &U) const {
  double W_Pressure = 0.0;
  DGVectorFieldFaceElement E(U, *c, face);

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    VectorField N = E.QNormal(q);

    double localPressure = -eProblem.Pressure(Time(), E.QPoint(q), bc);
    W_Pressure += w * localPressure * (N * E.VectorValue(q, U));
  }

  return W_Pressure;
}


void DGElasticity::Residual(const cell &c, const Vector &U, Vector &r) const {
  DGVectorFieldElement E(U, *c);
  DGSizedRowBndValues r_c(r, *c, E.shape_size());

  Tensor Q = OrientationMatrix(c.GetData());
  const Material &cellMat = eProblem.GetMaterial(c);

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    VectorField u = E.VectorValue(q, U);

    Tensor F = DeformationGradient(E, q, U);
    auto Fiso = eProblem.IsochoricPart(F);

    Tensor inverseFA = mat::active_deformation(Q, E.VectorValue(q, *gamma));
    F = F * inverseFA;

    VectorField f = eProblem.Load(Time(), E.QPoint(q), *c);
    double T = eProblem.ActiveStress(Time(), E.QPoint(q));

    for (int i = 0; i < E.shape_size(); ++i)
      for (int k = 0; k < E.Dim(); ++k) {
        auto u_ik = E.VectorComponentValue(q, i, k); // Verschiebungseinträge
        auto F_ik = E.VectorRowGradient(q, i, k);

        // Passive material response
        r_c(i, k) += w * cellMat.TotalDerivative(Fiso, F, Q, F_ik);

        // Body load
        r_c(i, k) -= w * (f * u_ik);
      }
  }
  for (int f = 0; f < c.Faces(); ++f) {
    double s = cellMat.Isochoric().GetMaxParameter() *
               (pow(degree, 2) * penalty) / U.GetMesh().MaxMeshWidth();

    DGVectorFieldFaceElement FE(U, *c, f);
    if (E.Bnd(f) >= PRESSURE_BC_LV && E.Bnd(f) <= PRESSURE_BC_RA) {// Neumann-Randbedingung
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point z = FE.QPoint(q);

        Tensor F = DeformationGradient(FE, q, U);
        VectorField N = mat::cofactor(F) * FE.QNormal(q);
        if (contains(cellMat.Name(), "Linear") || contains(cellMat.Name(), "Laplace")) {
          N = FE.QNormal(q);
        }
        double localPressure = -eProblem.Pressure(Time(), z, E.Bnd(f));
        for (int i = 0; i < FE.shape_size(); ++i)
          for (int k = 0; k < E.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            r_c(i, k) -= w * localPressure * N * u_ik;
          }
      }
    } else if ((E.Bnd(f) >= ROBIN_BC_EPI) && (E.Bnd(f) <= ROBIN_BC_BASE)) {
      TractionBoundary(c, f, E.Bnd(f), U, r);
    } else if (E.Bnd(f) == 1) { // Dirichlet-RB
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point z = FE.QPoint(q);
        VectorField N = FE.QNormal(q);
        VectorField u = eProblem.Deformation(Time(), z);
        VectorField u_diff = FE.VectorValue(q, U) - u;
        // mout << OUT(z) << OUT(U) << OUT(U_diff) << endl;
        Tensor F = DeformationGradient(FE, q, U);
        auto Fiso = eProblem.IsochoricPart(F);
        Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
        F = F * inverseFA;

        for (int i = 0; i < FE.shape_size(); ++i)
          for (int k = 0; k < E.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto Du_ik = FE.VectorRowGradient(q, i, k);
            double SN = cellMat.TotalDerivative(Fiso, F, Q, Product(u_ik, N));
            double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik, Product(u_diff, N));

            r_c(i, k) -= w * (SN + SN_ik * sign - s * u_diff * u_ik);
            //r(c(), k * E.shape_size() + i)
            //-= w * (SN + SN_ik * sign - s * u_diff * u_ik);
          }
      }

    } else if (E.Bnd(f) == 199) { // Dirichlet-RB
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point z = FE.QPoint(q);
        VectorField N = FE.QNormal(q);
        VectorField u_diff = FE.VectorValue(q, U);
        Tensor F = DeformationGradient(FE, q, U);
        auto Fiso = eProblem.IsochoricPart(F);
        Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
        F = F * inverseFA;

        for (int i = 0; i < FE.shape_size(); ++i)
          for (int k = 0; k < E.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto Du_ik = FE.VectorRowGradient(q, i, k);

            double SN = cellMat.TotalDerivative(Fiso, F, Q, Product(u_ik, N));
            double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik, Product(u_diff, N));
            r_c(i, k) -= w * (SN + SN_ik * sign - s * u_diff * u_ik);
          }
      }

    } else if (E.Bnd(f) == -1) {
      cell cf = r.find_neighbour_cell(c, f);
      if (cf() < c()) continue;
      int f1 = r.find_neighbour_face_id(c.Face(f), cf);
      DGVectorFieldElement E_nghbr(U, *cf);
      DGVectorFieldFaceElement FE_nghbr(r, *cf, f1);
      DGSizedRowBndValues r_cf(r, *cf, E_nghbr.shape_size());

      s = cellMat.Isochoric().GetMaxParameter() * (pow(degree, 2) * penalty) /
          U.GetMesh().MaxMeshWidth();
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point z = FE.QPoint(q);
        VectorField N = FE.QNormal(q);
        // für erstes Face-Element
        VectorField u = FE.VectorValue(q, U);

        Tensor F = DeformationGradient(FE, q, U);
        auto Fiso = eProblem.IsochoricPart(F);
        Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
        F = F * inverseFA;

        // das gleiche für das zweite Face-Element
        int q1 = FE.findQPointID(FE_nghbr, z);
        VectorField u_nghbr = FE_nghbr.VectorValue(q1, U);

        Tensor F_nghbr = DeformationGradient(FE_nghbr, q1, U);
        auto Fiso_nghbr = eProblem.IsochoricPart(F_nghbr);
        Tensor inverseFA_nghbr = mat::active_deformation(Q, FE_nghbr.VectorValue(q1, *gamma));
        F_nghbr = F_nghbr * inverseFA_nghbr;

        for (int i = 0; i < FE.shape_size(); ++i) {
          for (int k = 0; k < E.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto Du_ik = FE.VectorRowGradient(q, i, k);

            double SN = cellMat.TotalDerivative(Fiso, F, Q, Product(u_ik, N));
            double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik, Product(u, N));

            double SN_nghbr = cellMat.TotalDerivative(Fiso_nghbr, F_nghbr, Q, Product(u_ik, N));
            double SN_ik_nghbr = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                               Product(u_nghbr, N));
            r_c(i, k) += w * (s * u * u_ik - 0.5 * SN - 0.5 * SN_ik * sign);
            r_c(i, k) += w * (-s * u_nghbr * u_ik - 0.5 * SN_nghbr + 0.5 * SN_ik_nghbr * sign);
          }
        }

        for (int j = 0; j < FE_nghbr.shape_size(); ++j)
          for (int k = 0; k < E.Dim(); ++k) {
            auto u_jk = FE_nghbr.VectorComponentValue(q1, j, k);
            auto Du_jk = FE_nghbr.VectorRowGradient(q1, j, k);

            double SN = cellMat.TotalDerivative(Fiso, F, Q, Product(u_jk, N));
            double SN_jk = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_jk,
                                                         Product(u, N));

            double SN_nghbr = cellMat.TotalDerivative(Fiso_nghbr, F_nghbr, Q, Product(u_jk, N));
            double SN_jk_nghbr = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_jk,
                                                               Product(u_nghbr, N));

            r_cf(j, k) += w * (s * u_nghbr * u_jk + 0.5 * SN_nghbr + 0.5 * SN_jk_nghbr * sign);
            r_cf(j, k) += w * (-s * u * u_jk + 0.5 * SN - 0.5 * SN_jk * sign);
          }
      }
    }
  }
}

void DGElasticity::DirichletBoundary(const cell &c, int face, int bc, const Vector &U,
                                     Vector &r) const {
  DGVectorFieldElement E(U, *c);
  DGVectorFieldFaceElement FE(U, *c, face);
  DGSizedRowBndValues r_c(r, *c, E.shape_size());
  const auto &cellMat = eProblem.GetMaterial(c);
  double s = cellMat.Isochoric().GetMaxParameter() *
             (pow(degree, 2) * penalty) / U.GetMesh().MaxMeshWidth();
  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < FE.nQ(); ++q) {
    double w = FE.QWeight(q);
    Point z = FE.QPoint(q);
    VectorField N = FE.QNormal(q);
    VectorField u = FE.VectorValue(q, U) - eProblem.Deformation(Time(), z);
    Tensor F = DeformationGradient(FE, q, U);
    auto Fiso = eProblem.IsochoricPart(F);
    Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
    F = F * inverseFA;

    for (int i = 0; i < FE.shape_size(); ++i)
      for (int k = 0; k < E.Dim(); ++k) {
        auto u_ik = FE.VectorComponentValue(q, i, k);
        auto Du_ik = FE.VectorRowGradient(q, i, k);
        double SN = cellMat.TotalDerivative(Fiso, F, Q, Product(u_ik, N));
        double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik, Product(u, N));
        r_c(i, k) -= w * (SN + SN_ik * sign - s * (u * u_ik));
      }
  }
}


void DGElasticity::FixationBoundary(const cell &c, int face, int bc, const Vector &U,
                                    Vector &r) const {
  DGVectorFieldElement E(U, *c);
  DGVectorFieldFaceElement FE(U, *c, face);
  DGSizedRowBndValues r_c(r, *c, E.shape_size());
  const auto &cellMat = eProblem.GetMaterial(c);
  double s = cellMat.Isochoric().GetMaxParameter() *
             (pow(degree, 2) * penalty) / U.GetMesh().MaxMeshWidth();
  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < FE.nQ(); ++q) {
    double w = FE.QWeight(q);
    Point z = FE.QPoint(q);
    VectorField N = FE.QNormal(q);
    VectorField u = FE.VectorValue(q, U);
    // mout << OUT(z) << OUT(U) << OUT(U_diff) << endl;
    Tensor F = DeformationGradient(FE, q, U);
    auto Fiso = eProblem.IsochoricPart(F);
    Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
    F = F * inverseFA;

    for (int i = 0; i < FE.shape_size(); ++i)
      for (int k = 0; k < E.Dim(); ++k) {
        auto u_ik = FE.VectorComponentValue(q, i, k);
        auto Du_ik = FE.VectorRowGradient(q, i, k);
        double SN = cellMat.TotalDerivative(Fiso, F, Q, Product(u_ik, N));
        double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik, Product(u, N));
        r_c(i, k) -= w * (SN + SN_ik * sign - s * (u * u_ik));
      }
  }
}

void DGElasticity::PressureBoundary(const cell &c, int face, int bc, const Vector &U,
                                    Vector &r) const {
  DGVectorFieldElement E(U, *c);
  DGVectorFieldFaceElement FE(U, *c, face);
  DGRowValues r_c(r, *c, E.shape_size());

  for (int q = 0; q < FE.nQ(); ++q) {
    double w = FE.QWeight(q);
    VectorField N = FE.QNormal(q);


    double localPressure = -eProblem.Pressure(Time(), FE.QPoint(q), bc);

    for (int i = 0; i < FE.shape_size(); ++i) {
      for (int k = 0; k < E.Dim(); ++k) {
        //mout << "InPressure" << endl;
        auto u_ik = FE.VectorComponentValue(q, i, k);

        r_c(i, k) -= w * (localPressure) * N * u_ik;
      }
    }
  }
}

void DGElasticity::TractionBoundary(const cell &c, int face, int bc, const Vector &U,
                                    Vector &r) const {
  DGVectorFieldElement E(U, *c);
  DGVectorFieldFaceElement FE(U, *c, face);
  DGRowValues r_c(r, *c, E.shape_size());

  for (int q = 0; q < FE.nQ(); ++q) {
    double w = FE.QWeight(q);
    Point xi = FE.QPoint(q);
    VectorField n = FE.QNormal(q);
    VectorField normalStiffness = eProblem.TractionStiffness(Time(), xi, c.Subdomain()) *
                                  (Product(n, n) * E.VectorValue(q, U));

    for (int i = 0; i < FE.shape_size(); ++i) {
      for (int k = 0; k < E.Dim(); ++k) {
        auto u_ik = FE.VectorComponentValue(q, i, k);
        r_c(i, k) += w * normalStiffness * u_ik;
      }
    }
  }
}

static double
JacobiStress(double T, const VectorField &f, const Tensor &F, const Tensor &H, const Tensor &G) {
  double normf = norm(F * f);
  Tensor dyadicf = Product(f, f);

  return T * det(F) / normf * (
      Frobenius(transposeInvert(F), G) * Frobenius(F * dyadicf, H) +
      Frobenius(G * dyadicf, H) -
      Frobenius(F * dyadicf, transpose(G)) / (normf * normf)
  );
}

static double JacobiDeadStress(double T, const VectorField &f, const Tensor &F, const Tensor &H,
                               const Tensor &G) {
  Tensor Fit = transposeInvert(F);
  return -T * Frobenius(Fit * H * Product(f, f) * Fit, G);
}


void DGElasticity::Jacobi(const cell &c, const Vector &U, Matrix &A) const {
  DGVectorFieldElement E(A, *c);
  DGSizedRowEntries A_c(A, *c, E.shape_size(), *c, E.shape_size());


  const Material &cellMat = eProblem.GetMaterial(c);
  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    Tensor F = DeformationGradient(E, q, U);
    auto Fiso = eProblem.IsochoricPart(F);

    Tensor inverseFA = mat::active_deformation(Q, E.VectorValue(q, *gamma));
    F = F * inverseFA;

    double T = eProblem.ActiveStress(Time(), E.QPoint(q));

    for (int i = 0; i < E.shape_size(); ++i) {
      for (int k = 0; k < E.Dim(); ++k) {
        auto u_ik = E.VectorComponentValue(q, i, k);
        auto F_ik = E.VectorRowGradient(q, i, k) * transpose(inverseFA);
        for (int j = 0; j < E.shape_size(); ++j) {
          for (int l = 0; l < E.Dim(); ++l) {
            auto u_jl = E.VectorComponentValue(q, j, l);
            auto F_jl = E.VectorRowGradient(q, j, l) * transpose(inverseFA);

            // Passive Material response
            A_c(i, j, k, l) +=
                w * cellMat.TotalSecondDerivative(Fiso, F, Q, F_ik, F_jl);

          }
        }
      }
    }
  }
  for (int face = 0; face < c.Faces(); ++face) {
    double s = cellMat.Isochoric().GetMaxParameter() *
               (pow(degree, 2) * penalty) / U.GetMesh().MaxMeshWidth();
    DGVectorFieldFaceElement FE(U, *c, face);
    if (E.Bnd(face) == DIRICHLET_BC || E.Bnd(face) == DIRICHLET_FIX_BC) {
      for (int q = 0; q < FE.nQ(); ++q) {

        double w = FE.QWeight(q);
        Point z = FE.QPoint(q);
        VectorField N = FE.QNormal(q);
        VectorField u = FE.VectorValue(q, U);
        Tensor F = DeformationGradient(FE, q, U);
        auto Fiso = eProblem.IsochoricPart(F);
        Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
        F = F * inverseFA;

        for (int i = 0; i < FE.shape_size(); ++i)
          for (int k = 0; k < E.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto Du_ik = FE.VectorRowGradient(q, i, k);

            for (int j = 0; j < FE.shape_size(); ++j)
              for (int l = 0; l < E.Dim(); ++l) {
                auto u_jl = FE.VectorComponentValue(q, j, l);
                auto Du_jl = FE.VectorRowGradient(q, j, l);
                double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                             Product(u_jl, N));
                double SN_jl = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_jl,
                                                             Product(u_ik, N));
                A_c(i, j, k, l) -= w * (SN_ik * sign + SN_jl - s * (u_ik * u_jl));
              }
          }
      }
    }
    if (E.Bnd(face) >= PRESSURE_BC_LV && E.Bnd(face) <= PRESSURE_BC_RA) {
      if (contains(cellMat.Name(), "Linear") || contains(cellMat.Name(), "Laplace")) continue;
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        VectorField N = FE.QNormal(q);
        Tensor F = DeformationGradient(FE, q, U);
        double localPressure = -eProblem.Pressure(Time(), FE.QPoint(q), E.Bnd(face));
        for (int i = 0; i < FE.shape_size(); ++i) {
          for (int k = 0; k < E.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            for (int j = 0; j < FE.shape_size(); ++j) {
              for (int l = 0; l < E.Dim(); ++l) {
                auto F_jl = FE.VectorRowGradient(q, j, l);
                A_c(i, j, k, l) -= w * (localPressure) * (Cross(F, F_jl) * N) * u_ik;
              }
            }
          }
        }
      }
    }
    if (E.Bnd(face) == -1) {
      cell cf = U.find_neighbour_cell(c, face);
      if (cf() < c()) continue;
      DGVectorFieldElement E_nghbr(U, *cf);
      DGSizedRowEntries A_cf(A, *c, E.shape_size(), *cf, E_nghbr.shape_size());

      DGSizedRowEntries A_fc(A, *cf, E_nghbr.shape_size(), *c, E.shape_size());
      DGSizedRowEntries A_ff(A, *cf, E_nghbr.shape_size(), *cf, E_nghbr.shape_size());
      int f1 = U.find_neighbour_face_id(c.Face(face), cf);
      DGVectorFieldFaceElement FE_nghbr(U, *cf, f1);

      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point z = FE.QPoint(q);
        VectorField N = FE.QNormal(q);


        Tensor F = DeformationGradient(FE, q, U);
        auto Fiso = eProblem.IsochoricPart(F);
        Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
        F = F * inverseFA;

        int q_n = FE_nghbr.findQPointID(FE, z);
        Tensor F_nghbr = DeformationGradient(FE_nghbr, q_n, U);
        auto Fiso_nghbr = eProblem.IsochoricPart(F_nghbr);
        Tensor inverseFA_nghbr = mat::active_deformation(Q, FE_nghbr.VectorValue(q_n, *gamma));
        F_nghbr = F_nghbr * inverseFA_nghbr;

        for (int i = 0; i < FE.shape_size(); ++i) {
          for (int k = 0; k < E.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto Du_ik = FE.VectorRowGradient(q, i, k);
            for (int j = 0; j < FE.shape_size(); ++j) {
              for (int l = 0; l < E.Dim(); ++l) {
                auto u_jl = FE.VectorComponentValue(q, j, l);
                auto Du_jl = FE.VectorRowGradient(q, j, l);
                double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                             Product(u_jl, N));
                double SN_jl = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_jl,
                                                             Product(u_ik, N));
                A_c(i, j, k, l)
                    += w * (s * (u_ik * u_jl) - 0.5 * SN_ik * sign - 0.5 * SN_jl);
              }
            }

            for (int j = 0; j < FE_nghbr.shape_size(); ++j) {
              for (int l = 0; l < E.Dim(); ++l) {
                auto u_jl = FE_nghbr.VectorComponentValue(q_n, j, l);
                auto Du_jl = FE_nghbr.VectorRowGradient(q_n, j, l);
                double SN_jl = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q,  Du_jl,
                                                             Product(u_ik, N));
                double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                             Product(u_jl, N));
                A_cf(i, j, k, l)
                    += w * (-s * u_ik * u_jl + 0.5 * SN_ik * sign - 0.5 * SN_jl);

                A_fc(j, i, l, k)
                    += w * (-s * u_ik * u_jl + 0.5 * SN_ik - 0.5 * SN_jl * sign);
              }
            }
          }
        }
        for (int i = 0; i < FE_nghbr.shape_size(); ++i) {
          for (int k = 0; k < E.Dim(); ++k) {
            auto u_ik = FE_nghbr.VectorComponentValue(q_n, i, k);
            auto Du_ik = FE_nghbr.VectorRowGradient(q_n, i, k);
            for (int j = 0; j < FE_nghbr.shape_size(); ++j)
              for (int l = 0; l < E.Dim(); ++l) {
                auto u_jl = FE_nghbr.VectorComponentValue(q_n, j, l);
                auto Du_jl = FE_nghbr.VectorRowGradient(q_n, j, l);
                double SN_ik = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_ik,
                                                             Product(u_jl, N));
                double SN_jl = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_jl,
                                                             Product(u_ik, N));
                A_ff(i, j, k, l)
                    += w * (s * u_ik * u_jl + 0.5 * SN_ik * sign + 0.5 * SN_jl);
              }
          }
        }
      }
    }
  }
}


void DGElasticity::Project(const Vector &fine, Vector &u) const {
  int dlevel = fine.SpaceLevel() - u.SpaceLevel();

  std::vector<Point> childrenVerteces{};
  std::vector<Point> childVerteces{};
  std::vector<Point> coarseNodalPoints{};
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    std::vector<std::vector<std::unique_ptr<Cell>>> recursiveCells(dlevel + 1);
    recursiveCells[0].emplace_back(CreateCell(c.Type(), c.Subdomain(), c.AsVector(), false));
    for (int l = 1; l <= dlevel; ++l) {
      for (const auto &parent: recursiveCells[l - 1]) {
        const std::vector<Rule> &R = parent->Refine(childrenVerteces);
        for (int i = 0; i < R.size(); ++i) {
          R[i](childrenVerteces, childVerteces);
          recursiveCells[l].emplace_back(
              CreateCell(R[i].type(), c.Subdomain(), childVerteces, false));
        }
      }
    }
    auto r = u.find_row(c());
    int childCount = recursiveCells[dlevel].size();
    DGVectorFieldElement E(u, *c);

    for (int i = 0; i < E.NodalPoints(); ++i) {
      Point z = E.NodalPoint(i);
      bool found = false;
      for (const auto &child: recursiveCells[dlevel]) {
        DGVectorFieldElement CE(fine, *(fine.find_cell(child->Center())));
        for (int j = 0; j < CE.NodalPoints(); ++j) {
          if (CE.NodalPoint(j) == z) {
            auto rf = fine.find_row(child->Center());
            for (int k = 0; k < E.Dim(); ++k) {
              u(r, k * E.shape_size() + i) = fine(rf, k * CE.shape_size() + j);
            }
            found = true;
          }
          if (found)
            break;
        }
        if (found)
          break;
      }
    }
  }
}


void DGElasticity::Interpolate(const Vector &coarse, Vector &u) const {
  int dlevel = u.SpaceLevel() - coarse.SpaceLevel();

  std::vector<Point> childrenVerteces{};
  std::vector<Point> childVerteces{};
  std::vector<Point> coarseNodalPoints{};
  for (cell c = coarse.cells(); c != coarse.cells_end(); ++c) {
    std::vector<std::vector<std::unique_ptr<Cell>>> recursiveCells(dlevel + 1);
    recursiveCells[0].emplace_back(CreateCell(c.Type(), c.Subdomain(), c.AsVector(), false));
    for (int l = 1; l <= dlevel; ++l) {
      for (const auto &parent: recursiveCells[l - 1]) {
        const std::vector<Rule> &R = parent->Refine(childrenVerteces);
        for (int i = 0; i < R.size(); ++i) {
          R[i](childrenVerteces, childVerteces);
          recursiveCells[l].emplace_back(
              CreateCell(R[i].type(), c.Subdomain(), childVerteces, false));
        }
      }
    }

    int childCount = recursiveCells[dlevel].size();
    DGVectorFieldElement E(coarse, *c);


    for (const auto &child: recursiveCells[dlevel]) {
      auto fineRow = u.find_row(child->Center());
      cell fineCell = u.find_cell(child->Center());
      DGVectorFieldElement CE(coarse, *fineCell);

      // TODO: test for (int i = 0; i < E.shape_size(); ++i)
      for (int i = 0; i < CE.shape_size(); ++i) {
        auto x = c.GlobalToLocal(CE.NodalPoint(i));
        VectorField val = E.VectorValue(x, coarse);
        for (int k = 0; k < E.Dim(); ++k) {
          u(fineRow, k * E.shape_size() + i) = val[k];
        }
      }
    }
  }
  u.Accumulate();
}

void DGElasticity::UseLagrangeValuesAsStartVector(const Vector &u_conforming, Vector &u) const {
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    VectorFieldElement EC(u_conforming, *c);
    for (int i = 0; i < E.shape_size(); ++i) {
      Point x = E.NodalPoint(i);
      row r = u_conforming.find_row(x);
      if (r == u_conforming.rows_end()) continue;
      for (int k = 0; k < E.Dim(); ++k)
        u(c(), k * E.shape_size() + i) = u_conforming(r,k);
    }
    auto x = c.GlobalToLocal(c());
    VectorField val = E.VectorValue(x, u);
    VectorField valC = EC.VectorValue(x, u_conforming);
  }
  u.MakeAdditive();
  u.Accumulate();
}

double DGElasticity::StrainEnergyError(const Vector &u) const {
  double err = 0.0;

  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);

    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      const Point &z = E.QPoint(q);
      Tensor DD = (E.VectorGradient(q, u) - eProblem.DeformationGradient(Time(), z));
      err += w * abs(cellMat.TotalDerivative(One + DD, One + DD, Q, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}


double DGElasticity::StrainEnergyNorm(const Vector &u) const {
  double err = 0.0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      Tensor DD = E.VectorGradient(q, u);
      err += w * abs(cellMat.TotalDerivative(One + DD, One + DD, Q, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}

double DGElasticity::EnergyNorm(const Vector &u) const {
  double err = 0.0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      Tensor DD = E.VectorGradient(q, u);
      err += w * abs(cellMat.TotalSecondDerivative(One, One, Q, DD, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}

double DGElasticity::EnergyError(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      const Point &z = E.QPoint(q);
      Tensor DD = E.VectorGradient(q, u) - eProblem.DeformationGradient(Time(), z);
      err += w * abs(cellMat.TotalSecondDerivative(One, One, Q, DD, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}


double DGElasticity::EnergyValue(const Vector &u) const {
  double energy = 0.0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    const auto &cellMat = eProblem.GetMaterial(c);
    Tensor Q = OrientationMatrix(c.GetData());
    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      Tensor F = DeformationGradient(E, q, u);
      energy += w * cellMat.TotalEnergy(F, Q);
    }
  }
  return abs(PPM->Sum(energy));
}


double DGElasticity::StrainEnergyError(const Vector &u, const Vector &reference) const {
  Vector projection = Projection(u, reference);
  projection -= u;
  return EnergyNorm(projection);
}

double DGElasticity::L2Error(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u) - eProblem.Deformation(Time(), E.QPoint(q));
      err += E.QWeight(q) * (U * U);
    }
  }
  return sqrt(PPM->Sum(err));
}


double DGElasticity::L2Norm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u);
      err += E.QWeight(q) * (U * U);
    }
  }
  return sqrt(PPM->Sum(err));
}

double DGElasticity::L2AvgNorm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);

    double w = 0.0;
    VectorField U{};
    for (int q = 0; q < E.nQ(); ++q) {
      w += E.QWeight(q);
      U += E.VectorValue(q, u);
    }
    err += w * (U * U);

  }
  return sqrt(PPM->Sum(err));
}

double DGElasticity::L2Error(const Vector &u, const Vector &reference) const {
  Vector projection = Projection(u, reference);
  projection -= u;
  return L2Norm(projection);
}

double DGElasticity::H1Error(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u)
                      - eProblem.Deformation(Time(), E.QPoint(q));

      Tensor DU =
          E.VectorGradient(q, u) - eProblem.DeformationGradient(Time(), E.QPoint(q));
      err += E.QWeight(q) * ((U * U) + Frobenius(DU, DU));
    }
  }
  return sqrt(PPM->Sum(err));
}

double DGElasticity::H1Norm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u);
      Tensor DU = E.VectorGradient(q, u);
      err += E.QWeight(q) * ((U * U) + Frobenius(DU, DU));

    }
  }
  return sqrt(PPM->Sum(err));
}

double DGElasticity::H1Error(const Vector &u, const Vector &reference) const {
  Vector projection = Projection(u, reference);
  projection -= u;
  return H1Norm(projection);
}

void DGElasticity::prestress(const Vector &u, Vector &pStress) {
  Prestress = std::make_unique<Vector>(0.0, u);

  //TODO THis has to be updated
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    const auto &cellMat = eProblem.GetMaterial(c);

    DGVectorFieldElement E(u, *c);
    DGSizedRowBndValues r_c(*Prestress, *c, E.shape_size());

    Tensor Q = OrientationMatrix(c.GetData());

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);

      Tensor F = DeformationGradient(E, q, u);
      auto Fiso = eProblem.IsochoricPart(F);

      for (int i = 0; i < E.shape_size(); ++i) {
        for (int k = 0; k < E.Dim(); ++k) {
          auto F_ik = E.VectorRowGradient(q, i, k);

          // Passive material response
          r_c(i, k) += w * cellMat.TotalDerivative(Fiso, F, Q, F_ik);
        }
      }
    }
  }

  Prestress->ClearDirichletValues();
  Prestress->Collect();
}


double DGElasticity::PressureError(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    if (!E.Bnd()) continue;

    Tensor Q = OrientationMatrix(c.GetData());
    for (int f = 0; f < c.Faces(); ++f) {
      if (E.Bnd(f) < 230 || E.Bnd(f) > 233) continue;
      DGVectorFieldFaceElement FE(u, *c, f);
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point N = FE.QNormal(q);

        Tensor F = DeformationGradient(FE, q, u);
        Tensor Fiso = eProblem.IsochoricPart(F);
        Tensor S = eProblem.GetMaterial(c).FirstPiolaKirchhoff(F, Fiso, Q);

        VectorField SN =
            S * N - eProblem.Pressure(Time(), E.QPoint(q), E.Bnd(f)) * N;
        err += w * SN * SN;
      }
    }
  }
  return sqrt(PPM->Sum(err));
}

double DGElasticity::NeumannError(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    if (!E.Bnd()) continue;

    for (int f = 0; f < c.Faces(); ++f) {
      if (E.Bnd(f) != 2) continue;
      DGVectorFieldFaceElement FE(u, *c, f);
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point N = FE.QNormal(q);

        Tensor F = DeformationGradient(FE, q, u);
        Tensor Fiso = eProblem.IsochoricPart(F);
        Tensor S = eProblem.GetMaterial(c).FirstPiolaKirchhoff(F, Fiso, One);

        VectorField SN =
            S * N - eProblem.Pressure(Time(), E.QPoint(q), E.Bnd(f)) * N;
        err += w * SN * SN;
      }
    }
  }
  return sqrt(PPM->Sum(err));
}


double DGElasticity::NormPrestress() {
  return Prestress->Max();
}

double DGElasticity::MaxStress(const Vector &U) const {
  double cellS{0.0};
  double maxS{0.0};

  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    cellS = 0.0;
    DGVectorFieldElement E(U, *c);

    Tensor Q = RotationMatrix(c.GetData());
    Tensor QT = transpose(Q);
    Tensor QI = Invert(Q);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);

      Tensor F = DeformationGradient(E, q, U);
      Tensor Fiso = eProblem.IsochoricPart(F);
      auto P = eProblem.GetMaterial(c).FirstPiolaKirchhoff(F, Fiso, One);
      cellS += w * Frobenius(P, P);
    }

    maxS = std::max(maxS, cellS);
  }

  return maxS;
}


std::array<double, 4> DGElasticity::ChamberVolume(const Vector &U) const {
  std::array<double, 4> volumes{0.0, 0.0, 0.0, 0.0};
  Vector phi(U);

  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    DGVectorFieldElement E(U, *c);
    DGRowValues phi_c(phi, *c, E.shape_size());
    std::unordered_map<Point, size_t> pointToIndex;
    for (int i = 0; i < E.shape_size(); ++i) {
      pointToIndex[E.NodalPoint(i)] = i;
      for (int k = 0; k < E.Dim(); ++k) {
        phi_c(i, k) += E.NodalPoint(i)[k];

      }
    }
    BFParts bnd(U.GetMesh(), *c);
    if (!bnd.onBnd()) continue;

    for (int face = 0; face < c.Faces(); ++face) {
      int bc = bnd[face];
      if ((bc >= PRESSURE_BC_LV) && (bc <= PRESSURE_BC_RA)) {
        if (c.plane()) {
          size_t idx_a = pointToIndex[c.FaceCorner(face, 0)];
          size_t idx_b = pointToIndex[c.FaceCorner(face, 1)];

          double v = 0.5 * det(TensorT<double, 2>(phi_c(idx_a, 0), phi_c(idx_b, 0),
                                                  phi_c(idx_a, 1), phi_c(idx_b, 1)));
          volumes[bc % 230] += v;
        } else if (c.ReferenceType() == TETRAHEDRON || c.ReferenceType() == HEXAHEDRON) {

          size_t idx_a = pointToIndex[c.FaceCorner(face, 0)];
          size_t idx_b = pointToIndex[c.FaceCorner(face, 1)];
          size_t idx_c = pointToIndex[c.FaceCorner(face, 2)];

          double v = 1.0 / 6.0 * det(Tensor(
              phi_c(idx_a, 0), phi_c(idx_b, 0), phi_c(idx_c, 0),
              phi_c(idx_a, 1), phi_c(idx_b, 1), phi_c(idx_c, 1),
              phi_c(idx_a, 2), phi_c(idx_b, 2), phi_c(idx_c, 2)
          ));
          volumes[bc % 230] += v;
          if (c.ReferenceType() == HEXAHEDRON) {
            size_t idx_d = pointToIndex[c.FaceCorner(face, 3)];
            v = 1.0 / 6.0 * det(Tensor(
                phi_c(idx_a, 0), phi_c(idx_c, 0), phi_c(idx_d, 0),
                phi_c(idx_a, 1), phi_c(idx_c, 1), phi_c(idx_d, 1),
                phi_c(idx_a, 2), phi_c(idx_c, 2), phi_c(idx_d, 2)
            ));
            volumes[bc % 230] += v;
          }
          /*Assert(E.NodalPoints() == 4); // only degree 1
          Tensor locationMatrix(E.Dim(), E.Dim(), 0.0);
          size_t numberOfNPOnFace = phi.GetDoF().NumberOfNodalPointsOnFace(*c, face);
          Assert(numberOfNPOnFace == 3);
          Assert(E.Dim() == 3);
          for (size_t indexOfNPonFace = 0; indexOfNPonFace < numberOfNPOnFace; indexOfNPonFace++) {
            size_t idOfNPOnFace = phi.GetDoF().IdOfNodalPointOnFace(*c, face, indexOfNPonFace);
            for (size_t component = 0; component < E.Dim(); component++) {
              locationMatrix[component][indexOfNPonFace] = phi_c(idOfNPOnFace, component);
            }
          }
          double v = 1.0 / 6.0 * det(locationMatrix);*/
        } else {
          Warning("TODO implement DGElasticity::ChamberVolume for other celltypes")
        }

      }
    }
  }

  for (int i = 0; i < volumes.size(); ++i) {
    volumes[i] = abs(PPM->Sum(volumes[i])) * CUBICMMtoML;
  }
  return volumes;
}

void DGElasticity::GetInvariant(const Vector &displacement, Vector &iota4) const {
  iota4 = 0.0;
  for (cell c = displacement.cells(); c != displacement.cells_end(); ++c) {
    DGVectorFieldElement E(displacement, *c);
    //RowBndValues r_c(iota4, *c);

    const VectorField fibre = Direction(c.GetData());
    for (int j = 0; j < E.shape_size(); ++j) {
      Point xi = E[j]();
      iota4(xi, 0) += mat::invariant(4, DeformationGradient(E, xi, displacement), fibre);
      iota4(xi, 1) += 1.0;
    }
  }
  //iota4.Collect();

  for (row r = iota4.rows(); r != iota4.rows_end(); ++r) {
    iota4(r, 0) /= iota4(r, 1);
  }
  //iota4.Collect();
}

double DGElasticity::InitialVolume(const Mesh &M) const {
  double volume = 0.0;
  for (cell c = M.cells(); c != M.cells_end(); ++c) {
    volume += Volume(c.ReferenceType(), c.AsVector());
  }
  return PPM->Sum(volume);
}

double DGElasticity::DeformedVolume(const Vector &U) const {
  double volume = 0.0;
  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    DGVectorFieldElement E(U, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      Tensor F = DeformationGradient(E, q, U);
      double w = E.QWeight(q);
      volume += w * det(F);
    }
  }
  return PPM->Sum(volume);
}

void DGElasticity::PlotBoundary(const Vector &U, const Vector &scal) const {
  if (vtkplot < 1) return;
  Vector bnd(0.0, U.GetSharedDisc());

  auto &plot = mpp::plot("Boundary");
  for (cell c = U.cells(); c != U.cells_end(); c++) {
    DGVectorFieldElement E(U, *c);
    RowBndValues u_c(bnd, *c);
    if (!u_c.onBnd()) continue;

    for (int face = 0; face < c.Faces(); ++face) {
      DGVectorFieldFaceElement FE(U, *c, face);
      int bc = u_c.bc(face);
      bnd(c(), 0) = std::max(bnd(c(), 0), (double)bc);
      bnd(c(), 1) =
          std::max(bnd(c(), 1),
                   bc == 199 ? 1.0 : 0.0);  // Fixation Boundary bc == 199
    }
  }
  plot.AddData("Boundary", bnd, 0);
  plot.AddData("Fixation", bnd, 1);
  plot.PlotFile("Boundary");
}

Vector makePlottableDGVector(const Vector &U) {
  Vector plotU(0.0, U);
  for (cell c = U.cells(); c != U.cells_end(); c++) {
    row r = plotU.find_row(c());
    DGVectorFieldElement elem(U, *c);
    VectorField disp = elem.VectorValue(c.LocalCenter(), U);
    plotU(r, 0) = disp[0];
    plotU(r, 1) = disp[1];
    plotU(r, 2) = disp[2];
  }
  return plotU;
}

void DGElasticity::PlotDisplacement(const Vector &U, int step,
                                    const std::string &varname) const {
  // Plotting the deformation in src/CardiacAssemble.hpp
  PlotDeformed(U, {{"Displacement", makePlottableDGVector(U)}}, step, varname);
}

void DGElasticity::SetDisplacement(Vector &U) const {
  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    auto index = U.find_row(c());
    DGVectorFieldElement E(U, *c);
    for (int i = 0; i < E.NodalPoints(); ++i) {
      auto nodalPointValue = eProblem.Deformation(Time(), E.NodalPoint(i));
      for (int k = 0; k < E.Dim(); ++k) {
        U(index, k * E.shape_size() + i) = nodalPointValue[k];
      }
    }
  }
}


void DGElasticity::ProblemEvaluation(const Vector &u) const {
  std::string problemName{"None"};
  Config::Get("MechProblem", problemName);

  if (problemName == "CardiacBeam") {
    // Now in BeamProblem
  }
  if (problemName == "FullEllipsoid") {
    VectorField apexDisplacement{};
    VectorField pointDisplacemts{};
    int cellCount{0};
    int cellCount1{0};

    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      for (int i = 0; i < c.Corners(); ++i) {
        if (c.Corner(i) == Point(0.0, 0.0, -20.0)) {
          DGVectorFieldElement E(u, *c);
          apexDisplacement += E.VectorValue(c.LocalCorner(i), u);
          cellCount++;
        }
      }
    }
    cellCount = PPM->Sum(cellCount);
    if (cellCount > 0) {
      apexDisplacement = PPM->Sum(apexDisplacement) / cellCount;
    }

    const std::vector<Point> points = {Point(0, 1.4417481, -19.791044),
                                       (-5.886750221, 0, -16.16740036)};
    for (row r = u.rows(); r != u.rows_end(); ++r) {
      auto [i, isNear] = isIn(r(), points);
      if (isNear) {
        for (cell c = u.cells(); c != u.cells_end(); ++c) {
          for (int j = 0; j < c.Corners(); ++j) {
            if (c.Corner(j) == Point(0, 1.4417481, -19.791044)) {
              DGVectorFieldElement E(u, *c);
              pointDisplacemts += E.VectorValue(c.LocalCorner(j), u);
              cellCount1++;
            }
          }
        }
      }
    }
    cellCount1 = PPM->Sum(cellCount1);
    if (cellCount1 > 0) {
      pointDisplacemts = PPM->Sum(apexDisplacement) / cellCount;
    }

    mout << "Apex Displacement = " << apexDisplacement << endl;
    mout << "Apex Location = " << apexDisplacement[2] - 20.0 << endl;
    mout << "Point Displacement = " << pointDisplacemts << endl;
  }

  if (problemName == "LeftVentricle") {
    VectorField apexDisplacement{};
    VectorField pointDisplacemts{};
    int cellCount{0};
    int cellCount1{0};

    for (cell c = u.cells(); c != u.cells_end(); ++c) {
      for (int i = 0; i < c.Corners(); ++i) {
        if (c.Corner(i) == Point(84.434, -44.9479, -12.4509)) {
          DGVectorFieldElement E(u, *c);
          apexDisplacement += E.VectorValue(c.LocalCorner(i), u);
          cellCount++;
        }
      }
    }
    cellCount = PPM->Sum(cellCount);
    if (cellCount > 0) {
      apexDisplacement = PPM->Sum(apexDisplacement) / cellCount;
    }

    const std::vector<Point> points = {Point(84.434, -44.9479, -12.4509),
                                       (-5.886750221, 0, -16.16740036)};
    for (row r = u.rows(); r != u.rows_end(); ++r) {
      auto [i, isNear] = isIn(r(), points);
      if (isNear) {
        for (cell c = u.cells(); c != u.cells_end(); ++c) {
          for (int j = 0; j < c.Corners(); ++j) {
            if (c.Corner(j) == Point(84.434, -44.9479, -12.4509)) {
              DGVectorFieldElement E(u, *c);
              pointDisplacemts += E.VectorValue(c.LocalCorner(j), u);
              cellCount1++;
            }
          }
        }
      }
    }
    cellCount1 = PPM->Sum(cellCount1);
    if (cellCount1 > 0) {
      pointDisplacemts = PPM->Sum(apexDisplacement) / cellCount;
    }

    mout << "Apex Displacement = " << apexDisplacement << endl;
    mout << "Point Displacement = " << pointDisplacemts << endl;
  }
}


void DGElasticity::MassMatrix(Matrix &massMatrix) const {
  THROW("MassMatrix is not implemented for DG")
}

void DGElasticity::SystemMatrix(Matrix &systemMatrix) const {
  THROW("System is not implemented for DG")
}

void DGElasticity::Initialize(Vectors &vecs) const {
  THROW("Initialize is not implemented for DG")
}


std::pair<double, double> DGElasticity::detF(const Vector &u) const {
  double Jmax = 1.0;
  double Jmin = 1.0;

  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    DGVectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      Tensor DU = E.VectorGradient(q, u);
      double J = det(One + DU);
      Jmin = std::min(Jmin, J);
      Jmax = std::max(Jmax, J);
    }
  }
  Jmin = PPM->Min(Jmin);
  Jmax = PPM->Max(Jmax);

  return {Jmin, Jmax};
}
