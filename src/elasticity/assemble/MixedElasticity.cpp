#include "TaylorHoodElement.hpp"
#include "TaylorHoodFaceElement.hpp"
#include "MixedElasticity.hpp"


MixedElasticity::MixedElasticity(const ElasticityProblem &eP, const PressureProblem &pP)
    : FiniteElasticity(eP, pP) {}

void MixedElasticity::Energy(const cell &c, const Vector &W, double &energy) const {
  TaylorHoodElement E(W, c);

  double d_part = DomainPart(c);
  Tensor FA_inv = One;
  double gamma = GetStretch(c);

  DataContainer cellData = c.GetData();
  Tensor Q = RotationMatrix(cellData);
  Tensor QT = transpose(Q);
  Tensor QI = Invert(Q);

  if (d_part < 2) {
    FA_inv = Invert(mat::active_deformation(QT, gamma));
  }

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    double p = E.PressureValue(q, W);
    VectorField u = E.VelocityField(q, W);
    Tensor Du = E.VelocityFieldGradient(q, W);
    Tensor F = FA_inv + Q * Du * FA_inv * QI;

    energy += w * mat::isochoric_energy(eProblem.GetMaterial(d_part < 2), <#initializer#>,
                                        Q * F * QI);
    energy += w * p * mat::volumetric_energy(eProblem.GetMaterial(d_part < 2), det(F));
  }
}

void MixedElasticity::NeumannBoundary(const cell &c, int face, int bc, const Vector &U,
                                      Vector &r) const {
  TaylorHoodFaceElement E(U, c, face);
  RowValues r_c(r, E);
  double localPressure{0.0};

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    Tensor deformationGradient = DeformationGradient(E.VelocityFieldGradient(q, U));
    Tensor cofactorDisp = det(deformationGradient) * transposeInvert(deformationGradient);

    localPressure = -pProblem.Pressure(currentTime, E.QPoint(q), bc % 230);
    //mout << OUT(p) << endl;
    VectorField FN = cofactorDisp * E.QNormal(q);
    for (int i = 0; i < E.VelocitySizeOnFace(); ++i) {
      for (int k = 0; k < c.dim(); ++k) {
        VectorField u_ik = E.VelocityField(q, i, k);
        r_c(i, k) += w * localPressure * FN * u_ik;
      }
    }
  }
}

void MixedElasticity::Jacobi(const cell &c, const Vector &W, Matrix &A) const {
  TaylorHoodElement E(W, c);
  MixedRowEntries A_c(A, c);

  double d_part = DomainPart(c);
  Tensor FA_inv = One;
  double gamma = GetStretch(c);

  DataContainer cellData = c.GetData();
  Tensor Q = RotationMatrix(cellData);
  Tensor QT = transpose(Q);
  Tensor QI = Invert(Q);

  if (d_part < 2) {
    FA_inv = Invert(mat::active_deformation(QT, gamma));
  }

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    double p = E.PressureValue(q, W);
    VectorField u = E.VelocityField(q, W);
    Tensor Du = E.VelocityFieldGradient(q, W);
    Tensor F = FA_inv + Q * Du * FA_inv * QI;

    for (int i = 0; i < E.VelocitySize(); ++i) {
      for (int k = 0; k < E.Dim(); ++k) {

        VectorField u_ik = E.VelocityField(q, i, k);
        Tensor F_ik = E.VelocityFieldGradient(q, i, k);

        for (int j = 0; j < E.VelocitySize(); ++j) {
          for (int l = 0; l < E.Dim(); ++l) {

            VectorField u_jl = E.VelocityField(q, j, l);
            Tensor F_jl = E.VelocityFieldGradient(q, j, l);

            // Mass

            // Deviatoric Piola Kirchhoff Stress
            A_c(E.VelocityIndex(), E.VelocityIndex(), i, j, k, l) -=
                w * mat::isochoric_second_derivative(eProblem.GetMaterial(d_part < 2),
                                                     Q, F,
                                                     Q * F_ik * FA_inv * QI,
                                                     Q * F_jl * FA_inv * QI);
            // Volumetric Piola Kirchhoff Stress
            A_c(E.VelocityIndex(), E.VelocityIndex(), i, j, k, l) -=
                w * p * mat::volumetric_second_derivative(eProblem.GetMaterial(d_part < 2), det(F),
                                                          Q * F_ik * FA_inv * QI,
                                                          Q * F_jl * FA_inv * QI);
          }
        }

        for (int j = 0; j < E.PressureSize(); ++j) {
          double p_j = E.PressureValue(q, j);

          // Volumetric Piola Kirchhoff Stress
          A_c(E.VelocityIndex(), E.PressureIndex(), i, j, k, 0) +=
              w * p_j * mat::volumetric_derivative(eProblem.GetMaterial(d_part < 2), det(F),
                                                   Q * F_ik * FA_inv * QI);
          A_c(E.PressureIndex(), E.VelocityIndex(), j, i, 0, k) +=
              w * p_j * mat::volumetric_derivative(eProblem.GetMaterial(d_part < 2), det(F),
                                                   Q * F_ik * FA_inv * QI);
        }
      }
    }

    for (int i = 0; i < E.PressureSize(); ++i) {
      double p_i = E.PressureValue(q, i);
      for (int j = 0; j < E.PressureSize(); ++j) {
        double p_j = E.PressureValue(q, j);
        A_c(E.PressureIndex(), E.PressureIndex(), i, j, 0, 0) +=
            w * p_i * p_j / eProblem.GetMaterial(d_part < 2).Volumetric().Penalty();
      }
    }
  }
}

void MixedElasticity::Residual(const cell &c, const Vector &W, Vector &r) const {
  TaylorHoodElement E(W, c);
  MixedRowBndValues r_c(r, c);

  double d_part = DomainPart(c);
  Tensor FA_inv = One;
  double gamma = GetStretch(c);

  DataContainer cellData = c.GetData();
  Tensor Q = RotationMatrix(cellData);
  Tensor QT = transpose(Q);
  Tensor QI = Invert(Q);

  if (d_part < 2) {
    FA_inv = Invert(mat::active_deformation(QT, gamma));
  }

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    double p = E.PressureValue(q, W);
    VectorField u = E.VelocityField(q, W);
    Tensor Du = E.VelocityFieldGradient(q, W);
    Tensor F = FA_inv + Q * Du * FA_inv * QI;

    for (int i = 0; i < E.VelocitySize(); ++i) {
      for (int k = 0; k < E.Dim(); ++k) {

        VectorField u_ik = E.VelocityField(q, i, k);
        Tensor F_ik = E.VelocityFieldGradient(q, i, k);

        // Mass

        // Deviatoric Piola Kirchhoff Stress
        r_c(E.VelocityIndex(), i, k) -= w *
                                        mat::isochoric_derivative(eProblem.GetMaterial(d_part < 2),
                                                                  <#initializer#>,
                                                                  F, Q * F_ik * FA_inv * QI);
        // Volumetric Piola Kirchhoff Stress
        r_c(E.VelocityIndex(), i, k) -= w * p *
                                        mat::volumetric_derivative(eProblem.GetMaterial(d_part < 2),
                                                                   det(F), Q * F_ik * FA_inv * QI);
        // Boundary
        if (r_c.onBnd()) {
          for (int face = 0; face < c.Faces(); ++face) {
            int bc = r_c.bc(face);
            if ((bc > 229) && (bc < 234)) {
              NeumannBoundary(c, face, bc, W, r);
            }
          }
        }
      }
    }

    for (int j = 0; j < E.PressureSize(); ++j) {
      double p_j = E.PressureValue(q, j);
      // Volumetric Energy
      r_c(E.PressureIndex(), j, 0) +=
          w * p_j * mat::volumetric_energy(eProblem.GetMaterial(d_part < 2), det(F));
      // Penalty Term
      r_c(E.PressureIndex(), j, 0) +=
          w * p * p_j / (eProblem.GetMaterial(d_part < 2).Volumetric().Penalty());
    }
  }
}


void MixedElasticity::GetStress(const Vector &W, Vector &stress) const {
  stress = 0;

  for (cell c = W.cells(); c != W.cells_end(); c++) {
    TaylorHoodElement E(W, c);
    MixedRowBndValues r_c(stress, c);

    double d_part = DomainPart(c);
    Tensor FA_inv = One;
    double gamma = GetStretch(c);

    DataContainer cellData = c.GetData();
    Tensor Q = RotationMatrix(cellData);
    Tensor QT = transpose(Q);
    Tensor QI = Invert(Q);

    if (d_part < 2) {
      FA_inv = Invert(mat::active_deformation(QT, gamma));
    }

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      VectorField u = E.VelocityField(q, W);
/*
        VectorField uo = E.VectorValue(q, u_old);
        VectorField v = E.VectorValue(q, velocity);
        VectorField a = E.VectorValue(q, acceleration);

        VectorField u_tt = (2 * n_beta - 1) / 2.0 / n_beta * a;
        u_tt -= 1 / (timeStep * n_beta) * v;
        u_tt += 1 / (timeStep * timeStep * n_beta) * (u - uo);
*/
      Tensor Du = E.VelocityFieldGradient(q, W);
      Tensor F = FA_inv + Q * DeformationGradient(Du) * FA_inv * QI;

      //double W = mProblem.GetMaterial(d_part < 2)->W(F, Q, FA_inv);
      double W = w * mat::isochoric_energy(eProblem.GetMaterial(d_part < 2), <#initializer#>, F);

      for (int i = 0; i < E.VelocitySize(); ++i) {
        // Acceleration
        // r_c(i, k) += w * rho * u_tt[k];
        // Internal Energy
        r_c(i, 0) += W;
      }
    }
  }
}

double MixedElasticity::GetStretch(const cell &c) const {
  return 1.0;
}
