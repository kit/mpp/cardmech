#include "EGElasticity.hpp"

EGElasticity::EGElasticity(ElasticityProblem &eP, int degree, bool isStatic)
    : IElasticity(eP), disc(std::make_shared<const MixedEGDiscretization>(eP.GetMeshes(), degree, dim())){
  Config::Get("DGSign", sign);

  penalty = eP.GetDGPenalty();
  if (penalty == 0)   Config::Get("DGPenalty", penalty);

  this->degree = degree;
}

double EGElasticity::Energy(const Vector &u) const {
  double energy = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    Energy(c, u, energy);
  }
  return PPM->Sum(energy);
}


void EGElasticity::Energy(const cell &c, const Vector &U, double &energy) const {
  MixedEGVectorFieldElement E(U, *c);
  const auto &cellMat = eProblem.GetMaterial(c);

  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    Tensor F = DeformationGradient(E, q, U);
    Tensor FT = transpose(F);
    Tensor E = 0.5 * (FT * F - One);

//    VectorField v = timeScheme->Velocity<MixedEGVectorFieldElement>(U, E, q); // noch überlegen
//    energy += w * rho * (v * v);
    // energy += w * cellMat.TotalEnergy(F, Q);

    // energy = std::min(energy,det(F));

    energy = std::max(energy, cellMat.q(E, Q[0]));
  }

  return;

  
  BFParts bnd(U.GetMesh(), *c);
  if (!bnd.onBnd()) return;

  if (bnd.onBnd()) {
    for (int face = 0; face < c.Faces(); ++face) {
      int bc = bnd[face];
      if (bc == 2) {
        energy -= TractionEnergy(c, face, bc, U);
      }

      if ((bc > 229) && (bc < 234)) {
        energy += PressureEnergy(c, face, bc, U);
      }
    }
  }
}


double EGElasticity::TractionEnergy(const cell &c, int face, int bc, const Vector &U) const {
  double W_Neumann = 0.0;
  MixedEGVectorFieldFaceElement E(U, *c, face);

  Tensor Q = RotationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    VectorField n = E.QNormal(q);
    VectorField TN = eProblem.TractionStiffness(Time(), E.QPoint(q), c.Subdomain()) *
                     (Product(n, n) * E.VectorValue(q, U));
    W_Neumann += w * TN * TN;
  }
  return W_Neumann;
}


double EGElasticity::PressureEnergy(const cell &c, int face, int bc, const Vector &U) const {
  double W_Pressure = 0.0;
  MixedEGVectorFieldFaceElement E(U, *c, face);

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    VectorField N = E.QNormal(q);

    double localPressure = -eProblem.Pressure(Time(), E.QPoint(q), bc);
    W_Pressure += w * localPressure * (N * E.VectorValue(q, U));
  }

  return W_Pressure;
}


void EGElasticity::Residual(const cell &c, const Vector &U, Vector &r) const {
  MixedEGVectorFieldElement E(U, *c);
  MixedRowValues r_c(r, *c);
  Tensor Q = OrientationMatrix(c.GetData());
  const Material &cellMat = eProblem.GetMaterial(c);

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    VectorField u = E.VectorValue(q, U);
    Tensor F = DeformationGradient(E, q, U);
    auto Fiso = eProblem.IsochoricPart(F);
    Tensor inverseFA = mat::active_deformation(Q, E.VectorValue(q, *gamma));
    F = F * inverseFA;
    VectorField f = eProblem.Load(Time(), E.QPoint(q), *c);
//    double T = eProblem.ActiveStress(Time(), E.QPoint(q));

    for (int i = 0; i < E.ValueSize(); ++i) {
      for (int k = 0; k < E.Dim(); ++k) {
        auto u_ik = E.VectorComponentValue(q, i, k);
        auto F_ik = E.VectorRowGradient(q, i, k);
        r_c(E.ValueIndex(), i, k) +=
            w * (cellMat.TotalDerivative(Fiso, F, Q, F_ik) - f * u_ik);
      }
    }
    auto theta = E.PenaltyVectorValue(q);
    auto Dtheta = E.PenaltyVectorGradient(q);

    r_c(E.PenaltyIndex(), 0, 0) +=
        w * (cellMat.TotalDerivative(Fiso, F, Q, Dtheta) - f * theta);
  }
  for (int f = 0; f < c.Faces(); ++f) {
    double s = cellMat.Isochoric().GetMaxParameter() *
               (pow(degree, 2) * penalty) / U.GetMesh().MaxMeshWidth();
    MixedEGVectorFieldFaceElement FE(U, *c, f);
    if (E.Bnd(f) >= PRESSURE_BC_LV && E.Bnd(f) <= PRESSURE_BC_RA) {
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point z = FE.QPoint(q);
        Tensor F = DeformationGradient(FE, q, U);
        VectorField N = mat::cofactor(F) * FE.QNormal(q);
        if (contains(cellMat.Name(),"Linear") || contains(cellMat.Name(),"Laplace")){
          N = FE.QNormal(q);
        }
        double localPressure = -eProblem.Pressure(Time(), z, E.Bnd(f));
        for (int i = 0; i < FE.ValueSize(); ++i) {
          for (int k = 0; k < FE.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            r_c(FE.ValueIndex(), i, k) -= w * localPressure * N * u_ik;
          }
        }
        auto theta = FE.PenaltyVectorValue(q);
        r_c(FE.PenaltyIndex(), 0, 0) -= w * localPressure * N * theta;
      }
    } else if (E.Bnd(f) == 1) {
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        VectorField N = FE.QNormal(q);
        VectorField u_diff = FE.VectorValue(q, U)- eProblem.Deformation(Time(), FE.QPoint(q));

        Tensor F = DeformationGradient(FE, q, U);
        auto Fiso = eProblem.IsochoricPart(F);
        Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
        F = F * inverseFA;

        for (int i = 0; i < FE.ValueSize(); ++i) {
          for (int k = 0; k < FE.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto Du_ik = FE.VectorRowGradient(q, i, k);
            double SN = cellMat.TotalDerivative(Fiso, F, Q, Product(u_ik, N));
            double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                         Product(u_diff, N));
            r_c(FE.ValueIndex(), i, k) -= w * (SN_ik * sign + SN - s * u_diff * u_ik);
          }
        }
       auto theta = FE.PenaltyVectorValue(q);
       auto Dtheta = FE.PenaltyVectorGradient(q);
       double SN_theta = cellMat.TotalDerivative(Fiso, F, Q, Product(theta, N));
       double SN_ik_Dtheta = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta,
                                                            Product(u_diff, N));

       r_c(FE.PenaltyIndex(), 0, 0) -= w * (SN_ik_Dtheta * sign + SN_theta - s * u_diff * theta);

      }
      } else if( E.Bnd(f) == 199) {
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        VectorField N = FE.QNormal(q);
        VectorField u_diff = FE.VectorValue(q, U);

        Tensor F = DeformationGradient(FE, q, U);
        auto Fiso = eProblem.IsochoricPart(F);
        Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
        F = F * inverseFA;

        for (int i = 0; i < FE.ValueSize(); ++i) {
          for (int k = 0; k < FE.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto Du_ik = FE.VectorRowGradient(q, i, k);
            double SN = cellMat.TotalDerivative(Fiso, F, Q, Product(u_ik, N));
            double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                         Product(u_diff, N));
            r_c(FE.ValueIndex(), i, k) -= w * ( SN_ik * sign  + SN - s * u_diff * u_ik);
          }
        }
        auto theta = FE.PenaltyVectorValue(q);
        auto Dtheta = FE.PenaltyVectorGradient(q);
        double SN_theta = cellMat.TotalDerivative(Fiso, F, Q, Product(theta, N));
        double SN_Dtheta = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta,
                                                            Product(u_diff, N));

        r_c(FE.PenaltyIndex(), 0, 0) -= w *(SN_Dtheta * sign + SN_theta - s *  u_diff * theta);

      }
    } else if (E.Bnd(f) == -1) {
      cell cf = r.find_neighbour_cell(c, f);
      if (cf() < c()) continue;
      int f1 = r.find_neighbour_face_id(c.Face(f), cf);
      MixedEGVectorFieldElement E_nghbr(U, *cf);
      MixedEGVectorFieldFaceElement FE_nghbr(r, *cf, f1);
      MixedRowValues r_cf(r, *cf);

      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point z = FE.QPoint(q);
        VectorField N = FE.QNormal(q);
        VectorField u = FE.VectorValue(q, U);
        Tensor F = DeformationGradient(FE, q, U);
        auto Fiso = eProblem.IsochoricPart(F);
        Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
        F = F * inverseFA;
        int q1 = FE.FindQPointID(FE_nghbr, z);
        VectorField u_nghbr = FE_nghbr.VectorValue(q1, U);
        Tensor F_nghbr = DeformationGradient(FE_nghbr, q1, U);
        auto Fiso_nghbr = eProblem.IsochoricPart(F_nghbr);
        Tensor inverseFA_nghbr = mat::active_deformation(Q, FE_nghbr.VectorValue(q1, *gamma));
        F_nghbr = F_nghbr * inverseFA_nghbr;

        for (int i = 0; i < FE.ValueSize(); ++i) {
          for (int k = 0; k < FE.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto Du_ik = FE.VectorRowGradient(q, i, k);
            double SN = cellMat.TotalDerivative(Fiso, F, Q, Product(u_ik, N));
            double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik, Product(u, N));
            double SN_nghbr = cellMat.TotalDerivative(Fiso_nghbr, F_nghbr, Q, Product(u_ik, N));
            double SN_ik_nghbr = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                               Product(u_nghbr, N));
            r_c(FE.ValueIndex(), i, k) += w * (s * u * u_ik - 0.5 * SN - 0.5 * SN_ik * sign);
            r_c(FE.ValueIndex(), i, k) += w * (-s * u_nghbr * u_ik - 0.5 * SN_nghbr + 0.5 * SN_ik_nghbr * sign);
          }
        }
        for (int j = 0; j < FE_nghbr.ValueSize(); ++j) {
          for (int l = 0; l < FE_nghbr.Dim(); ++l) {
            auto u_jl = FE_nghbr.VectorComponentValue(q1, j, l);
            auto Du_jl = FE_nghbr.VectorRowGradient(q1, j, l);
            double SN = cellMat.TotalDerivative(Fiso, F, Q, Product(u_jl, N));
            double SN_jl = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_jl,
                                                         Product(u, N));
            double SN_nghbr = cellMat.TotalDerivative(Fiso_nghbr, F_nghbr, Q, Product(u_jl, N));
            double SN_jl_nghbr = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_jl,
                                                               Product(u_nghbr, N));
            r_cf(FE_nghbr.ValueIndex(), j, l) += w * (s * u_nghbr * u_jl + 0.5 * SN_nghbr + 0.5 * SN_jl_nghbr * sign);
            r_cf(FE_nghbr.ValueIndex(), j, l) += w * (-s * u * u_jl + 0.5 * SN - 0.5 * SN_jl * sign);
          }
        }
        auto theta = FE.PenaltyVectorValue(q);
        auto Dtheta = FE.PenaltyVectorGradient(q);
        double SN_theta = cellMat.TotalDerivative(Fiso, F, Q, Product(theta, N));
        double SN_ik_Dtheta = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta, Product(u, N));
        double SN_nghbr_theta = cellMat.TotalDerivative(Fiso_nghbr, F_nghbr, Q, Product(theta, N));
        double SN_ik_nghbr_Dtheta = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta,
                                                           Product(u_nghbr, N));
        r_c(FE.PenaltyIndex(), 0, 0) += w * (s * u * theta - 0.5 * SN_theta - 0.5 * SN_ik_Dtheta * sign);
        r_c(FE.PenaltyIndex(), 0, 0) += w * (-s * u_nghbr * theta - 0.5 * SN_nghbr_theta + 0.5 * SN_ik_nghbr_Dtheta * sign);

        auto theta_nghbr = FE_nghbr.PenaltyVectorValue(q1);
        auto Dtheta_nghbr = FE_nghbr.PenaltyVectorGradient(q1);
        double SN_theta_nghbr = cellMat.TotalDerivative(Fiso, F, Q, Product(theta_nghbr, N));
        double SN_jl_Dtheta_nghbr = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Dtheta_nghbr,
                                                     Product(u, N));
        double SN_nghbr_theta_nghbr = cellMat.TotalDerivative(Fiso_nghbr, F_nghbr, Q, Product(theta_nghbr, N));
        double SN_jl_nghbr_Dtheta_nghbr = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Dtheta_nghbr,
                                                           Product(u_nghbr, N));
        r_cf(FE_nghbr.PenaltyIndex(), 0, 0) += w * (s * u_nghbr * theta_nghbr + 0.5 * SN_nghbr_theta_nghbr + 0.5 * SN_jl_nghbr_Dtheta_nghbr * sign);//?
        r_cf(FE_nghbr.PenaltyIndex(), 0, 0) += w * (-s * u * theta_nghbr + 0.5 * SN_theta_nghbr - 0.5 * SN_jl_Dtheta_nghbr * sign);
      }
    }
  }
}

void EGElasticity::PressureBoundary(const cell &c, int face, int bc, const Vector &U,
                                    Vector &r) const {
  MixedEGVectorFieldElement E(U, *c);
  MixedEGVectorFieldFaceElement FE(U, *c, face);
  MixedRowValues r_c(r, *c);

  for (int q = 0; q < FE.nQ(); ++q) {
    double w = FE.QWeight(q);
    VectorField N = FE.QNormal(q);
    double localPressure = -eProblem.Pressure(Time(), FE.QPoint(q), bc);

    for (int i = 0; i < FE.ValueSize(); ++i) {
      for (int k = 0; k < FE.Dim(); ++k) {
        auto u_ik = FE.VectorComponentValue(q, i, k);
        r_c(FE.ValueIndex(), i, k) -= w * (localPressure) * N * u_ik;
      }
    }
    auto theta = FE.PenaltyVectorValue(q);
    r_c(FE.PenaltyIndex(), 0, 0) -= w * (localPressure) * N * theta;
  }
}

void EGElasticity::TractionBoundary(const cell &c, int face, int bc, const Vector &U,
                                    Vector &r) const {
  MixedEGVectorFieldElement E(U, *c);
  MixedEGVectorFieldFaceElement FE(U, *c, face);
  MixedRowValues r_c(r, *c);

  for (int q = 0; q < FE.nQ(); ++q) {
    double w = FE.QWeight(q);
    Point xi = FE.QPoint(q);
    VectorField n = E.QNormal(q);
    VectorField normalStiffness = eProblem.TractionStiffness(Time(), xi, c.Subdomain()) *
                                  (Product(n, n) * E.VectorValue(q, U));

    for (int i = 0; i < FE.ValueSize(); ++i) {
      for (int k = 0; k < FE.Dim(); ++k) {
        auto u_ik = FE.VectorComponentValue(q, i, k);
        r_c(FE.ValueIndex(), i, k) += w * normalStiffness * u_ik;
      }
    }
    auto theta = FE.PenaltyVectorValue(q);
    r_c(FE.PenaltyIndex(), 0, 0) += w * normalStiffness * theta;
  }
}


void EGElasticity::Jacobi(const cell &c, const Vector &U, Matrix &A) const {
  MixedEGVectorFieldElement E(U, *c); // A oder u?
  MixedRowEntries A_c(A, *c);

  const Material &cellMat = eProblem.GetMaterial(c);
  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    Tensor F = DeformationGradient(E, q, U);
    auto Fiso = eProblem.IsochoricPart(F);
    Tensor inverseFA = mat::active_deformation(Q, E.VectorValue(q, *gamma));
    F = F * inverseFA;
    double T = eProblem.ActiveStress(Time(), E.QPoint(q));

    auto theta = E.PenaltyVectorValue(q);
    auto Dtheta = E.PenaltyVectorGradient(q) * transpose(inverseFA);
    for (int i = 0; i < E.ValueSize(); ++i) {
      for (int k = 0; k < E.Dim(); ++k) {
        auto u_ik = E.VectorComponentValue(q, i, k);
        auto F_ik = E.VectorRowGradient(q, i, k) * transpose(inverseFA);
        for (int j = 0; j < E.ValueSize(); ++j) {
          for (int l = 0; l < E.Dim(); ++l) {
            auto u_jl = E.VectorComponentValue(q, j, l);
            auto F_jl = E.VectorRowGradient(q, j, l) * transpose(inverseFA);
            A_c(E.ValueIndex(), E.ValueIndex(), i, j, k, l) += //j, i, l, k ?
                w * cellMat.TotalSecondDerivative(Fiso, F, Q, F_ik, F_jl);
          }
        }

        A_c(E.ValueIndex(), E.PenaltyIndex(), i, 0, k, 0) += //j, i, l, k ?
            w * (cellMat.TotalSecondDerivative(Fiso, F, Q, F_ik, Dtheta));

        A_c(E.PenaltyIndex(), E.ValueIndex(), 0, i, 0, k) += //j, i, l, k ?
            w * (cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta, F_ik));
      }
    }
    A_c(E.PenaltyIndex(), E.PenaltyIndex(), 0, 0, 0, 0) += //j, i, l, k ?
        w * (cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta, Dtheta));
  }
  for (int face = 0; face < c.Faces(); ++face) {
    double s = cellMat.Isochoric().GetMaxParameter() *
               (pow(degree, 2) * penalty) / U.GetMesh().MaxMeshWidth();
    MixedEGVectorFieldFaceElement FE(U, *c, face);
    if (E.Bnd(face) == DIRICHLET_BC || E.Bnd(face) == DIRICHLET_FIX_BC) {
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        VectorField N = FE.QNormal(q);
        VectorField u = FE.VectorValue(q, U);
        Tensor F = DeformationGradient(FE, q, U);
        auto Fiso = eProblem.IsochoricPart(F);
        Tensor inverseFA = mat::inverse_active_deformation(Q, FE.VectorValue(q, *gamma));
        F = F * inverseFA;

        auto theta = FE.PenaltyVectorValue(q);
        auto Dtheta = FE.PenaltyVectorGradient(q);
        for (int i = 0; i < FE.ValueSize(); ++i) {
          for (int k = 0; k < FE.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto Du_ik = FE.VectorRowGradient(q, i, k);
            for (int j = 0; j < FE.ValueSize(); ++j) {
              for (int l = 0; l < FE.Dim(); ++l) {
                auto u_jl = FE.VectorComponentValue(q, j, l);
                auto Du_jl = FE.VectorRowGradient(q, j, l);
                double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                             Product(u_jl, N));
                double SN_jl = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_jl,
                                                             Product(u_ik, N));
                A_c(FE.ValueIndex(), FE.ValueIndex(), i, j, k, l) -= w * (SN_ik * sign + SN_jl - s * (u_ik * u_jl));
              }
            }

            double SN_theta = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                                Product(theta, N));

            double SN_Dtheta = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta,
                                                                Product(u_ik, N));
            A_c(FE.ValueIndex(), FE.PenaltyIndex(), i, 0, k, 0) -= w * (SN_theta * sign + SN_Dtheta - s * (u_ik * theta));

            A_c(FE.PenaltyIndex(), FE.ValueIndex(), 0, i, 0, k) -= w * (SN_Dtheta * sign + SN_theta - s * (theta * u_ik));

          }
        }
        double SN_theta = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta,
                                                        Product(theta, N));

        double SN_Dtheta = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta,
                                                         Product(theta, N));

        A_c(FE.PenaltyIndex(), FE.PenaltyIndex(), 0, 0, 0, 0) -=
            w * (SN_theta * sign + SN_Dtheta - s * (theta * theta));
      }
    } else if (E.Bnd(face) >= PRESSURE_BC_LV && E.Bnd(face) <= PRESSURE_BC_RA) {
      if (contains(cellMat.Name(),"Linear") || contains(cellMat.Name(),"Laplace")) continue;
      for (int q = 0; q < FE.nQ(); ++q) {
        auto theta = FE.PenaltyVectorValue(q);
        auto Dtheta = FE.PenaltyVectorGradient(q);
        double w = FE.QWeight(q);
        VectorField N = FE.QNormal(q);
        Tensor F = DeformationGradient(FE, q, U);
        double localPressure = -eProblem.Pressure(Time(), FE.QPoint(q), E.Bnd(face));

        for (int i = 0; i < FE.ValueSize(); ++i) {
          for (int k = 0; k < FE.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto F_ik = FE.VectorRowGradient(q, i, k);
            for (int j = 0; j < FE.ValueSize(); ++j) {
              for (int l = 0; l < FE.Dim(); ++l) {
                auto F_jl = FE.VectorRowGradient(q, j, l);
                A_c(FE.ValueIndex(), FE.ValueIndex(), i, j, k, l) -=
                    w * (localPressure) * (Cross(F, F_jl) * N) * u_ik;

              }
            }

            A_c(FE.ValueIndex(), FE.PenaltyIndex(), i, 0, k, 0) -=
                w * (localPressure) * (Cross(F, Dtheta) * N) * u_ik;

            A_c(FE.PenaltyIndex(), FE.ValueIndex(), 0, i, 0, k) -=
                w * (localPressure) * (Cross(F, F_ik) * N) * theta;
          }
        }
        A_c(FE.PenaltyIndex(), FE.PenaltyIndex(), 0, 0, 0, 0) -=
            w * (localPressure) * (Cross(F, Dtheta) * N) * theta;
      }
    } else if (E.Bnd(face) == -1) {
      cell cf = U.find_neighbour_cell(c, face);
      if (cf() < c()) continue;
      int f1 = U.find_neighbour_face_id(c.Face(face), cf);
      MixedEGVectorFieldElement E_nghbr(U, *cf);
      MixedEGVectorFieldFaceElement FE_nghbr(U, *cf, f1);
      MixedRowEntries A_cf(A, *c, *cf);
      MixedRowEntries A_fc(A, *cf, *c);
      MixedRowEntries A_ff(A, *cf);

      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        VectorField N = FE.QNormal(q);
        Tensor F = DeformationGradient(FE, q, U);
        auto Fiso = eProblem.IsochoricPart(F);
        Tensor inverseFA = mat::active_deformation(Q, FE.VectorValue(q, *gamma));
        F = F * inverseFA;
        Point z = FE.QPoint(q);
        int q_n = FE_nghbr.FindQPointID(FE, z);
        Tensor F_nghbr = DeformationGradient(FE_nghbr, q_n, U);
        auto Fiso_nghbr = eProblem.IsochoricPart(F_nghbr);
        Tensor inverseFA_nghbr = mat::active_deformation(Q, FE_nghbr.VectorValue(q_n, *gamma));
        F_nghbr = F_nghbr * inverseFA_nghbr;

        auto theta = FE.PenaltyVectorValue(q);
        auto Dtheta = FE.PenaltyVectorGradient(q);

        auto theta_nghbr = FE_nghbr.PenaltyVectorValue(q_n);
        auto Dtheta_nghbr = FE_nghbr.PenaltyVectorGradient(q_n);

        for (int i = 0; i < FE.ValueSize(); ++i) {
          for (int k = 0; k < FE.Dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            auto Du_ik = FE.VectorRowGradient(q, i, k);
            for (int j = 0; j < FE.ValueSize(); ++j) {
              for (int l = 0; l < FE.Dim(); ++l) {
                auto u_jl = FE.VectorComponentValue(q, j, l);
                auto Du_jl = FE.VectorRowGradient(q, j, l);
                double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                             Product(u_jl, N));
                double SN_jl = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_jl,
                                                             Product(u_ik, N));
                A_c(FE.ValueIndex(), FE.ValueIndex(), i, j, k, l) += w * (s * (u_ik * u_jl) - 0.5 * SN_ik * sign - 0.5 * SN_jl);
              }
            }

            double SN_theta = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                            Product(theta, N));
            double SN_Dtheta = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta,
                                                             Product(u_ik, N));

            A_c(FE.ValueIndex(), FE.PenaltyIndex(), i, 0, k, 0) += w * (s * (u_ik * theta) - 0.5 * SN_theta * sign - 0.5 * SN_Dtheta);
            A_c(FE.PenaltyIndex(), FE.ValueIndex(), 0, i, 0, k) += w * (s * (theta * u_ik) - 0.5 * SN_Dtheta * sign - 0.5 * SN_theta);

            for (int j = 0; j < FE_nghbr.ValueSize(); ++j) {
              for (int l = 0; l < FE_nghbr.Dim(); ++l) {
                auto u_jl = FE_nghbr.VectorComponentValue(q_n, j, l);
                auto Du_jl = FE_nghbr.VectorRowGradient(q_n, j, l);
                double SN_jl = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_jl,
                                                             Product(u_ik, N));
                double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                             Product(u_jl, N));

                A_cf(FE.ValueIndex(), FE_nghbr.ValueIndex(), i, j, k, l) += w * (-s * u_ik * u_jl + 0.5 * SN_ik * sign - 0.5 * SN_jl);
                A_fc(FE_nghbr.ValueIndex(), FE.ValueIndex(), j, i, l, k) += w * (-s * u_ik * u_jl + 0.5 * SN_ik - 0.5 * SN_jl * sign);
              }
            }
            double SN_theta_nghbr = cellMat.TotalSecondDerivative(Fiso, F, Q, Du_ik,
                                                                  Product(theta_nghbr, N));
            double SN_Dtheta_nghbr = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Dtheta_nghbr,
                                                         Product(u_ik, N));


            A_cf(FE.ValueIndex(), FE_nghbr.PenaltyIndex(), i, 0, k, 0) += w * (-s * u_ik * theta_nghbr + 0.5 * SN_theta_nghbr * sign - 0.5 * SN_Dtheta_nghbr);
            A_fc(FE_nghbr.PenaltyIndex(), FE.ValueIndex(), 0, i, 0, k) += w * (-s * u_ik * theta_nghbr + 0.5 * SN_theta_nghbr - 0.5 * SN_Dtheta_nghbr * sign);
          }
        }
        for (int i = 0; i < FE_nghbr.ValueSize(); ++i) {
          for (int k = 0; k < FE_nghbr.Dim(); ++k) {
            auto u_ik = FE_nghbr.VectorComponentValue(q_n, i, k);
            auto Du_ik = FE_nghbr.VectorRowGradient(q_n, i, k);

            double SN_theta = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_ik,
                                                         Product(theta, N));
            double SN_Dtheta = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta,
                                                         Product(u_ik, N));

            A_cf(FE.PenaltyIndex(), FE_nghbr.ValueIndex(), 0, i, 0, k) += w * (-s * theta * u_ik + 0.5 * SN_Dtheta * sign - 0.5 * SN_theta);
            A_fc(FE_nghbr.ValueIndex(), FE.PenaltyIndex(), i, 0, k, 0) += w * (-s * u_ik * theta + 0.5 * SN_Dtheta - 0.5 * SN_theta * sign);
            for (int j = 0; j < FE_nghbr.ValueSize(); ++j)
              for (int l = 0; l < FE_nghbr.Dim(); ++l) {
                auto u_jl = FE_nghbr.VectorComponentValue(q_n, j, l);
                auto Du_jl = FE_nghbr.VectorRowGradient(q_n, j, l);
                double SN_ik = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_ik,
                                                             Product(u_jl, N));
                double SN_jl = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_jl,
                                                             Product(u_ik, N));
                A_ff(FE_nghbr.ValueIndex(), FE_nghbr.ValueIndex(), i, j, k, l) += w * (s * u_ik * u_jl + 0.5 * SN_ik * sign + 0.5 * SN_jl);
              }
            double SN_theta_nghbr = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Du_ik,
                                                         Product(theta_nghbr, N));
            double SN_Dtheta_nghbr = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Dtheta_nghbr,
                                                         Product(u_ik, N));
            A_ff(FE_nghbr.ValueIndex(), FE_nghbr.PenaltyIndex(), i, 0, k, 0)  += w * (s * u_ik * theta_nghbr + 0.5 * SN_theta_nghbr * sign + 0.5 * SN_Dtheta_nghbr);
            A_ff(FE_nghbr.PenaltyIndex(), FE_nghbr.ValueIndex(), 0, i, 0, k)  += w * (s * theta_nghbr * u_ik + 0.5 * SN_Dtheta_nghbr * sign  + 0.5 * SN_theta_nghbr);
          }
        }
        double SN_ik = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta,
                                                     Product(theta, N));

        double SN_ik_nghbr = cellMat.TotalSecondDerivative(Fiso, F, Q, Dtheta,
                                                     Product(theta_nghbr, N));
        double SN_jl_nghbr = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Dtheta_nghbr,
                                                     Product(theta, N));

        double SN_nghbr = cellMat.TotalSecondDerivative(Fiso_nghbr, F_nghbr, Q, Dtheta_nghbr,
                                                           Product(theta_nghbr, N));

        A_c(FE.PenaltyIndex(), FE.PenaltyIndex(), 0, 0, 0, 0) += w * (-0.5 * SN_ik * sign - 0.5 *  SN_ik + s * (theta * theta));

        A_cf(FE.PenaltyIndex(), FE_nghbr.PenaltyIndex(), 0, 0, 0, 0) += w * (-s * theta * theta_nghbr + 0.5 * SN_ik_nghbr * sign - 0.5 * SN_jl_nghbr);

        A_fc(FE_nghbr.PenaltyIndex(), FE.PenaltyIndex(), 0, 0, 0, 0) += w * (-s * theta * theta_nghbr + 0.5 * SN_ik_nghbr - 0.5 * SN_jl_nghbr * sign);

        A_ff(FE_nghbr.PenaltyIndex(), FE_nghbr.PenaltyIndex(), 0, 0, 0, 0) += w * (s * theta_nghbr * theta_nghbr + 0.5 * SN_nghbr * sign + 0.5 * SN_nghbr);
      }
    }
  }
}

double EGElasticity::L2Norm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u);
      err += E.QWeight(q) * (U * U);
    }
  }
  return sqrt(PPM->Sum(err));
}


double EGElasticity::L2AvgNorm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);

    double w = 0.0;
    VectorField U{};
    for (int q = 0; q < E.nQ(); ++q) {
      w += E.QWeight(q);
      U += E.VectorValue(q, u);
    }
    err += w * (U * U);

  }
  return sqrt(PPM->Sum(err));
}

double EGElasticity::L2Error(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u) - eProblem.Deformation(Time(), E.QPoint(q));
      err += E.QWeight(q) * (U * U);
    }
  }
  return sqrt(PPM->Sum(err));
}

double EGElasticity::H1Norm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u);
      Tensor DU = E.VectorGradient(q, u);
      err += E.QWeight(q) * ((U * U) + Frobenius(DU, DU));

    }
  }
  return sqrt(PPM->Sum(err));
}


double EGElasticity::H1Error(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u)
                      - eProblem.Deformation(Time(), E.QPoint(q));

      Tensor DU =
          E.VectorGradient(q, u) - eProblem.DeformationGradient(Time(), E.QPoint(q));
      err += E.QWeight(q) * ((U * U) + Frobenius(DU, DU));
    }
  }
  return sqrt(PPM->Sum(err));
}


double EGElasticity::StrainEnergyNorm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      Tensor DD = E.VectorGradient(q, u);
      err += w * abs(cellMat.TotalDerivative(One + DD, One + DD, Q, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}

double EGElasticity::StrainEnergyError(const Vector &u) const {
  double err = 0;

  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);
    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      const Point &z = E.QPoint(q);
      Tensor DD = E.VectorGradient(q, u) - eProblem.DeformationGradient(Time(), z);
      err += w * abs(cellMat.TotalDerivative(One + DD, One + DD, Q, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}

double EGElasticity::EnergyValue(const Vector &u) const {
  double energy = 0.0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    const auto &cellMat = eProblem.GetMaterial(c);
    Tensor Q = OrientationMatrix(c.GetData());
    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      Tensor F = DeformationGradient(E, q, u);
      energy += w * cellMat.TotalEnergy(F, Q);
    }
  }
  return abs(PPM->Sum(energy));
}

double EGElasticity::EnergyNorm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      Tensor DD = E.VectorGradient(q, u);
      err += w * abs(cellMat.TotalSecondDerivative(One, One, Q, DD, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}

double EGElasticity::EnergyError(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      const Point &z = E.QPoint(q);
      Tensor DD = E.VectorGradient(q, u)  - eProblem.DeformationGradient(Time(), z);
      err += w * abs(cellMat.TotalSecondDerivative(One, One, Q, DD, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}


void EGElasticity::prestress(const Vector &u, Vector &pStress) {
  Prestress = std::make_unique<Vector>(0.0, u);

  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    const auto &cellMat = eProblem.GetMaterial(c);

    MixedEGVectorFieldElement E(u, *c);
    MixedRowValues r_c(*Prestress, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);

      Tensor F = DeformationGradient(E, q, u);
      auto Fiso = eProblem.IsochoricPart(F);

      for (int i = 0; i < E.ValueSize(); ++i) {
        for (int k = 0; k < E.Dim(); ++k) {
          auto F_ik = Q * E.VectorRowGradient(q, i, k);

          // Passive material response
          r_c(E.ValueIndex(), i, k) += w * cellMat.TotalDerivative(Fiso, F, Q, F_ik);
        }
      }
      auto Dtheta = E.PenaltyVectorGradient(q);
      r_c(E.PenaltyIndex(), 0, 0) += w * cellMat.TotalDerivative(Fiso, F, Q, Dtheta);
    }
  }
  Prestress->ClearDirichletValues();
  Prestress->Collect();
}

double EGElasticity::PressureError(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    if (!E.Bnd()) continue;

    Tensor Q = OrientationMatrix(c.GetData());
    for (int f = 0; f < c.Faces(); ++f) {
      if (E.Bnd(f) < 230 || E.Bnd(f) > 233) continue;
      MixedEGVectorFieldFaceElement FE(u, *c, f);
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point N = FE.QNormal(q);

        Tensor F = DeformationGradient(FE, q, u);
        Tensor Fiso = eProblem.IsochoricPart(F);
        Tensor S = eProblem.GetMaterial(c).FirstPiolaKirchhoff(F, Fiso, Q);

        VectorField SN =
            S * N - eProblem.Pressure(Time(), E.QPoint(q), E.Bnd(f)) * N;
        err += w * SN * SN;
      }
    }
  }
  return sqrt(PPM->Sum(err));
}

double EGElasticity::NeumannError(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    if (!E.Bnd()) continue;

    for (int f = 0; f < c.Faces(); ++f) {
      if (E.Bnd(f) != 2) continue;
      MixedEGVectorFieldFaceElement FE(u, *c, f);
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point N = FE.QNormal(q);

        Tensor F = DeformationGradient(FE, q, u);
        Tensor Fiso = eProblem.IsochoricPart(F);
        Tensor S = eProblem.GetMaterial(c).FirstPiolaKirchhoff(F, Fiso, One);

        VectorField SN =
            S * N - eProblem.Pressure(Time(), E.QPoint(q), E.Bnd(f)) * N;
        err += w * SN * SN;
      }
    }
  }
  return sqrt(PPM->Sum(err));
}


double EGElasticity::NormPrestress() {
  return Prestress->Max();
}

double EGElasticity::MaxStress(const Vector &U) const {
  double cellS{0.0};
  double maxS{0.0};

  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    cellS = 0.0;
    MixedEGVectorFieldElement E(U, *c);

    Tensor Q = RotationMatrix(c.GetData());
    Tensor QT = transpose(Q);
    Tensor QI = Invert(Q);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);

      Tensor F = DeformationGradient(E, q, U);
      Tensor Fiso = eProblem.IsochoricPart(F);
      auto P = eProblem.GetMaterial(c).FirstPiolaKirchhoff(F, Fiso, One);
      cellS += w * Frobenius(P, P);
    }

    maxS = std::max(maxS, cellS);
  }

  return maxS;
}


/*std::array<double, 4> EGElasticity::CalculateVolume(const Vector &U) const {
  std::array<double, 4> volumes{0.0, 0.0, 0.0, 0.0};

  return volumes;
  Vector phi(U);
  for (row r = phi.rows(); r != phi.rows_end(); ++r) {
    auto pos = r();
    for (int i = 0; i < pos.SpaceDim(); ++i) {
      phi(r, i) += pos[i];
    }
  }


  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    RowBndValues r_c(phi, *c);

    for (int face = 0; face < c.Faces(); ++face) {
      if (!U.GetMesh().onBndEG(c, face)) continue;
      int bc = r_c.bc(face);
      if ((bc >= PRESSURE_BC_LV) && (bc <= PRESSURE_BC_RA)) {
        if (c.plane()) {
          auto rx = phi.find_row(c.FaceCorner(face, 0));
          auto ry = phi.find_row(c.FaceCorner(face, 1));

          double v = 0.5 * det(TensorT<double, 2>(phi(rx, 0), phi(ry, 0),
                                                  phi(rx, 1), phi(ry, 1)));
          volumes[bc % 230] += v;
        } else {
          auto ra = phi.find_row(c.FaceCorner(face, 0));
          auto rb = phi.find_row(c.FaceCorner(face, 1));
          auto rc = phi.find_row(c.FaceCorner(face, 2));

          double v = 1.0 / 6.0 * det(Tensor(
              phi(ra, 0), phi(rb, 0), phi(rc, 0),
              phi(ra, 1), phi(rb, 1), phi(rc, 1),
              phi(ra, 2), phi(rb, 2), phi(rc, 2)
          ));

          volumes[bc % 230] += v;
          if (c.ReferenceType() == HEXAHEDRON) {
            auto rd = phi.find_row(c.FaceCorner(face, 3));
            v = 1.0 / 6.0 * det(Tensor(
                phi(ra, 0), phi(rc, 0), phi(rd, 0),
                phi(ra, 1), phi(rc, 1), phi(rd, 1),
                phi(ra, 2), phi(rc, 2), phi(rd, 2)
            ));
            volumes[bc % 230] += v;
          }
        }

      }
    }
  }

  for (int i = 0; i < 4; ++i) {
    volumes[i] = abs(PPM->Sum(volumes[i]));
  }
  return volumes;
}
*/
void EGElasticity::GetInvariant(const Vector &displacement, Vector &iota4) const {
  iota4 = 0.0;
  for (cell c = displacement.cells(); c != displacement.cells_end(); ++c) {
    MixedEGVectorFieldElement E(displacement, *c);
    RowBndValues r_c(iota4, *c);

    const VectorField fibre = Direction(c.GetData());
    for (int j = 0; j < E.Dim(); ++j) {
      Tensor Du = E.VectorGradient(E[j](), displacement);
      iota4(E[j](), 0) += mat::invariant(4, Du, fibre);
      iota4(E[j](), 1) += 1.0;
    }
  }
  iota4.Collect();

  for (row r = iota4.rows(); r != iota4.rows_end(); ++r) {
    iota4(r, 0) /= iota4(r, 1);
  }
  iota4.Collect();
}

void EGElasticity::PlotBoundary(const Vector &U, const Vector &scal) const {
  if (vtkplot < 1) return;  
  Vector bnd(scal);
  bnd.Clear();
  Vector count(bnd);
  count.Clear();
  auto &plot = mpp::plot("Boundary");
  for (cell c = U.cells(); c != U.cells_end(); c++) {
    RowBndValues u_c(bnd, *c);
    RowBndValues u_count(count, *c);

    if (!u_c.onBnd()) continue;

    for (int i = 0; i < c.Faces(); ++i) {
      for (int j = 0; j < U.NumberOfNodalPointsOnFace(*c, i); ++j) {
        int id = U.IdOfNodalPointOnFace(*c, i, j);
        if (u_c.bc(i) > u_c(id, 0)) {
          u_c(id, 0) = (double) u_c.bc(i);
          u_count(id, 0) = 1;
        }
      }
    }
  }
  bnd.Accumulate();
  count.Accumulate();
  for (size_t i = 0; i < count.size(); i++) {
    if (count[i] > 0) {
      bnd[i] /= count[i];
    }
  }
  plot.AddData("Boundary", bnd, 0);

  bnd = 0;
  for (cell c = U.cells(); c != U.cells_end(); c++) {
    RowBndValues u_c(bnd, *c);
    if (!u_c.onBnd()) continue;

    for (int i = 0; i < c.Faces(); ++i) {
      for (int j = 0; j < U.NumberOfNodalPointsOnFace(*c, i); ++j) {
        int id = U.IdOfNodalPointOnFace(*c, i, j);
        u_c(id, 0) = std::max(u_c(id, 0), u_c.bc(i) == 199 ? 1.0 : 0.0);
      }
    }
  }
  plot.AddData("Fixation", bnd);

  plot.PlotFile("Boundary");
}

void EGElasticity::SetDisplacement(Vector &U) const {
  for (row r = U.rows(); r != U.rows_end(); ++r) {
    int n = r.NumberOfDofs();
    if (n == 1) continue;
    VectorField d = eProblem.Deformation(Time(), r());
    for (int k = 0; k < U.dim(); ++k) {
      U(r, k) = d[k];
    }
  }
}

double EGElasticity::InitialVolume(const Mesh &M) const {
  double volume = 0.0;
  for (cell c = M.cells(); c != M.cells_end(); ++c) {
    volume += Volume(c.ReferenceType(), c.AsVector());
  }
  return PPM->Sum(volume);
}

double EGElasticity::DeformedVolume(const Vector &U) const {
  double volume = 0.0;
  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    MixedEGVectorFieldElement E(U, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      Tensor F = DeformationGradient(E, q, U);
      double w = E.QWeight(q);
      volume += w * det(F);
    }
  }
  return PPM->Sum(volume);
}

void EGElasticity::Project(const Vector &fine, Vector &u) const {

    for (row r1 = u.rows(); r1 != u.rows_end(); ++r1) {
        row r2 = fine.find_row(r1());
        if (r2 == fine.rows_end()) continue;
        int n = r1.NumberOfDofs();
        if (n == 1) continue;
        for (int j = 0; j < n ; ++j){
            u(r1,j) = fine(r2,j);
        }
    }

    int dlevel = fine.SpaceLevel() - u.SpaceLevel();

    std::vector<Point> childrenVerteces{};
    std::vector<Point> childVerteces{};
    std::vector<Point> coarseNodalPoints{};
    for (cell c = u.cells(); c != u.cells_end(); ++c) {
        std::vector<std::vector<std::unique_ptr<Cell>>> recursiveCells(dlevel + 1);
        recursiveCells[0].emplace_back(CreateCell(c.Type(),
                                                  c.Subdomain(), c.AsVector(), false));
        for (int l = 1; l <= dlevel; ++l) {
            for (const auto &parent: recursiveCells[l - 1]) {
              const std::vector<Rule> &R = parent->Refine(childrenVerteces);
              for (int i = 0; i < R.size(); ++i) {
                R[i](childrenVerteces, childVerteces);
                recursiveCells[l].emplace_back(
                    CreateCell(R[i].type(), c.Subdomain(), childVerteces,
                               false));
                }
            }
        }

        auto r = u.find_row(c()); // Zellmittelpunkt der groben Zelle
        //mout << "r " << r << endl;
        auto childCount = recursiveCells[dlevel].size();
        for(const auto & child : recursiveCells[dlevel]) {
            //mout << "child " << *child << endl;
            auto rc = fine.find_row(child->Center()); //Zellmittelpunkt der feinen Zelle
            u(r, 0) += fine(rc, 0) / childCount;
        }
    }

    //u.MakeAdditive();
    //u.Accumulate();
}

void EGElasticity::Interpolate(const Vector &coarse, Vector &u) const {

}


void EGElasticity::UseLagrangeValuesAsStartVector(const Vector &u_conforming, Vector &u) const{
  for (row r = u.rows(); r != u.rows_end(); ++r){
    row r2 = u_conforming.find_row(r());
    if (r2 == u_conforming.rows_end()) continue;
    int n = r.NumberOfDofs();
    if (n == 1) continue;
    for (int j = 0; j < n ; ++j){
      u(r,j) = u_conforming(r2, j);
    }
  }
  u.MakeAdditive();
  u.Accumulate();
}



void EGElasticity::MassMatrix(Matrix &massMatrix) const {
  THROW("MassMatrix is not implemented for DG")
}

void EGElasticity::SystemMatrix(Matrix &systemMatrix) const {
  THROW("System is not implemented for DG")
}

void EGElasticity::Initialize(Vectors &vecs) const {
  THROW("Initialize is not implemented for DG")
}

std::array<double, 4> EGElasticity::ChamberVolume(const Vector &U) const {
  std::array<double, 4> volumes{0.0, 0.0, 0.0, 0.0};

  Vector phi(U);
  int count  = 0;
  for (row r = phi.rows(); r != phi.rows_end(); ++r) {
    if (r.NumberOfDofs() > 1){
      auto pos = r();
      for (int i = 0; i < pos.SpaceDim(); ++i) {
        phi(r, i) += pos[i];
      }
    }
  }

  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    RowBndValues r_c(phi, *c);
    if (!r_c.onBnd()) continue;

    for (int face = 0; face < c.Faces(); ++face) {
      int bc = r_c.bc(face);

      if ((bc >= PRESSURE_BC_LV) && (bc <= PRESSURE_BC_RA)) {
        if (c.plane()) {
          auto rx = phi.find_row(c.FaceCorner(face, 0));
          auto ry = phi.find_row(c.FaceCorner(face, 1));

          double v = 0.5 * det(TensorT<double, 2>(phi(rx, 0), phi(ry, 0),
                                                  phi(rx, 1), phi(ry, 1)));
          volumes[bc % 230] += v;
        } else {
          auto ra = phi.find_row(c.FaceCorner(face, 0));
          auto rb = phi.find_row(c.FaceCorner(face, 1));
          auto rc = phi.find_row(c.FaceCorner(face, 2));

          double v = 1.0 / 6.0 * det(Tensor(
              phi(ra, 0), phi(rb, 0), phi(rc, 0),
              phi(ra, 1), phi(rb, 1), phi(rc, 1),
              phi(ra, 2), phi(rb, 2), phi(rc, 2)
          ));

          volumes[bc % 230] += v;
          if (c.ReferenceType() == HEXAHEDRON) {
            auto rd = phi.find_row(c.FaceCorner(face, 3));
            v = 1.0 / 6.0 * det(Tensor(
                phi(ra, 0), phi(rc, 0), phi(rd, 0),
                phi(ra, 1), phi(rc, 1), phi(rd, 1),
                phi(ra, 2), phi(rc, 2), phi(rd, 2)
            ));
            volumes[bc % 230] += v;
          }
        }

      }
    }
  }

  for (int i = 0; i < 4; ++i) {
    volumes[i] = abs(PPM->Sum(volumes[i])) * CUBICMMtoML;
  }
  return volumes;
}

#include "MixedEGElement.hpp"
void
EGElasticity::fixationBnd(const cell &c, const Vector &U, RowBndValues &u_c,
                                int face) const {
  for (int j = 0; j < U.NumberOfNodalPointsOnFace(*c, face); ++j) {
    int k = U.IdOfNodalPointOnFace(*c, face, j);
    for (int d = 0; d < c.dim(); ++d) {
      u_c(k, d) = 0.0;
      u_c.D(k, d) = true;
    }
  }
}


void EGElasticity::dirichletBnd(const cell &c, const Vector &U, RowBndValues &u_c,
                                      int face) const {
  MixedEGElement elem(U, *c);
  for (int j = 0; j < U.NumberOfNodalPointsOnFace(*c, face); ++j) {
    int k = U.IdOfNodalPointOnFace(*c, face, j);
    VectorField D(eProblem.Deformation(Time(), elem.NodalPoint(k)));
    for (int d = 0; d < c.dim(); ++d) {
      u_c(k, d) = D[d];
      u_c.D(k, d) = true;
    }
  }
}

void EGElasticity::planeFixationBnd(const cell &c, const Vector &U, RowBndValues &u_c,
                                          int face, int d) const {
  for (int j = 0; j < U.NumberOfNodalPointsOnFace(*c, face); ++j) {
    int k = U.IdOfNodalPointOnFace(*c, face, j);
    u_c(k, d) = 0.0;
    u_c.D(k, d) = true;
  }
}

void EGElasticity::Initialize(const cell &c, Vector &U) const {
//  U.ClearDirichletFlags();
  RowBndValues u_c(U, *c);
  if (!u_c.onBnd()) return;
  for (int face = 0; face < c.Faces(); ++face) {
//    mout << "bnd " << u_c.bc(face) << endl << endl;
    switch (u_c.bc(face)) {
      case 1:
        dirichletBnd(c, U, u_c, face);
        break;
      case 100:
        planeFixationBnd(c, U, u_c, face, 0);
        break;
      case 101:
        planeFixationBnd(c, U, u_c, face, 1);
        break;
      case 102:
        planeFixationBnd(c, U, u_c, face, 2);
        break;
      case 199:
        fixationBnd(c, U, u_c, face);
        break;
    }
  }
}

std::pair<double, double> EGElasticity::detF(const Vector &u) const {
  double Jmax = 1.0;
  double Jmin = 1.0;

  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    MixedEGVectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      Tensor DU = E.VectorGradient(q, u);
      double J = det(One + DU);
      Jmin = std::min(Jmin, J);
      Jmax = std::max(Jmax, J);
    }
  }
  Jmin = PPM->Min(Jmin);
  Jmax = PPM->Max(Jmax);

  return {Jmin, Jmax};
}

