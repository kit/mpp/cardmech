#ifndef DGELASTICITY_HPP
#define DGELASTICITY_HPP


#include "IElasticity.hpp"
#include "DGDiscretization.hpp"

class DGElasticity : public IElasticity {
  double sign{-1};
  double penalty{1};
  int size{3};

  std::shared_ptr<const DGDiscretization> disc;

protected:
  void prestress(const Vector &u, Vector &pStress) override;
public:
  DGElasticity(ElasticityProblem &eP, int degree, bool isStatic = false);

  std::shared_ptr<IDiscretization> CreateDisc(int sizes) const override {
    return std::make_shared<DGDiscretization>(disc->GetMeshes(), degree, sizes);
  }

  const IDiscretization &GetDisc() const override { return *disc; }

  std::shared_ptr<const IDiscretization> GetSharedDisc() const override {return disc; }

  void Initialize(Vector &u) const override {};

  double Energy(const Vector &u) const override;

  void Energy(const cell &c, const Vector &u, double &energy) const override;

  void Residual(const cell &c, const Vector &u, Vector &r) const override;

  void PressureBoundary(const cell &c, int face, int bc, const Vector &U, Vector &r) const;

  using IAssemble::Jacobi;

  void Jacobi(const cell &c, const Vector &u, Matrix &A) const override;

  void MassMatrix(Matrix &massMatrix) const override;

  void SystemMatrix(Matrix &systemMatrix) const override;

  void GetInvariant(const Vector &displacement, Vector &iota4) const override;

  double TractionEnergy(const cell &c, int face, int bc, const Vector &U) const;

  double PressureEnergy(const cell &c, int face, int bc, const Vector &U) const;


  void Project(const Vector &fine, Vector &u) const override;

  void Interpolate(const Vector &coarse, Vector &u) const override;

  void UseLagrangeValuesAsStartVector(const Vector &u_conforming, Vector &u) const override;

    //void Project(Vector &coarse, const Vector &fine) const;

  double L2Error(const Vector &u) const override;

  double L2Error(const Vector &u, const Vector &reference) const override;

  double H1Error(const Vector &u) const override;

  double H1Error(const Vector &u, const Vector &reference) const override;

  double StrainEnergyError(const Vector &u) const override;

  double StrainEnergyError(const Vector &u, const Vector &reference) const override;

  double EnergyError(const Vector &u) const override;

  /* double EnergyError(const Vector &u, const Vector &reference) const override;*/

  double PressureError(const Vector &u) const override;

  double NeumannError(const Vector &u) const override;


  void TractionBoundary(const cell &c, int face, int bc, const Vector &U, Vector &r) const;


  double MaxStress(const Vector &u) const override;


  double NormPrestress();


  void PlotBoundary(const Vector &U, const Vector &scal) const override;

  void PlotDisplacement(const Vector &u, int step,
                        const std::string &varname) const override;

  //std::array<double, 4> ChamberVolume(const Vector &U) const override;
  void FixationBoundary(const cell &c, int face, int bc, const Vector &U, Vector &r) const;

  void DirichletBoundary(const cell &c, int face, int bc, const Vector &U, Vector &r) const;

  void SetDisplacement(Vector &u) const override;

  //TODO
  std::array<double, 4> ChamberVolume(const Vector &u) const override;

  double InitialVolume(const Mesh &M) const override;

  double DeformedVolume(const Vector &U) const override;

  double L2Norm(const Vector &u) const override;

  double H1Norm(const Vector &u) const override;

  double StrainEnergyNorm(const Vector &u) const override;

  double EnergyNorm(const Vector &u) const override;

  double EnergyValue(const Vector &u) const override;

  double L2AvgNorm(const Vector &u) const override;

  void ProblemEvaluation(const Vector &u) const override;

  void Initialize(Vectors &vecs) const;

  void Scale (double s) const {
    Material &cellMat = eProblem.GetMaterial();
//    cellMat.ScalePenalty(s*s);
  }

  std::pair<double, double> detF(const Vector &u) const override;
};

#endif //DGELASTICITY_HPP
