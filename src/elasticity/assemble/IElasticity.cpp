//#include <LagrangeDiscretization.hpp>
#include "Plotting.hpp"
#include "IElasticity.hpp"
#include "VectorFieldElement.hpp"
#include "LagrangeElasticity.hpp"
#include "DGElasticity.hpp"
#include "EGElasticity.hpp"

IElasticity::IElasticity(ElasticityProblem &eP) :
    INonLinearTimeAssemble(), ICardiacAssemble("MechVTK"), eProblem(eP), check_p(false) {

  if (!eProblem.IsStatic()) {
    Config::Get("Density", base_rho);
    Config::Get("Density", rho);
  }

  Config::Get("StretchFactorSheet", factor_gs);
  Config::Get("StretchFactorNormal", factor_gn);
}

void IElasticity::SetInitialValue(Vector &u) {
  int prestressLevel=0;
  Config::Get("PrestressLevel", prestressLevel);
  // Initialize ElasticityProblem in src/elasticity/problem/ElasticityProblem.hpp
  eProblem.Initialize(StepSize(), Time(), ChamberVolume(u));
  const Meshes &M = u.GetMeshes();
  auto cellDisc = std::make_shared<LagrangeDiscretization>(M, 0, 3);
  gamma_c= !Prestress ? std::make_unique<Vector>(cellDisc,prestressLevel) : std::make_unique<Vector>(cellDisc);

  if (!Prestress) {
    Prestress = std::make_unique<Vector>(0.0, u);
  }
  gamma = std::make_unique<Vector>(0.0, u);
}

void IElasticity::SetInitialValue(Vectors &values) {
  SetInitialValue(values[0]);
  values[1] = *gamma;
  GetInvariant(values[0], values[2]);
}

void IElasticity::InitializePrestress(const Vector &u) {
  Prestress = std::make_unique<Vector>(0.0, u);

  prestress(u, *Prestress);

  Matrix traction(u);
  TractionStiffness(traction);

  *Prestress += traction * u;

  Prestress->ClearDirichletValues();
  Prestress->Collect();
}


void IElasticity::Initialize(Vector &u) const {  //timeScheme->StartVector(u);
  u.ClearDirichletFlags();
  for (cell c = u.cells(); c != u.cells_end(); ++c)
    Initialize(c, u);
  u.DirichletConsistent();
}

void IElasticity::FinishTimeStep(const Vector &u) {
  eProblem.Update(StepSize(), Time(), ChamberVolume(u));
}

void IElasticity::PlotDisplacement(const Vector &U, int step,
                                   const std::string &varname) const {
  // Plotting the deformation in src/CardiacAssemble.hpp
  PlotDeformed(U, {{"Displacement", U}}, step, varname);
}

void IElasticity::Initialize(Vectors &vecs) const {
  Initialize(vecs[0]);
  UpdateStretch(vecs[1]);
}


void IElasticity::Finalize(Vectors &vecs) {
  FinishTimeStep(vecs[0]);
  // Calculate current 4th invariant
  GetInvariant(vecs[0], vecs[2]);
}

void IElasticity::CalculateAndPrintMinMaxGammaF() const{
  if (gammaEvaluation.empty()) THROW("gammaEvaluation is empty, should not be used!")

  double max_gamma = gammaEvaluation[0][0];
  double min_gamma = gammaEvaluation[0][0];
  for (VectorField v : gammaEvaluation){
    min_gamma = std::min(min_gamma, v[0]);
    max_gamma = std::max(max_gamma, v[0]);
  }
  vout(0) << "minimal value of gamma_f is " << min_gamma << endl ;
  vout(0) << "maximal value of gamma_f is " << max_gamma << endl ;
}

void IElasticity::PointEvaluationGamma(const std::string &varname,
                                       const std::vector<Point> &point) {
  IElasticity::PrintPointEvaluation(*gamma, varname, point);
};
void IElasticity::PrintPointEvaluation(const Vector &var,
                                       const std::string &varname,
                                       const std::vector<Point> &point) {
  std::vector<VectorField> values(point.size());
  for (row r = var.rows(); r != var.rows_end(); ++r) {
    auto [i, isNear] = isIn(r(), point);
    if (isNear) {
      auto p = var.find_procset(r());
      for (int j = 0; j < SpaceDimension; ++j) {
        if (p != var.procsets_end()) {
          values[i][j] = var(r, j) / p.size();
        } else {
          values[i][j] = var(r, j);
        }
      }
    }
  }
  for (int j = 0; j < values.size(); ++j) {
    values[j] = PPM->Sum(values[j]);
    vout(0) << varname << point[j] << " = " << values[j] << " -- norm( "<< varname << j <<"): "
            << norm(values[j]) << endl ;
    if (contains(varname,"gamma")) {
      gammaEvaluation.emplace_back(values[j]);
    }
  }
  mout << endl;
};

void IElasticity::PrintIteration(const Vector &u) const {
  auto volumes = ChamberVolume(u);
  mout.PrintInfo("Dynamic Step Evaluation", verbose,
                 PrintInfoEntry("Time", Time(), 1),
                 PrintInfoEntry("Initial Geometry Volume", InitialVolume(u.GetMesh()), 2),
                 PrintInfoEntry("Deformed Geometry Volume", DeformedVolume(u), 2),
                 PrintInfoEntry("LV-Volume", volumes[0], 2),
                 PrintInfoEntry("RV-Volume", volumes[1], 2),
                 PrintInfoEntry("LA-Volume", volumes[2], 2),
                 PrintInfoEntry("RA-Volume", volumes[3], 2),
                 PrintInfoEntry("FrobeniusNorm", norm(u), 2),
                 PrintInfoEntry("L2Norm", L2Error(u), 2),
                 PrintInfoEntry("H1Norm", H1Error(u), 2),
                 PrintInfoEntry("EnergyNorm", EnergyError(u), 2)
  );
}

void IElasticity::PlotIteration(const Vector &u) const {
  if (Step() == 0) PlotBoundary(u, u);
  PlotDisplacement(u, Step(), "U");
}

void IElasticity::PlotPressureIteration(const Vector &u, int step) const {
  if (step == 0) PlotBoundary(u, u);
  PlotDisplacement(u, step, "U");
}

void IElasticity::PrintPressureIteration(const Vector &u) const {
  auto volumes = ChamberVolume(u);
  mout.PrintInfo("Pressure Step Evaluation", verbose,
                 PrintInfoEntry("Pressure Scale", eProblem.Scale(), 1),
                 PrintInfoEntry("Initial Geometry Volume (mm^3)", InitialVolume(u.GetMesh()), 1),
                 PrintInfoEntry("Deformed Geometry Volume (mm^3)", DeformedVolume(u), 1),
                 PrintInfoEntry("LV-Volume (ml)", volumes[0], 1),
                 PrintInfoEntry("RV-Volume (ml)", volumes[1], 1),
                 PrintInfoEntry("LA-Volume (ml)", volumes[2], 1),
                 PrintInfoEntry("RA-Volume (ml)", volumes[3], 1),
                 PrintInfoEntry("FrobeniusNorm", norm(u), 1),
                 PrintInfoEntry("L2Norm", L2Error(u), 2),
                 PrintInfoEntry("H1Norm", H1Error(u), 2),
                 PrintInfoEntry("EnergyNorm", EnergyError(u), 2)
  );
}

void IElasticity::PlotIteration(const Vectors &mechValues, int step) const {
  if (mechValues[0].GetDisc().DiscName().find("DG") != std::string::npos) {
    THROW("TODO: Override in DGElasticity as in PlotDisplacement!")
  }
  step = step < 0 ? Step() : step;

  if (step == 0) PlotBoundary(mechValues[0], mechValues[0]);

  PlotDeformed(mechValues[0], {
                   {"Displacement", mechValues[0]},
                   {"Stretch",      mechValues[1]},
                   {"Potential",    mechValues[3]},
                   {"Iota",         mechValues[2]}},
               step, "U");
}


std::unique_ptr<IElasticity>
CreateFiniteElasticityAssemble(ElasticityProblem &elasticityProblem, int degree, bool isStatic) {
  // Get used elasticity model in src/elasticity/problem/ElasticityProblem.hpp
  auto elasticityModel = elasticityProblem.Model();
  if (elasticityModel.empty()) Config::Get("MechDiscretization", elasticityModel);
  return CreateFiniteElasticityAssemble(elasticityProblem, elasticityModel, degree,
                                        isStatic);
}

std::unique_ptr<IElasticity>
CreateFiniteElasticityAssemble(ElasticityProblem &elasticityProblem, const std::string &modelName,
                               int degree, bool isStatic) {
  if (modelName.empty()) {
    Warning("No Mechanic Model given")
  }

  if (modelName == "DG") {
    return std::make_unique<DGElasticity>(elasticityProblem, degree, isStatic);
  } else if (modelName == "EG") {
    return std::make_unique<EGElasticity>(elasticityProblem, degree, isStatic);
  } else {
    return std::make_unique<LagrangeElasticity>(elasticityProblem, degree, isStatic);
  }
}
