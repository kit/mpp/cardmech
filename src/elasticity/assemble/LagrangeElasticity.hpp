#ifndef CARDMECH_LAGRANGEELASTICITY_HPP
#define CARDMECH_LAGRANGEELASTICITY_HPP

#include "IElasticity.hpp"
#include "LagrangeDiscretization.hpp"

class LagrangeElasticity : public IElasticity {
    int size{3};
    std::shared_ptr<const LagrangeDiscretization> disc;
    bool withPressure = true;



  std::string name;

  void fixationBnd(const cell &c, const Vector &U, RowBndValues &u_c, int face) const;

  void planeFixationBnd(const cell &c, const Vector &U, RowBndValues &u_c, int face, int d) const;

  void dirichletBnd(const cell &c, const Vector &U, RowBndValues &u_c, int face) const;
protected:
  void prestress(const Vector &u, Vector &pStress) override;
public:
  LagrangeElasticity(ElasticityProblem &eP, int degree, bool isStatic = false);

  const IDiscretization &GetDisc() const override { return *disc; }

  std::shared_ptr<IDiscretization> CreateDisc(int sizes) const override {
    return std::make_shared<LagrangeDiscretization>(*disc, degree, sizes);
  }

  std::shared_ptr<const IDiscretization> GetSharedDisc() const override {return disc; }

  void Initialize(Vectors &vecs) const override;

  void Initialize(const cell &c, Vector &U) const override;

  void SetInitialValue(Vector &u) override;

  double Energy(const Vector &u) const override;

  void Energy(const cell &c, const Vector &u, double &energy) const override;

  double NormFa(const cell &c, const Vector &u) const override;
  double NormFa(const Vector &u) const override;

  double NormPK(const cell &c, const Vector &u) const override;
  double NormPK(const Vector &u) const override;

  void Residual(const cell &c, const Vector &u, Vector &r) const override;

  void ViscoResidual(const cell &c, const Vector &u, const Vector &v, Vector &r) const override;

  void PressureBoundary(const cell &c, int face, int bc, const Vector &U, Vector &r) const;

  using IAssemble::Jacobi;

  void Jacobi(const cell &c, const Vector &u, Matrix &A) const override;

  void ViscoJacobi(const cell &c, const Vector &u, const Vector &v, Matrix &A) const override;

  void MassMatrix(Matrix &massMatrix) const override;

  void EvaluateTractionStiffness(Vector &u) const override;

  void TractionStiffness(Matrix &matrix) const override;

  void TractionViscosity(Matrix &matrix) const override;

  void GetInvariant(const Vector &u, Vector &iota4) const override;

  void GetInvariant(const Vector &u, Vector &iota4, Vector &iota_c) const override;

  double TractionEnergy(const cell &c, int face, int bc, const Vector &U) const;

  double PressureEnergy(const cell &c, int face, int bc, const Vector &U) const;

  double L2Error(const Vector &u, const Vector &reference) const override;

  double H1Error(const Vector &u, const Vector &reference) const override;

  double StrainEnergyError(const Vector &u, const Vector &reference) const override;

  double EnergyError(const Vector &u) const override;

  double PressureError(const Vector &u) const override;

  double NeumannError(const Vector &u) const override;


  void TractionBoundary(const cell &c, int face, int bc, const Vector &U, Vector &r) const;

  void PlotBoundary(const Vector &U, const Vector &scal) const override;

  double MaxStress(const Vector &u) const override;


  double NormPrestress();

  std::array<double, 4> ChamberVolume(const Vector &u) const override;

  double InitialVolume(const Mesh &M) const override;

  double DeformedVolume(const Vector &U) const override;

  double L2Norm(const Vector &u) const override;

  double H1Norm(const Vector &u) const override;

  double StrainEnergyNorm(const Vector &u) const override;

  double EnergyNorm(const Vector &u) const override;

  double EnergyValue(const Vector &u) const override;

  double L2AvgNorm(const Vector &u) const override;

  double L2Error(const Vector &u) const override;

  double H1Error(const Vector &u) const override;

  double StrainEnergyError(const Vector &u) const override;

  double PressureBndNorm(const Vector &u) const override;

  void Project(const Vector &fine, Vector &u) const override;

  void Interpolate(const Vector &coarse, Vector &u) const override;

  void SetDisplacement(Vector &u) const override;

  void UpdateStretch(const Vector &gamma_f) const override;

  void UpdateStretch(const Vector &gamma_f, const Vector &gamma_f_c) const override;

  VectorField CalculateStretch(double t, const Point &xi) const;

  void PointEvaluationActiveStrain(const Point &z, const Vector &u) const;

  void PointEvaluationActiveStrain( Vector &u) const override;

  std::pair<double, double> detF(const Vector &u) const override;

  void Scale (double s) const {
    //TODO warum konvergiert der LandBenchmark mit mehreren PressureSteps nicht, wenn einkommentiert?
    Material &cellMat = eProblem.GetMaterial();
// cellMat.ScalePenalty(s*s);
  }
      
};

#endif //CARDMECH_LAGRANGEELASTICITY_HPP
