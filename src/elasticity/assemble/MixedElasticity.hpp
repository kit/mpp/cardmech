#ifndef MIXEDELASTICITY_HPP
#define MIXEDELASTICITY_HPP

#include "IElasticity.hpp"

class MixedElasticity : public FiniteElasticity {
public:
  MixedElasticity(const ElasticityProblem &eP, const PressureProblem &pP);

  using IAssemble::Energy;

  void Energy(const cell &c, const Vector &W, double &energy) const override;

  using IAssemble::Residual;

  void Residual(const cell &c, const Vector &W, Vector &r) const override;

  void NeumannBoundary(const cell &c, int face, int bc, const Vector &U, Vector &r) const;

  using IAssemble::Jacobi;

  void Jacobi(const cell &c, const Vector &W, Matrix &A) const override;

  void GetStress(const Vector &displacement, Vector &stress) const override;

  double GetStretch(const cell &c) const;

  double MaxStress(const Vector &u) const override;
};

#endif //MIXEDELASTICITY_HPP
