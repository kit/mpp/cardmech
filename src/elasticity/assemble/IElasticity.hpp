#ifndef FINITEELASTICITY_H
#define FINITEELASTICITY_H

#include "Assemble.hpp"
#include "Boundary.hpp"
#include "CardiacAssemble.hpp"

#include "CardiacData.hpp"
#include "problem/ElasticityProblem.hpp"

/**
 * Interface to generate different linear and nonlinear assembles.
 */
class IElasticity : public INonLinearTimeAssemble, public ICardiacAssemble {
protected:
  using IAssemble::verbose;

  ElasticityProblem &eProblem;

  // Dynamics
  double base_rho{0.0};
  double rho{0.0};

  //Fluid Structure Interaction
  bool check_p;

  //Prestress
  std::unique_ptr<Vector> Prestress = nullptr;

  /// Calculates the prestress for the corresponding discretization.
  virtual void prestress(const Vector &u_0, Vector &pStress) = 0;

  //Active Deformation
  std::unique_ptr<Vector> gamma  = nullptr;
  std::vector<VectorFieldT<double,3>> gammaEvaluation{};
  std::unique_ptr<Vector> gamma_c = nullptr;
  bool withCellValues = false;
  double factor_gs{1.0};
  double factor_gn{1.0};

  //Generate Discretization
  int degree{1};

  int dim() { return eProblem.GetMeshes().dim(); }

public:
  IElasticity(ElasticityProblem &eP);

  /// Gets the corresponding discretization.
  virtual const IDiscretization &GetDisc() const = 0;

  virtual std::shared_ptr<const IDiscretization> GetSharedDisc() const = 0;

  /// Creates a new discretization with a different size.
  virtual std::shared_ptr<IDiscretization> CreateDisc(int size) const = 0;

  const char *Name() const override { return "Cardiac Elasticity"; }

  void Initialize(Vectors &vecs) const;

  void Initialize(Vector &u) const override;

  void InitializePrestress(const Vector &u);

  /**
   * Clears DirichletFlags and generate Dirichlet boundary dependent of the initialize
   * method in the assemble.
   * Is called once per time step.
   */
  virtual void Initialize(const cell &c, Vector &U) const {};

  ///sets boolian of cellvalues
  void SetIfWithCellValues(bool b){ withCellValues=b;};

  /// Updates the displacement vector to the next time step.
  void FinishTimeStep(const Vector &u) override;

  /// Calculates iota4 if time step is finished.
  void Finalize(Vectors &vecs);

  using IAssemble::Residual;

  double Residual(const Vector &u, Vector &r) const override {
    r = *Prestress;
    r = 0;
    for (cell ce = u.cells(); ce != u.cells_end(); ++ce) {
      Residual(ce, u, r);
    }
    r.ClearDirichletValues();
    r.Collect();
    return r.norm();
  }

  /**
   *   Calculates the stiffness part k_p for the traction boundary q(u,v,n) = k_p * u + c_p * v and
       stores it in matrix form.
   */
  virtual void TractionStiffness(Matrix &matrix) const {};

  virtual void EvaluateTractionStiffness(Vector &u) const {};

  /**
    *   Calculates the stiffness part c_p for the traction boundary q(u,v,n) = k_p * u + c_p * v and
        stores it in matrix form.
    */
  virtual void TractionViscosity(Matrix &matrix) const {};

  /// Calculates the residual including viscous part.
  virtual double ViscoResidual(const Vector &u, const Vector &v, Vector &defect) const {
    defect = *Prestress;
    defect = 0;
    TRY {
      for (cell ce = u.cells(); ce != u.cells_end(); ++ce)
        ViscoResidual(ce, u, v, defect);
    } CATCH ("Error in Residual")
    defect.ClearDirichletValues();
    defect.Collect();
    return defect.norm();
  }

  /// Calculates the residual including viscous part for every cell.
  virtual void
  ViscoResidual(const cell &c, const Vector &u, const Vector &v, Vector &defect) const {
    Residual(c, u, defect);
  }

  /// Calculate Jacobi including viscous part.
  virtual void ViscoJacobi(const Vector &u, const Vector &v, Matrix &jacobi) const {
    jacobi = 0;
    TRY {
      for (cell c = jacobi.cells(); c != jacobi.cells_end(); ++c)
        ViscoJacobi(c, u, v, jacobi);
    } CATCH ("Error in Jacobi")
    jacobi.ClearDirichletValues();
  }

  /// Calculate Jacobi including viscous part for every cell.
  virtual void ViscoJacobi(const cell &c, const Vector &u, const Vector &v, Matrix &jacobi) const {
    Jacobi(c, u, jacobi);
  }

  /// Calculates the invariant iota4.
  virtual void GetInvariant(const Vector &u, Vector &iota4) const = 0;

  virtual void GetInvariant(const Vector &u, Vector &iota4, Vector &iota) const { return GetInvariant(u,iota4); }

  virtual void PlotDisplacement(const Vector &u, int step,
                                const std::string &varname) const;

  /**
   *   Creates a new vector with the discretization of target (coarse vector) and projects fine
       vector on it.
   */
  Vector Projection(const Vector &target, const Vector &fine) const {
    Vector v(0.0, target);
    Project(fine, v);
    return v;
  }

  /// Project fine vector on coarse vector.
  virtual void Project(const Vector &fine, Vector &u) const {
    Warning("No Projection implemented")
  }

  /**
   * Creates a new vector with the discretization of the fine vector and interpolates it on the
   * coarse vector.
   */
  Vector Interpolation(const Vector &coarse, const Vector &target) const {
    Vector v(target);
    Interpolate(coarse, v);
    return v;
  }

  /// Interpolates the fine vector through the coarse vector.
  virtual void Interpolate(const Vector &coarse, Vector &u) const {
    Warning("No Interpolation implemented")
  }

  Vector UseLagrangeAsStartVector(const Vector &u_conforming, const Vector &target) const {
    Vector v(target);
    UseLagrangeValuesAsStartVector(u_conforming, v);
    return v;
  }


  virtual void UseLagrangeValuesAsStartVector(const Vector &u_conforming, Vector &u) const  {
    Warning("Not implemented for this discretization")
  }


  virtual double L2Norm(const Vector &u) const { return 0.0; };

  virtual double L2Error(const Vector &u) const {
    Warning("Method not implemented")
    return 0.0;
  };

  virtual double NormFa(const cell &c, const Vector &u) const { return 0.0; };
  virtual double NormFa(const Vector &u) const { return 0.0; };

  virtual double NormPK(const cell &c, const Vector &u) const { return 0.0; };
  virtual double NormPK(const Vector &u) const { return 0.0; };

  virtual double L2Error(const Vector &u, const Vector &reference) const { return 0.0; };

  virtual double L2Error_new(const Vector &u, const Vector &u1) const { return 0.0; };

  virtual double L2AvgNorm(const Vector &u) const { return 0.0; };

  virtual double H1Norm(const Vector &u) const { return 0.0; };

  virtual double H1Error(const Vector &u) const { return 0.0; };

  virtual double H1Error(const Vector &u, const Vector &reference) const { return 0.0; };

  virtual double StrainEnergyNorm(const Vector &u) const { return 0.0; };

  virtual double StrainEnergyError(const Vector &u) const { return 0.0; };

  virtual double StrainEnergyError(const Vector &u, const Vector &reference) const { return 0.0; };

  virtual double EnergyNorm(const Vector &u) const { return 0.0; };

  virtual double EnergyError(const Vector &u) const { return 0.0; };

  virtual double EnergyError(const Vector &u, const Vector &reference) const { return 0.0; };

  virtual double EnergyValue(const Vector &u) const { return 0.0; };

  virtual double PressureBndNorm(const Vector &u) const { return 0.0; };

  virtual double PressureError(const Vector &u) const { return 0.0; };

  virtual double NeumannError(const Vector &u) const { return 0.0; };

  virtual double MaxStress(const Vector &u) const = 0;

  virtual void ProblemEvaluation(const Vector &u) const {};

  /**
   * Calculates the Volumes if the cardiac chambers
   * May break if chambers are not fully enclosed
   * @param U Deformation Vector
   * @return Volumes of LV, RV, LA, RA
   */
  virtual std::array<double, 4> ChamberVolume(const Vector &U) const {
    return {0.0, 0.0, 0.0, 0.0};
  }

  virtual double InitialVolume(const Mesh &M) const {
    return 0.0;
  }

  virtual double DeformedVolume(const Vector &U) const {
    return 0.0;
  }

  /// For coupled system to update the stretch gamma_f.
  virtual void UpdateStretch(const Vector &gamma_f) const {
    Warning("UpdateStretch is not defined")
  };

  virtual void UpdateStretch(const Vector &gamma_f, const Vector &gamma_f_c) const {
    UpdateStretch(gamma_f);
  };

  virtual void PointEvaluationActiveStrain( Vector &u) const {};

  /// Sets the displacement for a problem with exact solution.
  virtual void SetDisplacement(Vector &u) const {
    Warning("SetDisplacement is not defined")
  };

  const ElasticityProblem &GetElasticityProblem() const {
    return eProblem;
  }

  virtual void PlotBoundary(const Vector &U, const Vector &scal) const {
    Warning("Nothing to Plot")
  };

  void SetInitialValue(Vector &u) override;

  void SetInitialValue(Vectors &values);

  void PointEvaluationGamma(const std::string &varname,
                            const std::vector<Point> &point);

  void PrintPointEvaluation(const Vector &var, const std::string &varname,
                            const std::vector<Point> &point);

  /// Every time step will be plotted if we are in the dynamic case.
  void PlotIteration(const Vector &u) const override;

  void PlotIteration(const Vectors &u, int step = -1) const;

  /// Every pressure step will be plotted (static).
  void PlotPressureIteration(const Vector &u, int step) const;

  /// Prints some information after every time step.
  void PrintIteration(const Vector &u) const override;

  /// Prints some information after every pressure step.
  void PrintPressureIteration(const Vector &u) const;

  void CalculateAndPrintMinMaxGammaF() const;

  virtual std::pair<double, double> detF(const Vector &u) const { return {0.0, 0.0}; }

  virtual void Scale (double s) const {}
};

/**
 * Is searching for the current discretization and than calls the function CreateFiniteElasticityAssemble
   to create different assembles.
 */
std::unique_ptr<IElasticity>
CreateFiniteElasticityAssemble(ElasticityProblem &elasticityProblem, int degree, bool isStatic);
/// Creates different ElasticityAssembles, e.g. Lagrange, DG and EG.
std::unique_ptr<IElasticity>
CreateFiniteElasticityAssemble(ElasticityProblem &elasticityProblem, const std::string &modelName,
                               int degree, bool isStatic);


#endif //FINITEELASTICITY_H
