#include "LagrangeElasticity.hpp"
#include "LagrangeTransfer.hpp"

#include "VectorFieldElement.hpp"

LagrangeElasticity::LagrangeElasticity(ElasticityProblem &eP, int degree, bool isStatic)
    : IElasticity(eP), disc(std::make_shared<LagrangeDiscretization>(eP.GetMeshes(), degree, dim())) {
  this->degree = degree;

  Config::Get("withPressure", withPressure);
  Config::Get("activeDeformation", name);
}

double LagrangeElasticity::Energy(const Vector &u) const {
  double energy = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    Energy(c, u, energy);
  }
  return PPM->Sum(energy);
}


void LagrangeElasticity::Energy(const cell &c, const Vector &U, double &energy) const {
  VectorFieldElement E(U, *c);
  const auto &cellMat = eProblem.GetMaterial(c);

  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);


    Tensor F = DeformationGradient(E, q, U);

    //VectorField v = timeScheme->Velocity<VectorFieldElement>(U, E, q);
    //energy += w * rho * (v * v);
    energy += w * cellMat.TotalEnergy(F, Q);
  }

  BFParts bnd(U.GetMesh(), *c);
  if (!bnd.onBnd()) return;

  for (int face = 0; face < c.Faces(); ++face) {
    int bc = bnd[face];
    if (bc == 2) {
      energy += TractionEnergy(c, face, bc, U);
    }

    if ((bc > 229) && (bc < 234)) {
      energy += PressureEnergy(c, face, bc, U);
    }
  }
}


double LagrangeElasticity::TractionEnergy(const cell &c, int face, int bc, const Vector &U) const {
  double W_Neumann = 0.0;
  VectorFieldFaceElement E(U, *c, face);

  Tensor Q = RotationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    VectorField n = E.QNormal(q);
    VectorField TN = eProblem.TractionStiffness(Time(), E.QPoint(q), c.Subdomain()) *
                     (Product(n, n) * E.VectorValue(q, U));
    W_Neumann += w * TN * TN;
  }
  return W_Neumann;
}


double LagrangeElasticity::PressureEnergy(const cell &c, int face, int bc, const Vector &U) const {
  double W_Pressure = 0.0;
  VectorFieldFaceElement E(U, *c, face);

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    //VectorField N = mat::cofactor(DeformationGradient(E, q, U)) * N;
    VectorField N = E.QNormal(q);

    double localPressure = -eProblem.Pressure(Time(), E.QPoint(q), bc);
    W_Pressure += w * localPressure * (N * E.VectorValue(q, U));
  }

  return W_Pressure;
}

double LagrangeElasticity::NormFa(const cell &c, const Vector &U) const {
  double f = 0;

  VectorFieldElement E(U, *c);
  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    Tensor F = DeformationGradient(E, q, U);
    VectorField stretch = E.VectorValue(q, *gamma);
    if (withCellValues) stretch = VectorField((*gamma_c)(c(), 0), (*gamma_c)(c(), 1), (*gamma_c)(c(), 2));

    Tensor inverseFA = Contracts(c) ? mat::inverse_active_deformation(Q,stretch)
                                    : One;
    inverseFA -= One;
    f += w * Frobenius(inverseFA,inverseFA);
  }
  return f;
}

double LagrangeElasticity::NormFa(const Vector &U) const {
  double f = 0;
  for (cell c = U.cells(); c != U.cells_end(); ++c)
    f += NormFa(c,U);
  return sqrt(PPM->Sum(f));
}


double LagrangeElasticity::NormPK(const cell &c, const Vector &U) const {
  double pk = 0;
  VectorFieldElement E(U, *c);

  const Material &cellMat = eProblem.GetMaterial(c);
  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    Tensor F = DeformationGradient(E, q, U);
    VectorField stretch = E.VectorValue(q, *gamma);
    if (withCellValues) stretch = VectorField((*gamma_c)(c(), 0), (*gamma_c)(c(), 1), (*gamma_c)(c(), 2));

    Tensor inverseFA = Contracts(c) ? mat::inverse_active_deformation(Q,stretch)
                                    : One;
    F = F * inverseFA;

    auto Fiso = eProblem.IsochoricPart(F);
    Tensor fpK = cellMat.FirstPiolaKirchhoff(F, Fiso, Q);
    pk += w * Frobenius(fpK,fpK);
    }
  return pk;
}


double LagrangeElasticity::NormPK(const Vector &U) const {
  double pk = 0;
  for (cell c = U.cells(); c != U.cells_end(); ++c)
    pk += NormPK(c,U);
  return sqrt(PPM->Sum(pk));
}

void LagrangeElasticity::Residual(const cell &c, const Vector &U, Vector &r) const {
  VectorFieldElement E(U, *c);
  RowBndValues r_c(r, *c);

  Tensor Q = OrientationMatrix(c.GetData());
  const Material &cellMat = eProblem.GetMaterial(c);

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    VectorField u = E.VectorValue(q, U);


    Tensor F = DeformationGradient(E, q, U);
    VectorField stretch =  E.VectorValue(q, *gamma);
    if (withCellValues) stretch = VectorField((*gamma_c)(c(), 0), (*gamma_c)(c(), 1), (*gamma_c)(c(), 2));

    Tensor inverseFA = Contracts(c) ? mat::inverse_active_deformation(Q,stretch)
                                    : One;
    F = F * inverseFA;

    auto Fiso = eProblem.IsochoricPart(F);


    VectorField f = eProblem.Load(Time(), E.QPoint(q), *c);
    for (int i = 0; i < E.NodalPoints(); ++i) {
      for (int k = 0; k < c.dim(); ++k) {
        auto u_ik = E.VectorComponentValue(q, i, k);

        auto F_ik = E.VectorRowGradient(q, i, k);
        auto FA_ik = F_ik * inverseFA;

        // Passive material response
        r_c(i, k) += w * cellMat.TotalDerivative(Fiso, F, Q, FA_ik);

        // Body load
        r_c(i, k) -= w * (f * u_ik);
      }
    }
  }

  if (!r_c.onBnd()) return;

  for (int face = 0; face < c.Faces(); ++face) {
    int bc = r_c.bc(face);

    if ((bc >= PRESSURE_BC_LV) && (bc <= PRESSURE_BC_RA)) {
      if (withPressure) {
        PressureBoundary(c, face, bc, U, r);
      }
    }
  }
}


void LagrangeElasticity::ViscoResidual(const cell &c, const Vector &u, const Vector &v,
                                       Vector &r) const {
  VectorFieldElement E(u, *c);
  RowBndValues r_c(r, *c);

  Tensor Q = OrientationMatrix(c.GetData());
  const Material &cellMat = eProblem.GetMaterial(c);

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    VectorField stretch =  E.VectorValue(q, *gamma);
    if (withCellValues) stretch = VectorField((*gamma_c)(c(), 0), (*gamma_c)(c(), 1), (*gamma_c)(c(), 2));

    Tensor inverseFA = Contracts(c) ?
                       mat::inverse_active_deformation(Q, stretch) : One;
    Tensor F = DeformationGradient(E, q, u) * inverseFA;
    auto Fiso = eProblem.IsochoricPart(F);
    Tensor L = E.VectorGradient(q, v);// * inverseFA;?


    VectorField f = eProblem.Load(Time(), E.QPoint(q), *c);
    for (int i = 0; i < E.NodalPoints(); ++i) {
      for (int k = 0; k < c.dim(); ++k) {
        auto u_ik = E.VectorComponentValue(q, i, k);
        auto F_ik = E.VectorRowGradient(q, i, k) * inverseFA;

        // Viscose damping
        r_c(i, k) += w * cellMat.ViscoDerivative(F, L, F_ik);

        // Passive material response
        r_c(i, k) += w * cellMat.TotalDerivative(Fiso, F, Q, F_ik);

        // Body load
        r_c(i, k) -= w * (f * u_ik);
      }
    }
  }

  if (!r_c.onBnd()) return;

  for (int face = 0; face < c.Faces(); ++face) {
    int bc = r_c.bc(face);

    if ((bc >= PRESSURE_BC_LV) && (bc <= PRESSURE_BC_RA)) {
      if (withPressure) {
        PressureBoundary(c, face, bc, u, r);
      }
    }
  }
}

void LagrangeElasticity::PressureBoundary(const cell &c, int face, int bc, const Vector &U,
                                          Vector &r) const {
  VectorFieldFaceElement FE(U, *c, face);
  RowValues r_c(r, FE);
  const Material &cellMat = eProblem.GetMaterial(c);

  for (int q = 0; q < FE.nQ(); ++q) {
    double w = FE.QWeight(q);
    Tensor F = DeformationGradient(FE, q, U);
    VectorField N = mat::cofactor(F) * FE.QNormal(q);
    if (contains(cellMat.Name(),"Linear") || contains(cellMat.Name(),"Laplace")) {
       N = FE.QNormal(q);
    }
    double localPressure = -eProblem.Pressure(Time(), FE.QPoint(q), bc);

    for (int i = 0; i < FE.NodalPoints(); ++i) {
      for (int k = 0; k < c.dim(); ++k) {
        auto u_ik = FE.VectorComponentValue(q, i, k);
        r_c(i, k) -= w * (localPressure) * N * u_ik;
      }
    }
  }
}

void LagrangeElasticity::TractionBoundary(const cell &c, int face, int bc, const Vector &U,
                                          Vector &r) const {
  VectorFieldFaceElement E(U, *c, face);
  RowValues r_c(r, E);

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);
    Point xi = E.QPoint(q);
    VectorField n = E.QNormal(q);
    VectorField normalStiffness = eProblem.TractionStiffness(Time(), xi, c.Subdomain()) *
                                  (Product(n, n) * E.VectorValue(q, U));

    for (int i = 0; i < E.NodalPoints(); ++i) {
      for (int k = 0; k < c.dim(); ++k) {
        r_c(i, k) += w * normalStiffness * E.VectorComponentValue(q, i, k);
      }
    }
  }
}


void LagrangeElasticity::Jacobi(const cell &c, const Vector &U, Matrix &A) const {
  VectorFieldElement E(U, *c);
  RowEntries A_c(A, E);


  // TODO: Should get the actual material used in this cell.
  const Material &cellMat = eProblem.GetMaterial(c);
  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    VectorField stretch =  E.VectorValue(q, *gamma);
    if (withCellValues) stretch = VectorField((*gamma_c)(c(), 0), (*gamma_c)(c(), 1), (*gamma_c)(c(), 2));

//    For Debugging
//    if ((*gamma_c)(c(), 0) != 0)
//      mout << "J " << c() << " gamma_f " << stretch[0] << endl;
    Tensor F = DeformationGradient(E, q, U);
    Tensor inverseFA = Contracts(c) ? mat::inverse_active_deformation(Q,stretch)
                                    : One;
    F = F * inverseFA;

    auto Fiso = eProblem.IsochoricPart(F);

    for (int i = 0; i < E.NodalPoints(); ++i) {
      for (int k = 0; k < c.dim(); ++k) {
        auto u_ik = E.VectorComponentValue(q, i, k);
        auto F_ik = E.VectorRowGradient(q, i, k);
        auto FA_ik = F_ik * inverseFA;
        for (int j = 0; j < E.NodalPoints(); ++j) {
          for (int l = 0; l < c.dim(); ++l) {
            auto u_jl = E.VectorComponentValue(q, j, l);
            auto F_jl = E.VectorRowGradient(q, j, l);
            auto FA_jl = F_jl * inverseFA;

            // Acceleration
            //A_c(i, j, k, l) += w * rho * timeScheme->Jabobi() * (u_ik * u_jl);

            // Passive Material response
            A_c(i, j, k, l) += w * cellMat.TotalSecondDerivative(Fiso, F, Q, FA_ik, FA_jl);
          }
        }
      }
    }
  }
  if (contains(cellMat.Name(),"Linear") || contains(cellMat.Name(),"Laplace")) return;
  for (int face = 0; face < c.Faces(); ++face) {
    if (E.Bnd(face) >= PRESSURE_BC_LV && E.Bnd(face) <= PRESSURE_BC_RA) {
      VectorFieldFaceElement FE(U, *c, face);
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        VectorField N = FE.QNormal(q);
        Tensor F = DeformationGradient(FE, q, U);
        double localPressure = -eProblem.Pressure(Time(), FE.QPoint(q), E.Bnd(face));
        for (int i = 0; i < FE.NodalPoints(); ++i) {
          for (int k = 0; k < c.dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            for (int j = 0; j < FE.NodalPoints(); ++j) {
              for (int l = 0; l < c.dim(); ++l) {
                auto F_jl = FE.VectorRowGradient(q, j, l);
                A_c(i, j, k, l) -= w * (localPressure) * (Cross(F, F_jl) * N) * u_ik;
              }
            }
          }
        }
      }
    }
  }
}

void LagrangeElasticity::ViscoJacobi(const cell &c, const Vector &u, const Vector &v, Matrix &A) const {
  VectorFieldElement E(u, *c);
  RowEntries A_c(A, E);


  // TODO: Should get the actual material used in this cell.
  const Material &cellMat = eProblem.GetMaterial(c);
  Tensor Q = OrientationMatrix(c.GetData());

  for (int q = 0; q < E.nQ(); ++q) {
    double w = E.QWeight(q);

    VectorField stretch =  E.VectorValue(q, *gamma);
    if (withCellValues) stretch = VectorField((*gamma_c)(c(), 0), (*gamma_c)(c(), 1), (*gamma_c)(c(), 2));

//    For Debugging
//    if ((*gamma_c)(c(), 0) != 0)
//      mout << "VJ " << c() << " gamma_f " << stretch[0] << endl;
    Tensor inverseFA = Contracts(c) ?
                       mat::inverse_active_deformation(Q,stretch) : One;
    Tensor F = DeformationGradient(E, q, u) * inverseFA;
    Tensor L = E.VectorGradient(q, v);// * inverseFA;?

    auto Fiso = eProblem.IsochoricPart(F);

    for (int i = 0; i < E.NodalPoints(); ++i) {
      for (int k = 0; k < c.dim(); ++k) {
        auto u_ik = E.VectorComponentValue(q, i, k);
        auto F_ik = E.VectorRowGradient(q, i, k) * inverseFA;
        for (int j = 0; j < E.NodalPoints(); ++j) {
          for (int l = 0; l < c.dim(); ++l) {
            auto u_jl = E.VectorComponentValue(q, j, l);
            auto F_jl = E.VectorRowGradient(q, j, l) * inverseFA;

            // Viscoelasic response
            A_c(i, j, k, l) += w * cellMat.ViscoSecondDerivative(F, L, F_ik, F_jl);

//    For Debugging
//	    double h = 0.0000001;
//	    if (stretch[0] < -0.01)
//	      mout << "c " << c()
//		   << "D^2 "     << cellMat.ViscoSecondDerivative(F, L, F_ik, F_jl)
//		   << "D^2_h "   << (cellMat.ViscoDerivative(F + h * F_jl, L, F_ik)
//				     - cellMat.ViscoDerivative(F, L, F_ik)) / h
//		   << endl;

            // Passive Material response
            A_c(i, j, k, l) += w * cellMat.TotalSecondDerivative(Fiso, F, Q, F_ik, F_jl);
          }
        }
      }
    }
  }
  if (contains(cellMat.Name(),"Linear") || contains(cellMat.Name(),"Laplace")) return;
  for (int face = 0; face < c.Faces(); ++face) {
    if (E.Bnd(face) >= PRESSURE_BC_LV && E.Bnd(face) <= PRESSURE_BC_RA) {
      VectorFieldFaceElement FE(u, *c, face);
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        VectorField N = FE.QNormal(q);
        Tensor F = DeformationGradient(FE, q, u);
        double localPressure = -eProblem.Pressure(Time(), FE.QPoint(q), E.Bnd(face));
        for (int i = 0; i < FE.NodalPoints(); ++i) {
          for (int k = 0; k < c.dim(); ++k) {
            auto u_ik = FE.VectorComponentValue(q, i, k);
            for (int j = 0; j < FE.NodalPoints(); ++j) {
              for (int l = 0; l < c.dim(); ++l) {
                auto F_jl = FE.VectorRowGradient(q, j, l);
                A_c(i, j, k, l) -= w * (localPressure) * (Cross(F, F_jl) * N) * u_ik;
              }
            }
          }
        }
      }
    }
  }
}

VectorField LagrangeElasticity::CalculateStretch(double t, const Point &xi) const {
  VectorField stretch{};
  stretch[0] = eProblem.ActiveStretch(t, xi);
  stretch[2] = factor_gn * (1.0 / sqrt(1.0 + stretch[0]) - 1.0);
  stretch[1] = 1.0 / ((1 + stretch[0]) * (1.0 + stretch[2])) - 1.0;
  return stretch;
}


double LagrangeElasticity::L2Error(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u) - eProblem.Deformation(Time(), E.QPoint(q));
      err += E.QWeight(q) * (U * U);
    }
  }
  return sqrt(PPM->Sum(err));
}

double LagrangeElasticity::L2Norm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u);
      err += E.QWeight(q) * (U * U);
    }
  }
  return sqrt(PPM->Sum(err));
}

double LagrangeElasticity::L2AvgNorm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);

    double w = 0.0;
    VectorField U{};
    for (int q = 0; q < E.nQ(); ++q) {
      w += E.QWeight(q);
      U += E.VectorValue(q, u);
    }
    err += w * (U * U);

  }
  return sqrt(PPM->Sum(err));
}

double LagrangeElasticity::L2Error(const Vector &u, const Vector &reference) const {
  Vector projection(u);
  LagrangeTransfer T(projection, reference);
  T.Project(projection, reference);
  return L2Norm(u - projection);
}


double LagrangeElasticity::H1Error(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u)
                      - eProblem.Deformation(Time(), E.QPoint(q));

      Tensor DU =
          E.VectorGradient(q, u) - eProblem.DeformationGradient(Time(), E.QPoint(q));
      err += E.QWeight(q) * ((U * U) + Frobenius(DU, DU));
    }
  }
  return sqrt(PPM->Sum(err));
}

double LagrangeElasticity::H1Norm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      VectorField U = E.VectorValue(q, u);
      Tensor DU = E.VectorGradient(q, u);
      err += E.QWeight(q) * ((U * U) + Frobenius(DU, DU));

    }
  }
  return sqrt(PPM->Sum(err));
}

double LagrangeElasticity::H1Error(const Vector &u, const Vector &reference) const {
  Vector projection(u);
  LagrangeTransfer T(projection, reference);
  T.Project(projection, reference);

  return H1Norm(u - projection);
}


double LagrangeElasticity::StrainEnergyError(const Vector &u) const {
  double err = 0;

  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);

    Tensor Q = OrientationMatrix(c.GetData());

    const auto &cellMat = eProblem.GetMaterial(c);
    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      const Point &z = E.QPoint(q);
      Tensor DD = E.VectorGradient(q, u) - eProblem.DeformationGradient(Time(), z);
      err += w * abs(cellMat.TotalDerivative(One + DD, One + DD, Q, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}

double LagrangeElasticity::StrainEnergyNorm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      Tensor DD = E.VectorGradient(q, u);
      err += w * abs(cellMat.TotalDerivative(One + DD, One + DD, Q, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}

double LagrangeElasticity::EnergyNorm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      Tensor DD = E.VectorGradient(q, u);
      err += w * abs(cellMat.TotalSecondDerivative(One, One, Q, DD, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}

double LagrangeElasticity::EnergyError(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      const Point &z = E.QPoint(q);
      Tensor DD = E.VectorGradient(q, u) - eProblem.DeformationGradient(Time(), z);
      err += w * abs(cellMat.TotalSecondDerivative(One, One, Q, DD, DD));
    }
  }
  return sqrt(PPM->Sum(err));
}


double LagrangeElasticity::EnergyValue(const Vector &u) const {
  double energy = 0.0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    const auto &cellMat = eProblem.GetMaterial(c);
    Tensor Q = OrientationMatrix(c.GetData());
    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);
      Tensor F = DeformationGradient(E, q, u);
      energy += w * cellMat.TotalEnergy(F, Q);
    }
  }
  return abs(PPM->Sum(energy));
}

void LagrangeElasticity::PointEvaluationActiveStrain(const Point& z, const Vector &u) const {
  double valueFibre = 0;
  double valueSheet = 0;
  double valueNormal = 0;
  double cellCount = 0;
  Tensor S = Zero;
  Tensor S_a = Zero;
  Tensor ff = Zero;
  VectorField stretch2 = zero;

  for (cell c = u.cells(); c != u.cells_end(); ++c){
    for (int i = 0; i < c.Corners(); ++i) {
      if ( c.Corner(i) == z) {
        VectorFieldElement E(u, *c);

        Tensor Q = OrientationMatrix(c.GetData());
        const Material &cellMat = eProblem.GetMaterial(c);
        Tensor F = DeformationGradient(E, c.LocalCenter(), u);
        auto Fiso = eProblem.IsochoricPart(F);
	      Tensor Finv = Invert(F);
	      Finv = One;
	      //TODO: To be checked: Is this the same as SecondPK?
        Tensor P = cellMat.FirstPiolaKirchhoff(F, Fiso, Q);
        S += Finv * P;

        VectorField stretch = E.VectorValue(c.LocalCenter(), *gamma);
        if (withCellValues)
          stretch = VectorField((*gamma_c)(c(),
                                           0), (*gamma_c)(c(), 1), (*gamma_c)(c(), 2));
        Tensor inverseFA = Contracts(c) ?
                           mat::inverse_active_deformation(Q, stretch)
                                        : One;

	      stretch2 += stretch;
        F = F * inverseFA;
        Fiso = eProblem.IsochoricPart(F);
        Tensor P_Fa = cellMat.FirstPiolaKirchhoff(F, Fiso, Q);
        Tensor P_a = P_Fa;
	      P_a -= P;

	      S_a += Finv * P_a;
        const VectorField fibre = Direction(c.GetData());
        const VectorField sheet = Sheet(c.GetData());
        const VectorField normal = Normal(c.GetData());

        valueFibre += fibre * P_a * fibre;
        valueSheet += sheet * P_a * sheet;
        valueNormal += normal * P_a * normal;
	      ff += Product(fibre,fibre);
        cellCount++;
      }
    }
  }
  cellCount = PPM->Sum(cellCount);
  if(cellCount > 1 ) {
    stretch2[0] = PPM->Sum(stretch2[0]) / cellCount;
    stretch2[1] = PPM->Sum(stretch2[1]) / cellCount;
    stretch2[2] = PPM->Sum(stretch2[2]) / cellCount;
    valueFibre = PPM->Sum(valueFibre) / cellCount;
    valueSheet = PPM->Sum(valueSheet) / cellCount;
    valueNormal = PPM->Sum(valueNormal) / cellCount;
    S[0][0] = PPM->Sum(S[0][0]) / cellCount;
    S[1][0] = PPM->Sum(S[1][0]) / cellCount;
    S[2][0] = PPM->Sum(S[2][0]) / cellCount;
    S[0][1] = PPM->Sum(S[0][1]) / cellCount;
    S[1][1] = PPM->Sum(S[1][1]) / cellCount;
    S[2][1] = PPM->Sum(S[2][1]) / cellCount;
    S[0][2] = PPM->Sum(S[0][2]) / cellCount;
    S[1][2] = PPM->Sum(S[1][2]) / cellCount;
    S[2][2] = PPM->Sum(S[2][2]) / cellCount;
    S_a[0][0] = PPM->Sum(S_a[0][0]) / cellCount;
    S_a[1][0] = PPM->Sum(S_a[1][0]) / cellCount;
    S_a[2][0] = PPM->Sum(S_a[2][0]) / cellCount;
    S_a[0][1] = PPM->Sum(S_a[0][1]) / cellCount;
    S_a[1][1] = PPM->Sum(S_a[1][1]) / cellCount;
    S_a[2][1] = PPM->Sum(S_a[2][1]) / cellCount;
    S_a[0][2] = PPM->Sum(S_a[0][2]) / cellCount;
    S_a[1][2] = PPM->Sum(S_a[1][2]) / cellCount;
    S_a[2][2] = PPM->Sum(S_a[2][2]) / cellCount;
    ff[0][0] = PPM->Sum(ff[0][0]) / cellCount;
    ff[1][0] = PPM->Sum(ff[1][0]) / cellCount;
    ff[2][0] = PPM->Sum(ff[2][0]) / cellCount;
    ff[0][1] = PPM->Sum(ff[0][1]) / cellCount;
    ff[1][1] = PPM->Sum(ff[1][1]) / cellCount;
    ff[2][1] = PPM->Sum(ff[2][1]) / cellCount;
    ff[0][2] = PPM->Sum(ff[0][2]) / cellCount;
    ff[1][2] = PPM->Sum(ff[1][2]) / cellCount;
    ff[2][2] = PPM->Sum(ff[2][2]) / cellCount;
  }
  /*mout << "F_a" << z
       << " g " << stretch2
       << " f " << valueFibre
       << " s " <<valueSheet
       << " n " << valueNormal
       << endl << " S " << endl << S + S_a
       << endl << " S_p " << endl << S
       << endl << " S_a " << endl << S_a
       << endl << " f x f " << endl << ff
       << endl;*/
}

void LagrangeElasticity::PointEvaluationActiveStrain(Vector &u) const {
  mout << endl;
  PointEvaluationActiveStrain(Point(86.75, -44.2302, 1.74331),u);
  PointEvaluationActiveStrain(Point(48.4428,  -35.0794,   33.8927),u);
  PointEvaluationActiveStrain(Point(22.5562,  -16.1836,   51.2538),u);
  PointEvaluationActiveStrain(Point(55.1275,  -62.8303,   -2.31392),u);
  mout << endl;
  }


double LagrangeElasticity::StrainEnergyError(const Vector &u, const Vector &reference) const {
  Vector projection(u);
  LagrangeTransfer T(projection, reference);
  T.Project(projection, reference);

  return StrainEnergyNorm(u - projection);
}

void LagrangeElasticity::planeFixationBnd(const cell &c, const Vector &U, RowBndValues &u_c,
                                          int face, int d) const {
  for (int j = 0; j < U.NumberOfNodalPointsOnFace(*c, face); ++j) {
    int k = U.IdOfNodalPointOnFace(*c, face, j);
    u_c(k, d) = 0.0;
    u_c.D(k, d) = true;
  }
}

void
LagrangeElasticity::fixationBnd(const cell &c, const Vector &U, RowBndValues &u_c,
                                int face) const {
  for (int j = 0; j < U.NumberOfNodalPointsOnFace(*c, face); ++j) {
    int k = U.IdOfNodalPointOnFace(*c, face, j);
    for (int d = 0; d < c.dim(); ++d) {
      u_c(k, d) = 0.0;
      u_c.D(k, d) = true;
    }
  }
}

void LagrangeElasticity::dirichletBnd(const cell &c, const Vector &U, RowBndValues &u_c,
                                      int face) const {
  ScalarElement elem(U, *c);
  for (int j = 0; j < U.NumberOfNodalPointsOnFace(*c, face); ++j) {
    int k = U.IdOfNodalPointOnFace(*c, face, j);
    VectorField D(eProblem.Deformation(Time(), elem.NodalPoint(k)));
    for (int d = 0; d < c.dim(); ++d) {
      u_c(k, d) = D[d];
      u_c.D(k, d) = true;
    }
  }
}

void LagrangeElasticity::prestress(const Vector &u, Vector &pStress) {
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    const auto &cellMat = eProblem.GetMaterial(c);

    VectorFieldElement E(u, *c);
    RowBndValues r_c(pStress, *c);

    Tensor Q = OrientationMatrix(c.GetData());

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);

      Tensor F = DeformationGradient(E, q, u);
      auto Fiso = eProblem.IsochoricPart(F);

      for (int i = 0; i < E.NodalPoints(); ++i) {
        for (int k = 0; k < c.dim(); ++k) {
          auto F_ik = E.VectorRowGradient(q, i, k);

          // Passive material response
          r_c(i, k) += w * cellMat.TotalDerivative(Fiso, F, Q, F_ik);
        }
      }
    }
  }
}


double LagrangeElasticity::PressureBndNorm(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    if (!E.Bnd()) continue;

    Tensor Q = OrientationMatrix(c.GetData());
    const auto &cellMat = eProblem.GetMaterial(c);
    for (int f = 0; f < c.Faces(); ++f) {
      if (E.Bnd(f) < 230 || E.Bnd(f) > 233) continue;
      VectorFieldFaceElement FE(u, *c, f);
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        VectorField N = FE.QNormal(q);
        Tensor NN = Product(N, N);

        Tensor F = DeformationGradient(FE, q, u);
        Tensor Fiso = eProblem.IsochoricPart(F);

        double S = cellMat.TotalDerivative(Fiso, F, Q, NN);

        err += w * S * S;
      }
    }
  }
  return sqrt(PPM->Sum(err));
}


double LagrangeElasticity::PressureError(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    if (!E.Bnd()) continue;

    Tensor Q = OrientationMatrix(c.GetData());
    for (int f = 0; f < c.Faces(); ++f) {
      if (E.Bnd(f) < 230 || E.Bnd(f) > 233) continue;
      VectorFieldFaceElement FE(u, *c, f);
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        VectorField N = FE.QNormal(q);
        Tensor NN = Product(N, N);

        Tensor F = DeformationGradient(FE, q, u);
        Tensor Fiso = eProblem.IsochoricPart(F);
        Tensor S = eProblem.GetMaterial(c).FirstPiolaKirchhoff(F, Fiso, Q);

        VectorField SN =
            S * N - eProblem.Pressure(Time(), E.QPoint(q), E.Bnd(f)) * N;
        err += w * SN * SN;
      }
    }
  }
  return sqrt(PPM->Sum(err));
}


double LagrangeElasticity::NeumannError(const Vector &u) const {
  double err = 0;
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    if (!E.Bnd()) continue;

    for (int f = 0; f < c.Faces(); ++f) {
      if (E.Bnd(f) != 2) continue;
      VectorFieldFaceElement FE(u, *c, f);
      for (int q = 0; q < FE.nQ(); ++q) {
        double w = FE.QWeight(q);
        Point N = FE.QNormal(q);

        Tensor F = DeformationGradient(FE, q, u);
        Tensor Fiso = eProblem.IsochoricPart(F);
        Tensor S = eProblem.GetMaterial(c).FirstPiolaKirchhoff(F, Fiso, One);

        VectorField SN =
            S * N - eProblem.Pressure(Time(), E.QPoint(q), E.Bnd(f)) * N;
        err += w * SN * SN;
      }
    }
  }
  return sqrt(PPM->Sum(err));
}

double LagrangeElasticity::NormPrestress() {
  return Prestress->Max();
}

double LagrangeElasticity::MaxStress(const Vector &U) const {
  double cellS{0.0};
  double maxS{0.0};

  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    cellS = 0.0;
    VectorFieldElement E(U, *c);

    Tensor Q = RotationMatrix(c.GetData());
    Tensor QT = transpose(Q);
    Tensor QI = Invert(Q);

    for (int q = 0; q < E.nQ(); ++q) {
      double w = E.QWeight(q);

      Tensor F = DeformationGradient(E, q, U);
      Tensor Fiso = eProblem.IsochoricPart(F);
      auto P = eProblem.GetMaterial(c).FirstPiolaKirchhoff(F, Fiso, One);
      cellS += w * Frobenius(P, P);
    }

    maxS = std::max(maxS, cellS);
  }

  return maxS;
}


std::array<double, 4> LagrangeElasticity::ChamberVolume(const Vector &U) const {
  std::array<double, 4> volumes{0.0, 0.0, 0.0, 0.0};

  Vector phi(U);
  for (row r = phi.rows(); r != phi.rows_end(); ++r) {
    auto pos = r();
    for (int i = 0; i < pos.SpaceDim(); ++i) {
      phi(r, i) += pos[i];
    }
  }


  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    RowBndValues r_c(phi, *c);
    if (!r_c.onBnd()) continue;

    for (int face = 0; face < c.Faces(); ++face) {
      int bc = r_c.bc(face);
      if ((bc >= PRESSURE_BC_LV) && (bc <= PRESSURE_BC_RA)) {
        if (c.plane()) {
          auto rx = phi.find_row(c.FaceCorner(face, 0));
          auto ry = phi.find_row(c.FaceCorner(face, 1));

          double v = 0.5 * det(TensorT<double, 2>(phi(rx, 0), phi(ry, 0),
                                                  phi(rx, 1), phi(ry, 1)));
          volumes[bc % 230] += v;
        } else {
          auto ra = phi.find_row(c.FaceCorner(face, 0));
          auto rb = phi.find_row(c.FaceCorner(face, 1));
          auto rc = phi.find_row(c.FaceCorner(face, 2));

          double v = 1.0 / 6.0 * det(Tensor(
              phi(ra, 0), phi(rb, 0), phi(rc, 0),
              phi(ra, 1), phi(rb, 1), phi(rc, 1),
              phi(ra, 2), phi(rb, 2), phi(rc, 2)
          ));

          volumes[bc % 230] += v;
          if (c.ReferenceType() == HEXAHEDRON) {
            auto rd = phi.find_row(c.FaceCorner(face, 3));
            v = 1.0 / 6.0 * det(Tensor(
                phi(ra, 0), phi(rc, 0), phi(rd, 0),
                phi(ra, 1), phi(rc, 1), phi(rd, 1),
                phi(ra, 2), phi(rc, 2), phi(rd, 2)
            ));
            volumes[bc % 230] += v;
          }
        }

      }
    }
  }

  for (int i = 0; i < 4; ++i) {
    volumes[i] = abs(PPM->Sum(volumes[i])) * CUBICMMtoML;
  }
  return volumes;
}

void LagrangeElasticity::GetInvariant(const Vector &u, Vector &iota4) const {
  iota4 = 0.0;
  //iota4.SetAccumulateFlag(false);
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    RowBndValues r_c(iota4, *c);

    const VectorField fibre = Direction(c.GetData());
    Tensor F = DeformationGradient(E, c(), u);
    double i4 = mat::invariant(4, F, fibre);
    for (int j = 0; j < E.NodalPoints(); ++j) {
      row r_xi = E[j];
      iota4(r_xi, 0) += i4;
      iota4(r_xi, 1) += 1.0;
    }
  }

  //TODO: Is this needed here?
  iota4.Accumulate();

  for (row r = iota4.rows(); r != iota4.rows_end(); ++r) {
    iota4(r, 0) /= iota4(r, 1);
  }
}

void LagrangeElasticity::GetInvariant(const Vector &u, Vector &iota4, Vector &iota_c) const {
  iota4 = 0.0;
  //iota4.SetAccumulateFlag(false);
  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    RowBndValues r_c(iota4, *c);

    const VectorField fibre = Direction(c.GetData());
    Tensor F = DeformationGradient(E, c(), u);
    double i4 = mat::invariant(4, F, fibre);
    iota_c(c())[0] = i4;
    for (int j = 0; j < E.NodalPoints(); ++j) {
      row r_xi = E[j];
      iota4(r_xi, 0) += i4;
      iota4(r_xi, 1) += 1.0;
    }
  }
  iota4.Accumulate();
  for (row r = iota4.rows(); r != iota4.rows_end(); ++r)
    iota4(r, 0) /= iota4(r, 1);
}

double LagrangeElasticity::InitialVolume(const Mesh &M) const {
  double volume = 0.0;
  for (cell c = M.cells(); c != M.cells_end(); ++c) {
    volume += Volume(c.ReferenceType(), c.AsVector());
  }
  return PPM->Sum(volume);
}

double LagrangeElasticity::DeformedVolume(const Vector &U) const {
  double volume = 0.0;
  for (cell c = U.cells(); c != U.cells_end(); ++c) {
    VectorFieldElement E(U, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      Tensor F = DeformationGradient(E, q, U);
      double w = E.QWeight(q);
      volume += w * det(F);
    }
  }
  return PPM->Sum(volume);
  // old version -> there is an error inside!
  /*double volume = 0.0;
for (cell c = U.cells(); c != U.cells_end(); ++c) {
  VectorFieldElement E(U, *c);

  std::vector<Point> corners(c.Corners());

  for (int i = 0; i < corners.size(); ++i) {
    Point p = c.Corner(i);
    VectorField u = E.VectorValue(p, U);
    corners[i] = Point(p[0] + u[0], p[1] + u[1], p[2] + u[2]);
  }

  volume += Volume(c.ReferenceType(), corners);
}
return PPM->Sum(volume);*/
}

void LagrangeElasticity::Project(const Vector &fine, Vector &u) const {
  LagrangeTransfer T(u, fine);
  T.Project(u, fine);
}

void LagrangeElasticity::Interpolate(const Vector &coarse, Vector &u) const {
  LagrangeTransfer T(coarse, u);
  T.Prolongate(coarse, u);

  mout.PrintInfo("Interpolation comparison", 10,
                 PrintInfoEntry("Coarse L2 Norm", L2Norm(coarse), 1),
                 PrintInfoEntry("Interpolated L2 Norm", L2Norm(u), 1),
                 PrintInfoEntry("Coarse L2 Avg Norm", L2AvgNorm(coarse), 1),
                 PrintInfoEntry("Interpolated L2 Avg Norm", L2AvgNorm(u), 1));
}

void LagrangeElasticity::PlotBoundary(const Vector &U, const Vector &scal) const {
  if (vtkplot < 1) return;
  Vector bnd(scal);
  bnd.Clear();
  Vector count(bnd);
  count.Clear();
  auto &plot = mpp::plot("Boundary");
  for (cell c = U.cells(); c != U.cells_end(); c++) {
    RowBndValues u_c(bnd, *c);
    RowBndValues u_count(count, *c);

    if (!u_c.onBnd()) continue;

    for (int i = 0; i < c.Faces(); ++i) {
      for (int j = 0; j < U.NumberOfNodalPointsOnFace(*c, i); ++j) {
        int id = U.IdOfNodalPointOnFace(*c, i, j);
        if (u_c.bc(i) > u_c(id, 0)) {
          u_c(id, 0) = (double) u_c.bc(i);
          u_count(id, 0) = 1;
        }
      }
    }
  }
  bnd.Accumulate();
  count.Accumulate();
  for (size_t i = 0; i < count.size(); i++) {
    if (count[i] > 0) {
      bnd[i] /= count[i];
    }
  }
  plot.AddData("Boundary", bnd, 0);

  bnd = 0;
  for (cell c = U.cells(); c != U.cells_end(); c++) {
    RowBndValues u_c(bnd, *c);
    if (!u_c.onBnd()) continue;

    for (int i = 0; i < c.Faces(); ++i) {
      for (int j = 0; j < U.NumberOfNodalPointsOnFace(*c, i); ++j) {
        int id = U.IdOfNodalPointOnFace(*c, i, j);
        u_c(id, 0) = std::max(u_c(id, 0), u_c.bc(i) == 199 ? 1.0 : 0.0);
      }
    }
  }
  plot.AddData("Fixation", bnd);

  plot.PlotFile("Boundary");
}

void LagrangeElasticity::SetDisplacement(Vector &u) const {
  for (row r = u.rows(); r != u.rows_end(); ++r) {
    VectorField d = eProblem.Deformation(Time(), r());
    for (int k = 0; k < u.dim(); ++k) {
      u(r, k) = d[k];
    }
  }
}

void LagrangeElasticity::UpdateStretch(const Vector &gamma_f) const {
  //Quarteroni
    if ((name == "Quarteroni") || name == "Quarteroni2") {
      for (row r = gamma->rows(); r != gamma->rows_end(); ++r) {
        double g = gamma_f(r, 0);
        if (contains(eProblem.Name(),"Land")) {
          Config::Get("g", g);
        }
        (*gamma)(r, 0) = g;
      }
    } else if (name == "") { //Rossi
      for (row r = gamma->rows(); r != gamma->rows_end(); ++r) {
        double g = gamma_f(r, 0);
        if (contains(eProblem.Name(),"Land")) {
          Config::Get("g", g);
        }
        (*gamma)(r, 0) = g;
        (*gamma)(r, 2) = factor_gn * g;
        // To ensure det(FA)=1  [See Bonet et al.(2019)]
        (*gamma)(r, 1) = (1.0 / (1.0 + g)) * (1.0 / (1.0 + factor_gn * g)) - 1.0;
      }
    }
  }

void LagrangeElasticity::UpdateStretch(const Vector &gamma_f, const Vector &gamma_f_c) const {
  //TODO: Remove (g_min and g_max are for debugging!)
  double g_min = 0;
  double g_max = 0;
  if ((name == "Quarteroni") || name == "Quarteroni2") {
    for (row r = gamma->rows(); r != gamma->rows_end(); ++r) {
      double g = gamma_f(r, 0);
      if (contains(eProblem.Name(), "Land")) {
        Config::Get("g", g);
      }
      (*gamma)(r, 0) = g;
    }
    for (row r = gamma_c->rows(); r != gamma_c->rows_end(); ++r){
      double g_c = gamma_f_c(r,0);
      if (contains(eProblem.Name(), "Land")) {
        Config::Get("g_c", g_c);
      }
      (*gamma_c)(r, 0) = g_c;
      g_min = std::min(g_c,g_min);
      g_max = std::max(g_c,g_max);            
    }
  } else if (name == "") { //Rossi
    for (row r = gamma->rows(); r != gamma->rows_end(); ++r) {
      double g = gamma_f(r, 0);
      if (contains(eProblem.Name(),"Land")) {
        Config::Get("g", g);
      }
      (*gamma)(r, 0) = g;
      (*gamma)(r, 2) = factor_gn * g;
      // To ensure det(FA)=1  [See Bonet et al.(2019)]
      (*gamma)(r, 1) = (1.0 / (1.0 + g)) * (1.0 / (1.0 + factor_gn * g)) - 1.0;
    }
    for (row r = gamma_c->rows(); r != gamma_c->rows_end(); ++r) {
      double g_c = gamma_f_c(r, 0);
      if (contains(eProblem.Name(), "Land")) {
        Config::Get("g_c", g_c);
      }
      (*gamma_c)(r, 0) = g_c;
      (*gamma_c)(r, 2) = factor_gn * g_c;
      // To ensure det(FA)=1  [See Bonet et al.(2019)]
      (*gamma_c)(r, 1) = (1.0 / (1.0 + g_c)) * (1.0 / (1.0 + factor_gn * g_c)) - 1.0;
    }
  }
  g_min = PPM->Min(g_min);
  g_max = PPM->Min(g_max);

  //mout << "gamma_c in [" << g_min << "," << g_max << "]" << endl;
}

void LagrangeElasticity::SetInitialValue(Vector &u) {
  IElasticity::SetInitialValue(u);
}


void LagrangeElasticity::Initialize(const cell &c, Vector &U) const {
  RowBndValues u_c(U, *c);
  if (!U.OnBoundary(*c)) return;
  for (int face = 0; face < c.Faces(); ++face) {
    switch (u_c.bc(face)) {
      case 1:
        dirichletBnd(c, U, u_c, face);
        break;
      case 100:
        planeFixationBnd(c, U, u_c, face, 0);
        break;
      case 101:
        planeFixationBnd(c, U, u_c, face, 1);
        break;
      case 102:
        planeFixationBnd(c, U, u_c, face, 2);
        break;
      case 199:
        fixationBnd(c, U, u_c, face);
        break;
    }
  }
}

void LagrangeElasticity::MassMatrix(Matrix &massMatrix) const {
  for (cell c = massMatrix.cells(); c != massMatrix.cells_end(); ++c) {
    VectorFieldElement elem(massMatrix, *c);
    RowEntries M_c(massMatrix, elem);
    for (int q = 0; q < elem.nQ(); ++q) {
      double w = elem.QWeight(q);
      for (int i = 0; i < elem.NodalPoints(); ++i) {
        for (int k = 0; k < c.dim(); ++k) {
          auto u_ik = elem.VectorComponentValue(q, i, k);
          for (int j = 0; j < elem.NodalPoints(); ++j) {
            for (int l = 0; l < c.dim(); ++l) {
              auto u_jl = elem.VectorComponentValue(q, j, l);
              M_c(i, j, k, l) += w * rho * (u_ik * u_jl);
            }
          }
        }
      }
    }
  }
}

void LagrangeElasticity::Initialize(Vectors &vecs) const {
  Vector &displacement = vecs[0];
  Vector &velocity = vecs[1];
  Vector &acceleration = vecs[2];

  for (row r = vecs.rows(); r != vecs.rows_end(); ++r) {
    VectorField d = eProblem.Deformation(FirstTStep(), r());
    VectorField v = eProblem.Velocity(FirstTStep(), r());
    VectorField a = eProblem.Acceleration(FirstTStep(), r());
    for (int k = 0; k < vecs.dim(); ++k) {
      displacement(r, k) += d[k];
      velocity(r, k) = v[k];
      acceleration(r, k) = a[k];
    }
  }
}

void LagrangeElasticity::TractionStiffness(Matrix &matrix) const {
  matrix = 0;
  for (cell c = matrix.cells(); c != matrix.cells_end(); ++c) {
    VectorFieldElement E(matrix, *c);
    RowEntries A_c(matrix, E);

    for (int f = 0; f < c.Faces(); ++f) {
      VectorFieldFaceElement faceElem(matrix, *c, f);
      if (matrix.OnBoundary(*c, f)) {
        int bc = matrix.GetMesh().BoundaryFacePart(c.Face(f));

        if ((bc >= ROBIN_BC_EPI) && (bc <= ROBIN_BC_BASE)) {
          for (int q = 0; q < faceElem.nQ(); ++q) {
            VectorField n = faceElem.QNormal(q);
            Tensor N = Product(n, n);

            double w = faceElem.QWeight(q);
            for (int i = 0; i < faceElem.NodalPoints(); ++i) {
              for (int k = 0; k < c.dim(); ++k) {
                auto Nphi_ik = N * faceElem.VectorComponentValue(q, i, k);
                for (int j = 0; j < faceElem.NodalPoints(); ++j) {
                  for (int l = 0; l < c.dim(); ++l) {
                    VectorFieldComponent phi_jl = faceElem.VectorComponentValue(q, j, l);
                    A_c(i, j, k, l) += w * eProblem.TractionStiffness(Time(), faceElem.QPoint(q),
                                                                      bc) * Nphi_ik * phi_jl;
                  }
                }
              }
            }
          }

        }

      }
    }
  }
}

void LagrangeElasticity::EvaluateTractionStiffness(Vector &u) const {
  double s_max = 0;
  double s = 0;

  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);

    for (int f = 0; f < c.Faces(); ++f) {
      VectorFieldFaceElement faceElem(u, *c, f);
      if (u.OnBoundary(*c, f)) {
        for (int face = 0; face < c.Faces(); ++face) {
          int bc = u.GetMesh().BoundaryFacePart(c.Face(f));

          if ((bc >= ROBIN_BC_EPI) && (bc <= ROBIN_BC_BASE)) {
            for (int q = 0; q < faceElem.nQ(); ++q) {
              double Un = faceElem.QNormal(q) * faceElem.VectorValue(q,u);
              double w = faceElem.QWeight(q);

              s_max = std::max(s_max, abs(Un));
              s += w * Un * Un;
            }
	        }
	      }
      }
    }
  }
}

void LagrangeElasticity::TractionViscosity(Matrix &matrix) const {
  matrix = 0;
  for (cell c = matrix.cells(); c != matrix.cells_end(); ++c) {
    VectorFieldElement E(matrix, *c);
    RowEntries A_c(matrix, E);

    for (int f = 0; f < c.Faces(); ++f) {
      VectorFieldFaceElement faceElem(matrix, *c, f);
      if (matrix.OnBoundary(*c, f)) {
        int bc = matrix.GetMesh().BoundaryFacePart(c.Face(f));

        if ((bc >= ROBIN_BC_EPI) && (bc <= ROBIN_BC_BASE)) {
          for (int q = 0; q < faceElem.nQ(); ++q) {
            VectorField n = faceElem.QNormal(q);
            Tensor N = Product(n, n);

            double w = faceElem.QWeight(q);
            for (int i = 0; i < faceElem.NodalPoints(); ++i) {
              for (int k = 0; k < c.dim(); ++k) {
                auto Nphi_ik = N * faceElem.VectorComponentValue(q, i, k);
                for (int j = 0; j < faceElem.NodalPoints(); ++j) {
                  for (int l = 0; l < c.dim(); ++l) {
                    VectorFieldComponent phi_jl = faceElem.VectorComponentValue(q, j, l);
                    A_c(i, j, k, l) += w * eProblem.TractionViscosity(Time(), faceElem.QPoint(q),
                                                                      bc) * Nphi_ik *phi_jl;
                  }
                }
              }
            }
          }

        }
      }
    }
  }
}


std::pair<double, double> LagrangeElasticity::detF(const Vector &u) const {
  double Jmax = 1.0;
  double Jmin = 1.0;

  for (cell c = u.cells(); c != u.cells_end(); ++c) {
    VectorFieldElement E(u, *c);
    for (int q = 0; q < E.nQ(); ++q) {
      Tensor DU = E.VectorGradient(q, u);
      double J = det(One + DU);
      Jmin = std::min(Jmin, J);
      Jmax = std::max(Jmax, J);
    }
  }
  Jmin = PPM->Min(Jmin);
  Jmax = PPM->Max(Jmax);

  return {Jmin, Jmax};
}