#include "MultiElasticity.hpp"


MultiElasticity::MultiElasticity() : main(), initialLevel(main.mlevel) {
  Config::Get("ReferenceLevel", referenceLevel);
  Config::Get("ReferenceVerbose", verbose);
  Config::Get("InterpolateStartVector", useReference);
  solutions.resize(referenceLevel - initialLevel + 1);
}


void MultiElasticity::Initialize() {
  // Initialize MainElasticity in src/elasticity/MainElasticity.cpp
  main.Initialize();
}

Vector &MultiElasticity::Run() {
  for (int level = initialLevel; level <= referenceLevel; ++level) {
    mout << "\n=================== Running level " << level << "================ " << endl;
    main.ResetLevel(level);
    main.Initialize();
    bool useCoarse =  (useReference && level > 0);
    if(useCoarse)
      main.pressureSteps = 1;

    const auto &solution = useCoarse ? main.Run(solutions[level - 1]()) : main.Run();
    main.Evaluate(solution);
    solutions[level] = main.MoveVector();
  }

  return solutions[referenceLevel]();
}

std::vector<double> MultiElasticity::Evaluate(const Vector &solution) {
  for (int level = initialLevel; level < referenceLevel; ++level) {
    main.EvaluateReferece(solutions[level](), solution);
  }
  mout << endl << "=============================================================" << endl << endl;
  return {};
}
