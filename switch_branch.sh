#!/usr/bin/env bash
echo 'Removing all local changes'
git checkout -- .
echo 'Fetching from git...'
git fetch
git checkout debugging-multipart
git pull
git submodule update
echo 'Building Project...'
cd build
make -j
