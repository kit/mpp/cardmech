## Getting Started
M++ is a parallel PDE solving software requiring some preinstalled packages. This guide hopefully walks you through the installation process.

### Prerequisites
This version of M++ uses CMake as building tool. If you haven't installed CMake Version 3.15 or higher, find out about
the installation process on https://cmake.org/download/.

Furthermore, you need the packages BLAS (Basic Linear Algebra Subroutines) and LAPACK (Linear Algebra PACKage). Find out
more on https://www.netlib.org/blas/ and https://www.netlib.org/lapack/.

Most importantly, M++ is a is a parallel PDE solving software. In order to distribute your computations on parallel processes you need to have Open MPI (Message Passing Interface) installed.
If not installed, get a version of Open MPI (e.g. 2.1) on https://www.open-mpi.org/software/ompi/v2.1/ and follow the installation instructions.

#### On Unix
If you are using a Unix based system, you can simply run:

```sudo apt install cmake```

```sudo apt install libblas-dev liblapack-dev```

```sudo apt install openmpi-bin libopenmpi-dev```

### Installing M++
This Project uses the M++Core to run cardiac electromechanical simulations. To install it and the M++ core, use the command

```git clone --recurse-submodules https://gitlab.kit.edu/kit/mpp/cardmech.git``` or

```git clone --recurse-submodules git@gitlab.kit.edu:kit/mpp/cardmech.git``` if you have a SSH key.

Then change into the project root, create a new build folder with ```mkdir build```, go into this folder and run ```cmake ..```.
CMake generates a Makefile tailored to your system. Run ```make -j``` to finally build the project.

### Running M++
You can run the predefined code by using the command

```./M++```

or, to use multiple cores, 

```mpirun -n N M++```,

where N is the number of cores you want to use.

### Updating M++
If you want to update the project to its latest release, simply pull the repository by using

```git pull --recurse-submodules```.

This will automatically update the M++Core as well as all used submodules as well.
