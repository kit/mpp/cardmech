import numpy as np

if __name__ == "__main__":
    matrix = np.array([
        [1, 0, 0, 0, 0],
        [-1 / 6, 2 / 3, 0, -1 / 3, -1 / 6],
        [0, 0, 10, 0, 0],
        [0, 0, 0, 1, 0],
        [-1 / 3, -1 / 6, 0, -1 / 6, 2 / 3]])

    print(np.linalg.inv(matrix))
