
import math
import matplotlib.pyplot as plt
from utility import plotting as plotting
from utility import reading as read
from utility import computing as comp
from utility.latexTables import Table

folderPath ='../../../datafiles/convergenceTest/SpaceTimeHexaTest025t0001BRTwoDimLinearImplicit/'
case ='2Dim'
logPath = folderPath + 'log/'
dataPath = folderPath + 'data/'
plotPath = folderPath + 'plot/'
latexPath = folderPath + 'tex/'
lList = [0,1,2,3,4,5]
tlist = [0,1,2,3,4]
start_dx = 0.25

listEvaluationPoints =[1,2,3,4,5,6,7,8]
totalNumberOfEvaluationPointsWithNorms = 9
substep=1
cutT= 0.3



def WriteLatexSpace(tab):
    with open(latexPath +'SpaceTableSdt'+str(timename(0))+'m'+str(substep)+case +'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSpaceLongtable())
            for p in listEvaluationPoints:
                (time,V) = read.getDataFromTXT(dataPath,read.createFilename(lList[-1],tlist[-1],substep),p)
                output_file.write(tab.writeSpaceData(V,p,totalNumberOfEvaluationPointsWithNorms))
            output_file.write(tab.writeEndTable())

def WriteLatexTime(tab):
    with open(latexPath +'TimeTableSdt'+str(timename(0))+'m'+str(substep)+case +'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginTimeLongtable())
            for p in listEvaluationPoints:
                (time,V) = read.getDataFromTXT(dataPath,read.createFilename(lList[-1],tlist[-1],substep),p)
                output_file.write(tab.writeTimeData(V,p,totalNumberOfEvaluationPointsWithNorms))
            output_file.write(tab.writeEndTable())

def LatexTables():
    tab=Table(lList, tlist,start_dt,T,substep,folderPath)
    WriteLatexSpace(tab)
    WriteLatexTime(tab)
    
def pointsInPlotForFixlAndjAndm(l,j,m):
    for p in listEvaluationPoints[:(totalNumberOfEvaluationPointsWithNorms-2)]:
        (time,V)= read.getDataFromTXT(dataPath,read.createFilename(l,j,m),p)
        for i in range(len(V)):
            V[i] = float(V[i])
        labelname = 'P'+str(p)
        #print(V)
        if j>1:
            (time,V)=plotting.resizeData(time,V,100,len(time))  #such that the tex file is not too big...
        plt.plot(time,V, label = labelname)

    
    #plt.title('BeelerReuter '+', l='+str(l)+ ', dt='+str(start_dt/(2**j))+ ', m= '+str(m))
    plotname ='BRl' +str(l)+ 'j'+str(j)+'m'+str(m)+'AllPoints'
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential (mV)')
    plt.legend()
    import tikzplotlib
    tikzplotlib.save(latexPath + plotname + ".tex")
    plt.savefig(plotPath + plotname + '.png')
    #plt.show()  
    plt.close()
def plotSpaceForPointp(p,j,m):
    for l in lList:
        (time,V)= read.getDataFromTXT(dataPath,read.createFilename(l,j,m),p)
        for i in range(len(V)):
            V[i] = float(V[i])
        labelname ='$\\ell = ' + str(l) +'$'
        #labelname = '${\\vartriangle}x='+str(start_dx/(2**l))+'$ mm'
        #print(V)
        (time,V)=plotting.resizeData(time,V,100,len(time))  #such that the tex file is not too big...
        plt.plot(time,V, label = labelname)
        
    #plt.title('Space convergence for BeelerReuter with dt='+str(start_dt/(2**j))+ ', m= '+str(m)+ 'in P'+str(p))
    plotname ='BRSpaceConvergence'+ 'j'+str(j)+'m'+str(m)+'P'+str(p)
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential (mV)')
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    #plt.legend()
    #import tikzplotlib
    #tikzplotlib.save(latexPath + plotname + ".tex")
    #plt.savefig(plotPath + plotname + '.png')
    plt.show()  
    plt.close()
def plotTimeForPointp(p,l,m):
    for j in tlist:
        (time,V)= read.getDataFromTXT(dataPath,read.createFilename(l,j,m),p)
        for i in range(len(V)):
            V[i] = float(V[i])
        labelname = '$j = '+str(j) + '$'
        #labelname = '${\\vartriangle}t='+str(1000.0*start_dt/(2**j))+'$ ms'
        #print(V)
        if j>1:
            (time,V)=plotting.resizeData(time,V,100,len(time))  #such that the tex file is not too big...
        plt.plot(time,V, label = labelname)
        
    #plt.title('Time convergence for BeelerReuter with dx='+str(start_dx/(2**l))+ ', m= '+str(m)+ 'in P'+str(p))
    plotname ='BRTimeConvergence'+ 'l'+str(l)+'m'+str(m)+'P'+str(p)
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential (mV)')
    #plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.legend()
    import tikzplotlib
    tikzplotlib.save(latexPath + plotname + ".tex")
    plt.savefig(plotPath + plotname + '.png')
    #plt.show()  
    plt.close()  

def convergencePlotTime(l):
    print("Plot Time convergence")
    for p in listEvaluationPoints:
        error=[]
        time =[]
        for j in range(len(tlist)-1):
            time.append(1000.0*start_dt/(2**j))
            error.append(float(comp.L2Diff(dataPath,l,j,substep,l,j+1,substep,p,start_dt,T)))
        if p<=totalNumberOfEvaluationPointsWithNorms-2:
            labelname = 'P'+str(p)
        elif p==totalNumberOfEvaluationPointsWithNorms-1:
            labelname = '$\\L2$'
        elif p==totalNumberOfEvaluationPointsWithNorms:
            labelname = 'H1'
        plt.plot(time,error,'s-', markersize=5,label=labelname)
    order = time
    order =[2*i for i in order]
    plt.plot(time,order,'k--',linewidth=3.5,label ='order 1')
    order =[15*i*i for i in order]
    #print(order)
    plt.plot(time,order,'r--',linewidth=3.5,label ='order 2')
    plt.xlabel('${\\vartriangle}t$ in ms')
    plt.ylabel('$\\L2$ distance ')
    #plt.title('For Beeler Reuter and ${\\vartriangle}x='+str(start_dx/(2**l))+'$ mm')
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.yscale('log', basey=2)
    plt.xscale('log', basex=2)
    plotname ='ConvergenceTime'  + 'l'+ str(l)+'i'+str(substep)
    plt.savefig(plotPath + plotname + '.png')
    
    import tikzplotlib
    tikzplotlib.save(latexPath + plotname + ".tex")
    #plt.show()
    plt.close()
   
   
def convergencePlotSpace(j):
    timename = '{:.10f}'.format(start_dt/(2**j)).rstrip("0") [2:]
    print("Plot Space convergence")
    for p in listEvaluationPoints:
        error=[]
        space =[]
        for l in range(lList[0],len(lList)+lList[0]-1):
            space.append(start_dx/(2**l))
            error.append(float(comp.L2Diff(dataPath,l,j,substep,l+1,j,substep,p,start_dt,T)))
        if p<=totalNumberOfEvaluationPointsWithNorms-2:
            labelname = 'P'+str(p)
        elif p==totalNumberOfEvaluationPointsWithNorms-1:
            labelname = '$\\L2$'
        elif p==totalNumberOfEvaluationPointsWithNorms:
            labelname = 'H1'
        plt.plot(space,error,'s-', markersize=5,label=labelname)
    order = space
    plt.plot(space,order,'k--',linewidth=3.5, label='order 1')
    plt.xlabel('${\\vartriangle}x$ in mm')
    plt.ylabel('$\\L2$ distance')
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    
    plotname ='ConvergenceSpace'+ 'j'+str(j)+'m'+str(substep)
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    #plt.title('For Beeler Reuter and ${\\vartriangle}t='+str(1000.0*start_dt/(2**j))+'$ ms')
    plt.yscale('log', basey=2)
    plt.xscale('log', basex=2)
    plt.savefig(plotPath + plotname + '.png')
    #plt.show()
    
    import tikzplotlib
    tikzplotlib.save(latexPath + plotname + ".tex")
    
    plt.close()
    
if __name__=="__main__":
    #extracts data from logfile into txt file
    #read.writeDataInFile(dataPath,logPath,lList,tlist,substep)
    (T,start_dt)=read.getTimeData(logPath,lList[0],tlist[0],substep)
    #pointsInPlotForFixlAndjAndm(lList[0],tlist[0],1)
    #pointsInPlotForFixlAndjAndm(lList[-1],tlist[-1],1)
    #plotSpaceForPointp(9,tlist[-1],1)
    #plotTimeForPointp(9,lList[-1],1)
    #plotSpaceForPointp(6,tlist[-1],1)
    #plotTimeForPointp(6,lList[-1],1)
    #plotSpaceForPointp(3,tlist[-1],1)
    #plotTimeForPointp(3,lList[-1],1)
    #convergencePlotTime(lList[-1])
    #convergencePlotSpace(tlist[-1])
    LatexTables()
