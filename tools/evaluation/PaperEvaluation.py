#import numpy as np
#import math
import matplotlib.pyplot as plt
from utility import reading as read
from utility import computing as comp
from utility.plotting import Plot 
#from utility import plotting as plotting
from utility.latexTables import Table
from utility.latexTables import TableWriter
from utility.documentWriter import DW

def getPointNamebyNumber(p,inRef=False):
        if inRef:
            if p==1:
                return '\\peins'
            elif p==2:
                return '\\pzwei'
            elif p==3:
                return '\\pdrei'
            elif p==4:
                return '\\pvier'
            elif p==5:
                return '\\pfuenf'
            elif p==6:
                return '\\psechs'
            elif p==7:
                return '\\psieben'
            elif p==8:
                return '\\LtwoSpace'
        else:    
            if p==1:
                return '\\Peins'
            elif p==2:
                return '\\Pzwei'
            elif p==3:
                return '\\Pdrei'
            elif p==4:
                return '\\Pvier'
            elif p==5:
                return '\\Pfuenf'
            elif p==6:
                return '\\Psechs'
            elif p==7:
                return '\\Psieben'
            elif p==8:
                return '\\LtwoSpace'
def  convertPath(fL,n):
    newList=[]
    for file in fL:
        f=''
        if n<0:
            f=file[(abs(n)*3):]
        elif n>0:
            for i in range(n):
                f+='../'
            f+=file
        else:
            f=file
        newList.append(f)
    return newList

def comparisonOfQuadrature():
    lL=[2,3,4]
    tL=[0,1,2,3]
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7]
    eP='../../../datafiles/TimeAsymptoticTest/TestEllipsoid3dExcitation/'
    fSI=eP+'SI0001SAT/'
    fSINodes=eP+'SI0001SATNodes/'
    fIE=eP+'IE0001SAT/'
    
    fL=[fSI,fSINodes]
    nL=['\\SISVI','\\SIICI']
    #for file in fL:
        #read.writeDataInFile(file,lL,tL,1,False)
    
    tab=Table(lL,tL,sdt,endTime,1,fL)
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        filename=eP+'QuadTest/'+'tex/'+'SchemeComparisonValuesSVIICITabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Comparison of SVI and ICI approach for SI on different levels and time step sizes for $\\|\\V^{j,\\ell}(\\cdot, \\textbf{P'+str(p)+'})\\|_\\Ltwonorm$'
            label='\\label{tab:QuadTestP'+str(p)+'}'
            output_file.write(tab.writeBeginQuadValueTable(p,caption,label))
            output_file.write(tab.writeValueSchemeCompQuadTable(p,nL))
            output_file.write(tab.writeEndTable())
    for p in listEvaluationPoints:
        #plot.plotInP(p,lL[0],tL[0],fL,nL,eP+'QuadTest/')
        #plot.plotInP(p,lL[0],tL[-1],fL,nL,eP+'QuadTest/',True,1)
        plot.plotVTwoTimeIntMethodsTime(fSI,fSINodes,p,2,'',eP+'QuadTest/')
        plot.plotVTwoTimeIntMethodsTime(fSI,fSINodes,p,3,'',eP+'QuadTest/')
        plot.plotVTwoTimeIntMethodsTime(fSI,fSINodes,p,4,'',eP+'QuadTest/')
        plot.plotVTwoTimeIntMethodsSpace(fSI,fSINodes,p,2,eP+'QuadTest/')
        plot.plotVTwoTimeIntMethodsSpace(fSI,fSINodes,p,3,eP+'QuadTest/')
        
    
def comparisonLISI():
    lL=[2,3,4]
    tL=[0,1,2,3]
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    eP='../../../datafiles/TestEllipsoid3d/'
    fSI=eP+'SI0001SAT/'
    fLI=eP+'LI0001SAT/'
    
    fL=[fSI,fLI]
    nL=['\\SISVI','\\LISVI']
    tab=Table(lL,tL,sdt,endTime,1,fL)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'SchemeComparisonValuesSILITabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Comparison of semi-implicit (\\SISVI) and linearization (\\LISVI) approach for the componentwise splitting on different levels and time step sizes for $\\|\\V^{j,\\ell}(\\cdot, \\textbf{P'+str(p)+'})\\|_\\Ltwonorm$'
            label='\\label{tab:SILITestP'+str(p)+'}'
            output_file.write(tab.writeBeginQuadValueTable(p,caption,label))
            output_file.write(tab.writeValueSchemeCompQuadTable(p,nL,True))
            output_file.write(tab.writeEndTable())
    
def SiLIDiffConvergence():
    lL=[3,4,5]
    tL=[1,2,3,4,5]
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    eP='../../../datafiles/TestEllipsoid3d/'
    fSI=eP+'SI0001SAT/'
    fLI=eP+'LI0001SAT/'
    
    fL=[fSI,fLI]
    nL=['\\SISVI','\\LISVI']
    tab=Table(lL,tL,sdt,endTime,1,fL)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'NewConDiffSILITabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Substracting the approximations of the semi-implicit (\\SISVI) and linearization (\\LISVI) approach for the component-wise splitting on different levels and time step sizes for $\\|\\V^{j,\\ell}_{\\SISVI}(\\cdot, \\textbf{P'+str(p)+'})-\\V^{j,\\ell}_{\\LISVI}(\\cdot, \\textbf{P'+str(p)+'})\\|_\\Ltwonorm$'
            label='\\label{tab:SILITestP'+str(p)+'}'
            output_file.write(tab.writeBeginQuadValueTable(p,caption,label))
            output_file.write(tab.writeCompareSILI(p,fL))
            output_file.write(tab.writeEndTable())
def PlotDiffSILI():
    lL=[3,4,5]
    tL=[1,2,3,4,5]
    tL=[3,4]
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    eP='../../../datafiles/TestEllipsoid3d/'
    fSI=eP+'SI0001SAT/'
    fLI=eP+'LI0001SAT/'
    
    fL=[fSI,fLI]
    nL=['\\SISVI','\\LISVI']        
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=2
    for p in listEvaluationPoints:
         #plot.plotVTwoTimeIntMethodsSpace(fSI,fLI,p,j,eP,'DiffSILISpacej'+str(j)+'P'+str(p))
         plot.plotVTwoTimeIntMethodsTimeRef(fSI,fLI,p,3,fLI,6,4,eP,'DiffSILITimel3'+'P'+str(p))
    
def SiLIDiffConvergenceMaxNorm():
    lL=[3,4,5]
    tL=[1,2,3,4,5]
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    eP='../../../datafiles/TestEllipsoid3d/'
    fSI=eP+'SI0001SAT/'
    fLI=eP+'LI0001SAT/'
    
    fL=[fSI,fLI]
    nL=['\\SISVI','\\LISVI']
    tab=Table(lL,tL,sdt,endTime,1,fL)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'MaxNormConDiffSILITabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Substracting the approximations of the semi-implicit (\\SISVI) and linearization (\\LISVI) approach for the component-wise splitting on different levels and time step sizes for $\\|\\V^{j,\\ell}_{\\SISVI}(\\cdot, \\textbf{P'+str(p)+'})-\\V^{j,\\ell}_{\\LISVI}(\\cdot, \\textbf{P'+str(p)+'})\\|_\\infty$'
            label='\\label{tab:MaxSILITestP'+str(p)+'}'
            output_file.write(tab.writeBeginQuadValueTable(p,caption,label))
            output_file.write(tab.writeCompareSILI(p,fL,True))
            output_file.write(tab.writeEndTable())
def ValueTableSI():
    eP='../../../datafiles/TestEllipsoid3d/'
    fSI=eP+'SI0001SAT/'
    read.writeDataInFile(fSI,[5],[5],1)
    read.writeDataInFile(fSI,[3],[5],1)
    #read.writeDataInFile(fSI,[5],[4],1)
    #read.writeDataInFile(fSI,[6],[5],1)
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    
    lL=[2,3,4,5,6]
    tL=[0,1,2,3,4,5]
    fL=[fSI]
    nL=['\\SISVI']
    tab=Table(lL,tL,sdt,endTime,1,fL[0])
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        filename=fL[0]+'tex/'+'SIValueTabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Value table of semi-implicit (\\SISVI)  approach for the component-wise splitting on different levels and time step sizes for $\\|\\V^{j,\\ell}(\\cdot, \\textbf{P'+str(p)+'})\\|_\\Ltwonorm$'
            label='\\label{tab:SIValueP'+str(p)+'}'
            output_file.write(tab.writeBeginQuadValueTable(p,caption,label))
            output_file.write(tab.writeValueColums(p,False))
            output_file.write('\\end{tabular}\n')
            output_file.write(tab.writeEndTable())
    
        #plot.plotTimeForPointpWithRef(fSI,p,4,1,6,5,1)
def DifferenceTableSI(which=''):
    eP='../../../datafiles/TestsFromHoreka/'
    fSI = eP+'SI0001SAT/'
    fLI=eP+'LI0001SAT/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[6,7]
    #read.writeDataInFile(fSI,[5],[6],1)
    
    lL=[4,5,6]
    tL=[3,4,5,6]
    if which=='SIandLI':
        fL=[fLI,fSI]
        tabWriter = TableWriter(lL,tL,sdt,0.02963125,1,fL,listEvaluationPoints)
        tabWriter.SpaceAndTimeDiffList(4,5,eP,['\\SI','\\LI'])
    else:
        fL=[fSI]
        tabWriter = TableWriter(lL,tL,sdt,endTime,1,fL,listEvaluationPoints)
        tabWriter.SpaceAndTimeDiff(4,5)
    
def DifferenceTable():
    eP='../../../datafiles/TestEllipsoid3d/'
    fLI=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    #read.writeDataInFile(fSI,[4],[5],1)
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    
    
    fL=[fLI,fSI]
    nL=['LI','SI']
    lL=[3,4,5,6]
    tL=[2,3,4,5]
    tabWriter =TableWriter(lL, tL,sdt,endTime,1,fL,listEvaluationPoints)
    #tabWriter.SpaceDiffTable(nL)
    
    lL=[3,4,5]
    tL=[2,3,4,5]
    tabWriter =TableWriter(lL, tL,sdt,endTime,1,fL,listEvaluationPoints)
    #tabWriter.TimeDiffTable(nL)
    
    lL=[3,4,5]
    tL=[3,4,5]
    EvalPoints=[6,7,8]
    tabWriter =TableWriter(lL, tL,sdt,endTime,1,fL,EvalPoints)
    #tabWriter.DiagDiffTable(nL)
    
    
    lL=[3,4,5,6]
    tL=[2,3,4,5]
    tabWriter =TableWriter(lL, tL,sdt,endTime,1,fL,listEvaluationPoints)
    tabWriter.SpaceTimeTogether(nL)
    
def MaxNormTable():
    eP='../../../datafiles/TestEllipsoid3d/'
    fSI=eP+'SI0001SAT/'
    #read.writeDataInFile(fSI,[4],[4],1)
    #read.writeDataInFile(fSI,[5],[4],1)
    #read.writeDataInFile(fSI,[6],[5],1)
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    
    lL=[2,3,4,5,6]
    tL=[0,1,2,3,4,5]
    fL=[fSI]
    nL=['\\SISVI']
    tab=Table(lL,tL,sdt,endTime,1,fL[0])
    for p in listEvaluationPoints:
        filename=fL[0]+'tex/'+'SIValueTabMaxP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Value table of semi-implicit (\\SISVI)  approach for the componentwise splitting on different levels and time step sizes for $\\|\\V^{j,\\ell}(\\cdot, \\textbf{P'+str(p)+'})\\|_\\infty$'
            label='\\label{tab:SIValueMaxNormP'+str(p)+'}'
            output_file.write(tab.writeBeginSpaceLengthTable(p,caption,label))
            output_file.write(tab.writeValueMaxNorm(p))
            output_file.write(tab.writeEndTable())
def CompareWithRef():
    eP='../../../datafiles/TestEllipsoid3d/'
    fSI=eP+'SI0001SAT/'
    #read.writeDataInFile(fSI,[4],[4],1)
    #read.writeDataInFile(fSI,[5],[4],1)
    #read.writeDataInFile(fSI,[6],[5],1)
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    
    lL=[3,4,5]
    tL=[2,3,4,5]
    fL=[fSI]
    nL=['\\SISVI']
    tab=Table(lL,tL,sdt,endTime,1,fL[0])
    for p in listEvaluationPoints:
        filename=fL[0]+'tex/'+'SICompRefTabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Value table of semi-implicit (\\SISVI)  approach for the component wise splitting on different levels and time step sizes for $\\frac{\\|\\V^{j,\\ell}(\\cdot, \\textbf{P'+str(p)+'})- \\V^{5,6}(\\cdot,\\textbf{P'+str(p)+'})\\|_\\Ltwonorm}{\\|\\V^{5,6}(\\cdot,\\textbf{P'+str(p)+'})\\|_\\Ltwonorm}$'
            label='\\label{tab:SICompRefP'+str(p)+'}'
            output_file.write(tab.writeBeginSpaceLengthTable(p,caption,label))
            output_file.write(tab.writeCompareRef(p,5,6))
            output_file.write(tab.writeEndTable())
def ValueTableLI():
    eP='../../../datafiles/TestEllipsoid3d/'
    fLI=eP+'LI0001SAT/'
    #for i in range(1,6):
        #read.writeDataInFile(fLI,[3],[i],1)
        #read.writeDataInFile(fLI,[4],[i],1)
    #read.writeDataInFile(fLI,[5],[2],1)
    #read.writeDataInFile(fLI,[5],[3],1)
    #read.writeDataInFile(fLI,[5],[4],1)
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    
    lL=[3,4,5,6]
    tL=[2,3,4,5]
    fL=[fLI]
    nL=['\\LISVI']
    tab=Table(lL,tL,sdt,endTime,1,fL[0])
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        filename=fL[0]+'tex/'+'LIValueTabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Value table of linearized (\\LISVI)  approach for the component-wise splitting on different levels and time step sizes for $\\|\\V^{j,\\ell}(\\cdot, \\textbf{P'+str(p)+'})\\|_\\Ltwonorm$'
            label='\\label{tab:LIValueP'+str(p)+'}'
            output_file.write(tab.writeBeginSpaceLengthTable(p,caption,label))
            output_file.write(tab.writeValueColums(p,False))
            output_file.write('\\end{tabular}\n')
            output_file.write(tab.writeEndTable())
        plot.plotTimeForPointpWithRef(fLI,p,4,1,6,4,1,[0.01,0.013])
        plot.plotSpaceForPointpWithRef(fLI,p,3,1,6,4,1,[0.01,0.013])
    plot=Plot(lL,tL,sdt,endTime,1,[4,5,6,7])
    plot.pointsInPlotForFixlAndjAndm(fL[0],4,3,1,1)
def ValuePlotLI():
    eP='../../../datafiles/TestEllipsoid3d/'
    fLI=eP+'LI0001SAT/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[6]
    
    lL=[3,4,5,6]
    tL=[2,3,4,5]
    fL=[fLI]
    nL=['\\LISVI']
    tab=Table(lL,tL,sdt,endTime,1,fL[0])
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    plot.valuePlot(6,fLI,nL)
def computeErrorsExtrapolated(which):
    sdt=0.0001
    evalPoints = [6,7]
    lL=[3,4,5]
    tL=[2,3,4,5]
    if which=='LI':
        fn='../../../datafiles/TestEllipsoid3d/'+'LI0001SAT/'
        l=6
        j=4
        tab=Table(lL,tL,sdt,0.03,1,fn)
        filename=fn+'tex/'+'LIExtraErrorTab'
        with open(filename+'.tex', 'w') as output_file:
            caption='Error in space, time and both for the reference solution $\\V^{'+str(l)+','+str(j)+'}(\\cdot,\\points)$ computed with (\\LISVI)  compared to the extrapolates respectively in different evaluation points'
            label='\\label{tab:RefErrorTabLI}'
            output_file.write(tab.ExtraErrorTable(evalPoints,caption,label,l,j))
    elif which=='SI4':
        fn='../../../datafiles/TestsFromHoreka/'
        fSI=fn+'SI0001SAT/'
        J=4
        L=6
        tab=Table(lL,tL,sdt,0.02963125,1,fSI)
        filename=fn+'tex/'+'SI4ExtraErrorTab'
        with open(filename+'.tex', 'w') as output_file:
            caption='Estimated error in space and time and both for the  solution $\\V^{'+str(J)+','+str(L)+'}_\\text{SI}(\\cdot,\\points)$ computed with the semi-implicit (\\SISVI) scheme'
            label='\\label{tab:SI4ErrorTabLI}'
            output_file.write(tab.ExtraErrorTable(evalPoints,caption,label,L,J,'\\text{SI}'))
    elif which =='SI5':
        fn='../../../datafiles/TestsFromHoreka/'
        fSI=fn+'SI0001SAT/'
        J=5
        L=6
        tab=Table(lL,tL,sdt,0.03,1,fSI)
        filename=fn+'tex/'+'SI5ExtraErrorTab'
        with open(filename+'.tex', 'w') as output_file:
            caption='Estimated error in space and time and both for the  solution $\\V^{'+str(J)+','+str(L)+'}_\\text{SI}(\\cdot,\\points)$ computed with the semi-implicit (\\SISVI) scheme'
            label='\\label{tab:SI5ErrorTabLI}'
            output_file.write(tab.ExtraErrorTable(evalPoints,caption,label,L,J,'\\text{SI}'))
    elif which=='SI5LI':
        fn='../../../datafiles/TestsFromHoreka/'
        fLI= fn+'LI0001SAT/'
        fSI= fn+'SI0001SAT/'
        fL=[fLI,fSI]
        nL=['\\LI','\\SI']
        discPair=[[4,6],[5,6]]
        tabWriter =TableWriter(lL, tL,sdt,0.03,1,fL,evalPoints)
        tabWriter.writeExtraErrorListTable(fn,nL,discPair,5,5)
    else:
        print('case not defined')
    
def CompareAllWithLIRef():
    eP='../../../datafiles/TestEllipsoid3d/'
    fLI=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    fIE=eP+'TestsFromHoreka/IE0001SAT/'
    fGODE = eP+'TestsFromHoreka/GODE0001SAT/IE/'
    fGPDE = eP+'TestsFromHoreka/GPDE0001SAT/IE/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    
    lL=[3,4]
    tL=[3,4]
    fL=[fLI,fSI,fIE,fGODE,fGPDE]
    nL=['\\LISVI','\\SISVI','\\IESVI','\\GODE','\\GPDE']
    tab=Table(lL,tL,sdt,endTime,1,fL[0])
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'TICompRefTabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Comparison of different time integration approaches for $\\V$ on different levels and time step sizes for $\\frac{\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')- \\V^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')\\|_\\Ltwonorm}{\\|\\V^{4,6}(\\cdot,\\textbf{P'+str(p)+'})\\|_\\Ltwonorm}$'
            label='\\label{tab:TICompRefP'+str(p)+'}'
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())      
def smoothingTest():
    eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    fTimeSmooth=eP+'LI0001SAT/timeSmoothing/'
    fSpaceSmooth=eP+'LI0001SAT/spaceSmoothing/'
    allSmoooth=eP+'LI0001SAT/wcV/'#standard LI0001SAT
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    fL=[allSmoooth,fTimeSmooth,fSpaceSmooth]
    lL=[3,4]
    tL=[3,4]
    #for direction in fL:
        #read.writeDataInFile(direction,lL,tL,1)
    nL=['\\LISVI-\\SAT','\\LISVI-T','\\LISVI-S']
    tab=Table(lL,tL,sdt,endTime,1,fL[0])
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'SmoothingCompRefTabP'+str(p)
        caption='Smoothing options of the external current computed with the linearized (\\LISVI)  approach for the component-wise splitting on different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p)+')\\|_\\Ltwonorm$'
        label='\\label{tab:SmoothingCompRefP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())
def orderTest():
    eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    fwcV=eP+'LI0001SAT/wcV/'#standard LI0001SAT
    fwVc=eP+'LI0001SAT/wVc/'
    fVcw=eP+'LI0001SAT/Vcw/'
    fVwc=eP+'LI0001SAT/Vwc/'
    fcVw=eP+'LI0001SAT/cVw/'
    fcwV=eP+'LI0001SAT/cwV/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    fL=[fVcw,fVwc,fwcV,fwVc,fcVw,fcwV]
    lL=[3,4]
    tL=[3,4]
    #for direction in fL:
        #read.writeDataInFile(direction,lL,tL,1)
    nL=['$\\V$-$c$-$\\w$','$\\V$-$\\w$-$c$','$\\w$-$c$-$\\V$','$\\w$-$\\V$-$c$','$c$-$\\V$-$\\w$','$c$-$\\w$-$\\V$']
    tab=Table(lL,tL,sdt,endTime,1,fwcV)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'OrderCompRefTabP'+str(p)
        caption='Different order options for  the linearized (\\LISVI)  approach of the component-wise splitting on different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p)+')\\|_\\Ltwonorm$'
        label='\\label{tab:SmoothingCompRefP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())
def SVI_ICI_Test():
    eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    fSVI=eP+'LI0001SAT/wcV/'#standard LI0001SAT
    fICI=eP+'LI0001SAT/ICI/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    fL=[fSVI,fICI]
    lL=[3,4]
    tL=[3,4,5]
    read.writeDataInFile(fSVI,[4],[5],1)
    nL=['\\SVI','\\ICI']
    tab=Table(lL,tL,sdt,endTime,1,fSVI)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'ICICompRefTabP'+str(p)
        caption='Comparison of the \\SVI~and \\ICI~approach for the linearized  approach of the component-wise splitting on different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')\\|_\\Ltwonorm$'
        label='\\label{tab:ICICompRefP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())
            
    
    fSISVI='../../../datafiles/TestEllipsoid3d/'+'SI0001SAT/'#standard SI0001SAT
    fSIICI='../../../datafiles/TimeAsymptoticTest/TestEllipsoid3dExcitation/'+'SI0001SATNodes/'#ICI
    fL=[fSISVI,fSIICI]
    nL=['\\SVI','\\ICI']
    tab=Table(lL,tL,sdt,endTime,1,fSVI)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'SIICICompRefTabP'+str(p)
        caption='Comparison of the \\SVI~and \\ICI~approach for (SI) on different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')\\|_\\Ltwonorm$'
        label='\\label{tab:SIICICompRefP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())

def LIDiffLIRef():
    eP='../../../datafiles/TestEllipsoid3d/'
    fwcV=eP+'LI0001SAT/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    
    lL=[3,4,5,6]
    tL=[2,3,4,5]
    fL=[fwcV]
    nL=['\\LISVI']
    tab=Table(lL,tL,sdt,endTime,1,fwcV)
    for p in listEvaluationPoints:
        filename=fwcV+'tex/'+'LICompRefLITabP'+str(p)
        caption='Linearized impllicit scheme (\\LISVI) for different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')\\|_\\Ltwonorm$'
        label='\\label{tab:LICompRefLIP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())
def SIDiffLIRef():
    eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    fSI='../../../datafiles/TestEllipsoid3d/'+'SI0001SAT/'
    #fwcV ='../../../datafiles/TestEllipsoid3d/'+'LI0001SAT/' 
    fwcV=eP+'LI0001SAT/wcV/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    
    lL=[2,3,4,5,6]
    tL=[2,3,4,5]
    fL=[fSI]
    nL=['\\SISVI']
    tab=Table(lL,tL,sdt,endTime,1,fwcV)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'SICompRefLITabP'+str(p)
        caption='Semi-implicit approach (\\SISVI) for different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')\\|_\\Ltwonorm$'
        label='\\label{tab:SICompRefLIP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())
            
def IELISI_TimeTest(which):
    eP='../../../datafiles/TestsFromHoreka/'
    fIE=eP+'IE0001SAT/'
    fwcV=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    fG=eP+'GPDE0001SATIE/'
    sdt=0.0001
    listEvaluationPoints=[4,5,6,7,8]
    fL=[fIE,fwcV,fSI,fG]
    nL=['\\IESVI','\\LISVI','\\SISVI','\\GPDE']
    tL=[2,3,4,5]
    lL=[4]
    if which=='LI':
        tab=Table(lL,tL,sdt,0.03,1,fwcV)
        for p in listEvaluationPoints:
                filename=eP+'tex/'+'IELISIGTimeConRefTabP'+str(p)
                caption='The time convergence for the implicit (\\IESVI), the linear implicit (\\LISVI),the semi-implicit (\\SISVI) time integration and the Godunov splitting (\\GPDE) approach compared to the reference Solution'
                label='tab:IELISIGTimeConRefP'+str(p)
                with open(filename+'.tex', 'w') as output_file:
                    output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL,'space',False))
                    output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
                    output_file.write(tab.writeEndTable())
    elif which=='SI':
        tL=[2,3,4]
        tab=Table(lL,tL,sdt,0.03,1,fSI)
        for p in listEvaluationPoints:
            filename = eP+'tex/'+'IELISIGTimeConRefSITabP'+str(p)
            caption='The time convergence for the implicit (\\IESVI), the linear implicit (\\LISVI),the semi-implicit (\\SISVI) time integration and the Godunov splitting (\\GPDE) approach compared to the finest approximation of \\SISVI~$\\V_\\SI^{5,6}$'
            label='tab:IELISIGTimeConRefSIP'+str(p)
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL,'space',False))
                output_file.write(tab.writeCompareRefList(p,5,6,fL,nL,'\\SI'))
                output_file.write(tab.writeEndTable())
def IELISI_SpaceTest(which):
    eP='../../../datafiles/TestsFromHoreka/'
    fIE=eP+'IE0001SAT/'
    fwcV=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    fG=eP+'GPDE0001SATIE/'
    sdt=0.0001
    listEvaluationPoints=[4,5,6,7,8]
    if which =='LISI':
        endTime=0.02963125    
        fL=[fwcV,fSI]
        lL=[2,3,4,5,6]
        tL=[4]
        nL=['\\LISVI','\\SISVI']
        tab=Table(lL,tL,sdt,endTime,1,fwcV)
        for p in listEvaluationPoints:
            filename=eP+'tex/'+'LISISpaceConTabP'+str(p)
            caption='\\LISVI~and \\SISVI~on  fixed  $j=4$ showing the space convergence with the space extrapolates $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')- \\V^{4,\\infty}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')\\|_\\Ltwonorm$'
            label='\\label{tab:LISISpaceConP'+str(p)+'}'
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
                output_file.write(tab.writeConSpaceList(p,4,6,fL,nL))
                output_file.write(tab.writeEndTable())
    elif which=='SIj5':
        #read.writeDataInFile(fSI,[4],[5],1,True)    
        lL=[2,3,4,5,6]
        tL=[5]    
        fL=[fSI]
        tab=Table(lL,tL,sdt,endTime,1,fSI)
        nL=['\\SISVI']
        for p in listEvaluationPoints:
            filename=eP+'tex/'+'SISpaceConTabP'+str(p)
            caption='\\SISVI~on  fixed  $j=5$ showing the space convergence with the space extrapolates $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')- \\V^{5,\\infty}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')\\|_\\Ltwonorm$'
            label='\\label{tab:SISpaceConP'+str(p)+'}'
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
                output_file.write(tab.writeConSpaceList(p,5,6,fL,nL))
                output_file.write(tab.writeEndTable())
    elif which =="allRefLI":
        fL=[fIE,fwcV,fSI,fG]
        nL=['\\IESVI','\\LISVI','\\SISVI','\\GPDE']
        lL=[2,3,4,5]
        tab=Table(lL,[4],sdt,0.03,1,fwcV)
        for p in listEvaluationPoints:
            filename=eP+'tex/'+'IELISIGSpaceConRefTabP'+str(p)
            caption='The space convergence for the implicit (\\IESVI), the linear implicit (\\LISVI),the semi-implicit (\\SISVI) time integration and the Godunov splitting (\\GPDE) approach compared to the reference Solution'
            label='\\label{tab:IELISIGSpaceConRefP'+str(p)+'}'
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL,'time',False))
                output_file.write(tab.writeCompareSpaceRefList(p,4,6,fL,nL))
                output_file.write(tab.writeEndTable())
            
    elif which == 'allRefSI4':
        fL=[fIE,fwcV,fSI]
        nL=['\\IESVI','\\LISVI','\\SISVI']
        lL=[2,3,4,5]
        tab=Table(lL,[4],sdt,0.02963125,1,fSI)
        for p in listEvaluationPoints:
            filename=eP+'tex/'+'IELISISpaceConSI4TabP'+str(p)
            caption='The space convergence for the implicit (\\IESVI), the linear implicit (\\LISVI) and the semi-implicit (\\SISVI) time integration approach compared to $\\V_\\text{SI}^{4,6}$'
            label='\\label{tab:IELISISpaceConSI4P'+str(p)+'}'
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL,False))
                output_file.write(tab.writeCompareSpaceRefList(p,4,6,fL,nL,'\\text{SI}'))
                output_file.write(tab.writeEndTable())
    elif which == 'allRefSI5':
        fL=[fIE,fwcV,fSI,fG]
        nL=['\\IESVI','\\LISVI','\\SISVI']
        lL=[2,3,4,5]
        tab=Table(lL,[4],sdt,0.03,1,fSI)
        for p in listEvaluationPoints:
            filename=eP+'tex/'+'IELISISpaceConSI5TabP'+str(p)
            caption='The space convergence for the implicit (\\IESVI), the linear implicit (\\LISVI) and the semi-implicit (\\SISVI) time integration approach compared to $\\V_\\text{SI}^{5,6}$'
            label='tab:IELISISpaceConSI5P'+str(p)
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL,False))
                output_file.write(tab.writeCompareSpaceRefList(p,5,6,fL,nL,'\\text{SI}'))
                output_file.write(tab.writeEndTable())
    else:
        print('case not known')
def TestTimeIE():
    eP='../../../datafiles/TestsFromHoreka/'
    fIE=eP+'IE0001SAT/'
    fLI=eP+'LI0001SAT/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[6]
    VTimeLI=comp.getExtrapolateInTime(3,7,1,6,sdt,endTime,fLI)
    (t,V) = read.getDataFromTXT(fIE,read.createFilename(3,2,1),6)
    value=comp.L2DiffPot(V,VTimeLI,sdt*2**(-2),endTime)
    print( str("%8.4f"%(value)))
def IELISI_TITest(which='', DivideNorm=False):
    #eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    eP='../../../datafiles/TestsFromHoreka/'
    fIE=eP+'IE0001SAT/'
    fwcV=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    fG=eP+'GPDE0001SATIE/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    #Time:
    fL=[fIE,fwcV,fSI,fG]
    lL=[3]
    tL=[1,2,3,4,5,6,7]
    read.writeDataInFile(fIE,[3],[2],1,True)
    #read.writeDataInFile(fwcV,lL,[1,2],1,True)
    #read.writeDataInFile(fSI,lL,[4],1,True)
    nL=['\\IESVI','\\LISVI','\\SISVI','\\GPDE']
    tab=Table(lL,tL,sdt,endTime,1,fwcV)
    if which=='Extra':
        for p in listEvaluationPoints:
            filename=eP+'tex/'+'IELISIGPDETimeConTabP'+str(p)
            caption='\\IESVI, \\LISVI, \\SISVI~and \\GPDE~on  fixed level $\\ell=3$ showing the time convergence with the time extrapolates $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')- \\V^{\\infty,3}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')\\|_\\Ltwonorm$'
            label='\\label{tab:IELISIGPDETimeConP'+str(p)+'}'
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL,'space',False))
                output_file.write(tab.writeConTimeList(p,7,3,fL,nL))
                output_file.write(tab.writeEndTable())
    elif which=='ref':
        J=4
        L=6
        for p in listEvaluationPoints:
            if DivideNorm:
                filename=eP+'tex/'+'IELISIGPDETimeConRefDivTabP'+str(p)
                caption='\\IESVI, \\LISVI, \\SISVI~and \\GPDE~on  fixed level $\\ell=3$ showing the error in time with the reference solution computed by \\LISVI'
                label='\\label{tab:IELISIGPDETimeConRefDivP'+str(p)+'}'
            else:
                filename=eP+'tex/'+'IELISIGPDETimeConRefTabP'+str(p)
                caption='\\IESVI, \\LISVI, \\SISVI~and \\GPDE~on  fixed level $\\ell=3$ showing the time convergence with the reference solution computed by \\LISVI'
                label='\\label{tab:IELISIGPDETimeConRefP'+str(p)+'}'
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL,'space',False))
                output_file.write(tab.writeCompareRefList(p,J,L,fL,nL,'\\Ref',DivideNorm))
                output_file.write(tab.writeEndTable())
    elif which=='SI5':
        tab=Table(lL,tL,sdt,endTime,1,fSI)
        J=5
        L=6
        for p in listEvaluationPoints:
            filename=eP+'tex/'+'IELISIGPDETimeConSI5TabP'+str(p)
            caption='\\IESVI, \\LISVI, \\SISVI~and \\GPDE~on  fixed level $\\ell=3$ showing the time convergence with the reference solution computed by \\SISVI'
            label='\\label{tab:IELISIGPDETimeConSI5P'+str(p)+'}'
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL,'space',False))
                output_file.write(tab.writeCompareRefList(p,J,L,fL,nL,'\\text{SI}'))
                output_file.write(tab.writeEndTable())
    elif which=='SI4':
        tab=Table(lL,tL,sdt,0.02963125,1,fSI)
        J=4
        L=6
        for p in listEvaluationPoints:
            filename=eP+'tex/'+'IELISIGPDETimeConSI4TabP'+str(p)
            caption='\\IESVI, \\LISVI, \\SISVI~and \\GPDE~on  fixed level $\\ell=3$ showing the time convergence with the reference solution computed by \\SISVI$'
            label='\\label{tab:IELISIGPDETimeConSI5P'+str(p)+'}'
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL,'space',False))
                output_file.write(tab.writeCompareRefList(p,J,L,fL,nL,'\\text{SI}'))
                output_file.write(tab.writeEndTable())
    elif which=='SI45':
        for p in listEvaluationPoints:
            L=6
            if DivideNorm:
                filename=eP+'tex/'+'TITimeConRefSI45DivTabP'+str(p)
                caption='\\IESVI, \\LISVI, \\SISVI~and \\GPDE~on  fixed level $\\ell=3$ showing the error in time with the finest solutions of \\SISVI'
                label='\\label{tab:TITimeConRefSI45DivP'+str(p)+'}'
            else:
                filename=eP+'tex/'+'TITimeConRefSI45TabP'+str(p)
                caption='\\IESVI, \\LISVI, \\SISVI~and \\GPDE~on  fixed level $\\ell=3$ showing the time convergence with the finest solutions of \\SISVI'
                label='\\label{tab:TITimeConRefSI45P'+str(p)+'}'
            with open(filename+'.tex', 'w') as output_file:
                tab=Table(lL,tL,sdt,0.02963125,1,fSI)
                output_file.write(tab.writeBeginTable())
                output_file.write(tab.writeTabelCapLab(caption,label))
                output_file.write('\\begin{tabular}{cc}\n')
                output_file.write(tab.beginTabular(fL,'space'))
                output_file.write(tab.writeCompareRefList(p,4,L,fL,nL,'\\text{SI}',DivideNorm))
                output_file.write('&\n')
                tab=Table(lL,tL,sdt,endTime,1,fSI)
                output_file.write(tab.beginTabular(fL,'space'))
                output_file.write(tab.writeCompareRefList(p,5,L,fL,nL,'\\text{SI}',DivideNorm))
                #output_file.write()
                output_file.write('\\end{tabular}')
                output_file.write(tab.writeEndTable())
                
    elif which=='compareRefs':
        fL=[fwcV,fSI,fSI]
        nL=['\\text{LI}','\\text{SI}','\\text{SI}']
        discpair=[[4,6],[4,6],[5,6]]
        tab=Table(lL,tL,sdt,0.02963125,1,fwcV)
        for p in listEvaluationPoints:
            filename=eP+'tex/'+'DiffRefsP'+str(p)
            caption='Differences of the finest space and time discretized solutions'
            label='\\label{tab:DiffRefsP'+str(p)+'}'
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(tab.writeBeginDiffTable(p,caption,label,fL,False))
                output_file.write(tab.writeDiffRow(p,fL,nL,discpair))
                output_file.write(tab.writeEndTable())
    else:
        print('test')
        print('case not implemented')
def TimeTestWithDifferentExtrapolate():
    eP='../../../datafiles/TestsFromHoreka/'
    fSI=eP+'SI0001SAT/'
    fG=eP+'GPDE0001SATIE/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    lL=[3]
    tL=[1,2,3,4,5,6,7]
    fL=[fG]
    nL=['\\GPDE']
    tab=Table(lL,tL,sdt,endTime,1,fSI)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'GPDETimeConExtraSITabP'+str(p)
        caption='\\GPDE~on  fixed level $\\ell=3$ showing the time convergence with the time extrapolate from \\SISVI~$\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')- \\V^{\\infty,3}_\\text{SI}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')\\|_\\Ltwonorm$'
        label='\\label{tab:GPDETimeConExtraSIP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL,False))
            output_file.write(tab.writeConTimeListDiffExtra(p,7,3,fL,nL))
            output_file.write(tab.writeEndTable())
    
def IELISITest():
    eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    fIE=eP+'IE0001SAT/'
    fwcV=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    fL=[fIE,fwcV,fSI]
    lL=[3,4]
    tL=[3,4,5,6,7]
    nL=['\\IESVI','\\LISVI','\\SISVI']
    
    #read.writeDataInFile(alg,[6],[4],1,True)    
    tab=Table(lL,tL,sdt,endTime,1,fwcV)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'IELISICompRefTabP'+str(p)
        caption='\\IESVI, \\LISVI~and \\SISVI~on different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p,True)+')\\|_\\Ltwonorm$'
        label='\\label{tab:IELISICompRefP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())
def PlotRefSolutions():
    eP='../../../datafiles/TestsFromHoreka/'
    fwcV=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[6]
    nL=['\\LISVI','\\SISVI~$j=4$','\\SISVI~$j=5$']
    L=6
    l=3
    #read.writeDataInFile(fwcV,[4],[1,2,3,4,5],1,True)
    plot=Plot([6],[4,5],sdt,endTime,1,listEvaluationPoints)
    interval=[0.007,0.010]
    (time,V)= read.getDataFromTXT(fwcV,read.createFilename(L,4,1),6)
    for i in range(len(V)):
        V[i] = float(V[i])
    labelname = nL[0]
    plot.plotInInterval(time,V,labelname,interval,sdt*2**(-4))
    for j in range(3,6):
        (time,V)= read.getDataFromTXT(fwcV,read.createFilename(l,j,1),6)
        for i in range(len(V)):
            V[i] = float(V[i])
        labelname = nL[0]+ '~$ \ell='+str(l)+'$, $j='+str(j)+'$'
        plot.plotInInterval(time,V,labelname,interval,sdt*2**(-j),'dotted')
    
    (time,V)= read.getDataFromTXT(fSI,read.createFilename(L,5,1),6)
    for i in range(len(V)):
        V[i] = float(V[i])
    labelname = nL[2]
    plot.plotInInterval(time,V,labelname,interval,sdt*2**(-5))
    
    for j in range(3,6):
        (time,V)= read.getDataFromTXT(fSI,read.createFilename(l,j,1),6)
        for i in range(len(V)):
            V[i] = float(V[i])
        labelname = '\\SISVI'+ '~$ \ell='+str(l)+'$, $j='+str(j)+'$'
        plot.plotInInterval(time,V,labelname,interval,sdt*2**(-j),'dashed')
    plt.legend(bbox_to_anchor=(1.1,1), loc="upper left")
    plot.plotData(True)
    plot.saveTikz(eP,'PlotRefAndSolu')

def FigureWorkPrecision(which):
    eP='../../../datafiles/TestsFromHoreka/'
    sP='../../datafiles/TestsFromHoreka/tex/'
    
    fLI=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    fIE =eP+'IE0001SAT/'
    fG=eP +'GPDE0001SATIE/'
    #read.writeDataInFile(fIE,[3],[2,3,4,5,6,7],1,True)
    #read.writeDataInFile(fIE,[4],[3,4,5],1,True)
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[6]
    nL=['\\IESVI','\\LISVI','\\SISVI','\\GPDE']
    fL=[fIE,fLI,fSI,fG]
    #for alg in fL:
        #read.writeDataInFile(alg,[2],[4],1,True)
    
    j=4
    l=4
    lL=[2,3,4,5]
    point=6
    plot=Plot([2,3,4,5],[2,3,4,5,6],sdt,endTime,1,listEvaluationPoints)
    if which =='SI':
        sP='images/'
        L=6
        J=5
        ydescription='$\\errSI$'
        (nameS,minprocS)=plot.workPrecisionInSpace(L,J,j,fL,nL,fSI,eP,point,ydescription)
        (nameT,minprocT)=plot.workPrecisionInTime(L,J,l,fL,nL,fSI,eP,point,ydescription,minprocS)
        print(minprocS,minprocT)
        filename=eP+'tex/'+'WPSIFigP'+str(point)
        caption='\\IESVI, \\LISVI, \\SISVI, ~and \GPDE~with fixed  time step size $j='+str(j)+'$ (left) and $\ell='+str(l)+'$ (right) comparing the CPU time scaled to $\\#$procs$='+str(minprocS)+'$ (left) and $\\#$procs$='+str(minprocT)+'$ (right) to the error $\\errSI('+getPointNamebyNumber(point,True)+')$'
        label='\\label{fig:WPSI'+str(point)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write('\\begin{figure}[h]\n')
            output_file.write('\\centering\n')
            output_file.write('\\resizebox{7cm}{!}{\\input{'+sP+nameS+'}}\n')
            output_file.write('\\resizebox{7cm}{!}{\\input{'+sP+nameT+'}}\n')
            output_file.write('\\caption{'+caption+'.}\n')
            output_file.write(label+'\n')
            output_file.write('\\end{figure}\n')
    elif which =='LI':
        sP='testtables/'
        L=6
        J=4
        ydescription='$\\eta_\\text{LI}$'
        (nameS,minprocS)=plot.workPrecisionInSpace(L,J,j,fL,nL,fLI,eP,point,ydescription)
        (nameT,minprocT)=plot.workPrecisionInTime(L,J,l,fL,nL,fLI,eP,point,ydescription)
        print(minprocS,minprocT)
        filename=eP+'tex/'+'WPLIFigP'+str(point)
        caption='\\IESVI, \\LISVI, \\SISVI, ~and \GPDE~with fixed  time step size $j='+str(j)+'$ (left) and $\ell='+str(l)+'$ (right) comparing the CPU time scaled to $\\#$procs$='+str(minprocS)+'$ (left) and $\\#$procs$='+str(minprocT)+'$ (right) to the error $\\eta_\\text{LI}('+getPointNamebyNumber(point,True)+')$'
        label='\\label{fig:WPLI'+str(point)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write('\\begin{figure}[h]\n')
            output_file.write('\\centering\n')
            output_file.write('\\resizebox{7cm}{!}{\\input{'+sP+nameS+'}}\n')
            output_file.write('\\resizebox{7cm}{!}{\\input{'+sP+nameT+'}}\n')
            output_file.write('\\caption{'+caption+'.}\n')
            output_file.write(label+'\n')
            output_file.write('\\end{figure}\n')
    else:
        print('case not implemented')
    
def PlotTimeExtrapolates():
    eP='../../../datafiles/TestsFromHoreka/'
    fIE=eP+'IE0001SAT/'
    fwcV=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    fG =eP+'GPDE0001SATIE/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[6]
    fL=[fIE,fwcV,fSI,fG]
    lL=[3]
    tL=[2,3,4,5,6,7]
    nL=['\\IESVI','\\LISVI','\\SISVI','\\GPDE']
    interval =[0.007,0.012]
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    plot.plotTimeExtrapolates(fL,nL,eP,interval)
def PlotSpaceExtrapolates():
    eP='../../../datafiles/TestsFromHoreka/'
    fwcV=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[6]
    fL=[fwcV,fSI]
    lL=[2,3,4,5,6]
    tL=[4]
    nL=['\\LISVI','\\SISVI']
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    plot.plotSpaceExtrapolates(fL,nL,eP,[0.007,0.012])    
def IETest():
    eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    fIE=eP+'IE0001SAT/'
    fwcV=eP+'LI0001SAT/wcV/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    fL=[fIE,fwcV]
    lL=[3,4]
    tL=[3,4,5,6,7]
    
    read.writeDataInFile(fIE,[3],[7],1,False)
    read.writeDataInFile(fwcV,[3],[7],1)
    nL=['\\IESVI','\LISVI']
    tab=Table(lL,tL,sdt,endTime,1,fwcV)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'IECompRefTabP'+str(p)
        caption='\\IESVI~on different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p)+')\\|_\\Ltwonorm$'
        label='\\label{tab:IECompRefP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())
def InPDETest():
    eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    fGODE=eP+'GODE0001SAT/'
    fGPDE=eP+'GPDE0001SAT/IE/'
    fwcV=eP+'LI0001SAT/wcV/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    fL=[fGODE,fGPDE]
    lL=[3,4]
    tL=[3,4]
    for direction in fL:
        read.writeDataInFile(direction,lL,tL,1,False)
    nL=['\\GODE','\\GPDE']
    tab=Table(lL,tL,sdt,endTime,1,fwcV)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'InPDECompRefTabP'+str(p)
        caption='\\GODE~and \\GPDE~on different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p)+')\\|_\\Ltwonorm$'
        label='\\label{tab:InPDECompRefP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())

def Strang():
    eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    fExp= eP + 'S0001SAT/Exponential/'
    fRK2=eP+'S0001SAT/RK2/'
    fRK4=eP+'S0001SAT/RK4/'
    fwcV=eP+'LI0001SAT/wcV/'
    fL=[fExp,fRK2,fRK4]
    lL=[3,4,5]
    tL=[3,4,5]
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    #for direction in fL:
       # read.writeDataInFile(direction,lL,tL,1,False)
    nL=['RL','RK2','RK4']
    tab=Table(lL,tL,sdt,endTime,1,fwcV)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'StrangTICompRefTabP'+str(p)
        caption='\\Strang~with different time integration methods for the ODE different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p)+')\\|_\\Ltwonorm$'
        label='\\label{tab:StrangTICompRefP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())
    nL=['Strang-RL','Strang-RK2','Strang-RK4']
    tabWriter =TableWriter(lL, tL,sdt,endTime,1,fL,listEvaluationPoints)
    tabWriter.SpaceTimeTogether(nL,True)
 
def TITest():
    eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    fEE=eP+'GPDE0001SAT/EE/'
    fCN=eP+'GPDE0001SAT/CN/'
    fIE=eP+'GPDE0001SAT/IE/'
    fwcV=eP+'LI0001SAT/wcV/'
    fL=[fCN,fIE]
    lL=[3,4,5]
    tL=[2,3,4,5]
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    #for direction in fL:
        #read.writeDataInFile(direction,lL,tL,1,False)
    nL=['CN','IE']
    tab=Table(lL,tL,sdt,endTime,1,fwcV)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'GPDETICompRefTabP'+str(p)
        caption='\\GPDE~with different time integration methods for the PDE on different levels and time step sizes compared to the reference solution $\\|\\V^{j,\\ell}(\\cdot,'+tab.getPointNamebyNumber(p)+')- \\V_{\\Ref}^{4,6}(\\cdot,'+tab.getPointNamebyNumber(p)+')\\|_\\Ltwonorm$'
        label='\\label{tab:GPDETICompRefP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())
            
    nL=['GPDE-CN','GPDE-IE']
    tabWriter =TableWriter(lL, tL,sdt,endTime,1,fL,listEvaluationPoints)
    tabWriter.SpaceTimeTogether(nL,True)    
def Stein():
    eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
    f=eP+'B0001SAT/Heun/'
    fL=[f]
    lL=[3,4]
    tL=[1,2,3,4,5,6]
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    for direction in fL:
        read.writeDataInFile(direction,lL,tL,1,False)
    nL=['Stein']
    tabWriter =TableWriter(lL, tL,sdt,endTime,1,fL,listEvaluationPoints)
    tabWriter.TimeDiffTable(nL)
def DurationAndProcs(which):
    sdt=0.0001
    endTime=0.03 
    if which =='all':
        eP='../../../datafiles/TestEllipsoid3d/'
        fLI=eP+'LI0001SAT/'
        fLI_ICI =eP+'TestsFromHoreka/LI0001SAT/ICI/'
        fSI=eP+'SI0001SAT/'
        fIE=eP+'TestsFromHoreka/IE0001SAT/'
        fGODE = eP+'TestsFromHoreka/GODE0001SAT/IE/'
        fGPDE = eP+'TestsFromHoreka/GPDE0001SAT/IE/'
        lL=[3,4]
        tL=[3,4]
        fL=[fLI,fSI,fIE,fGODE,fGPDE]
        nL=['\\LISVI','\\SISVI','\\IESVI','\\GODE','\\GPDE']
        tab=Table(lL,tL,sdt,endTime,1,fL[0])
        filename=eP+'tex/'+'CompDurAllAlg'
        caption='CPU time (hours:minutes:seconds) including number of processes for the different time integration schemes in the benchmark configuration'
        label='\\label{tab:DurAllAlg'+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginDurationTabList(caption,label,fL))
            output_file.write(tab.writeDurAProcs(fL,nL))
            output_file.write(tab.writeEndTable())
    elif which=='SVI-ICI':
        fL=[fLI,fLI_ICI]
        nL=['\\LISVI','\\LIICI']
        tL=[3,4,5]
        tab=Table(lL,tL,sdt,endTime,1,fL[0])
        filename=eP+'tex/'+'CompDurSVIICI'
        caption='CPU time (hours:minutes:seconds) including number of processes comparing the \SVI~and \ICI~approach for the linear implicit scheme (LI) with the benchmark configuration'
        label='\\label{tab:DurSVIICI'+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginDurationTabList(caption,label,fL))
            output_file.write(tab.writeDurAProcs(fL,nL))
            output_file.write(tab.writeEndTable())
    elif which =='IE-LI-SI':
        eP='../../../datafiles/TestsFromHoreka/'
        fLI=eP+'LI0001SAT/'
        fSI=eP+'SI0001SAT/'
        fIE=eP+'IE0001SAT/'
        lL=[3,4]
        tL=[3,4]
        fL=[fIE,fLI,fSI]
        nL=['\\IESVI','\\LISVI','\\SISVI']
        tab=Table(lL,tL,sdt,endTime,1,fL[0])
        filename=eP+'tex/'+'CompDurIELISI'
        caption='CPU time (hours:minutes:seconds) including number of processes for the different time integration schemes in the benchmark configuration'
        label='\\label{tab:CompDurIELISI'+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginDurationTabList(caption,label,fL))
            output_file.write(tab.writeDurAProcs(fL,nL))
            output_file.write(tab.writeEndTable())
    elif which =='IE-LI-SI-GPDE':
        eP='../../../datafiles/TestsFromHoreka/'
        fLI=eP+'LI0001SAT/'
        fSI=eP+'SI0001SAT/'
        fIE=eP+'IE0001SAT/'
        fG = eP+'GPDE0001SATIE/'
        lL=[3,4]
        tL=[3,4]
        fL=[fIE,fLI,fSI,fG]
        nL=['\\IESVI','\\LISVI','\\SISVI','\\GPDE']
        tab=Table(lL,tL,sdt,endTime,1,fL[0])
        filename=eP+'tex/'+'CompDurIELISIG'
        caption='CPU time (hours:minutes:seconds) including number of processes for the different time integration schemes in the benchmark configuration'
        label='tab:CompDurIELISIG'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginDurationTabList(caption,label,fL))
            output_file.write(tab.writeDurAProcs(fL,nL))
            output_file.write(tab.writeEndTable())
    else:
        print('case is not implemented')
def IETestEps():
    eP='../../../datafiles/TestsFromHoreka/'
    fIE=eP+'IE0001SAT/'
    fIE8=eP+'IE0001SAT/eps8/'
    fwcV=eP+'LI0001SAT/'
    #read.writeDataInFile(fIE8,[3],[3,4,5],1,False)
    sdt=0.0001
    endTime=0.03
    lL=[3]
    tL=[3,4,5]
    listEvaluationPoints=[4,5,6,7,8]
    fL=[fIE,fIE8]
    nL=['$\\varepsilon=1\cdot10^{-6}$','$\\varepsilon=1\cdot10^{-8}$']
    tab=Table(lL,tL,sdt,endTime,1,fwcV)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'IEepsTestP'+str(p)
        caption='Test with \\IESVI~and different $\\varepsilon$ with $\\delta=1\cdot 10^{-12}$'
        label='\\label{tab:IEepsTestP'+str(p)+'}'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginSmoothingTable(p,caption,label,fL))
            output_file.write(tab.writeCompareRefList(p,4,6,fL,nL))
            output_file.write(tab.writeEndTable())
def ScalabilityHoreka():
    eP='../../../datafiles/TestsFromHoreka/'
    fSI=eP+'SI0001SAT/'
    f_64 =fSI+'64/'
    f_128 =fSI+'128/'
    f_256 =fSI+'256/'
    f_512 =fSI+'512/'
    f_1024 =fSI+'1024/'
    
    sdt=0.0001
    endTime=0.03
    lL=[3]
    tL=[3]
    fL=[f_64,f_128,f_256,f_512,f_1024]
    tab=Table(lL,tL,sdt,endTime,1,fL[0])
    filename=eP+'tex/'+'ScalabilitySI'
    caption='CPU time (hours:minutes:seconds) for different number of processes using \SISVI, fixed space level $\\ell=3$ and fixed time level $j=3$ with the benchmark configuration'
    label='\\label{tab:ScalaSI'+'}'
    with open(filename+'.tex', 'w') as output_file:
        output_file.write(tab.writeBeginScalability(caption,label))
        output_file.write(tab.writeScalability(fL,3,3))
        output_file.write(tab.writeEndTable())
 
def studyFinest():
    fn='../../../datafiles/TestsFromHoreka/'
    fLI= fn+'LI0001SAT/'
    fSI= fn+'SI0001SAT/'
    fL=[fLI,fSI]
    nL=['\\LI','\\SI']
    dPairs=[[4,6],[5,6]]
    evalPoints=[6]
    
    sdt=0.0001
    endTime=0.03
    for p in evalPoints:
        (time,VLI)= read.getDataFromTXT(fL[0],read.createFilename(dPairs[0][1],dPairs[0][0],1),p)
        (time_SI,VSI)= read.getDataFromTXT(fL[1],read.createFilename(dPairs[1][1],dPairs[1][0],1),p)
        Diff=[]
        k=int((len(VSI)-1)/(len(VLI)-1))
        for i in range(len(VLI)):
            Diff.append(VLI[i]-VSI[k*i])
        print(comp.L2(Diff,sdt*2**(-4),endTime))
        normfullExtraLI = comp.L2(comp.getFullExtrapolate(5,5,1,p,sdt,endTime,fLI),sdt,endTime)
        print('divided by ExtraLI:',comp.L2(Diff,sdt*2**(-4),endTime)/normfullExtraLI)
        normfullExtraSI = comp.L2(comp.getFullExtrapolate(5,5,1,p,sdt,endTime,fSI),sdt,endTime)
        print('divided by ExtraSI:',comp.L2(Diff,sdt*2**(-4),endTime)/normfullExtraSI)
        print('diff extras LI-SI:',comp.L2DiffPot(comp.getFullExtrapolate(5,5,1,p,sdt,endTime,fLI),comp.getFullExtrapolate(5,5,1,p,sdt,endTime,fSI),sdt,endTime) )
        print(comp.MaxNorm(Diff,True),VLI[1685],VSI[1685])
        
        plot=Plot([6],[4,5],sdt,endTime,1,evalPoints)
        interval=[0.007,0.010]
        plot.plotInInterval(time,VLI,'$\\V_\\LI^{4,6}(\\cdot,\\psechs)$',interval,sdt*2**(-4))
        plot.plotInInterval(time_SI,VSI,'$\\V_\\SI^{5,6}(\\cdot,\\psechs)$',interval,sdt*2**(-5))
        plot.plotInInterval(time,Diff,'\\LI-\\SI',interval,sdt*2**(-4))
    
    
        plt.legend(bbox_to_anchor=(1.1,1), loc="upper left")
        plot.plotData(True)
        #plt.show()
        plot.saveTikz(fn,'PlotFinest')
def Test():
    fn='../../../datafiles/TestsFromHoreka/'
    fSI= fn+'SI0001SAT/'
    
    sdt=0.0001
    T=0.03
    (t,V)=read.getDataFromTXT(fSI,read.createFilename(5,3,1),6)
    norm=comp.L2(V,sdt*2**(-4),0.02963125,4,True)
    
def MeshInfoTab():
    lL=[0,1,2]
    tL=[0,1,2]
    sdt=0.0001
    endTime=0.1
    SI='../../../datafiles/BiVentricle/SI0001/'
    fL=[SI]
    listEvaluationPoints=[0,1,2,3,4,5,6]
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,fL,listEvaluationPoints)
    tabWriter.writeMeshInfoTable(SI)
def ComputationsCV():
    lL=[1,2]
    tL=[1,2]
    sdt=0.0001
    endTime=0.1
    listEvaluationPoints=[]
    SI='../../../datafiles/BiVentricle/SI0001/'
    fL=[SI]
    x=(39.611698, -83.080399, -34.947701)
    y=(-0.33377, -44.208099, -7.27446)
    #read.writeActivationDataFiles(SI,lL,tL)
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,fL,listEvaluationPoints)
    tabWriter.writeCVTable(SI,x,y)

def CVTableSI():
    lL=[0,1,2,3,4]
    tL=[4]
    sdt=0.0001
    endTime=0.1
    listEvaluationPoints=[]
    SI='../../../datafiles/TestsFromHoreka/ActivationTimeSI/'
    fL=[SI]
    #read.writeActivationDataFiles(SI,lL,tL)
    z1=(0.0,0.0,-18.5)
    z2=(2.07904, 6.36634, -11.2664)
    x1=(1.20855999, 4.18372011, -15.88949966)
    y1=(2.18665004, 6.58256006, -10.56809998)
    x2=(-2.57154012, 0.00000000, -15.81130028)
    y2=(-5.61778021, 0.00000000, -10.10529995)
    pairlist=[[x1,y1],[x2,y2]]
    nL=['midwall', 'inner surface']
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,fL,listEvaluationPoints)
    #tabWriter.writeCVFromListTable(SI,pairlist,nL)
    tabWriter.writeActivationPointlistTab(SI,'truncatedEllipsoid',[x1,y1,x2,y2])
    
def MiddledActivatationTimeTable():
    lL=[0,1]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.16
    listEvaluationPoints=[]
    
    GS='../../../datafiles/BiVentricle/GS0004/'
    fL=[GS]
    
    l0Path='../../../datafiles/BiVentricle/GS0004/vtu/AT_l0j0.vtu'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,fL,listEvaluationPoints)
    tabWriter.writeMiddeledActivationTab(GS,l0Path,'\\GPDE\\')
    
    lL=[0,1,2]
    tL=[0,1,2,3,4]
    SI='../../../datafiles/BiVentricle/SI0004/'
    fL=[SI]
    l0Path='../../../datafiles/BiVentricle/SI0004/vtu/AT_l0j0.vtu'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,fL,listEvaluationPoints)
    tabWriter.writeMiddeledActivationTab(SI,l0Path,'\\SISVI\\')
def ActivatationTimeTable():
    lL=[0,1,2,3]
    tL=[0,1,2,3,4]
    sdt=0.0001
    endTime=0.16
    listEvaluationPoints=[]
    SI='../../../datafiles/BiVentricle/SI0001/'
    fL=[SI]
    #read.writeDataInFile(SI,lL,tL,1,True)
    #read.writeActivationDataFiles(SI,lL,tL)
    x=(-16.0075, -47.811001, 4.33503)
    x=(-8.8439, -95.517899, -19.521999)
    x=8
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,fL,listEvaluationPoints)
    tabWriter.writeActivationTab(SI,x,'biventricle')
def ActvationRMSError():
    lL=[0,1,2,3]
    tL=[0,1,2,3,4]
    sdt=0.0001
    endTime=0.16
    listEvaluationPoints=[]
    SI='../../../datafiles/BiVentricle/SI0001/'
    fL=[SI]
    #read.writeIDLists(lL,tL,SI)
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,fL,listEvaluationPoints)
    #tabWriter.writeActivaitionRMSError(SI)
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    spacePlotName=plot.ActivationSpaceRMSErrors(SI)
    timePlotName=plot.ActivationTimeRMSErrors(SI)
    ActivationErrorPlot(SI,spacePlotName,timePlotName)
def ActvationRMSErrorGS():
    lL=[0,1]
    tL=[0,1]
    sdt=0.0001
    endTime=0.16
    listEvaluationPoints=[]
    GS='../../../datafiles/BiVentricle/GS0001/'
    fL=[GS]
    #read.writeDataInFile(GS,lL,tL,1,True)
    read.writeActivationDataFiles(GS,[0,1],[0,1])
    #read.writeIDLists(lL,tL,GS)
def ActivationErrorPlot(path,leftPlot,rightPlot):
    sP='images/'
    caption="Root mean square error of the activation times in space (left) and time (right)"
    label='\\label{fig:RMSEPlot}'
    filename = path+'tex/ActivationRMSErrorPlot'
    with open(filename+'.tex', 'w') as output_file:
            output_file.write('\\begin{figure}[h]\n')
            output_file.write('\\centering\n')
            output_file.write('\\resizebox{7cm}{!}{\\input{'+sP+leftPlot+'}}\n')
            output_file.write('\\resizebox{7cm}{!}{\\input{'+sP+rightPlot+'}}\n')
            output_file.write('\\caption{'+caption+'.}\n')
            output_file.write(label+'\n')
            output_file.write('\\end{figure}\n')
            
def EvaluationGSTab8():
    lL=[0,1,2,3]
    tL=[0,1,2,3,4]
    SI='../../../datafiles/BiVentricle/'+'SI0004/'
    GS='../../../datafiles/BiVentricle/'+'GS0004/'
    sdt=0.0004
    endTime=0.16
    listEvaluationPoints=[]
    l0Path='../../../datafiles/BiVentricle/'+'GS0004/vtu/AT_l0j0.vtu'
    
    #plotTact:
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)

    #ValueArray=plot.MeanTactSpace(GS,l0Path)
    #print('len value array: ', len(ValueArray))
    print('Plot finished')
    #tables tact
    L=4
    J=4
    savePath='images/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[SI],listEvaluationPoints)
    tabWriter.writeMeanTactTable(GS,l0Path,savePath,L,J,SI,[])
if __name__=="__main__": 
    #DurationAndProcs('IE-LI-SI-GPDE') #possible: 'all', 'SVI-ICI','IE-LI-SI','IE-LI-SI-GPDE'
    #comparisonOfQuadrature()
    #comparisonLISI()
    #ValueTableLI()
    #ValuePlotLI()
    #ValueTableSI()
    #MaxNormTable()
    #CompareWithRef()
    #CompareWithRef()
    #LIDiffLIRef()
    #CompareAllWithLIRef()
    #SiLIDiffConvergence()
    #SiLIDiffConvergenceMaxNorm()
    #PlotDiffSILI()
    #DifferenceTable()
    #smoothingTest()
    #orderTest()
    #SVI_ICI_Test()
    #InPDETest()
    #Strang()
    #Stein()
    #TITest()
    #SIDiffLIRef()
    #IETest()
    #computeErrorsExtrapolated('SI5LI') #LI,SI4,SI5
    #IELISITest()
    list=['ref','SI45']
    #list=['SI45']
    withDivideNorm=True
    #for a in list:
        #IELISI_TITest(a,withDivideNorm)# 'Extra','ref','SI5','SI4','compareRefs'
    #IELISI_TITest('Extra',False)
    #TimeTestWithDifferentExtrapolate()
    #ScalabilityHoreka()
    #IETestEps()
    #IELISI_SpaceTest('allRefSI5') #allRefLI,LISI,SI4,allRefSI4,allRefSI5
    #IELISI_TimeTest('SI') #'SI','LI'
    #IELISI_TimeTest('LI')
    #PlotTimeExtrapolates()
    #PlotSpaceExtrapolates()
    #DifferenceTableSI('SIandLI')
    #PlotRefSolutions()
    #FigureWorkPrecision('SI') #'SI','LI'
    #studyFinest()
    #TestTimeIE()
    #Test()
    #MeshInfoTab()
    #ComputationsCV()
    #ActivatationTimeTable()
    #CVTableSI()
    #ActvationRMSError()
    #ActvationRMSErrorGS()
    #MiddledActivatationTimeTable()
    EvaluationGSTab8()
