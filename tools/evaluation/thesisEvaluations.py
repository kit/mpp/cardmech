from utility import reading as read
from utility import computing as comp
from utility.plotting import Plot 
from utility.latexTables import Table
from utility.latexTables import TableWriter
from utility.documentWriter import DW
import Meshinfos
import matplotlib.pyplot as plt

pathTT='../../../data/biventricleTT/'
pathCoupled='../../../data/Kopplung/'

def getPointNamebyNumber(p,inRef=False):
        if inRef:
            if p==1:
                return '\\peins'
            elif p==2:
                return '\\pzwei'
            elif p==3:
                return '\\pdrei'
            elif p==4:
                return '\\pvier'
            elif p==5:
                return '\\pfuenf'
            elif p==6:
                return '\\psechs'
            elif p==7:
                return '\\psieben'
            elif p==8:
                return '\\pacht'
            elif p==9:
                return '\\pneun'
        else:    
            if p==1:
                return '\\Peins'
            elif p==2:
                return '\\Pzwei'
            elif p==3:
                return '\\Pdrei'
            elif p==4:
                return '\\Pvier'
            elif p==5:
                return '\\Pfuenf'
            elif p==6:
                return '\\Psechs'
            elif p==7:
                return '\\Psieben'
            elif p==8:
                return '\\Pacht'
            elif p==9:
                return '\\Pneun'

def Histplot(Path,language):
    path = Path+'SI0004/vtu/AT_l0j0.vtu'
    savePath=Path+'SI0004/tex/HistBiVentriclel0.tex'
    
    Meshinfos.comHistplot(path,savePath,language)

            
def MeshInfoTab(path,savepath,tablename,language):
    lL=[0,1,2,3,4,5]
    tL=[0,1,2,3]
    lL=[1,2,3]
    tL=[2,3]
    sdt=0.0004
    endTime=0.4
    SI=path
    fL=[SI]
    listEvaluationPoints=[]
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,fL,listEvaluationPoints)
    tabWriter.writeMeshInfoTableCoupled(SI,savepath,tablename,language)
    
def plotConvergenceActivationtime(path):
    lL=[0,1,2,3]
    tL=[0,1,2]#,3]
    SI=path+'SI0004/'
    sdt=0.0004
    endTime=0.1
    listEvaluationPoints=[]
    l0Path=path+'SI0004/vtu/AT_l0j0.vtu'
    #plotTact:
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    ValueArray=plot.MeanTactSpace(SI,l0Path,[])
    print('Plot finished')
    savePath='images/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[SI],listEvaluationPoints)
    L=0
    J=0
    tabWriter.writeMeanTactTable(SI,l0Path,savePath,L,J,SI,ValueArray)

def plotCa(path):
    lL=[1,2,3]
    tL=[2]
    SIOC=path+'SIOC0004/TestAusgaben/'
    sdt=0.0004
    endTime=0.4
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    #read.writeCaDataInFile(SIOC,lL,tL,1,len(listEvaluationPoints),True)
    j=2
    resize=4
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        #plot.plotSpaceForPointp(SIOC,p,j,1,resize,'SpaceConPurkinje') 
        plot.plotSpaceForPointp(SIOC,p,j,1,resize,'SpaceCa','Ca') 
    
def plotGamma(path):
    lL=[1]#,2,3]#,2]#[0,1,2,3]
    tL=[2]#[0,1,2]#,3]
    SIOC=path+'SIOC0004/TestAusgaben/'
    GS=path+'GS0004/'
    SI=path+'SI0004/'
    sdt=0.0004
    endTime=0.4
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    read.writeGammaDataInFile(GS,lL,tL,1,len(listEvaluationPoints),True)
    read.writeGammaDataInFile(SI,lL,tL,1,len(listEvaluationPoints),True)
    j=2
    resize=4
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        plot.plotSpaceForPointp(SI,p,j,1,resize,'SpaceSIGamma','Gamma') 
        plot.plotSpaceForPointp(GS,p,j,1,resize,'SpaceGSGamma','Gamma') 
    plot.plotSpaceForPointp(GS,10,j,1,resize,'SpaceGSGammaINfty','GammaInfty')
    plot.plotSpaceForPointp(SI,10,j,1,resize,'SpaceSIGammaINfty','GammaInfty')
def plotPotential(path):
    lL=[1]#,2,3]#,2]#[0,1,2,3]
    tL=[2]#,3]#[0,1,2]#,3]
    #SI=path+'SI0004/'
    SIOC=path+'SIOC0004/'
    SE=SIOC+'SE/'
    TestPrestressLevel=SIOC+'TestPRstressLevel1/'
    GS=path+'GS0004/'
    SI=path+'SI0004/'

    sdt=0.0004
    endTime=0.4
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    nL=['\\SIOC']#['\\SISVI']
    
    read.writeDataInFile(GS,[1],[2],1,True)
    read.writeDataInFile(SI,[1],[2],1,True)
    #read.writeDataInFile(SIOC,lL,tL,1,True)
    
    j=2
    resize=1
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        #plot.plotSpaceForPointp(SIOC,p,j,1,resize,'SpaceConPurkinje') 
        plot.plotSpaceForPointp(GS,p,j,1,resize,'SpaceGSV') 
        plot.plotSpaceForPointp(SI,p,j,1,resize,'SpaceSIV')
        
        
    endTime=0.5
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    #for p in listEvaluationPoints:
        #plot.plotSpaceForPointp(SE,p,j,1,resize,'SpaceConSE') 
        
        
        
        
        #(time_SIOC,V_SIOC)=read.getDataFromTXT(SIOC,read.createFilename(l,j,m),p)
    #(time_P,V_P)=read.getDataFromTXT(SE,read.createFilename(l,j,m),p)
    #(time_SI,V_SI)=read.getDataFromTXT(SI,read.createFilename(l,j,m),p)
    Vdiff=[]
    #for i in range(len(V_P)):
        #Vdiff.append(V_SIOC[i]-V_P[i])
        #if V_SIOC[i]-V_P[i]>0.001:
            #print(V_SIOC[i]-V_P[i])
    #plt.plot(time_P,V_P,label='SE')
    #plt.plot(time_SI,V_SI,label='SI')
    #plt.plot(time_SIOC,V_SIOC,label='Purkinje')
    #plt.legend()
    #plt.show()
    #for point in listEvaluationPoints:
        #(time,V)=read.getDataFromTXT(test,read.createFilename(2,j,m),point)
        #plt.plot(time,V,label=str(point))
    #plt.legend()
    #plt.show()
   # plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    #for p in listEvaluationPoints:
        #convergence in space
        #plot.plotSpaceForPointp(,p,2,1,1)   
   
    #plotallPoints,
    #plot.pointsInPlotForFixlAndjAndm(SI,3,2,1,1)

def writeConvergenceBiventricleActivationTime(language='eng'):
    path='../../../data/biventricleTT/SI0004/'
    path='../../../data/biventricleTT/KovachevaSI0004/'
    SI=path
    OnCellsSI ='../../../data/biventricleTT/OnCells/SI0004/'
    lL=[0,1,2]#,3]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.3
    listEvaluationPoints=[]
    saveInDiss=True
    
    #read.writeActivationDataFiles(SI,lL,tL,True)
    
    experimentslist=[SI]#,OnCellsSI]
    nameList=['SI']#['SIVert','SICells']
    experimentslist=[SI]
    #nameList=['TTSIVert']
    for i in range(len(experimentslist)):
        l0Path=experimentslist[i]+'vtu/AT_l0j0.vtu'
        plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
        ValueArray=plot.MeanTactSpace(experimentslist[i],l0Path,nameList[i],[],saveInDiss,language)
        print('Plot finished')
        savePath='figure/'
        tabWriter = TableWriter(lL,tL,sdt,endTime,1,[experimentslist[i]],listEvaluationPoints)
        L=0
        J=0
        tabWriter.writeMeanTactTable(experimentslist[i],l0Path,savePath,L,J,experimentslist[i],ValueArray,nameList[i],saveInDiss,language)

def writeConvergenceBiventricleActivationTimeBR(language='eng'):
    path='../../../data/biventricleBR/SI0004/'
    OnCellsSI ='../../../data/biventricleBR/OnCells/SI0004/'
    SI=path
    lL=[0,1,2]#,3]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.16
    listEvaluationPoints=[]
    saveInDiss=True
    #read.writeActivationDataFiles(OnCellsSI,lL,tL,True)
    experimentslist=[SI,OnCellsSI]
    nameList=['BRSIVert','BRSICells']
    for i in range(len(experimentslist)):
        l0Path=experimentslist[i]+'vtu/AT_l0j0.vtu'
        plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
        ValueArray=plot.MeanTactSpace(experimentslist[i],l0Path,nameList[i],[],saveInDiss,language)
        #print(ValueArray)
        print('Plot finished')
        savePath='figure/'
        tabWriter = TableWriter(lL,tL,sdt,endTime,1,[experimentslist[i]],listEvaluationPoints)
        L=0
        J=0
        tabWriter.writeMeanTactTable(experimentslist[i],l0Path,savePath,L,J,experimentslist[i],[],nameList[i],saveInDiss,language)

def CompareApproximationIion():
    eP='../../../data/cuttedEllipsoidBR/'
    fLI='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/LI0001SAT/wcV/'
    fSISVI='../../../data/cuttedEllipsoidBR/SISVI0001/'#SVI
    fSIICI='../../../data/cuttedEllipsoidBR/SIICI0001/'#ICI
    fSIOC= '../../../data/cuttedEllipsoidBR/SIOC0001/'#OC
    lL=[3,4]
    tL=[3,4,5]
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    #read.writeDataInFile(fSIICI,[3,4],[3,4,5],1)
    #read.writeDataInFile(fSISVI,[3],[3],1)
    #read.writeDataInFile(fSISVI,[3,4],[3,4,5],1)
    
    fL=[fSISVI,fSIICI,fSIOC]
    nL=['\\SVI','\\ICI','\\OC']
    tab=Table(lL,tL,sdt,endTime,1,fSISVI)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'ApproxIionCompRefTabP'+str(p)
        caption='Vergleich des \\SVI, \\ICI~und \\OC~Ansatz für das semi-implizite Verfahren (SI) mit verschiedenen Zeit- und Ortsdiekretisierungen'
        label='tab:ApproxIionCompRefP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginApproxIiionTable(p,caption,label,fL))
            output_file.write(tab.CompareApproxIion(p,5,6,fL,nL,'\\mathrm{SI}'))
            output_file.write(tab.writeEndTable())
            
    
    
        
      
    filename=eP+'tex/'+'CompDurApproxIion'
    caption='Rechenzeit (Stunden:Minuten:Sekunden) und Anzahl der Prozesse für den \\SVI, \\ICI und \\OC~Ansatz mit dem semi-impliziten Verfahren (SI) auf dem abgeschnittenten Ellipsoid'
    label='tab:DurApproxIiion'
    with open(filename+'.tex', 'w') as output_file:
        output_file.write(tab.writeBeginDurationTabList(caption,label,fL))
        output_file.write(tab.writeDurAProcs(fL,nL))
        output_file.write(tab.writeEndTable())
    
    
def CompareApproxWithDur(case='SI'):
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    if case=='SI':
        eP='../../../data/cuttedEllipsoidBR/'
        fSISVI='../../../data/cuttedEllipsoidBR/SISVI0001/'#SVI
        fSIICI='../../../data/cuttedEllipsoidBR/SIICI0001/'#ICI
        fSIOC= '../../../data/cuttedEllipsoidBR/SIOC0001/'#OC
        fSIOCSVI='../../../data/cuttedEllipsoidBR/SIOCSVI0001/'#OC-SVI
        lL=[3,4]
        tL=[3,4,5]
        
        #read.writeDataInFile(fSIICI,[3,4],[3,4,5],1)
        #read.writeDataInFile(fSISVI,[3],[3],1)
        #read.writeDataInFile(fSISVI,[3,4],[3,4,5],1)
    
        fL=[fSISVI,fSIICI,fSIOC]
        nL=['\\SVI','\\ICI','\\OC']
        tab=Table(lL,tL,sdt,endTime,1,fSISVI)
        filename=eP+'tex/'+'CompApproxWithDurSI'
        caption='Genauigkeiten der Varianten \\SVI, \\ICI~und \\OC~bezogen auf die Referenzlösung $\V^\\Ref_\\SI$ in Auswertungspunkt $\\psechs$ für das semi-implizite Verfahren (SI) mit verschiedenen Zeit- und Ortslevel inklusive der Rechenzeit (Stunden:Minuten:Sekunden) und der Anzahl der Prozesse  auf dem abgeschnittenten Ellipsoid'
        label='tab:CompareApproxWithDurSI'
        p=6
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginApproxAndDurationTabList(p,caption,label,fL))
            output_file.write(tab.ApproxIionAndDuration(p,5,6,fL,nL,'\\mathrm{SI}'))
            output_file.write(tab.writeEndTable())
    elif case=='All-SI':
        eP='../../../data/cuttedEllipsoidBR/'
        fSISVI='../../../data/cuttedEllipsoidBR/SISVI0001/'#SVI
        fSIICI='../../../data/cuttedEllipsoidBR/SIICI0001/'#ICI
        fSIOC= '../../../data/cuttedEllipsoidBR/SIOC0001/'#OC
        fSIOCSVI='../../../data/cuttedEllipsoidBR/SIOCSVI0001/'#OC-SVI
        fL=[fSISVI,fSIICI,fSIOC,fSIOCSVI]
        nL=['\\SVI','\\ICI','\\OC','\\OC-\\SVI']
        lL=[0,1,2,3,4]
        tL=[0,1,2,3,4,5]
        read.writeDataInFile(fSIICI,[4],[4,5],1)
        read.writeDataInFile(fSIOC,[4],[4,5],1)
        tab=Table(lL,tL,sdt,endTime,1,fSISVI)
        p=6
        filename=fL[0]+'tex/'+'CompRefAndDurationAllSITabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Vergleich der Varianten vom semi-impliziten Verfahren (SI) im Auswertungspunkt $\\psechs$ auf dem abgeschnitten Ellipsoid'
            label = 'tab:CompRefAndDurationAllSIP'+str(p)
            output_file.write(tab.writeBeginApproxAndDurationTabList(p,caption,label,fL))
            output_file.write(tab.ApproxIionAndDuration(p,5,6,fL,nL,'\\mathrm{SI}'))
            output_file.write(tab.writeEndTable())
    elif case=='ProcTest':
        eP='../../../data/cuttedEllipsoidBR/ProzessorTest/'
        ref='../../../data/cuttedEllipsoidBR/SISVI0001/'
        fSISVI='../../../data/cuttedEllipsoidBR/ProzessorTest/SISVI0001/'#SVI
        fSIICI='../../../data/cuttedEllipsoidBR/ProzessorTest/SIICI0001/'#ICI
        fSIOC= '../../../data/cuttedEllipsoidBR/ProzessorTest/SIOC0001/'#OC
        fSIOCSVI='../../../data/cuttedEllipsoidBR/ProzessorTest/SIOCSVI0001/'#OC-SVI
        fL=[fSISVI,fSIICI,fSIOC,fSIOCSVI]
        nL=['\\SVI','\\ICI','\\OC','\\OC-\\SVI']
        lL=[0]
        tL=[0,1,2,3,4,5]
        read.writeDataInFile(fSIICI,[0],tL,1)
        read.writeDataInFile(fSIOC,[0],tL,1)
        read.writeDataInFile(fSISVI,[0],tL,1)
        read.writeDataInFile(fSIOCSVI,[0],tL,1)
        tab=Table(lL,tL,sdt,endTime,1,ref)
        p=6
        filename=fL[0]+'tex/'+'CompRefAndDurationProcTestTabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Vergleich der Varianten vom semi-impliziten Verfahren (SI) im Auswertungspunkt $\\psechs$ auf dem abgeschnitten Ellipsoid'
            label = 'tab:CompRefAndDurationProcTestP'+str(p)
            output_file.write(tab.writeBeginApproxAndDurationTabList(p,caption,label,fL))
            output_file.write(tab.ApproxIionAndDuration(p,5,6,fL,nL,'\\mathrm{SI}'))
            output_file.write(tab.writeEndTable())
    elif case=='LI':
        eP='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/'
        sP='../../../data/cuttedEllipsoidBR/'
        fLISVI=eP+'LI0001SAT/wcV/'#standard LI0001SAT
        fLIICI=eP+'LI0001SAT/ICI/'
        lL=[3,4]
        tL=[3,4,5]
        sdt=0.0001
        endTime=0.03
        listEvaluationPoints=[4,5,6,7,8]
        fL=[fLISVI,fLIICI]
        nL=['\\SVI','\\ICI']
        tab=Table(lL,tL,sdt,endTime,1,fLISVI)
        filename=sP+'tex/'+'CompApproxWithDurLI'
        p=6
        caption='Genauigkeiten der Varianten \\SVI~und \\ICI~bezogen auf die Referenzlösung $\V^\\Ref_\\LI$ in Auswertungspunkt $\\psechs$ für das linear implizite Verfahren (LI) mit verschiedenen Zeit- und Ortslevel inklusive der Rechenzeit (Stunden:Minuten:Sekunden) und der Anzahl der Prozesse  auf dem abgeschnittenten Ellipsoid'
        label='tab:CompareApproxWithDurLI'
        
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginApproxAndDurationTabList(p,caption,label,fL))
            output_file.write(tab.ApproxIionAndDuration(p,4,6,fL,nL,'\\mathrm{LI}'))
            output_file.write(tab.writeEndTable())
    else:
        print('Case not defined')
def CompareWithRef(option='SI'):
    eP='../../../data/cuttedEllipsoidBR/'
    fSI=eP+'SISVI0001/'
    fSIOCSVI= eP+'SIOCSVI0001/'
    #read.writeDataInFile(fSI,[0,1,2,3,4],[0,1,2,3,4,5],1)
    #read.writeDataInFile(fSI,[5],[4],1)
    #read.writeDataInFile(fSI,[6],[5],1)
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[4,5,6,7,8]
    
    lL=[0,1,2,3,4]
    tL=[0,1,2,3,4,5]
    fL=[fSI,fSIOCSVI]
    nL=['\\SISVI','\\OC-\\SVI']
    tab=Table(lL,tL,sdt,endTime,1,fL[0])
    if option=='SI':
        for p in listEvaluationPoints:
            filename=fL[0]+'tex/'+'SICompRefTabP'+str(p)
            with open(filename+'.tex', 'w') as output_file:
                caption='Value table of semi-implicit (\\SISVI)  approach for the component wise splitting on different levels and time step sizes for $\\frac{\\|\\V^{j,\\ell}(\\cdot, \\textbf{P'+str(p)+'})- \\V^{5,6}(\\cdot,\\textbf{P'+str(p)+'})\\|_\\LtwonormT}{\\|\\V^{5,6}(\\cdot,\\textbf{P'+str(p)+'})\\|_\\LtwonormT}$'
                label='tab:SICompRefP'+str(p)
                output_file.write(tab.writeBeginSpaceLengthTable(p,caption,label))
                output_file.write(tab.writeCompareRef(p,5,6))
                output_file.write(tab.writeEndTable())
                
    elif option=='SI-OCSVI':
        p=6
        filename=fL[0]+'tex/'+'CompRefAndDurationTabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Vergleich SI und Variante SI-OC-SVI im Auswertungspunkt $\\psechs$ auf dem abgeschnitten Ellipsoid'
            label = 'tab:CompRefAndDurationP'+str(p)
            output_file.write(tab.writeBeginApproxAndDurationTabList(p,caption,label,fL))
            output_file.write(tab.ApproxIionAndDuration(p,5,6,fL,nL,'\\mathrm{SI}'))
            output_file.write(tab.writeEndTable())
            
def PlotTwoMethods(case ='ellipsoid'):
    if case =='ellipsoid':
        lL=[0,1,2,3,4]
        tL=[0]
        sdt=0.0001
        endTime=0.03
        listEvaluationPoints=[1,2,3,4,5,6,7]
        eP='../../../data/cuttedEllipsoidBR/'
        fSISVI=eP+'SISVI0001/'
        f2=eP+'SIOC0001/'
    
        #read.writeDataInFile(fSISVI,[0,1,2,3,4],[0],1)
        #read.writeDataInFile(fSIOC,[1,2,3],[0],1)
    
        fL=[fSISVI,f2]
        nL=['\\SVI','\\OC']        
        plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
        j=3
        for p in listEvaluationPoints:
            plot.plotVTwoTimeIntMethodsSpace(fSISVI,f2,p,j,eP,'DiffSVIOCSVISpacej'+str(j)+'P'+str(p))
            #plot.plotVTwoTimeIntMethodsTimeRef(fSI,fLI,p,3,fLI,6,4,eP,'DiffSILITimel3'+'P'+str(p))
    elif case =='biventricle':
        lL=[1,2,3]#,1,2,3]
        tL=[3]
        sdt=0.0004
        endTime=0.5
        listEvaluationPoints=[1,2,3,4,5,6,7,8]
        eP='../../../data/biventricleTT/talkDifferentDiffusion/'
        fSISVI=eP+'SI0004/'
        f2=eP+'SIOC0004/'
    
        #read.writeDataInFile(fSISVI,lL,[2],1)
        #read.writeDataInFile(f2,lL,[2],1)
        fL=[fSISVI,f2]
        nL=['\\SVI','\\OC']        
        plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
        j=2
        for p in listEvaluationPoints:
            plot.plotVTwoTimeIntMethodsSpace(fSISVI,f2,p,j,eP,'BIVentricleDifferentDiffusionDiffSVIOCSpacej'+str(j)+'P'+str(p),nL,'ten Tusscher--Panfilov')
    elif case=='biventricleBR':
        lL=[1,2,3]#,1,2,3]
        tL=[2]
        sdt=0.0004
        endTime=0.5
        listEvaluationPoints=[1,2,3,4,5,6,7,8]
        eP='../../../data/biventricleBR/talk/'
        fSISVI=eP+'SI0004/'
        f2=eP+'SIOC0004/'
    
        #read.writeDataInFile(fSISVI,lL,[2],1)
        #read.writeDataInFile(f2,lL,[2],1)
        fL=[fSISVI,f2]
        nL=['\\SVI','\\OC']        
        plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
        j=2
        for p in listEvaluationPoints:
            plot.plotVTwoTimeIntMethodsSpace(fSISVI,f2,p,j,eP,'BIVentricleBRDiffSVIOCSpacej'+str(j)+'P'+str(p),nL,'Beeler--Reuter')
    else:
        print('case not known')
def ConvergenceLTwoNorm():
    lL=[2,3,4]#,1,2,3]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    eP='../../../data/biventricleTT/SimpleExcitation/'
    fSISVI=eP+'SI0004/'
    fSIOC=eP+'SIOC0004/'
    nL=['\\SISVI','\\SIOC']
    fL=[fSISVI,fSIOC]
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSISVI],listEvaluationPoints)
    savePath='../../../thesis-lindner/thesis/tables/erstmal/'
    threshold=-50.0
    tabWriter.witeLtwoNormTab(fL,nL,savePath)
def ConvergenceDurationAP():
    lL=[2,3]#,1,2,3]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    eP='../../../data/biventricleTT/SimpleExcitation/'
    fSISVI=eP+'SI0004/'
    nL=['\SISVI']
    fL=[fSISVI]
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSISVI],listEvaluationPoints)
    savePath='../../../thesis-lindner/thesis/tables/erstmal/'
    threshold=-50.0
    tabWriter.writeAPDTable(fL,nL,savePath,threshold)
def ConvergenceActivationTime():
    lL=[2,3]#,1,2,3]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    eP='../../../data/biventricleTT/SimpleExcitation/'
    fSISVI=eP+'SI0004/'
    nL=['\\SISVI']
    fL=[fSISVI]
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSISVI],listEvaluationPoints)
    savePath='../../../thesis-lindner/thesis/tables/erstmal/'
    threshold=-50.0
    tabWriter.writeTactTable(fL,nL,savePath,threshold)
def plotOneMethod():
    lL=[2,3]#,1,2,3]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    eP='../../../data/biventricleTT/SimpleExcitation/'
    fSISVI=eP+'SI0004/'
    
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        #convergence in space
        plot.plotSpaceForPointp(fSISVI,p,2,1,1,'SISVI')   

def plotAllPoints():
    lL=[2,3]#,1,2,3]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    eP='../../../data/biventricleTT/SimpleExcitation/'
    fSISVI=eP+'SI0004/'

    
    #read.writeDataInFile(fSISVI,lL,tL,1)       
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    l=3
    j=0
    plot.pointsInPlotForFixlAndjAndm(fSISVI,l,j,1,1,'TTSE')
def EvaluationsForBiventricle(case):
    eP=''
    if case == 'TT':
        eP='../../../data/biventricleTT/SimpleExcitation/NiedererDiffusion/'
    elif case =='TTAD':
        eP='../../../data/biventricleTT/SimpleExcitation/atrialDiffusion/'
    elif case=='BR':
        eP='../../../data/biventricleBR/SimpleExcitation/'
    elif case =='TTGD':
        eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    else:
        print('case is not defined')
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]

    fSISVI=eP+'SI0004/'
    fSIOC=eP+'SIOC0004/'
    fSIICI=eP+'SIICI0004/'
    fGS=eP+'GS0004/'
    fGStheta1=eP+'GS0004/theta1/'
    nL=['\\SISVI','\\SIOC','\\SIICI','\\GPDE','\\GShalve']
    fL=[fSISVI,fSIOC,fSIICI,fGStheta1,fGS]
    
    #read.writeDataInFile(fSISVI,lL,tL,1)
    #read.writeDataInFile(fSIOC,lL,tL,1)
    #read.writeDataInFile(fSIICI,lL,tL,1)
    #read.writeDataInFile(fGStheta1,lL,[1,2,3,4],1)
    
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSISVI],listEvaluationPoints)
    savePath=''
    if case == 'TT':
        savePath='../../../thesis-lindner/thesis/tables/erstmal/TTND/'
    elif case =='TTAD':
        savePath='../../../thesis-lindner/thesis/tables/erstmal/TTAD/'
    elif case=='BR':
        savePath='../../../thesis-lindner/thesis/tables/erstmal/BR/'
    elif case =='TTGD':
        savePath='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    else:
        print('case is not defined')
    
    threshold=-50.0
    
    #tabWriter.writeTactTable(fL,nL,savePath,threshold)
    #print('wrote tact Tab')
    #tabWriter.writeAPDTable(fL,nL,savePath,threshold)
    #print('wrote apd Tab')
    #tabWriter.witeLtwoNormTab(fL,nL,savePath,False)
    #print('wrote L_2-norm Tab')
    #tabWriter.writeIntervalLtwoNormTab(fL,nL,savePath,threshold)
    
    #All Points
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    l=3
    j=1
    plot.pointsInPlotForFixlAndjAndm(fSIOC,l,j,1,3,'SEOC','\\SIOC')
    plot.pointsInPlotForFixlAndjAndm(fSISVI,l,j,1,3,'SESVI','\\SISVI')
    plot.pointsInPlotForFixlAndjAndm(fSIICI,l,j,1,3,'SEICI','\\SIICI')
    plot.pointsInPlotForFixlAndjAndm(fGS,l,j,1,3,'SEGS','\\GShalve')
    plot.pointsInPlotForFixlAndjAndm(fGStheta1,l,j,1,3,'SEGSteta1','\\GPDE')
    print('plotted all Points')
    j=3
    l=3
    for p in listEvaluationPoints:
        #plot.plotVTwoTimeIntMethodsSpace(fSISVI,fSIOC,p,j,eP,'BIVentricleSEDiffSVIOCSpace',['\\SISVI','\\SIOC'],'in '+getPointNamebyNumber(p,False)+'\\, with $j='+str(j)+'$')
        #plot.plotVTwoTimeIntMethodsSpace(fSIICI,fGS,p,j,eP,'BIVentricleSEDiffICIGSSpace',['\\SIICI','\\GPDE'],'in '+getPointNamebyNumber(p,False)+'\\, with $j='+str(j)+'$')
        plot.plotVTwoTimeIntMethodsTime(fSISVI,fSIOC,p,l,'',eP,'BIVentricleSEDiffSVIOCTimel'+str(l)+'P'+str(p),['\\SISVI','\\SIOC'],'in '+getPointNamebyNumber(p,False)+'\\, with $\\ell='+str(l)+'$')
        plot.plotVTwoTimeIntMethodsTime(fSIICI,fGS,p,l,'',eP,'BIVentricleSEDiffICIGSTimel'+str(l)+'P'+str(p),['\\SIICI','\\GShalve'],'in '+getPointNamebyNumber(p,False)+'\\, with $\\ell='+str(l)+'$')

def WriteDurationPerTimeStepTab():
    lL=[2,3,4]
    tL=[0]
    sdt=0.0004
    endTime=0.004
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/TimeTest/'
    fN=['logSId','logSIOCd','logSIICId','logGSAddd'] #,'logGSd'
    nL=['\\SISVI','\\SIOC','\\SIICI','\\GPDE']
    sP='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fN[0]],[])
    tabWriter.writeComparisionDurationTab(eP,fN,sP,nL,5)
    
    
    
def TimeDiffSIAndSIOC():
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/TimeTest/DifferenceSI-SIOC/log/'
    fN=['logSI','logSIOC']
    gatingTime=[]
    concentrationTime=[]
    PDETime=[]
    for f in fN:
        (gating,conc,pde)=read.getSpecialisedTimeDurationsAveraged(eP+f)
        gatingTime.append(gating)
        concentrationTime.append(conc)
        PDETime.append(pde)
    print(gatingTime,concentrationTime,PDETime)

def activationTimeExtrapolateTab():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    
    threshold=-50.0
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    fSIOC=eP+'SIOC0004/'
    
    sP='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    nL=['\\SIOC']
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    tabWriter.writeActivationExtrapolationTab(eP,[fSIOC],sP,nL,threshold)
    
def VExtraTab():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    
    threshold=-50.0
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    fSIOC=eP+'SIOC0004/'
    
    sP='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    nL=['\\SIOC']
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    tabWriter.writeVExtrapolationTab(eP,[fSIOC],sP,nL,threshold)
    
def ErrorExtraVInt():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    
    threshold=-50.0
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    fSISVI=eP+'SI0004/'
    fSIOC=eP+'SIOC0004/'
    fSIICI=eP+'SIICI0004/'
    fGS=eP+'GS0004/'
    nL=['\\SISVI','\\SIOC','\\SIICI','\\GPDE']
    fL=[fSISVI,fSIOC,fSIICI,fGS]
    sP='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    tabWriter.writeErrorExtraVIntTab(eP,fL,sP,nL,threshold)

def plotExtrapolates():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    fSI=eP+'SIOC0004/'
    
    p=8
   # V_full=comp.getFullExtrapolate(lL[-1],tL[-1],1,8,sdt,endTime,fSI)
    #V_space=comp.getExtrapolateInSpace(lL[-1],tL[-1],1,8,sdt,endTime,fSI)
    #V_time=comp.getExtrapolateInTime(lL[-1],tL[-1],1,8,sdt,endTime,fSI)
    
    tact_ref=comp.getTextFullExtrapolate(lL[-1],tL[-1],1,p,fSI,-50.0)
    (t_s,VInt_space)=comp.getVIntExtrapolateInSpace(lL[-1],tL[-1],1,8,fSI,sdt,-50.0,0.4,tact_ref)
    (t_t,VInt_time)=comp.getVIntExtrapolateInTime(lL[-1],tL[-1],1,8,fSI,sdt,-50.0,0.4,tact_ref)
    (t_f,VInt_full)=comp.getVIntFullExtrapolate(lL[-1],tL[-1],1,8,fSI,sdt,-50.0,0.4,tact_ref)

    #plt.plot(t_t,VInt_time,label='V time')
    #plt.plot(t_s,VInt_space,label='V space')
    plt.plot(t_f,VInt_full,label='V full')
    j=3
    l=5
    Diff=[]
    for i in range(len(VInt_time)):
        Diff.append(VInt_space[2*i]-VInt_time[i])
    
    #plt.plot(t_t,Diff,label='V space - V time')    
    for l in lL:
        (t,V)=read.getDataFromTXT(fSI,read.createFilename(l,j,1),p)
        (tact,id_tact)=comp.getActivaitionTimeFromVWithID(t,V,-50.0)
        n=int(0.4/(sdt*2**(-j)))
        id_dur=id_tact+n
        plt.plot(t[id_tact:id_dur],V[id_tact:id_dur],label='$l='+str(l)+'$')
                       
    #plt.plot(time,V_time,label='V time')
    #plt.plot(time,V_space,label='V space')
    plt.legend()
    plt.show()

def DifferenceTabs(case):
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    
    
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    fSISVI=eP+'SI0004/'
    fSIOC=eP+'SIOC0004/'
    fSIICI=eP+'SIICI0004/'
    fGS=eP+'GS0004/'
    nL=['\\SISVI','\\SIOC','\\SIICI','\\GPDE']
    fL=[fSISVI,fSIOC,fSIICI,fGS]
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSISVI],listEvaluationPoints)
    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    l=5
    j=3
    if case=='V':
        tabWriter.writeDifferenceTable(l,j,fL,nL,savePath,case)
    elif case=='tact':
        threshold=-50.0
        tabWriter.writeDifferenceTable(l,j,fL,nL,savePath,case,threshold)
    else:
        print('case ', case,'not known')
def DifferencePerPointTabs(case):
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    
    
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    fSISVI=eP+'SI0004/'
    fSIOC=eP+'SIOC0004/'
    fSIICI=eP+'SIICI0004/'
    fGS=eP+'GS0004/'
    nL=['\\SISVI','\\SIOC','\\SIICI','\\GPDE']
    fL=[fSISVI,fSIOC,fSIICI,fGS]
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSISVI],listEvaluationPoints)
    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    l=5
    j=3
    threshold=-50.0
    tabWriter.writeDifferenceTablePerPoint(l,j,fL,nL,savePath,case,threshold)

def plotSchwinger():
    lL=[2,3,4,5]
    tL=[0]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    listEvaluationPoints=[8]
    
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    fSISVI=eP+'SI0004/'
    fSIOC=eP+'SIOC0004/'
    fSIICI=eP+'SIICI0004/'
    fGS=eP+'GS0004/'
    nL=['\\SISVI','\\SIOC','\\SIICI','\\GPDE']
    fL=[fSISVI,fSIOC,fSIICI,fGS]
    threshold=-50.0
    
    savePath='../../../thesis-lindner/thesis/images/erstmal/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=3
    for p in listEvaluationPoints:
        plot.Unterschwinger(fL,nL,threshold,p,j,savePath)
        plot.Oberschwinger(fL,nL,threshold,p,j,savePath)
def tabUnterschwinger():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    
    
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    fSISVI=eP+'SI0004/'
    fSIOC=eP+'SIOC0004/'
    fSIICI=eP+'SIICI0004/'
    fGS=eP+'GS0004/'
    nL=['\\SISVI','\\SIOC','\\SIICI','\\GPDE']
    fL=[fSISVI,fSIOC,fSIICI,fGS]
    threshold=-50.0
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSISVI],listEvaluationPoints)
    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    tabWriter.writeSchwingerPerPoint(fL,nL,savePath,threshold)
    #tabWriter.writeUnterschwingerPerPoint(fL,nL,savePath,threshold)
def tactValuesAndErrorPerPoint():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    
    
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    fSISVI=eP+'SI0004/'
    fSIOC=eP+'SIOC0004/'
    fSIICI=eP+'SIICI0004/'
    fGS=eP+'GS0004/'
    nL=['\\SISVI','\\SIOC','\\SIICI','\\GPDE']
    fL=[fSISVI,fSIOC,fSIICI,fGS]
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSISVI],listEvaluationPoints)
    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    threshold=-50.0
    tabWriter.writetactAndErrorPerPoint(fL,nL,savePath,case,threshold)

def plotTactErrorPerPoint():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    listEvaluationPoints=[8]
    
    threshold=-50.0
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
    fSISVI=eP+'SI0004/'
    fSIOC=eP+'SIOC0004/'
    fSIICI=eP+'SIICI0004/'
    fGS=eP+'GS0004/'
    nL=['\\SISVI','\\SIOC','\\SIICI','\\GPDE']
    fL=[fSISVI,fSIOC,fSIICI,fGS]
    
    savePath='../../../thesis-lindner/thesis/images/erstmal/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=tL[-1]
    l=lL[-1]
    for p in listEvaluationPoints:
        plot.tactErrorInSpace(fL,nL,threshold,p,j,savePath)
        plot.tactErrorInTime(fL,nL,threshold,p,l,savePath)
    
    
    
def plotSpaceConVolume(path,j,lL,plotname,resize):
    lvRvLabel=["LV volume", "RV volume"]
    for l in lL:
       vol, dataTime, dataStep = read.VolumeDataFromLog(path, read.createFilename(l,j,1))
       resizeLV=[]
       resizeRV=[]
       resizeVol=[]
       resizetime=[]
       number=resize*2**(j)
       
       for i in range(len(dataTime)):
            if i%number==0:
                resizetime.append(dataTime[i])
                resizeLV.append(vol[0][i])
                resizeRV.append(vol[1][i])
       resizeVol.append(resizeLV)
       resizeVol.append(resizeRV)
       
       for i in range(len(resizeVol)):
           plt.plot(resizetime, resizeVol[i], label = lvRvLabel[i]+', $\ell='+str(l)+'$',linewidth=2.5)
    plt.legend(bbox_to_anchor=(1.05,1.00), loc="upper left")
    plt.xlabel('Zeit (s)')
    plt.ylabel('Volumen (ml)')
    #plt.show()
    import tikzplotlib
    tikzplotlib.save(path+'/tex/'+plotname+ ".tex")
    plt.close()
    
def plotVolume():
    path='../../../data/Kopplung/' 
    SIOC=path+'SIOC0004/'
    SE =SIOC+'SE/'
    TestPrestressLevel=SIOC+'TestPRstressLevel1/'
    GS=path+'GS0004/'
    SI=path+'SI0004/'
    lL=[1]#,3]
    tL=[2]
    #Purkinje
    j=2
    resize=4
    plotname='spaceConVolumesGSj'+str(j)
    plotSpaceConVolume(GS,j,lL,plotname,resize)
    plotname='spaceConVolumesSIj'+str(j)
    plotSpaceConVolume(SI,j,lL,plotname,resize)
    #plotname='spaceConVolumesPurnkinjej'+str(j)
    #plotSpaceConVolume(SIOC,j,lL,plotname,resize)
    #plotname='spaceConVolumesSEj'+str(j)
    #plotSpaceConVolume(SE,j,lL,plotname,resize)
    
def evaluateL2Ellipsoid():
    eP='../../../data/cuttedEllipsoidBR/'
    fLI='../../../datafiles/TestEllipsoid3d/TestsFromHoreka/LI0001SAT/wcV/'
    fSISVI='../../../data/cuttedEllipsoidBR/SISVI0001/'#SVI
    #fSISVI='../../../datafiles/TestsFromHoreka/SI0001SAT/'#SVI
    fSIICI='../../../data/cuttedEllipsoidBR/SIICI0001/'#ICI
    fSIOC= '../../../data/cuttedEllipsoidBR/SIOC0001/'#OC
    fGS='../../../data/cuttedEllipsoidBR/GS0001/' #GS
    lL=[2,3,4]
    tL=[1,2,3]
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[8]

    
    #read.writeDataInFile(fGS,lL,tL,1)
    fL=[fSISVI,fSIOC,fGS]
    nL=['\\SISVI','\\SIOC','\\GPDE']
    #fL=[fSISVI]
    #nL=['\\SVI']
    savePath='../../../thesis-lindner/thesis/images/erstmal/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=tL[-1]
    l=4 #lL[-1]
    p=8
    resize =10
    plot.L2space(fL,nL,p,j,savePath,fSISVI,resize)
    plot.L2time(fL,nL,p,l,savePath,fSISVI,resize)
    tab=Table(lL,tL,sdt,endTime,1,fSISVI)
    for p in listEvaluationPoints:
        filename='../../../thesis-lindner/thesis/tables/erstmal/'+'L2ErrorEllipsoidTabP'+str(p)
        caption='Der Fehler $\\errorL^{j,\ell}$ des Verlaufs von $\\SpaceNorm^{j,\\ell}$ in Ort und Zeit bezüglich der Referenzlösung $\\SpaceNorm^\\Ref$'# gemessen in der $\\LtwonormT$-Norm  '
        label='tab:L2EllipErrorP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginApproxAndDurationTabList(p,caption,label,fL,False))
            output_file.write(tab.L2ErrorTab(p,5,6,fL,nL))
            output_file.write(tab.writeEndTable())
    
if __name__=="__main__":
    language='d'
    #MeshInfoTab(pathCoupled+'SIOC0004/','../../../thesis-lindner/thesis/tables/','MeshInfoSpaceBiventricleCoupled',language)
    #MeshInfoTab(pathTT+'SI0004/','../../../thesis-lindner/thesis/tables/','MeshInfoSpaceBiventricle',language)
    #Histplot(pathTT,language)
    #writeConvergenceBiventricleActivationTime(language)
    #writeConvergenceBiventricleActivationTimeBR(language)
   # CompareApproximationIion()
    #CompareApproxWithDur('SI')
    #CompareApproxWithDur('LI')
    #CompareApproxWithDur('All-SI')
    #CompareApproxWithDur('ProcTest')
    #CompareWithRef('SI-OCSVI')
    #PlotTwoMethods('ellipsoid')
    #PlotTwoMethods('biventricleBR')
    #PlotTwoMethods('biventricle')
    #plotAllPoints()
    #plotOneMethod()
    #ConvergenceActivationTime()
    #ConvergenceDurationAP()
    #ConvergenceLTwoNorm()
    case='TTAD'
    #EvaluationsForBiventricle(case)
    case='TT'
   # EvaluationsForBiventricle(case)
    case='BR'
    #EvaluationsForBiventricle(case)
    case ='TTGD'
    #EvaluationsForBiventricle(case)
    
    #plotExtrapolates()
    diffcase ='tact' #'V'
    #DifferenceTabs(diffcase)
    diffcase='VDiffInt'
    #DifferencePerPointTabs(diffcase)
    diffcase='VInt'
    #DifferencePerPointTabs(diffcase)
    diffcase ='tact'
   # DifferencePerPointTabs(diffcase)
    #WriteDurationPerTimeStepTab()
    #TimeDiffSIAndSIOC()
    #activationTimeExtrapolateTab()
    #VExtraTab()
    #ErrorExtraVInt()
    #tactValuesAndErrorPerPoint()
    #plotTactErrorPerPoint()
    #tabUnterschwinger()
    #plotSchwinger()

    #plotPotential('../../../data/Kopplung/')
    #plotVolume()
    #plotCa('../../../data/Kopplung/')
    #plotGamma('../../../data/Kopplung/')
    
    evaluateL2Ellipsoid()
    
