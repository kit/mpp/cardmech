from utility import reading as read
from utility import computing as comp
import matplotlib.pyplot as plt
from matplotlib.ticker import LinearLocator
import numpy as np


durEvalActi=0.4


class Plot:
    def __init__(self, lList, tList,start_dt,endTime,substep,evaluationPoints):
        self.lL = lList
        self.tL = tList
        self.m = substep
        self.sdt = start_dt
        self.T =endTime
        self.evalP = evaluationPoints
        self.colorList = ['black','red','blue','orange','green','purple','brown','cyan','grey']
        self.markerList=['s','o','d','v']
        self.markerSizeList=[8,5,4,4]
        self.linestyleList=['-','--',':','-.']
    def getPointNamebyNumber(self,p):
        if p==1:
            return '\\Peins'
        elif p==2:
            return '\\Pzwei'
        elif p==3:
            return '\\Pdrei'
        elif p==4:
            return '\\Pvier'
        elif p==5:
            return '\\Pfuenf'
        elif p==6:
            return '\\Psechs'
        elif p==7:
            return '\\Psieben'
        elif p==8:
            return '\\Pacht'
        elif p==9:
            return '\\Pneun'
        elif p==10:
            return '\\LtwoSpace'
    
    def getTimeID(self,id_tact,j):
        ID=-1
        n=int(durEvalActi/(self.sdt*2**(-j)))
        ID=id_tact+n
        return ID
    
    def plotInP(self,p,l,j,fL,nL,sP,plotLegend=False,resize=1):
        for index in range(len(fL)):
            (time,V)= read.getDataFromTXT(fL[index],read.createFilename(l,j,self.m),p)
            for i in range(len(V)):
                V[i] = float(V[i])
            (time,V)=self.resizeData(time,V,resize*2**(j),len(time))
            labelname = nL[index]
            plt.plot(time,V, label = labelname)
        plotname ='DiffSchemesl' +str(l)+ 'j'+str(j)+'P'+str(p)
        self.plotData()
        if plotLegend:
            plt.legend(bbox_to_anchor=(1.1,1), loc="upper left")
        self.saveTikz(sP+'/tex/', plotname)
        plt.close()
    def plotSchemesInP(self, p,l,j,L,J,fL,nL,refF,refN,sP,plotLegend=False,resize=1):
        for index in range(len(fL)):
            (time,V)= read.getDataFromTXT(fL[index],read.createFilename(l,j,self.m),p)
            for i in range(len(V)):
                V[i] = float(V[i])
            (time,V)=self.resizeData(time,V,resize*2**(j),len(time))
            labelname = nL[index]
            plt.plot(time,V, label = labelname)
        (t,V)=read.getDataFromTXT(refF,read.createFilename(L,J,self.m),p)
        for i in range(len(V)):
                V[i] = float(V[i])
        (t,V)=self.resizeData(t,V,resize*2**(J),len(t))
        labelname = refN+'~$\ell='+str(L)+'$, $j='+str(J)+'$'
        plt.plot(t,V, label = labelname)
        plotname ='DiffSchemesl' +str(l)+ 'j'+str(j)+'P'+str(p)
        self.plotData()
        if plotLegend:
            plt.legend(bbox_to_anchor=(1.1,1), loc="upper left")
        self.saveTikz(sP+'/tex/', plotname)
        plt.close()
    def plotL2NormData(self):
        xmin, xmax, ymin, ymax = 0.0, self.T+0.05, -100.0,70.0
        plt.xlabel('Zeit (s)')
        plt.ylabel('$\\SpaceNorm$')
    def L2space(self,fL,nL,p,j,savePath,ref,resize):
        counter=0
        for alg in range(len(fL)):
            for l in self.lL:
                (time,V)= read.getDataFromTXT(fL[alg],read.createFilename(l,j,1),p)
                (time,V)=self.resizeData(time,V,resize,len(time))
                labelname = nL[alg]+'~$\\ell='+str(l)+'$'
                plt.plot(time,V, label = labelname,color=self.colorList[l-self.lL[0]],linestyle=self.linestyleList[counter],linewidth=2.5)
            counter+=1
        (t_ref,Vref)=read.getDataFromTXT(ref,read.createFilename(6,5,1),p)
        (t_ref,Vref)=self.resizeData(t_ref,Vref,resize,len(t_ref))
        plt.plot(t_ref,Vref, label = '$\\SpaceNorm^\\Ref$',color=self.colorList[-1],linestyle=self.linestyleList[-1],linewidth=2.5) #$\\V_\\SI^\\Ref$
        self.plotL2NormData()          
        plt.legend(bbox_to_anchor=(1.05,1.0), loc="upper left")
        plt.title('$j='+str(j)+'$')
        self.saveTikz(savePath, 'L2SpacePlot')
        plt.close()
    def L2time(self,fL,nL,p,l,savePath,ref,resize):
        counter=0
        for alg in range(len(fL)):
            for j in self.tL:
                (time,V)= read.getDataFromTXT(fL[alg],read.createFilename(l,j,1),p)
                (time,V)=self.resizeData(time,V,resize,len(time))
                labelname = nL[alg]+'~$j='+str(j)+'$'
                plt.plot(time,V, label = labelname,color=self.colorList[j-self.tL[0]],linestyle=self.linestyleList[counter],linewidth=2.5)
            counter+=1
        (t_ref,Vref)=read.getDataFromTXT(ref,read.createFilename(6,5,1),p)
        (t_ref,Vref)=self.resizeData(t_ref,Vref,resize,len(t_ref))
        plt.plot(t_ref,Vref, label = '$\\SpaceNorm^\\Ref$',color=self.colorList[-1],linestyle=self.linestyleList[-1],linewidth=2.5) #$\\V_\\SI^\\Ref$
        self.plotL2NormData()
        plt.legend(bbox_to_anchor=(1.05,1.0), loc="upper left")
        plt.title('$\\ell='+str(l)+'$')
        self.saveTikz(savePath, 'L2TimePlot')
        plt.close()
        
    def plotAllPoints(self,fL):
        for file in fL:
            self.pointsInPlotForFixlAndjAndm(file,self.lL[-1],self.tL[-1],1,10)
    def pointsInPlotForFixlAndjAndm(self,path,l,j,m,resize,name,title=''):
        for p in self.evalP:
            (time,V)= read.getDataFromTXT(path,read.createFilename(l,j,m),p)
            for i in range(len(V)):
                V[i] = float(V[i])
            labelname = self.getPointNamebyNumber(p)
            (time,V)=self.resizeData(time,V,resize,len(time),1000) 
            plt.plot(time,V, label = labelname,color=self.colorList[p-1],linewidth=2.5)
        plotname =name +str(l)+ 'j'+str(j)+'m'+str(m)+'AllPoints'
        self.plotDataDeutsch(True)
        plt.legend(bbox_to_anchor=(1.05,1.0), loc="upper left")
        plt.title(title)
        #plt.show()
        self.saveTikz(path+'/tex/', plotname)
        plt.close()
    def getTexnameByFnfix(self,prefix):
        if prefix=='Ca':
            return '\\Ca'
        elif prefix=='Gamma':
            return '\\gf'
        else:
            return '\\V'
    def getTimeIntervalBYFnfix(self,prefix):
        if prefix=='Ca':
            return '(\\tCa^\\Ref,\\TCa^\\Ref)'
        elif prefix=='':
            return '(\\tact,\\Tact)'
    def definePositionOfLegendByPrefix(self,fnfix):
        if fnfix=='Ca':
            plt.legend(loc='upper right')
        elif fnfix=='Gamma':
            plt.legend(loc='lower right')
        elif fnfix=='':
            plt.legend(bbox_to_anchor=(1.01,1.0), loc="upper left")
        elif fnfix=='Volumes':
            plt.legend(bbox_to_anchor=(1.01,1.0), loc="upper left")
    
    def plotVIntRef(self,fnfix,resize,p,path):
        th=-50.0
        if fnfix=='Ca':
            th=0.0002
        elif fnfix=='Gamma':
            th=-0.0001
        
        
        if fnfix=='Ca' or fnfix=='Gamma':
            V_full=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,p,self.sdt,self.T,path,fnfix)
            time_full=np.linspace(self.sdt*2**(-(self.tL[-2])), self.T, num=len(V_full))
            (time_full,V_full)=self.resizeData(time_full,V_full,10,len(time_full))
            labelname='$'+self.getTexnameByFnfix(fnfix)+'^{\\infty,\\infty,'+str(p)+'}$'
            plt.plot(time_full,V_full,label = labelname,linewidth=2.5,color=self.colorList[self.lL[-1]+1],linestyle='dotted')
            
            
        t_f=[]
        VInt_full=[]
        if fnfix!='GammaInfty':
            tact_ref=comp.getTextFullExtrapolate(self.lL[-1],3,1,p,path,th,fnfix)
            (t_f,VInt_full)=comp.getVIntFullExtrapolate(self.lL[-1],self.tL[-1],1,p,path,self.sdt,th,durEvalActi,tact_ref,fnfix)
            #for i in range(len(t_f)):
               #t_f[i]-=tact_ref
            #print('maxExtra',max(VInt_full))
            (t_f,VInt_full)=self.resizeData(t_f,VInt_full,10,len(t_f))
        
        labelname=''
        if fnfix!='Gamma':
            labelname ='$'+self.getTexnameByFnfix(fnfix)+'^{\\infty,\\infty,'+str(p)+'}_{'+self.getTimeIntervalBYFnfix(fnfix)+'}$'
        else:
            labelname = '$'+'\\gfAct'+'^{\\infty,\\infty}$'
        
        plt.plot(t_f,VInt_full,label = labelname,linewidth=3.5,color=self.colorList[self.lL[-1]+2],linestyle='dashed')
        
        
            
            
    def plotSpaceForPointp(self,path,p,j,m,n,name='BRSpaceConvergence',fnfix=''):
        for l in self.lL:
            time=[]
            V=[]
            
            if fnfix!='GammaInfty':
                (time,V)= read.getDataFromTXT(path,read.createFilename(l,j,m)+fnfix,p)
            else:
                (time,V)= read.getDataFromTXT(path,read.createFilename(l,j,m)+'Gamma',p)
            for i in range(len(V)):
                V[i] = float(V[i])
            #print(j,l,max(V))
            #(tact1,id_tact1)=comp.getActivaitionTimeFromVWithID(time,V,-50.0)
            #id_dur1=self.getTimeID(id_tact1,j)
            (time,V)=self.resizeData(time,V,n,len(time))
            if 'Vortrag' in name:
                for index in range(len(time)):
                    if time[index]>=0.5:
                        time=time[:index]
                        V=V[:index]
                        break
            labelname = '$\\ell='+str(l)+'$'
            #for i in range(len(time)):
                #time[i]-=tact1
            #plt.plot(time[id_tact1:id_dur1],V[id_tact1:id_dur1], label = labelname,linewidth=2.5,color=self.colorList[l-self.lL[0]])
            lw=2.5
            if 'Vortrag' in name:
                lw=3.5
            #for index in range(len(time)):
                #if time[index]>=0.45:
                    #time=time[:index]
                    #V=V[:index]
                    #break
            #for index in range(len(time)):
                #time[index]=1000.0*time[index]
            #lw=3.5
            plt.plot(time,V, label = labelname,linewidth=lw)#,color=self.colorList[l-self.lL[0]])
        if 'GS0004' in path and 'SimpleExcitation' in path: 
            self.plotVIntRef(fnfix,n,p,'../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/SIOC0004/')
        elif 'GS0004' in path and 'Kopplung' in path: 
            self.plotVIntRef(fnfix,n,p,'../../../data/Kopplung/SIOC0004/')
        else:
            #print('test')
            self.plotVIntRef(fnfix,n,p,path)
        
        
        plotname =name+ 'j'+str(j)+'m'+str(m)+'P'+str(p)
        self.plotDataDeutsch(False,fnfix)
        #self.definePositionOfLegendByPrefix(fnfix)
        plt.title('$j='+str(j)+'$')
        #plt.show()
        self.saveTikz(path+'/tex/', plotname)
        plt.close()
    def plotTimeForPointp(self,path,p,l,m,n,name='BRSpaceConvergence',fnfix=''):
        for j in self.tL:
            time=[]
            V=[]
            if fnfix!='GammaInfty':
                (time,V)= read.getDataFromTXT(path,read.createFilename(l,j,m)+fnfix,p)
            else:
                (time,V)= read.getDataFromTXT(path,read.createFilename(l,j,m)+'Gamma',p)
            for i in range(len(V)):
                V[i] = float(V[i])
            #print(j,l,max(V))
            (tact1,id_tact1)=comp.getActivaitionTimeFromVWithID(time,V,-50.0)
            id_dur1=self.getTimeID(id_tact1,j)
            (time,V)=self.resizeData(time,V,n,len(time))
            if 'Vortrag' in name:
                for index in range(len(time)):
                    if time[index]>=0.5:
                        time=time[:index]
                        V=V[:index]
                        break
            
            labelname = '$j='+str(j)+'$'
            #print(len(time),len(V))
            #for i in range(len(time)):
                #time[i]-=tact1
            #plt.plot(time[id_tact1:id_dur1],V[id_tact1:id_dur1], label = labelname,linewidth=2.5,color=self.colorList[j-self.tL[0]])
            lw=2.5
            if 'Vortrag' in name:
                lw=3.5
            plt.plot(time,V, label = labelname,linewidth=lw,color=self.colorList[j-self.tL[0]])
        #if 'GS0004' in path and 'SimpleExcitation' in path: 
        self.plotVIntRef(fnfix,n,p,'../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/SIOC0004/')
        #elif 'GS0004' in path and 'Kopplung' in path: 
            #self.plotVIntRef(fnfix,n,p,'../../../data/Kopplung/SIOC0004/')
        #else:
            #self.plotVIntRef(fnfix,n,p,path)
        
        plotname =name+ 'l'+str(l)+'m'+str(m)+'P'+str(p)
        self.plotDataDeutsch(False,fnfix)
        self.definePositionOfLegendByPrefix(fnfix)
        plt.title('$\\ell='+str(l)+'$')
        #plt.show()
        self.saveTikz(path+'/tex/', plotname)
        plt.close()
    
    def plotSpaceVolumes(self,path,j,n):
        for l in self.lL:
            (time_lv,LV)= read.getDataFromTXT(path,read.createFilename(l,j,1)+'Volumes',1)
            (time_rv,RV)= read.getDataFromTXT(path,read.createFilename(l,j,1)+'Volumes',2)
            for i in range(len(LV)):
                LV[i] = float(LV[i])
                RV[i] = float(RV[i])
            (time_lv,LV)=self.resizeData(time_lv,LV,n,len(time_lv))
            (time_rv,RV)=self.resizeData(time_rv,RV,n,len(time_rv))
            labelname = '$\\LV$  $\\ell='+str(l)+'$'
            plt.plot(time_lv,LV, label = labelname,linewidth=2.5,color=self.colorList[l])
            labelname = '$\\RV$ $\\ell='+str(l)+'$'
            plt.plot(time_rv,RV, label = labelname,linewidth=2.5,color=self.colorList[l],linestyle='dotted')
        
        
        LV_ref=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,1,self.sdt,self.T,path,'Volumes')
        time_lv=np.linspace(self.sdt*2**(-(self.tL[-2])), self.T, num=len(LV_ref))
        (time_lv,LV_ref)=self.resizeData(time_lv,LV_ref,10,len(time_lv))
        labelname='$\\LV^{\\infty,\\infty}$'
        plt.plot(time_lv,LV_ref,label = labelname,linewidth=2.5,color=self.colorList[self.lL[-1]+1],linestyle='dashed')
        
        RV_ref=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,2,self.sdt,self.T,path,'Volumes')
        time_rv=np.linspace(self.sdt*2**(-(self.tL[-2])), self.T, num=len(RV_ref))
        (time_rv,RV_ref)=self.resizeData(time_rv,RV_ref,10,len(time_rv))
        labelname='$\\RV^{\\infty,\\infty}$'
        plt.plot(time_rv,RV_ref,label = labelname,linewidth=2.5,color=self.colorList[self.lL[-1]+1],linestyle='-.')
        
        plotname ='SpaceVolumes'+ 'j'+str(j)
        self.plotDataDeutsch(False,'Volumes')
        self.definePositionOfLegendByPrefix('Volumes')
        plt.title('$j='+str(j)+'$')
        #plt.show()
        self.saveTikz(path+'/tex/', plotname)
        plt.close()
    
    def plotTimeVolumes(self,path,l,n):
        for j in self.tL:
            (time_lv,LV)= read.getDataFromTXT(path,read.createFilename(l,j,1)+'Volumes',1)
            (time_rv,RV)= read.getDataFromTXT(path,read.createFilename(l,j,1)+'Volumes',2)
            for i in range(len(LV)):
                LV[i] = float(LV[i])
                RV[i] = float(RV[i])
            (time_lv,LV)=self.resizeData(time_lv,LV,n,len(time_lv))
            (time_rv,RV)=self.resizeData(time_rv,RV,n,len(time_rv))
            labelname = 'LV $j='+str(j)+'$'
            plt.plot(time_lv,LV, label = labelname,linewidth=2.5,color=self.colorList[j])
            labelname = 'RV $j='+str(j)+'$'
            plt.plot(time_rv,RV, label = labelname,linewidth=2.5,color=self.colorList[j],linestyle='dotted')
        
        
        LV_ref=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,1,self.sdt,self.T,path,'Volumes')
        time_lv=np.linspace(self.sdt*2**(-(self.tL[-2])), self.T, num=len(LV_ref))
        (time_lv,LV_ref)=self.resizeData(time_lv,LV_ref,10,len(time_lv))
        labelname='$\\LV^{\\infty,\\infty}$'
        plt.plot(time_lv,LV_ref,label = labelname,linewidth=2.5,color=self.colorList[self.lL[-1]+1],linestyle='dashed')
        
        RV_ref=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,2,self.sdt,self.T,path,'Volumes')
        time_rv=np.linspace(self.sdt*2**(-(self.tL[-2])), self.T, num=len(RV_ref))
        (time_rv,RV_ref)=self.resizeData(time_rv,RV_ref,10,len(time_rv))
        labelname='$\\RV^{\\infty,\\infty}$'
        plt.plot(time_rv,RV_ref,label = labelname,linewidth=2.5,color=self.colorList[self.lL[-1]+1],linestyle='-.')
        
        plotname ='TimeVolumes'+ 'l'+str(l)
        self.plotDataDeutsch(False,'Volumes')
        self.definePositionOfLegendByPrefix('Volumes')
        plt.title('$\ell='+str(l)+'$')
        #plt.show()
        self.saveTikz(path+'/tex/', plotname)
        plt.close()
    
    
    #####################################################################################
    def plotSpaceForPointpWithRef(self,path,p,j,m,L,J,resize,interval=None):
        levelList=[]
        if L==self.lL[-1]:
            levelList=self.lL[:-1]
        else:
            levelList=self.lL
            
        for l in levelList:
            (time,V)= read.getDataFromTXT(path,read.createFilename(l,j,m),p)
            for i in range(len(V)):
                V[i] = float(V[i])
            if interval==None:
                (time,V)=self.resizeData(time,V,resize*2**(j),len(time),1000)
            labelname = '$\ell='+str(l)+'$'
            self.plotInInterval(time,V,labelname,interval,self.sdt*2**(-j))
            #plt.plot(time,V, label = labelname)
        (time,V)= read.getDataFromTXT(path,read.createFilename(L,J,m),p)
        for i in range(len(V)):
                V[i] = float(V[i])
        (time,V)=self.resizeData(time,V,resize*2**(J),len(time),1000)
        
        #labelname = '$j='+str(J)+'$, $\ell='+str(L)+'$'
        labelname = '\\Ref'
        self.plotInInterval(time,V,labelname,interval,self.sdt,'dotted')
        #plt.plot(time,V, label = labelname,linestyle='dotted')
        if interval==None:
            plotname ='BRSpaceConvergenceWithRef'+ 'l'+str(l)+'m'+str(m)+'P'+str(p)
        else:
            plotname ='BRSpaceConvergenceWithRef1015'+ 'l'+str(l)+'m'+str(m)+'P'+str(p)
        #plotname ='BRSpaceConvergenceWithRef'+ 'j'+str(j)+'m'+str(m)+'P'+str(p)
        self.plotData(True)
        plt.legend()
        self.saveTikz(path+'/tex/', plotname)
        plt.close()
    def plotTimeForPointpWithRef(self,path,p,l,m,L,J,resize,interval=None):
        for j in self.tL:
            (time,V)= read.getDataFromTXT(path,read.createFilename(l,j,m),p)
            for i in range(len(V)):
                V[i] = float(V[i])
            if interval==None:
                (time,V)=self.resizeData(time,V,resize*2**(j),len(time),1000)
            
            labelname = '$j='+str(j)+'$'
            self.plotInInterval(time,V,labelname,interval,self.sdt*2**(-j))
            #plt.plot(time,V, label = labelname)
        (time,V)= read.getDataFromTXT(path,read.createFilename(L,J,m),p)
        for i in range(len(V)):
                V[i] = float(V[i])
        (time,V)=self.resizeData(time,V,resize*2**(J),len(time),1000)
        #labelname = '$j='+str(J)+'$, $\ell='+str(L)+'$'
        labelname = '\\Ref'
        self.plotInInterval(time,V,labelname,interval,self.sdt,'dotted')
        #plt.plot(time,V, label = labelname,linestyle='dotted')
        if interval==None:
            plotname ='BRTimeConvergenceWithRef'+ 'l'+str(l)+'m'+str(m)+'P'+str(p)
        else:
            plotname ='BRTimeConvergenceWithRef1015'+ 'l'+str(l)+'m'+str(m)+'P'+str(p)
        self.plotData(True)
        plt.legend()
        self.saveTikz(path+'/tex/', plotname)
        plt.close()
    def valuePlot(self,p,filepath,nL):
        for j in self.tL:
            values=[]
            levels=[]
            for l in self.lL:
                standardlength=int(self.T/(self.sdt*2**(-j)))+1
                (t,V)=read.getDataFromTXT(filepath,read.createFilename(l,j,self.m),p)
                if len(t)==standardlength:
                    values.append(comp.L2(V,self.sdt,self.T))
                    levels.append(l)
            labelname = '$j='+str(j)+'$'
            plt.plot(levels,values, label = labelname,marker="o")
        
        plotname ='LIValuesPlotP'+str(p)
        
        plt.xlabel('level in space $\ell$')
        plt.ylabel('$\\|\\V^{j,\ell}(\\cdot,\psechs)\\|_\\Ltwonorm$')
        plt.legend(loc='upper right')        
        self.saveTikz(filepath,plotname)
        plt.close()
            
            
    def plotInInterval(self,t,f,lName,interval,dt,style='-',withMarker=False,markerIndex=0):
        if interval==None:
            plt.plot(t,f, label = lName,linestyle=style)
        else:
            time=[]
            F=[]
            left=int(interval[0]/dt)
            right=int(interval[-1]/dt)+1
            inMS=1
            if t[-1]<1.0:
                inMS=1000
            for i in range(left,right):
                time.append(inMS*t[i])
                F.append(f[i])
            if withMarker:
                plt.plot(time,F, label = lName,linestyle=style,linewidth=3.5,marker=self.markerList[markerIndex], markersize=self.markerSizeList[markerIndex])
            else:
                plt.plot(time,F, label = lName,linestyle=style,linewidth=2.5)
    def plotVTwoTimeIntMethodsSpace(self,fP1,fP2,p,j,eP,name='BRTwoSchemesSpace',nL=[],title=''):
        index=0
        resize=1
        for l in self.lL:
            (time,V1)= read.getDataFromTXT(fP1,read.createFilename(l,j,1),p)
            (time_G,V2)= read.getDataFromTXT(fP2,read.createFilename(l,j,1),p)
            
            size=0
            if len(V1)>len(V2):
                size=len(V2)
            else:
                size=len(V1)
            for i in range(size):
                V1[i] = float(V1[i])
                V2[i] = float(V2[i])
                #print(V1[i]-V2[i])
            (time,V1)=self.resizeData(time,V1,resize*2**(j),len(time))
            (time_G,V2)=self.resizeData(time_G,V2,resize*2**(j),len(time_G))
            
            
            label1="\\SVI~ $\\ell="+str(l)+"$"
            label2="\\OC~ $\\ell="+str(l)+"$"
            if nL!=[]:
                label1=nL[0]+"~ $\\ell="+str(l)+"$"
                label2=nL[1]+"~ $\\ell="+str(l)+"$"
                
            plt.plot(time,V1,linestyle='dotted',color=self.colorList[index],label=label1,linewidth=2.5)
            plt.plot(time_G,V2,color=self.colorList[index],label=label2,linewidth=3.5)
            index+=1
        plt.title(title)
        plotname =name+ 'j'+str(j)+'P'+str(p)
        #self.plotData()
        self.plotDataDeutsch()
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        self.saveTikz(eP,plotname)
        plt.close()
    def plotVTwoTimeIntMethodsTimeRef(self,fP1,fP2,p,l,exPath,L,J,eP,name='BRTwoSchemesTime',nL=[]): 
        resize=1
        for j in self.tL:
            (time,V)= read.getDataFromTXT(fP1,read.createFilename(l,j,1),p)
            (time_G,V_G)= read.getDataFromTXT(fP2,read.createFilename(l,j,1),p)
            for i in range(len(V)):
                V[i] = float(V[i])
                V_G[i] = float(V_G[i])
            (time,V)=self.resizeData(time,V,resize*2**(j),len(time))
            (time_G,V_G)=self.resizeData(time_G,V_G,resize*2**(j),len(time_G))
            plt.plot(time,V,linestyle='--',color=self.colorList[j])
            plt.plot(time_G,V_G,linestyle='dotted',color=self.colorList[j])
        if exPath!='':
            (time,V)= read.getDataFromTXT(exPath,read.createFilename(L,J,1),p)
            for i in range(len(V)):
                V[i] = float(V[i])
            (time,V)=self.resizeData(time,V,resize*2**(j),len(time))
            plt.plot(time,V,color='black')
        plotname =name+ 'l'+str(l)+'P'+str(p)
        self.plotData()
        self.saveTikz(eP,plotname)
        plt.close()
    def plotVTwoTimeIntMethodsTime(self,fP1,fP2,p,l,exPath,eP,name='BRTwoSchemesTime',nL=[],title=''):
        resize=1
        for j in self.tL:
            (time,V)= read.getDataFromTXT(fP1,read.createFilename(l,j,1),p)
            (time_G,V_G)= read.getDataFromTXT(fP2,read.createFilename(l,j,1),p)
            size=0
            if len(V)>len(V_G):
                size=len(V_G)
            else:
                size=len(V)
            for i in range(size):
                V[i] = float(V[i])
                V_G[i] = float(V_G[i])
            (time,V)=self.resizeData(time,V,resize*2**(j),len(time))
            (time_G,V_G)=self.resizeData(time_G,V_G,resize*2**(j),len(time_G))
            label1="\\SVI~ $j="+str(j)+"$"
            label2="\\OC~ $j="+str(j)+"$"
            if nL!=[]:
                label1=nL[0]+"~ $j="+str(j)+"$"
                label2=nL[1]+"~ $j="+str(j)+"$"
            plt.plot(time,V,linestyle='dotted',color=self.colorList[j],label=label1,linewidth=2.5)
            plt.plot(time_G,V_G,color=self.colorList[j],label=label2,linewidth=2.5)
            
        if exPath!='':
            V_intyinfty_LinImpl =comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,p,self.sdt,0.03,exPath)
            Cutted_V_intyinfty_LinImpl=[]
            V_intyinfty_G =comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,p,self.sdt,0.03,fP1)
            Cutted_V_intyinfty_G=[]
            for t in range(len(time)):
                Cutted_V_intyinfty_LinImpl.append(V_intyinfty_LinImpl[t])
                Cutted_V_intyinfty_G.append(V_intyinfty_G[t])
            labelname='$\\V^{\\infty,\\infty}$'
            plt.plot(time,Cutted_V_intyinfty_LinImpl,color='blue')
            plt.plot(time,Cutted_V_intyinfty_G,color='purple')
        plt.title(title)
        self.plotDataDeutsch()
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        self.saveTikz(eP,name)
        
        plt.close()
    def Oberschwinger(self,fL,nL,threshold,p,j,sP):
        for i in range(len(nL)):
            minimalV=[]
            for l in self.lL:
                (time,V)= read.getDataFromTXT(fL[i],read.createFilename(l,j,1),p)
                (tact,id_tact)=comp.getActivaitionTimeFromVWithID(time,V,threshold)
                value=max(V[id_tact:])
                minimalV.append(value)
                labelname=nL[i]
            plt.plot(self.lL,minimalV,color=self.colorList[i],label=labelname,marker=self.markerList[i],linewidth=3.5)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plotname='OberschwingerPlotj'+str(j)+'P'+str(p)
        plt.xlabel('Ortslevel $\\ell$')
        plt.ylabel('$\\max(\\V_'+str(p)+'^{'+str(j)+',\ell})$ (mV)')
        plt.xticks(np.arange(1, len(self.lL), step=1.0))
        #plt.show()
        self.saveTikz(sP,plotname)
        plt.close()

    def Unterschwinger(self,fL,nL,threshold,p,j,sP):
        for i in range(len(nL)):
            minimalV=[]
            for l in self.lL:
                (time,V)= read.getDataFromTXT(fL[i],read.createFilename(l,j,1),p)
                (tact,id_tact)=comp.getActivaitionTimeFromVWithID(time,V,threshold)
                value=min(V[:id_tact])
                minimalV.append(value)
            labelname=nL[i]
            plt.plot(self.lL,minimalV,color=self.colorList[i],label=labelname,marker=self.markerList[i],linewidth=3.5)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plotname='UnterschwingerPlotj'+str(j)+'P'+str(p)
        plt.xlabel('Ortslevel $\\ell$')
        plt.ylabel('$\\min(\\V_'+str(p)+'^{'+str(j)+',\ell})$ (mV)')
        plt.xticks(np.arange(1, len(self.lL), step=1.0))
        #plt.show()
        self.saveTikz(sP,plotname)
        plt.close()
        
    def VolumesErrorInSpace(self,fL,nL,p,j,sP,exP):
        V_ref=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,p,self.sdt,self.T,exP,'Volumes')
        normLV_ref=comp.L2(V_ref,self.sdt,self.T,self.tL[-2],False)
        for i in range(len(nL)):
            VError=[]
            for l in self.lL:
                (t,V)=read.getDataFromTXT(fL[i],read.createFilename(l,j,self.m)+'Volumes',p)
                value=comp.L2DiffInterval(V,V_ref,self.sdt,j,self.tL[-2])
                value=value/normLV_ref
                VError.append(value)
            labelname=nL[i]
            plt.plot(self.lL,VError,color=self.colorList[i],label=labelname,marker=self.markerList[i],linewidth=3.5)
            
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        if p==1:
            plotname='LVErrorSpacePlotj'+str(j)
            plt.ylabel('$\errorLV^{'+str(j)+',\ell}$ ')
        else:
            plotname='RVErrorSpacePlotj'+str(j)
            plt.ylabel('$\errorRV^{'+str(j)+',\ell}$ ')
        
        plt.xlabel('Ortsevel $\\ell$')
        plt.xticks(np.arange(1, len(self.lL), step=1.0))
        plt.title('$j='+str(j)+'$')
        #plt.show()
        self.saveTikz(sP,plotname)
        plt.close()
    def VolumesErrorInTime(self,fL,nL,p,l,sP,exP):
        V_ref=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,p,self.sdt,self.T,exP,'Volumes')
        normLV_ref=comp.L2(V_ref,self.sdt,self.T,self.tL[-2],False)
        for i in range(len(nL)):
            VError=[]
            for j in self.tL:
                (t,V)=read.getDataFromTXT(fL[i],read.createFilename(l,j,self.m)+'Volumes',p)
                value=comp.L2DiffInterval(V,V_ref,self.sdt,j,self.tL[-2])
                value=value/normLV_ref
                VError.append(value)
            labelname=nL[i]
            plt.plot(self.tL,VError,color=self.colorList[i],label=labelname,marker=self.markerList[i],linewidth=3.5)
            
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        if p==1:
            plotname='LVErrorTimePlotl'+str(l)
            plt.ylabel('$\errorLV^{j,'+str(l)+'}$ ')
        else:
            plotname='RVErrorTimePlotl'+str(l)
            plt.ylabel('$\errorRV^{j,'+str(l)+'}$ ')
        
        plt.xlabel('Zeitlevel $j$')
        plt.xticks(np.arange(1, len(self.tL), step=1.0))
        plt.title('$\\ell='+str(l)+'$')
        #plt.show()
        self.saveTikz(sP,plotname)
        plt.close()
        
    def VIntErrorInSpace(self,fL,nL,th,p,j,sP,exP):
        tact_ref=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,p,exP,th,'')
        (t_f,VInt_full)=comp.getVIntFullExtrapolate(self.lL[-1],self.tL[-1],1,p,exP,self.sdt,th,durEvalActi,tact_ref,'')
        norm_full=comp.L2(VInt_full,self.sdt,self.T,self.tL[-2],False)
        for i in range(len(nL)):
            VIntError=[]
            for l in self.lL:
                (t,V)= read.getDataFromTXT(fL[i],read.createFilename(l,j,1),p)
                (tact,id_tact)=comp.getTactPerCase(t,V,th,'',True)
                id_dur=self.getTimeID(id_tact,j)
                value=comp.L2DiffInterval(V[id_tact:id_dur],VInt_full,self.sdt,j,self.tL[-2],p)
                value=value/norm_full
                VIntError.append(value)
            labelname=nL[i]
            plt.plot(self.lL,VIntError,color=self.colorList[i],label=labelname,marker=self.markerList[i],linewidth=3.5)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plotname='VIntErrorSpacePlotj'+str(j)+'P'+str(p)
        plt.xlabel('Ortsevel $\\ell$')
        plt.ylabel('$\errorVact^{'+str(j)+',\ell,'+str(p)+'}$ ')
        plt.xticks(np.arange(1, len(self.lL), step=1.0))
        plt.title('$j='+str(j)+'$')
        #plt.show()
        self.saveTikz(sP,plotname)
        plt.close()
    def VIntErrorInTime(self,fL,nL,th,p,l,sP,exP):
        tact_ref=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,p,exP,th,'')
        (t_f,VInt_full)=comp.getVIntFullExtrapolate(self.lL[-1],self.tL[-1],1,p,exP,self.sdt,th,durEvalActi,tact_ref,'')
        norm_full=comp.L2(VInt_full,self.sdt,self.T,self.tL[-2],False)
        for i in range(len(nL)):
            VIntError=[]
            for j in self.tL:
                (t,V)= read.getDataFromTXT(fL[i],read.createFilename(l,j,1),p)
                (tact,id_tact)=comp.getTactPerCase(t,V,th,'',True)
                id_dur=self.getTimeID(id_tact,j)
                value=comp.L2DiffInterval(V[id_tact:id_dur],VInt_full,self.sdt,j,self.tL[-2],p)
                value=value/norm_full
                VIntError.append(value)
            labelname=nL[i]
            plt.plot(self.tL,VIntError,color=self.colorList[i],label=labelname,marker=self.markerList[i],linewidth=3.5)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plotname='VIntErrorTimePlotl'+str(l)+'P'+str(p)
        plt.xlabel('Zeitlevel $j$')
        plt.ylabel('$\errorVact^{j,'+str(l)+','+str(p)+'}$ ')
        plt.xticks(np.arange(1, len(self.lL), step=1.0))
        plt.title('$\ell='+str(l)+'$')
        #plt.show()
        self.saveTikz(sP,plotname)
        plt.close()
    def tactErrorInSpace(self,fL,nL,threshold,p,j,sP,exP,experiment=''):
        tactRef=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,p,exP,threshold)
        for i in range(len(nL)):
            tactError=[]
            for l in self.lL:
                (t,V)= read.getDataFromTXT(fL[i],read.createFilename(l,j,1),p)
                t_act=comp.getActivaitionTimeFromV(t,V,threshold)
                #if p==8 and l==5 and nL[i]=='\\GPDE':
                   # print('plotting',1000.0*t_act,tactRef,abs(t_act-tactRef)/tactRef)
                value=abs(t_act-tactRef)/tactRef
                tactError.append(value)
            labelname=nL[i]
            plt.plot(self.lL,tactError,color=self.colorList[i],label=labelname,marker=self.markerList[i],linewidth=3.5)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plotname=experiment+'TactErrorSpacePlotj'+str(j)+'P'+str(p)
        plt.xlabel('Ortsevel $\\ell$')
        plt.ylabel('$\errortact^{'+str(j)+',\ell,'+str(p)+'}$ ')
        plt.xticks(np.arange(1, len(self.lL), step=1.0))
        plt.title('$j='+str(j)+'$')
        #plt.show()
        self.saveTikz(sP,plotname)
        plt.close()
                
                
    def tactErrorInTime(self,fL,nL,threshold,p,l,sP,exP,experiment=''):
        tactRef=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,p,exP,threshold)
        for i in range(len(nL)):
            tactError=[]
            for j in self.tL:
                (t,V)= read.getDataFromTXT(fL[i],read.createFilename(l,j,1),p)
                t_act=comp.getActivaitionTimeFromV(t,V,threshold)
                value=abs(t_act-tactRef)/tactRef
                tactError.append(value)
            labelname=nL[i]
            plt.plot(self.tL,tactError,color=self.colorList[i],label=labelname,marker=self.markerList[i],linewidth=3.5)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plotname=experiment+'TactErrorTimePlotl'+str(l)+'P'+str(p)
        plt.xlabel('Zeitlevel $j$')
        plt.ylabel('$\errortact^{j,'+str(l)+','+str(p)+'}$')
        plt.xticks(np.arange(-1, len(self.tL), step=1.0))
        plt.title('$\ell='+str(l)+'$')
        #plt.show()
        self.saveTikz(sP,plotname)
        plt.close()
                
                
    def CVPlot(self,fn, pairL,nL,distL,j):
        for index in range(len(pairL)):
            valueList=[]
            for l in self.lL:
                print('l=',l)
                valueList.append(comp.computeCV(fn,l,j,pairL[index][0],pairL[index][1]))
            plt.plot(self.lL,valueList,label=nL[index],marker=self.markerList[0],linewidth=3.5,color=self.colorList[index])
        plt.legend()
        plotname='CVEllipsoidPlot'
        plt.xlabel('space level $\\ell$')
        plt.ylabel('CV (m/s)')
        plt.xticks(np.arange(0, len(self.lL), step=1.0))
        #plt.show()
        self.saveTikz(fn,plotname)
        plt.close()
        
    def ActivationSpaceRMSErrors(self,eP):
        spaceErrorArray=comp.getSpaceRMSerrorArray(self.lL[:-1],self.tL,eP)
        for j in self.tL:
            labelname='$j='+str(j)+'$'
            plt.plot(self.lL[:-1],spaceErrorArray[j],label=labelname,linewidth=3.5,color=self.colorList[j])
        plotname='RMSErrorSpace'
        plt.xlabel('space level $\\ell$')
        plt.ylabel('$\\rmseSpace$')
        plt.xticks(np.arange(0, len(self.lL)-1, step=1.0))
        plt.legend()
        self.saveTikz(eP,plotname)
        plt.close()
        return plotname
    def ActivationTimeRMSErrors(self,eP):
        timeErrorArray=comp.getTimeRMSerrorArray(self.lL,self.tL[:-1],eP)
        for l in self.lL:
            labelname='$\ell='+str(l)+'$'
            plt.plot(self.tL[:-1],timeErrorArray[l],label=labelname,linewidth=3.5,color=self.colorList[l])
        plt.xlabel('time level $j$')
        plt.ylabel('$\\rmseTime$')
        plt.legend()
        plotname='RMSErrorTime'
        self.saveTikz(eP,plotname)
        plt.close()
        return plotname
    def MeanTactSpace(self,eP,l0Path,saveName='',vArr=[],saveInDissFolder=False,language='eng'):
        vArray=[]
        for l in self.lL:
            tactList=[]
            jVector=[]
            for j in self.tL:
                print(l,j)
                tact=-10
                if len(vArr)==0:
                    tact=comp.computeMeanTact(eP+'vtu/AT_l'+str(l)+'j'+str(j)+'.vtu',l0Path)
                else:
                    tact=vArr[l][j]
                if tact > 0.0 :
                    tactList.append(tact)
                    jVector.append(j)
                labelname = '$\\tact(\\V^{j,' + str(l) + '})$'
            plt.plot(jVector, tactList, label=labelname, marker=self.markerList[0], linewidth=3.5,
                     color=self.colorList[l])
            vArray.append(tactList)
        self.DataTact(language)
        # plt.show()
        plotname = 'ActivationSpace'+saveName
        if saveInDissFolder:
            self.saveTikz('../../../thesis-lindner/thesis/figure/', plotname)
        else:
            self.saveTikz(eP+'/tex/', plotname)
        plt.close()
        return vArray
    def ActivationTimePlot(self):
        print('not implemented')


    def ActivationTimeForBenchmarkPointEight3DPlot(self, x, y, z, path):
        path += '/images/'
        fig = plt.figure()
        ax = plt.axes(projection='3d')

        surf = ax.plot_trisurf(y, x, z, cmap='cool', shade=False, linewidth=0, antialiased=True, alpha=0.4)
        fig.colorbar(surf, shrink=0.55, aspect=5)

        ax.set_ylabel('$\\vartriangle t$ (ms)')
        ax.set_xlabel('$\\vartriangle x$ (mm)')
        ax.set_zlabel('activation time (ms)')
        ax.set_ylim(bottom=0.05, top=0.005)
        ax.set_xlim(left=0.5, right=0.1)
        ax.set_zlim(0, 150.0)
        ax.xaxis.set_major_locator(LinearLocator(3))
        ax.yaxis.set_major_locator(LinearLocator(3))
        ax.zaxis.set_major_locator(LinearLocator(4))
        ax.zaxis.set_major_formatter('{x:.02f}')
        ax.set_xticks([0.1, 0.2, 0.5])
        ax.set_yticks([0.05, 0.01, 0.005])
        ax.set_zticks([0, 50, 100, 150])

        # ax.set_title('Activation time at point $P_8$ for every combination of spatial and temporal refinement.')
        ax.view_init(20, 60)
        plotname = 'NiedBMP8'
        plt.savefig(path + plotname)
        plt.show()
        plt.close()

    def ActivationTimePlotSpace(self, path, j,spacedisc =[]):
        for l in self.lL:
            (lengthList, actiList) = comp.getDiagActivViaPython(path, l, j)
            if len(spacedisc)!=0:
                labelname = '${\\vartriangle}x=' + str(spacedisc[l]) + '$'
            else:
                labelname = '$\\ell=' + str(l) + '$'
            plt.plot(lengthList, actiList, label=labelname, linewidth=1.5)
        self.DiagonalActivationTimeLabel()

        # Limits for NiedererBenchmark
        plt.xlim([0, 21.5])
        plt.ylim([0, 150.01])
        plt.xticks(np.arange(0, 21.5, 5.35))
        plt.yticks(np.arange(0, 151, 50))
        plt.grid(alpha=0.5)

        plotname = 'NiedBMInSpace'
        self.saveTikz(path+'/tex/', plotname)
        #plt.show()
        plt.close()

    def ActivationTimePlotTime(self,path,tdisc,level):
        for j in range(len(self.tL)):
            (lengthList,actiList)=comp.getDiagActivViaPython(path,level,j)
            labelname='${\\vartriangle}t='+str(tdisc[j])+'$'
            plt.plot(lengthList, actiList, label=labelname, linewidth=1.5)
        self.DiagonalActivationTimeLabel()
        plotname='NiedBMInTime'
        self.saveTikz(path+'/tex/', plotname)
        #plt.show()
        plt.close()

    def DiagonalActivationTimeLabel(self):
        
        plt.xlabel('length [mm]')
        plt.ylabel('$\\tact$ [ms]')
        plt.legend()
    ########################################################################
    #Aufwand-Genauigkeit mit Extrapolierter
    #########################################################################
    
    def getPlotnameWPD(self,path,fn,p,name):
        plotname =''
        model =read.getModel(path,fn)
        if model=='Coupled':
            plotname='WP'+name+'CoupledP'+str(p)
        elif model=='Monodomain':
            plotname='WP'+name+'MonoP'+str(p)
        return plotname
        
    def workPrecisionInSpaceExtra(self,exP,j,fL,nL,sP,p,ydes,fnfix,timeunit='min'):        
        minproc=1000000000
        th=-50.0
        #if fnfix=='Ca':
            #th=0.0002
        #elif fnfix=='Gamma':
           # th=-0.0001
        
        t_f=[]
        VInt_full=[]
        tact_ref=-10.0
        norm_full=0.0
        if fnfix=='' or fnfix=='Grob':
            tact_ref=comp.getTextFullExtrapolate(self.lL[-1],4,1,p,exP,th,'')
            (t_f,VInt_full)=comp.getVIntFullExtrapolate(self.lL[-1],self.tL[-1],1,p,exP,self.sdt,th,durEvalActi,tact_ref,'')
            norm_full=comp.L2(VInt_full,self.sdt,self.T,self.tL[-2],False)
        elif fnfix=='tact' or fnfix=='tactGrob':
            tact_ref=comp.getTextFullExtrapolate(self.lL[-1],4,1,p,exP,th,'')
            norm_full=tact_ref
        else:
            print(fnfix,' not defined')
        errorA=[]
        durA=[]
        procA=[]
        standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
        for index in range(len(fL)):
            errorL=[]
            durL=[]
            procsL=[]
            for l in self.lL:
                (t,V)=read.getDataFromTXT(fL[index],read.createFilename(l,j,self.m),p)
                if len(t)==standardlength or len(t)==standardlength-1:
                    (tact,id_tact)=comp.getTactPerCase(t,V,th,'',True)
                    id_dur=self.getTimeID(id_tact,j)
                    value=0.0
                    if fnfix=='' or fnfix=='Grob':
                        value=comp.L2DiffInterval(V[id_tact:id_dur],VInt_full,self.sdt,j,self.tL[-2],p)
                        value=value/norm_full
                    elif fnfix=='tact' or fnfix=='tactGrob':
                        value=abs(tact-tact_ref)/tact_ref
                    if value<0:
                        print(value)
                    errorL.append(value)
                    
                    (procs,dur)=read.getProcsAndDur(fL[index],l,j,self.m)
                    procs=int(procs)
                    procsL.append(procs)
                    if procs<minproc:
                        minproc=procs
                    durL.append(self.stringToTime(dur[:-3],timeunit))
            errorA.append(errorL)
            durA.append(durL)
            procA.append(procsL)
            
        for index in range(len(fL)):
            for k in range(len(procA[index])):
                factor=procA[index][k]/minproc
                durA[index][k]=durA[index][k]*factor
            plt.plot(durA[index],errorA[index],label=nL[index],marker=self.markerList[index],linewidth=3.5,color=self.colorList[index])    
        self.DataWP(timeunit,ydes)
        plotname=self.getPlotnameWPD(fL[0],read.createFilename(self.lL[0],self.tL[0],self.m),p,'Spacej'+str(j)+fnfix)
        plt.title('$j='+str(j)+'$')
        self.saveTikz(sP,plotname)
        plt.close()
        return(plotname,minproc)
        
    def workPrecisionInTimeExtra(self,exP,l,fL,nL,sP,p,ydes,fnfix,procscale =0,timeunit='min'):
        minproc=1000000000
        th=-50.0
        #if fnfix=='Ca':
            #th=0.0002
        #elif fnfix=='Gamma':
           # th=-0.0001
        
        t_f=[]
        VInt_full=[]
        norm_full=0.0
        tact_ref=-10.0
        if fnfix=='' or fnfix=='Grob':
            tact_ref=comp.getTextFullExtrapolate(self.lL[-1],4,1,p,exP,th,'')
            (t_f,VInt_full)=comp.getVIntFullExtrapolate(self.lL[-1],self.tL[-1],1,p,exP,self.sdt,th,durEvalActi,tact_ref,'')
            norm_full=comp.L2(VInt_full,self.sdt,self.T,self.tL[-2],False)
        elif fnfix=='tact' or fnfix=='tactGrob':
            tact_ref=comp.getTextFullExtrapolate(self.lL[-1],4,1,p,exP,th,'')
            norm_full=tact_ref
        else:
            print(fnfix,' not defined')
        errorA=[]
        durA=[]
        procA=[]
        
        for index in range(len(fL)):
            errorL=[]
            durL=[]
            procsL=[]
            for j in self.tL:
                (t,V)=read.getDataFromTXT(fL[index],read.createFilename(l,j,self.m)+'',p)
                standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                if len(t)==standardlength or len(t)==standardlength-1:
                    (tact,id_tact)=comp.getTactPerCase(t,V,th,'',True)
                    id_dur=self.getTimeID(id_tact,j)
                    value=0.0
                    if fnfix=='' or fnfix=='Grob':
                        value=comp.L2DiffInterval(V[id_tact:id_dur],VInt_full,self.sdt,j,self.tL[-2],p)
                        value=value/norm_full
                    elif fnfix=='tact' or fnfix=='tactGrob':
                        value=abs(tact-tact_ref)/tact_ref
                    if value<0:
                        print(value)
                    errorL.append(value)
                    
                    (procs,dur)=read.getProcsAndDur(fL[index],l,j,self.m)
                    #if index==2:
                        #print(dur,int(procs),l,j,fL[index])
                    procs=int(procs)
                    procsL.append(procs)
                    if procs<minproc:
                        minproc=procs
                    durL.append(self.stringToTime(dur[:-3],timeunit))
                    
            errorA.append(errorL)
            durA.append(durL)
            procA.append(procsL)
        if procscale!=0:
            minproc=procscale    
        for index in range(len(fL)):
            for k in range(len(procA[index])):
                factor=procA[index][k]/minproc
                durA[index][k]=durA[index][k]*factor
            plt.plot(durA[index],errorA[index],label=nL[index],marker=self.markerList[index],linewidth=3.5,color=self.colorList[index])
                
        self.DataWP(timeunit,ydes)
        plt.title('$\\ell='+str(l)+'$')
        plotname =self.getPlotnameWPD(fL[0],read.createFilename(self.lL[0],self.tL[0],self.m),p,'Timel'+str(l)+fnfix)
        
        self.saveTikz(sP,plotname)
        plt.close()
        return(plotname,minproc)
    
    
    #############################################################################
    #Aufwand-Genauigkeit mit Referenzlösung
    #############################################################################
    def workPrecisionInSpace(self,L,J,j,fL,nL,refPath,eP,p,ydes,timeunit='min'):
        (t,Ref)=read.getDataFromTXT(refPath,read.createFilename(L,J,self.m),p)
        normRef=comp.L2(Ref,self.sdt,self.T,J,False)
        standardlength=int(self.T/(self.sdt*2**(-j)))+1
        minproc=1000000000
        errorA=[]
        durA=[]
        procA=[]
        for index in range(len(fL)):
            errorL=[]
            durL=[]
            procsL=[]
            for l in self.lL:
                (t,V)=read.getDataFromTXT(fL[index],read.createFilename(l,j,self.m),p)
                if(len(t))==standardlength:
                    error=comp.L2DiffPot(V,Ref,self.sdt*2**(-j),self.T)
                    error=error/normRef
                    errorL.append(error)
                
                    (procs,dur)=read.getProcsAndDur(fL[index],l,j,self.m)
                    procs=int(procs)
                    procsL.append(procs)
                    if procs<minproc:
                        minproc=procs
                    durL.append(self.stringToTime(dur[:-3],timeunit))
            errorA.append(errorL)
            durA.append(durL)
            procA.append(procsL)
        for index in range(len(fL)):
            for k in range(len(procA[index])):
                factor=procA[index][k]/minproc
                durA[index][k]=durA[index][k]*factor
            plt.plot(durA[index],errorA[index],label=nL[index],marker=self.markerList[index],linewidth=3.5,color=self.colorList[index])
        self.DataWP(timeunit,ydes)
        plotname='WPSpaceP'+str(p)
        self.saveTikz(eP,plotname)
        plt.close()
        return(plotname,minproc)
        
    def workPrecisionInTime(self,L,J,l,fL,nL,refPath,eP,p,ydes,procscale =0,timeunit='min'):
        (t,Ref)=read.getDataFromTXT(refPath,read.createFilename(L,J,self.m),p)
        normRef=comp.L2(Ref,self.sdt,self.T,J,False)
        minproc=1000000000
        errorA=[]
        durA=[]
        procA=[]
        for index in range(len(fL)):
            errorL=[]
            durL=[]
            procsL=[]
            for j in self.tL:
                (t,V)=read.getDataFromTXT(fL[index],read.createFilename(l,j,self.m),p)
                standardlength=int(self.T/(self.sdt*2**(-j)))+1
                if(len(t))==standardlength:
                    error=comp.L2DiffPot(V,Ref,self.sdt*2**(-j),self.T)
                    error=error/normRef
                    errorL.append(error)
                
                    (procs,dur)=read.getProcsAndDur(fL[index],l,j,self.m)
                    procs=int(procs)
                    procsL.append(procs)
                    if procs<minproc:
                        minproc=procs
                    durL.append(self.stringToTime(dur[:-3],timeunit))
            errorA.append(errorL)
            durA.append(durL)
            procA.append(procsL)
        if procscale!=0:
            minproc=procscale
        for index in range(len(fL)):
            for k in range(len(procA[index])):
                factor=procA[index][k]/minproc
                durA[index][k]=durA[index][k]*factor
            plt.plot(durA[index],errorA[index],label=nL[index],marker=self.markerList[index],linewidth=3.5,color=self.colorList[index])
        self.DataWP(timeunit,ydes)
        plotname='WPTimeP'+str(p)
        self.saveTikz(eP,plotname)
        plt.close()
        return(plotname,minproc)
    
    
    #############################################################################################
    ############################################################################################
    def stringToTime(self,string,unit):
        if unit =='min':
            s=string.split(':')
            if len(s)==1:
                return float(s[0])/60.0
            elif len(s)==2:
                return float(s[0])+float(s[1])/60.0
            else:
                return float(s[0])*60.0 + float(s[1])+float(s[2])/60.0
        elif unit=='sec':
            if len(s)==1:
                return float(s[0])
            elif len(s)==2:
                return float(s[0])*60.0+float(s[1])
            else:
                return float(s[0])*60.0*60.0 + float(s[1])*60.0+float(s[2])
        else:
            print('time unit not implemented')
    def saveTikz(self, p,pname):
        import tikzplotlib
        tikzplotlib.save(p+ pname + ".tex")
        
        
    def plotDataDeutsch(self,inMS=False,fnfix=''):
        xmin, xmax, ymin, ymax = 0.0, self.T+0.05, -105.0,50.0
        if inMS:
            plt.xlabel('Zeit (ms)')
        else:
            plt.xlabel('Zeit (s)')
        if fnfix=='':
            plt.ylabel('Transmembranspannung (mV)')  
        elif fnfix=='Ca':
            plt.ylabel('$\\Ca$\,(m\,mol/l)')
        elif fnfix=='Gamma':
            plt.ylabel('$\\gf$')
        elif fnfix=='GammaInfty':
            plt.ylabel('$\\|\\gf \\|_\\infty$')
        elif fnfix=='Volumes':
            plt.ylabel('Volumen (ml)')
    def plotData(self,inMS=False):
        xmin, xmax, ymin, ymax = 0.0, self.T+0.05, -100.0,70.0
        if inMS:
            plt.xlabel('time (ms)')
        else:
            plt.xlabel('time (s)')
        plt.ylabel('transmembrane voltage (mV)')
    def DataTact(self,language):
        if language=='eng':
            plt.xlabel('time discretization ${\\vartriangle}t_0^{-j}$')
        elif language=='d':
            plt.xlabel(' ${\\vartriangle}t_0^{-j}$')
        else:
            print('language not defined')
        
        plt.ylabel('$\\tact(\\V^{j,\\ell})$')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        
    def DataWP(self,unit,ydes=''):
        plt.xlabel('Rechenzeit in '+unit)
        if ydes=='':
            plt.ylabel('error')
        else:
            plt.ylabel(ydes)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.yscale('log', base=2)
        plt.xscale('log', base=2)
    def plotDataConPlot(self):
        plt.xlabel('${\\vartriangle}t$ in ms')
        plt.ylabel('$\\L2$ distance ')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.yscale('log', basey=2)
        plt.xscale('log', basex=2)
        
    def resizeData(self,t,V,number,length, inMS=1):
        time=[]
        pot=[]
        for i in range(length):
            if i%number==0:
                time.append(inMS*t[i])
                pot.append(V[i])
        return(time,pot)
    
    def getLableName(self,p,maxNumber):
        labelname='not defined yet'
        if p<=maxNumber-2:
            labelname = 'P'+str(p)
        elif p==maxNumber-1:
            labelname = '$\\L2$'
        elif p==maxNumber:
            labelname = 'H1'
        return labelname
            
    def plotSpaceTimeConvergence(self,fL):
        for file in fL:
            for point in self.evalP:
                self.plotSpaceForPointp(file,point,self.tL[0],self.m,1)
                self.plotTimeForPointp(file,point,self.lL[0],self.m,1)
                self.plotSpaceForPointp(file,point,self.tL[-1],self.m,10)
                self.plotTimeForPointp(file,point,self.lL[-1],self.m,1)
                
    def convergencePlotTime(self,path,l,numberofEPwithNorms):
        print("Plot Time convergence")
        for p in self.evalP:
            error=[]
            time =[]
            for j in range(len(self.tL)-1):
                time.append(1000.0*self.sdt/(2**j))
                error.append(float(comp.L2Diff(path,l,j,self.m,l,j+1,self.m,p,self.sdt,self.T)))
            labelname=self.getLableName(p,numberofEPwithNorms)
            plt.plot(time,error,'s-', markersize=5,label=labelname)
        order = time
        order =[2*i for i in order]
        plt.plot(time,order,'k--',linewidth=3.5,label ='order 1')
        order =[15*i*i for i in order]
        #print(order)
        plt.plot(time,order,'r--',linewidth=3.5,label ='order 2')
        self.plotDataConPlot()
        plotname ='ConvergenceTime'  + 'l'+ str(l)+'m'+str(self.m)
        self.saveTikz(path+'/tex/', plotname)
        plt.close()
    def convergencePlotSpace(self, path,j,numberofEPwithNorms):
        print("Plot Space convergence")
        for p in self.evalP:
            error=[]
            space =[]
            for l in range(self.lL[0],len(self.lL)+self.lL[0]-1):
                space.append(start_dx/(2**l))
                error.append(float(comp.L2Diff(path,l,j,self.m,l+1,j,self.m,p,self.sdt,self.T)))
        
                labelname =self.getLableName(p,numberofEPwithNorms)
                plt.plot(space,error,'s-', markersize=5,label=labelname)
        order = space
        plt.plot(space,order,'k--',linewidth=3.5, label='order 1')
        self.plotDataConPlot()
        plotname ='ConvergenceSpace'  + 'j'+ str(j)+'m'+str(self.m)
        self.saveTikz(path+'/tex/', plotname)
        plt.close()  
    def plotSpaceExtrapolates(self,fL,nL,savePath,interval=None):
        for point in self.evalP:
            for index in range(len(fL)):
                V=comp.getExtrapolateInSpace(self.lL[-1],self.tL[-1],self.m,point,self.sdt,self.T,fL[index])
                time = np.linspace(0,self.T,len(V))
                self.plotInInterval(time,V,nL[index],interval,self.sdt)
            self.plotData(True)
            plt.legend(bbox_to_anchor=(0.95,0.05), loc="lower right")
            pN='SpaceExtrapolatesP'+str(point)
            self.saveTikz(savePath,pN)
            plt.close()
            VLI=comp.getExtrapolateInSpace(self.lL[-1],self.tL[-1],self.m,point,self.sdt,self.T,fL[0])
            VSI=comp.getExtrapolateInSpace(self.lL[-1],self.tL[-1],self.m,point,self.sdt,self.T,fL[1])
            Diff_LI_SI=[]
            for i in range(len(VLI)):
                Diff_LI_SI.append(VLI[i]-VSI[i])
            print('max LI', comp.MaxNorm(VLI))
            print('max SI', comp.MaxNorm(VSI))
            print('Diff LI-SI',comp.MaxNorm(Diff_LI_SI,True))
            plt.plot(time,Diff_LI_SI,label='LI-SI')
            plt.legend()
            plt.show()
            
    def plotTimeExtrapolates(self,fL,nL,savePath,interval=None):
        for point in self.evalP:
            for index in range(len(fL)):
                V=comp.getExtrapolateInTime(self.lL[-1],self.tL[-1],self.m,point,self.sdt,self.T,fL[index])
                time = np.linspace(0,self.T,len(V))
                self.plotInInterval(time,V,nL[index],interval,self.sdt,'-',True,index)
                #plt.plot(time,V,label =nL[index])
            self.plotData(True)
            plt.legend(bbox_to_anchor=(0.95,0.05), loc="lower right")
            pN='TimeExtrapolatesP'+str(point)
            #plt.show()
            self.saveTikz(savePath,pN)
            plt.close()
            
            VIE=comp.getExtrapolateInTime(self.lL[-1],self.tL[-1],self.m,point,self.sdt,self.T,fL[0])
            VLI=comp.getExtrapolateInTime(self.lL[-1],self.tL[-1],self.m,point,self.sdt,self.T,fL[1])
            VSI=comp.getExtrapolateInTime(self.lL[-1],self.tL[-1],self.m,point,self.sdt,self.T,fL[2])
            VG=comp.getExtrapolateInTime(self.lL[-1],self.tL[-1],self.m,point,self.sdt,self.T,fL[3])
            time = np.linspace(0,self.T,len(VIE))
            Diff_IE_LI=[]
            Diff_LI_SI=[]
            Diff_IE_SI=[]
            Diff_G_SI=[]
            for i in range(len(VIE)):
                Diff_IE_LI.append(VIE[i]-VLI[i])
                Diff_LI_SI.append(VLI[i]-VSI[i])
                Diff_IE_SI.append(VIE[i]-VSI[i])
                Diff_G_SI.append(VG[i]-VSI[i])
            print(comp.MaxNorm(Diff_IE_LI))
            print(comp.MaxNorm(Diff_LI_SI))
            print(comp.MaxNorm(Diff_IE_SI))
            print(comp.MaxNorm(Diff_G_SI))
            plt.plot(time,Diff_IE_LI,label='IE-LI')
            plt.plot(time,Diff_LI_SI,label='LI-SI')
            plt.legend()
            #plt.show()
    def plotExtrapolates(self,savePath,fPL,nL):
        print('PlotExtrapolates')
        n=comp.getnumberOfNormSteps(T,start_dt)
        time=np.linspace(0,T,n)
        for point in self.evalP:
            for index in range(len(fPL)):
                V=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,point,self.sdt,self.T,fPL[index])
                labelname=nL[index]
                plt.plot(time,V, label = labelname)
        self.plotData()
        plt.legend()
        pN='ExtrapolatesP'+str(point)
        self.saveTikz(savePath,pN)
        plt.close()
        
    def plotDiffExtrapolates(self,fL,nL,p,LList,JList,sP,withDiff=False,pN='',withMaxV=False):
        #print(fL, nL, p,LList,JList)
        n=comp.getnumberOfNormSteps(self.T,self.sdt)
        time=np.linspace(0,self.T,n)
        for index in range(len(fL)):
            V=comp.getFullExtrapolate(LList[index],JList[index],self.m,p,self.sdt,self.T,fL[index])
            labelN ='$\\V^{\\infty,\\infty}$ of '+nL[index]
            plt.plot(time,V,label=labelN,color=self.colorList[index])
        if withDiff:
            Diff=[]
            #max =0.0
            #i_max=0
            V1=comp.getFullExtrapolate(LList[0],JList[0],self.m,p,self.sdt,self.T,fL[0])
            V2=comp.getFullExtrapolate(LList[1],JList[1],self.m,p,self.sdt,self.T,fL[1])
            for i in range(len(time)):
                Diff.append(V1[i]-V2[i])
                #if abs(V1[i]-V2[i])>max:
                # max = abs(V1[i]-V2[i])
                #i_max =i
            labelN='Extrapolates: '+nL[0]+ '~ - '+nL[1]
            plt.plot(time,Diff, label = labelN)
            #print(i_max,max, V1[i_max],V2[i_max])    
        if withMaxV:
            for index in range(len(fL)):
                (t,V)=read.getDataFromTXT(fL[index],read.createFilename(LList[index],JList[index],1),p)
                for i in range(len(V)):
                    V[i] = float(V[i])
                (t,V)=self.resizeData(t,V,2**(JList[index]),len(t))
                labelN=nL[index]+': $\\ell='+str(LList[index])+'$, $j='+str(JList[index])+'$'
                plt.plot(t,V,linestyle='dotted',label=labelN,color=self.colorList[index])
            if withDiff:
                (t1,V1)=read.getDataFromTXT(fL[0],read.createFilename(LList[0],JList[0],1),p)
                (t2,V2)=read.getDataFromTXT(fL[1],read.createFilename(LList[1],JList[1],1),p)
                for i in range(len(V1)):
                    V1[i] = float(V1[i])
                    V2[i] = float(V2[i])
                (t1,V1)=self.resizeData(t1,V1,2**(JList[0]),len(t1))
                (t2,V2)=self.resizeData(t2,V2,2**(JList[1]),len(t2))
                Diff=[]
                for i in range(len(t1)):
                    Diff.append(V1[i]-V2[i])
                labelN=nL[0]+ '~ - '+nL[1]
                plt.plot(t1,Diff, label = labelN,color='green')
                
        if pN=='':
            pN ='FullExtrapolatesLIj'+str(J1)+'LIj'+str(J2)+'P'+str(p)
        self.plotData()
        plt.legend(bbox_to_anchor=(1.1,1), loc="upper left")
        #plt.legend()
        self.saveTikz(sP+'/tex/', pN)
        plt.close()
    
    def plotTwoPotentials(self,dt,T,V,W):
        n=comp.getnumberOfNormSteps(T,dt)
        time=np.linspace(0,T,n)
        plt.plot(time,V,label='LI')
        plt.plot(time,W,label='G')
        plt.legend()
        plt.show()
    def plotPotential(self,dt,T,V):
        n=comp.getnumberOfNormSteps(T,dt)
        time=np.linspace(0,T,n)
        plt.plot(time,V)
        plt.show()

    def plotAdaptive(self,l,path,name,sP):
        resize=2
        lvRvLabel=['LV','RV']#["$\\LV$", "\\RV$"]
        fig, ax1 = plt.subplots()
        ax1.set_xlabel('Zeit (s)')
        #ax1.set_ylabel('Volumen (ml)~~~~')
        ax1.set_ylabel('$\\norm{\\gf}_\\infty$')
        ax2 = ax1.twinx()
        ax2.set_ylabel('${\\vartriangle}t^{\\mathrm{m}}$ (ms)') #10^6\\cdot
        for j in self.tL:
            vol, dataTime, dataStep = read.VolumeDataFromLog(path, read.createFilename(l,j,1))
            (time,V)= read.getDataFromTXT(path,read.createFilename(l,j,1)+'Gamma',10)
            (time,V)=self.resizeData(time,V,resize*2**(j),len(time))
            ax1.plot(time,V,label='$\\norm{\\gf}_\\infty$ für'+' $j='+str(j)+'$ ',linewidth=2.5,color=self.colorList[j],linestyle=self.linestyleList[2])
            times=[dataTime,dataTime]
            #for i in range(len(vol)):
                #(times[i],vol[i])=self.resizeData(times[i],vol[i],resize*2**(j),len(times[i]))
                #ax1.plot(times[i], vol[i], label =lvRvLabel[i]+' $j='+str(j)+'$ ',linewidth=2.5,color=self.colorList[j],linestyle=self.linestyleList[i+1])
    
            stepSize=[]
            for i in range(len(dataTime)-1):
                stepSize.append(1000.0*(dataTime[i+1]-dataTime[i])) #100000*
            #(dataTime,stepSize)=self.resizeData(dataTime,stepSize,resize*2**(j),len(dataTime))
            ax2.plot(dataTime[:-2],stepSize[:-1],label='${\\vartriangle}t^{\\mathrm{m}}$ für $j='+str(j)+'$',linewidth=3.5,color=self.colorList[j])#, marker=self.markerList[j],linestyle='dotted',markersize=self.markerSizeList[j])
            
        ax1.legend(bbox_to_anchor=(-0.2,1.00), loc="upper right")
        ax2.legend(title=' ${\\vartriangle}t^{\\mathrm{m}}$',bbox_to_anchor=(1.2,1.00), loc="upper left")
        ax1.set_ylim([-0.4,0.3])
        ax2.set_ylim([0.0,3.0])
        #ax2.tick_params(axis ='y', labelcolor = color) 
        plt.title(name)
        #plt.show()
        #plotname='TimeStepsizeWithVolumes'
        plotname='TimeStepsizeWithMaxnormGamma'
        self.saveTikz(sP, plotname)
        plt.close()
        
def resizeData(t,V,number,length):
    time=[]
    pot=[]
    for i in range(length):
        if i%number==0:
            time.append(t[i])
            pot.append(V[i])
    return(time,pot)
