from utility import reading as read
from utility import computing as comp
import matplotlib.pyplot as plt
import math
from decimal import Decimal
import numpy as np

durEvalActi=0.4
class Table:
    def __init__(self, lList, tList,start_dt,endTime,substep,Path,exPath=''):
        self.lL = lList
        self.tL = tList
        self.m = substep
        self.sdt = start_dt
        self.T =endTime
        self.tableAlignment='c'
        self.path =Path
        self.exP =exPath
    
    def timename(self,j):
        return '{:.10f}'.format(self.sdt/(2**j)).rstrip("0") [2:]
    
    def getTimeID(self,id_tact,j):
        ID=-1
        n=int(durEvalActi/(self.sdt*2**(-j)))
        ID=id_tact+n
        return ID
        
    def getPointNamebyNumber(self,p,inRef=False):
        if inRef:
            if p==1:
                return '\\peins'
            elif p==2:
                return '\\pzwei'
            elif p==3:
                return '\\pdrei'
            elif p==4:
                return '\\pvier'
            elif p==5:
                return '\\pfuenf'
            elif p==6:
                return '\\psechs'
            elif p==7:
                return '\\psieben'
            elif p==8:
                return '\\pacht'
            elif p==9:
                return '\\pneun'
        else:    
            if p==1:
                return '\\Peins'
            elif p==2:
                return '\\Pzwei'
            elif p==3:
                return '\\Pdrei'
            elif p==4:
                return '\\Pvier'
            elif p==5:
                return '\\Pfuenf'
            elif p==6:
                return '\\Psechs'
            elif p==7:
                return '\\Psieben'
            elif p==8:
                return '\\Pacht'
            elif p==9:
                return '\\Pneun'

    def setFilePath(self,file):
        self.path=file
    def setExPath(self,path):
        self.exP=path
    def tableRowsOrder(self,number):
        block =''
        block+=self.tableAlignment + '|'
        for i in range(number-1):
            block += self.tableAlignment + self.tableAlignment +'|'
        block +=self.tableAlignment + self.tableAlignment
        return block
    
    def writeBeginValueLongtable(self,p, withExtrapolates =True):
        block = ''
        block+= '\\begin{longtable}[H]{l|'
        for l in range(self.lL[0],len(self.lL)+self.lL[0]):
            block+='r'
        if withExtrapolates:
            block+='|r} \n'
        else:
           block+='} \n' 
        block+= '\\caption{$\\| \\V^{j,\\ell}  \\|_\\LtwonormT$ for different time and space levels'
        block+=self.addEvaluationToCaption(p)
        block+= '\\\\ \n' 
        block += ' '
        for l in range(self.lL[0],len(self.lL)+self.lL[0]):
            block +=  '&$ \\ell=' + str(l)+'$' 
        if withExtrapolates:
            block+= '& $\\| \\V^{j,\\infty}  \\|_\\LtwonormT$ \\\\\n'
        else:
            block+= '\\\\\n'
                
        block+='\\hline\n'
        return block
    def writeSpaceDiff(self,p,caption,label):
        block = ''
        for j in self.tL:
            block+='& $j='+str(j)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        
        for l in self.lL[:len(self.lL)-1]:
            block += '$\\ell='+str(l)+'$'
            for j in self.tL:
                standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                (t1,V1)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                (t2,V2)=read.getDataFromTXT(self.path,read.createFilename(l+1,j,self.m),p)
                if len(t1)==standardlength and len(t2)==standardlength:
                    value=comp.L2DiffPot(V1,V2,self.sdt*2**(-j),self.T)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\\n'
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeTimeDiff(self,p,caption,label):
        block = ''
        for l in self.lL:
            block+='& $\\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        
        for j in self.tL[:len(self.tL)-1]:
            block += '$j='+str(j)+'$'
            for l in self.lL:
                standardlength1=int(self.T/(self.sdt*2**(-j)))+1
                standardlength2=int(self.T/(self.sdt*2**(-(j+1))))+1
                (t1,V1)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                (t2,V2)=read.getDataFromTXT(self.path,read.createFilename(l,j+1,self.m),p)
                if len(t1)==standardlength1 and len(t2)==standardlength2:
                    value=comp.L2DiffPot(V1,V2,self.sdt*2**(-j),self.T)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\\n'
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeSpaceDiffPointRows(self,j,evalP,name=''):
        standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
        block = '$j='+str(j)+'$'
        for l in self.lL:
            block+='& $\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for p in evalP:
            block +=self.getPointNamebyNumber(p)
            if name!='':
                block +='$\\V_'+name+'^{j,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')-\\V_'+name+'^{j,\\ell-1}(\\cdot,'+self.getPointNamebyNumber(p,True)+')$'
            else:
                block +='$\\V^{j,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')-\\V^{j,\\ell-1}(\\cdot,'+self.getPointNamebyNumber(p,True)+')$'
            for l in range(self.lL[0],self.lL[-1]+1):
                
                (t1,V1)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                (t2,V2)=read.getDataFromTXT(self.path,read.createFilename(l-1,j,self.m),p)
                if len(t1)==standardlength :
                    value=comp.L2DiffPot(V1,V2,self.sdt*2**(-(j-1)),self.T)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                elif len(t1)>standardlength:
                    value=comp.L2DiffPot(V1[:standardlength],V2[:standardlength],self.sdt*2**(-(j-1)),self.T)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\\n'
        return block
    def writeTimeDiffPointRows(self,l,evalP,name=''):
        block = '$\ell='+str(l)+'$'
        for j in self.tL:
            block+='& $j='+str(j)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for p in evalP:
            if name!='':
                block +='$\\V_'+name+'^{j,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')-\\V_'+name+'^{j-1,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')$'
            else:
                block +='$\\V^{j,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')-\\V^{j-1,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')$'
            for j in range(self.tL[0],self.tL[-1]+1):
                standardlength1=int(self.T/(self.sdt*2**(-j)))+1
                standardlength2=int(self.T/(self.sdt*2**(-(j-1))))+1
                (t1,V1)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                (t2,V2)=read.getDataFromTXT(self.path,read.createFilename(l,j-1,self.m),p)
                if len(t1)==standardlength1 and len(t2)==standardlength2:
                    value=comp.L2DiffPot(V1,V2,self.sdt*2**(-(j-1)),self.T)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\\n'
        return block
    def writeSpaceAndTimeDiff(self,p,shift=1):
        block = ''
        block +='\\multicolumn{'+str(len(self.tL)+1)+'}{c}{$\\|\\V^{j,\\ell}(\\cdot, \\textbf{P'+str(p)+'})-\\V^{j,\\ell+1}(\\cdot,'+self.getPointNamebyNumber(p)+')\\|_\\LtwonormT$} &  &\multicolumn{'+str(len(self.lL)+1-shift)+'}{c}{$\\|\\V^{j,\\ell}(\\cdot, '+self.getPointNamebyNumber(p)+')-\\V^{j+1,\\ell}(\\cdot, \\textbf{P'+str(p)+'})\\|_\\LtwonormT$}\\\\\n'
        for j in self.tL:
            block+='&$j='+str(j)+'$'
        block += '& & '
        for l in self.lL[:len(self.lL)-shift]:
            block+='& $\\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for index in range(len(self.lL)-1):
            l=self.lL[index]
            block += '$\\ell='+str(l)+'$'
            for j in self.tL:
                standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                (t1,V1)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                (t2,V2)=read.getDataFromTXT(self.path,read.createFilename(l+1,j,self.m),p)
                if len(t1)==standardlength and len(t2)==standardlength:
                    value=comp.L2DiffPot(V1,V2,self.sdt*2**(-j),self.T)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            j=self.tL[index]
            block+=' & &$j='+str(j)+'$'
            for level in self.lL[:len(self.lL)-shift]:
                standardlength1=int(self.T/(self.sdt*2**(-j)))+1
                standardlength2=int(self.T/(self.sdt*2**(-(j+1))))+1
                (t1,V1)=read.getDataFromTXT(self.path,read.createFilename(level,j,self.m),p)
                (t2,V2)=read.getDataFromTXT(self.path,read.createFilename(level,j+1,self.m),p)
                if len(t1)==standardlength1 and len(t2)==standardlength2:
                    value=comp.L2DiffPot(V1,V2,self.sdt*2**(-j),self.T)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            
            block+='\\\\\n'
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeDiagDiff(self,caption,label,pL):
        block = ''
        for p in pL:
            block+='&$p=\\textbf{P'+str(p)+'}$'
        block+= '\\\\\n'+'\\midrule\n'
        
        for k in self.tL[:len(self.tL)-1]:
            block+='$k='+str(k)+'$'
            for p in pL:
                (t1,V1)=read.getDataFromTXT(self.path,read.createFilename(k,k,self.m),p)
                (t2,V2)=read.getDataFromTXT(self.path,read.createFilename(k+1,k+1,self.m),p)
                value=comp.L2DiffPot(V1,V2,self.sdt*2**(-k),self.T)
                block += " & $"+ str("%8.4f"%(value))+"$"
            block+='\\\\\n'
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeCompareSILI(self,p,fL, withMaxNorm=False):
        if len(fL)!=2:
            return 'not possible'
        block = ''
        for l in self.lL:
            block+='& $\\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for j in self.tL:
            block += '$j='+str(j)+'$'
            for l in self.lL:
                standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                (t1,V1)=read.getDataFromTXT(fL[0],read.createFilename(l,j,self.m),p)
                (t2,V2)=read.getDataFromTXT(fL[1],read.createFilename(l,j,self.m),p)
                if len(t1)==standardlength and len(t2)==standardlength:
                    if withMaxNorm:
                        Diff=[]
                        for i in range(len(V1)):
                            Diff.append(float(V1[i])-float(V2[i]))
                        value=comp.MaxNorm(Diff,True)
                    else:
                        value=comp.L2DiffPot(V1,V2,self.sdt*2**(-j),self.T)
                    
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\\n'
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeCompareRef(self,p,J,L):
        block = ''
        (t,Ref)=read.getDataFromTXT(self.path,read.createFilename(L,J,self.m),p)
        normRef=comp.L2(Ref,self.sdt,self.T,J,False)
        for l in self.lL:
            block+='& $\\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for j in self.tL:
            block += '$j='+str(j)+'$'
            for l in self.lL:
                standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                (t,V)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                if len(t)==standardlength:
                    value=comp.L2Diff(self.path,l, j, self.m,L,J, self.m,p,self.sdt,self.T)
                    #value =comp.MaxNorm(V)
                    value=value/normRef
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\\n'
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeScalability(self,fL,l,j):
        block=''
        block+= 'procs & CPU time\\\\\n'
        block+='\\midrule\n'
        for index in range(len(fL)):
            (procs,dur)=read.getProcsAndDur(fL[index],l,j,self.m)
            dur=dur[:-3]
            block+=str(procs)+'&'+str(dur)+'\\\\\n'
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    
    def writeCompareSpaceRefList(self,p,J,L,fL,nL,name='\\Ref',divideNorm=False):
        standardlength=int(self.T/(self.sdt*2**(-self.tL[0])))+1
        block = ''
        (t,Ref)=read.getDataFromTXT(self.path,read.createFilename(L,J,self.m),p)
        factor=(len(t)-1)/(standardlength-1)
        if len(t)>standardlength:
                if factor%1==0.0:
                    shortRef=[]
                    factor =int(factor)
                    for i in range(standardlength):
                        shortRef.append(Ref[factor*i])
                    Ref=shortRef
                else:
                    Ref=Ref[:standardlength]
        normRef=comp.L2(Ref,self.sdt,self.T,J,False)
        block+= '& \\multicolumn{'+str(len(fL)*len(self.tL))+'}{c}{$\\|\\V^{j,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')-\\V_'+name+'^{'+str(J)+','+str(L)+'}(\\cdot,'+self.getPointNamebyNumber(p,True)+')\\|_\\LtwonormT$}\\\\[5pt]\n'
        for name in nL[:-1]:
            block+='&'
            block +='\\multicolumn{'+str(len(self.tL))+'}{c|}{'+name+'}'
        block +='&\\multicolumn{'+str(len(self.tL))+'}{c}{'+nL[-1]+'}'
        block+='\\\\\n'
        #block+=self.getPointNamebyNumber(p)
        for index in range(len(fL)):
            for j in self.tL:
                block += '& $j='+str(j)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for l in self.lL:
            block+=' $\\ell='+str(l)+'$'
            for alg in fL:
                for j in self.tL:
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    print(len(V),len(Ref))
                    if len(t)==standardlength:
                        value=comp.L2DiffPot(V,Ref,self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normRef
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    elif len(t)>standardlength:
                        value=comp.L2DiffPot(V[:standardlength],Ref,self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normRef
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
            block+='\\\\\n'
        #block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeDiffRow(self,p,fL,nL,discpair):
        block=''
        block+='$\V1-\V2$'
        for index in range(len(fL)):
            block+='& $\\V_'+nL[index]+'^{'+str(discpair[index][0])+','+str(discpair[index][1])+'}(\\cdot,'+self.getPointNamebyNumber(p,True)+')$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for index in range(len(fL)):
            block+=' $\\V_'+nL[index]+'^{'+str(discpair[index][0])+','+str(discpair[index][1])+'}(\\cdot,'+self.getPointNamebyNumber(p,True)+')$'
            (t,V1)=read.getDataFromTXT(fL[index],read.createFilename(discpair[index][1],discpair[index][0],self.m),p)
            sl1=int(self.T/(self.sdt*2**(-discpair[index][0])))+1
            if len(V1)>sl1:
                V1=V1[:sl1]
            for k in range(len(fL)):
                (t,V2)=read.getDataFromTXT(fL[k],read.createFilename(discpair[k][1],discpair[k][0],self.m),p)
                sl2=int(self.T/(self.sdt*2**(-discpair[k][0])))+1
                if len(t)>=sl2:
                    if discpair[k][0]<discpair[index][0]:
                        value=comp.L2DiffPot(V1,V2[:sl2],self.sdt*2**(-discpair[k][0]),self.T)
                    else:
                        value=comp.L2DiffPot(V1,V2[:sl2],self.sdt*2**(-discpair[index][0]),self.T)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+='& -'
            block+='\\\\\n'
                
        block+='\\\\\n'
        block+=self.EndTabular()
        return block
    def writeDurAProcs(self,fL,nL):
        block=''
        block+='\\multicolumn{2}{c|}{ }'
        for name in nL:
            block +='&\\multicolumn{2}{c|}{'+name+'}'
        block+='\\\\\n'
        block+='\\multicolumn{2}{c|}{Diskretisierung}'
        for name in nL:
            block+='& procs & CPU time'
        block+='\\\\\n'
        
        block+='\\midrule\n'
        for l in self.lL:
            block +='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for path in fL:
                    (procs,dur)=read.getProcsAndDur(path,l,j,self.m)
                    if dur==0:
                        block+='& - & - '
                    else:
                        dur=dur[:-3]
                        block+='&'+str(procs)+'&'+str(dur)
                block+='\\\\\n'
            block+='\\midrule\n'
       
        block+=self.EndTabular()
        return block
    def writeExtraVTab(self,path,fnList,nL,threshold,evalP):
        block=''
        block+=self.BeginDifferenceTable(3,'c')
        block+='& $\\norm{\\V^{J,\infty}_k}_\\LtwonormAct$ & $\\norm{\\V^{\infty,L}_k}_\\LtwonormAct$ &$\\norm{\\V^{\infty,\infty}_k}_\\LtwonormAct$\\\\\n'
        block+='\\midrule\n'
        for p in evalP:
            block+=self.getPointNamebyNumber(p,False)
            tact_ref=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,p,fnList[0],-50.0)
            (t_s,VInt_space)=comp.getVIntExtrapolateInSpace(self.lL[-1],self.tL[-1],1,p,fnList[0],self.sdt,threshold,durEvalActi,tact_ref)
            (t_t,VInt_time)=comp.getVIntExtrapolateInTime(self.lL[-1],self.tL[-1],1,p,fnList[0],self.sdt,threshold,durEvalActi,tact_ref)
            (t_f,VInt_full)=comp.getVIntFullExtrapolate(self.lL[-1],self.tL[-1],1,p,fnList[0],self.sdt,threshold,durEvalActi,tact_ref)
        
            v_space=comp.L2(VInt_space,self.sdt,self.T,self.tL[-1],False)
            v_time=comp.L2(VInt_time,self.sdt,self.T,self.tL[-2],False)
            v_full=comp.L2(VInt_full,self.sdt,self.T,self.tL[-2],False)
            
            block+=' & $'+str("%8.4f"%(v_space))+'$ & $'+str("%8.4f"%(v_time))+'$ & $'+str("%8.4f"%(v_full))+'$\\\\\n'

        
        block+='\\bottomrule\n'
        return block
    def writeExtraTactTab(self,path,fnList,nL,threshold,evalP):
        block=''
        block+=self.BeginDifferenceTable(3,'c')
        block+='& $\\tact^{J,\infty,k}$ & $\\tact^{\infty,L,k}$ &$\\tact^{\infty,\infty,k}$\\\\\n'
        block+='\\midrule\n'
        for p in evalP:
            block+=self.getPointNamebyNumber(p,False)
            tact_space=comp.getTactExtrapolateInSpace(self.lL[-1],self.tL[-1],1,p,fnList[0],threshold)
            tact_time=comp.getTactExtrapolateInTime(self.lL[-1],self.tL[-1],1,p,fnList[0],threshold)
            tact_full=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,p,fnList[0],threshold)

            block+=' & $'+str("%8.4f"%(tact_space))+'$ & $'+str("%8.4f"%(tact_time))+'$ & $'+str("%8.4f"%(tact_full))+'$\\\\\n'
        
        block+='\\bottomrule\n'
        return block
    def writeCPUrow(self,path,fnList,anzahl,l,j,procs):
        
        path+='l'+str(l)+'/'
        block=''
        firstStepTime=list(range(len(fnList)))
        averagedTimeStepduration=list(range(len(fnList)))
        addedTimeStepDuration=list(range(len(fnList)))
        dur=list(range(len(fnList)))
        
        gatingTime=list(range(len(fnList)))
        concentrationTime=list(range(len(fnList)))
        PDETime=list(range(len(fnList)))
        
        for i in range(len(fnList)):
            firstStepTime[i]=0.0
            averagedTimeStepduration[i]=0.0
            addedTimeStepDuration[i]=0.0
            dur[i]=0.0
            gatingTime[i]=0.0
            concentrationTime[i]=0.0
            PDETime[i]=0.0
            for k in range(1,anzahl+1):
                (firstStep,average)=read.getAverageTimeStep(path+fnList[i]+str(k))
                firstStepTime[i]+=firstStep
                averagedTimeStepduration[i]+=average
                addedTimeStepDuration[i]+=read.getAdditonofTimeSteps(path+fnList[i]+str(k))
                dur[i]+=read.getDuration(path+fnList[i]+str(k))
                (gating,conc,pde)=read.getSpecialisedTimeDurationsAveraged(path+fnList[i]+str(k))
                gatingTime[i]+=gating
                concentrationTime[i]+=conc
                PDETime[i]+=pde
            firstStepTime[i]=firstStepTime[i]/anzahl
            averagedTimeStepduration[i]=averagedTimeStepduration[i]/anzahl
            addedTimeStepDuration[i]=addedTimeStepDuration[i]/anzahl
            dur[i]=dur[i]/anzahl
            if dur[i]>60:
                minutes=int(dur[i]//60)
                seconds=str(int(dur[i]-(60*minutes)))
                dur[i]=str(minutes)+':'+seconds.zfill(2)
            #print(dur[i],minutes,seconds)
            else:
                dur[i]=str(0)+':'+str("%8.2f"%(dur[i]))
            gatingTime[i]=gatingTime[i]/anzahl
            concentrationTime[i]=concentrationTime[i]/anzahl
            if concentrationTime[i]<0.001:
                concentrationTime[i]=0.5*gatingTime[i]
                gatingTime[i]=0.5*gatingTime[i]
                
            PDETime[i]=PDETime[i]/anzahl
            #print(dur[i])
        
        alltimes=[firstStepTime,averagedTimeStepduration,dur,gatingTime,concentrationTime,PDETime]
        names=['Erster Zeitschritt',' Gemittel über alle Zeitschritte', 'Gesamtdauer der Rechnung', 'SolveGating','SolveConcentration','SolvePDE']
        block+='$\\ell='+str(l)+'$'
        for k in range(len(alltimes)):
            block+=' & '+names[k]
            for v in alltimes[k]:
                if k==2:
                    block +='&'+v
                else:
                    block +='&'+str("%8.3f"%(v))
            
            block+='\\\\\n'
            if k==2:
                block+='\\cmidrule(r){2-2}\n'
                block+='\\cmidrule(l){3-'+str(len(fnList)+2)+'}\n'
        return block
    def writeCPUTab(self,path,fnList,nL,anzahl):
        block=''
        block=self.BeginDifferenceTable(len(fnList)+1,'l')
        block+='$j=0$, $\\#procs=64$ & '
        for i in range(len(fnList)):
            block+='&'+nL[i]
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(r){2-2}\n'
        block+='\\cmidrule(l){3-'+str(len(fnList)+2)+'}\n'
        for l in self.lL:
            block+=self.writeCPUrow(path,fnList,anzahl,l,0,64)
            if l<self.lL[-1]:
                block+='\\midrule\n'
                #block+='\\cmidrule(r){1-1}\n'
                #block+='\\cmidrule(r){2-2}\n'
                #block+='\\cmidrule(l){3-'+str(len(fnList)+2)+'}\n'
        block+='\\bottomrule\n'
        return block
    
    
    ####################################################################################
    #Volumes
    ####################################################################################
    def writeDifferenceTableSpaceVolume(self,j,fL,nL,p,case):
        standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
        block=''
        block+=self.BeginDifferenceTable(len(nL))
        block+='$j='+str(j)+'$'
        block+=self.getTitleForDifferenceTableSpacePerPoint(case,fL,p)
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(l){'+str(2)+'-'+str(len(fL)+1)+'}\n'
        
        for name in nL:
            block+=' & '+name
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(l){'+str(2)+'-'+str(len(fL)+1)+'}\n'
        
        
        for l in self.lL[1:]:
            block+=' $\\ell='+str(l)+'$'
            for alg in range(len(fL)): 
                (t1,V1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m)+'Volumes',p)
                (t2,V2)=read.getDataFromTXT(fL[alg],read.createFilename(l-1,j,self.m)+'Volumes',p)
                if len(t1)==standardlength and len(t2)==standardlength:
                    value=comp.L2DiffInterval(V1,V2,self.sdt,j,j)
                    block+= " & $"+ str("%8.3f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\\n'
                
        
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    
    def writeDifferenceTableTimeVolume(self,l,fL,nL,p,case):
        block=''
        block+=self.BeginDifferenceTable(len(nL))
        block+='$\ell='+str(l)+'$'
        block+=self.getTitleForDifferenceTableTimePerPoint(case,fL,p)
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(l){'+str(2)+'-'+str(len(fL)+1)+'}\n'
        
        for name in nL:
            block+=' & '+name
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(l){'+str(2)+'-'+str(len(fL)+1)+'}\n'
        
        for j in self.tL[1:]:
            block+=' $j='+str(j)+'$'
            for alg in range(len(fL)):
                (t1,V1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m)+'Volumes',p)
                (t2,V2)=read.getDataFromTXT(fL[alg],read.createFilename(l,j-1,self.m)+'Volumes',p)
                standardlength1=int(round(self.T/(self.sdt*2**(-j))))+1
                standardlength2=int(round(self.T/(self.sdt*2**(-(j-1)))))+1
                if len(t1)==standardlength1 and len(t2)==standardlength2:
                    value=comp.L2DiffInterval(V1,V2,self.sdt,j,j-1)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\\n'
        
        
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    
    
    
    #####################################################################################
    def getTitleForDifferenceTableSpacePerPoint(self,case,fL,p):
        block=''
        if case=='tact':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\tact^{j,\\ell,'+str(p)+'}-\\tact^{j,\\ell-1,'+str(p)+'}$}'
        elif case=='VInt':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\V^{j,\\ell,'+str(p)+'}'+'}_\\LtwonormAct-\\norm{\\V^{j,\\ell-1,'+str(p)+'}'+'}_\\LtwonormAct$}'
        elif case =='VDiffInt':
             block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\V^{j,\\ell,'+str(p)+'}'+'-'+'\\V^{j,\\ell-1,'+str(p)+'}'+'}_\\LtwonormAct$}'
        elif case =='Ca':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\Ca^{j,\\ell,'+str(p)+'}'+'-'+'\\Ca^{j,\\ell-1,'+str(p)+'}'+'}_\\LtwonormT$}'
        elif case =='Gamma':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\gf^{j,\\ell}'+'-'+'\\gf^{j,\\ell-1}'+'}_\\LtwonormT$}'
        elif case=='LV':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\LV^{j,\\ell}'+'-'+'\\LV^{j,\\ell-1}'+'}_\\LtwonormT$}'
        elif case=='RV':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\RV^{j,\\ell}'+'-'+'\\RV^{j,\\ell-1}'+'}_\\LtwonormT$}'
        else:
            print('case',case,'not defined for title of getTitleForDifferenceTableSpacePerPoint')
        return block
    def writeDifferenceTableSpacePerPoint(self,j,fL,nL,p,case,threshold=-50.0):
        standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
        block = ''
        block+=self.BeginDifferenceTable(len(nL))
        block+='$j='+str(j)+'$'
        block+=self.getTitleForDifferenceTableSpacePerPoint(case,fL,p)
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(l){'+str(2)+'-'+str(len(fL)+1)+'}\n'
        
        for name in nL:
            block+=' & '+name
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(l){'+str(2)+'-'+str(len(fL)+1)+'}\n'
        for l in self.lL[1:]:
            block+=' $\\ell='+str(l)+'$'
            for alg in range(len(fL)): 
                (t1,V1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m),p)
                (t2,V2)=read.getDataFromTXT(fL[alg],read.createFilename(l-1,j,self.m),p)
                if len(t1)==standardlength and len(t2)==standardlength:
                    (tact1,id_tact1)=comp.getActivaitionTimeFromVWithID(t1,V1,threshold)
                    (tact2,id_tact2)=comp.getActivaitionTimeFromVWithID(t2,V2,threshold)
                    value=-1000.0
                    if case=='tact':
                        value = 1000.0*(tact1-tact2)
                    elif case=='VInt':
                        id_dur1=self.getTimeID(id_tact1,j)
                        id_dur2=self.getTimeID(id_tact2,j)
                        value1=comp.L2(V1[id_tact1:id_dur1],self.sdt,self.T,j,False)
                        value2=comp.L2(V2[id_tact2:id_dur2],self.sdt,self.T,j,False)
                        value=value1-value2
                    elif case =='VDiffInt':
                        id_dur1=self.getTimeID(id_tact1,j)
                        id_dur2=self.getTimeID(id_tact2,j)
                        #print(p ,l,j,id_dur1,id_tact1,len(V1),len(V1[id_tact1:id_dur1]),tact1)
                        value=comp.L2DiffInterval(V1[id_tact1:id_dur1],V2[id_tact2:id_dur2],self.sdt,j,j)
                    elif case=='Ca':
                        (t1,Ca1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m)+'Ca',p)
                        (t2,Ca2)=read.getDataFromTXT(fL[alg],read.createFilename(l-1,j,self.m)+'Ca',p)
                        value=1000.0*comp.L2DiffInterval(Ca1,Ca2,self.sdt,j,j)
                    elif case=='Gamma':
                        (t1,G1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m)+'Gamma',p)
                        (t2,G2)=read.getDataFromTXT(fL[alg],read.createFilename(l-1,j,self.m)+'Gamma',p)
                        value=comp.L2DiffInterval(G1,G2,self.sdt,j,j)
                    if case =='tact':
                        block += " & $"+ str("%8.3f"%(value))+"$"
                    else:
                        block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - " 
            block+='\\\\\n'
            
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def getTitleForDifferenceTableTimePerPoint(self,case,fL,p):
        block=''
        if case=='tact':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\tact^{j,\\ell,'+str(p)+'}-\\tact^{j-1,\\ell,'+str(p)+'}$}'
        elif case=='VInt':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\V^{j,\\ell,'+str(p)+'}'+'}_\\LtwonormAct-\\norm{\\V^{j-1,\\ell,'+str(p)+'}'+'}_\\LtwonormAct$}'
        elif case =='VDiffInt':
             block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\V^{j,\\ell,'+str(p)+'}'+'-'+'\\V^{j-1,\\ell,'+str(p)+'}'+'}_\\LtwonormAct$}'
        elif case =='Ca':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\Ca^{j,\\ell,'+str(p)+'}'+'-'+'\\Ca^{j-1,\\ell,'+str(p)+'}'+'}_\\LtwonormT$}'
        elif case =='LV':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\LV^{j,\\ell}'+'-'+'\\LV^{j-1,\\ell}'+'}_\\LtwonormT$}'
        elif case =='RV':
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\RV^{j,\\ell}'+'-'+'\\RV^{j-1,\\ell}'+'}_\\LtwonormT$}'
        return block
    
    def writeDifferenceTableTimePerPoint(self,l,fL,nL,p,case,threshold=-50.0):
        block = ''
        block+=self.BeginDifferenceTable(len(nL))
        block+='$\ell='+str(l)+'$'
        block+=self.getTitleForDifferenceTableTimePerPoint(case,fL,p)
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(l){'+str(2)+'-'+str(len(fL)+1)+'}\n'
        
        for name in nL:
            block+=' & '+name
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(l){'+str(2)+'-'+str(len(fL)+1)+'}\n'
        for j in self.tL[1:]:
            block+=' $j='+str(j)+'$'
            for alg in range(len(fL)):
                standardlength1=int(round(self.T/(self.sdt*2**(-j))))+1
                standardlength2=int(round(self.T/(self.sdt*2**(-(j-1)))))+1
                (t1,V1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m),p)
                (t2,V2)=read.getDataFromTXT(fL[alg],read.createFilename(l,j-1,self.m),p)
                if len(t1)==standardlength1 and len(t2)==standardlength2:
                    (tact1,id_tact1)=comp.getActivaitionTimeFromVWithID(t1,V1,threshold)
                    (tact2,id_tact2)=comp.getActivaitionTimeFromVWithID(t2,V2,threshold)
                    value=-1000.0
                    if case=='tact':
                        value = 1000.0*(tact1-tact2)
                        block += " & $"+ str("%8.3f"%(value))+"$"
                    elif case=='VInt':
                        id_dur1=self.getTimeID(id_tact1,j)
                        id_dur2=self.getTimeID(id_tact2,j-1)

                        value1=comp.L2(V1[id_tact1:id_dur1],self.sdt,self.T,j,False)
                        value2=comp.L2(V2[id_tact2:id_dur2],self.sdt,self.T,j-1,False)
                        value=value1-value2
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    elif case =='VDiffInt':
                        id_dur1=self.getTimeID(id_tact1,j)
                        id_dur2=self.getTimeID(id_tact2,j-1)
                        #if alg==0:
                            #diff=[]
                            #Test1=V1[id_tact1:id_dur1]
                            #Test2=V2[id_tact2:id_dur2]
                            #for index in range(len(Test2)):
                                #diff.append(Test2[index]-Test1[2*index])
                            #for index in range(len(t1)):
                                #t1[index]-=tact1
                            #for index in range(len(t2)):
                                #t2[index]-=tact2
                            #print('max diff',max(diff))
                            #plt.plot(t2[id_tact2:id_dur2],diff,label='diff')
                            #plt.plot(t2[id_tact2:id_dur2],Test2,label='Test2')
                            #plt.plot(t1[id_tact1:id_dur1],Test1,label='Test1')
                            #plt.legend()
                            #plt.show()
                        #if j==3 and p==8:
                            #print(tact2,id_tact2,t2[id_tact2])
                            #plt.plot(t1[id_tact1:id_dur1],V1[id_tact1:id_dur1],label='V1 mit j='+str(j-1))
                            #plt.plot(t2[id_tact2:id_dur2],V2[id_tact2:id_dur2],label='V2 mit j='+str(j-1))
                            #plt.legend()
                            #plt.show()
                        value=comp.L2DiffInterval(V1[id_tact1:id_dur1],V2[id_tact2:id_dur2],self.sdt,j,j-1)
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    elif case=='Ca':
                        (t1,Ca1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m)+'Ca',p)
                        (t2,Ca2)=read.getDataFromTXT(fL[alg],read.createFilename(l,j-1,self.m)+'Ca',p)
                        value=1000.0*comp.L2DiffInterval(Ca1,Ca2,self.sdt,j,j-1)
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    elif case=='Gamma':
                        (t1,G1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m)+'Gamma',p)
                        (t2,G2)=read.getDataFromTXT(fL[alg],read.createFilename(l,j-1,self.m)+'Gamma',p)
                        value=comp.L2DiffInterval(G1,G2,self.sdt,j,j-1)
                        block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - " 
            block+='\\\\\n'
            
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
        
    def writeDifferenceTableSpace(self,j,fL,nL,p,case,threshold=-50.0):
        standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
        block = ''
        block+=self.BeginDifferenceTable(len(self.lL[1:]))
        block+='$j='+str(j)+'$'
        for l in self.lL[1:]:
            block+='& $\\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(l){'+str(2)+'-'+str(len(self.lL))+'}\n'
        for alg in range(len(fL)):
            block+=nL[alg]
            for level in self.lL[1:]:
                block+='& '
            block+='\\\\\n'
            block+='\\cmidrule(r){1-1}\n'
            for p in ePoints:
                if case=='V':
                    block+='$\\norm{\\V^{j,\\ell}_{'+str(p)+'}-\\V^{j,\\ell-1}_{'+str(p)+'}}$'
                    for l in self.lL[1:]:
                        (t1,V1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m),p)
                        (t2,V2)=read.getDataFromTXT(fL[alg],read.createFilename(l-1,j,self.m),p)
                        if len(t1)==standardlength and len(t2)==standardlength:
                            value=comp.L2DiffPot(V1,V2,self.sdt*2**(-j),self.T)
                            block += " & $"+ str("%8.4f"%(value))+"$"
                        else:
                            block+=" & - "
                elif case=='tact':
                    block+='$\\tact^{j,\\ell,'+str(p)+'}-\\tact^{j,\\ell-1,'+str(p)+'}$'
                    for l in self.lL[1:]:
                        (t1,V1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m),p)
                        (t2,V2)=read.getDataFromTXT(fL[alg],read.createFilename(l-1,j,self.m),p)
                        if len(t1)==standardlength and len(t2)==standardlength:
                            t_act1=comp.getActivaitionTimeFromV(t1,V1,threshold)
                            t_act2=comp.getActivaitionTimeFromV(t2,V2,threshold)
                            difference = t_act1-t_act2
                            block += " & $"+ str("%8.4f"%(difference))+"$"
                        else:
                          block+=" & - "  
                        
                else:
                    print('case ', case,'not known')
                block+='\\\\[3pt]\n'
            if alg<len(fL)-1:
                block+='\\cmidrule(r){1-1}\n'
                block+='\\cmidrule(l){'+str(2)+'-'+str(len(self.tL))+'}\n'
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    
    def writeDifferenceTableTime(self,l,fL,nL,ePoints,case,threshold=-50.0):
        block = ''
        block+=self.BeginDifferenceTable(len(self.tL[1:]))
        block+='$\\ell='+str(l)+'$'
        for j in self.tL[1:]:
            block+='& $j='+str(j)+'$'
        block+='\\\\\n'
        block+='\\cmidrule(r){1-1}\n'
        block+='\\cmidrule(l){'+str(2)+'-'+str(len(self.tL))+'}\n'
        for alg in range(len(fL)):
            block+=nL[alg]
            for jlevel in self.tL[1:]:
                block+='& '
            block+='\\\\\n'
            block+='\\cmidrule(r){1-1}\n'
            for p in ePoints:
                if case=='V':
                    block+='$\\norm{\\V^{j,\\ell}_{'+str(p)+'}-\\V^{j-1,\\ell}_{'+str(p)+'}}$'
                    for j in self.tL[1:]:
                        standardlength1=int(self.T/(self.sdt*2**(-j)))+1
                        standardlength2=int(self.T/(self.sdt*2**(-(j-1))))+1
                        (t1,V1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m),p)
                        (t2,V2)=read.getDataFromTXT(fL[alg],read.createFilename(l,j-1,self.m),p)
                        if len(t1)==standardlength1 and len(t2)==standardlength2:
                            value=comp.L2DiffPot(V1,V2,self.sdt*2**(-(j-1)),self.T)
                            block += " & $"+ str("%8.4f"%(value))+"$"
                        else:
                            block+=" & - "
                elif case=='tact':
                    block+='$\\tact^{j,\\ell,'+str(p)+'}-\\tact^{j-1,\\ell,'+str(p)+'}$'
                    for j in self.tL[1:]:
                        standardlength1=int(self.T/(self.sdt*2**(-j)))+1
                        standardlength2=int(self.T/(self.sdt*2**(-(j-1))))+1
                        (t1,V1)=read.getDataFromTXT(fL[alg],read.createFilename(l,j,self.m),p)
                        (t2,V2)=read.getDataFromTXT(fL[alg],read.createFilename(l,j-1,self.m),p)
                        if len(t1)==standardlength1 and len(t2)==standardlength2:
                            t_act1=comp.getActivaitionTimeFromV(t1,V1,threshold)
                            t_act2=comp.getActivaitionTimeFromV(t2,V2,threshold)
                            difference = t_act1-t_act2
                            block += " & $"+ str("%8.4f"%(difference))+"$"
                        else:
                          block+=" & - "  
                else:
                    print('case ', case,'not known')
                block+='\\\\[3pt]\n'
            if alg<len(fL)-1:
                block+='\\cmidrule(r){1-1}\n'
                block+='\\cmidrule(l){'+str(2)+'-'+str(len(self.tL))+'}\n'
            
                    
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def ApproxIionAndDuration(self,p,J,L,fL,nL,refname,divideNorm=False):
        block = ''
        block+='\\toprule\n'
        (t,Ref)=read.getDataFromTXT(self.path,read.createFilename(L,J,self.m),p)
        normRef=comp.L2(Ref,self.sdt,self.T,J,False)
        block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\V^{j,\\ell,'+str(p)+'}'+'-\\V_{'+refname+','+str(p)+'}^{\\Ref}}_\\LtwonormT$}'
        block+='&\\multicolumn{'+str(len(fL))+'}{c}{Rechenzeit}&'
        block+='\\\\\n'
        block+='\\addlinespace\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        block+='\cmidrule(lr){'+str(len(fL)+3)+'-'+str(2*len(fL)+2)+'}\n'
        
        block+='\\multicolumn{2}{c}{}'
        for name in nL:
            block+='& '+name
        for name in nL:
            block+='& '+name
        block+='&$\\#$ Prozesse\\\\\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        block+='\cmidrule(lr){'+str(len(fL)+3)+'-'+str(2*len(fL)+2)+'}\n'
        block+='\cmidrule(l){'+str(2*len(fL)+2+1)+'-'+str(2*len(fL)+2+1)+'}\n'
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                proc=0
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        value=comp.L2DiffPot(V,Ref,self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normRef
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    elif len(t)>standardlength:
                        value=comp.L2DiffPot(V[:standardlength],Ref[:standardlength],self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normRef
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                for alg in fL:
                    (procAlg,dur)=read.getProcsAndDur(alg,l,j,self.m)
                    if proc!=0 and proc!=procAlg:
                        print('Achtung, die Rechnungen sind auf unterschiedlichen Prozessoren gemacht!',proc,alg,procAlg,'j',j,'l',l)
                    proc=procAlg
                    if dur==0:
                        block+='& - '
                    else:
                        dur=dur[:-3]
                        block+='&$'+str(dur)+'$'
                block+='&$'+str(proc)+'$'
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
                block+='\cmidrule(lr){'+str(len(fL)+3)+'-'+str(2*len(fL)+2)+'}\n'
                block+='\cmidrule(l){'+str(2*len(fL)+2+1)+'-'+str(2*len(fL)+2+1)+'}\n'
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def CompareApproxIion(self,p,J,L,fL,nL,refname,divideNorm=False):
        block = ''
        (t,Ref)=read.getDataFromTXT(self.path,read.createFilename(L,J,self.m),p)
        normRef=comp.L2(Ref,self.sdt,self.T,J,False)
        block+= '\\multicolumn{2}{c|}{$\\norm{\\V^{j,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')-\\V_'+refname+'^{'+str(J)+','+str(L)+'}}_\\LtwonormT$}'
        for name in nL:
            block+='& '+name
        block+='\\\\\n'
        block+='\\midrule\n'
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        value=comp.L2DiffPot(V,Ref,self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normRef
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    elif len(t)>standardlength:
                        value=comp.L2DiffPot(V[:standardlength],Ref[:standardlength],self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normRef
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            block+='\\midrule\n'
        
        block+=self.EndTabular()
        return block
    def writeCompareRefList(self,p,J,L,fL,nL,refname = '\\Ref',divideNorm=False):
        block = ''
        (t,Ref)=read.getDataFromTXT(self.path,read.createFilename(L,J,self.m),p)
        normRef=comp.L2(Ref,self.sdt,self.T,J,False)
        if divideNorm:
            block+= '& \\multicolumn{'+str(len(fL)*len(self.lL))+'}{c}{$\\frac{\\|\\V^{j,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')-\\V_'+refname+'^{'+str(J)+','+str(L)+'}(\\cdot,'+self.getPointNamebyNumber(p,True)+')\\|_\\LtwonormT}{\\|\V_'+refname+'^{'+str(J)+','+str(L)+'}(\\cdot,'+self.getPointNamebyNumber(p,True)+')\\|_\\LtwonormT}$}\\\\[10pt]\n'
        else:
            block+= '& \\multicolumn{'+str(len(fL)*len(self.lL))+'}{c}{$\\|\\V^{j,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')-\\V_'+refname+'^{'+str(J)+','+str(L)+'}(\\cdot,'+self.getPointNamebyNumber(p,True)+')\\|_\\LtwonormT$}\\\\[5pt]\n'
        for name in nL[:-1]:
            block+='&'
            block +='\\multicolumn{'+str(len(self.lL))+'}{c|}{'+name+'}'
        block+='&\\multicolumn{'+str(len(self.lL))+'}{c}{'+nL[-1]+'}\\\\\n'
        block+=self.getPointNamebyNumber(p)
        for index in range(len(fL)):
            #block+='&'
            for l in self.lL:
                block+='& $\\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for j in self.tL:
            block += '$j='+str(j)+'$'
            for alg in fL:
                #block+='& '
                for l in self.lL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        value=comp.L2DiffPot(V,Ref,self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normRef
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    elif len(t)>standardlength:
                        value=comp.L2DiffPot(V[:standardlength],Ref[:standardlength],self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normRef
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
            block+='\\\\\n'
        #block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeConSpaceList(self,p,J,L,fL,nL,divideNorm=False):
        block = ''
        refTList=[]
        refVList=[]
        refNormList=[]
        extraList= []
        normExtraList =[]
        standardlength=int(self.T/(self.sdt*2**(-J)))+1
        for index in range(len(nL)):
            (t,V)= read.getDataFromTXT(fL[index],read.createFilename(L,J,self.m),p)
            if len(t)>standardlength:
                V=V[:standardlength]
            normRef=comp.L2(V,self.sdt,self.T,J,False)
            ExtraV=comp.getExtrapolateInSpace(L,J,self.m,p,self.sdt,self.T,fL[index])
            if len(ExtraV)>standardlength:
                print(len(ExtraV))
                ExtraV=ExtraV[:standardlength]
            
            normExtra =comp.L2(ExtraV,self.sdt,self.T)
            refTList.append(t)
            refVList.append(V)
            refNormList.append(normRef)
            extraList.append(ExtraV)
            normExtraList.append(normExtra)
            block+='&'
            block +='\\multicolumn{'+str(len(self.tL))+'}{c}{'+nL[index]+'}'
        block+='\\\\\n'
        block+=self.getPointNamebyNumber(p)
        for index in range(len(fL)):
            #block+='&'
            for j in self.tL:
                block+='& $j='+str(j)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for l in self.lL:
            block += '$\\ell='+str(l)+'$'
            for index in range(len(fL)):
               # #block+='& '
                for j in self.tL:
                    (t,V)=read.getDataFromTXT(fL[index],read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        value=comp.L2DiffPot(V,extraList[index],self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normExtraList[index]
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    elif len(t)>standardlength:
                        value=comp.L2DiffPot(V[:standardlength],extraList[index][:standardlength],self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normExtraList[index]
                        block += " & $"+ str("%8.4f"%(value))+"$"
                        
                    else:
                        block+=" & - "
            block+='\\\\\n'
        block +='\\midrule'
        block +='$\\| \\V^{j,\\infty}(\\cdot,'+self.getPointNamebyNumber(p,True)+')\\|_\\LtwonormT$'
        for norm in normExtraList:
            block +='& $'+str(norm)+'$'
        block+='\\\\\n'
        block +='\\midrule'
        block +='$\\| \\V^{j,'+str(L)+'}(\\cdot,'+self.getPointNamebyNumber(p,True)+')\\|_\\LtwonormT$'
        for norm in refNormList:
            block +='& $'+str(norm)+'$'
        block+='\\\\\n'
        
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeConTimeListDiffExtra(self,p,J,L,fL,nL,divideNorm=False):
        block = ''
        timeExtraV=comp.getExtrapolateInTime(L,J,self.m,p,self.sdt,self.T,self.path)
        normTimeExtra =comp.L2(timeExtraV,self.sdt,self.T)
        for index in range(len(fL)):
            #block+='&'
            for l in self.lL:
                block+='& $\\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for j in self.tL:
            block += '$j='+str(j)+'$'
            for index in range(len(fL)):
               # #block+='& '
                for l in self.lL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(fL[index],read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        value=comp.L2DiffPot(V,timeExtraV,self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normTimeExtra
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
            block+='\\\\\n'
        #block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
            
            
            
    def writeConTimeList(self,p,J,L,fL,nL,divideNorm=False):
        block = ''
        #(t,Ref)=read.getDataFromTXT(self.path,read.createFilename(L,J,self.m),p)
        #normRef=comp.L2(Ref,self.sdt,self.T,J,False)
        refTList=[]
        refVList=[]
        refNormList=[]
        extraList= []
        normExtraList =[]
        for index in range(len(nL)):
            (t,V)= read.getDataFromTXT(fL[index],read.createFilename(L,J,self.m),p)
            normRef=comp.L2(V,self.sdt,self.T,J,False)
            timeExtraV=comp.getExtrapolateInTime(L,J,self.m,p,self.sdt,self.T,fL[index])
            normTimeExtra =comp.L2(timeExtraV,self.sdt,self.T)
            refTList.append(t)
            refVList.append(V)
            refNormList.append(normRef)
            extraList.append(timeExtraV)
            normExtraList.append(normTimeExtra)
            block+='&'
            block +='\\multicolumn{'+str(len(self.lL))+'}{c}{'+nL[index]+'}'
        block+='\\\\\n'
        block+=self.getPointNamebyNumber(p)
        for index in range(len(fL)):
            #block+='&'
            for l in self.lL:
                block+='& $\\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for j in self.tL:
            block += '$j='+str(j)+'$'
            for index in range(len(fL)):
               # #block+='& '
                for l in self.lL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(fL[index],read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        value=comp.L2DiffPot(V,extraList[index],self.sdt*2**(-j),self.T)
                        if divideNorm:
                            value=value/normExtraList[index]
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
            block+='\\\\\n'
        block +='\\midrule'
        block +='$\\| \\V^{\\infty,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')\\|_\\LtwonormT$'
        for norm in normExtraList:
            block +='& $'+str("%8.4f"%(norm))+'$'
        block+='\\\\\n'
        block +='\\midrule'
        block +='$\\| \\V^{'+str(J)+',\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')\\|_\\LtwonormT$'
        for norm in refNormList:
            block +='& $'+str("%8.4f"%(norm))+'$'
        block+='\\\\\n'
        
        #block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeValueMaxNorm(self,p):
        block = ''
        for j in self.tL:
            block += '$j='+str(j)+'$'
            for l in self.lL:
                standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                (t,V)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                if len(t)==standardlength:
                    value =comp.MaxNorm(V)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\\n'
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    def writeValueColums(self,p,withExtrapolates=True):
        block = ''
        for l in self.lL:
            block+='& $\\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for j in self.tL:
            block += '$j='+str(j)+'$'
            for l in self.lL:
                standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                (t,V)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                if len(t)==standardlength:
                    value =comp.L2(V,self.sdt,self.T)
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            if withExtrapolates:
                value2=comp.L2(comp.getExtrapolateInSpace(self.lL[-1],j,self.m,p,self.sdt,self.T,self.path),self.sdt,self.T)
                block+='& $'+str("%8.4f"%(value2))+' $ '
            block+= '\\\\\n'
        block+='\\hline\n'
        if withExtrapolates:
            block += '$\\| \\V^{\\infty,\\ell}  \\|_\\LtwonormT$'
            for l in self.lL:
                value3=comp.L2(comp.getExtrapolateInTime(l,self.tL[-1],self.m,p,self.sdt,self.T,self.path),self.sdt,self.T)
                block += " & "+ str("%8.4f"%(value3))
            value4=comp.L2(comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,p,self.sdt,self.T,self.path),self.sdt,self.T)
            block+='& $ '+str("%8.4f"%(value4))+' $ '
            block+= '\\\\\n'
        return block
    
    def writeBeginExtrapolationLongtable(self,caption,p):
        block = ''
        block+= '\\begin{longtable}[H]{l|'
        for l in range(self.lL[0],len(self.lL)+self.lL[0]):
            block+='r'
        block+='|r} \n'
        block+= caption + '\\\\ \n' 
        #block+= '\\caption{Normalized differences to the space and time extrapolates $\\frac{|\\| \\V^{j,\\ell}  \\|_\\LtwonormT - \\| \\V^{\\infty,\\infty}  \\|_\\LtwonormT|}{\\| \\V^{\\infty,\\infty}  \\|_\\LtwonormT}$ for different time and space levels in \\textbf{P'+str(p)+'}.}' + '\\\\ \n' 
        block += ' '
        for l in range(self.lL[0],len(self.lL)+self.lL[0]):
            block +=  '&$ \\ell=' + str(l)+'$' 
        block+= '& \\\\\n'
        block+='\\hline\n'
        return block
    def writeFullExtrapolationColums(self,p):
        V_infty =comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,p,self.sdt,self.T,self.exP)
        V_infty_L2=comp.L2(V_infty,self.sdt,self.T)
        block = ''
        for j in self.tL:
            block += '$j='+str(j)+'$'
            for l in self.lL:
                (t,V)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                if len(t)==0:
                    block += ' & $ - $ '
                else:
                    value =comp.L2DiffPot(V,V_infty,self.sdt,self.T)/V_infty_L2
                    block += " & $ "+ str("%8.4f"%(value))+' $ '
            
            block+='&'
            block+= '\\\\\n'
        return block
    
    def writeExtraTimeColums(self,p):
        block = ''
        for j in self.tL:
            block += '$j='+str(j)+'$'
            V_jinfty =comp.getExtrapolateInSpace(self.lL[-1],j,self.m,p,self.sdt,self.T,self.path)
            V_jinfty_L2=comp.L2(V_jinfty,self.sdt,self.T)
            for l in self.lL:
                (t,V)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                value =comp.L2DiffPot(V,V_jinfty,self.sdt,self.T)/V_jinfty_L2
                block += " & $ "+ str("%8.4f"%(value))+' $ '
            
            block+='&'
            block+= '\\\\\n'
        return block
    
    def writeExtraSpaceColums(self,p):
        block = ''
        for j in self.tL:
            block += '$j='+str(j)+'$'
            for l in self.lL:
                (t,V)=read.getDataFromTXT(self.path,read.createFilename(l,j,self.m),p)
                V_inftyl =comp.getExtrapolateInTime(l,self.tL[-1],self.m,p,self.sdt,self.T,self.path)
                V_inftyl_L2=comp.L2(V_inftyl,self.sdt,self.T)
                value = comp.L2DiffPot(V,V_inftyl,self.sdt,self.T)/V_inftyl_L2
                block += " & $ "+ str("%8.4f"%(value))+' $ '
            block+='&'
            block+= '\\\\\n'

        return block
    def writeExtraErrorRows1(self,fL,nL,evalPoints,J,L,dPairs):
        block =''
        block += '$\\V^\\text{fine}$:' 
        block +='&\\multicolumn{'+str(len(evalPoints))+'}{c|}{$\\V_'+nL[0]+'^{'+str(dPairs[0][0])+','+str(dPairs[0][1])+'}(\\cdot,\\points)$'+'}'
        block +='&\\multicolumn{'+str(len(evalPoints))+'}{c}{$\\V_'+nL[1]+'^{'+str(dPairs[1][0])+','+str(dPairs[1][1])+'}(\\cdot,\\points)$'+'}'
        block+='\\\\[5pt]\n'
        block += 'relative error'
        for index in range(len(fL)):
            for p in evalPoints:
                block +='&' + self.getPointNamebyNumber(p,False)
        block+='\\\\\n'
        block+='\\midrule\n'
        RE =['$\\frac{\\|\\V^{5,\\infty}(\\cdot,\\points)- \\V^{\\text{fine}}(\\cdot,\\points)\\|_\\LtwonormT}{\\|\\V^{5,\\infty}(\\cdot,\\points)\\|_\\LtwonormT}$','$\\frac{\\|\\V^{\\infty,5}(\\cdot,\\points)- \\V^{\\text{fine}}(\\cdot,\\points)\\|_\\LtwonormT}{\\|\\V^{\\infty,5}(\\cdot,\\points)\|_\\LtwonormT}$','$\\frac{\\|\\V^{\\infty,\\infty}(\\cdot,\\points)- \\V^{\\text{fine}}(\\cdot,\\points)\\|_\\LtwonormT}{\\|\\V^{\\infty,\\infty}(\\cdot,\\points)\\|_\\LtwonormT}$']
        V_space=[]
        V_time=[]
        V_infty=[]
        for alg in fL:
            V_spacePoints=[]
            V_timePoints=[]
            V_fullPoints=[]
            for p in evalPoints:
                V_spacePoints.append(comp.getExtrapolateInSpaceDefinedOrder(2,J,L,p,self.m,alg))
                V_timePoints.append(comp.getExtrapolateInTimeDefinedOrder(1,J,L,p,self.m,alg))
                V_fullPoints.append(comp.getFullExtrapolateDefinedOrder(1,2,J,L,p,self.m,alg))
            V_space.append(V_spacePoints)
            V_time.append(V_timePoints)
            V_infty.append(V_fullPoints)
        ExtraList=[V_space,V_time,V_infty]    
        for re in range(len(RE)):
            block +=RE[re]
            for index in range(len(fL)):
                for p in range(len(evalPoints)):
                    (time,V)= read.getDataFromTXT(fL[index],read.createFilename(dPairs[index][1],dPairs[index][0],1),evalPoints[p])
                    value=comp.L2DiffPot(V,ExtraList[re][index][p],self.sdt,self.T)/(comp.L2(ExtraList[re][index][p],self.sdt,self.T))
                    block+='&$'+str("%8.4f"%(value))+'$'
            block+='\\\\[10pt]\n'
        return block
    def writeExtraErrorRows2(self,fL,nL,evalPoints,J,L,dPairs):
        block =''
        RE =['$\\frac{\\|\\V^{5,\\infty}(\\cdot,\\points)- \\V^{\\text{fine}}(\\cdot,\\points)\\|_\\LtwonormT}{\\|\\V^{5,\\infty}(\\cdot,\\points)\\|_\\LtwonormT}$','$\\frac{\\|\\V^{\\infty,5}(\\cdot,\\points)- \\V^{\\text{fine}}(\\cdot,\\points)\\|_\\LtwonormT}{\\|\\V^{\\infty,5}(\\cdot,\\points)\|_\\LtwonormT}$','$\\frac{\\|\\V^{\\infty,\\infty}(\\cdot,\\points)- \\V^{\\text{fine}}(\\cdot,\\points)\\|_\\LtwonormT}{\\|\\V^{\\infty,\\infty}(\\cdot,\\points)\\|_\\LtwonormT}$']
        block += '$\\V^\\text{fine}$' + '& point'
        for re in RE:
            block += '&' +re
        block += '\\\\\n'
        block +='\midrule\n'
        
        for index in range(len(fL)):
            block += '$\\V_'+nL[index]+'^{'+str(dPairs[index][0])+','+str(dPairs[index][1])+'}(\\cdot,\\points)$'
            for p in evalPoints:
                block +='&'+self.getPointNamebyNumber(p,False)
                (time,V)= read.getDataFromTXT(fL[index],read.createFilename(dPairs[index][1],dPairs[index][0],1),p)
                V_space=comp.getExtrapolateInSpace(L,J,1,p,self.sdt,self.T,fL[index])
                value=comp.L2DiffPot(V,V_space,self.sdt,self.T)/comp.L2(V_space,self.sdt,self.T)
                block+='&$'+str("%8.4f"%(value))+'$'
                V_time=comp.getExtrapolateInTime(L,J,1,p,self.sdt,self.T,self.path)
                value=comp.L2DiffPot(V,V_time,self.sdt,self.T)/comp.L2(V_time,self.sdt,self.T)
                block+='&$'+str("%8.4f"%(value))+'$'
                V_full=comp.getFullExtrapolate(L,J,1,p,self.sdt,self.T,self.path)
                value=comp.L2DiffPot(V,V_full,self.sdt,self.T)/comp.L2(V_full,self.sdt,self.T)
                block+='&$'+str("%8.4f"%(value))+'$'
                block+='\\\\\n'
            block+='\\midrule\n'
        return block    
    def ExtraErrorTable(self,evalPoints,cap,label,L,J,name='\\Ref'):
        standardlength=int(self.T/(self.sdt))+1
        block=''
        block=self.writeBeginTable()
        block+=self.writeTabelCapLab(cap,label)
        block+='\\begin{tabular}{l|c|c|c}\n'
        #block+='\\toprule\n'
        
        block += '& $\\frac{\\|\\V^{5,\\infty}(\\cdot,\\points)- \\V^{'+str(J)+','+str(L)+'}(\\cdot,\\points)\\|_\\LtwonormT}{\\|\\V^{5,\\infty}(\\cdot,\\points)\\|_\\LtwonormT}$'
        block+='& $\\frac{\\|\\V^{\\infty,5}(\\cdot,\\points)- \\V^{'+str(J)+','+str(L)+'}(\\cdot,\\points)\\|_\\LtwonormT}{\\|\\V^{\\infty,5}(\\cdot,\\points)\|_\\LtwonormT}$'
        block+='&  $\\frac{\\|\\V^{\\infty,\\infty}(\\cdot,\\points)- \\V^{'+str(J)+','+str(L)+'}(\\cdot,\\points)\\|_\\LtwonormT}{\\|\\V^{\\infty,\\infty}(\\cdot,\\points)\\|_\\LtwonormT}$\\\\\n'
        block+='\\midrule\n'
        for p in evalPoints:
            block+=self.getPointNamebyNumber(p)
            (time,V)= read.getDataFromTXT(self.path,read.createFilename(L,J,1),p)
            if len(V)>standardlength:
                k_V = int((len(V)-1)/(standardlength-1))
                cuttedV=[]
                for i in range(standardlength):
                    cuttedV.append(float(V[k_V*i]))
                V=cuttedV
            V_space=comp.getExtrapolateInSpace(self.lL[-1],self.tL[-1],1,p,self.sdt,self.T,self.path)
            V_space_L2=comp.L2(V_space,self.sdt,self.T)
            value_space=comp.L2DiffPot(V,V_space,self.sdt,self.T)/V_space_L2
            block+='&$'+str("%8.4f"%(value_space))+'$'
            V_time=comp.getExtrapolateInTime(self.lL[-1],self.tL[-1],1,p,self.sdt,self.T,self.path)
            V_time_L2=comp.L2(V_time,self.sdt,self.T)
            value_time=comp.L2DiffPot(V,V_time,self.sdt,self.T)/V_time_L2
            block+='&$'+str("%8.4f"%(value_time))+'$'
        
            V_infty =comp.getFullExtrapolate(self.lL[-1],self.tL[-1],1,p,self.sdt,self.T,self.path)
            V_infty_L2=comp.L2(V_infty,self.sdt,self.T)
            value_all =comp.L2DiffPot(V,V_infty,self.sdt,self.T)/V_infty_L2
            block+='&$'+str("%8.4f"%(value_all))+'$\\\\\n'
         
        #block+='\\bottomrule\n'
        block+='\\end{tabular}\n'
        block+=self.writeEndTable()
        return block
    def writeExtrapolationSpaceColums(self,p):
        block = ''
        Vex=comp.getExtrapolateInSpace(self.lL[-1],self.tL[-1],self.m,p,self.sdt,self.T,self.path)
        for j in self.tL:
            block += '$j='+str(j)+'$'
            for l in self.lL:
                value =float(comp.L2DiffEx(self.path,l,j,self.m,Vex,p,self.sdt,self.T)/comp.L2(Vex,self.sdt,self.T))
                block += " & $ "+ str("%8.4f"%(value)) +' $ '
            block+= '\\\\\n'
            
        return block
    
    def writeBeginTimeLongtable(self):
        block = ''
        block+= '\\begin{longtable}[H]{'+ self.tableRowsOrder(len(self.lL))+'} \n'
        block+= '\\caption{Convergence in time for fixed substep $m='+str(self.m)+'$  and $t_\\text{beg}= 0. '+ str(self.timename(0)) + '$ s with $g_{j,\\ell} = \\| V^{j+1,\\ell}  - V^{j,\\ell} \\|_\\LtwonormT$ as  the differences of calculated potentials. The rate at timestep $j$ with fixed  level $\\ell$ is computed by $\\operatorname{log}_2\\frac{g_{j,\\ell}}{g_{j+1,\\ell}}$.}' + '\\\\ \n' #\\frac{  }{\\|V^{J,L}\\|_\\L2}  normalized
        block += '$j$'
        for l in range(self.lL[0],len(self.lL)+self.lL[0]):
            block +=  '&$ g_{j,' + str(l) + '}$ &  ' 
        block+= '\\\\\n'
        block+='\\hline\n'
        return block
    
    def writeTimeData(self,V,p,totalNumberOfEvaluationPointsWithNorms):
        #Points=read.getPoints(self.path,self.lL[0],self.tL[0],self.m,totalNumberOfEvaluationPointsWithNorms)
        block = ''
        if p<totalNumberOfEvaluationPointsWithNorms-2:
            block+='\\textbf{P'+str(p)+'}'
            #if len(self.lL)<=5:
                #block+='('
                #dim=len(Points[p-1])
                #for i in range(dim-1):
                    #block+= Points[p-1][i]+', '
                #block+= Points[p-1][-1]+')'
        elif p==totalNumberOfEvaluationPointsWithNorms-2:
            block+= '$\\textbf{L}_2$'
        elif p== totalNumberOfEvaluationPointsWithNorms-1:
            block+= '$\\textbf{H}_1$'
        else:
            block+='TEST'
        for j in range(self.lL[0],len(self.lL)+self.lL[0]):
            block +=" & & "
        block+= '\\\\\n'
        for j in range(len(self.tL)-1):
            block += str(j) 
            for l in range(self.lL[0],len(self.lL)+self.lL[0]):
                block += " & $"+ str("%8.4f"%(float(comp.L2Diff(self.path,l,j,self.m,l,j+1,self.m,p,self.sdt,self.T))))+ ' $ & '  #/float(L2(V))
            block+= '\\\\\n'
            t = j
            if t!= len(self.tL)-2:
                block +='  '
                for l in range(self.lL[0],len(self.lL)+self.lL[0]):
                    value1 = float(comp.L2Diff(self.path,l,t,self.m,l,t+1,self.m,p,self.sdt,self.T))
                    value2 = float(comp.L2Diff(self.path,l,t+1,self.m,l,t+2,self.m,p,self.sdt,self.T))
                    block += "&  & \\textbf{  "+ str("%8.2f"%(math.log(value1/value2,2)))+ "}" 
                block+='\\\\\n'
        block+='\\hline\n'
        return block

    def writeBeginSpaceLongtable(self):
        block = ''
        block+= '\\begin{longtable}[H]{'+ self.tableRowsOrder(len(self.tL))+'} \n'
        block+= '\\caption{Convergence in space for fixed substep $m='+str(self.m)+'$ and $t_\\text{beg}= 0.'+ str(self.timename(0)) + '$s with $f_{j,\\ell} = \\| V^{j,\\ell+1}  - V^{j,\\ell} \\|_\\LtwonormT$ as the  differences of calculated potentials. The rate at level $\\ell$ with fixed  time step $j$ is computed by $\\operatorname{log}_2\\frac{f_{j,\\ell}}{f_{j,\\ell+1}}$.}' + '\\\\ \n' #\\frac{  }{\\|V^{J,L}\\|_\\L2}  normalized
        block += '$\\ell$'
        for i in range(len(self.tL)):
            block +=  '&$ f_{' + str(i) + ',\\ell}$ &  ' 
        block+= '\\\\\n'
        block+='\\hline\n'
        return block
    
    def writeSpaceData(self,V,p,totalNumberOfEvaluationPointsWithNorms):
        #Points=read.getPoints(self.path,self.lL[0],self.tL[0],self.m,totalNumberOfEvaluationPointsWithNorms)
        block = ''
        if p<totalNumberOfEvaluationPointsWithNorms-2:
            block+='\\textbf{P'+str(p)+'}'
            #if len(self.tL)<=5:
                #block+='('
                #dim=len(Points[p-1])
                #for i in range(dim-1):
                    #block+= Points[p-1][i]+', '
                #block+= Points[p-1][-1]+')'
        elif p==totalNumberOfEvaluationPointsWithNorms-2:
            block+= '$\\textbf{L}_2$'
        elif p== totalNumberOfEvaluationPointsWithNorms-1:
            block+= '$\\textbf{H}_1$'
        else:
            block+='TEST'
        for j in range(len(self.tL)):
            block +=" & & "
        block+= '\\\\\n'
        for l in range(self.lL[0],len(self.lL)+self.lL[0]-1):
            block += str(l) 
            for j in range(len(self.tL)):
                block += " & $ "+ str("%8.4f"%(float(comp.L2Diff(self.path,l,j,self.m,l+1,j,self.m,p,self.sdt,self.T))))+ ' $ & '  #/float(L2(V))
            block+= '\\\\\n'
            level = l
            if level!= self.lL[-1]-1:
                block +='  '
                for j in range(len(self.tL)):
                    value1 = float(comp.L2Diff(self.path,level,j,self.m,level+1,j,self.m,p,self.sdt,self.T))
                    value2 = float(comp.L2Diff(self.path,level+1,j,self.m,level+2,j,self.m,p,self.sdt,self.T))
                    block += "&  & \\textbf{"+str("%8.2f"%(math.log(value1/value2,2)))+ "}"
                block+='\\\\\n'
        block+='\\hline\n'
        return block


    def writeEndLongTable(self):
        block = ''
        block +='\\end{longtable}\n'
        return block
    def writeBeginTable(self,pos='H'):
        block = ''
        block+= '\\begin{table}['+pos+']\n'
        return block
    def writeEndTable(self):
        block = ''
        block +='\\end{table}\n'
        return block
    def writeBeginMinMaxLongtable(self):
        block = ''
        block+= '\\begin{longtable}[H]{l|l|l|c|c|c|c|c|c|c|c} \n'
        block+= '\\caption{Minimal and maximal values of $\\V$, $\\Ca$ and $\\w$ for different space and time discretizations.}' + '\\\\ \n' 
        block += '$\\ell$ & $j$ & & $\\V$ & $\\Ca$ & $ d$ & $f $ & $h $ &  $j $& $m $ & $x_1$ '
        block+= '\\\\\n'
        block+='\\hline\n'
        return block
    
    def writeMinMaxRows(self,l,j,min,max):
        block=''
        
        #Minrow
        block += str(l) +' & '+str(j) +' & ' + 'Min' 
        for value in min:
            block += ' & ' 
            if(abs(value) < 0.009):
                block+=' $ '+str("%8.3e" % Decimal(value))+' $ ' #"%8.4f"%
            else:
                block+=' $ '+str("%8.3f" % (value))+' $ '
                    
        block+= '\\\\\n'
        
        #Maxrow
        block +=  ' & '+' & ' + 'Max' 
        for value in max:
            block += ' & ' 
            if(abs(value) < 0.009):
                block+=' $ '+str("%8.3e" % Decimal(value))+' $ ' #"%8.4f"%
            else:
                block+=' $ '+str("%8.3f" % (value))+' $ '
        block+= '\\\\\n'
        block+='[7pt]'
        #block+= '\\\\\n'
        return block
    def writeEndMinMaxTable(self):
        block = ''
        block +='\\end{longtable}\n'
        return block
    
        
    def writeDurationRows(self):
        block =''
        for j in self.tL:
            block +='j='+str(j)
            for l in self.lL:
                fn = read.createFilename(l,j,self.m)
                duration=read.getDurationFromLog(self.path,fn)
                block+= ' & '+ duration 
            block+='& \\\\\n'
                
        return block
            
    def writeBeginDurationLongtable(self):
        block = ''
        block+= '\\begin{longtable}[H]{l'
        for l in self.lL:
            block +='|r'
    
        block+='|l} \n'
        block+= '\\caption{Duration  with different discretizations using 64 procs on node 21+22 (h:min:sec).}' + '\\\\ \n' 
        for l in self.lL:
            block += ' & $\\ell='+str(l)+'$'
        block+= '& \\\\\n'        
        block+='\\hline\n'
        return block
    
    def writeRowsDurationSteps(self,n,c):
        block =''
        for j in self.tL:
             for m in range(n):
                if m==0:
                    block +='j='+str(j)
                else:
                    block += ' '
                for l in self.lL:
                    fn = read.createFilename(l,j,self.m)
                    duration=read.getDurationFromTXT(self.path,fn,n,c)
                    block+= ' & '+ str(duration[m])
                block+='& \\\\\n'
        return block
        
    def writeBeginDurationTable(self,caption):
        block = ''
        block+= '\\begin{longtable}[H]{l'
        for l in self.lL:
            block +='|r'
    
        block+='|l} \n'
        block+= '\\caption{'+caption+'.}' + '\\\\ \n' 
        for l in self.lL:
            block += ' & $\\ell='+str(l)+'$'
        block+= '& \\\\\n'        
        block+='\\hline\n'
        return block
    def addEvaluationToCaption(self,p):
        if p==9:
            return ' for $\\textbf{L}_2$.}'
        else:
         return ' in \\textbf{P'+str(p)+'}.}'
    
    def addEvaluationPointName(self,p):
        if p==9:
            return 'L2'
        else:
         return 'P'+str(p)
        
    def getFolderPath(self):
        return self.path
    
    def BeginDifferenceTable(self,length,alignment='c'):
        block = ''
        block+= '\\begin{tabular}{'+alignment
        for i in range(length):
            if alignment!='c':
                block+=alignment
            else:
                block +='c'
        block+='} \n' 
        block+='\\toprule\n'
        return block
    def writeBeginTableSpace(self,line,ifCoupled=False):
        block = ''
        if ifCoupled:
            block+= '\\begin{tabular}[H]{lcrrrr} \n'   
            block+='\\toprule\n'
            block += ' & $\\ell$ & '+line
        else:
            block+= '\\begin{tabular}[H]{crrrr} \n'
            block+='\\toprule\n'
            block += ' $\\ell$ & '+line
        block+= ' \\\\\n'        
        block+='\\midrule\n'
        return block
    
    def writeMeshInfoRowsCoupled(self,):
        block=''
        for l in self.lL:
            (min_dx,max_dx,cells,vertices)=read.getMeshInfoCoupled(self.path,l,self.tL[0])
            print(min_dx,max_dx,cells,vertices)
            for i in range(len(cells)):
                print(cells[i],vertices[i])
                cells[i]=self.addSpaces(str(cells[i]))
                vertices[i] = self.addSpaces(str(vertices[i]))
            if l==1:
                block+='mech &'+ str(l)+'& $'+str(format(float(min_dx[1]), '.4f'))+'$& $'+str(format(float(max_dx[1]), '.4f'))+'$& $'+str(cells[1])+'$&$ '+str(vertices[1])+'$\\\\[5pt]\n'
               
                block+='electro &'+ str(l)+'& $'+str(format(float(min_dx[0]), '.4f'))+'$& $'+str(format(float(max_dx[0]), '.4f'))+'$& $'+str(cells[0])+'$&$ '+str(vertices[0])+'$\\\\\n'
            else:
                block+= ' &'+ str(l)+'& $'+str(format(float(min_dx[0]), '.4f'))+'$& $'+str(format(float(max_dx[0]), '.4f'))+'$& $'+str(cells[0])+'$&$ '+str(vertices[0])+'$\\\\\n'
        block+='\\bottomrule\n'
        return block
    
    def writeMeshInfoRows(self,):
        block=''
        for l in self.lL:
            (min_dx,max_dx,cells,vertices)=read.getMeshInfo(self.path,l,self.tL[0])
            print(min_dx,max_dx,cells,vertices)
            cells=self.addSpaces(str(cells))
            vertices = self.addSpaces(str(vertices))
            block+= str(l)+'& $'+str(format(float(min_dx), '.4f'))+'$& $'+str(format(float(max_dx), '.4f'))+'$& $'+str(cells)+'$&$ '+str(vertices)+'$\\\\\n'
        return block
    def addSpaces(self,string):
        fString=''
        returnString=''
        i =1
        for s in string[::-1]:
            fString+=s
            if i%3==0:
                fString+=',\\'
            i+=1
            
        for s in fString[::-1]:
            returnString+=s
        return returnString
    
    def EndTabular(self):
        block='\\end{tabular}\n'
        return block
    def writeBeginScalability(self,cap,label):
        block=''
        block=self.writeBeginTable('h')
        block+=self.writeTabelCapLab(cap,label)
        block+='\\begin{tabular}{l|r}\n'
        block+='\\toprule\n'
        return block
    def writeBeginDurationTabList(self,cap,label,fL):
        block=''
        block=self.writeBeginTable('h')
        block+=self.writeTabelCapLab(cap,label)
        block+='\\begin{tabular}{c|c'
        for index in range(len(fL)):
            #block+='|c'
            for l in self.lL:
                block+='|r|r'
        block+='|}\n'
        return block
    def writeBeginValueTable(self,p,cap):
        block=''
        block=self.writeBeginTable()
        block+='\\caption{'+cap+'.}\n'
        block+='\\centering \n'
        block+='\\begin{tabular}{l'
        for s in self.path:
            block+='|c'
        block+='}\n'
        return block
    def writeBeginTimeLengthTable(self,p,cap,label=''):
        block=''
        block=self.writeBeginTable()
        block+=self.writeTabelCapLab(cap,label)
        block+='\\begin{tabular}{l'
        for j in self.tL:
            block+='|c'
        block+='}\n'
        block+='\\toprule\n'
        return block
    def writeTabelCapLab(self,cap,label):
        block=''
        block+='\\caption{'+cap+'.}\n'
        if label!='':
            block+='\\label{'+label +'}\n'
        block+='\\centering \n'
        return block
    def writeBeginDiffTable(self,p,caption,label,fL,withToprule=True):
        block=''
        block=self.writeBeginTable()
        block+=self.writeTabelCapLab(caption,label)
        block+='\\begin{tabular}{l'
        for alg in fL:
            block += '|c'
        block+='}\n'
        if withToprule:
            block+='\\toprule\n'
        return block
    def beginTabular(self,fL,which):
        block=''
        block+='\\begin{tabular}{l'
        for index in range(len(fL)):
            if which=='space':
                for l in self.lL:
                    block+='|c'
            elif which =='time':
                for j in self.tL:
                    block +='|c'
            else:
                print('case not implemented')
        block+='}\n'
        return block
    def writeBeginApproxAndDurationTabList(self,p,cap,label,fL,withDuration=True):
        block=''
        block=self.writeBeginTable('h')
        block+=self.writeTabelCapLab(cap,label)
        block+='\\begin{tabular}{cc'
        for index in range(len(fL)):
            block+='c'
        if withDuration==True:
            for index in range(len(fL)):
                block+='r'
            block+='c}\n'
        else:
            block+='}\n'
        return block
    def writeBeginApproxIiionTable(self,p,cap,label,fL):
        block=''
        block=self.writeBeginTable()
        block+=self.writeTabelCapLab(cap,label)
        block+='\\begin{tabular}{c|c'
        for index in range(len(fL)):
            block+='|c'
        block+='}\n'
        return block
        
    def writeBeginSmoothingTable(self,p,cap,label,fL,which='space',withToprule=True):
        block=''
        block=self.writeBeginTable()
        block+=self.writeTabelCapLab(cap,label)
        block+='\\begin{tabular}{l'
        for index in range(len(fL)):
            if which=='space':
                #block+='|c'
                for l in self.lL:
                    block+='|c'
            elif which=='time':
                for j in self.tL:
                    block +='|c'
            else:
                print('case not implemented')
        block+='}\n'
        if withToprule:
            block+='\\toprule\n'
        return block
    def writeBeginSpaceAndTimeLengthTable(self,p,cap,label=''):
        block=''
        block=self.writeBeginTable()
        block+=self.writeTabelCapLab(cap,label)
        block+='\\begin{tabular}{l'
        for j in self.tL:
            block+='|c|c'
        block+='|c'
        for l in self.lL[:len(self.lL)-1]:
            block+='|c'
        block+='}\n'
        block+='\\toprule\n'
        return block
    def writeBeginSpaceLengthTable(self,p,cap,label=''):
        block=''
        block=self.writeBeginTable()
        block+=self.writeTabelCapLab(cap,label)
        block+='\\begin{tabular}{l'
        for l in self.lL:
            block+='|c'
        block+='}\n'
        block+='\\toprule\n'
        return block
    def writeBeginPointLengthTable(self,pL,cap,label=''):
        block=''
        block=self.writeBeginTable()
        block+='\\caption{'+cap+'.}\n'
        if label!='':
            block+=label +'\n'
        block+='\\centering \n'
        block+='\\begin{tabular}{l'
        for p in pL:
            block+='|c'
        block+='}\n'
        block+='\\toprule\n'
        return block
    def writeBeginQuadValueTable(self,p,cap,label=''):
        block=''
        block=self.writeBeginTable()
        block+=self.writeTabelCapLab(cap,label)
        block+='\\begin{tabular}{l'
        for l  in self.lL:
            block+='|c|c'
        block+='}\n'
        block+='\\toprule\n'
        return block
    def writeCVRows(self,pairL,nL=[]):
        block=''
        if len(self.tL)==1:
            block +='$j='+str(self.tL[0])+'$'
        for l in self.lL:
            block+='& $ \\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        if len(pairL)>1:
            for index in range(len(pairL)):
                block+=nL[index]
                if len(self.tL)>1:
                    for l in self.lL:
                        block+='& '
                    block+='\\\\\n'
                    for j in self.tL:
                        block +='$j='+str(j)+'$'
                        for l in self.lL:
                            value =comp.computeCV(self.path,l,j,pairL[index][0],pairL[index][1])
                            block += " & $"+ str("%8.4f"%(value))+"$"
                    block +='\\\\\n'
                else:
                    for l in self.lL:
                        value =comp.computeCV(self.path,l,self.tL[0],pairL[index][0],pairL[index][1])
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    block +='\\\\\n'
            
        else:
            for j in self.tL:
                block +='$j='+str(j)+'$'
                for l in self.lL:
                    value =comp.computeCV(self.path,l,j,pairL[0][0],pairL[0][1])
                    block += " & $"+ str("%8.4f"%(value))+"$"
                    block +='\\\\\n'
            
        return block
    def writeActivationExtraErrorRow(self,pL):
        block=''
        extra = comp.getFullExtraActivation(self.lL[-1],self.tL[-1],self.path,pL[0])
        for l in self.lL:
            block+='& $ \\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        if len(pL)<=1:
            for j in self.tL:
                block +='$j='+str(j)+'$'
                for l in self.lL:
                    value=read.getActivationTime(self.path,l,j,pL[0])
                    error=abs(extra-value)
                    if extra!=0.0:
                        block += " & $"+ str("%8.4f"%(error))+"$"
                    else:
                        block +='& -'
                block +='\\\\\n'
        return block
    def writeCVDiffTab(self,fn,pairL,nL,distL,j):
        block=''
        block+=self.writeBeginVariableLength(len(pairL))
        if len(self.tL)==1:
            block +='$\CV(\V^{j,\\ell})-\CV(\V^{j,\\ell-1})$'
        for name in nL:
            block+='& '+name
        block+='\\\\\n'
        block+='\\midrule\n'  
        for l in range(self.lL[1],len(self.lL)):
            block +='$\\ell='+str(l)+'$'
            for index in range(len(pairL)):
                value_l = comp.computeCV(fn,l,j,pairL[index][0],pairL[index][1])
                value_lminusOne =comp.computeCV(fn,l-1,j,pairL[index][0],pairL[index][1])
                value=value_l-value_lminusOne
                block += " & $"+ str("%8.4f"%(value))+"$"
            block+='\\\\\n'
        block+=self.EndTabular()
        return block
    def writeMeanTactTabular(self,fn,l0path, varr=[]):
        block=''
        block+=self.writeBeginSpace()
        block+=self.writeMeanTact(fn,l0path,varr)
        block+=self.EndTabular()
        return block
    def writeMeanTact(self,eP,l0Path,varr=[]):
        block =''
        block += '$\\tact(v^{j,\ell})$ [ms]'
        for l in self.lL:
            block += '& $\\ell='+ str(l)+'$'
        block +='\\\\\n'
        block+='\\midrule\n'
        for j in self.tL:
            block+='$j='+str(j)+'$'
            for l in self.lL:
                path = eP+ 'vtu/AT_l'+str(l)+'j'+str(j)+'.vtu'
                tact=-10
                if l<len(varr) and j<len(varr[l]):
                    tact=varr[l][j]
                else:
                    tact=comp.computeMeanTact(path,l0Path)
                if tact>0.0:
                    block += " & $"+ str("%8.4f"%(tact))+"$"
                elif tact==-1:
                    block += " & \\nfa"
                else:
                    block += " & -"
                
            block+='\\\\\n'
        return block
    def writeRMSRefErrorTabular(self,L,J,eP,l0Path,refP):
        print('started RMSRefError with ref in ', refP)
        refPath=refP+'vtu/AT_l'+str(L)+'j'+str(J)+'.vtu'
        refPoints,atRef=read.readVTU(refPath)
        refDict={}
        for index in range(len(refPoints)):
            p=str(refPoints[index][0])+', '+str(refPoints[index][1])+', '+str(refPoints[index][2])
            refDict[p]=atRef[index]
        block=''
        block+=self.writeBeginSpace()
        block += '$  \\rmse(\V^{j,\\ell},\V^{'+str(J)+','+str(L)+'})$  [ms]'
        for l in self.lL:
            block += '& $\\ell='+ str(l)+'$'
        block +='\\\\\n'
        block+='\\midrule\n'
        for j in self.tL:
            block+='$j='+str(j)+'$'
            for l in self.lL:
                print(l,j)
                path = eP+ 'vtu/AT_l'+str(l)+'j'+str(j)+'.vtu'
                rmseRef=comp.RMSRefError(refDict,path,l0Path)
                if rmseRef>0.0:
                    block += " & $"+ str("%8.4f"%(rmseRef))+"$"
                elif rmseRef==-1:
                    block += " & \\nfa"
                else:
                    block += " & -"
            block+='\\\\\n'
        block+=self.EndTabular()
        return block
    def writeActivationRMSerrorTimeRow(self):
        errorArray=comp.getTimeRMSerrorArray(self.lL,self.tL[:-1],self.path)
        block=''
        for j in self.tL[:-1]:
            block+='& $ j='+str(j)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for l in self.lL:
            block +='$\\ell='+str(l)+'$'
            for j in self.tL[:-1]:
                value=errorArray[l][j]
                block += " & $"+ str("%8.4f"%(value))+"$"
            block +='\\\\\n'
        return block
    def writeActivationRMSerrorSpaceRow(self):
        errorArray=comp.getSpaceRMSerrorArray(self.lL[:-1],self.tL,self.path)
        block=''
        for l in self.lL[:-1]:
            block+='& $ \\ell='+str(l)+'$'
        block+='\\\\\n'
        block+='\\midrule\n'
        for j in self.tL:
            block +='$j='+str(j)+'$'
            for l in self.lL[:-1]:
                #value=comp.getRMSerror(self.lL[0],l,j,self.path)
                value=errorArray[j][l]
                block += " & $"+ str("%8.4f"%(value))+"$"
            block +='\\\\\n'
        return block
    def writeActivationRow(self,pL, withExtrapolates=False):
        block=''
        for l in self.lL:
            block+='& $ \\ell='+str(l)+'$'
        if withExtrapolates:
            block+='& $\\tact^{j,\\infty}$'
        block+='\\\\\n'
        block+='\\midrule\n'
        if len(pL)<=1:
            for j in self.tL:
                block +='$j='+str(j)+'$'
                for l in self.lL:
                    value=read.getActivationTime(self.path,l,j,pL[0])
                    block += " & $"+ str("%8.4f"%(value))+"$"
                if withExtrapolates:
                    extra= comp.getSpaceExtraActivation(self.lL[-1],j,self.path,pL[0])
                    block += " & $" +str("%8.4f"%(extra))+"$"
                block +='\\\\\n'
            #if withExtrapolates:
                #block += '$\\tact^{\\infty,\\ell}$'
                #for l in self.lL:
                    #extra=comp.getTimeExtraActivation(l,self.tL[-1],self.path,pL[0])
                    #block += " & $" +str("%8.4f"%(extra))+"$"
               # block +='& $'+str("%8.4f"%(comp.getFullExtraActivation(self.lL[-1],self.tL[-1],self.path,pL[0])))+'$'
        else:
            for x in pL:
                block +=str(x)
                for l in self.lL:
                    block+= '&'
                block +='\\\\\n'
                for j in self.tL:
                    block +='$j='+str(j)+'$'
                    for l in self.lL:
                        value=read.getActivationTime(self.path,l,j,x)
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    block +='\\\\\n'
                block+='\\midrule\n'
        return block
    def writeValueSchemeCompQuadTable(self,point,nL,withExtrapolates=False):
        block=''
        block +='$j$ '
        for l in self.lL:
            block +=' &\\multicolumn{2}{c}{$\\ell='+str(l) +'$}'
        block +='\\\\\n'
        
        for l in self.lL:
            for name in  nL:
                block+='& ' +name
        block+='\\\\\n'
        block+='\\midrule\n'
        for j in self.tL:
            block+='$'+str(j)+'$'
            for l in self.lL:
                for file in self.path:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(file,read.createFilename(l,j,self.m),point)
                    if len(t)==standardlength:
                        value =comp.L2(V,self.sdt,self.T,j,False)
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block += " & - "
            block +='\\\\\n'  
        if withExtrapolates:
            block+=self.writeExtrapolatesinTime(point)
            
        block+='\\bottomrule\n'
        block+=self.EndTabular()
        return block
    
    def writeExtrapolatesinTime(self,point):
        block='\\midrule\n'
        block += '$\\| \\V^{\\infty,\\ell}  \\|_\\LtwonormT$'
        for l in self.lL:
            for file in self.path:
                value=comp.L2(comp.getExtrapolateInTime(l,self.tL[-1],self.m,point,self.sdt,self.T,file),self.sdt,self.T)
                block += " & $"+ str("%8.4f"%(value))+'$'
        block+='\\\\\n'
        return block
    def writeValueSchemeCompTable(self,point,nL,levelList=[]):
        block=''
        n=len(levelList)
        if n<1:
            levelList=self.lL
        
        
        block +='\\\\\n'
        block +='\\hline \n'
        
        for l in levelList:
            if n>1:
                block += '$\ell='+str(l)+'$'
                for name in  nL:
                    block+='& ' 
                block +='\\\\\n'
            for j in self.tL:
                block+=str(j)
                for file in self.path:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(file,read.createFilename(l,j,self.m),point)
                    if len(t)==standardlength:
                        value =comp.L2(V,self.sdt,self.T,j,False)
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block += " & - "
                
                block +='\\\\\n'
            block +='\\hline \n'
                
        block+=self.EndTabular()
        return block
    def writeCaption(self,cap,label):
        block =''
        block+=self.writeTabelCapLab(cap,label)
        return block
    def writeBeginTime(self):
        block=''
        block+='\\begin{tabular}{l'
        for j in self.tL:
            block+='|c'
        block+='}\n'
        return block
    def writeBeginVariableLength(self,length):
        block=''
        block+='\\begin{tabular}{l'
        for i in range(length):
            block+='|c'
        block+='}\n'
        return block
    def writeBeginSpace(self,withExtrapolates=False):
        block=''
        block+='\\begin{tabular}{l'
        for l in self.lL:
            block+='|c'
        if withExtrapolates:
            block+='|c'
        block+='}\n'
        return block
    def writeExtraErrorVolumes(self,fL,nL,inInt):
        block = ''
        th=[135.0,150.0]
        length=len(fL)*2+2
        block+=self.BeginDifferenceTable(length,'c')
        block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{ $\\errorLV^{j,\ell}$}'+'& &\\multicolumn{'+str(len(fL))+'}{c}{ $\\errorRV^{j,\ell}$}'+'\\\\\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        block+='\cmidrule(l){'+str(len(fL)+4)+'-'+str(2*len(fL)+3)+'}\n'
        block+='\\multicolumn{2}{c}{}'
        for alg in nL:
            block+='&'+alg
        block +=' & '
        for alg in nL:
            block+='&'+alg
        block+='\\\\\n'
       #block+='\\addlinespace\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        block+='\cmidrule(l){'+str(len(fL)+4)+'-'+str(2*len(fL)+3)+'}\n'
        
        #Extrapolates
        LV_ref=[]
        RV_ref=[]
        t=[]
        if inInt:
            tact_ref=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,1,self.exP,th[0],'Volumes')
            (t,LV_ref)=comp.getVIntFullExtrapolate(self.lL[-1],self.tL[-1],1,1,self.exP,self.sdt,th[0],durEvalActi,tact_ref,'Volumes')
            tact_ref=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,2,self.exP,th[1],'Volumes')
            (t,RV_ref)=comp.getVIntFullExtrapolate(self.lL[-1],self.tL[-1],1,2,self.exP,self.sdt,th[1],durEvalActi,tact_ref,'Volumes')
            
        else:
            LV_ref=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,1,self.sdt,self.T,self.exP,'Volumes')
            RV_ref=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,2,self.sdt,self.T,self.exP,'Volumes')
        
        #plt.plot(t,RV_ref,label='ref',linewidth=2.5)
        normLV_ref=comp.L2(LV_ref,self.sdt,self.T,self.tL[-2],False)
        normRV_ref=comp.L2(RV_ref,self.sdt,self.T,self.tL[-2],False)
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                for alg in fL:
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m)+'Volumes',1)
                    if len(t)==standardlength:
                        value=0.0
                        if inInt:
                            (tact,id_tact)=comp.getTactPerCase(t,V,th[0],'Volumes',True)
                            id_dur=self.getTimeID(id_tact,j)
                            value=comp.L2DiffInterval(V[id_tact:id_dur],LV_ref,self.sdt,j,self.tL[-2])
                        else:
                            value=comp.L2DiffInterval(V,LV_ref,self.sdt,j,self.tL[-2])
                        value=value/normLV_ref
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='& '
                for alg in fL:
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m)+'Volumes',2)
                    if len(t)==standardlength :
                        value=0.0
                        if inInt:
                            (tact,id_tact)=comp.getTactPerCase(t,V,th[1],'Volumes',True)
                            id_dur=self.getTimeID(id_tact,j)
                            value=comp.L2DiffInterval(V[id_tact:id_dur],RV_ref,self.sdt,j,self.tL[-2])
                            #if l==4:
                                #for time in t:
                                    #time-=tact
                                #plt.plot(t[id_tact:id_dur],V[id_tact:id_dur],label='j='+str(j),linewidth=2.5)
                        else:
                            value=comp.L2DiffInterval(V,RV_ref,self.sdt,j,self.tL[-2])
                        value=value/normRV_ref
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
                block+='\cmidrule(l){'+str(len(fL)+4)+'-'+str(2*len(fL)+3)+'}\n'
        #plt.legend()
        #plt.show()
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
    
    def MechTimeStepTest(self,l,p,fL,nL,th, numberofAdaptive):
        L=4
        block = ''
        block+='\\toprule\n'
        block+='\\multicolumn{2}{c}{}'+ '&\\multicolumn{'+str(len(fL)-numberofAdaptive)+'}{c}{festes ${\\vartriangle}t^{\\mathrm{m}}$} '+'&\\multicolumn{'+str(numberofAdaptive)+'}{c}{adaptives Verfahren }'
        block+='\\\\\n'
        #block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)-numberofAdaptive+2)+'}\n'
        block+='\cmidrule(l){'+str(len(fL)-numberofAdaptive+3)+'-'+str(len(fL)+2)+'}\n'

        block+='\\multicolumn{2}{c}{$\\ell='+str(l)+'$}'
        for name in nL:
            block+='& '+name
        block+='\\\\\n'
        block+='\cmidrule(r){1-2}\n'
        for i in range(len(nL)-1):
            block+='\cmidrule(lr){'+str(3+i)+'-'+str(3+i)+'}\n'
        block+='\cmidrule(l){'+str(len(fL)+2)+'-'+str(len(fL)+2)+'}\n'
        
        #Extrapolierte tact:
        j_refT=3
        tactRef=comp.getTextFullExtrapolate(L,j_refT,1,p,self.exP,th)
        
        #Extrapolierte bzgl. Vact:
        j_refV=4
        tact_ref=comp.getTextFullExtrapolate(L,j_refV,1,p,self.exP,th,'')
        (t_f,VInt_full)=comp.getVIntFullExtrapolate(L,j_refV,1,p,self.exP,self.sdt,th,durEvalActi,tact_ref,'')
        norm_full=comp.L2(VInt_full,self.sdt,self.T,j_refV-1,False)
        
        #Exptapolierte CaInt:
        j_refCaInt=4
        thCa=0.0002
        tact_refCa=comp.getTextFullExtrapolate(L,j_refCaInt,1,p,self.exP,thCa,'Ca')
        (t_f,CaInt_full)=comp.getVIntFullExtrapolate(L,j_refCaInt,1,p,self.exP,self.sdt,thCa,durEvalActi,tact_refCa,'Ca')
        normCa_full=comp.L2(CaInt_full,self.sdt,self.T,j_refCaInt-1,False)
        
        #Extraopolierte GammaInt:
        j_refGInt=3
        thG=-0.0005
        tact_refG=comp.getTextFullExtrapolate(L,j_refGInt,1,p,self.exP,thG,'Gamma')
        (t_f,GInt_full)=comp.getVIntFullExtrapolate(L,j_refGInt,1,p,self.exP,self.sdt,thG,durEvalActi,tact_ref,'Gamma')
        normG_full=comp.L2(GInt_full,self.sdt,self.T,j_refGInt-1,False)
        #Extrapolierte Volumes:
        j_refVol=3
        LV_ref=comp.getFullExtrapolate(L,j_refVol,self.m,1,self.sdt,self.T,self.exP,'Volumes')
        RV_ref=comp.getFullExtrapolate(L,j_refVol,self.m,2,self.sdt,self.T,self.exP,'Volumes')
        t_ref=np.linspace(0.0, self.T, num=len(LV_ref))

        normLV_ref=comp.L2(LV_ref,self.sdt,self.T,j_refVol-1,False)
        normRV_ref=comp.L2(RV_ref,self.sdt,self.T,j_refVol-1,False)
        
        
        for j in self.tL:
            standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
            block+='$j='+str(j)+'$'
            
            #duration:
            proc=512
            block +='& Rechenzeit '
            for alg in fL:
                (procAlg,dur)=read.getProcsAndDur(alg,l,j,self.m)
                if dur==0:
                    block+='& - '
                else:
                    dur=dur[:-3]
                    dur=dur.split(':')
                    #print(dur)
                    block+='&$ '+str(dur[0])+'$'
                    for k in range(1,len(dur)):
                        block+=':$'+str(dur[k])+'$'
                    #block+='&$'+str(procAlg)+'$'
            block+='\\\\[2pt]\n'       
                    
            #tact:
            block+='& $\\errortact^{'+str(j)+','+str(l)+','+str(p)+'}$'
            for alg in fL:
                (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                if len(t)==standardlength:
                    t_act=comp.getActivaitionTimeFromV(t,V,th)
                    value=abs(t_act-tactRef)/tactRef
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\[2pt]\n'
            
            #IntV:
            block+='& $\\errorVact^{'+str(j)+','+str(l)+','+str(p)+'}$'
            for alg in fL:
                (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                if len(t)==standardlength:
                    (tact,id_tact)=comp.getTactPerCase(t,V,th,'',True)
                    id_dur=self.getTimeID(id_tact,j)
                    value=comp.L2DiffInterval(V[id_tact:id_dur],VInt_full,self.sdt,j,j_refV-1,p)
                    value=value/norm_full
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\[2pt]\n'
            
            
            #Ca IntV:
            block+='&$\\errorCaAct^{'+str(j)+','+str(l)+','+str(p)+'}$'
            for alg in fL:
                (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m)+'Ca',p)
                if len(t)==standardlength:
                    (tact,id_tact)=comp.getTactPerCase(t,V,thCa,'Ca',True)
                    id_dur=self.getTimeID(id_tact,j)
                    value=comp.L2DiffInterval(V[id_tact:id_dur],CaInt_full,self.sdt,j,j_refCaInt-1,p)
                    value=value/normCa_full
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
            block+='\\\\[2pt]\n'
            
            ##Gamma IntV:
            #block+='&$\\errorGAct^{'+str(j)+','+str(l)+','+str(p)+'}$'
            #for alg in fL:
                #(t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m)+'Gamma',p)
                #if len(t)==standardlength-1:
                    #(tact,id_tact)=comp.getTactPerCase(t,V,thG,'Gamma',True)
                    #id_dur=self.getTimeID(id_tact,j)
                    #value=comp.L2DiffInterval(V[id_tact:id_dur],GInt_full,self.sdt,j,j_refGInt-1,p)
                    #value=value/normG_full
                    #block += " & $"+ str("%8.4f"%(value))+"$"
                #else:
                    #block+=" & - "
            #block+='\\\\[2pt]\n'
            
            
            #LV:
            block+='& $\\errorLV^{'+str(j)+','+str(l)+'}$'
            for alg in fL:
                
                (t,LV)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m)+'Volumes',1)
                if len(LV)!=0:  
                    value=0.0
                    if 'Mech' in alg:
                        sdtnew=(int(1000.0*1000.0*self.T)//((len(LV)-1)))/(1000*1000)
                        refJ=int(math.log(sdtnew/(self.sdt*2**(-(j_refVol-1))) ,2))
                        value=comp.L2DiffInterval(LV,LV_ref,self.sdt,0,refJ)
                    else:
                        value=comp.L2DiffWithVaryingTimeStepSize(LV,LV_ref,t,t_ref)
                    
                    value=value/normLV_ref
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
                    
            block+='\\\\[2pt]\n'
            
            #RV:
            block+='& $\\errorRV^{'+str(j)+','+str(l)+'}$'
            for alg in fL:
                (t,RV)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m)+'Volumes',2)
                if len(RV)!=0:
                    value=0.0
                    if 'Mech' in alg:
                        sdtnew=(int(1000.0*1000.0*self.T)//((len(RV)-1)))/(1000*1000)
                        refJ=int(math.log(sdtnew/(self.sdt*2**(-(j_refVol-1))) ,2))
                        value=comp.L2DiffInterval(RV,RV_ref,self.sdt,0,refJ)
                        
                    else:
                       value=comp.L2DiffWithVaryingTimeStepSize(RV,RV_ref,t,t_ref) 
                    
                    value=value/normRV_ref
                    block += " & $"+ str("%8.4f"%(value))+"$"
                else:
                    block+=" & - "
                
            block+='\\\\[2pt]\n'
            
            if j!=self.tL[-1]:
                block+='\cmidrule(r){1-2}\n'
                for i in range(len(nL)-1):
                    block+='\cmidrule(lr){'+str(3+i)+'-'+str(3+i)+'}\n'
                block+='\cmidrule(l){'+str(len(fL)+2)+'-'+str(len(fL)+2)+'}\n'
                        
                
            
            
        
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
        
    def writeExtraErrorTab(self,fL,nL,p,case):
        block = ''
        block+='\\toprule\n'
        if case=='Ca':
            block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{$\\errorCa^{j,\ell,'+str(p)+'}$}'
        elif case=='Gamma':
            block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{$\\errorG^{j,\ell,'+str(p)+'}$}'
        else:
            print('case not defined!')
        
        block+='\\\\\n'
        block+='\\addlinespace\n'
        block+='\cmidrule(l){3-'+str(len(fL)+2)+'}\n'
        block+='\\multicolumn{2}{c}{}'
        for name in nL:
            block+='& '+name
        block+='\\\\\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        
        V_ref=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,p,self.sdt,self.T,self.exP,case)
        norm_ref=comp.L2(V_ref,self.sdt,self.T,self.tL[-2],False)
        
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m)+case,p)
                    #print(l,j,len(V),len(V_ref))
                    if len(t)==standardlength or len(t)==standardlength-1:
                        value=comp.L2DiffInterval(V,V_ref,self.sdt,j,self.tL[-2])
                        value=value/norm_ref
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
                    
        
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
    def L2ErrorTab(self,p,J,L,fL,nL):
        block = ''
        block+='\\toprule\n'
        block+='\\multicolumn{2}{c}{}'
        for name in nL:
            block+='& '+name
        block+='\\\\\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        
        (t_ref,Vref)=read.getDataFromTXT(self.path,read.createFilename(L,J,1),p)
        norm_ref=comp.L2(Vref,self.sdt,self.T,J-1,False)
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,1),p)
                    if len(t)==standardlength:
                        value=comp.L2DiffInterval(V,Vref,self.sdt,j,J)
                        value=value/norm_ref
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'        
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
        
    def writeExtraErrorLtwoNormAct(self,fL,nL,p,th,case=''):
        block = ''
        block+='\\toprule\n'
        if case=='Ca':
            block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{$\\errorCaAct^{j,\ell,'+str(p)+'}$}'
        elif case=='Gamma':
            block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{$\\errorGAct^{j,\ell,'+str(p)+'}$}'
        else:
            block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{$\\errorVact^{j,\ell,'+str(p)+'}$}'
        block+='\\\\\n'
        block+='\\addlinespace\n'
        block+='\cmidrule(l){3-'+str(len(fL)+2)+'}\n'
        block+='\\multicolumn{2}{c}{}'
        for name in nL:
            block+='& '+name
        block+='\\\\\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        tact_ref=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,p,self.exP,th,case)
        (t_f,VInt_full)=comp.getVIntFullExtrapolate(self.lL[-1],self.tL[-1],1,p,self.exP,self.sdt,th,durEvalActi,tact_ref,case)
        norm_full=comp.L2(VInt_full,self.sdt,self.T,self.tL[-2],False)
        
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m)+case,p)
                    if len(t)==standardlength or len(t)==standardlength-1:
                        (tact,id_tact)=comp.getTactPerCase(t,V,th,case,True)
                        id_dur=self.getTimeID(id_tact,j)
                        
                            #plt.plot(t_f,VInt_full,label='Extra')
                            #plt.plot(t[id_tact:id_dur],V[id_tact:id_dur],label=case+'l'+str(l)+'j'+str(j))
                            #plt.legend()
                            #plt.show()
                        value=comp.L2DiffInterval(V[id_tact:id_dur],VInt_full,self.sdt,j,self.tL[-2],p)
                        value=value/norm_full
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
    def writeLtwoNormAct(self,fL,nL,p,th):
        block = ''
        block+='\\toprule\n'
        block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\V^{j,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')}_\\LtwonormAct$}'
        block+='\\\\\n'
        block+='\\addlinespace\n'
        block+='\cmidrule(l){3-'+str(len(fL)+2)+'}\n'
        block+='\\multicolumn{2}{c}{}'
        for name in nL:
            block+='& '+name
        block+='\\\\\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        (tact,id_tact)=comp.getActivaitionTimeFromVWithID(t,V,th)
                        id_dur=self.getTimeID(id_tact,j)
                        value=comp.L2(V[id_tact:id_dur],self.sdt,self.T,j,False)
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
    def writeLtwoNormsAndDurationTab(self,fL,nL,p,withDuration):
        block = ''
        block+='\\toprule\n'
        block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{$\\norm{\\V^{j,\\ell}(\\cdot,'+self.getPointNamebyNumber(p,True)+')}_\\LtwonormT$}'
        if withDuration:
            block+='&\\multicolumn{'+str(len(fL))+'}{c}{Rechenzeit}&'
        block+='\\\\\n'
        block+='\\addlinespace\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        if withDuration:
            block+='\cmidrule(lr){'+str(len(fL)+3)+'-'+str(2*len(fL)+2)+'}\n'
        
        block+='\\multicolumn{2}{c}{}'
        for name in nL:
            block+='& '+name
        if withDuration:
            for name in nL:
                block+='& '+name
            block+='&$\\#$ Prozesse'
        block+='\\\\\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        if withDuration:
            block+='\cmidrule(lr){'+str(len(fL)+3)+'-'+str(2*len(fL)+2)+'}\n'
            block+='\cmidrule(l){'+str(2*len(fL)+2+1)+'-'+str(2*len(fL)+2+1)+'}\n'
        
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                proc=0
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        value=comp.L2(V,self.sdt,self.T,j,False)
                        #value=comp.L2DiffPot(V,Ref,self.sdt*2**(-j),self.T)
                        #if divideNorm:
                            #value=value/normRef
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    #elif len(t)>standardlength:
                        #value=comp.L2DiffPot(V[:standardlength],Ref[:standardlength],self.sdt*2**(-j),self.T)
                       # if divideNorm:
                            #value=value/normRef
                        #block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                if withDuration:
                    for alg in fL:
                        (procAlg,dur)=read.getProcsAndDur(alg,l,j,self.m)
                        if proc!=0 and proc!=procAlg:
                            print('Achtung, die Rechnungen sind auf unterschiedlichen Prozessoren gemacht!',proc,alg,procAlg,'j',j,'l',l)
                        proc=procAlg
                        if dur==0:
                            block+='& - '
                        else:
                            dur=dur[:-3]
                            block+='&$'+str(dur)+'$'
                    block+='&$'+str(proc)+'$'
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
                if withDuration:
                    block+='\cmidrule(lr){'+str(len(fL)+3)+'-'+str(2*len(fL)+2)+'}\n'
                    block+='\cmidrule(l){'+str(2*len(fL)+2+1)+'-'+str(2*len(fL)+2+1)+'}\n'
        
        
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
    
    def writeAPDTimesTab(self,fL,nL,threshold,p):
        block = ''
        block+='\\toprule\n'
        block+='\\multicolumn{2}{c}{}'
        for alg in nL:
            block+='&'+alg
        block+='\\\\\n'
       #block+='\\addlinespace\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(l){3-'+str(len(fL)+2)+'}\n'

        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        t_dur=comp.getAPDFromV(t,V,threshold)
                        block += " & $"+ str("%8.4f"%(t_dur))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(l){3-'+str(len(fL)+2)+'}\n'
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
    def writeActivationTimesTab(self,fL,nL,threshold,p):
        block = ''
        block+='\\toprule\n'
        block+='\\multicolumn{2}{c}{}'
        for alg in nL:
            block+='&'+alg
        block+='\\\\\n'
       #block+='\\addlinespace\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(l){3-'+str(len(fL)+2)+'}\n'

        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    #print(alg,l,j)
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        t_act=comp.getActivaitionTimeFromV(t,V,threshold)
                        block += " & $"+ str("%8.4f"%(t_act))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(l){3-'+str(len(fL)+2)+'}\n'
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
    def writeSchwingerPerPoint(self,fL,nL,p,th):
        block = ''
        length=2*len(fL)+3
        block+=self.BeginDifferenceTable(length,'c')
        block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{$\\min(\\V^{j,\ell,'+str(p)+'}'+')$}'+'& &\\multicolumn{'+str(len(fL))+'}{c}{$\\max(\\V^{j,\ell,'+str(p)+'}'+')$}'
        block+='\\\\\n'
        block+='\\addlinespace\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        block+='\cmidrule(l){'+str(len(fL)+4)+'-'+str(2*len(fL)+3)+'}\n'
        block+='\\multicolumn{2}{c}{}'
        for name in nL:
            block+='& '+name
        block+='& '
        for name in nL:
            block+='& '+name
        block+='\\\\\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        block+='\cmidrule(lr){'+str(len(fL)+4)+'-'+str(2*len(fL)+3)+'}\n'
        
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        (tact,id_tact)=comp.getActivaitionTimeFromVWithID(t,V,th)
                        value=min(V[:id_tact])
                        block += " & $"+ str("%8.2f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='& '
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        (tact,id_tact)=comp.getActivaitionTimeFromVWithID(t,V,th)
                        value=max(V[id_tact:])
                        block += " & $"+ str("%8.2f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
                block+='\cmidrule(lr){'+str(len(fL)+4)+'-'+str(2*len(fL)+3)+'}\n'
         
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
    def writeUnterschwingerPerPoint(self,fL,nL,p,th):
    
        block = ''
        length=len(fL)+2
        block+=self.BeginDifferenceTable(length,'c')
        block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{$\\min(\\V^{j,\ell,'+str(p)+'}'+')$}'
        block+='\\\\\n'
        block+='\\addlinespace\n'
        block+='\cmidrule(l){3-'+str(len(fL)+2)+'}\n'
        block+='\\multicolumn{2}{c}{}'
        for name in nL:
            block+='& '+name
        block+='\\\\\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        (tact,id_tact)=comp.getActivaitionTimeFromVWithID(t,V,th)
                        value=min(V[:id_tact])
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
         
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
    def writeExtraErrorandIntTab(self,fL1,fL2,nL1,nL2,p,threshold,case):
        block = ''
        length=len(fL1)+len(fL2)+2
        block+=self.BeginDifferenceTable(length,'c')
        
        if case=='Ca':
            block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL1))+'}{c}{ $\\errorCa^{j,\ell,'+str(p)+'}$}'+'& &\\multicolumn{'+str(len(fL2))+'}{c}{ $\\errorCaAct^{j,\ell,'+str(p)+'}$}'+'\\\\\n'
        else:
            print('case',case,'not defined')
        
        block+='\cmidrule(lr){3-'+str(len(fL1)+2)+'}\n'
        block+='\cmidrule(l){'+str(len(fL1)+4)+'-'+str(len(fL1)+len(fL2)+3)+'}\n'
        block+='\\multicolumn{2}{c}{}'
        for alg in nL1:
            block+='&'+alg
        block +=' & '
        for alg in nL2:
            block+='&'+alg
        block+='\\\\\n'
        
        
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL1)+2)+'}\n'
        block+='\cmidrule(l){'+str(len(fL1)+4)+'-'+str(len(fL1)+len(fL2)+3)+'}\n'
        
        
        V_ref=comp.getFullExtrapolate(self.lL[-1],self.tL[-1],self.m,p,self.sdt,self.T,self.exP,case)
        norm_ref=comp.L2(V_ref,self.sdt,self.T,self.tL[-2],False)
        
        tact_ref=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,p,self.exP,threshold,case)
        (t_f,VInt_full)=comp.getVIntFullExtrapolate(self.lL[-1],self.tL[-1],1,p,self.exP,self.sdt,threshold,durEvalActi,tact_ref,case)
        norm_full=comp.L2(VInt_full,self.sdt,self.T,self.tL[-2],False)
        
        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                for alg in fL1:
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m)+case,p)
                    if len(t)==standardlength:
                        value=comp.L2DiffInterval(V,V_ref,self.sdt,j,self.tL[-2])
                        value=value/norm_ref
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+=' & '
                for alg in fL2:
                    
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m)+case,p)
                    if len(t)==standardlength or len(t)==standardlength-1:
                        (tact,id_tact)=comp.getTactPerCase(t,V,threshold,case,True)
                        id_dur=self.getTimeID(id_tact,j)
                        value=comp.L2DiffInterval(V[id_tact:id_dur],VInt_full,self.sdt,j,self.tL[-2],p)
                        value=value/norm_full
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL1)+2)+'}\n'
                block+='\cmidrule(l){'+str(len(fL1)+4)+'-'+str(len(fL1)+len(fL2)+3)+'}\n'
        
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
        
    def writeTactErrorPerPoint(self,fL,nL,threshold,p):
        tactRef=comp.getTextFullExtrapolate(self.lL[-1],self.tL[-1],1,p,self.exP,threshold)
        #if p==8:
            #print(tactRef)
        block = ''
        length=len(fL)*2+2
        block+=self.BeginDifferenceTable(length,'c')
        block+='\\multicolumn{2}{c}{}'+'&\\multicolumn{'+str(len(fL))+'}{c}{ $\\tact^{j,\ell,'+str(p)+'}$}'+'& &\\multicolumn{'+str(len(fL))+'}{c}{ $\\errortact^{j,\ell,'+str(p)+'}$}'+'\\\\\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        block+='\cmidrule(l){'+str(len(fL)+4)+'-'+str(2*len(fL)+3)+'}\n'
        block+='\\multicolumn{2}{c}{}'
        for alg in nL:
            block+='&'+alg
        block +=' & '
        for alg in nL:
            block+='&'+alg
        block+='\\\\\n'
       #block+='\\addlinespace\n'
        block+='\cmidrule(r){1-2}\n'
        block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
        block+='\cmidrule(l){'+str(len(fL)+4)+'-'+str(2*len(fL)+3)+'}\n'

        for l in self.lL:
            block+='$\\ell='+str(l)+'$'
            for j in self.tL:
                block +='& $j='+str(j)+'$'
                for alg in fL:
                    #print(alg,l,j)
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        t_act=comp.getActivaitionTimeFromV(t,V,threshold)
                        t_act=1000.0*t_act
                        block += " & $"+ str("%8.3f"%(t_act))+"$"
                    else:
                        block+=" & - "
                block+=' & '
                for alg in fL:
                    #print(alg,l,j)
                    standardlength=int(round(self.T/(self.sdt*2**(-j))))+1
                    (t,V)=read.getDataFromTXT(alg,read.createFilename(l,j,self.m),p)
                    if len(t)==standardlength:
                        t_act=comp.getActivaitionTimeFromV(t,V,threshold)
                        #if p==8 and l==5 and j==3  and alg=='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/GS0004/theta1/':
                           # print('table',1000.0*t_act,tactRef,abs(t_act-tactRef)/tactRef)
                        value=abs(t_act-tactRef)/tactRef
                        block += " & $"+ str("%8.4f"%(value))+"$"
                    else:
                        block+=" & - "
                block+='\\\\\n'
            if l<self.lL[-1]:
                block+='\cmidrule(r){1-2}\n'
                block+='\cmidrule(lr){3-'+str(len(fL)+2)+'}\n'
                block+='\cmidrule(l){'+str(len(fL)+4)+'-'+str(2*len(fL)+3)+'}\n'
        block+='\\bottomrule\n'
        block +=self.EndTabular()
        return block
    def writeNiederBMActivationTimeConvergencePointwise(self,p,pname,timedisc,spacedisc):
        block =''
        block+='\\begin{tabular}{ccr}\n'
        block +='${\\vartriangle}t$ & ${\\vartriangle}x$ & $t_\\text{act}(\\cdot,\\bf {P'+str(pname)+'})$\\\\\n'
        for j in self.tL:
            block +='\\hline    \n'
            block+= '$'+str(timedisc[j])+ '$'
            for l in self. lL:
                block +='& $'+str(spacedisc[l])+ '$'
                activationTime=read.getActivationTimeofPointFromData(self.path, p,l,j)
                block += '& $'+str("%8.4f"%(activationTime))+'$'
                block+='\\\\\n'
        block +=self.EndTabular()
        return block
                
                
    
    
    
class TableWriter:
    def __init__(self, lList, tList,start_dt,endTime,substep,PathList,evaluationPoints,exPath=''):
        self.lL = lList
        self.tL = tList
        self.m = substep
        self.sdt = start_dt
        self.T =endTime
        self.tab = Table(lList, tList,start_dt,endTime,substep,PathList[0],PathList[0])
        self.evalP=evaluationPoints
        self.fL=PathList
        
    def AllExtrapolationTables(self):
        print("Write all extrapolation tables")
        for file in self.fL:
            self.tab.setFilePath(file)
            self.tab.setExPath(file)
            for p in self.evalP:
                self.writeValueTable(p)
                self.writeExtraSpace(p)
                self.writeExtraTime(p)
                self.writeFullExtrapolation(p)
        
    def writeValueTable(self,p):
        filename=self.tab.getFolderPath()+'tex/' +'ValueTab'+str(self.tab.timename(0))+'m'+str(self.m)
        filename+=self.tab.addEvaluationPointName(p)
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginValueLongtable(p))
            output_file.write(self.tab.writeValueColums(p))
            output_file.write(self.tab.writeEndLongTable())
    def writeExtraSpace(self,p):
        filename = self.tab.getFolderPath()+'tex/' +'ExtrapolationSpace'+str(self.tab.timename(0))+'m'+str(self.m)
        filename+=self.tab.addEvaluationPointName(p)
        caption='\\caption{Normalized differences to the space extrapolates $\\frac{\\| \\V^{j,\\ell}   -  \\V^{\\infty,\\ell}  \\|_\\LtwonormT}{\\| \\V^{\\infty,\\ell}  \\|_\\LtwonormT}$ for different time and space levels'
        caption +=self.tab.addEvaluationToCaption(p)
        with open(filename +'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginExtrapolationLongtable(caption,p))
            output_file.write(self.tab.writeExtraSpaceColums(p))
            output_file.write(self.tab.writeEndLongTable())
    def writeExtraTime(self,p):
        filename =self.tab.getFolderPath()+'tex/' +'ExtrapolationTime'+str(self.tab.timename(0))+'m'+str(self.m)
        filename+=self.tab.addEvaluationPointName(p)
        caption='\\caption{Normalized differences to the time extrapolates $\\frac{\\| \\V^{j,\\ell}   -  \\V^{j,\\infty}  \\|_\\LtwonormT}{\\| \\V^{j,\\infty}  \\|_\\LtwonormT}$ for different time and space levels'
        caption +=self.tab.addEvaluationToCaption(p)
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginExtrapolationLongtable(caption,p))
            output_file.write(self.tab.writeExtraTimeColums(p))
            output_file.write(self.tab.writeEndLongTable())
    def writeFullExtrapolation(self,p):
        filename = self.tab.getFolderPath()+'tex/' +'L2FullExtrapolation'+str(self.tab.timename(0))+'m'+str(self.m)
        filename+=self.tab.addEvaluationPointName(p)
        caption ='\\caption{Normalized differences to the space and time extrapolates $\\frac{\\| \\V^{j,\\ell} -  \\V^{\\infty,\\infty}  \\|_\\LtwonormT}{\\| \\V^{\\infty,\\infty}  \\|_\\LtwonormT}$ for different time and space levels'
        caption +=self.tab.addEvaluationToCaption(p)
        with open(filename +'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginExtrapolationLongtable(caption,p))
            output_file.write(self.tab.writeFullExtrapolationColums(p))
            output_file.write(self.tab.writeEndLongTable())
            
    def writeMinMaxTables(self,fP):
        self.tab.setFilePath(fP)
        with open(self.tab.getFolderPath()+'tex/' +'MinMaxTable'+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginMinMaxLongtable())
            for l in range(self.lL[0],len(self.lL)+self.lL[0]):
                for j in range(self.tL[0],len(self.tL)+ self.tL[0]):
                    fn = read.createFilename(l,j,self.m)
                    print(fn)
                    (min,max)=read.getMinMaxFromLog(fP,fn)
                    output_file.write(self.tab.writeMinMaxRows(l,j,min,max))
                output_file.write('\\hline\n')
            output_file.write(tab.writeEndMinMaxTable())
    def writeMeshInfoTable(self,path,savepath='',name='MeshInfo',language='e'):
        self.tab.setFilePath(path)
        if language=='e':
            line='min ${\\vartriangle}x$ (mm)& max ${\\vartriangle}x$ (mm) &$\\#$ cells & $\\#$ vertices'
        elif language=='d':
            line='min ${\\vartriangle}x$ (mm)& max ${\\vartriangle}x$ (mm) &$\\#$ Zellen & $\\#$ Eckpunkte '
        else:
            print('language: '+str(language)+' is not defined!')
        if savepath=='':
            with open(self.tab.getFolderPath()+'tex/' +name+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginTableSpace(line))
                output_file.write(self.tab.writeMeshInfoRows())
                output_file.write(self.tab.EndTabular()) 
        else:
             with open(savepath +name+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginTableSpace(line))
                output_file.write(self.tab.writeMeshInfoRows())
                output_file.write(self.tab.EndTabular()) 
                
    def writeMeshInfoTableCoupled(self,path,savepath,name='MeshInfo',language='e'):
        self.tab.setFilePath(path)
        if language=='e':
            line='min ${\\vartriangle}x$ (mm)& max ${\\vartriangle}x$ (mm) &$\\#$ cells & $\\#$ vertices'
        elif language=='d':
            line='min ${\\vartriangle}x$ (mm)& max ${\\vartriangle}x$ (mm) &$\\#$ Zellen & $\\#$ Eckpunkte '
        else:
            print('language: '+str(language)+' is not defined!')
        with open(savepath +name+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginTableSpace(line,True))
                output_file.write(self.tab.writeMeshInfoRowsCoupled())
                output_file.write(self.tab.EndTabular())
    
    def writeDurationTable(self,fP1,fP2):
        with open(tab.getFolderPath()+'tex/' +'Duration'+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginDurationLongtable())
            output_file.write('Godunov& & & & & \\\\\n')
            self.tab.setFilePath(fP1)
            output_file.write(self.tab.writeDurationRows())
            output_file.write('\\hline\n')
            output_file.write('Linear implicit& & & & & \\\\\n')
            self.tab.setFilePath(fP2)
            output_file.write(self.tab.writeDurationRows())
            output_file.write('\\hline\n')
            output_file.write(tab.writeEndLongTable())

       
    def LatexTables(self,fP,allEvalPoints):
        self.tab.setFilePath(fP)
        self.WriteLatexSpace(allEvalPoints)
        self.WriteLatexTime(allEvalPoints)
    
    def WriteLatexSpace(self,tab,AllEvalPoints):
        print("Write Space Table")
        with open(self.tab.getFolderPath()+'tex/' +'SpaceTableSdt'+str(self.tab.timename(0))+'m'+str(self.m)+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginSpaceLongtable())
            for p in self.evalP:
                (time,V) = read.getDataFromTXT(self.tab.getFolderPath(),read.createFilename(self.lL[-1],self.tL[-1],self.m),p)
                output_file.write(self.tab.writeSpaceData(V,p,AllEvalPoints))
            output_file.write(self.tab.writeEndLongTable())
            
    def WriteLatexTime(self):
        print("Write Time Table")
        with open(self.tab.getFolderPath()+'tex/' +'TimeTableSdt'+str(self.tab.timename(0))+'m'+str(self.m)+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTimeLongtable())
            for p in self.evalP:
                (time,V) = read.getDataFromTXT(self.tab.getFolderPath(),read.createFilename(self.lL[-1],len(self.tL)-1,self.m),p)
                output_file.write(self.tab.writeTimeData(V,p,AllEvalPoints))
            output_file.write(self.tab.writeEndLongTable())
            
    def SpaceDiffTable(self, nL):
        for index in range(len(nL)):
            tab=Table(self.lL,self.tL,self.sdt,self.T,self.m,self.fL[index])
            for p in self.evalP:
                filename=self.fL[index]+'tex/'+nL[index]+'DiffSpaceTabP'+str(p)
                caption='\\'+nL[index]+'SVI~space convergence:\\\\$\\|\\V^{j,\\ell}(\\cdot, \\textbf{P'+str(p)+'})-\\V^{j,\\ell+1}(\\cdot, \\textbf{P'+str(p)+'})\\|_\\LtwonormT$'
                label='\\label{tab:'+nL[index]+'SpaceDiffP'+str(p)+'}'
                with open(filename+'.tex', 'w') as output_file:
                    output_file.write(tab.writeBeginTimeLengthTable(p,caption,label))
                    output_file.write(tab.writeSpaceDiff(p,caption,label))
                    output_file.write(tab.writeEndTable())
        
    def TimeDiffTable(self, nL):
        for index in range(len(nL)):
            tab=Table(self.lL,self.tL,self.sdt,self.T,self.m,self.fL[index])
            for p in self.evalP:
                filename=self.fL[index]+'tex/'+nL[index]+'DiffTimeTabP'+str(p)
                caption='\\'+nL[index]+'~time convergence:\\\\$\\|\\V^{j,\\ell}(\\cdot, \\textbf{P'+str(p)+'})-\\V^{j+1,\\ell}(\\cdot, \\textbf{P'+str(p)+'})\\|_\\LtwonormT$'
                label='\\label{tab:'+nL[index]+'TimeDiffP'+str(p)+'}'
                with open(filename+'.tex', 'w') as output_file:
                    output_file.write(tab.writeBeginSpaceLengthTable(p,caption,label))
                    output_file.write(tab.writeTimeDiff(p,caption,label))
                    output_file.write(tab.writeEndTable())
        
    def DiagDiffTable(self, nL):
        for index in range(len(nL)):
            tab=Table(self.lL,self.tL,self.sdt,self.T,self.m,self.fL[index])
            filename=self.fL[index]+'tex/'+nL[index]+'DiffDiagAllPTab'
            with open(filename+'.tex', 'w') as output_file:
                caption='\\'+nL[index]+'SVI~ space-time convergence: $\\|\\V^{k,k}(\\cdot, p)-\\V^{k+1,k+1}(\\cdot, p)\\|_\\LtwonormT$'
                label='\\label{tab:'+nL[index]+'DiagDiffAllP}'
                output_file.write(tab.writeBeginPointLengthTable(self.evalP,caption,label))
                output_file.write(tab.writeDiagDiff(caption,label,self.evalP))
                output_file.write(tab.writeEndTable())
                
    def SpaceTimeTogether(self,nL,isSplitting=False):
        for index in range(len(nL)):
            tab=Table(self.lL,self.tL,self.sdt,self.T,self.m,self.fL[index])
            for p in self.evalP:
                filename=self.fL[index]+'tex/'+nL[index]+'DiffSpaceAndTimeTabP'+str(p)
                if isSplitting:
                    caption='\\'+nL[index]+'~space and time  convergence'
                else:
                    caption='\\'+nL[index]+'SVI~space and time  convergence'
                label='\\label{tab:'+nL[index]+'SpaceATimeDiffP'+str(p)+'}'
                with open(filename+'.tex', 'w') as output_file:
                    output_file.write(tab.writeBeginSpaceAndTimeLengthTable(p,caption,label))
                    if isSplitting:
                        output_file.write(tab.writeSpaceAndTimeDiff(p,0))
                    else:
                        output_file.write(tab.writeSpaceAndTimeDiff(p))
                    output_file.write(tab.writeEndTable())
    def SpaceAndTimeDiff(self,j,l):
        cap = 'Convergence of $\\V^{j,\ell}$ in $\\|\\cdot\\|_\\LtwonormT$ at ' 
        for p in self.evalP[:-2]:
            cap += self.tab.getPointNamebyNumber(p)+ ', '
        cap+=self.tab.getPointNamebyNumber(self.evalP[-2]) +'~and '+ self.tab.getPointNamebyNumber(self.evalP[-1]) +'~for the \\SISVI~scheme'
        filename =self.fL[0]+'tex/SpaceTimeSIP'
        label='tab:SpaceTimeSIP'
        for p in self.evalP:
            label+=str(p)
            filename+=str(p)
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable())
            output_file.write(self.tab.writeCaption(cap,label))
            output_file.write('\\begin{tabular}{cc}\n')
            output_file.write(self.tab.writeBeginSpace())
            output_file.write(self.tab.writeSpaceDiffPointRows(j,self.evalP))
            output_file.write(self.tab.EndTabular())
            output_file.write('&\n')#output_file.write('\\hfil\n')
            output_file.write(self.tab.writeBeginTime())
            output_file.write(self.tab.writeTimeDiffPointRows(l,self.evalP))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
            
            
    def SpaceAndTimeDiffList(self,j,l,fn,nL):
        cap = 'Convergence of $\\V^{j,\ell}$ in $\\|\\cdot\\|_\\LtwonormT$ at ' 
        for p in self.evalP[:-2]:
            cap += self.tab.getPointNamebyNumber(p)+ ', '
        cap+=self.tab.getPointNamebyNumber(self.evalP[-2]) +'~and '+ self.tab.getPointNamebyNumber(self.evalP[-1]) +'~for the \\LISVI~and \\SISVI~scheme'
        filename =fn+'tex/SpaceTimeLISIP'
        label='tab:SpaceTimeLISIP'
        for p in self.evalP:
            label+=str(p)
            filename+=str(p)
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable())
            output_file.write(self.tab.writeCaption(cap,label))
            output_file.write('\\begin{tabular}{cc}\n')
            output_file.write(self.tab.writeBeginSpace())
            self.tab.setFilePath(self.fL[0])
            output_file.write(self.tab.writeSpaceDiffPointRows(j,self.evalP,nL[0]))
            self.tab.setFilePath(self.fL[1])
            output_file.write('\\midrule\n')
            output_file.write(self.tab.writeSpaceDiffPointRows(j,self.evalP,nL[1]))
            output_file.write(self.tab.EndTabular())
            output_file.write('&\n')#output_file.write('\\hfil\n')
            output_file.write(self.tab.writeBeginTime())
            self.tab.setFilePath(self.fL[0])
            output_file.write(self.tab.writeTimeDiffPointRows(l,self.evalP,nL[0]))
            self.tab.setFilePath(self.fL[1])
            output_file.write('\\midrule\n')
            output_file.write(self.tab.writeTimeDiffPointRows(l,self.evalP,nL[1]))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
            
    def writeExtraErrorListTable(self,fn,nL,discPair,J,L):
        cap='Estimated relative error in space, time and both for the finest discretizations computed with the linear implicit (\\LISVI) and the semi-implicit (\\SISVI) scheme'
        filename =fn+'tex/LISIExtraErrorTab'
        label='tab:LISIExtraError'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap,label))
            output_file.write('\\begin{tabular}{c|c|c|c|c}\n')
            output_file.write(self.tab.writeExtraErrorRows2(self.fL,nL,self.evalP,J,L,discPair))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
    def writeCVTable(self,fn,x,y):
        cap='Conduction velocities between $\\vec x$ and $\\vec y$ computed with the semi-implicit (\\SISVI) scheme on the truncated ellipsoidal mesh'
        filename =fn+'tex/CVEllipsoid'
        label='tab:CVEllipsoid'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap,label))
            output_file.write(self.tab.writeBeginSpace())
            output_file.write(self.tab.writeCVRows(x,y))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
    def writeCVFromListTable(self,fn,pairL,nL):
        cap='Conduction velocities computed with the semi-implicit (\\SISVI) scheme on the truncated ellipsoidal mesh'
        filename =fn+'tex/CVsEllipsoid'
        label='tab:CVsEllipsoid'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap,label))
            output_file.write(self.tab.writeBeginSpace())
            output_file.write(self.tab.writeCVRows(pairL,nL))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
    def writeActivationPointlistTab(self,fn, meshname,pL):
        if meshname=="truncatedEllipsoid":
            cap='Activation times $\\tact(\\vec x)$ computed with the semi-implicit (\\SISVI) scheme on the truncated ellipsoidal mesh'
            filename =fn+'tex/ActivationsEllipsoid'
            label='tab:ActisEllipsoid'
        else:
            print('not implemented')
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap,label))
            output_file.write(self.tab.writeBeginSpace())
            output_file.write(self.tab.writeActivationRow(pL))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
    def writeActivaitionRMSError(self,fn):
        cap="Root mean square error of the activation times in space (left) and time (right)"
        label='tab:RMSerror'
        filename = fn+'tex/RMSError'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap,label))
            output_file.write('\\centering \n \\begin{tabular}{cc}\n')
            output_file.write(self.tab.writeBeginVariableLength(len(self.lL)-1))
            output_file.write(self.tab.writeActivationRMSerrorSpaceRow())
            output_file.write(self.tab.EndTabular())
            output_file.write('&\n')
            output_file.write(self.tab.writeBeginVariableLength(len(self.tL)-1))
            output_file.write(self.tab.writeActivationRMSerrorTimeRow())
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
            
    def writeActivationTab(self,fn,x, meshname):
        if meshname=="biventricle":
            if isinstance(x,int):
                cap='The activation time $\\tact(\\pacti)$  including the extrapolates in space and time (left) and the error $\errorActi(\pacti)$ from \eqref{eq:errorActiv} (right) for different space and time discretizations both computed with the semi-implicit (\SISVI) scheme on the realistic bi-ventricular mesh'
                filename =fn+'tex/ActivationVentriclesAndError'
                label='tab:ActiAndErrorBiVentricel'
            else:
                cap='Activation time $\\tact'+str(x)+'$ computed with the semi-implicit (\\SISVI) scheme on the realistic ventricle mesh'
                filename =fn+'tex/ActivationVentriclesx'
                label='tab:ActiVentriclesx'
        elif meshname=="truncatedEllipsoid":
            cap='Activation time $\\tact'+str(x)+'$ computed with the semi-implicit (\\SISVI) scheme on the truncated ellipsoidal mesh'
            filename =fn+'tex/ActivationEllipsoidx'
            label='tab:ActiEllipsoidx'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap,label))
            if meshname=="biventricle":
                output_file.write('\\centering \n \\begin{tabular}{cc}\n')
                output_file.write(self.tab.writeBeginSpace(True))
                output_file.write(self.tab.writeActivationRow([x],True))
                output_file.write(self.tab.EndTabular())
                output_file.write('&\n')
                output_file.write(self.tab.writeBeginSpace(False))
                output_file.write(self.tab.writeActivationExtraErrorRow([x]))
                output_file.write(self.tab.EndTabular())
                output_file.write(self.tab.EndTabular())
                
            else:
                output_file.write(self.tab.writeBeginSpace())
                output_file.write(self.tab.writeActivationRow([x]))
                output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
    
    def writeMeanTactTable(self,fn,l0Path,sP,L,J,refPath, vArr,saveName='',saveInDiss=False,language='eng'):
        if language=='eng':
            cap='The activation time $\\tact(\V)$  and the difference with respect to the reference solution on different levels in space and time for the semi-implicit (\\SISVI) scheme (the numbers are displayed in ms)'
        elif language=='d':
            cap='Die Aktivierungszeit $\\tact(\V)$  und die Differenz zur Referenzlösung $\\rmse(\V^{j,l},\V^{'+str(J)+','+str(L)+'})$ für verschiedene Zeit- und Ortsdiskretisierungen mit dem semi-impliciten  Verfahren(\\SISVI)'
        else:
            print('language not known')
        label='tab:ActiAndError'+saveName
        if saveInDiss:
            filename='../../../thesis-lindner/thesis/figure/'+'ActivationVentricles'+saveName
        else:
            filename=fn+'tex/'+'ActivationVentricles'+saveName
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap,label))
            #output_file.write('\\hspace*{-2mm}\n')
            output_file.write('\\vspace*{7mm}\n')
            output_file.write('\\begin{tabular}{cc}\n')
            output_file.write('\\resizebox{8cm}{!}{\\input{'+sP+'ActivationSpace'+saveName+'}}\n')
            output_file.write('\\\\[-5.5cm]\n')
            output_file.write('&\n')
            output_file.write(self.tab.writeMeanTactTabular(fn,l0Path,vArr))
            output_file.write('\\\\[1.9cm]\n')  
            output_file.write('&\n')
            if L!=0 and J !=0:
                output_file.write(self.tab.writeRMSRefErrorTabular(L,J,fn,l0Path,refPath))
            else:
                output_file.write("Noch keine Referenzlösung berechnet.\\\\[3cm]\n")
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
            
    def writeMiddeledActivationTab(self,fn,l0Path,name):
        cap='The activation time $\\tact(\V)$   on different levels in space and time for the '+name+' scheme (the numbers are displayed in ms)'
        label='tab:ActiTimes'
        filename=fn+'tex/'+'ActivationTimesVentricles'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap,label))
            output_file.write(self.tab.writeMeanTactTabular(fn,l0Path))
            output_file.write(self.tab.writeEndTable())
    def writeCVPlotAndTable(self,fn,sP,pairL,nL,distL,j):
        cap='Conduction velocities  in m/s computed with the semi-implicit (\SISVI) scheme on the truncated ellipsoidal mesh (left) and the convergence of the conduction velocities in space (right) both for fixed time $j='+str(j)+'$. We abbreviate $\\CV(v,\\vec x,\\vec y)$ by $\\CV(v)$'
        label='tab:CVsEllipsoid'
        filename=fn+'tex/'+'CVsEllipsoid'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap, label))
            output_file.write('\\hspace*{-2mm}\n')
            output_file.write('\\begin{tabular}{cc}\n')
            output_file.write('\\resizebox{8.3cm}{!}{\\input{' + sP + 'CVEllipsoidPlot}}\n')
            output_file.write('\\\\[-6.2cm]\n')
            output_file.write('&\n')
            output_file.write(self.tab.writeCVDiffTab(fn, pairL, nL, distL, j))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
    def writeErrorExtraandIntTab(self,path,fN1,fN2,savePath,nL1,nL2,th,case):
        for p in self.evalP:
            if case=='Ca':
                cap='Der Fehler $\\errorCa^{j,\ell,'+str(p)+'}$ der Näherungslösungen des \\emm~bezüglich der Orts-Zeit-Grenzwertlösung $\\Ca^{\\infty,\\infty,'+str(p)+'}$ in der  $\\LtwonormT$-Norm auf dem biventrikulären Gebiet (links). Der Fehler $\\errorCaAct^{j,\ell,'+str(p)+'}$ der Näherungslösungen des \\emm~bezüglich der Orts-Zeit-Grenzwertlösung $\\Ca^{\\infty,\\infty,'+str(p)+'}_{(\\tCa^\\Ref,\\TCa^\\Ref)}$ in der  $\\LtwonormCa$-Norm auf dem biventrikulären Gebiet mit $\\TCa^\\Ref=\\tCa^\\Ref+'+str(durEvalActi)+'$ (rechts)'
                label ='tab:ErrorExtraAndIntCaP'+str(p)
                filename=savePath +'ErrorExtraAndIntCaP'+str(p)
            else:
                print('case',case,'is not defined for writeErrorExtraandIntTab')
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginTable('h'))
                output_file.write(self.tab.writeCaption(cap, label))
                output_file.write(self.tab.writeExtraErrorandIntTab(fN1,fN2,nL1,nL2,p,th,case))
                output_file.write(self.tab.writeEndTable())
        
    def writeErrorExtraTab(self,path,fN,savePath,nL,case):
        for p in self.evalP:
            if case=='Ca':
                cap='Der Fehler $\\errorCa^{j,\ell,'+str(p)+'}$ der Näherungslösungen  bezüglich der Orts-Zeit-Grenzwertlösung $\\Ca_'+str(p)+'^{\\infty,\\infty}$ in der  $\\LtwonormT$-Norm auf dem biventrikulären Gebiet'# mit $\\Tact=\\tact+'+str(durEvalActi)+'$\,'
                label ='tab:ErrorExtraCaP'+str(p)
                filename=savePath +'ErrorExtraCaP'+str(p)
            elif case=='Gamma':
                cap='Der  Fehler $\\errorG^{j,\ell}$ der Näherungslösungen  bezüglich der Orts-Zeit-Grenzwertlösung $\\gf'+'^{\\infty,\\infty}$ in der  $\\LtwonormT$-Norm auf dem biventrikulären Gebiet'# mit $\\Tact=\\tact+'+str(durEvalActi)+'$\,'
                label ='tab:ErrorExtraGammaP'+str(p)
                filename=savePath +'ErrorExtraGammaP'+str(p)
            elif case=='Volumes':
                cap='Der  Fehler $\\errorLV^{j,\ell}$ bzw. $\\errorRV^{j,\ell}$ der Näherungslösungen  bezüglich der Orts-Zeit-Grenzwertlösung $\\LV'+'^{\\infty,\\infty,}$ bzw.$\\RV'+'^{\\infty,\\infty,}$  in der  $\\LtwonormT$-Norm auf dem biventrikulären Gebiet'# mit $\\Tact=\\tact+'+str(durEvalActi)+'$\,'
                label ='tab:ErrorVolumes'
                filename=savePath +'ErrorVolumes'
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginApproxAndDurationTabList(p,cap,label,fN,False))
                output_file.write(self.tab.writeExtraErrorTab(fN,nL,p,case))
                output_file.write(self.tab.writeEndTable())
    def writeVolumesExtraError(self,path,fN,savePath,nL,inInt):
        if inInt:
            cap='Der  Fehler $\\errorLV^{j,\ell}$ bzw. $\\errorRV^{j,\ell}$ der Näherungslösungen  bezüglich der Orts-Zeit-Grenzwertlösung $\\LV'+'^{\\infty,\\infty,}$ bzw. $\\RV'+'^{\\infty,\\infty,}$  in der  $\\LtwonormAct$-Norm auf dem biventrikulären Gebiet'
            label ='tab:ErrorActVolumes'
            filename=savePath +'ErrorActVolumes'
        else:
            cap='Der  Fehler $\\errorLV^{j,\ell}$ bzw. $\\errorRV^{j,\ell}$ der Näherungslösungen  bezüglich der Orts-Zeit-Grenzwertlösung $\\LV'+'^{\\infty,\\infty,}$ bzw. $\\RV'+'^{\\infty,\\infty,}$  in der  $\\LtwonormT$-Norm auf dem biventrikulären Gebiet'
            label ='tab:ErrorVolumes'
            filename=savePath +'ErrorVolumes'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap, label))
            output_file.write(self.tab.writeExtraErrorVolumes(fN,nL,inInt))
            output_file.write(self.tab.writeEndTable())
        
    def writeErrorExtraVIntTab(self,path,fN,savePath,nL,threshold,case='',experiment=''):
        cap=''
        for p in self.evalP:
            if case=='Ca':
                cap='Der Fehler $\\errorCaAct^{j,\ell,'+str(p)+'}$ der Näherungslösungen  bezüglich der Orts-Zeit-Grenzwertlösung $\\Ca^{\\infty,\\infty,'+str(p)+'}_{(\\tCa^\\Ref,\\TCa^\\Ref)}$ in der  $\\LtwonormCa$-Norm auf dem biventrikulären Gebiet mit $\\TCa^\\Ref=\\tCa^\\Ref+'+str(durEvalActi)+'$\,'
                label ='tab:ErrorExtraCaIntP'+str(p)
                filename=savePath +'ErrorExtraCaIntP'+str(p)
            elif case=='Gamma':
                cap='Der Fehler $\\errorGAct^{j,\ell}$ der Näherungslösungen  bezüglich der Orts-Zeit-Grenzwertlösung $\\gfAct^{\\infty,\\infty}$ in der  $\\LtwonormG$-Norm auf dem biventrikulären Gebiet mit $\\Tg=\\tg+'+str(durEvalActi)+'$\,'
                label ='tab:ErrorExtraGammaIntP'+str(p)
                filename=savePath +'ErrorExtraGammaIntP'+str(p)
            else:
                if experiment=='Coupled':
                    cap='Der Fehler $\\errorVact^{j,\ell,'+str(p)+'}$ der Näherungslösungen des \\emm~bezüglich der Orts-Zeit-Grenzwertlösung $\\V_'+str(p)+'^{\\infty,\\infty}$ in der  $\\LtwonormAct$-Norm auf dem biventrikulären Gebiet $\\Tact=\\tact+'+str(durEvalActi)+'$ mit der vereinfachten Anregung'
                else:
                    cap='Der Fehler $\\errorVact^{j,\ell,'+str(p)+'}$ der Näherungslösungen  bezüglich der Orts-Zeit-Grenzwertlösung $\\V_'+str(p)+'^{\\infty,\\infty}$ in der  $\\LtwonormAct$-Norm auf dem biventrikulären Gebiet $\\Tact=\\tact+'+str(durEvalActi)+'$ mit der vereinfachten Anregung'
                label ='tab:'+experiment+'ErrorExtraVIntP'+str(p)
                filename=savePath+experiment+'ErrorExtraVIntP'+str(p)
                
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginApproxAndDurationTabList(p,cap,label,fN,False))
                output_file.write(self.tab.writeExtraErrorLtwoNormAct(fN,nL,p,threshold,case))
                output_file.write(self.tab.writeEndTable())
    def writeVExtrapolationTab(self,path,fN,savePath,nL,threshold):
        cap='Extrapolierte der Näherungslösung in der  $\\norm{\\V^{j,\\ell}_k}_\\LtwonormAct$ berechnet mit dem '+nL[0]+'~in den verschiedenen Auswertungspunkten \Points~für $k=1,\\dots,9$ auf dem biventrikulären Gebiet und $\\Tact=\\tact+'+str(durEvalActi)+'$\,'
        label ='tab:ExtraV'
        filename=savePath +'ExtraV'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap, label))
            output_file.write(self.tab.writeExtraVTab(path,fN,nL,threshold,self.evalP))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
    def writeActivationExtrapolationTab(self,path,fN,savePath,nL,threshold):
        cap='Extrapolierte der Aktivierungszeit $\\tact$ für '+nL[0]+'~in den verschiedenen Auswertungspunkten auf dem biventrikulären Gebiet'
        label ='tab:ExtraTact'
        filename=savePath +'ExtraTact'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap, label))
            output_file.write(self.tab.writeExtraTactTab(path,fN,nL,threshold,self.evalP))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
        
    def writeAPDTable(self,fL,nL,savePath,threshold,experiment=''):
        cap=''
        label=''
        filename=''
        for p in self.evalP:
            if experiment=='Coupled':
                cap='Die Dauer des Aktionspotentials $\\tdur(\\V^{j,\\ell},'+self.tab.getPointNamebyNumber(p,True) +')$ in s der Näherungslösungen des \\emm~auf dem biventrikulären Gebiet mit der vereinfachten Anregung für verschiedene Zeitdiskretisierungsverfahren'
            else:
                cap='Die Dauer des Aktionspotentials $\\tdur(\\V^{j,\\ell},'+self.tab.getPointNamebyNumber(p,True) +')$ in s der Näherungslösungen des \\mono s auf dem biventrikulären Gebiet mit der vereinfachten Anregung für verschiedene Zeitdiskretisierungsverfahren'
            label ='tab:'+experiment+'APDBVSEP'+str(p)
            filename=savePath+experiment+'APDBVSEinP'+str(p)
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginApproxAndDurationTabList(p,cap,label,fL,False))
                output_file.write(self.tab.writeAPDTimesTab(fL,nL,threshold,p))
                output_file.write(self.tab.writeEndTable())
    def writeTactTable(self,fL,nL,savePath,threshold,experiment=''):
        cap=''
        label=''
        filename=''
        for p in self.evalP:
            if experiment=='Coupled':
                cap='Aktivierungszeiten $\\tact(\\V^{j,\\ell},'+self.tab.getPointNamebyNumber(p,True) +')$ der Näherungslösungen des \\emm~auf dem biventrikulären Gebiet mit der vereinfachten Anregung für verschiedene Zeitdiskretisierungsverfahren'
                label ='tab:CoupledActitimesBVSEP'+str(p)
                filename=savePath +'CoupledActiTimesBVSEinP'+str(p)
            else:
                cap='Aktivierungszeiten $\\tact(\\V^{j,\\ell},'+self.tab.getPointNamebyNumber(p,True) +')$ der Näherungslösungen des \\mono s auf dem biventrikulären Gebiet mit der vereinfachten Anregung für verschiedene Zeitdiskretisierungsverfahren'
                label ='tab:ActitimesBVSEP'+str(p)
                filename=savePath +'ActiTimesBVSEinP'+str(p)
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginApproxAndDurationTabList(p,cap,label,fL,False))
                output_file.write(self.tab.writeActivationTimesTab(fL,nL,threshold,p))
                output_file.write(self.tab.writeEndTable())
    def writeComparisionDurationTab(self,path,fnList,savePath,nL,n):
        cap='Vergleich der Rechenzeiten in Sekunden für verschiedene Verfahren mit $T='+str(self.T)+'\\,$s. Die Gesamtdauer ist in \\textit{Minuten}:\\textit{Sekunden} angegeben. Die Werte sind Durchschnittswerte von jeweils $'+str(n)+'$ Rechnungen'
        label ='tab:CPU'
        filename=savePath +'CPU'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap, label))
            output_file.write(self.tab.writeCPUTab(path,fnList,nL,n))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
    def writeSchwingerPerPoint(self,fL,nL,savePath,threshold):
        for p in self.evalP:
             cap='Minimum und Maximum der Transmembranspannung einer Näherungslösung $\\V^{j,\\ell,'+str(p)+'}'+'$ auf dem biventrikulären Gebiet mit der vereinfachten Anregung für verschiedene Zeitdiskretisierungsverfahren'
             label='tab:SchwingerP'+str(p)
             filename=savePath+'SchwingerP'+str(p)
             with open(filename+'.tex', 'w') as output_file:
                 output_file.write(self.tab.writeBeginTable('h'))
                 output_file.write(self.tab.writeCaption(cap, label))
                 output_file.write(self.tab.writeSchwingerPerPoint(fL,nL,p,threshold))
                 output_file.write(self.tab.writeEndTable())
                 
    def writeUnterschwingerPerPoint(self,fL,nL,savePath,threshold):
        for p in self.evalP:
             cap='Minimum der Transmembranspannung einer Näherungslösung $\\V^{j,\\ell,'+str(p)+'}'+'$ auf dem biventrikulären Gebiet mit der vereinfachten Anregung für verschiedene Zeitdiskretisierungsverfahren'
             label='tab:UnterschwingerP'+str(p)
             filename=savePath+'UnterschwingerP'+str(p)
             with open(filename+'.tex', 'w') as output_file:
                 output_file.write(self.tab.writeBeginTable('h'))
                 output_file.write(self.tab.writeCaption(cap, label))
                 output_file.write(self.tab.writeUnterschwingerPerPoint(fL,nL,p,threshold))
                 output_file.write(self.tab.writeEndTable())
                 
    def writetactAndErrorPerPoint(self,fL,nL,savePath,threshold,experiment=''):
         for p in self.evalP:
             tactRef=comp.getTextFullExtrapolate(self.tab.lL[-1],self.tab.tL[-1],1,p,self.tab.exP,threshold)
             if experiment=='Coupled':
                cap='Aktivierungszeiten $\\tact^{j,\ell,'+self.tab.getPointNamebyNumber(p,True) +'}$ in ms  und der Fehler $\\errortact^{j,\ell,'+self.tab.getPointNamebyNumber(p,True) +'}$ der Aktivierungszeit bezüglich der Referenzlösung $\\tact^{\\infty,\\infty,'+str(p)+'}='+str("%8.3f"%(1000.0*tactRef))+'$\,ms auf dem biventrikulären Gebiet mit der vereinfachten Anregung für verschiedene Zeitdiskretisierungsverfahren zur Lösung des \\emm'
             else:
                cap='Aktivierungszeiten $\\tact^{j,\ell,'+self.tab.getPointNamebyNumber(p,True) +'}$ in ms  und der Fehler $\\errortact^{j,\ell,'+self.tab.getPointNamebyNumber(p,True) +'}$ der Aktivierungszeit bezüglich der Referenzlösung $\\tact^{\\infty,\\infty,'+str(p)+'}='+str("%8.3f"%(1000.0*tactRef))+'$\,ms auf dem biventrikulären Gebiet mit der vereinfachten Anregung für verschiedene Zeitdiskretisierungsverfahren'
             label='tab:'+experiment+'TactAndErrorP'+str(p)
             filename=savePath+experiment+'TactAndErrorP'+str(p)
             with open(filename+'.tex', 'w') as output_file:
                 output_file.write(self.tab.writeBeginTable('h'))
                 output_file.write(self.tab.writeCaption(cap, label))
                 output_file.write(self.tab.writeTactErrorPerPoint(fL,nL,threshold,p))
                 output_file.write(self.tab.writeEndTable())
                 
    def writeDifferencesVolumes(self,l,j,fL,nL,savePath):
        #LV:
        cap='$\\LtwonormT$-Norm der benachbarten Differenzen Volumenkurve $\\LV$  für die  benachbarten Ortslevel (links) und Zeitlevel (rechts) '
        label ='tab:DifferencesLtwoLV'
        filename=savePath +'DifferencesBiventricleLtwoLV'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap, label))
            output_file.write('\\begin{tabular}{cc}\n')
            output_file.write(self.tab.writeDifferenceTableSpaceVolume(j,fL,nL,1,'LV'))
            output_file.write('&\n')
            output_file.write(self.tab.writeDifferenceTableTimeVolume(l,fL,nL,1,'LV'))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
        
        #RV:
        cap='$\\LtwonormT$-Norm der benachbarten Differenzen Volumenkurve $\\RV$  für die  benachbarten Ortslevel (links) und Zeitlevel (rechts) '
        label ='tab:DifferencesLtwoRV'
        filename=savePath +'DifferencesBiventricleLtwoRV'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap, label))
            output_file.write('\\begin{tabular}{cc}\n')
            output_file.write(self.tab.writeDifferenceTableSpaceVolume(j,fL,nL,2,'RV'))
            output_file.write('&\n')
            output_file.write(self.tab.writeDifferenceTableTimeVolume(l,fL,nL,2,'RV'))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
            
            
    def writeMechTimeStepTestTable(self,l,p,fL,nL,threshold,savePath,numberofAdaptive):
        
        cap='Untersuchung verschiedener Varianten zur Anpassung der Zeitschrittweite ${\\vartriangle}t^{\\mathrm{m}}$  für die Mechanik für das \\emm~berechnet mit dem \\SISVI~in '+self.tab.getPointNamebyNumber(p,False)+'. Es wurden $512$ parallele Prozesse auf dem HoreKa verwendet.  Die Rechenzeit ist in Stunden:Minuten:Sekunden und die Zeitschrittweiten sind in Millisekunden  angegeben'
        label ='tab:MechTimeStepTestP'+str(p)
        filename=savePath +'MechTimeStepTestP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginApproxAndDurationTabList(p,cap,label,fL,False))
                output_file.write(self.tab.MechTimeStepTest(l,p,fL,nL,threshold,numberofAdaptive))
                output_file.write(self.tab.writeEndTable())
        
    def writeDifferenceTablePerPoint(self,l,j,fL,nL,savePath,case,threshold=-50.0,experiment=''):
        cap=''
        filename=''
        label=''
        for p in self.evalP:
            if case =='VInt':
                cap='Differenzen der $\\norm{\\V^{j,\\ell}(\\cdot,'+self.tab.getPointNamebyNumber(p,True) +')}_\\LtwonormAct$ mit $\\Tact=\\tact+'+str(durEvalActi)+'$ der benachbarten Ortslevel (links) und Zeitlevel (rechts) in '+self.tab.getPointNamebyNumber(p,False)
                label ='tab:DifferencesLtwoActP'+str(p)
                filename=savePath +'DifferencesBiventricleLtwoActP'+str(p)
            elif case =='tact':
                if(experiment=='Coupled'):
                    cap='Differenzen der Aktivierungszeiten $\\tact$ in ms der benachbarten Ortslevel (links) und Zeitlevel (rechts) in '+self.tab.getPointNamebyNumber(p,False)+'~der Näherungslösungen von \\emm'
                    label ='tab:'+experiment+'DifferencestactP'+str(p)
                    filename=savePath+experiment +'DifferencesBiventricletactP'+str(p)
                else:
                    cap='Differenzen der Aktivierungszeiten $\\tact$ in ms der benachbarten Ortslevel (links) und Zeitlevel (rechts) in '+self.tab.getPointNamebyNumber(p,False)
                    label ='tab:DifferencestactP'+str(p)
                    filename=savePath +'DifferencesBiventricletactP'+str(p)
            elif case =='VDiffInt':
                if(experiment=='Coupled'):
                    cap='$\\LtwonormAct$-Norm der benachbarten Differenzen  der benachbarten Ortslevel (links) und Zeitlevel (rechts) in '+self.tab.getPointNamebyNumber(p,False)+'~für $\\Tact=\\tact+'+str(durEvalActi)+'$'+' der Näherungslösungen von \\emm~mit der vereinfachten Anregung'
                    label ='tab:'+experiment+'DifferencesLtwoDiffActP'+str(p)
                    filename=savePath+experiment +'DifferencesBiventricleLtwoDiffActP'+str(p)
                else:
                    cap='$\\LtwonormAct$-Norm der benachbarten Differenzen  der benachbarten Ortslevel (links) und Zeitlevel (rechts) in '+self.tab.getPointNamebyNumber(p,False)+'~für $\\Tact=\\tact+'+str(durEvalActi)+'$ mit der vereinfachten Anregung'
                    label ='tab:DifferencesLtwoDiffActP'+str(p)
                    filename=savePath +'DifferencesBiventricleLtwoDiffActP'+str(p)
            elif case =='Ca':
                cap='$\\LtwonormT$-Norm der benachbarten Differenzen der intazellulären Kalziumkonzentration $\\Ca$ in \\textmu mol/l für die  benachbarten Ortslevel (links) und Zeitlevel (rechts) in '+self.tab.getPointNamebyNumber(p,False)
                label ='tab:DifferencesLtwoCaP'+str(p)
                filename=savePath +'DifferencesBiventricleLtwoCaP'+str(p)
            elif case =='Gamma':
                cap='$\\LtwonormT$-Norm der benachbarten Differenzen der Dehnung in Faserrichtung $\\gf$ für die  benachbarten Ortslevel (links) und Zeitlevel (rechts) '#in '+self.tab.getPointNamebyNumber(p,False)
                label ='tab:DifferencesLtwoGammaP'+str(p)
                filename=savePath +'DifferencesBiventricleLtwoGammaP'+str(p)
            
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginTable('h'))
                output_file.write(self.tab.writeCaption(cap, label))
                output_file.write('\\begin{tabular}{cc}\n')
                output_file.write(self.tab.writeDifferenceTableSpacePerPoint(j,fL,nL,p,case,threshold))
                output_file.write('&\n')
                output_file.write(self.tab.writeDifferenceTableTimePerPoint(l,fL,nL,p,case,threshold))
                output_file.write(self.tab.EndTabular())
                output_file.write(self.tab.writeEndTable())
            
    def writeDifferenceTable(self,l,j,fL,nL,savePath,case,threshold=-50.0):
        if case =='V':
            cap='Differenzen der Näherungslösungen $\\V^{j,\\ell}$ der benachbarten Ortslevel (links) und Zeitlevel (rechts)'
            label ='tab:DifferencesV'
            filename=savePath +'DifferencesBiventricleV'
        elif case =='tact':
            cap='Differenzen der Aktivierungszeiten $\\tact$ der benachbarten Ortslevel (links) und Zeitlevel (rechts)'
            label ='tab:Differencestact'
            filename=savePath +'DifferencesBiventricletact'
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(self.tab.writeBeginTable('h'))
            output_file.write(self.tab.writeCaption(cap, label))
            output_file.write('\\begin{tabular}{cc}\n')
            output_file.write(self.tab.writeDifferenceTableSpace(j,fL,nL,self.evalP,case,threshold))
            output_file.write('&\n')
            output_file.write(self.tab.writeDifferenceTableTime(l,fL,nL,self.evalP,case,threshold))
            output_file.write(self.tab.EndTabular())
            output_file.write(self.tab.writeEndTable())
    def writeIntervalLtwoNormTab(self, fL,nL,savePath,th):
        for p in self.evalP:
            cap='Die Werte von  $\\norm{\\V^{j,\\ell}(\\cdot,'+self.tab.getPointNamebyNumber(p,True) +')}_\\LtwonormAct$ auf dem biventrikulären Gebiet mit der vereinfachten Anregung für verschiedene Zeitdiskretisierungsverfahren und Diskretisierungen in Ort und Zeit mit $\\Tact=\\tact+0.4$\,'
            label ='tab:ActLtwoNormsBVSEP'+str(p)
            filename=savePath +'ActLtwonormsBVSEinP'+str(p)
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginApproxAndDurationTabList(p,cap,label,fL,False))
                output_file.write(self.tab.writeLtwoNormAct(fL,nL,p,th))
                output_file.write(self.tab.writeEndTable())
    def witeLtwoNormTab(self,fL,nL,savePath,withDuration=True):
        for p in self.evalP:
            cap='Die Werte von  $\\norm{\\V^{j,\\ell}(\\cdot,'+self.tab.getPointNamebyNumber(p,True) +')}_\\LtwonormT$ auf dem biventrikulären Gebiet mit der vereinfachten Anregung für verschiedene Zeitdiskretisierungsverfahren und Diskretisierungen in Ort und Zeit'
            label ='tab:LtwoNormsBVSEP'+str(p)
            filename=savePath +'LtwonormsBVSEinP'+str(p)
            with open(filename+'.tex', 'w') as output_file:
                output_file.write(self.tab.writeBeginApproxAndDurationTabList(p,cap,label,fL,withDuration))
                output_file.write(self.tab.writeLtwoNormsAndDurationTab(fL,nL,p,withDuration))
                output_file.write(self.tab.writeEndTable())
    def NiedererBMTabActivationTimeConvergence(self,point,pointNumber,tdisc,sdisc):
        cap='Ortskonvergenz der Aktivierunszeit in Punkt P'+str(pointNumber)+ ' für verschiedene Zeitdiskretisierungen'
        label = 'tab:NiedererBMActivationConv'
        filename=self.fL[0] + 'tex/'+'NiedererBMActivationP'+str(pointNumber)
        with open(filename+'.tex', 'w') as output_file:
            #output_file.write(self.tab.writeBeginTable('H'))
            #output_file.write(self.tab.writeCaption(cap, label))
            output_file.write(self.tab.writeNiederBMActivationTimeConvergencePointwise(point,pointNumber,tdisc,sdisc))
            #output_file.write(self.tab.writeEndTable())

    def writeDatFilesForNiedererBenchmark(self, path, delta_x, tdisc, evaluationPoints):
        with open(path + 'data/' + str('ActivationTimesPointsBenchmark'), 'w') as output_file:
            for i in range(len(delta_x)):
                for j in range(len(tdisc)):
                    (P_test, acti) = comp.getActivationForPointsForBenchmark(path, i, j, evaluationPoints)
                    output_file.write(str("%.*f" % (3, delta_x[i])) + " " + str(tdisc[j]) + " ")
                    for t in range(len(acti)):
                        if t != len(acti) - 1:
                            # "%.*f" %(prec,value)
                            output_file.write(str("%.*f" % (4, acti[t])) + " ")
                        else:
                            output_file.write(str("%.*f" % (4, acti[t])))
                    output_file.write("\n")

    def writeLatexTableForNiedererBenchmark(self, path):
        atimes = []
        with open(path + 'data/' + 'ActivationTimesPointsBenchmark', 'r') as datafile:
            for line in datafile:
                atimes.append(line.split(' '))

        for i in range(len(atimes[0])):
            for j in range(len(atimes)):
                atimes[j][i] = float(atimes[j][i])

        activation_times = []
        for u in reversed(atimes):
            activation_times.append(u)

        with open(path + 'tex/' + 'ActivationTimesPointsBenchmark.tex', 'w') as tex_file:
            tex_file.write("\\begin{table}[h] \n \\begin{center} \n \\begin{tabular}[h]{c| c|c c c c c c c c c}\n \
            $\\vartriangle x$ & $\\vartriangle t $ & $P_1$ &$P_2$ & $P_3$ & $P_4$ & $P_5$ & $P_6$ & $P_7$ & $P_8$ & $C$  \\\  \n \
            \\hline    \n")
            for l in range(len(activation_times)):
                if l != len(activation_times) - 1:
                    for k in range(len(activation_times[0])):
                        if k != len(activation_times[0]) - 1:
                            tex_file.write(str("%.*f" % (4, activation_times[l][k])) + " & ")
                        else:
                            tex_file.write(str("%.*f" % (4, activation_times[l][k])) + "\\\ \n \hline \n ")
                else:
                    for k in range(len(activation_times[0])):
                        if k != len(activation_times[0]) - 1:
                            tex_file.write(str("%.*f" % (4, activation_times[l][k])) + " & ")
                        else:
                            tex_file.write(str("%.*f" % (4, activation_times[l][k])) + "\n")

            tex_file.write(
                "\\end{tabular} \n \\end{center} \n \\caption{Activation time at point $P_8$ for every combination of spatial and temporal refinement.} \n \\end{table}")
