

class DW:
    def __init__(self, lList, tList,start_dt,endTime,substep,pathlist,namelist,evaluationPoints,savePath,sourcePath):
        self.lL = lList
        self.tL = tList
        self.m = substep
        self.sdt = start_dt
        self.T =endTime
        self.pL =pathlist
        self.nL =namelist
        self.eP = evaluationPoints
        self.sP=savePath
        self.sourceP =sourcePath
    
    def write(self,option):
        with open(self.sP +self.nL[0]+'.tex', 'w') as output_file:
            output_file.write(self.section('All Points'))
            output_file.write(self.figureAllPoints())
            for point in self.eP:
                caption='\\textbf{P'+str(point)+'}'
                output_file.write('\\newpage\n')
                output_file.write(self.section(caption))
                for index in range(len(self.pL)):
                    
                    output_file.write( '\\subsection{'+self.nL[index+1]+'}\n')
                    if option>=3:
                        output_file.write(self.ExtrapolationTables(point,index))
                    output_file.write(self.PointConvergencePlots(point,index))
                    output_file.write('\\newpage\n')
                #if option>=3:
                    #output_file.write(self.ExtrapolateFigure(point))
                if option>=4:
                    cap='Comparing '+self.nL[1]+'~(dashed) and ' +self.nL[-1]+'~(dotted) for fixed levels and with varying time step size in \\textbf{P'+str(point)+'}, while the blue line corresponds to the full extrapolate of '+self.nL[-1]+ '~and the purple line to the full extrapolate of '+self.nL[1]
                    output_file.write(self.CompareTwoAlgsTime(point,cap))
                
    def writeSplittingTest(self,option,j,refNames,L,J,source):
        with open(self.sP +self.nL[0]+'.tex', 'w') as output_file:
            output_file.write(self.section('Difference Extrapolates'))
            for point in self.eP:
                output_file.write(self.DiffExtrapolates(point,refNames,L,J,source))
            output_file.write('\\newpage\n')
            output_file.write(self.section('Splitting test'))
            #output_file.write(self.figureAllPoints())
            for point in self.eP:
                caption='\\textbf{P'+str(point)+'}'
                #output_file.write('\\newpage\n')
                output_file.write( '\\subsection{'+caption+'}'+'\n')
                output_file.write(self.SchemeComparison(point,j,'Different splitting schemes in \\textbf{P'))
                if option>=2:
                    for index in range(len(self.pL)):
                        output_file.write(self.PointConvergencePlots(point,index))
                    output_file.write('\\newpage\n')
    
    def writeDokumentTimeAsymptoticPlots(self,l):
        with open(self.sP +self.nL[0]+'.tex', 'w') as output_file:
            output_file.write(self.section('Asymptotic time test plots'))
            for p in self.eP:
                output_file.write(self.SchemeComparisonTime(p,l,'Different schemes in \\textbf{P'))
                
    def writeDokumentValueTables(self):
        with open(self.sP +self.nL[0]+'.tex', 'w') as output_file:
            output_file.write(self.section('Asymptotic time test'))
            for p in self.eP:
                output_file.write(self.input(self.sourceP,'SchemeComparisonValuesTabP'+str(p)))
                
        
    
    def writeAsymptoticData(self,lL,j,addTwoAlgComp,test=''):
        with open(self.sP +self.nL[0]+'.tex', 'w') as output_file:
            output_file.write(self.section('Asymptotic time test'))
            for p in self.eP:
                output_file.write(self.input(self.sourceP,'SchemeComparison'+test+'ValuesTabP'+str(p)))
            output_file.write(self.section('Asymptotic time test plots'))
            for p in self.eP:
                #output_file.write(self.SchemeComparisonTime(p,l,j,'Different schemes for $\\ell='+str(l)+'$ in \\textbf{P'))
                if addTwoAlgComp:
                    cap='Comparing the the algorithm '+self.nL[1] +'(dashed) and '+self.nL[2] +' (dotted) for different time step sizes in \\textbf{P'+str(p)+'}'
                    output_file.write(self.CompareTwoAlgsTime(p,cap,lL))
                    cap='Comparing the the algorithm '+self.nL[1] +'(dashed) and '+self.nL[2] +' (dotted) for different levels in \\textbf{P'+str(p)+'}'
                    output_file.write(self.CompareTwoAlgsSpace(p,cap))
                else:
                    output_file.write(self.SchemeComparisonTime(p,lL[0],j,'Different schemes in \\textbf{P'))
                    
        
    def figureAllPoints(self):
        filename='BRl'+str(self.lL[-1])+'j'+str(self.tL[-1])+'m1AllPoints'
        figurecaption ='Transmembrane potential in all evaluation points at $\\ell='+str(self.lL[-1])+'$ and $j='+str(self.tL[-1])+'$ on the ellipsoid with two layers'
        block =''
        block+=self.beginFigure()
        for index in range(len(self.pL)):
            block += self.beginSubfigure()
            block += self.inputResizeBox(self.pL[index],filename)
            block+=self.subfigCaption('All points for '+self.nL[index+1])
            block += self.endSubfigure()
        block +=self.figureCaption(figurecaption) 
        block += self.figureLabel('AllPoints')
        block +=self.endFigure()
        return block
    
    def ExtrapolationTables(self,p,i):
        block=''
        block += self.input(self.pL[i],'ValueTab0001m1P'+str(p))
        block += self.input(self.pL[i],'ExtrapolationSpace0001m1P'+str(p))
        block += self.input(self.pL[i],'ExtrapolationTime0001m1P'+str(p))
        block += self.input(self.pL[i],'L2FullExtrapolation0001m1P'+str(p))
        block +='\\newpage\n'
        return block
        
    def PointConvergencePlots(self,p,i):
        figurecaption='Convergence for Space and time for '+self.nL[i+1]+ '~in \\textbf{P'+str(p)+'}'
        block =''
        block+=self.beginFigure()
        
        block+=self.beginSubfigure()
        block+=self.inputResizeBox(self.pL[i],'BRSpaceConvergencej'+str(self.tL[0])+'m1P'+str(p))
        block+=self.subfigCaption('$j='+str(self.tL[0])+'$')
        block+= self.endSubfigure()
        
        block+=self.beginSubfigure()
        block+=self.inputResizeBox(self.pL[i],'BRTimeConvergencel'+str(self.lL[0])+'m1P'+str(p))
        block+=self.subfigCaption('$\ell='+str(self.lL[0])+'$')
        block += self.endSubfigure()
        
        block+=self.beginSubfigure()
        block+=self.inputResizeBox(self.pL[i],'BRSpaceConvergencej'+str(self.tL[-1])+'m1P'+str(p))
        block+=self.subfigCaption('$j='+str(self.tL[-1])+'$')
        block += self.endSubfigure()
        
        block+=self.beginSubfigure()
        block+=self.inputResizeBox(self.pL[i],'BRTimeConvergencel'+str(self.lL[-1])+'m1P'+str(p))
        block+=self.subfigCaption('$\\ell='+str(self.lL[-1])+'$')
        block += self.endSubfigure()
        
        block +=self.figureCaption(figurecaption) 
        block += self.figureLabel('ConSTP'+str(p)+self.nL[i+1])
        block +=self.endFigure()
        return block
    def CompareTwoAlgsTime(self,p,caption,lList=[-1]):
        figurecaption=caption
        block =''
        block+=self.beginFigure()
        block+=self.beginSubfigure()
        if lList[0]==-1:
            block+=self.inputResizeBox(self.sourceP,'BRTwoSchemesTimel'+str(self.lL[0])+'P'+str(p))
            block+=self.subfigCaption('$\\ell='+str(self.lL[0])+'$')
        else:
            block+=self.inputResizeBox(self.sourceP,'BRTwoSchemesTimel'+str(lList[0])+'P'+str(p))
            block+=self.subfigCaption('$\\ell='+str(lList[0])+'$')
        block += self.endSubfigure()
        block+=self.beginSubfigure()
        if lList[0]==-1:
            block+=self.inputResizeBox(self.sourceP,'BRTwoSchemesTimel'+str(self.lL[-1])+'P'+str(p))
            block+=self.subfigCaption('$\\ell='+str(self.lL[-1])+'$')
        else:
            block+=self.inputResizeBox(self.sourceP,'BRTwoSchemesTimel'+str(lList[-1])+'P'+str(p))
            block+=self.subfigCaption('$\\ell='+str(lList[-1])+'$')
        block += self.endSubfigure()
        block +=self.figureCaption(figurecaption) 
        block += self.figureLabel('TwoSchemesTimeP'+str(p))
        block +=self.endFigure()
        return block
    def CompareTwoAlgsSpace(self,p,caption):
        figurecaption=caption
        block =''
        block+=self.beginFigure()
        block+=self.beginSubfigure()
        block+=self.inputResizeBox(self.sourceP,'BRTwoSchemesSpacej'+str(2)+'P'+str(p))
        block+=self.subfigCaption('$j='+str(2)+'$')
        block += self.endSubfigure()
        block+=self.beginSubfigure()
        block+=self.inputResizeBox(self.sourceP,'BRTwoSchemesSpacej'+str(3)+'P'+str(p))
        block+=self.subfigCaption('$j='+str(3)+'$')
        block += self.endSubfigure()
        block +=self.figureCaption(figurecaption) 
        block += self.figureLabel('TwoSchemesSpaceP'+str(p))
        block +=self.endFigure()
        return block
    def SchemeComparison(self,p,j,caption):
        figurecaption=caption+str(p)+'}'
        block =''
        block+=self.beginFigure()
        block+=self.beginSubfigure(0.3)
        block+=self.inputResizeBoxHöhe(self.sourceP,'DiffSchemesl'+str(self.lL[0])+'j'+str(j)+'P'+str(p),5)
        block+=self.subfigCaption('$\\ell='+str(self.lL[0])+'$')
        block += self.endSubfigure()
        #block+='\n'
        block+=self.beginSubfigure(0.65)
        block+=self.inputResizeBoxHöhe(self.sourceP,'DiffSchemesl'+str(self.lL[-1])+'j'+str(j)+'P'+str(p),5)
        block+=self.subfigCaption('$\\ell='+str(self.lL[-1])+'$')
        block += self.endSubfigure()
        block +=self.figureCaption(figurecaption) 
        block += self.figureLabel('SplittingTestP'+str(p))
        block +=self.endFigure()
        return block
    def SchemeComparisonTime(self,p,l,j,caption):
        figurecaption=caption+str(p)+'}'
        block =''
        block+=self.beginFigure()
        block+=self.beginSubfigure(0.3)
        block+=self.inputResizeBoxHöhe(self.sourceP,'DiffSchemesl'+str(l)+'j'+str(self.tL[0])+'P'+str(p),5)
        block+=self.subfigCaption('$j='+str(self.tL[0])+'$')
        block += self.endSubfigure()
        #block+='\n'
        block+=self.beginSubfigure(0.65)
        block+=self.inputResizeBoxHöhe(self.sourceP,'DiffSchemesl'+str(l)+'j'+str(j)+'P'+str(p),5)
        block+=self.subfigCaption('$j='+str(j)+'$')
        block += self.endSubfigure()
        block +=self.figureCaption(figurecaption) 
        block += self.figureLabel('SplittingTestP'+str(p))
        block +=self.endFigure()
        return block
    def ExtrapolateFigure(self,p):
        figurecaption='Extrapolates of the different time integration methods in \\textbf{P'+str(p)+'}'
        block =''
        block+=self.beginFigure()
        block+=self.inputResizeBox(self.sourceP,'ExtrapolatesP'+str(p))
        block +=self.figureCaption(figurecaption) 
        block += self.figureLabel('PlotExtrapolatesP'+str(p))
        block +=self.endFigure()
        return block
    
    def DiffExtrapolates(self,p,refNameL,L,J,sP):
        figurecaption = 'Extrapolates of '+ refNameL[0]+ '~ and '+ refNameL[1]+'~ and their difference (blue) in \\textbf{P'+str(p)+'}. Dotted are the corresponding $\\V^{3,4}(\\cdot,\\textbf{P'+str(p)+'})$'
        block=''
        block+=self.beginFigure()
        block+='\\centering\n'
        block+=self.inputResizeBox(sP,'DiffExtraLISIl'+str(L)+'j'+str(J)+'P'+str(p))
        block +=self.figureCaption(figurecaption) 
        block += self.figureLabel('PlotDiffExtrapolatesP'+str(p))
        block +=self.endFigure()
        return block
    
    def section(self,caption):
        return '\\section{'+caption+'}\n'
    def beginFigure(self):
        return '\\begin{figure}[H]\n'
    def endFigure(self):
        return '\\end{figure}\n'
    def figureCaption(self,caption):
        return'\caption{'+caption+'.}\n'
    def figureLabel(self,label):
        return '\label{fig:'+label+'}\n'
    def beginSubfigure(self,size=0.45):
        return '\\begin{subfigure}[r]{'+str(size)+'\\textwidth}\n'
    def endSubfigure(self):
        return '\\end{subfigure}\hfill\n'
    def subfigCaption(self,name):
        return '\\subcaption{'+name+'.}\n'
    def inputResizeBox(self,path,name,size=8):
        return'\\resizebox{'+str(size)+'cm}{!}{\\input{'+path +'tex/'+name+'}}\n'
    def inputResizeBoxHöhe(self,path,name,size=8):
        return'\\resizebox{!}{'+str(size)+'cm}{\\input{'+path +'tex/'+name+'}}\n'
    def input(self,path,name):
        return '\\input{'+path +'tex/'+name+'}\n'
        


