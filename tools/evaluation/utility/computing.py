from utility import reading as read
import math
from math import dist
from scipy.interpolate import RegularGridInterpolator
from scipy.interpolate import interp1d
from numpy import linspace, zeros, array
import matplotlib.pyplot as plt
import numpy as np

def getnumberOfNormSteps(T,start_dt):
    n=T/start_dt+1
    return int(n)
def L2TXT(l,j,m,p,dt,T,path):
    (time,V)=read.getDataFromTXT(path,read.createFilename(l,j,m),p)
    return L2(V,dt,T)
def MaxNorm(V,withabs=False):
    if withabs:
        max=0.0
        maxI=0
        for i in range(len(V)):
            if abs(float(V[i]))>max:
                #print(float(V[i]))
                max=abs(float(V[i]))
                #print(max)
                maxI=i
    else:
        max=0.0
        for i in range(len(V)):
            if float(V[i])>max:
                max=float(V[i])
                maxI=i
    print(len(V),maxI)
    return max

def getTimeID(id_tact,j,durEvalActi,dt):
        ID=-1
        n=int(durEvalActi/(dt*2**(-j)))
        ID=id_tact+n
        return ID
def evaluateRightLength(f,dt,T):
    NewF=[]
    numberOfNormSteps = getnumberOfNormSteps(T,dt)
    if len(f)>=numberOfNormSteps:
        k=int((len(f)-1)/(numberOfNormSteps-1))
        for i in range(numberOfNormSteps):
            NewF.append(f[k*i])
    else:
        print('operation really wanted? dt could be smaller')
        k=numberOfNormSteps/len(f)
        if k%1==0.0:
            NewF=f
        else:
            length=int((numberOfNormSteps-1)/math.ceil(k)) +1
            NewF=f[:length]      
    return NewF   
def L2(V,dt,T,j=0,resize=True):
    n = len(V)
    numberOfNormSteps = getnumberOfNormSteps(T,dt)
    k = int((n-1)/(numberOfNormSteps-1))
    if resize:
        if k ==1:
            sum = -0.5 * dt* (V[0]*V[0]);
            sum += -0.5 * dt * (V[-1]*V[-1]);
            for i in range(0,len(V)):
                sum += dt*(V[i]*V[i])
        else:
            evaluateV =[]
            for i in range(numberOfNormSteps):
                evaluateV.append(float(V[k*i]))
            sum = -0.5 * dt* (evaluateV[0]*evaluateV[0]);
            sum += -0.5 * dt * (evaluateV[-1]*evaluateV[-1]);
            for i in range(0,len(evaluateV)):
                sum += dt*(evaluateV[i]*evaluateV[i])
    else:
        dt=dt*2**(-j)
        sum = -0.5 * dt* (V[0]*V[0]);
        sum += -0.5 * dt * (V[-1]*V[-1]);
        for i in range(0,len(V)):
            sum += dt*(V[i]*V[i]) 
    return(math.sqrt(sum))#"%8.4f"%
    #return(1000*math.sqrt(sum))#"%8.4f"%

def L2Diff(path,l_f, j_f, m_f,l_ref,j_ref, m_ref,p,start_dt,T,factor=0):
    dt=start_dt*2**(-factor)
    (time_ref,reference) = read.getDataFromTXT(path,read.createFilename(l_ref,j_ref,m_ref)+case,p)
    (time_f,f)= read.getDataFromTXT(path,read.createFilename(l_f,j_f,m_f)+case,p)
    if len(time_ref)== 0 or len(time_f)== 0:
        print(l_ref,j_ref,path)
        return 'not finished'
    
    else:
        numberOfNormSteps = getnumberOfNormSteps(T,dt)
        #print(numberOfNormSteps)
        k_ref = int((len(reference)-1)/(numberOfNormSteps-1))
        k_f = int((len(f)-1)/(numberOfNormSteps-1))
        Diff =[]
        for i in range(numberOfNormSteps):
            Diff.append(float(f[k_f*i])-float(reference[k_ref*i]))
        norm =L2(Diff,dt,T)
        #print(len(Diff),len(f),len(reference))
        #norm =L2Simpson(Diff)
        return(norm)#"%8.4f"%
def L2DiffEx(path,l,j,m,V,p,start_dt,T):
    (time,f)= read.getDataFromTXT(path,read.createFilename(l,j,m),p)
    numberOfNormSteps = getnumberOfNormSteps(T,start_dt)
    k_f = int((len(f)-1)/(numberOfNormSteps-1))
    Diff =[]
    for i in range(numberOfNormSteps):
        Diff.append(float(f[k_f*i])-float(V[i]))
    norm =L2(Diff,start_dt,T)
    return(norm)

def L2DiffWithVaryingTimeStepSize(V,W,t_v,t_w):
    Diff=[]
    for i in range(len(t_w)):
        interpolatedV=np.interp(t_w[i], t_v, V)
        Diff.append(interpolatedV-W[i])
        
    
    dt=t_w[1]
    norm =L2(Diff,dt,1.0,0,False)
    return(norm)
    
    
def L2DiffInterval(V,W,dt,j_v,j_w,p=0):
    Diff=[]
    diff=abs(j_v-j_w)
    #print(j_v,j_w,len(V),len(W))
    maxvalue=0.0
    id_max=0
    if j_v==j_w:
        for i in range(len(W)):
            Diff.append(float(V[i])-float(W[i]))
            if maxvalue <abs(float(V[i])-float(W[i])):
                maxvalue=abs(float(V[i])-float(W[i]))
                id_max=i
    else: 
        if j_w>j_v:
            for i in range(len(V)):
                Diff.append(float(V[i])-float(W[diff*2*i]))
                if maxvalue <abs(float(V[i])-float(W[i])):
                    maxvalue=abs(float(V[i])-float(W[i]))
                    id_max=i
        else:
            for i in range(len(W)):
                Diff.append(float(V[diff*2*i])-float(W[i]))
                if maxvalue <abs(float(V[i])-float(W[i])):
                    maxvalue=abs(float(V[i])-float(W[i]))
                    id_max=i
    
    
    j=j_w
    if j_v<j_w:
        j=j_v
    #if p==8:
        #print(id_max,maxvalue,dt*2**(-j)*float(id_max))
    norm =L2(Diff,dt,1.0,j,False)
    return(norm)

def L2DiffPot(V,W,dt,T):
    Diff =[]
    if len(V)>len(W):
        k_V = int((len(V)-1)/(len(W)-1))
        dt=T/(len(W)-1)
        for i in range(len(W)):
            Diff.append(float(V[k_V*i])-float(W[i]))
    elif len(V)<len(W):
        k_W = int((len(W)-1)/(len(V)-1))
        dt=T/(len(V)-1)
        for i in range(len(V)):
            Diff.append(float(V[i])-float(W[k_W*i]))
    else:
        for i in range(len(V)):
            Diff.append(float(V[i])-float(W[i]))
    
    norm =L2(Diff,dt,T)
    return(norm)



###################################################################################
#Extrapolation für Zahl in R
###############################################################################

def getGTact(j,l,m,p,path,th,case):
    tact_j=read.getActivaitionTimeFromLog(l,j,m, path, p,th,False,case)
    tact_jMinusOne=read.getActivaitionTimeFromLog(l,j-1,m, path, p,th,False,case)
    tact_jMinusTwo=read.getActivaitionTimeFromLog(l,j-2,m, path, p,th,False,case)
    if tact_j-tact_jMinusOne==0:
        print(l,j,p)
        #(t,V)=read.getDataFromTXT(path,read.createFilename(l,j,m)+case,p)
        #(tact,id_tact)=getTactPerCase(t,V,th,case,False)
        #print(j,V[id_tact],t[id_tact-1])
        #(t,V)=read.getDataFromTXT(path,read.createFilename(l,j-1,m)+case,p)
        #(tact,id_tact)=getTactPerCase(t,V,th,case,False)
        #print(j-1,V[id_tact],t[id_tact-1])
    #g=0.0    
    #if tact_j-tact_jMinusOne==0:
        #print(l,j,p)
        #g=2.0
    #else:
    g=abs(tact_jMinusOne-tact_jMinusTwo)/abs(tact_j-tact_jMinusOne)
    
    return g
def getFTact(j,l,m,p,path,th,case):
    tact_l=read.getActivaitionTimeFromLog(l,j,m, path, p,th,False,case)
    tact_lMinusOne=read.getActivaitionTimeFromLog(l-1,j,m, path, p,th,False,case)
    tact_lMinusTwo=read.getActivaitionTimeFromLog(l-2,j,m, path, p,th,False,case)
    #print(tact_l,tact_lMinusOne,tact_lMinusTwo)
    if tact_l-tact_lMinusOne==0.0:
        print(p,j,l,path)
    f=abs(tact_lMinusOne-tact_lMinusTwo)/abs(tact_l-tact_lMinusOne)
    return f
def getTactExtrapolateInSpace(l,j,m,p,path,th,case):
    T_ex=-10.0
    f=getFTact(j,l,m,p,path,th,case)
    f1=f/(f-1)
    f2=1/(f-1)
    tact_l=read.getActivaitionTimeFromLog(l,j,m, path, p,th,False,case)
    tact_lMinusOne=read.getActivaitionTimeFromLog(l-1,j,m, path, p,th,False,case)
    T_ex=f1*tact_l-f2*tact_lMinusOne
    return T_ex
def getTactExtrapolateInTime(l,j,m,p,path,th,case):
    T_ex=-10.0
    g=getGTact(j,l,m,p,path,th,case)
    g1=g/(g-1)
    g2=1/(g-1)
    tact_j=read.getActivaitionTimeFromLog(l,j,m, path, p,th,False,case)
    tact_jMinusOne=read.getActivaitionTimeFromLog(l,j-1,m, path, p,th,False,case)
    T_ex=g1*tact_j-g2*tact_jMinusOne
    return T_ex   
def getTextFullExtrapolate(L,J,m,p,path,th,case=''):
    T_ex=-10.0
    tact_JEx = getTactExtrapolateInSpace(L,J,m,p,path,th,case)
    tact_JMinusOneEx = getTactExtrapolateInSpace(L,J-1,m,p,path,th,case)

    g1 =(getGTact(J,L,m,p,path,th,case)/(getGTact(J,L,m,p,path,th,case)-1))
    g2 = (1)/(getGTact(J,L,m,p,path,th,case)-1)

    T_ex=g1*tact_JEx - g2*tact_JMinusOneEx
    return T_ex 







###################################################################################
#in L_2(tact,Tact)-Norm
###############################################################################

def getFInt(l,j,m,p,path,dt,threshold,durEvalActi,case):
    (t,V_l)=read.getDataFromTXT(path,read.createFilename(l,j,m)+case,p)
    (t,V_lMinusOne)=read.getDataFromTXT(path,read.createFilename(l-1,j,m)+case,p)
    (t,V_lMinusTwo)=read.getDataFromTXT(path,read.createFilename(l-2,j,m)+case,p)
    
    (tact,id_tactl)=getTactPerCase(t,V_l,threshold,case,True)#getActivaitionTimeFromVWithID(t,V_l,threshold,True)
    (tact,id_tactlMinusOne)=getTactPerCase(t,V_lMinusOne,threshold,case,True)#getActivaitionTimeFromVWithID(t,V_lMinusOne,threshold,True)
    (tact,id_tactlMinusTwo)=getTactPerCase(t,V_lMinusTwo,threshold,case,True)#getActivaitionTimeFromVWithID(t,V_lMinusTwo,threshold,True)
    
    
    V_l=V_l[id_tactl:getTimeID(id_tactl,j,durEvalActi,dt)]
    V_lMinusOne=V_lMinusOne[id_tactlMinusOne:getTimeID(id_tactlMinusOne,j,durEvalActi,dt)]
    V_lMinusTwo=V_lMinusTwo[id_tactlMinusTwo:getTimeID(id_tactlMinusTwo,j,durEvalActi,dt)]
    
    f1=L2DiffInterval(V_lMinusOne,V_lMinusTwo,dt,j,j)
    f2=L2DiffInterval(V_l,V_lMinusOne,dt,j,j)
    
    
    return f1/f2
def getGInt(l,j,m,p,path,dt,threshold,durEvalActi,case):
    (t,V_j)=read.getDataFromTXT(path,read.createFilename(l,j,m)+case,p)
    (t,V_jMinusOne)=read.getDataFromTXT(path,read.createFilename(l,j-1,m)+case,p)
    (t,V_jMinusTwo)=read.getDataFromTXT(path,read.createFilename(l,j-2,m)+case,p)
    
    (tact,id_tactj)=getTactPerCase(t,V_j,threshold,case,True)#getActivaitionTimeFromVWithID(t,V_j,threshold,True)
    (tact,id_tactjMinusOne)=getTactPerCase(t,V_jMinusOne,threshold,case,True)#getActivaitionTimeFromVWithID(t,V_jMinusOne,threshold,True)
    (tact,id_tactjMinusTwo)=getTactPerCase(t,V_jMinusTwo,threshold,case,True)#getActivaitionTimeFromVWithID(t,V_jMinusTwo,threshold,True)
    
    V_j=V_j[id_tactj:getTimeID(id_tactj,j,durEvalActi,dt)]
    V_jMinusOne=V_jMinusOne[id_tactjMinusOne:getTimeID(id_tactjMinusOne,j-1,durEvalActi,dt)]
    V_jMinusTwo=V_jMinusTwo[id_tactjMinusTwo:getTimeID(id_tactjMinusTwo,j-2,durEvalActi,dt)]
    
    g1=L2DiffInterval(V_jMinusOne,V_jMinusTwo,dt,j-1,j-2)
    g2=L2DiffInterval(V_j,V_jMinusOne,dt,j,j-1)
    
    return g1/g2

def getVIntExtrapolateInSpace(l,j,m,p,path,dt,threshold,durEvalActi,tact_ref,case='') :
    V_space=[]
    fv=getFInt(l,j,m,p,path,dt,threshold,durEvalActi,case)
    #fv=2
    f1=fv/(fv-1)
    f2=1/(fv-1)
    (t_l,V_l)=read.getDataFromTXT(path,read.createFilename(l,j,m)+case,p)
    (t,V_lMinusOne)=read.getDataFromTXT(path,read.createFilename(l-1,j,m)+case,p)
    
    (tact,id_tactl)=getTactPerCase(t_l,V_l,threshold,case,True)#getActivaitionTimeFromVWithID(t_l,V_l,threshold,True)
    (tact2,id_tactlMinusOne)=getTactPerCase(t,V_lMinusOne,threshold,case,True)#getActivaitionTimeFromVWithID(t,V_lMinusOne,threshold,True)
    
    V_l=V_l[id_tactl:getTimeID(id_tactl,j,durEvalActi,dt)]
    V_lMinusOne=V_lMinusOne[id_tactlMinusOne:getTimeID(id_tactlMinusOne,j,durEvalActi,dt)]
    
    
    t=t[id_tactlMinusOne:getTimeID(id_tactlMinusOne,j,durEvalActi,dt)]
    #if p==8:
        #plt.plot(t_l[id_tactl:getTimeID(id_tactl,j,durEvalActi,dt)],V_l,label='l='+str(l))
        #plt.plot(t,V_lMinusOne,label='l='+str(l-1))
    for i in range(len(V_l)):
        V_space.append(f1*V_l[i]-f2*V_lMinusOne[i])
        t[i]+=tact_ref-tact2
    #if p==8:
        #plt.plot(t,V_space,label='VspaceExtra')

    #if p==8:
        #plt.title('j='+str(j))
        #plt.legend()
        #plt.show()
    #if j==2 and p==8:
        #print( ' in extrapolate Space ',fv,f1,f2)

    return t,V_space

def getVIntExtrapolateInTime(l,j,m,p,path,dt,threshold,durEvalActi,tact_ref,case=''):
    V_time=[]
    gv=getGInt(l,j,m,p,path,dt,threshold,durEvalActi,case)
    #gv=2
    g1=gv/(gv-1)
    g2=1/(gv-1)
    
    (t_j,V_j)=read.getDataFromTXT(path,read.createFilename(l,j,m)+case,p)
    (t_jMinusOne,V_jMinusOne)=read.getDataFromTXT(path,read.createFilename(l,j-1,m)+case,p)
    
    
    (tact,id_tactj)=getTactPerCase(t_j,V_j,threshold,case,True) #getActivaitionTimeFromVWithID(t_j,V_j,threshold,True)
    (tact2,id_tactjMinusOne)=getTactPerCase(t_jMinusOne,V_jMinusOne,threshold,case,True) #getActivaitionTimeFromVWithID(t_jMinusOne,V_jMinusOne,threshold,True)
    
    V_j=V_j[id_tactj:getTimeID(id_tactj,j,durEvalActi,dt)]
    V_jMinusOne=V_jMinusOne[id_tactjMinusOne:getTimeID(id_tactjMinusOne,j-1,durEvalActi,dt)]
    t_jMinusOne=t_jMinusOne[id_tactjMinusOne:getTimeID(id_tactjMinusOne,j-1,durEvalActi,dt)]
    for i in range(len(V_jMinusOne)):
        V_time.append(g1*V_j[2*i]-g2*V_jMinusOne[i])
        t_jMinusOne[i]+=tact_ref-tact2
        
    return t_jMinusOne,V_time

def getVIntFullExtrapolate(l,j,m,p,path,dt,threshold,durEvalActi,tact_ref,case=''):
    V_full=[]
    (t,V_Jex)=getVIntExtrapolateInSpace(l,j,m,p,path,dt,threshold,durEvalActi,tact_ref,case)
    (t_t,V_JexMinusOne)=getVIntExtrapolateInSpace(l,j-1,m,p,path,dt,threshold,durEvalActi,tact_ref,case)
    gv=getGInt(l,j,m,p,path,dt,threshold,durEvalActi,case)
    g1=gv/(gv-1)
    g2=1/(gv-1)

    for i in range(len(V_JexMinusOne)):
        V_full.append(g1*V_Jex[2*i]-g2*V_JexMinusOne[i])
    #if p==8:
        #print(V_full[0],V_Jex[0],V_JexMinusOne[0])
    
    return (t_t,V_full)



################################################################
#in L_2(t=0,T)-Norm
#############################################################

def getF(j,l,m,p,dt,T,path,case):
    (t1,V1)=read.getDataFromTXT(path,read.createFilename(l,j,m)+case,p)
    (t2,V2)=read.getDataFromTXT(path,read.createFilename(l-1,j,m)+case,p)
    (t3,V3)=read.getDataFromTXT(path,read.createFilename(l-2,j,m)+case,p)
    #f = float(L2Diff(path,l-1,j,m,l-2,j,m,p,dt,T,j))/float(L2Diff(path,l,j,m,l-1,j,m,p,dt,T,j))
    #return f
    f1=L2DiffInterval(V2,V3,dt,j,j)
    f2=L2DiffInterval(V1,V2,dt,j,j)
    
    return f1/f2

def getG(j,l,m,p,dt,T,path,case):
    (t1,V1)=read.getDataFromTXT(path,read.createFilename(l,j,m)+case,p)
    (t2,V2)=read.getDataFromTXT(path,read.createFilename(l,j-1,m)+case,p)
    (t3,V3)=read.getDataFromTXT(path,read.createFilename(l,j-2,m)+case,p)
    #g = float(L2Diff(path,l,j-1,m,l,j-2,m,p,dt,T))/float(L2Diff(path,l,j,m,l,j-1,m,p,dt,T))
    #return g
    g1=L2DiffInterval(V2,V3,dt,j-1,j-2)
    g2=L2DiffInterval(V1,V2,dt,j,j-1)
    
    return g1/g2

def getExtrapolateInSpace(l,j,m,p,dt,T,path,case=''):
    fv=getF(j,l,m,p,dt,T,path,case)
    fv=2
    f1 =fv/(fv-1)
    f2 = (1)/(fv-1)
    #if p==8:
       # print(f1,f2)
    (t1,VjL)=read.getDataFromTXT(path,read.createFilename(l,j,m)+case,p)
    (t2,VjLOld) = read.getDataFromTXT(path,read.createFilename(l-1,j,m)+case,p)
    #VjL=resizeList(T,dt,VjL)
    #VjLOld=resizeList(T,dt,VjLOld)
    VjEx=[]
    for index in range(len(VjL)):
        VjEx.append(f1*VjL[index] - f2*VjLOld[index])
        #if p==8:
            #print(t1[index],VjL[index],VjLOld[index],f1*VjL[index] - f2*VjLOld[index])
            
    #print("l ",l," j ",j," len(Vjex) ",len(VjEx))
    #if p==8:
        #plt.plot(t1,VjL,label='VjL')
        #plt.plot(t2,VjLOld,label='VjLOld')
        #plt.plot(t2,VjEx,label='VjEx')
    
        #plt.title('P'+str(p))
    
       # plt.legend()
       # plt.show()
    return VjEx

def getExtrapolateInTime(l,j,m,p,dt,T,path,case=''):
    gv=getG(j,l,m,p,dt,T,path,case)
    g1 =gv/(gv-1)
    g2 = (1)/(gv-1)
    
    (t1,VJl)=read.getDataFromTXT(path,read.createFilename(l,j,m)+case,p)
    (t2,VJlOld) = read.getDataFromTXT(path,read.createFilename(l,j-1,m)+case,p)
    #VJl=resizeList(T,dt,VJl)
    #VJlOld=resizeList(T,dt,VJlOld)
    VExl=[]
    for index in range(len(VJlOld)):
        VExl.append(g1*VJl[2*index] - g2*VJlOld[index])
    return VExl

def getFullExtrapolate(L,J,m,p,dt,T,path,case=''):
    VJEx = getExtrapolateInSpace(L,J,m,p,dt,T,path,case)
    VJMinusOneEx = getExtrapolateInSpace(L,J-1,m,p,dt,T,path,case)
    gv=getG(J,L,m,p,dt,T,path,case)
    g1=gv/(gv-1)
    g2= (1)/(gv-1)
    
    #VJEx=resizeList(T,dt,VJEx)
    #VJMinusOneEx=resizeList(T,dt,VJMinusOneEx)
    
    VEx=[]
    #print(len(VJEx),len(VJMinusOneEx))
    for index in range(len(VJMinusOneEx)):
        VEx.append(g1*VJEx[2*index] - g2*VJMinusOneEx[index])
    
    #if p==8:
        #t_Jex=np.linspace(dt*2**(-(J)), T, num=len(VJEx))
        #t_ex=np.linspace(dt*2**(-(J-1)), T, num=len(VEx))
       # plt.plot(t_Jex,VJEx,label='VJEx')
       # plt.plot(t_ex,VJMinusOneEx,label='VJMinusOneEx')
        #plt.plot(t_ex,VEx,label='VEx')
    
        #plt.title('P'+str(p))
    
        #plt.legend()
        #plt.show()
    return VEx

############################################
#komisches Extrapolate
############################################

def getExtrapolateInSpaceDefinedOrder(order,j,L,p,m,path):
    (t1,VjL)=read.getDataFromTXT(path,read.createFilename(L,j,m),p)
    (t2,VjLOld) = read.getDataFromTXT(path,read.createFilename(L-1,j,m),p)
    VjEx =[]
    for index in range(len(VjL)):
        VjEx.append((2**order/(2**order-1))*VjL[index] - (1/(2**order-1))*VjLOld[index])
    return VjEx

def getExtrapolateInTimeDefinedOrder(order,l,J,p,m,path):
    (t1,Vj)=read.getDataFromTXT(path,read.createFilename(l,J,m),p)
    (t2,VjOld) = read.getDataFromTXT(path,read.createFilename(l,J-1,m),p)
    Vj=resizeVbyW(Vj,VjOld)
    VjEx =[]
    for index in range(len(Vj)):
        VjEx.append((2**order/(2**order-1))*Vj[index] - (1/(2**order-1))*VjOld[index])
    return VjEx
def getFullExtrapolateDefinedOrder(order_t,order_s,J,L,p,m,path):
    VLEx = getExtrapolateInSpaceDefinedOrder(order_s,J,L,p,m,path)
    VLExJMinusOne = getExtrapolateInSpaceDefinedOrder(order_s,J-1,L,p,m,path)
    VLEx =resizeVbyW(VLEx,VLExJMinusOne)
    VEx=[]
    for index in range(len(VLEx)):
        VEx.append((2**order_t/(2**order_t-1))*VLEx[index] - (1/(2**order_t-1))*VLExJMinusOne[index])
    
    return VEx

def getSpaceExtraActivation(l,j,path,p):
    f=abs(read.getActivationTime(path,l-1,j,p)-read.getActivationTime(path,l-2,j,p))/abs(read.getActivationTime(path,l,j,p)-read.getActivationTime(path,l-1,j,p))
    f1 =f/(f-1)
    f2 = (1)/(f-1)
    
    acti=read.getActivationTime(path,l,j,p)
    acti_old = read.getActivationTime(path,l-1,j,p)
    extra= f1*acti- f2*acti_old
    return extra
def getTimeExtraActivation(l,j,path,p):
    g=abs(read.getActivationTime(path,l,j-1,p)-read.getActivationTime(path,l,j-2,p))/abs(read.getActivationTime(path,l,j,p)-read.getActivationTime(path,l,j-1,p))
    f1 =g/(g-1)

    f2 = (1)/(g-1)
    
    acti=read.getActivationTime(path,l,j,p)
    acti_old = read.getActivationTime(path,l,j-1,p)
    extra= f1*acti- f2*acti_old
    return extra
def getFullExtraActivation(L,J,path,p):
    acti_space = getSpaceExtraActivation(L,J,path,p)
    acti_space_jminusone = getSpaceExtraActivation(L,J-1,path,p)
    
    g=abs(read.getActivationTime(path,l,j-1,p)-read.getActivationTime(path,l,j-2,p))/abs(read.getActivationTime(path,l,j,p)-read.getActivationTime(path,l,j-1,p))
    g1 =g/(g-1)
    g2 = (1)/(g-1)
    extra=g1*acti_space - g2*acti_space_jminusone
    return extra

def getTimeRMSerrorArray(lL,tL,path):
    (evalPoints,activation)=read.getActivationData(path,read.createActiFilename(lL[0],tL[0]))
    n=len(evalPoints)
    arrayOfErrors=[]
    for l in lL:
        listOfTimeErrors=[]
        for j in tL:
            listOfTimeErrors.append(getTimeRMSerrorFromTXT(l,j,path,n))
        arrayOfErrors.append(listOfTimeErrors)
    return arrayOfErrors

def getSpaceRMSerrorArray(lL,tL,path):
    (evalPoints,activation)=read.getActivationData(path,read.createActiFilename(lL[0],tL[0]))
    n=len(evalPoints)
    arrayOfErrors=[]
    for j in tL:
        listOfTimeErrors=[]
        for l in lL:
            listOfTimeErrors.append(getSpaceRMSerrorFromTXT(l,j,path,n))
        arrayOfErrors.append(listOfTimeErrors)
    return arrayOfErrors 
def getSpaceRMSerrorFromTXT(l,j,path,n):
    print(l,j)
    (evalPoints,activation)=read.getActivationData(path,read.createActiFilename(0,j))
    (points_l,activation_l)=read.getActivationData(path,read.createActiFilename(l,j))
    (points_lPlusOne,activation_lPlusOne)=read.getActivationData(path,read.createActiFilename(l+1,j))
    dict1={}
    dict2={}
    for index in range(len(points_l)):
        p=str(points_l[index][0])+', '+str(points_l[index][1])+', '+str(points_l[index][2])
        dict1[p]=activation_l[index]

    for index in range(len(points_lPlusOne)):
        p=str(points_lPlusOne[index][0])+', '+str(points_lPlusOne[index][1])+', '+str(points_lPlusOne[index][2])
        dict2[p]=activation_lPlusOne[index]
    
    sumDiffSquares=0.0
    for p in evalPoints:
        key=str(p[0])+', '+str(p[1])+', '+str(p[2])
        diff=abs(dict1[key]-dict2[key])
        sumDiffSquares+=diff*diff
    rmse=math.sqrt((1/len(evalPoints))*sumDiffSquares)
    return rmse
def getDiagonalPointList(EP):
    length=[]
    diagpoints=[]
    dx=5
    diagonalLength = math.sqrt(EP[0]*EP[0] + EP[1]*EP[1] + EP[2]*EP[2])
    dxValue = float('0.'+ str(dx))
    numberOfDiagSteps =  int(diagonalLength/dxValue)+1
    diagSize = float(1.0/(numberOfDiagSteps))
    for i in range(0,(numberOfDiagSteps+1)):
        diagpoints.append([i*diagSize *EP[0],i*diagSize *EP[1],i*diagSize *EP[2]])
        length.append(i*diagSize*diagonalLength)
    return (length,diagpoints)
def getDiagActivViaPython(path,l,j):
    EP=[20.0, 7.0, 3.0]
    (points,actiTime)=read.getActivationData(path,read.createActiFilename(l,j))
    #print(l,j,len(points),len(actiTime))
    dict={}
    dx =10000000
    for index in range(len(points)):
        p="{:.4f}".format(points[index][0])+', '+"{:.4f}".format(points[index][1])+', '+"{:.4f}".format(points[index][2])
        dict[p]=actiTime[index]
        if points[index][0]!=0.0 and points[index][0]<dx:
            dx=points[index][0]
    
    #geht nur bei Halbierung...
    #nx=int((EP[0]*2**(l+1))+1)
    #ny=int((EP[1]*2**(l+1))+1)
    #nz=int((EP[2]*2**(l+1))+1)
    
    nx=int((EP[0]/dx)+1)
    ny=int((EP[1]/dx)+1)
    nz=int((EP[2]/dx)+1)

    x = linspace(0,EP[0],nx)
    y = linspace(0,EP[1],ny)
    z = linspace(0,EP[2],nz)
    V = zeros((len(x), len(y), len(z)))
    for i in range(len(x)):
        for j in range(len(y)):
            for k in range(len(z)):
                P = key = str(format(x[i], '.4f')) + ', ' + str(format(y[j], '.4f')) + ', ' + str(format(z[k], '.4f'))
                #print(P)
                V[i, j, k] = dict[P]
    fn = RegularGridInterpolator((x, y, z), V)
    (length, diagps) = getDiagonalPointList(EP)
    pts = array(diagps)
    return (length, fn(pts))


def getActivationForPointsForBenchmark(path, l, j, evaluationPoints):
    (points, actiTime) = read.getActivationData(path, read.createActiFilename(l, j))
    dict = {}
    for index in range(len(points)):
        p = str(points[index][0]) + ', ' + str(points[index][1]) + ', ' + str(points[index][2])
        dict[p] = actiTime[index]

    evalPointsList = []
    if len(evaluationPoints) == 3:
        activation_times = 0
        p_test = str(evaluationPoints[0]) + ', ' + str(evaluationPoints[1]) + ', ' + str(evaluationPoints[2])
        evalPointsList.append(p_test)
        for j in range(len(evalPointsList)):
            if evalPointsList[j] in dict.keys():
                activation_times += dict.get(evalPointsList[j])
    elif len(evaluationPoints) > 3:
        activation_times = []
        for i in range(len(evaluationPoints)):
            p_test = str(evaluationPoints[i][0]) + ', ' + str(evaluationPoints[i][1]) + ', ' + str(
                evaluationPoints[i][2])
            evalPointsList.append(p_test)
        for j in range(len(evalPointsList)):
            if evalPointsList[j] in dict.keys():
                activation_times.append(dict.get(evalPointsList[j]))
    return (evaluationPoints, activation_times)


def getActivationOnDiagonalForBenchmark(path, l, j):
    EP = [20.0, 7.0, 3.0]
    dx = 5
    (points, actiTime) = read.getActivationData(path, read.createActiFilename(l, j))
    actiDiag = []
    space = []

    diagonalLength = math.sqrt(EP[0] * EP[0] + EP[1] * EP[1] + EP[2] * EP[2])
    dxValue = float('0.' + str(dx))
    numberOfDiagSteps =  int(diagonalLength/dxValue)+1
    diagSize = float(1.0/(numberOfDiagSteps))
    IDEP = -1
    pointlist=[]
    
    
    for i in range(0,(numberOfDiagSteps-1)):
        P = [i*diagSize *EP[0],i*diagSize *EP[1],i*diagSize *EP[2]]
        PDist = 1000000000
        PID=-1
        ID_counter =0
        for j in points:
            PSum =0.0
            for k in range(3):
                PSum += (j[k]-P[k])*(j[k]-P[k])
            
            PSumSqrt =math.sqrt(PSum)
            if PSumSqrt<PDist:
                PDist = PSumSqrt
                PID =ID_counter
            if i==0:
                if j[0]==EP[0] and j[1]==EP[1] and j[2]==EP[2]:
                    IDEP = ID_counter
            ID_counter+=1
        pointlist.append( points[PID])
        actiDiag.append(actiTime[PID])
        space.append(i*diagSize*diagonalLength)
        
    #Damit P8 mit drin ist
    pointlist.append(points[IDEP])
    actiDiag.append(actiTime[IDEP])
    space.append(diagonalLength)
    return (space,actiDiag)
def findDiagPoints(EP,points):
    AP=[0.0, 0.0, 0.0]
    diagP = []
    diagP.append(AP)
    for p in points:
        alpha = 0.0
        alpha = p[0]/EP[0]
        if alpha == p[1]/EP[1] and alpha ==p[2]/EP[2]:
            diagP.append(p)
            print (p)
    return diagP
def getTimeRMSerrorFromTXT(l,j,path,n):
    print(l,j)
    (evalPoints,activation)=read.getActivationData(path,read.createActiFilename(0,j))
    (points_l,activation_l)=read.getActivationData(path,read.createActiFilename(l,j))
    (points_jPlusOne,activation_jPlusOne)=read.getActivationData(path,read.createActiFilename(l,j+1))
    sumDiffSquares=0.0
    dict1={}
    dict2={}
    for index in range(len(points_l)):
        p=str(points_l[index][0])+', '+str(points_l[index][1])+', '+str(points_l[index][2])
        dict1[p]=activation_l[index]

    for index in range(len(points_lPlusOne)):
        p=str(points_jPlusOne[index][0])+', '+str(points_jPlusOne[index][1])+', '+str(points_jPlusOne[index][2])
        dict2[p]=activation_jPlusOne[index]
    
    sumDiffSquares=0.0
    for p in evalPoints:
        key=str(p[0])+', '+str(p[1])+', '+str(p[2])
        diff=abs(dict1[key]-dict2[key])
        sumDiffSquares+=diff*diff
    rmse=math.sqrt((1/len(evalPoints))*sumDiffSquares)
    return rmse
def getRMSerror(l_0,l,j,path):
    (evalPoints,activation)=read.getActivationData(path,read.createActiFilename(l_0,j))
    (points_l,activation_l)=read.getActivationData(path,read.createActiFilename(l,j))
    (points_lPlusOne,activation_lPlusOne)=read.getActivationData(path,read.createActiFilename(l+1,j))
    sumDiffSquares=0.0
    dict1={}
    dict2={}
    for index in range(len(points_l)):
        p=str(points_l[index][0])+', '+str(points_l[index][1])+', '+str(points_l[index][2])
        dict1[p]=activation_l[index]

    for index in range(len(points_lPlusOne)):
        p=str(points_lPlusOne[index][0])+', '+str(points_lPlusOne[index][1])+', '+str(points_lPlusOne[index][2])
        dict2[p]=activation_lPlusOne[index]
    
    sumDiffSquares=0.0
    for p in evalPoints:
        key=str(p[0])+', '+str(p[1])+', '+str(p[2])
        diff=abs(dict1[key]-dict2[key])
        sumDiffSquares+=diff*diff
    rmse=math.sqrt((1/len(evalPoints))*sumDiffSquares)
    return rmse
def getID(x,pL):
    index=-1
    for i in range(len(pL)):
        if pL[i][0]==x[0] and pL[i][1]==x[1] and pL[i][2]==x[2]:
            index=i
            break
    if index ==-1:
        print('id was -1')
        eps=1*10**(-6)
        for i in range(len(pL)):
            diff_0=abs(pL[i][0]-x[0])
            diff_1=abs(pL[i][1]-x[1])
            diff_2=abs(pL[i][2]-x[2])
            if diff_0<eps and diff_1<eps and diff_2<eps:
                index=i
                break
    return index
    
def computeCV(path,l,j,x,y):
    (points,activation)=read.getActivationData(path,read.createActiFilename(l,j))
    
    xdiff =100.0
    ydiff=100.0
    id_x=-1
    id_y =-1
    for i in range(len(points)):
        if points[i][0]==x[0] and points[i][1]==x[1] and points[i][2]==x[2]:
            id_x=i
            break
        if points[i][0]==y[0] and points[i][1]==y[1] and points[i][2]==y[2]:
            id_y=i
            break
    if id_x==-1:
        eps=1*10**(-6)
        for i in range(len(points)):
            diff_0=abs(points[i][0]-x[0])
            diff_1=abs(points[i][1]-x[1])
            diff_2=abs(points[i][2]-x[2])
            if diff_0<eps and diff_1<eps and diff_2<eps:
                id_x=i
                break
    if id_y==-1:
        eps=1*10**(-6)
        for i in range(len(points)):
            diff_0=abs(points[i][0]-y[0])
            diff_1=abs(points[i][1]-y[1])
            diff_2=abs(points[i][2]-y[2])
            if diff_0<eps and diff_1<eps and diff_2<eps:
                id_y=i
                break
    cv=(dist(x, y))/(abs(activation[id_x]-activation[id_y]))
    return cv
def RMSRefError(refDict,path,pathLevel0):
    points, at = read.readVTU(path)
    if len(points)==0:
        return 0.0
    evalPoints,ateP=read.readVTU(pathLevel0)
    
    dict1={}
    for index in range(len(points)):
        p=str(points[index][0])+', '+str(points[index][1])+', '+str(points[index][2])
        dict1[p]=at[index]
    
    sumDiffSquares=0.0
    for p in evalPoints:
        key=str(p[0])+', '+str(p[1])+', '+str(p[2])
        if dict1[key]==-1:
            return -1.0
        diff=abs(dict1[key]-refDict[key])
        sumDiffSquares+=diff*diff
    rmse=math.sqrt((1/len(evalPoints))*sumDiffSquares)
    return rmse
    
def computeMeanTact(p,pathLevel0):
    evalPoints,ateP=read.readVTU(pathLevel0)
    points, at = read.readVTU(p)
    if len(points)==0:
        return 0.0
    
    dict={}
    for index in range(len(points)):
        p=str(points[index][0])+', '+str(points[index][1])+', '+str(points[index][2])
        dict[p]=at[index]
    Sum=0.0
    for p in evalPoints:
        key=str(p[0])+', '+str(p[1])+', '+str(p[2])
        value=dict[key]
        if value==-1:
            print("in value=-1")
            return -1.0
        else:
            Sum+=value*value

    tact=math.sqrt((1/(len(evalPoints)))*Sum)
    print('in compute',tact)
    return tact
def getTactPerCase(t,V,th,case,withNearest=False):
    if case=='Ca' or case=='':
        return getActivaitionTimeFromVWithID(t,V,th,withNearest)
    elif case=='Gamma' or case=='Volumes':
        return getActivaitionTimeFromGammaWithID(t,V,th,withNearest)
    else:
        print('comp.getTactPerCase: case not defined')
    
def getActivaitionTimeFromVWithID(t,V,th,withNearest=False):
    value=-10.0 
    id_v=0
    for i in range(len(V)):
        if V[i]>=th:
            id_v=i
            break
    Y = [t[id_v-1],t[id_v]] 
    X = [abs(V[id_v-1]),abs(V[id_v])]
    y_interp = interp1d(X, Y)
    #print(X,Y,th)
    value=y_interp(abs(th))
    if withNearest:
        diff=abs(V[id_v]-th)
        diff2=abs(V[id_v-1]-th)
        if diff2<diff:
            id_v=id_v-1
    #value=t[id_v]
    numberOfStepsBeforeTact=int(0.005/float(t[1]))
    #if id_v-numberOfStepsBeforeTact>=0:
        #id_v-=numberOfStepsBeforeTact
    #else:
       # print('not possible to look at the 5ms before activaiton time')
    return value,id_v
def getActivaitionTimeFromV(t,V,th,withNearest=False):
    value,id_v=getActivaitionTimeFromVWithID(t,V,th,withNearest)
    return value
def getActivaitionTimeFromGammaWithID(t,V,th,withNearest=False):
    value=-10.0 
    id_v=0
    for i in range(len(V)):
        if V[i]<=th:
            id_v=i
            break
    if withNearest:
        diff=abs(V[id_v]-th)
        diff2=abs(V[id_v-1]-th)
        if diff2<diff:
            id_v=id_v-1
    value=t[id_v]
    return value,id_v
def getActivaitionTimeFromGamma(t,V,th,withNearest=False):
    value,id_v=getActivaitionTimeFromGammaWithID(t,V,th,withNearest)
    return value
def getAPDFromV(t,V,th):
    t_act=-10.0
    id_act=0
    t_dur=0.0
    apd=0.0
    for i in range(len(V)):
        if V[i]>=th:
            t_act=t[i]
            id_act=i
            break
    for i in range(id_act+1,len(V)):
        if float(V[i])<=th:
            t_dur=t[i]
            break
    apd=t_dur-t_act
    return apd
def getAPDFromVWithID(t,V,th):
    t_act=-10.0
    id_act=0
    t_dur=0.0
    apd=0.0
    for i in range(len(V)):
        if V[i]>=th:
            t_act=t[i]
            id_act=i
            break
    for i in range(id_act+1,len(V)):
        if float(V[i])<=th:
            t_dur=t[i]
            break
    apd=t_dur-t_act
    return apd,i

def resizeList(T,dt,V):
    n=getnumberOfNormSteps(T,dt)
    k =int((len(V)-1)/(n-1))
    V_resized=[]
    for i in range(n):
        V_resized.append(V[k*i])
    return V_resized
def resizeVbyW(V,W):
    shortV=[]
    k = int((len(V)-1)/(len(W)-1))
    for i in range(len(W)):
        shortV.append(V[k*i])
    return shortV
