from collections import OrderedDict
import re
from vtk.util.numpy_support import vtk_to_numpy
from vtk.vtkIOXML import vtkXMLUnstructuredGridReader
from utility import computing as comp


def getProcsAndDur(path,l,j,m):
    fn = createFilename(l,j,m)
    procs=0
    dur=0
    try:
        f = open(path+ 'log/'+"log_"+fn)
        l=f.readlines()[0]
        line=l.split(" ")
        procs=line[3]
        f = open(path+ 'log/'+"log_"+fn)
        for l in f:
            l = l.strip()
            #m=re.search("Finished Calculations after 1:06:44.00 hours seconds",l)
            m=re.search("Finished Calculations after\s*([\d]*\:{0,1}[\d]*\:{0,1}[\d]+\.[\d]*)\s*",l)
            if m:
                dur=m.group(1)
        f.close()
    except FileNotFoundError:
        try:
            f = open(path+"log_"+fn)
            l=f.readlines()[0]
            line=l.split(" ")
            procs=line[3]
            f = open(path+"log_"+fn)
            for l in f:
                l = l.strip()
                m=re.search("Finished Calculations after\s*([\d]*\:{0,1}[\d]*\:{0,1}[\d]+\.[\d]*)\s*",l)
                if m:
                    dur=m.group(1)
            f.close()
        except FileNotFoundError:
            procs=0
            dur=0
    return(procs,dur)
def getDuration(filename):
    dur=-1.0
    sec=-1.0
    minutes=-1.0
    f = open(filename)
    for l in f:
        l = l.strip()
        m=re.search("Finished Calculations after\s*([\d]*\:{0,1}[\d]*\:{0,1}[\d]+\.[\d]*)\s*",l)
        if m:
            dur=m.group(1)
    f.close()
    if len(dur)<=5:
        sec=float(dur)
    elif len(dur)==7:
        sec=float(dur[-5:])
        minutes=float(dur[0])
        sec+=60*minutes
    elif len(dur)==8:
        sec=float(dur[-5:])
        minutes=float(dur[0]+dur[1])
        sec+=60*minutes
    else:
        print('not defined yet')
    return sec
def getAdditonofTimeSteps(filename):
    value=0.0
    f = open(filename)
    for l in f:
        l = l.strip()
        m=re.search("Step Time:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*seconds",l)
        if m:
            value+=float(m.group(1))
    f.close()
    return value
def getSpecialisedTimeDurationsAveraged(filename):
    gating=0.0
    conc=0.0
    pde=0.0
    pdeList=[]
    counter=0.0
    f = open(filename)
    for l in f:
        l = l.strip()
        m=re.search("Solve Gating:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*seconds",l)
        if m:
            gating+=float(m.group(1))
            counter+=1.0
        m=re.search("Solve Concentration:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*seconds",l)
        if m:
            conc+=float(m.group(1))
        m=re.search("Solve PDE:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*seconds",l)
        if m:
            pdeList.append(float(m.group(1)))
    f.close()
    
    gating =gating/counter
    conc=conc/counter
    for i in range(1,len(pdeList)):
        pde+=pdeList[i]
    pde=pde/(counter-1)
    return gating,conc,pde
            
def getAverageTimeStep(filename):
    value=0.0
    values=[]
    counter=0.0
    f = open(filename)
    for l in f:
        l = l.strip()
        m=re.search("Step Time:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*seconds",l)
        if m:
            values.append(float(m.group(1)))
            counter+=1.0
    f.close()
    for v in values[1:]:
        value+=v
    return values[0],value/(counter-1)
def getActivationTime(path,l,j,x):
    if isinstance(x,int):
        activation =-10.0
        (t,V)=getDataFromTXT(path,createFilename(l,j,1),x)
        for i in range(len(V)):
             if V[i]>=-40.0:
                 activation= 1000.0*t[i]
                 break
        return activation
    else:
        (points,activation)=getActivationData(path,createActiFilename(l,j))
        id_x=-1
        for i in range(len(points)):
            if points[i][0]==x[0] and points[i][1]==x[1] and points[i][2]==x[2]:
                id_x=i
                break
        if id_x==-1:
            eps=1*10**(-6)
            for i in range(len(points)):
                diff_0=abs(points[i][0]-x[0])
                diff_1=abs(points[i][1]-x[1])
                diff_2=abs(points[i][2]-x[2])
                if diff_0<eps and diff_1<eps and diff_2<eps:
                    id_x=i
                    break
       # print(points[id_x])
        return activation[id_x]
def getDataFromTXT(path,filename, row):
    list = []
    time = []
    values = []
    try:
        f=open(str(path + 'data/' + filename + '.txt'), 'r')
        for l in f:
            line = l.split(" ")
            list.append(line)
        f.close()
    
    except FileNotFoundError:
        try:
            f=open(str(path+ filename + '.txt'), 'r')
            for l in f:
                line = l.split(" ")
                list.append(line)
            f.close()
        except FileNotFoundError:
            k=0
            print(path + 'data/' + filename ,'and', path+filename, "not existing")
    for i in range(0,len(list)):
        #if len(list[i])-1<row+1:
            #print(len(list[i])-1,row+1,i)
            #print('not finished')
       # else:
        time.append(float(list[i][0]))
            #print(path, filename,row,list[i])
        values.append(float(list[i][row]))
    return (time,values) 

def createFilename(l,j,m):  
    return "l{}j{}m{}".format(l,j,m)
def createActiFilename(l,j):
     return "l{}j{}".format(l,j)
 
def getModel(path,fn):
    try:
        f = open(path+ 'log/'+'log_' +fn) 
    except FileNotFoundError:
        try:
            f = open(path+'log_' +fn) 
        except FileNotFoundError:
            print('file not found to get time data')
    model=''
    for l in f:
        m=re.search('Model: [\.]+ Coupled',l)
        if m:
            model='Coupled'
            break
        m=re.search('Model: [\.]+ Monodomain',l)
        if m:
            model='Monodomain'
            break
    return model
def getTimeData(path,l_entry,t_entry,m):
    try:
        f = open(path+ 'log/'+'log_' +createFilename(l_entry,t_entry,m)) 
    except FileNotFoundError:
        try:
            f = open(path+'log_' +createFilename(l_entry,t_entry,m)) 
        except FileNotFoundError:
            print('file not found to get time data')
    EndTime=0.0
    dt = 0.0
    for l in f:
        m = re.search("EndTime:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
          EndTime =m.group(1) 
        m = re.search("DeltaTime:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
            dt = m.group(1)
    f.close() 
    return (float(EndTime),float(dt))
def getElpyhDeltaTime(path,fn):
    try:
        f = open(path+ 'log/'+'log_' +fn) 
    except FileNotFoundError:
        try:
            f = open(path+'log_' +fn) 
        except FileNotFoundError:
            print('file not found to get time data')
    dt = 0.0
    for l in f:
        m = re.search("ElphyDeltaTime:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
            dt = m.group(1)
            break
    f.close() 
    
    return float(dt)
def getTimeDataFromLog(logfile):
    f = open(logfile)
    EndTime=0.0
    dt = 0.0
    for l in f:
        m = re.search("\s*DeltaTime:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
            dt = m.group(1)
            break
    for l in f:
        m = re.search("\s*EndTime:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
          EndTime =m.group(1) 
          break
    f.close() 
    return (float(EndTime),float(dt))
    
def getPoints(path,l_entry,t_entry,m,number):
    f = open(path+ 'log/'+'log_' +createFilename(l_entry,t_entry,m))
    eP = [None] * (number-3)
    for l in f:
        m = re.search( "v(\d+)\(\s*([+\-eE\d]+\.{0,1}\d*)\,\s*([+\-eE\d]+\.{0,1}\d*)\,\s*([+\-eE\d]+\.{0,1}\d*)\)\s*=\s*(\-{0,1}\d+\.{0,1}\d*[e+\-\d*]*)",l)
        if m:
            eP[int(m.group(1))] =[str("%8.2f"%(float(m.group(2)))),str("%8.2f"%(float(m.group(3)))),str("%8.2f"%(float(m.group(4))))]
    f.close()
    twodim=True
    for point in range(len(eP)):
        if float(eP[point][2])!=0.0:
            twodim=False
    if twodim==True:
        for point in range(len(eP)):
            eP[point].pop()
                     
    return (eP)

def NormsOfGammaAndIota(path,fn):
    iota=[]
    gamma=[]
    iotaPot=[]
    gammaPot=[]
    f = open(path+'log/'+"log_"+fn)
    for l in f:
            l = l.strip()
            iotaNorm = re.search("norm of iota_c\\s*([+\-eE\d]+\.{0,1}[\-e\d]*)",l)
            if iotaNorm:
                iota.append(float(iotaNorm.group(1))  ) 
                
            iotaPotNorm = re.search("norm of iota_c_pot\\s*([+\-eE\d]+\.{0,1}[\-e\d]*)",l)
            if iotaPotNorm:
                iotaPot.append(float(iotaPotNorm.group(1))  ) 
                
                
            gammaNorm = re.search("norm of gamma_f_c\s*([+\-eE\d]+\.{0,1}[\-e\d]*)",l)
            if gammaNorm:
                gamma.append(float(gammaNorm.group(1)))
                
            gammaPotNorm = re.search("norm of gamma_f_c_pot\s*([+\-eE\d]+\.{0,1}[\-e\d]*)",l)
            if gammaPotNorm:
                gammaPot.append(float(gammaPotNorm.group(1)))
    
    f.close()
    return iota,gamma,iotaPot,gammaPot



################################################################################
#Gamma
################################################################################
def writeGammaDataInFile(path,l_List,t_List,m,sizeEvalPoints,option=True):
    fn = ''
    for l in range(l_List[0],len(l_List)+l_List[0]):
        for j in range(t_List[0],len(t_List)+ t_List[0]):
            fn = createFilename(l,j,m)
            print(fn)
            extractGammaDataFromLog(path,fn,sizeEvalPoints,option)

def extractGammaDataFromLog(path,fn,sizeEvalPoints,option):
    if fn=='logfile.log':
        f=open(path+ 'log/'+fn)
    else:
        if option:
            f = open(path+ 'log/'+"log_"+fn)
        else:
            f = open(path+"log_"+fn)
    steps = OrderedDict()
    current_step = 1
    #steps[current_step] = OrderedDict()
    model ='splitting'
    elphytimeStep=getElpyhDeltaTime(path,fn)
    #To add Iext for t=0
    #steps[current_step][12] = '0.0'
    for l in f:
        l = l.strip()
        m=re.search("Model: .................... LinearImplicit",l)
        if m:
            model='LinearImplicit'
            
            
        m=re.search("ElphyModel: [\.]+ LinearImplicit",l)
        if m:
            model='LinearImplicit'
        
        m=re.search("ElphyModel: [\.]+ ImplictEuler",l)
        if m:
            model='ImplicitEuler'
            
        m=re.search("ElphyModel: [\.]+ SemiImplicit",l)
        if m:
            model='SemiImplicit'
            
        m=re.search("ElphyModel: [\.]+ SemiImplicitOnCells",l)
        if m:
            model='SemiImplicitOnCells'
        if model=='LinearImplicit':
            m = re.search("# Linear implicit step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        elif model=='ImplicitEuler':
            m = re.search("# Implicit step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        elif model == 'SemiImplicit':
            m=re.search("# Semi implicit step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        elif model == 'SemiImplicitOnCells':
            m=re.search("# Semi implicit on cells step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
                
        else:
            m = re.search("Solving Step (\d+)\s*at time\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\.",l)
            #m = re.search("# Euler step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                dt=float(m.group(2))+elphytimeStep
                steps[dt] = OrderedDict()
                #steps[m.group(2)] = OrderedDict()
                current_step = dt#m.group(2)
            
        m = re.search("\|\|gamma\|\|_infty = \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
        if m:
            steps[current_step][sizeEvalPoints+1] = m.group(1)
       
        
        m = re.search("gamma(\d+)\(\s*([+\-eE\d]+\.{0,1}[\-e\d]*)\,\s*([+\-eE\d]+\.{0,1}[\-e\d]*)\,\s*([+\-eE\d]+\.{0,1}[\-e\d]*)\)\s*=\s*(\-{0,1}\d+\.{0,1}\d*[e+\-\d*]*)", l)
        if m:       
            steps[current_step][int(m.group(1))-sizeEvalPoints] = m.group(5)
        #print(steps[current_step])
        
    f.close()
    if fn=='logfile.log':
        out_f = open(path + 'log/logfile.txt', "w")
    else:
        if option:
            out_f = open(path + 'data/' +fn +'Gamma.txt', "w")
        else:
            out_f = open(path +fn +'Ca.txt', "w")

    for k,v in steps.items():
        #listVValues =list(v.values())
        #print(list(v.values()))
        listVValues=[]
        keys=[]
        #print(k,v)
        for  i in v:
            if i!=sizeEvalPoints+1:
                keys.append(i)
        if len(keys)!=0:
            for i in range(max(keys)+1):
                if i  in v:
                    listVValues.append(v[i])
                
                else:
                    listVValues.append('0.0')
            listVValues.append(v[sizeEvalPoints+1])
        
        #listVValues[5],listVValues[6]=listVValues[6],listVValues[5]
        out_f.write("{} {}\n".format('{:.8f}'.format(float(k)), " ".join(listVValues)))
        
    out_f.close()
################################################################################
#Ca
################################################################################    
def writeCaDataInFile(path,l_List,t_List,m,sizeEvalPoints,option=True):
    fn = ''
    for l in range(l_List[0],len(l_List)+l_List[0]):
        for j in range(t_List[0],len(t_List)+ t_List[0]):
            fn = createFilename(l,j,m)
            print(fn)
            extractCaDataFromLog(path,fn,sizeEvalPoints,option)

def extractCaDataFromLog(path,fn,sizeEvalPoints,option):
    if fn=='logfile.log':
        f=open(path+ 'log/'+fn)
    else:
        if option:
            f = open(path+ 'log/'+"log_"+fn)
        else:
            f = open(path+"log_"+fn)
    steps = OrderedDict()
    current_step = 0
    steps[current_step] = OrderedDict()
    model ='splitting'
    elphytimeStep=0.0
    if getModel(path,fn)=='Coupled':
        elphytimeStep=getElpyhDeltaTime(path,fn)
    else:
        (T,elphytimeStep)=getTimeDataFromLog(path+'log/log_'+fn)
    #To add Iext for t=0
    #steps[current_step][12] = '0.0'
    for l in f:
        l = l.strip()
        m=re.search("Model: .................... LinearImplicit",l)
        if m:
            model='LinearImplicit'
            
            
        m=re.search("ElphyModel: [\.]+ LinearImplicit",l)
        if m:
            model='LinearImplicit'
        
        m=re.search("ElphyModel: [\.]+ ImplictEuler",l)
        if m:
            model='ImplicitEuler'
            
        m=re.search("ElphyModel: [\.]+ SemiImplicit",l)
        if m:
            model='SemiImplicit'
            
        m=re.search("ElphyModel: [\.]+ SemiImplicitOnCells",l)
        if m:
            model='SemiImplicitOnCells'
        if model=='LinearImplicit':
            m = re.search("# Linear implicit step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        elif model=='ImplicitEuler':
            m = re.search("# Implicit step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        elif model == 'SemiImplicit':
            m=re.search("# Semi implicit step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        elif model == 'SemiImplicitOnCells':
            m=re.search("# Semi implicit on cells step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
                
        else:
            m = re.search("Solving Step (\d+)\s*at time\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\.",l)
            #m = re.search("# Euler step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                dt=float(m.group(2))+elphytimeStep
                steps[dt] = OrderedDict()
                #steps[m.group(2)] = OrderedDict()
                current_step = dt#m.group(2)
            
        m = re.search("\|\|Ca\|\|_L2 = \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
        if m:
            steps[current_step][sizeEvalPoints+1] = m.group(1)
       
        
        m = re.search("Ca(\d+)\(\s*([+\-eE\d]+\.{0,1}[\-e\d]*)\,\s*([+\-eE\d]+\.{0,1}[\-e\d]*)\,\s*([+\-eE\d]+\.{0,1}[\-e\d]*)\)\s*=\s*(\-{0,1}\d+\.{0,1}\d*[e+\-\d*]*)", l)
        if m:       
            steps[current_step][int(m.group(1))-sizeEvalPoints] = m.group(5)
        #print(steps[current_step])
        
    f.close()
    if fn=='logfile.log':
        out_f = open(path + 'log/logfile.txt', "w")
    else:
        if option:
            out_f = open(path + 'data/' +fn +'Ca.txt', "w")
        else:
            out_f = open(path +fn +'Ca.txt', "w")

    for k,v in steps.items():
        #listVValues =list(v.values())
        #print(list(v.values()))
        listVValues=[]
        keys=[]
        #print(k,v)
        for  i in v:
            if i!=sizeEvalPoints+1:
                keys.append(i)
        if len(keys)!=0:
            for i in range(max(keys)+1):
                if i  in v:
                    listVValues.append(v[i])
                
                else:
                    listVValues.append('0.0')
            listVValues.append(v[sizeEvalPoints+1])
        
        #listVValues[5],listVValues[6]=listVValues[6],listVValues[5]
        out_f.write("{} {}\n".format('{:.8f}'.format(float(k)), " ".join(listVValues)))
        
    out_f.close()
################################################################################
#Volumes
################################################################################
def writeVolumeDataInFile(path,l_List,t_List,m,option=True):
    fn = ''
    for l in range(l_List[0],len(l_List)+l_List[0]):
        for j in range(t_List[0],len(t_List)+ t_List[0]):
            fn = createFilename(l,j,m)
            print(fn)
            extractVolumeDataFromLog(path,fn,option)

def extractVolumeDataFromLog(path,fn,option):
    if fn=='logfile.log':
        f=open(path+ 'log/'+fn)
    else:
        if option:
            f = open(path+ 'log/'+"log_"+fn)
        else:
            f = open(path+"log_"+fn)
    lv = []
    rv = []
    t = []
    s = []
    for l in f:
        l = l.strip()
        lv_volume = re.search("LV-Volume\s*=\s*([+\-eE\d]+\.{0,1}[\-e\d]*)",l)
        if lv_volume:
                lv.append(float(lv_volume.group(1))  )      
                
        rv_volume = re.search("RV-Volume\s*=\s*([+\-eE\d]+\.{0,1}[\-e\d]*)",l)
        if rv_volume:
            rv.append(float(rv_volume.group(1)))  
               
                
        time = re.search("Time\s*=\s*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if time:
            t.append(float(time.group(1)))
               
        step = re.search("Step\s*=\s*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if step:
            s.append(float(step.group(1)))
    f.close()
    
    if fn=='logfile.log':
        out_f = open(path + 'log/logfile.txt', "w")
    else:
        if option:
            out_f = open(path + 'data/' +fn +'Volumes.txt', "w")
        else:
            out_f = open(path +fn +'Volumes.txt', "w")

    if len(lv)!=len(rv) or len(lv)!=len(t):
        print('length of volumes or time mismatch')
    else:
        for i in range(len(t)):
            out_f.write("{} {} {}\n".format('{:.8f}'.format(float(t[i])), '{:.8f}'.format(float(lv[i])),'{:.8f}'.format(float(rv[i]))))
    out_f.close()
        
        
def VolumeDataFromLog(path,fn):

    lv = []
    rv = []
    t = []
    s = []
    volume = []
    
    f = open(path+'log/'+"log_"+fn)
    for l in f:
        l = l.strip()
        lv_volume = re.search("LV-Volume\s*=\s*([+\-eE\d]+\.{0,1}[\-e\d]*)",l)
        if lv_volume:
            lv.append(float(lv_volume.group(1))  )      
                
                
        rv_volume = re.search("RV-Volume\s*=\s*([+\-eE\d]+\.{0,1}[\-e\d]*)",l)
        if rv_volume:
            rv.append(float(rv_volume.group(1)))  
               
                
        time = re.search("Time\s*=\s*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if time:
            t.append(float(time.group(1)))
               
        step = re.search("Step\s*=\s*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if step:
            s.append(float(step.group(1)))
    volume.append(lv)
    volume.append(rv)
    f.close()
    return volume,t,s        
        
        
################################################################################
#V
################################################################################
def writeDataInFile(path,l_List,t_List,m,option=True):
    fn = ''
    for l in range(l_List[0],len(l_List)+l_List[0]):
        for j in range(t_List[0],len(t_List)+ t_List[0]):
            fn = createFilename(l,j,m)
            print(fn)
            extractDataFromLog(path,fn,option)
            


def extractDataFromLog(path,fn,option):
    if fn=='logfile.log':
        f=open(path+ 'log/'+fn)
    else:
        if option:
            f = open(path+ 'log/'+"log_"+fn)
        else:
            f = open(path+"log_"+fn)
    steps = OrderedDict()
    current_step = 0
    steps[current_step] = OrderedDict()
    model ='splitting'
    elphytimeStep=0.0
    if getModel(path,fn)=='Coupled':
        elphytimeStep=getElpyhDeltaTime(path,fn)
    else:
        (T,elphytimeStep)=getTimeDataFromLog(path+'log/log_'+fn)
    #To add Iext for t=0
    #steps[current_step][12] = '0.0'
    for l in f:
        l = l.strip()
        m=re.search("Model: .................... LinearImplicit",l)
        if m:
            model='LinearImplicit'
            
            
        m=re.search("ElphyModel: [\.]+ LinearImplicit",l)
        if m:
            model='LinearImplicit'
        
        m=re.search("ElphyModel: [\.]+ ImplictEuler",l)
        if m:
            model='ImplicitEuler'
            
        m=re.search("ElphyModel: [\.]+ SemiImplicit",l)
        if m:
            model='SemiImplicit'
            
        m=re.search("ElphyModel: [\.]+ SemiImplicitOnCells",l)
        if m:
            model='SemiImplicitOnCells'
        if model=='LinearImplicit':
            m = re.search("# Linear implicit step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        elif model=='ImplicitEuler':
            m = re.search("# Implicit step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        elif model == 'SemiImplicit':
            m=re.search("# Semi implicit step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        elif model == 'SemiImplicitOnCells':
            m=re.search("# Semi implicit on cells step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
                
        else:
            m = re.search("Solving Step (\d+)\s*at time\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\.",l)
            
            #m = re.search("# Euler step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                dt=float(m.group(2))+elphytimeStep
                steps[dt] = OrderedDict()
                current_step = dt
        
        
        #m = re.search("\|\|Iext\|\|_L2 = \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
        #if m:
            #steps[current_step][12] = m.group(1)
            
        m = re.search("\|\|v\|\|_L2 = \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
        if m:
            steps[current_step][10] = m.group(1)
            
        m = re.search("\|\|v\|\|_H1 = \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
        if m:
            steps[current_step][11] = m.group(1)
            
        
        m = re.search("v(\d+)\(\s*([+\-eE\d]+\.{0,1}[\-e\d]*)\,\s*([+\-eE\d]+\.{0,1}[\-e\d]*)\,\s*([+\-eE\d]+\.{0,1}[\-e\d]*)\)\s*=\s*(\-{0,1}\d+\.{0,1}\d*[e+\-\d*]*)", l)
        if m:       
            steps[current_step][int(m.group(1))] = m.group(5)
        #print(steps[current_step])
        
    f.close()
    if fn=='logfile.log':
        out_f = open(path + 'log/logfile.txt', "w")
    else:
        if option:
            out_f = open(path + 'data/' +fn +'.txt', "w")
        else:
            out_f = open(path +fn +'.txt', "w")

    for k,v in steps.items():
        #listVValues =list(v.values())
        #print(list(v.values()))
        listVValues=[]
        keys=[]
        #print(k,v)
        for  i in v:
            if i!=10 and i!=11:
                keys.append(i)
        if len(keys)!=0:
            for i in range(max(keys)+1):
                if i  in v:
                    listVValues.append(v[i])
                
                else:
                    listVValues.append('0.0')
            listVValues.append(v[10])
            listVValues.append(v[11])
        
        #listVValues[5],listVValues[6]=listVValues[6],listVValues[5]
        out_f.write("{} {}\n".format('{:.8f}'.format(float(k)), " ".join(listVValues)))
        
    out_f.close()
#########################################################################################
    
def getDurationFromLog(path,fn):
    f = open(path+ 'log/'+"log_"+fn)
    dur=''    
    for l in f:
        l = l.strip() #16:59:56.00
        m=re.search("Finished Calculations after\s*([\d]*\:{0,1}[\d]*\:{0,1}[\d]+\.[\d]*)\s*",l) #([\d\:]\d\.{0,1}[\d]) ",l)
        if m:
            dur=m.group(1)
   
    return dur
def getMinMaxFromLog(path,fn):
    f = open(path+ 'log/'+"log_"+fn)
    max=[]
    min=[]
    
    for l in f:
        l = l.strip()
        #print(l)
        m=re.search("Max values: \s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)",l)
        if m:
            for i in range(1,9):
                max.append(float(m.group(i)))
        m=re.search("Min values: \s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)\s*([+\-\d]+\.{0,1}[+\-e\d]*)",l)
        if m:
            for i in range(1,9):
                min.append(float(m.group(i)))
        #Min values: -90.59779572 1.811478322e-07 0.002849061516 0.9071142229 2.739632235e-11 0.0002928914012 0.004956142319 0.005637954401
    return(min,max)

def ExtractDurations(path,l_List,t_List,m):
    fn = ''
    for l in range(l_List[0],len(l_List)+l_List[0]):
        for j in range(t_List[0],len(t_List)+ t_List[0]):
            fn = createFilename(l,j,m)
            #print(fn)
            extractTestDurationFromLog(path,fn)

def extractTestDurationFromLog(path,fn):
    f = open(path+ "log_"+fn)
    iext=[]
    gating=[]
    assembleA=[]
    invertA=[]
    assembleRHS=[]
    gmres =[]
    PDE=[]
    ion=[]
    ODE=[]
    SplittingStep=[]
    model ='splitting'
    EndTime=0.0
    dt = 0.0
    
    for l in f:
        l = l.strip() #16:59:56.00
        m=re.search("ElphyModel: ............... LinearImplicit",l)
        if m:
            model='LinearImplicit'
            
        if model=='LinearImplicit':
            m=re.search("Solve Gating: ([\d]+.[\d]*) seconds",l)
            if m:
                gating.append(m.group(1))
            m=re.search("Solve Ion: ([\d]+.[\d]*) seconds",l)
            if m:
                ion.append(m.group(1))
        elif model=='splitting':
            m=re.search("Solve ODE: ([\d]+.[\d]*) seconds",l)
            if m:
                ODE.append(m.group(1))
            m=re.search("Solve Splittingstep: ([\d]+.[\d]*) seconds",l)
            if m:
                SplittingStep.append(m.group(1))
                
        m=re.search("Update Iext: ([\d]+.[\d]*) seconds",l) #([\d\:]\d\.{0,1}[\d]) ",l)
        if m:
            iext.append(m.group(1))        
        m=re.search("Assemble Systemmatrix: ([\d]+.[\d]*) seconds",l)
        if m:
            assembleA.append(m.group(1))
        m=re.search("Invert Systemmatrix: ([\d]+.[\d]*) seconds",l)
        if m:
            invertA.append(m.group(1))
        m=re.search("Assemble RHS: ([\d]+.[\d]*) seconds",l)
        if m:
            assembleRHS.append(m.group(1))
        m=re.search("GMRES: ([\d]+.[\d]*) seconds",l)
        if m:
            gmres.append(m.group(1))
        m=re.search("Solve PDE:\s*([\d]+.[\d]*) seconds",l)
        if m:
            PDE.append(m.group(1))
        m = re.search("EndTime:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
          EndTime =m.group(1) 
        m = re.search("DeltaTime:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
            dt = float(m.group(1))
 
    #print(EndTime,dt,len(iext),len(gating),len(assembleA),len(invertA),len(assembleRHS),len(gmres),len(PDE),len(ion))
    out_f = open(path+fn +'Dur.txt', "w")
    if model=='LinearImplicit':
        out_f.write('time  gating assembleA  assembleRHS gmres PDE ion\n')
        for i in range(len(PDE)):
            t=dt*(i+1)
            listvalues=[gating[i],assembleA[i],assembleRHS[i],gmres[i],PDE[i],ion[i]]
            out_f.write("{} {}\n".format('{:.8f}'.format(float(t)), " ".join(listvalues)))
    elif model=='splitting':
        out_f.write('time  ODE assembleA  assembleRHS gmres PDE SplittingStep\n')
        if len(assembleA)==1:
            for i in range(1,len(PDE)):
                assembleA.append('0.0')
                #invertA.append('0.0')
        for i in range(len(PDE)):
            t=dt*(i+1)
            listvalues=[ODE[i],assembleA[i],assembleRHS[i],gmres[i],PDE[i],SplittingStep[i]]
            out_f.write("{} {}\n".format('{:.8f}'.format(float(t)), " ".join(listvalues)))
    out_f.close()

def getDurationFromTXT(path,fn,n,c):
    list = []
    value=[]
    try:
        f=open(str(path + fn + 'Dur.txt'), 'r')
        for l in f:
            line = l.split(" ")
            list.append(line)
        f.close()
    #print(filename)
    except FileNotFoundError:
        print("not existing")
    if len(list)<n:
        print('something went wrong')
    else:
        for i in range(1,n+1):
            value.append(float(list[i][c]))
    return (value) 
    
def getMeshInfo(path, l,t):
    min_dx=0.0
    max_dx=0.0
    cells=0
    vertices=0
    f = open(path+ "log/log_"+createFilename(l,t,1))
    for l in f:
        l = l.strip()
                      
        m=re.search('Cells: ...................................\s*([\d]+)',l)
        if m:
            cells=m.group(1)
            
        m=re.search('Vertices: ................................\s*([\d]+)',l)
        if m:
            vertices=m.group(1)
        m=re.search('Mesh width \[min, max\]: ................... \[([\d]+.[\d]*),\s+([\d]+.[\d]*)\]',l)
        if m:
           min_dx=m.group(1)
           max_dx=m.group(2)
    return(min_dx,max_dx,cells,vertices)

def getMeshInfoCoupled(path, l,t):
    min_dx=[-1.0, -1.0]
    max_dx=[-1.0, -1.0]
    cells=[-1.0, -1.0]
    vertices=[-1.0, -1.0]
    f = open(path+ "log/log_"+createFilename(l,t,1))
    ifMono=False
    ifMech=False
    for l in f:
        l = l.strip()
        
        monodomain=re.search('==== Monodomain Information ==============',l)
        if monodomain:
            ifMono=True
        mech = re.search('=================== Elasticity Information ==================',l)
        if mech:
            ifMech=True
            ifMono=False
            
        m=re.search('Cells: ...................................\s*([\d]+)',l)
        if m:
            if ifMono:
                cells[0]=m.group(1)
            elif ifMech:
                cells[1]=m.group(1)
                
            else:
                print('something went wrong')
            
            
        m=re.search('Vertices: ................................\s*([\d]+)',l)
        if m:
            if ifMono:
                vertices[0]=m.group(1)
            elif ifMech:
                vertices[1]=m.group(1)
                
            else:
                print('something went wrong')
        m=re.search('Mesh width \[min, max\]: ................... \[([\d]+.[\d]*),\s+([\d]+.[\d]*)\]',l)
        if m:
            if ifMono:
                min_dx[0]=m.group(1)
                max_dx[0]=m.group(2)
            elif ifMech:
                min_dx[1]=m.group(1)
                max_dx[1]=m.group(2)
                
            else:
                print('something went wrong')
    return(min_dx,max_dx,cells,vertices)
def writeActivationDataFiles(path,lL,tL,option=True):
    fn = ''
    print('write Activation data files')
    for l in range(lL[0],len(lL)+lL[0]):
        for j in range(tL[0],len(tL)+ tL[0]):
            fn = createActiFilename(l,j)
            print(fn)
            extractDataFromVTU(path,fn,option)
def changeNumberOFCellsiInVTU(path,lL,tL,number):
    for l in lL:
        for j in tL:
            fin=open(path+'AT_l'+str(l)+'j'+str(j)+'.vtu','r')
            fout=open(path+'CAT_l'+str(l)+'j'+str(j)+'.vtu','w')
            for line in fin:
                if line == '    <Piece NumberOfPoints="71809719" NumberOfCells="420352000">\n':
                    fout.write(line.replace('    <Piece NumberOfPoints="71809719" NumberOfCells="420352000">\n', '    <Piece NumberOfPoints="71809719" NumberOfCells="1">\n'))
                else:
                    fout.write(line)
            fin.close()
            fout.close()
def readVTU(filename):
    try:
        f=open(filename, 'r')
        f.close()
        reader = vtkXMLUnstructuredGridReader()
        reader.SetFileName(filename)
        reader.Update()
        data = reader.GetOutput()
        # Extract Points
        points = vtk_to_numpy(data.GetPoints().GetData())
        #print(points[0])
        point_data = data.GetPointData()
        data = vtk_to_numpy(point_data.GetAbstractArray(0))
        
        return  points, data
    except FileNotFoundError:
        return [],[]
def readVTUgetPointsAndCells(filename):
    reader = vtkXMLUnstructuredGridReader()
    reader.SetFileName(filename)
    reader.Update()
    data = reader.GetOutput()
    # Extract Points
    points = vtk_to_numpy(data.GetPoints().GetData())
    # Extract Cells
    cells = vtk_to_numpy(data.GetCells().GetData())
    cellList=[]
    numberPoints =cells[0]
    if numberPoints==4:
        lencells=int(len(cells)/5)
        for i in range(lencells):
            cell =[cells[(i*5)+1],cells[(i*5)+2],cells[(i*5)+3],cells[(i*5)+4]]
            cellList.append(cell)
    else:
        print('bis jetzt nur linerare tetraeder implementiert, der Rest geht noch nicht!')
        
    return  cellList, points
def extractDataFromVTU(path,fn,option):
    filename =''
    if fn=='ActivationTime':
        filename=path+ 'log/'+fn
    else:
        if option:
            filename=path+ 'vtu/'+"AT_"+fn
        else:
            filename=path+"AT_"+fn
    filename +='.vtu'
    points, point_data = readVTU(filename)
    
    out_f = open(path+'data/AT_'+fn , "w")
    #print("Len(points)",len(points))
    for i in range(len(points)):
        listvalues=[points[i],point_data[i]]
        #print("{} {} {} {}\n".format('{:.8f}'.format(points[i][0]),'{:.8f}'.format(points[i][1]),'{:.8f}'.format(points[i][2]), point_data[i]))
        out_f.write("{} {} {} {}\n".format('{:.6f}'.format(points[i][0]),'{:.6f}'.format(points[i][1]),'{:.6f}'.format(points[i][2]), point_data[i]))
    
    out_f.close()
def getActivaionTimeByLine(linenumber,path,filename):
    f=open(str(path + 'data/AT_' + filename), 'r')
    at=-1.0
    with open(path + 'data/AT_' + filename) as f:
        line=f.readlines()[linenumber]
        line= line.split(" ")
        at = line[3]
    return at
def getIDAti(i,l,path):
    index =-1
    with open(path + 'data/IDListl' + str(l)) as f:
        index = f.readlines()[i]
    return index
def getActivationTimeofPointFromData(path, point,l,j):
    #print(l,j,point)
    (points,actiTime)=getActivationData(path,createActiFilename(l,j))
    index =0
    for p in points:
        if set(p)!=set(point):
            index +=1
        else:
            break
    return actiTime[index]       
    
def getActivationData(path,filename):
    list = []
    points = []
    data = []
    f=open(str(path + 'data/AT_' + filename), 'r')
    for l in f:
        line = l.split(" ")
        list.append(line)
    f.close()
    if len(list)==0:
        print('did not work')
    for i in range(0,len(list)):
        point=[float(list[i][0]),float(list[i][1]),float(list[i][2])]
        points.append(point)
        data.append(float(list[i][3]))
    return (points,data) 
def getActivationOnly(path,filename):
    list = []
    data = []
    f=open(str(path + 'data/AT_' + filename), 'r')
    for l in f:
        line = l.split(" ")
        list.append(line)
    f.close()
    if len(list)==0:
        print('did not work')
    for i in range(0,len(list)):
        data.append(float(list[i][3]))
    return data
def getIDList(path,l):
    list = []
    data = []
    f=open(str(path + 'data/IDListl' + str(l)), 'r')
    for l in f:
        line = l.split(" ")
        list.append(line)
    f.close()
    if len(list)==0:
        print('did not work')
    for i in range(0,len(list)):
        data.append(int(list[i][0]))
    return data
def writeIDLists(lL,tL,path):
    (evalPoints,activation)=getActivationData(path,createActiFilename(lL[0],0))
    for l in lL:
        (points_l,activation_l)=getActivationData(path,createActiFilename(l,0))
        idList=[]
        for x in evalPoints:
            index=-1
            for i in range(len(points_l)):
                if points_l[i][0]==x[0] and points_l[i][1]==x[1] and points_l[i][2]==x[2]:
                    index=i
                    break
            idList.append(index)
        print('list is collected', len(idList))
        out_f = open(path+'data/IDListl'+str(l) , "w")
        for i in range(len(idList)):
            out_f.write("{}\n".format(idList[i]))
        out_f.close()
        print('l='+str(l)+' finished')
    
def getActivaitionTimeFromLog(l,j,m, path, p,th, withNearest=False,case=''):
    (t,V)=getDataFromTXT(path,createFilename(l,j,m)+case,p)
    tact=-10.0
    if case=='Ca' or case=='':
        tact=comp.getActivaitionTimeFromV(t,V,th,withNearest)
    elif case=='Gamma' or case=='Volumes':
        tact=comp.getActivaitionTimeFromGamma(t,V,th,withNearest)
    else:
        print('read.getActivaitionTimeFromLog: case not defined')
    return tact
    
    
