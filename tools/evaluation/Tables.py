#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import csv
import matplotlib.pyplot as plt

sys.path.append('../..')

import mpp.python.mppy as mppy

if __name__ == "__main__":
    mpp = mppy.Mpp(project_name='CardMech',
                   executable='M++',
                   kernels=32,
                   mute=False)
    
    mpp.clean_log()
    mpp.build()
    
    texstring = "\\begin{table}\n \
                 \\begin{tabular}[h]{c|c|c|c|c|c|c|c|c|c|c|c|c}\n \
                 \\text{PolynomialDegree} &\\text{Level} & \\text{Preconditioner} & \\text{solver} & \
                 $L_2$- \\text{Error} & $H_1$-\\text{Error} & \\text{Energy-Error} & \
                \\text{max. Newton steps} & \\text{max. duration} & \\text{end time}  & \
                \\text{Linesearch steps} & \\text{avg linesearches} & \\text{avg Newton steps} \\\  \
                \\hline    \n"
    
    #solver = {"GMRES"}
    solver = {"CG"}
    #solver = {"CG","GMRES"}
    length_solver = len(solver)
    
    #mech_preconditioner = {"Jacobi"}
    #mech_preconditioner = {"GaussSeidel"}
    #mech_preconditioner = {"GaussSeidel","Jacobi"}
    #mech_preconditioner = {"GaussSeidel","Jacobi","SGS","SSOR"}
    #mech_preconditioner = {"GaussSeidel","Jacobi","SGS","SSOR","PointBlockGaussSeidel","PointBlockJacobi"}
    mech_preconditioner = {"Jacobi","SGS","PointBlockJacobi"}
    
    #mech_level = {'2'}
    mech_level = {'1'}
    #mech_level = {'0', '1'}
    #mech_level = {'0','1','2'}
    
    #mech_degree = {'1','2'}
    mech_degree = {'2'}
    
    
    comp_duration_CG=[];
    comp_duration_GMRES=[];
    comp_duration =[];
    newton_steps_CG =[]
    newton_steps_GMRES = []
    comp_newton_steps = []
    
    total_data = []
    
    b = len(solver)*len(mech_preconditioner)*len(mech_level)
    a=range(b)
    
    for i in solver:
        for k in mech_preconditioner:
            for w in mech_degree:
                for j in mech_level:
                    mpp = mppy.Mpp(project_name='CardMech',
                                   executable='M++',
                                   kernels=32,
                                   mute=False)
                    mpp.clean_log()
                    mpp.run(32, config='klotz', kwargs={
                        "MechSolver": i,
                        "MechLevel": j,
                        "MechPreconditioner": k,
                        "MechPolynomialDegree" : w
                        })
    
    
                    mpp.parse_log()

                    #print(mpp.data)
                
                    degree = mpp.data["MechPolynomialDegree"][0]
                    level = mpp.data["MechLevel"][0]
                    preconditioner = mpp.data["MechPreconditioner"][0]
                    solver = mpp.data["MechSolver"][0]
                    convergence = mpp.data["Convergence"][0]
                    linesearch_steps = mpp.data["line search steps"][0]
                
                

                if convergence == 0:
                    texstring += str(w) + "&" + str(j ) + "&"  + str(k ) + "&" + str(i ) + "&" \
                                + "no convergence " +  "&" + "no convergence " + "&" + "no convergence " + "&" \
                                + "no convergence " + "&" + "no convergence " +  "&" + "no convergence " + "&" \
                                + str(linesearch_steps) + "&" + "no convergence " + "&" + "no convergence " + "\\\ \n"                  
                    
                    end_time = "no convergence"
                    avg_steps = "no convergence"
                    avg_linesearches = "no convergence"
                    l2_err ="no convergence"
                    h1_err = "no convergence"
                    energy_err = "no convergence"
                    
                    d = ([i],[k],[int(w)],[int(j)],["no convergence"],["no convergence"],["no convergence"],\
                         ["no convergence"], ["no convergence"],["no convergence"])
                    total_data.append(d)
                        
                    
                else:
                    l2_err = mpp.data["L2 Error"][0]
                    h1_err = mpp.data["H1 Error"][0]
                    energy_err = mpp.data["Energy Error"][0]
                    newton_steps = mpp.data["Steps"][0]
                    pressure_steps = mpp.data["Pressure Scale"][0]
                    duration = mpp.data["Duration"][0]
                    end_time = mpp.data["end time"][0]
            
                    #pressure_steps.pop(30)
                    #newton_steps.pop(30)
                    #duration.pop(30)
                    #linesearch_steps.pop(30)
                
                    
                # Gibt die höchste Anzahl an Newton-Schritte an (insgesamt)
                    max_steps = max(newton_steps)
                    avg_steps = sum(newton_steps)/len(newton_steps)
                    #texstring += str(max_steps)
                # Gibt alle Newtonschritte aus (für jeden Pressure step)
                    #texstring += str(newton_steps)
                    
                # Gibt die längste Dauer an (insgesamt)
                    max_time = max(duration)
                    avg_time = sum(duration)/len(duration)
                        
                # Gibt die maximale Verwendung von line_searchesavg(newton_steps) an
                    max_linesearches = max(linesearch_steps)
                    avg_linesearches = sum(linesearch_steps)/len(linesearch_steps)
                    
                    end_csv_data = {"end_csv_data"}
                    for ä in end_csv_data:
                        with open(str(end_csv_data) + '.csv', 'a') as end_table_data:
                            writer = csv.writer(end_table_data)
                            writer.writerow(["PolynomialDegree", "Level", "Preconditioner",\
                                         "Solver","L2-Error", "H1-Error" , "Energy-Error",\
                                         "max. Newton steps", "max. duration" , "end time",\
                                         "Linesearch steps", "avg linesearches", "avg Newton steps" ])
                    for ä in end_csv_data:        
                        with open(str(end_csv_data) + '.csv', 'a', newline = '') as end_table_data:
                            writer = csv.writer(end_table_data)
                            writer.writerow([ w, j, k, i, l2_err, h1_err, energy_err, max_steps, max_time, end_time, max_linesearches, avg_linesearches, avg_steps])

                
            
                    texstring += str(w ) +" & " + str(j ) + " & "  + str(k ) + " & " + str(i ) + " & " \
                                + str(l2_err ) +  " & "  + str(h1_err ) + " & " + str(energy_err ) + " & " \
                                + str(max_steps ) + " & " + str(max_time ) +  " & " + str(end_time ) + " & " \
                                + str(max_linesearches ) + " & " + str(avg_linesearches ) + " & " + str(avg_steps ) +"\\\ \n"
             
                    z = ([i],[k],[int(w)],[int(j)], [end_time], [avg_steps], [avg_linesearches],\
                        [l2_err],[h1_err],[energy_err])
                    total_data.append(z)
                        
                      ##########################################################
                      # Erstellen CSV-Datei in Python
                      ##########################################################
                      # für pressure_steps/newton_steps Plot
                      ##########################################################
                      
                    csv_datafiles = {str(i) + "_" +str(j) + "_" +str(k) + "_" + str(w) + "_"}
                    for m in csv_datafiles:
                        with open(str(m)+ "pressure_newton" + '.csv', 'w', newline='') as plot_pressure_newton:
                            writer = csv.writer(plot_pressure_newton)
                            writer.writerow(["Pressure Steps", "Newton Steps"])
                            writer.writerow(["0", "0"])
                            for l in range(len(pressure_steps)):
                                writer.writerow([ pressure_steps[l], newton_steps[l]])
                   
                    ##########################################################
                    # für pressure_steps/duration Plot
                    ##########################################################
                    
                    for u in csv_datafiles:
                        with open(str(u)+ "pressure_duration" + '.csv', 'w', newline='') as plot_pressure_duration:
                            writer = csv.writer(plot_pressure_duration)
                            writer.writerow(["Pressure Steps", "Duration"])
                            writer.writerow(["0", "0"])
                            for n in range(len(pressure_steps)):
                                writer.writerow([ pressure_steps[n], duration[n]])
                                    
                    ##########################################################
                    # für pressure_steps/linesearch_steps Plot
                    ##########################################################
                
                    for p in csv_datafiles:
                        with open(str(p)+ "pressure_linesearch" + '.csv', 'w', newline='') as plot_pressure_linesearch:
                            writer = csv.writer(plot_pressure_linesearch)
                            writer.writerow(["Pressure Steps", "Linesearch Steps"])
                            writer.writerow(["0", "0"])
                            for q in range(len(pressure_steps)):
                                writer.writerow([ pressure_steps[q], linesearch_steps[q]])
                        
        
    texstring += "\\end{tabular} \n \
                  \\end{table}"
    print(texstring)
    
    total_data_0 = []
    total_data_1 = []
    total_data_2 = []
    
    for e in range(len(mech_preconditioner)*length_solver*len(mech_level)*len(mech_degree)):
        if "no convergence" not in total_data[e][4]:
            if total_data[e][3] == [0]:
                total_data_0.append(total_data[e])
            elif total_data[e][3] == [1]:
                total_data_1.append(total_data[e])
            elif total_data[e][3] == [2]:
                total_data_2.append(total_data[e])
        else:
            print("combination of solver " + str(total_data[e][0]) + " and preconditioner " \
                                                            + str(total_data[e][1]) + " is not suitable on level "\
                                                            + str(total_data[e][2]) + " and degree " + str(total_data[e][3]))

    
    total_data_01 = []
    total_data_02 = [] 
    total_data_11 = [] 
    total_data_12 = [] 
    total_data_21 = [] 
    total_data_22 = []
    
    for f in range(len(total_data_0)):
        if "no convergence" not in total_data_0[f][4]:
            if total_data_0[f][2] == [1]:
                total_data_01.append(total_data_0[f])
            elif total_data_0[f][2] == [2]:
                total_data_02.append(total_data_0[f])
                
    for g in range(len(total_data_1)):
        if "no convergence" not in total_data_1[g][4]:
            if total_data_1[g][2] == [1]:
                total_data_11.append(total_data_1[g])
            elif total_data_1[g][2] == [2]:
                total_data_12.append(total_data_1[g])
                
    for h in range(len(total_data_2)):
        if "no convergence" not in total_data_2[h][4]:
            if total_data_2[h][2] == [1]:
                total_data_21.append(total_data_2[h])
            elif total_data_2[h][2] == [2]:
                total_data_22.append(total_data_2[h])
            
    
    
    if total_data_01 != []:
        versuch = []
        
        for e in range(len(total_data_01)):
            versuch_1 = []
            table_entries = [str(total_data_01[e][0]), str(total_data_01[e][1]), \
                             str(total_data_01[e][2]), str(total_data_01[e][3]),\
                            "end time", "average newton steps", "average linesearch steps", "L2 error", "H1 error", "energy error"]
            if e == 0:
                versuch = total_data_01[e]
            else:
                for i in range(4,10):
                    if total_data_01[e][i] == versuch[i]:
                        versuch_1.append([[total_data_01[e][i],table_entries[i]]])
            
                versuch_1.append(total_data_01[e][0])
                versuch_1.append(total_data_01[e][1])
                
                same_data_01 = "same_data_01"
                with open(str(same_data_01) + '.csv', 'a') as same_01:
                    writer = csv.writer(same_01)
                    writer.writerow([str(versuch[0]), "with preconditioner", str(versuch[1]), "has the same values like",\
                                      str(versuch_1), "on level", str(versuch[3]) , "and degree", str(versuch[2])])
        
        minimal_data_01 ="minimal_data_01"
        with open(str(minimal_data_01) + '.csv', 'a') as minimal_01:
               writer = csv.writer(minimal_01)
               writer.writerow(["minimal end time:" , str(min(total_data_01, key=lambda x: x[4])) ])
               writer.writerow(["minimal average newton steps:"  , str(min(total_data_01, key=lambda x: x[5])) ])
               writer.writerow(["minimal average linesearch steps:" , str(min(total_data_01, key=lambda x: x[6])) ])
               writer.writerow(["minimal L2-error:" , str(min(total_data_01, key=lambda x: x[7])) ])
               writer.writerow(["minimal H1-error:" , str(min(total_data_01, key=lambda x: x[8])) ])
               writer.writerow(["minimal energy-error:" , str(min(total_data_01, key=lambda x: x[9]))])
                
                
    if total_data_02 != []:
        versuch = []
        for e in range(len(total_data_02)):
            versuch_1 = []
            table_entries = [str(total_data_02[e][0]), str(total_data_02[e][1]), \
                             str(total_data_02[e][2]), str(total_data_02[e][3]),\
                            "end time", "average newton steps", "average linesearch steps", "L2 error", "H1 error", "energy error"]
            if e == 0:
                versuch = total_data_02[e]
            else:
                for i in range(4,10):
                    if total_data_02[e][i] == versuch[i]:
                        versuch_1.append([[total_data_02[e][i],table_entries[i]]])
            
                versuch_1.append(total_data_02[e][0])
                versuch_1.append(total_data_02[e][1])
                
                same_data_02 = "same_data_02"
                with open(str(same_data_02) + '.csv', 'a') as same_02:
                    writer = csv.writer(same_02)
                    writer.writerow([str(versuch[0]), "with preconditioner", str(versuch[1]), "has the same values like",\
                                      str(versuch_1), "on level", str(versuch[3]) , "and degree", str(versuch[2])])
                    
        minimal_data_02 ="minimal_data_02"
        with open(str(minimal_data_02) + '.csv', 'a') as minimal_02:
               writer = csv.writer(minimal_02)
               writer.writerow(["minimal end time:" , str(min(total_data_02, key=lambda x: x[4])) ])
               writer.writerow(["minimal average newton steps:"  , str(min(total_data_02, key=lambda x: x[5])) ])
               writer.writerow(["minimal average linesearch steps:" , str(min(total_data_02, key=lambda x: x[6])) ])
               writer.writerow(["minimal L2-error:" , str(min(total_data_02, key=lambda x: x[7])) ])
               writer.writerow(["minimal H1-error:" , str(min(total_data_02, key=lambda x: x[8])) ])
               writer.writerow(["minimal energy-error:" , str(min(total_data_02, key=lambda x: x[9]))])
        
    if total_data_11 != []:
        versuch = []
        for e in range(len(total_data_11)):
            versuch_1 = []
            table_entries = [str(total_data_11[e][0]), str(total_data_11[e][1]), \
                             str(total_data_11[e][2]), str(total_data_11[e][3]),\
                            "end time", "average newton steps", "average linesearch steps", "L2 error", "H1 error", "energy error"]
            if e == 0:
                versuch = total_data_11[e]
            else:
                for i in range(4,10):
                    if total_data_11[e][i] == versuch[i]:
                        versuch_1.append([[total_data_11[e][i],table_entries[i]]])
            
                versuch_1.append(total_data_11[e][0])
                versuch_1.append(total_data_11[e][1])
                
                same_data_11 = "same_data_11"
                with open(str(same_data_11) + '.csv', 'a') as same_11:
                    writer = csv.writer(same_11)
                    writer.writerow([str(versuch[0]), "with preconditioner", str(versuch[1]), "has the same values like",\
                                      str(versuch_1), "on level", str(versuch[3]) , "and degree", str(versuch[2])])
        
        minimal_data_11 ="minimal_data_11"
        with open(str(minimal_data_11) + '.csv', 'a') as minimal_11:
               writer = csv.writer(minimal_11)
               writer.writerow(["minimal end time:" , str(min(total_data_11, key=lambda x: x[4])) ])
               writer.writerow(["minimal average newton steps:"  , str(min(total_data_11, key=lambda x: x[5])) ])
               writer.writerow(["minimal average linesearch steps:" , str(min(total_data_11, key=lambda x: x[6])) ])
               writer.writerow(["minimal L2-error:" , str(min(total_data_11, key=lambda x: x[7])) ])
               writer.writerow(["minimal H1-error:" , str(min(total_data_11, key=lambda x: x[8])) ])
               writer.writerow(["minimal energy-error:" , str(min(total_data_11, key=lambda x: x[9]))])
        
    if total_data_12 != []:
        versuch = []
        for e in range(len(total_data_12)):
            versuch_1 = []
            table_entries = [str(total_data_12[e][0]), str(total_data_12[e][1]), \
                             str(total_data_12[e][2]), str(total_data_12[e][3]),\
                            "end time", "average newton steps", "average linesearch steps", "L2 error", "H1 error", "energy error"]
            if e == 0:
                versuch = total_data_12[e]
            else:
                for i in range(4,10):
                    if total_data_12[e][i] == versuch[i]:
                        versuch_1.append([[total_data_12[e][i],table_entries[i]]])
            
                versuch_1.append(total_data_12[e][0])
                versuch_1.append(total_data_12[e][1])
                
                same_data_12 = "same_data_12"
                with open(str(same_data_12) + '.csv', 'a') as same_12:
                    writer = csv.writer(same_12)
                    writer.writerow([str(versuch[0]), "with preconditioner", str(versuch[1]), "has the same values like",\
                                      str(versuch_1), "on level", str(versuch[3]) , "and degree", str(versuch[2])])
          
        minimal_data_12 ="minimal_data_12"
        with open(str(minimal_data_12) + '.csv', 'a') as minimal_12:
               writer = csv.writer(minimal_12)
               writer.writerow(["minimal end time:" , str(min(total_data_12, key=lambda x: x[4])) ])
               writer.writerow(["minimal average newton steps:"  , str(min(total_data_12, key=lambda x: x[5])) ])
               writer.writerow(["minimal average linesearch steps:" , str(min(total_data_12, key=lambda x: x[6])) ])
               writer.writerow(["minimal L2-error:" , str(min(total_data_12, key=lambda x: x[7])) ])
               writer.writerow(["minimal H1-error:" , str(min(total_data_12, key=lambda x: x[8])) ])
               writer.writerow(["minimal energy-error:" , str(min(total_data_12, key=lambda x: x[9]))])
        
    if total_data_21 != []:
        versuch = []
        for e in range(len(total_data_21)):
            versuch_1 = []
            table_entries = [str(total_data_21[e][0]), str(total_data_21[e][1]), \
                             str(total_data_21[e][2]), str(total_data_21[e][3]),\
                            "end time", "average newton steps", "average linesearch steps", "L2 error", "H1 error", "energy error"]
            if e == 0:
                versuch = total_data_21[e]
            else:
                for i in range(4,10):
                    if total_data_21[e][i] == versuch[i]:
                        versuch_1.append([[total_data_21[e][i],table_entries[i]]])
            
                versuch_1.append(total_data_21[e][0])
                versuch_1.append(total_data_21[e][1])
                
                same_data_21 = "same_data_21"
                with open(str(same_data_21) + '.csv', 'a') as same_21:
                    writer = csv.writer(same_21)
                    writer.writerow([str(versuch[0]), "with preconditioner", str(versuch[1]), "has the same values like",\
                                      str(versuch_1), "on level", str(versuch[3]) , "and degree", str(versuch[2])])
                    
        minimal_data_21 ="minimal_data_21"
        with open(str(minimal_data_21) + '.csv', 'a') as minimal_21:
               writer = csv.writer(minimal_21)
               writer.writerow(["minimal end time:" , str(min(total_data_21, key=lambda x: x[4])) ])
               writer.writerow(["minimal average newton steps:"  , str(min(total_data_21, key=lambda x: x[5])) ])
               writer.writerow(["minimal average linesearch steps:" , str(min(total_data_21, key=lambda x: x[6])) ])
               writer.writerow(["minimal L2-error:" , str(min(total_data_21, key=lambda x: x[7])) ])
               writer.writerow(["minimal H1-error:" , str(min(total_data_21, key=lambda x: x[8])) ])
               writer.writerow(["minimal energy-error:" , str(min(total_data_21, key=lambda x: x[9]))])
        
    if total_data_22 != []:
        versuch = []
        for e in range(len(total_data_22)):
            versuch_1 = []
            table_entries = [str(total_data_22[e][0]), str(total_data_22[e][1]), \
                             str(total_data_22[e][2]), str(total_data_22[e][3]),\
                            "end time", "average newton steps", "average linesearch steps", "L2 error", "H1 error", "energy error"]
            if e == 0:
                versuch = total_data_22[e]
            else:
                for i in range(4,10):
                    if total_data_22[e][i] == versuch[i]:
                        versuch_1.append([[total_data_22[e][i],table_entries[i]]])
            
                versuch_1.append(total_data_22[e][0])
                versuch_1.append(total_data_22[e][1])
                
                same_data_22 = "same_data_22"
                with open(str(same_data_22) + '.csv', 'a') as same_22:
                    writer = csv.writer(same_22)
                    writer.writerow([str(versuch[0]), "with preconditioner", str(versuch[1]), "has the same values like",\
                                      str(versuch_1), "on level", str(versuch[3]) , "and degree", str(versuch[2])])
                    
        minimal_data_22 ="minimal_data_22"
        with open(str(minimal_data_22) + '.csv', 'a') as minimal_22:
               writer = csv.writer(minimal_22)
               writer.writerow(["minimal end time:" , str(min(total_data_22, key=lambda x: x[4])) ])
               writer.writerow(["minimal average newton steps:"  , str(min(total_data_22, key=lambda x: x[5])) ])
               writer.writerow(["minimal average linesearch steps:" , str(min(total_data_22, key=lambda x: x[6])) ])
               writer.writerow(["minimal L2-error:" , str(min(total_data_22, key=lambda x: x[7])) ])
               writer.writerow(["minimal H1-error:" , str(min(total_data_22, key=lambda x: x[8])) ])
               writer.writerow(["minimal energy-error:" , str(min(total_data_22, key=lambda x: x[9]))])
        
        
    

