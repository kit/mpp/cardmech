
from utility import reading as read
from utility import computing as comp
import math

def computeRMSE(t1,t2,l0path):
    evalPoints,ateP=read.readVTU(l0path)
    points1, at1 = read.readVTU(t1)
    points2, at2 = read.readVTU(t2)
    
    dict1={}
    dict2={}
    for index in range(len(points1)):
        p=str(points1[index][0])+', '+str(points1[index][1])+', '+str(points1[index][2])
        dict1[p]=at1[index]

    for index in range(len(points2)):
        p=str(points2[index][0])+', '+str(points2[index][1])+', '+str(points2[index][2])
        dict2[p]=at2[index]
    
    sumDiffSquares=0.0
    for p in evalPoints:
        key=str(p[0])+', '+str(p[1])+', '+str(p[2])
        diff=abs(dict1[key]-dict2[key])
        sumDiffSquares+=diff*diff
    rmse=math.sqrt((1/len(evalPoints))*sumDiffSquares)
    print(rmse)





if __name__=="__main__":     
    
    at1 ='../../../datafiles/BiVentricle/SI0001/vtu/AT_l0j0.vtu'
    at2 = '../../../datafiles/BiVentricle/SI0001/vtu/AT_l1j1.vtu'
    l0Path='../../data/rmsError/AT_l0j0.vtu'
    computeRMSE(at1,at2,l0Path)
    
    


