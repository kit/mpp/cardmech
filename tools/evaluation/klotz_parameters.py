#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# CAUTION:
# Use a smaller error barrier for the fine sweep!
# The functions "Klotz3DPlot" and "contourPlot" are only meaningful for a material dependent
# of two parameters´

###################
# Linear
###################
# param1 = lambda
# param2 = mu


###################
# Guccione
###################
# param1 = C_G
# param2 = bf
#
# range for coarse sweep (vgl. Kovacheva):
# C_G \in [0.3,10.3] in 1er steps
# b_f \in [2,62] in 20er steps
#
# range for fine sweep (vgl. Kovacheva):
# C_s \in [0.1,2.0] in 10er steps
# b_f \in [2,42] in 5er steps


import sys
import csv
import argparse

sys.path.append('../..')

import mpp.python.mppy as mppy
import matplotlib.pyplot as plt
import numpy as np


def KlotzCurve(volume, pressure):
    max_v = max(volume)
    min_v = min(volume)

    normalized_volumes = [(v - min_v) / (max_v - min_v) for v in volume]

    klotz = [28.2 * pow(v, 2.79) for v in normalized_volumes]  # Klotz pressure in mmHg
    adapted_klotz = [7.015 * v + 22.985 * pow(v, 4.322) for v in normalized_volumes]  # Adapted Klotz pressure in mmHg
    experimental_pressure = [t * 30 for t in pressure]  # Experimental pressure in mmHg

    return (normalized_volumes, experimental_pressure, adapted_klotz, klotz)


def parameter_sweep(mesh_name, kernel, mechLevel, materialName, ranges):
    logfile_name = mesh_name + '.log'
    errors = {}

    for elems in ranges:
        mpp = mppy.Mpp(project_name='CardMech',
                       executable='M++',
                       kernels=kernel,
                       mute=False)
        #mpp.clean_log()
        if materialName == "Guccione":
            running=mpp.run(kernel, config='elasticity/klotz', kwargs={
                "Mesh": mesh_name,
                "MechLevel": mechLevel,
                "ActiveMaterial": "Guccione",
                "GuccioneMat_C": elems[0],
                "GuccioneMat_bf": elems[1],
                "GuccioneMat_bs": 0.4 * elems[1],
                "GuccioneMat_bfs": 0.7 * elems[1],
                "QuasiCompressiblePenalty": "Ciarlet",
                "VolumetricPenalty": 20
            }
                    )
        elif materialName == "Linear":
            running=mpp.run(kernel, config='elasticity/klotz', kwargs={
                "Mesh": mesh_name,
                "MechLevel": mechLevel,
                "ActiveMaterial": "Linear",
                "LinearMat_Lambda": elems[0],
                "LinearMat_Mu": elems[1],
                "QuasiCompressiblePenalty": "None"
            }
                    )
        # check if the programm has run through
        if running == 0:
            mpp.parse_log(log_file=logfile_name)
            
            convergence=mpp.data["Static:   Convergence"][0]
            # check if programm is converging
            if convergence == 0 or 0.0 in convergence:
                continue
            else:
                time = mpp.data["Static:   Pressure Scale"][0]
                volume = mpp.data["Static:   LV-Volume"][0]
        
                normalized_volumes, experimental_pressure, adapted_klotz, klotz = KlotzCurve(volume, time)
        
                err = sum((experimental_pressure[i] - adapted_klotz[i]) * (experimental_pressure[i] - adapted_klotz[i]) for i in
                          range(len(volume)))
                errors[elems] = err
                print("errors in sweep " + str(errors))
                #mpp.clean_data()
        else:
            continue          
    return errors


# Useful after using a coarse parameter sweep to receive a upper and a lower bound
# for the material parameters with normalized errors < error_bound.
# -> can be helpful to find suitable ranges to start a fine parameter sweep
def find_barriers(normalized_error, error_bound):
    errors = []
    # search for the material parameters, which achieve a normalized error < error_bound
    for schluessel in normalized_error.keys():
        if normalized_error[schluessel] < error_bound:  ### < 0.01
            errors.append(schluessel)
       
    # find highest and lowest value for the material parameters
    lowest = []
    highest = []
    for k in range(len(errors[0])):
        lowest_params = min(errors[i][k] for i in range(len(errors)))
        lowest.append(lowest_params)
        highest_params = max(errors[i][k] for i in range(len(errors)))
        highest.append(highest_params)

    return (lowest, highest)


def KlotzPlot(mesh_name, normalized_volumes, experimental_pressure, adapted_klotz, klotz):
    plt.clf()
    plt.xlabel('Normalized Volume')
    plt.ylabel('Pressure [mmHg]')
    plt.xlim(0, 1)
    plt.ylim(0, 30)

    plt.plot(normalized_volumes, experimental_pressure, 'r', label='Experimental pressure curve')
    plt.plot(normalized_volumes, adapted_klotz, 'b', label='Adapted Klotz curve')
    plt.plot(normalized_volumes, klotz, 'g', label='Klotz curve')
    # plt.title("Comparison of the klotz curve, adapted klotz curve and the experimental achieved data")
    plt.legend(loc='upper left', frameon=True)
    plt.savefig(mesh_name + 'KlotzPlot.png')
    plt.show()


def Klotz3DPlot(mesh_name, materialName, x, y, z, all_ranges, needed_error):
    plt.clf()
    max_elem=max(e for e in needed_error.keys())

    fig = plt.figure()

    ax = plt.axes(projection='3d')
    surf = ax.plot_trisurf(x, y, z, cmap='jet', edgecolor='none', linewidth=0.5, antialiased=False)

    fig.colorbar(surf, shrink=0.95, aspect=5)

    # Adding labels
    if materialName == "Guccione":
        ax.set_xlabel('$C_{\mathrm{G}}$')
        ax.set_ylabel('$b_{\mathbf{f}}$')
    elif materialName == "Linear":
        ax.set_xlabel('$\lambda$')
        ax.set_ylabel('$\mu$')
    ax.set_zlabel('$F^{\text{obj}}$')

    ax.set_xlim(0, max_elem[0])
    ax.set_ylim(0, max_elem[1])
    ax.set_zlim(0, 1)
    ax.set_title('Normalized values of the objetive function $F^{\text{obj}}$')
    # ax.view_init(30, 45) # to rotate
    plt.savefig(mesh_name + 'Klotz3DPlot.png')

    plt.show()


def contourPlot(mesh_name, materialName, needed_range, needed_error, error_bound):
    plt.clf()
    min_error = min(needed_error, key=needed_error.get)

    # List of points in x axis
    XPoints = []

    # List of points in y axis
    YPoints = []

    # X and Y points
    for val in needed_range[0]:
        XPoints.append(val)
    
    for val1 in needed_range[1]:
        YPoints.append(val1)
        
    # Z values as a matrix
    ZPoints = np.ndarray((len(YPoints), len(XPoints)))

    for x in range(0, len(XPoints)):
        for y in range(0, len(YPoints)):
            ZPoints[y][x] = needed_error[(XPoints[x], YPoints[y])]

    for schluessel in needed_error.keys():
        if needed_error[schluessel] < error_bound:  ### < 0.01 for fine parameter sweep
            for i in range(len(schluessel)):
                if i == 0:
                    param1 = schluessel[i]
                if i == 1:
                    param2 = schluessel[i]
            if (param1 == min_error[0]) and (param2 == min_error[1]):
                plt.plot(param1, param2, 'or')
            else:
                plt.plot(param1, param2, 'ow')

    plt.contourf(XPoints, YPoints, ZPoints, 400, cmap='jet')
    # plt.plot(0,0)
    if materialName == "Guccione":
        plt.xlabel('$C_{\mathrm{G}}$')
        plt.ylabel('$b_{\mathbf{f}}$')
    elif materialName == "Linear":
        plt.xlabel('$\lambda$')
        plt.ylabel('$\mu$')

    plt.colorbar();
    plt.savefig(mesh_name + 'ContourPlot.png')
    plt.show()


def minimal_dataCSV(mesh_name, materialName, name, normalized_volumes, experimental_pressure, adapted_klotz, klotz):
    for ä in name:
        with open(mesh_name + name + materialName + '.csv', 'w') as data:
            writer = csv.writer(data)
            writer.writerow(
                ["normalized_volumes", "experimental_pressure", "optimal pressure (adapted klotz)", "klotz"])
            for l in range(len(experimental_pressure)):
                writer.writerow([normalized_volumes[l], experimental_pressure[l], adapted_klotz[l], klotz[l]])


def run_minimal(mesh_name, kernel, mechLevel, materialName, best_param):
    logfile_name = mesh_name + '.log'
    mpp = mppy.Mpp(project_name='CardMech',
                   executable='M++',
                   kernels=kernel,
                   mute=False)
    # mpp.clean_log()
    mpp.build()

    errors = {}
    normalized_volumes = []
    klotz = []
    adapted_klotz = []
    experimental_pressure = []

    mpp = mppy.Mpp(project_name='CardMech',
                   executable='M++',
                   kernels=kernel,
                   mute=False)
    # mpp.clean_log()
    if materialName == "Guccione":
        mpp.run(kernel, config='elasticity/klotz', kwargs={
            "Mesh": mesh_name,
            "MechLevel": mechLevel,
            "ActiveMaterial": "Guccione",
            "GuccioneMat_C": best_param[0],
            "GuccioneMat_bf": best_param[1],
            "GuccioneMat_bs": 0.4 * best_param[1],
            "GuccioneMat_bfs": 0.7 * best_param[1],
            "QuasiCompressiblePenalty": "Ciarlet",
            "VolumetricPenalty": 20
        }
                )
    elif materialName == "Linear":
        mpp.run(kernel, config='elasticity/klotz', kwargs={
            "Mesh": mesh_name,
            "MechLevel": mechLevel,
            "ActiveMaterial": "Linear",
            "LinearMat_Lambda": best_param[0],
            "LinearMat_Mu": best_param[1],
            "QuasiCompressiblePenalty": "None"
        }
                )

    mpp.parse_log(log_file=logfile_name)

    time = mpp.data["Static:   Pressure Scale"][0]
    volume = mpp.data["Static:   LV-Volume"][0]

    normalized_volumes, experimental_pressure, adapted_klotz, klotz = KlotzCurve(volume, time)

    err = sum((experimental_pressure[i] - adapted_klotz[i]) * (experimental_pressure[i] - adapted_klotz[i]) for i in
              range(len(volume)))
    errors[(best_param)] = err
    KlotzPlot(mesh_name, normalized_volumes, experimental_pressure, adapted_klotz, klotz)

    minimal_dataCSV(mesh_name, "Guccione", "minimal data", normalized_volumes, experimental_pressure, adapted_klotz,
                    klotz)
    # mpp.clean_data()


def GenerateCSV(mesh_name, materialName, name, needed_error):
    for ä in name:
        with open(mesh_name + name + materialName + '.csv', 'w') as data:
            writer = csv.writer(data)
            writer.writerow(["material parameters", name])
            for schluessel in needed_error.keys():
                writer.writerow([schluessel, needed_error[schluessel]])


def GenerateCSV_minimalErrors(mesh_name, materialName, name, needed_error, error_bound):
    for ä in name:
        with open(mesh_name + name + materialName + '.csv', 'w') as data:
            writer = csv.writer(data)
            writer.writerow(["material parameter", name])
            for schluessel in needed_error.keys():
                if needed_error[schluessel] < error_bound:  ### < 0.01
                    writer.writerow([schluessel, needed_error[schluessel]])


def guccione_param_ranges(min_vals=(0.3, 2), max_vals=(11.3, 82), steps=(11, 4)):
    c_diff = (max_vals[0] - min_vals[0]) / steps[0]
    b_diff = (max_vals[1] - min_vals[1]) / steps[1]
    for c_step in range(0, steps[0] + 1):
        for b_step in range(0, steps[1] + 1):
            yield min_vals[0] + c_step * c_diff, min_vals[1] + b_step * b_diff

def param_range(min_val, max_val, step):
    diff = (max_val - min_val) / step
    for step_i in range(0, step + 1):
        yield min_val + step_i * diff  
        
        
def main():
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser('Python interface to M++')

    # Run options
    parser.add_argument('--kernel', type=int, default=4,
                        help='file names to be converted')
    parser.add_argument('--name', type=str, default="BiventricleDirichlet", #"KovachevaBiventricle"
                        help='file names to be converted')
    parser.add_argument('--mechlevel', type=str, default="0",#"1,
                        help='file names to be converted')
    
    args = parser.parse_args()
    mpp = mppy.Mpp(project_name='CardMech',
                   executable='M++',
                   kernels=args.kernel,
                   mute=False)
    #mpp.clean_log()
    mpp.build()

    normalized_volumes = []
    klotz = []
    adapted_klotz = []
    experimental_pressure = []
    used_errorBound = 0.1
    mesh_name = args.name
    mechLevel = args.mechlevel

    usedMaterial = "Guccione"
    minVals=(0.1,2)
    maxVals=(2.0,42)
    Steps=(10,8)
    all_ranges = [param for param in guccione_param_ranges(minVals, maxVals, Steps)]
    parameterRanges = [[param1 for param1 in param_range(minVals[0], maxVals[0], Steps[0])],
                    [param2 for param2 in param_range(minVals[1], maxVals[1], Steps[1])]]

    errors = parameter_sweep(mesh_name, args.kernel, mechLevel, usedMaterial, all_ranges)

    max_error = max(e for e in errors.values())
    print("max error " + str(max_error))

    x, y, z = [], [], []
    normalized_error = {}

    for schluessel in errors.keys():
        x.append(schluessel[0])
        y.append(schluessel[1])
        z.append(errors[(schluessel)] / max_error)
        normalized_error[(schluessel)] = errors[(schluessel)] / max_error
        
    print("normalized error " + str(normalized_error))    
    lowest, highest = find_barriers(normalized_error, used_errorBound)

    GenerateCSV(mesh_name, usedMaterial, "LeastSquareErrors", errors)
    GenerateCSV(mesh_name, usedMaterial, "normalized_error", normalized_error)
    GenerateCSV_minimalErrors(mesh_name, usedMaterial, "minimal_errors", normalized_error, used_errorBound)

    min_error = min(normalized_error, key=normalized_error.get)
    print("minimal error " + str(min_error))

    contourPlot(mesh_name, usedMaterial, parameterRanges, normalized_error, used_errorBound)
    run_minimal(mesh_name, args.kernel, mechLevel, usedMaterial, min_error)

    Klotz3DPlot(mesh_name, usedMaterial, x, y, z, all_ranges, normalized_error)

    print("max error " + str(max_error))
    print("errors " + str(errors))
    print("normalized error " + str(normalized_error))
    print("lowest " + str(lowest))
    print("highest " + str(highest))

    print("minimal error " + str(min_error))
