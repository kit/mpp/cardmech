#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 22 19:38:27 2022

@author: laura
"""

import sys
import csv
import os
import matplotlib.pyplot as plt
import argparse
import numpy as np
from math import sqrt

def concatenate():
    append = {"yes", "no"}
    concatenate_input = input("Do you want to concatenate the tables? ")
    if concatenate_input not in append: 
        print("Please choose one of the following answers: yes or no!") 
        concatenate_input = concatenate()
    return concatenate_input 


def chose_material():
    possible_material = {"Linear", "Bonet", "Holzapfel"}
    material = input("Which material do you want to look at? ")
    if material not in possible_material: 
        print("Please choose one of the following materials: Linear, Bonet or Holzapfel!") 
        material = chose_material()
    return material   

def chose_values():
    possible_values = {"Step", "ApexY", "ApexX","H1Norm(gamma_u)", "L2Norm(gamma_u)",
                         "FrobeniusNorm(gamma_u)", "ApexZ", "H1Norm(gamma_v)", "RA-Volume",
                         "LA-Volume", "LV-Volume", "MyocardThickness", "FrobeniusNorm(gamma_v)", 
                         "RV-Volume", "MyocardStrain", "L2AvgNorm(u)", "Time", "FrobeniusNorm(v)",
                         "L2Norm(gamma_v)", "Geometry-Volume", "H1Norm(u)", "L2Norm(u)", 
                         "EnergyNorm(u)", "FrobeniusNorm", "L2Norm(v)", "H1Norm(v)"}
    values = input("Which value do you want to look at? ")
    if values not in possible_values: 
        print("Please choose one of the following datas: Step, ApexY, ApexX," \
              "H1Norm(gamma_u), L2Norm(gamma_u), FrobeniusNorm(gamma_u), ApexZ,"\
                  "H1Norm(gamma_v), RA-Volume, LA-Volume, LV-Volume, MyocardThickness,"\
                      "FrobeniusNorm(gamma_v), RV-Volume, MyocardStrain, L2AvgNorm(u)," \
                          "Time, FrobeniusNorm(v),L2Norm(gamma_v), Geometry-Volume,"\
                              "H1Norm(u), L2Norm(u), EnergyNorm(u), FrobeniusNorm,"\
                                  "L2Norm(v) or H1Norm(v)!") 
        values = chose_values()
    return values  
            
def write_dat_file(name, datadict):
    with open(str(name + '.dat'), 'w') as output_file:
        datalist = []
        N=0
        datastring = ""
        for dataname, data in datadict.items():
            datalist.append(data)
            N = len(data)
            datastring += dataname + ","
        output_file.write(str(datastring[:-1] + "\n"))

        M=len(datalist)
        for n in range(N):
            datastring = ""
            for m in range(M):
                datastring+=str(datalist[m][n]) + ","
            output_file.write(str(datastring[:-1] + "\n"))
            
            
def generate_table(polynomialdegree, data_value, name, lists, concatenate, nb_t, needed_tables):
    values = {"t": time1}
    N = len(time1)
    for i in range(len(lists)):
        values.update({str(i): lists[i]})

    errors = [0 for _ in range(len(lists))]
    for n in range(N):
        for i in range(len(lists)-1):
            errors[i] += 1/N * (lists[i][n] - lists[-1][n])*(lists[i][n] - lists[-1][n])

    errorstring = data_value + "Errors "
    ratestring = data_value + "Rates "
    for i in range(len(lists) - 1):
        errors[i] = sqrt(errors[i])
        errorstring += "& $" + str(round(errors[i], 6))+ "$ "
        if i>0:
            ratestring += "& $" + str(round(errors[i-1] / errors[i],2)) + "$ "
    print(errorstring)
    print(ratestring)
    if concatenate == "yes":
        if nb_t == 1:
            with open(str(name) + "_Tables" + ".tex", "w") as o:
                o.write("\\begin{table}\n ")
                o.write("\centering \n ")
                o.write(" \\begin{tabular}[t]{c|")
                for j in range(len(lists)):
                    o.write("c")
                    if j == len(lists) - 1:
                        o.write("}  \n")
                o.write(errorstring + " \\\ \n")
                o.write(ratestring + "\\\ \n")
        elif nb_t < needed_tables:
            with open(str(name) + "_Tables" + ".tex", "a") as o:
                o.write(errorstring + " \\\ \n")
                o.write(ratestring + "\\\ \n")
        elif nb_t == needed_tables:
             with open(str(name) + "_Tables" + ".tex", "a") as o:
                o.write(errorstring + " \\\ \n")
                o.write(ratestring + "\\\ \n")
                o.write("\\end{tabular}\n ")
                o.write("\\end{table}\n ")
    elif concatenate == "no":
        with open(str(name) + "_Tables" + ".tex", "w") as o:
                o.write("\\begin{table}\n ")
                o.write("\centering \n ")
                o.write(" \\begin{tabular}[t]{c|")
                for j in range(len(lists)):
                    o.write("c")
                    if j == len(lists) - 1:
                        o.write("}  \n")
                o.write(errorstring + " \\\ \n")
                o.write(ratestring + " \n")
                o.write("\\end{tabular}\n ")
                o.write("\\end{table}\n ")
    write_dat_file(name + data_value + polynomialdegree, values)            
    
    
    
    

sys.path.append('../..')
needed_tables = int(input("How many tables do you need? "))
concatenate = concatenate()
nb_t = 1
k = 1
append_tables = []

while nb_t <= needed_tables:
    Level = []
    data_test = []
    test_data = []
    test_time = []
    needed_logfiles = int(input("How many Logfiles do you want to read out for the " + str(nb_t) + ". table? "))
    k = 1
    print("All following choices are made for the " + str(nb_t) + ". table!")
    material = {}
    material = chose_material()
    polynomialdegree = "P"
    polynomialdegree = polynomialdegree + input("Which Polynomial Degree? ")
    print("possible input: Step, ApexY, ApexX, H1Norm(gamma_u), \
              L2Norm(gamma_u), FrobeniusNorm(gamma_u), ApexZ,\
                  H1Norm(gamma_v), RA-Volume ,LA-Volume, LV-Volume, \
                      MyocardThickness, FrobeniusNorm(gamma_v), RV-Volume,\
                          MyocardStrain, L2AvgNorm(u), Time, FrobeniusNorm(v),\
                              L2Norm(gamma_v), Geometry-Volume, H1Norm(u),\
                                  L2Norm(u), EnergyNorm(u), FrobeniusNorm,\
                                      L2Norm(v), H1Norm(v)")
    values = chose_values()
    while k <= needed_logfiles:
        k += 1
        logfiles="/home/laura/Schreibtisch/cardmech/data/activestrain/AS-LeftVentricle"
        # material = {}
        # material = chose_material()
        #print(material)

        logfiles = logfiles + material
        level = "L"
        l = input("Which Level? ")
        level =  level + str(l)
        Level.append(int(l))
        logfiles = logfiles + level
        # polynomialdegree = "P"
        # polynomialdegree = polynomialdegree + input("Which Polynomial Degree? ")
        logfiles = logfiles + polynomialdegree
        data = []


        with open(logfiles, 'r') as g: 
            time = "Time" + " = ["
            t = len(time)
            for line1 in g:
                start1 = line1.find(time)
                if start1 == -1:
                    continue
                end1 = line1.find("]")   
                time = (line1[start1 + t:end1])
                time1 = time.split(",")
                time1 = [float(v) for v in time1]
                values_test = {"t": time1}
    
    
        #FullCardmechInfo
            parser = argparse.ArgumentParser()
            args = parser.parse_args()
    
            # number = int(input("how much data you need? "))
    
            # if number == 26:
            # #TODO: Auslesen von allen Daten
            #     print("test")
            # else:    
            # i = 1
            # while i <= number:
            value = {}
            # i += 1
            # print("possible input: Step, ApexY, ApexX, H1Norm(gamma_u), \
            #   L2Norm(gamma_u), FrobeniusNorm(gamma_u), ApexZ,\
            #       H1Norm(gamma_v), RA-Volume ,LA-Volume, LV-Volume, \
            #           MyocardThickness, FrobeniusNorm(gamma_v), RV-Volume,\
            #               MyocardStrain, L2AvgNorm(u), Time, FrobeniusNorm(v),\
            #                   L2Norm(gamma_v), Geometry-Volume, H1Norm(u),\
            #                       L2Norm(u), EnergyNorm(u), FrobeniusNorm,\
            #                           L2Norm(v), H1Norm(v)")
            # values = chose_values()

            value = values  + " = ["
            j = len(value)

            with open(logfiles, 'r') as f: 
                for line in f:
                    start = line.find(value)
                    if start == -1:
                        continue
                    end = line.find("]")   
                    Step = (line[start + j:end])
                    Step1 = Step.split(",")
                    Step1 = [float(u) for u in Step1]
                
                
                    data_test.append(Step1)
                    test_data.append(data)
                    data.extend(data_test) 
                    for w in range(len(Level)):
                        values_test.update({str(w): data[w]})
            
            #str(material) + str(values) + str(polynomialdegree)
    if concatenate == "yes":        
        generate_table(polynomialdegree, values, str(material) +  "_Errors", data, concatenate, nb_t, needed_tables)    #"GuccioneL2NormP1"
    elif concatenate == "no":  
        generate_table(polynomialdegree, values, str(material) + str(values) + str(polynomialdegree), data, concatenate, nb_t, needed_tables)
    nb_t += 1 
 
    
