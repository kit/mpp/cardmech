import numpy as np
import math
import matplotlib.pyplot as plt
from utility import reading as read
from utility import computing as comp
from utility import plotting as plotting

folderPathIE ='../../../datafiles/Level0Tests/ImplicitEuler/ImpEuler0001SAT/'
pathStandardLI ='../../../datafiles/Level0Tests/LinearImplicit/HexaBRLinearImplicit0001IextSmoothInSpaceAndTime/'
pathStandardG ='../../../datafiles/Level0Tests/Godunov/HexaBRSplittingGodunov0001IextSmoothTimeASpace/'
folderPathGodunov ='../../../datafiles/Level0Tests/Godunov/G0001SATTheta1/'#G0001SATInODE/'
#folderPathGodunov=pathStandardG
extrapolatePath='../../../datafiles/convergenceTest3D/HexaBRLinearImplicit0001IextSmoothInSpaceAndTime/'
#folderPathLinImp ='../../../datafiles/Level0Tests/LinearImplicit/HexaBRLinearImplicit0001IextSmoothInSpaceAndTime/'
folderPathLinImp ='../../../datafiles/Level0Tests/LinearImplicit/LI00001IextSATMultCm/'
folderPathLinImp='../../../datafiles/Level0Tests/LinearImplicit/LI0001SATQ18/'

#fPG ='../../../datafiles/Level0Tests/Tetra/G0001SAT/'
#fPLI ='../../../datafiles/Level0Tests/Tetra/LI0001SAT/'
lList = [0,1,2,3,4]
tlist = [0,1,2]#,3,4,5]
latexPath=folderPathLinImp + 'tex/'
#latexPath='../../../datafiles/Level0Tests/tex/'
colorList = ['black','red','purple','orange','green','brown','blue']
substep=1

def plotVTwoTimeIntMethodsTime(p,l,tP,fP1,fP2):
    resize=1
    for j in tlist:
        (time,V)= read.getDataFromTXT(fP1,read.createFilename(l,j,1),p)
        (time_G,V_G)= read.getDataFromTXT(fP2,read.createFilename(l,j,1),p)
        for i in range(len(V)):
            V[i] = float(V[i])
            V_G[i] = float(V_G[i])
        (time,V)=plotting.resizeData(time,V,resize*2**(j),len(time))
        (time_G,V_G)=plotting.resizeData(time_G,V_G,resize*2**(j),len(time_G))
        labelname = ' (LI) $  j='+str(j)+'$'
        labelname_G = ' (G) $  j='+str(j)+'$'

        plt.plot(time,V,linestyle='--', label = labelname,color=colorList[j])
        plt.plot(time_G,V_G, label = labelname_G,linestyle='dotted',color=colorList[j])
    
    V_intyinfty_LinImpl =comp.getFullExtrapolate(lList[-1],tlist[-1],substep,p,start_dt,0.03,extrapolatePath)
    Cutted_V_intyinfty_LinImpl=[]
    for t in range(len(time)):
        Cutted_V_intyinfty_LinImpl.append(V_intyinfty_LinImpl[t])
    labelname='$\\V^{\\infty,\\infty}$'
    
    plt.plot(time,Cutted_V_intyinfty_LinImpl,label = labelname,color='black')

    plotname ='BRDiffTime'+ 'l'+str(l)+'P'+str(p)
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential (mV)')
    #plt.legend()
    #plt.savefig(plotPath + plotname + '.png')
    
    import tikzplotlib
    tikzplotlib.save(tP + plotname + ".tex")
    #plt.show()
    plt.close()

def plotDiffOfTwo(p,l,tP,path1,path2,name):
    resize=1
    for j in tlist:
        (time1,V1)= read.getDataFromTXT(path1,read.createFilename(l,j,1),p)
        (time2,V2)= read.getDataFromTXT(path2,read.createFilename(l,j,1),p)
        Diff=[]
        for i in range(len(V1)):
            Diff.append(float(V1[i])-float(V2[i]))
        (time1,Diff)=plotting.resizeData(time1,Diff,resize*2**(j),len(time1))
        labelname='  $  j='+str(j)+'$'
        plt.plot(time1,Diff,label=labelname,color=colorList[j])    

    plotname ='BRDiffTimeofTwo'+name+ 'l'+str(l)+'P'+str(p)
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential (mV)')
    plt.legend()
    #plt.savefig(plotPath + plotname + '.png')
    
    import tikzplotlib
    tikzplotlib.save(tP + plotname + ".tex")
    #plt.show()
    plt.close()
def plotIext(l):
    for j in tlist:
        (time,Iext_LI_standard)=read.getDataFromTXT('../../../datafiles/Level0Tests/LinearImplicit/HexaBRLinearImplicit0001IextSmoothInSpaceAndTime/',read.createFilename(l,j,1),11)
        (time,Iext_LI)=read.getDataFromTXT(folderPathLinImp,read.createFilename(l,j,1),11)
        (time_G,Iext_G)= read.getDataFromTXT(folderPathGodunov,read.createFilename(l,j,1),11)
        cutted_Iext_LI_standard=[]
        cutted_Iext_LI=[]
        cutted_Iext_G=[]
        cutted_time =[]
        length =40*2**j
        for i in range(1,length):
            cutted_Iext_LI_standard.append(Iext_LI_standard[i])
            cutted_Iext_LI.append(Iext_LI[i])
            cutted_Iext_G.append(Iext_G[i])
            cutted_time.append(time[i])
        plt.plot(cutted_time,cutted_Iext_LI,linestyle='--', color=colorList[j])
        #plt.plot(cutted_time,cutted_Iext_G,linestyle='dotted',color=colorList[j])
        plt.plot(cutted_time,cutted_Iext_LI_standard,color=colorList[j])
        
    plotname='DiffIext'+'Halve' #+'Int'
    import tikzplotlib
    tikzplotlib.save(latexPath + plotname + ".tex")
    #plt.show()
    plt.close()
if __name__=="__main__":
    fPLI=folderPathLinImp
    fPG=pathStandardG
    #extracts data from logfile into txt file
    #read.writeDataInFile(fPG,[0],tlist,substep)
    read.writeDataInFile(fPLI,[0],tlist,substep)
    #read.writeDataInFile(folderPathIE,[0],tlist,substep)
    (T,start_dt)=read.getTimeData(pathStandardLI,lList[0],tlist[0],substep)
    PList=[1,2,3,4]
    #plotIext(0)
    texPath=fPLI+'tex/'
    for p in PList:
        plotVTwoTimeIntMethodsTime(p,0,texPath,fPLI,fPG)
        #plotDiffOfTwo(p,0,texPath,pathStandardG,folderPathGodunov,'GminusGTheta1')
        #plotDiffOfTwo(p,0,texPath,pathStandardLI,folderPathLinImp,'LIminusLIQuad')
    
