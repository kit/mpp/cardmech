#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  8 11:41:24 2023

@author: laura
"""

import os
import sys
sys.path.append('../../')
import matplotlib.pyplot as plt
import re

#from utility.plotting import Plot 


#foldername = "80schritte.log"
#path = "/home/laura/Schreibtisch/GAMMPaper/data/preparationsGamm/"

def readDataFromLog(pE, foldername):
    checkIfFolderExists(pE)
    lv = []
    rv = []
    t = []
    s = []
    volume = []
    
    f = open(foldername)
    for l in f:
            l = l.strip()
            lv_volume = re.search("LV-Volume\s*=\s*([+\-eE\d]+\.{0,1}[\-e\d]*)",l)
            if lv_volume:
                lv.append(lv_volume.group(1))        
                
                
            rv_volume = re.search("RV-Volume\s*=\s*([+\-eE\d]+\.{0,1}[\-e\d]*)",l)
            if rv_volume:
                rv.append(rv_volume.group(1))  
               
                
            time = re.search("Time\s*=\s*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if time:
                t.append(time.group(1))
               
            step = re.search("Step\s*=\s*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if step:
                s.append(step.group(1))
    volume.append(lv)
    volume.append(rv)
    f.close()
    return volume, t, s
    
    
def convertToFloat(liste):
    if len(liste) > 2:
        for i in range(len(liste)):
            liste[i] = float(i)
    elif len(liste) == 2:
        for i in range(len(liste)):
            for j in range(len(liste[i])):
                liste[i][j] = float(liste[i][j])
    return liste


def plotVolumeCurves(volume, volumeLabel, timeOrStep, timeOrStepLabel,lvRvLabelcV, coupledProblem, cV, dCS):
    #plt.clf()
    plt.xlabel(str(timeOrStepLabel))
    plt.ylabel(str(volumeLabel))
    plt.xlim(0, 800)
    plt.ylim(60, 180)
    for i in range(len(volume)):
        plt.plot(timeOrStep, volume[i], label = lvRvLabel[i] + str(coupledProblem[i]) + " cV" + str(cV) + "dCV" + str(dCS))
        plt.legend( loc='lower right', frameon=True)#bbox_to_anchor=(1.8,0),
    plt.show()

        #plt.savefig(foldername + 'VolumePlot.png') #Todo anpassen an data/LogFileName
        


CoupledProblem = ["BiventricleCoarse","DeformedBiventricleCoarse"]
CoupledProblemDeformed = ["DeformedBiventricleCoarse"]
cell_values = [0] #, 1
deformConcentrationStretch = [0]#, 1

def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":    
    
    path='../data/preparationGamm/' 
    pathToMakeFolders=path 
    pathExperiment=""
    
    for cP in CoupledProblem:
        for cV in cell_values:
            for dCS in deformConcentrationStretch:
                foldername = 'DeformedRayleigh_l1.log'
                #foldername = path + pathExperiment + 'log' + str(cP) + '_cellV'+ str(cV) +'_deformCS' + str(dCS) + '.log'
                #foldernameDeformed = path + pathExperiment + 'log' + str(CoupledProblemDeformed[0]) + '_cellV' + str(cV) + '_deformCS' + str(dCS) + '.log'
    
                vol, dataTime, dataStep = readDataFromLog(pathExperiment, foldername)
                vol = convertToFloat(vol)
                dataStep = convertToFloat(dataStep)
                
                #volDeformed, dataTimeDeformed, dataStepDeformed = readDataFromLog(pathExperiment, foldernameDeformed)
                #volDeformed = convertToFloat(volDeformed)
                #dataStepDeformed = convertToFloat(dataStepDeformed)
                
                volumeLabel = "volume (ml)"
                lvRvLabel = ["LV volume", "RV volume"]
                plotVolumeCurves(vol, volumeLabel, dataStep, "Step", lvRvLabel, CoupledProblem, cV, dCS)
               # plotVolumeCurves(volDeformed, volumeLabel, dataStepDeformed, "Step", lvRvLabel, CoupledProblemDeformed, cV, dCS)






























