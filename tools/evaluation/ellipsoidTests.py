from utility import reading as read
from utility import computing as comp
from utility.plotting import Plot 
from utility.latexTables import Table
from utility.latexTables import TableWriter
from utility.documentWriter import DW
import Meshinfos


def PlotTwoMethods():
    lL=[1,2,3]
    tL=[0]
    sdt=0.0001
    endTime=0.1
    listEvaluationPoints=[1,2,3,4,5,6,7]
    eP='../../../data/cuttedEllipsoidBR/'
    fSISVI=eP+'SISVI0001/'
    fSIOC=eP+'SIOC0001/'
    
    read.writeDataInFile(fSISVI,[3],[0],1)
    #read.writeDataInFile(fSIOC,[1,2,3],[0],1)
    
    fL=[fSISVI,fSIOC]
    nL=['\\SISVI','\\SIOC']        
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=0
    for p in listEvaluationPoints:
         plot.plotVTwoTimeIntMethodsSpace(fSISVI,fSIOC,p,j,eP,'DiffSVIOCSpacej'+str(j)+'P'+str(p))
         #plot.plotVTwoTimeIntMethodsTimeRef(fSI,fLI,p,3,fLI,6,4,eP,'DiffSILITimel3'+'P'+str(p))


def PlotTwoMethodsBIVentricle():
    lL=[0,1,2,3]
    tL=[0]
    sdt=0.0001
    endTime=0.16
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    eP='../../../data/svi_cWieners/'
    fSISVI=eP+'SISVI/'
    fSIOC=eP+'SIOC/'
    
    read.writeDataInFile(fSISVI,[0,1,2,3],[0],1)
    read.writeDataInFile(fSIOC,[0,1,2,3],[0],1)
    
    fL=[fSISVI,fSIOC]
    nL=['\\SISVI','\\SIOC']        
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=0
    for p in listEvaluationPoints:
         plot.plotVTwoTimeIntMethodsSpace(fSISVI,fSIOC,p,j,eP,'DiffSVIOCSpacej'+str(j)+'P'+str(p))
    
if __name__=="__main__":
    language='d'

    PlotTwoMethods()
    #PlotTwoMethodsBIVentricle()
