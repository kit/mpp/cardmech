
import math
import matplotlib.pyplot as plt
from utility import plotting as plotting
from utility import reading as read
from utility import computing as comp

folderPath ='../../../datafiles/convergenceTest/SpaceTimeHexaTest025t0001BRTwoDimLinearImplicit/'
case ='2Dim'
logPath = folderPath + 'log/'
dataPath = folderPath + 'data/'
plotPath = folderPath + 'plot/'
latexPath = folderPath + 'tex/'
lList = [0,1,2,3,4,5]
tlist = [0,1,2,3,4]
start_dx = 0.25

listEvaluationPoints =[1,2,3,4,5,6,7,8]
totalNumberOfEvaluationPointsWithNorms = 9
substep=1
cutT= 0.3
tableAlignment = 'c'
infvalue ='not finished'




def getPoints():
    f = open(logPath+'log_' +createFilename(lList[0],tlist[0],substep))
    eP = [None] * (totalNumberOfEvaluationPointsWithNorms-2)
    for l in f:
        m = re.search( "v(\d+)\(\s*([+\-eE\d]+\.{0,1}\d*)\,\s*([+\-eE\d]+\.{0,1}\d*)\,\s*([+\-eE\d]+\.{0,1}\d*)\)\s*=\s*(\-{0,1}\d+\.{0,1}\d*[e+\-\d*]*)",l)
        if m:
            eP[int(m.group(1))] =[str("%8.2f"%(float(m.group(2)))),str("%8.2f"%(float(m.group(3))))]
    f.close()
    return (eP)
def timename(j):
    return '{:.10f}'.format(start_dt/(2**j)).rstrip("0") [2:]
def getnumberOfNormSteps():
    n=cutT/start_dt+1
    return int(n)
def tableRowsOrder(number):
    block =''
    block+=tableAlignment + '|'
    for i in range(number-1):
        block += tableAlignment + tableAlignment +'|'
    block +=tableAlignment + tableAlignment
    return block
def writeDataInFile():
    fn = ''
    for l in range(lList[0],len(lList)+lList[0]):
        for j in range(tlist[0],len(tlist)+ tlist[0]):
            fn = createFilename(l,j,substep)
            print(fn)
            extractDataFromLog(fn)
            
def createFilename(l,j,m):   
    return "l{}j{}m{}".format(l,j,m)

def extractDataFromLog(fn):
    f = open(logPath+"log_"+fn)
    steps = OrderedDict()
    current_step = 0
    steps[current_step] = OrderedDict()
    model =False

    for l in f:
        l = l.strip()
        m=re.search("Model: .................... LinearImplicit",l)
        if m:
            model=True
        
        if model:
            m = re.search("# Linear implicit (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        else:
            m = re.search("# Euler step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                steps[m.group(3)] = OrderedDict()
                current_step = m.group(3)
        
        
        m = re.search("\|\|v\|\|_L2 = \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
        if m:
            steps[current_step][10] = m.group(1)
            
        m = re.search("\|\|v\|\|_H1 = \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
        if m:
            steps[current_step][11] = m.group(1)
            
        
        m = re.search("v(\d+)\(\s*([+\-eE\d]+\.{0,1}\d*)\,\s*([+\-eE\d]+\.{0,1}\d*)\,\s*([+\-eE\d]+\.{0,1}\d*)\)\s*=\s*(\-{0,1}\d+\.{0,1}\d*[e+\-\d*]*)", l)
        if m:        
            steps[current_step][m.group(1)] = m.group(5)
        
    f.close()       

    out_f = open(dataPath +fn +'.txt', "w")

    for k,v in steps.items():
        listVValues =list(v.values()) 
        out_f.write("{} {}\n".format('{:.8f}'.format(float(k)), " ".join(listVValues)))
        
    out_f.close()
def getDataFromTXT(filename, row):
    list = []
    time = []
    values = []
    f=open(str(dataPath + filename + '.txt'), 'r')
    for l in f:
        line = l.split(" ")
        list.append(line)
    f.close()
    #print(filename)
    for i in range(0,len(list)):
        time.append(float(list[i][0]))
        values.append(float(list[i][row]))

    return (time,values)    

def L2(V):
    n = len(V)
    numberOfNormSteps = getnumberOfNormSteps()
    k = int((n-1)/(numberOfNormSteps-1))
    if k ==1:
        sum = -0.5 * start_dt* (V[0]*V[0]);
        sum += -0.5 * start_dt * (V[-1]*V[-1]);
        for i in range(0,len(V)):
            sum += start_dt*(V[i]*V[i])
    else:
        evaluateV =[]
        for i in range(numberOfNormSteps):
            evaluateV.append(float(V[k*i]))
        sum = -0.5 * start_dt* (evaluateV[0]*evaluateV[0]);
        sum += -0.5 * start_dt * (evaluateV[-1]*evaluateV[-1]);
        for i in range(0,len(evaluateV)):
            sum += start_dt*(evaluateV[i]*evaluateV[i])
    return(1000*math.sqrt(sum))#"%8.4f"%
def L2Diff(l_f, j_f, m_f,l_ref,j_ref, m_ref,p):
    (time_ref,reference) = getDataFromTXT(createFilename(l_ref,j_ref,m_ref),p)
    (time_f,f)= getDataFromTXT(createFilename(l_f,j_f,m_f),p)
    if len(time_ref)== 0 or len(time_f)== 0:
        return infvalue
    
    else:
        numberOfNormSteps = getnumberOfNormSteps()
        k_ref = int((len(reference)-1)/(numberOfNormSteps-1))
        k_f = int((len(f)-1)/(numberOfNormSteps-1))
        Diff =[]
        for i in range(numberOfNormSteps):
            Diff.append(float(f[k_f*i])-float(reference[k_ref*i]))
        norm =L2(Diff)
        #norm =L2Simpson(Diff)
        return(norm)#"%8.4f"%
def convergencePlotTime(l):
    print("Plot Time convergence")
    for p in listEvaluationPoints:
        error=[]
        time =[]
        for j in range(len(tlist)-1):
            time.append(1000.0*start_dt/(2**j))
            error.append(float(L2Diff(l,j,substep,l,j+1,substep,p)))
        if p<=totalNumberOfEvaluationPointsWithNorms-2:
            labelname = 'P'+str(p)
        elif p==totalNumberOfEvaluationPointsWithNorms-1:
            labelname = '$\\L2$'
        elif p==totalNumberOfEvaluationPointsWithNorms:
            labelname = 'H1'
        plt.plot(time,error,'s-', markersize=5,label=labelname)
    order = time
    order =[2*i for i in order]
    plt.plot(time,order,'k--',linewidth=3.5,label ='order 1')
    order =[15*i*i for i in order]
    #print(order)
    plt.plot(time,order,'r--',linewidth=3.5,label ='order 2')
    plt.xlabel('${\\vartriangle}t$ in ms')
    plt.ylabel('$\\L2$ distance ')
    #plt.title('For Beeler Reuter and ${\\vartriangle}x='+str(start_dx/(2**l))+'$ mm')
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.yscale('log', basey=2)
    plt.xscale('log', basex=2)
    plotname ='ConvergenceTime'  + 'l'+ str(l)+'i'+str(substep)
    plt.savefig(plotPath + plotname + '.png')
    
    import tikzplotlib
    tikzplotlib.save(latexPath + plotname + ".tex")
    #plt.show()
    plt.close()
   
   
def convergencePlotSpace(j):
    timename = '{:.10f}'.format(start_dt/(2**j)).rstrip("0") [2:]
    print("Plot Space convergence")
    for p in listEvaluationPoints:
        error=[]
        space =[]
        for l in range(lList[0],len(lList)+lList[0]-1):
            space.append(start_dx/(2**l))
            error.append(float(L2Diff(l,j,substep,l+1,j,substep,p)))
        if p<=totalNumberOfEvaluationPointsWithNorms-2:
            labelname = 'P'+str(p)
        elif p==totalNumberOfEvaluationPointsWithNorms-1:
            labelname = '$\\L2$'
        elif p==totalNumberOfEvaluationPointsWithNorms:
            labelname = 'H1'
        plt.plot(space,error,'s-', markersize=5,label=labelname)
    order = space
    plt.plot(space,order,'k--',linewidth=3.5, label='order 1')
    plt.xlabel('${\\vartriangle}x$ in mm')
    plt.ylabel('$\\L2$ distance')
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    
    plotname ='ConvergenceSpace'+ 'j'+str(j)+'m'+str(substep)
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    #plt.title('For Beeler Reuter and ${\\vartriangle}t='+str(1000.0*start_dt/(2**j))+'$ ms')
    plt.yscale('log', basey=2)
    plt.xscale('log', basex=2)
    plt.savefig(plotPath + plotname + '.png')
    #plt.show()
    
    import tikzplotlib
    tikzplotlib.save(latexPath + plotname + ".tex")
    
    plt.close()
    
def WriteLatexSpace():
    with open(latexPath +'SpaceTableSdt'+str(timename(0))+'m'+str(substep)+case +'.tex', 'w') as output_file:
            output_file.write(writeBeginSpaceLongtable())
            for p in listEvaluationPoints:
                (time,V) = getDataFromTXT(createFilename(lList[-1],tlist[-1],substep),p)
                output_file.write(writeSpaceData(V,p))
            output_file.write(writeEndTable())
            
def writeBeginSpaceLongtable():
    block = ''
    block+= '\\begin{longtable}[H]{'+ tableRowsOrder(len(tlist))+'} \n'
    block+= '\\caption{Convergence in space for fixed substep $m='+str(substep)+'$ and $t_\\text{beg}= '+ str(start_dx) + '$s with $f_{j,\\ell} = \\| V^{j,\\ell+1}  - V^{j,\\ell} \\|_\\L2$  the  differences of calculated potentials. The rate on level $\\ell$ on fixed  time step $j$ is computed as $\\operatorname{log}_2\\frac{f_{j,\\ell}}{f_{j,\\ell+1}}$.}' + '\\\\ \n' #\\frac{  }{\\|V^{J,L}\\|_\\L2}  normalized
    block += '$\\ell$'
    for i in tlist:
        block +=  '&$ f_{' + str(i) + ',\\ell}$ &  ' 
    block+= '\\\\\n'
    block+='\\hline\n'
    return block
def writeEndTable():
    block = ''
    block +='\\end{longtable}\n'
    return block

def writeSpaceData(V,p):
    Points=getPoints()
    block = ''
    if p<totalNumberOfEvaluationPointsWithNorms-1:
        block+='\\textbf{P'+str(p)+'}'#+'('+Points[p-1][0]+', '+Points[p-1][1]+')'
    elif p==totalNumberOfEvaluationPointsWithNorms-1:
        block+= '$\\textbf{L}_2$'
    elif p== totalNumberOfEvaluationPointsWithNorms:
        block+= '$\\textbf{H}_1$'
    else:
        block+='TEST'
    for j in range(len(tlist)):
        block +=" & & "
    block+= '\\\\\n'
    for l in range(lList[0],len(lList)+lList[0]-1):
        block += str(l) 
        for j in tlist:
            block += " & "+ str("%8.2f"%(float(L2Diff(l,j,substep,l+1,j,substep,p))))+ ' & '  #/float(L2(V))
        block+= '\\\\\n'
        level = l
        if level!= lList[-1]-1:
            block +='  '
            for j in tlist:
                value1 = float(L2Diff(level,j,substep,level+1,j,substep,p))
                value2 = float(L2Diff(level+1,j,substep,level+2,j,substep,p))
                #print(p)
                #print(value1)
                #print(value2)
                block += "&  & \\textbf{"+str("%8.2f"%(math.log(value1/value2,2)))+ "}"
            block+='\\\\\n'
    block+='\\hline\n'
    return block
def WriteLatexTime():
    with open(latexPath +'TimeTableSdt'+str(timename(0))+'m'+str(substep)+case +'.tex', 'w') as output_file:
            output_file.write(writeBeginTimeLongtable())
            for p in listEvaluationPoints:
                (time,V) = getDataFromTXT(createFilename(lList[-1],tlist[-1],substep),p)
                output_file.write(writeTimeData(V,p))
            output_file.write(writeEndTable())

def writeBeginTimeLongtable():
    block = ''
    block+= '\\begin{longtable}[H]{'+ tableRowsOrder(len(lList))+'} \n'
    block+= '\\caption{Convergence in time for fixed substep $m='+str(substep)+'$  and $t_\\text{beg}=  '+ str(start_dt) + '$ s with $g_{j,\\ell} = \\| V^{j+1,\\ell}  - V^{j,\\ell} \\|_\\L2$  the differences of calculated potentials. The rate on timestep $j$ on fixed  level $\\ell$ is computed as $\\operatorname{log}_2\\frac{g_{j,\\ell}}{g_{j+1,\\ell}}$.}' + '\\\\ \n' #\\frac{  }{\\|V^{J,L}\\|_\\L2}  normalized
    block += '$j$'
    for l in range(lList[0],len(lList)+lList[0]):
        block +=  '&$ g_{j,' + str(l) + '}$ &  ' 
    block+= '\\\\\n'
    block+='\\hline\n'
    return block

def writeTimeData(V,p):
    Points=getPoints()
    block = ''
    if p<totalNumberOfEvaluationPointsWithNorms-1:
        block+='\\textbf{P'+str(p)+'}'#+'('+Points[p-1][0]+', '+Points[p-1][1]+')'
    elif p==totalNumberOfEvaluationPointsWithNorms-1:
        block+= '$\\textbf{L}_2$'
    elif p== totalNumberOfEvaluationPointsWithNorms:
        block+= '$\\textbf{H}_1$'
    else:
        block+='TEST'
    for l in range(lList[0],len(lList)+lList[0]):
        block +=" & & "
    block+= '\\\\\n'
    for j in tlist[:(tlist[-1])]:
        block += str(j) 
        for l in range(lList[0],len(lList)+lList[0]):
            block += " & "+ str("%8.2f"%(float(L2Diff(l,j,substep,l,j+1,substep,p))))+ ' & '  #/float(L2(V))
        block+= '\\\\\n'
        t = j
        if t!= tlist[-1]-1:
            block +='  '
            for l in range(lList[0],len(lList)+lList[0]):
                value1 = float(L2Diff(l,t,substep,l,t+1,substep,p))
                value2 = float(L2Diff(l,t+1,substep,l,t+2,substep,p))
                block += "&  & \\textbf{"+ str("%8.2f"%(math.log(value1/value2,2)))+ "}" 
            block+='\\\\\n'
    block+='\\hline\n'
    return block
def LatexTables():
    WriteLatexSpace()
    WriteLatexTime()
    
def pointsInPlotForFixlAndjAndm(l,j,m):
    for p in listEvaluationPoints[:(totalNumberOfEvaluationPointsWithNorms-2)]:
        (time,V)= getDataFromTXT(createFilename(l,j,m),p)
        for i in range(len(V)):
            V[i] = float(V[i])
        labelname = 'P'+str(p)
        #print(V)
        if j>1:
            (time,V)=plotting.resizeData(time,V,100,len(time))  #such that the tex file is not too big...
        plt.plot(time,V, label = labelname)

    
    #plt.title('BeelerReuter '+', l='+str(l)+ ', dt='+str(start_dt/(2**j))+ ', m= '+str(m))
    plotname ='BRl' +str(l)+ 'j'+str(j)+'m'+str(m)+'AllPoints'
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential (mV)')
    plt.legend()
    import tikzplotlib
    tikzplotlib.save(latexPath + plotname + ".tex")
    plt.savefig(plotPath + plotname + '.png')
    #plt.show()  
    plt.close()
def plotSpaceForPointp(p,j,m):
    for l in lList:
        (time,V)= getDataFromTXT(createFilename(l,j,m),p)
        for i in range(len(V)):
            V[i] = float(V[i])
        labelname ='$\\ell = ' + str(l) +'$'
        #labelname = '${\\vartriangle}x='+str(start_dx/(2**l))+'$ mm'
        #print(V)
        (time,V)=plotting.resizeData(time,V,100,len(time))  #such that the tex file is not too big...
        plt.plot(time,V, label = labelname)
        
    #plt.title('Space convergence for BeelerReuter with dt='+str(start_dt/(2**j))+ ', m= '+str(m)+ 'in P'+str(p))
    plotname ='BRSpaceConvergence'+ 'j'+str(j)+'m'+str(m)+'P'+str(p)
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential (mV)')
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    #plt.legend()
    #import tikzplotlib
    #tikzplotlib.save(latexPath + plotname + ".tex")
    #plt.savefig(plotPath + plotname + '.png')
    plt.show()  
    plt.close()
def plotTimeForPointp(p,l,m):
    for j in tlist:
        (time,V)= getDataFromTXT(createFilename(l,j,m),p)
        for i in range(len(V)):
            V[i] = float(V[i])
        labelname = '$j = '+str(j) + '$'
        #labelname = '${\\vartriangle}t='+str(1000.0*start_dt/(2**j))+'$ ms'
        #print(V)
        if j>1:
            (time,V)=plotting.resizeData(time,V,100,len(time))  #such that the tex file is not too big...
        plt.plot(time,V, label = labelname)
        
    #plt.title('Time convergence for BeelerReuter with dx='+str(start_dx/(2**l))+ ', m= '+str(m)+ 'in P'+str(p))
    plotname ='BRTimeConvergence'+ 'l'+str(l)+'m'+str(m)+'P'+str(p)
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential (mV)')
    #plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.legend()
    import tikzplotlib
    tikzplotlib.save(latexPath + plotname + ".tex")
    plt.savefig(plotPath + plotname + '.png')
    #plt.show()  
    plt.close()  
if __name__=="__main__":
    #extracts data from logfile into txt file
    writeDataInFile()
    (T,start_dt)=getTimeData()
    #pointsInPlotForFixlAndjAndm(lList[0],tlist[0],1)
    #pointsInPlotForFixlAndjAndm(lList[-1],tlist[-1],1)
    #plotSpaceForPointp(9,tlist[-1],1)
    #plotTimeForPointp(9,lList[-1],1)
    plotSpaceForPointp(6,tlist[-1],1)
    #plotTimeForPointp(6,lList[-1],1)
    #plotSpaceForPointp(3,tlist[-1],1)
    #plotTimeForPointp(3,lList[-1],1)
    #convergencePlotTime(lList[-1])
    #convergencePlotSpace(tlist[-1])
    #LatexTables()
