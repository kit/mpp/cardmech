from utility import reading as read
from utility import computing as comp
from utility.plotting import Plot 
from utility.latexTables import Table
from utility.latexTables import TableWriter
from utility.documentWriter import DW
import Meshinfos

pathBV='../../../datafiles/BiVentricle/'
pathEll='../../../datafiles/TestsFromHoreka/'
def getPointNamebyNumber(p,inRef=False):
        if inRef:
            if p==1:
                return '\\peins'
            elif p==2:
                return '\\pzwei'
            elif p==3:
                return '\\pdrei'
            elif p==4:
                return '\\pvier'
            elif p==5:
                return '\\pfuenf'
            elif p==6:
                return '\\psechs'
            elif p==7:
                return '\\psieben'
            elif p==8:
                return '\\LtwoSpace'
        else:    
            if p==1:
                return '\\Peins'
            elif p==2:
                return '\\Pzwei'
            elif p==3:
                return '\\Pdrei'
            elif p==4:
                return '\\Pvier'
            elif p==5:
                return '\\Pfuenf'
            elif p==6:
                return '\\Psechs'
            elif p==7:
                return '\\Psieben'
            elif p==8:
                return '\\LtwoSpace'
            
def table2():
    #histplot benchmark ellipsoid
    path = pathEll+'ActivationTimeSI/vtu/AT_l0j4.vtu'
    savePath=pathEll+'ActivationTimeSI/tex/HistEllipsoidl0.tex'
    
    comHistplot(path,savePath)
def figure2():
    LI = pathEll+'LI0001SAT'
    #plotallPoints, 
    
    #convergence in time
    
    #convergence in space
    i=0
    
def table4():
    #differences in z6 and z7  in space and time LI and SI
    i=0
def table5():
    #extrapolates in z6 and z7  in space and time LI and SI
    i=0
 
def figure3():
#referece solutions
    i=0

def table6():
    #ICI SVI and cpu times
    lL=[0,1,2,3,4]
    tL=[4]
    sdt=0.0001
    endTime=0.05
    listEvaluationPoints=[]
    SI=pathEll+'ActivationTimeSI/'
    fL=[SI]
    x1=(0.000000, 5.166110, -14.656000)
    y1=(0.000000, 6.971190, -10.556600)
    x2=(-2.57154012, 0.00000000, -15.81130028)
    y2=(-5.61778021, 0.00000000, -10.10529995)
    pairlist=[[x1,y1],[x2,y2]]
    nL=['myocardial wall', 'inner surface']
    distL=[4.492067,6.535686]
    
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    #plot.CVPlot(SI,pairlist,nL,distL,4)
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,fL,listEvaluationPoints)
    sP='images/'
    tabWriter.writeCVPlotAndTable(SI,sP,pairlist,nL,distL,4)

def table7():
    #histplot bi-ventricle
    path = pathBV+'SI0004/vtu/AT_l0j0.vtu'
    savePath=pathBV+'SI0004/tex/HistBiVentriclel0.tex'
    
    Meshinfos.comHistplot(path,savePath)

def figure5():
    #work precision diagrams
    eP=pathEll+''
    
    fLI=eP+'LI0001SAT/'
    fSI=eP+'SI0001SAT/'
    fIE =eP+'IE0001SAT/'
    fG=eP +'GPDE0001SATIE/'
    sdt=0.0001
    endTime=0.03
    listEvaluationPoints=[6]
    nL=['\\IESVI','\\LISVI','\\SISVI','\\GPDE']
    fL=[fIE,fLI,fSI,fG]
    j=4
    l=4
    lL=[2,3,4,5]
    point=6
    plot=Plot([2,3,4,5],[2,3,4,5,6],sdt,endTime,1,listEvaluationPoints)
    
    sP='images/'
    L=6
    J=5
    ydescription='$\\errSI$'
    (nameS,minprocS)=plot.workPrecisionInSpace(L,J,j,fL,nL,fSI,eP,point,ydescription)
    (nameT,minprocT)=plot.workPrecisionInTime(L,J,l,fL,nL,fSI,eP,point,ydescription,minprocS)
    print(minprocS,minprocT)
    filename=eP+'tex/'+'WPSIFigP'+str(point)
    caption='\\IESVI, \\LISVI, \\SISVI, ~and \GPDE~with fixed  time step size $j='+str(j)+'$ (left) and $\ell='+str(l)+'$ (right) comparing the CPU time scaled to $\\#$procs$='+str(minprocS)+'$ (left) and $\\#$procs$='+str(minprocT)+'$ (right) to the error $\\errSI('+getPointNamebyNumber(point,True)+')$'
    label='\\label{fig:WPSI'+str(point)+'}'
    with open(filename+'.tex', 'w') as output_file:
        output_file.write('\\begin{figure}[h]\n')
        output_file.write('\\centering\n')
        output_file.write('\\resizebox{7cm}{!}{\\input{'+sP+nameS+'}}\n')
        output_file.write('\\resizebox{7cm}{!}{\\input{'+sP+nameT+'}}\n')
        output_file.write('\\caption{'+caption+'.}\n')
        output_file.write(label+'\n')
        output_file.write('\\end{figure}\n')

            
def table8():
    lL=[0,1,2,3,4]
    tL=[0,1,2,3,4]
    SI=pathBV+'SI0004/'
    sdt=0.0004
    endTime=0.16
    listEvaluationPoints=[]
    l0Path=pathBV+'SI0004/vtu/AT_l0j0.vtu'
    #read.changeNumberOFCellsiInVTU(SI+'data/',[4],[1,2,3,4],1)

    #plotTact:
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    backupArray=[[-1.0,59.295870358255655,57.96067339027709,57.5545764529468,57.468872878475665],[49.749577731126486,45.46817896679911,43.798303227582146,43.28796384626254,43.16367877136338],[44.95291943209599,40.81325902629804,39.09870508453703,38.55494553911038,38.41666873598738],[43.36173025251009,38.80735467113771,36.91194880075053,36.30816906164418,36.15129271842806],[42.583927617282406,37.81610107773048,35.8441826475743,35.229766544910895,35.07516596693433]]

    ValueArray=plot.MeanTactSpace(SI,l0Path,backupArray)
    #print('len value array: ', len(ValueArray))
    print('Plot finished')
    #tables tact
    L=4
    J=4
    savePath='images/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[SI],listEvaluationPoints)
    tabWriter.writeMeanTactTable(SI,l0Path,savePath,L,J,SI,backupArray)
    
if __name__=="__main__":
    #table2() #x
    #figure2()
    #table4()
    #table5()
    #figure3()
    table6()
    #table7() #x
    #figure5()
    #table8()
