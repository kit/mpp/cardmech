from utility import reading as read
from utility import computing as comp
import math
import matplotlib.pyplot as plt
import numpy as np



def comHistplot(f,sP,language='e'):
    cells, points =read.readVTUgetPointsAndCells(f)
    edges ={}
        
    for c in cells:
        c0 =points[c[0]]
        for i in range(1,len(c)):
            ci=points[c[i]]
            key=makeKeyName(c0,ci)
            keyreverse = makeKeyName(ci,c0)
            if key not in edges and keyreverse not in edges:
                edges[key]=math.dist(c0,ci)
        for i in range(2,len(c)):
            c1 =points[c[1]]
            ci=points[c[i]]
            key=makeKeyName(c1,ci)
            keyreverse = makeKeyName(ci,c1)
            if key not in edges and keyreverse not in edges:
                edges[key]=math.dist(c1,ci)
        c2=points[c[2]]
        c3=points[c[3]]
        key=makeKeyName(c2,c3)
        keyreverse = makeKeyName(c3,c2)
        if key not in edges and keyreverse not in edges:
            edges[key]=math.dist(c2,c3)
    edgelist=[]
    for key in edges:
        edgelist.append(edges[key])
        
    edgelist.sort()
    print('min',edgelist[0],'max',edgelist[-1])
    
    ranges=10
    n, bins, patches=plt.hist(edgelist,bins=np.linspace(0,int(edgelist[-1])+1,15), weights=np.ones(len(edgelist)) / len(edgelist)*100, edgecolor='black') 
    if language=='e':
        plt.xlabel('edge length ${\\vartriangle}x$ (mm)')
        plt.ylabel('number of edges ($\\%$)')
    elif language=='d':
        plt.xlabel('Kantenlänge ${\\vartriangle}x$ (mm)')
        plt.ylabel('Anzahl der Kanten ($\\%$)')
    else: 
         print('language: '+str(language)+' is not defined!')
    #plt.show()
    
    import tikzplotlib
    tikzplotlib.save(sP)
    plt.close()
    
    
            
def makeKeyName(x,y):
    return str(x[0])+', '+str(x[1])+', '+str(x[2]) +' to '+ str(y[0])+', '+str(y[1])+', '+str(y[2])
if __name__=="__main__":     
    #biventricle
    path = '../../../datafiles/BiVentricle/SI0001/vtu/AT_l0j0.vtu'
    savePath='../../../cardpub/Monodomain/images/HistBiVentriclel0.tex'
    comHistplot(path,savePath)
    #ellipsoid
    path = '../../../datafiles/TestsFromHoreka/ActivationTimeSI/vtu/AT_l0j4.vtu'
    savePath='../../../cardpub/Monodomain/images/HistEllipsoidl0.tex'
    
    comHistplot(path,savePath)
