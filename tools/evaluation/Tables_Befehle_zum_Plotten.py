

                ##########################################################
                # Befehle zum Lesen und Plotten einer CSV-Datei in Python
                ##########################################################
                    
                filename = "GMRES_0_SSOR.csv"    
                with open(filename) as csv_file:
                    reader = csv.reader(csv_file, delimiter=',')
                    header_row = next(reader)
                    for index, column_header in enumerate(header_row):
                        print(index, column_header)
                    pressuresteps, newtonsteps = [], []
                    for row in reader:
                        pressuresteps.append(float(row[0]))
                        value = float(row[1])
                        newtonsteps.append(value)
                plt.subplot(224)
                plt.plot(pressuresteps,newtonsteps)
                plt.xlabel('Pressure steps')
                plt.ylabel('Newton steps')
                plt.plot(pressure_steps, newton_steps, label = {k,j,i})
                plt.legend(bbox_to_anchor=(0,1,1,0), loc='lower left')
     
                ##########################################################
                # "Normales" Plotten, ohne CSV-Datei,
                ##########################################################
                # Plot pressure steps auf newton steps
                ##########################################################

                plt.subplot(221) 
                plt.xlabel('Pressure steps')
                plt.ylabel('Newton steps')
                plt.plot(pressure_steps, newton_steps, label = {k,j,i})
                plt.legend(bbox_to_anchor=(0,1,1,0), loc='lower left')
            
                
                ##########################################################
                # Plot pressure steps auf duration
                ##########################################################
                
                plt.subplot(222)
                plt.xlabel('Pressure steps')
                plt.ylabel('duration')
                plt.plot(pressure_steps, duration, label = {k,j,i})
                plt.legend(bbox_to_anchor=(0,1,1,0), loc='lower left')
                
                ##########################################################
                # Plot pressure steps auf linesearch steps
                ##########################################################
                    
                plt.subplot(223)
                plt.xlabel('Pressure steps')
                plt.ylabel('lineseachr_steps')
                plt.plot(pressure_steps, linesearch_steps, label = {k,j,i})
                plt.legend(bbox_to_anchor=(0,1,1,0), loc='lower left')
            

    
    plt.show()
    