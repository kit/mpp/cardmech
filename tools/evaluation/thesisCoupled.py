from utility import reading as read
from utility import computing as comp
from utility.plotting import Plot 
from utility.latexTables import Table
from utility.latexTables import TableWriter
import matplotlib.pyplot as plt


eP='../../../data/Kopplung/'
fSISVI=eP+'SI0004/'
fSIOC=eP+'SIOC0004/'
#fSIICI=eP+'SIICI0004/'
fGS=eP+'GS0004/'
threshold=-50.0

fMech0008='../../../data/Kopplung/TimeStepTest/Mech0008/'
fMech0016='../../../data/Kopplung/TimeStepTest/Mech0016/'
fMech0032='../../../data/Kopplung/TimeStepTest/Mech0032/'
fMechAdaptive16='../../../data/Kopplung/TimeStepTest/adaptive16/'
fMechAdaptive32='../../../data/Kopplung/TimeStepTest/adaptive32/'
fMechAdaptive='../../../data/Kopplung/TimeStepTest/adaptiveTest/'


def DifferencePerPointTabs(case):
    lL=[1,2,3,4]
    tL=[]
    if case=='tact':
        tL=[0,1,2,3]
    elif case=='VDiffInt':
        tL=[1,2,3,4]
    elif case=='Ca':
        tL=[1,2,3,4]
    elif case =='Gamma':
        tL=[0,1,2,3]
    elif case=='Volumes':
        tL=[0,1,2,3]
    else:
        print('tL not defined for case:',case)
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[1,2,3,4,5,6,7,8]
    #read.writeCaDataInFile(fGS,[4],[4],1,len(listEvaluationPoints),True)
    
    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    #nL=['\\SIOC']
    #fL=[fSIOC]
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/coupled/'
    l=4
    j=3
    if case=='Ca':
        j=4
        
    if case =='Volumes':
        tabWriter.writeDifferencesVolumes(l,j,fL,nL,savePath)
    else:
        tabWriter.writeDifferenceTablePerPoint(l,j,fL,nL,savePath,case,threshold,'Coupled')
 
def tactValuesAndErrorPerPoint():
    lL=[1,2,3,4]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    listEvaluationPoints=[2,3,4,6,7,8,9] #Achtung, bei z5 kommt zufälligerweise die gleiche Aktivierungszeit für l=4 und j=3,4 raus. Daher kann  für diesen Punkt keine extrapolierte berechnet werden...
    
    nL=['\\SIOC']
    fL=[fSIOC]
    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/coupled/'
    tabWriter.writetactAndErrorPerPoint(fL,nL,savePath,threshold,'Coupled') 

def plotTactErrorPerPoint():
    lL=[1,2,3,4]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    savePath='../../../thesis-lindner/thesis/images/erstmal/coupled/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=tL[-1]
    l=lL[-1]
    for p in listEvaluationPoints:
        plot.tactErrorInSpace(fL,nL,threshold,p,j,savePath,fSIOC,'Coupled')
        plot.tactErrorInTime(fL,nL,threshold,p,l,savePath,fSIOC,'Coupled')

def plotExtraError():
    lL=[1,2,3,4]
    tL=[1,2,3,4]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    listEvaluationPoints=[8]
        
    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    savePath='../../../thesis-lindner/thesis/images/erstmal/coupled/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=3
    l=lL[-1]
    for p in listEvaluationPoints:
        plot.VIntErrorInSpace(fL,nL,threshold,p,j,savePath,fSIOC)
        plot.VIntErrorInTime(fL,nL,threshold,p,l,savePath,fSIOC)
def plotExtraErrorVolumes():
    lL=[1,2,3,4]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[1,2]
        
    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    savePath='../../../thesis-lindner/thesis/images/erstmal/coupled/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=3
    l=lL[-1]
    for p in listEvaluationPoints:
        plot.VolumesErrorInSpace(fL,nL,p,j,savePath,fSIOC)
        plot.VolumesErrorInTime(fL,nL,p,l,savePath,fSIOC)

def ErrorExtraAndInt(case):
    lL=[1,2,3,4]
    tL=[1,2,3,4]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    if case=='Gamma':
        tL=[0,1,2,3]
    

    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    #nL=['\\SIOC']
    #fL=[fSIOC]
    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/coupled/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    if case=='Ca':
        tabWriter.writeErrorExtraandIntTab(eP,[fSIOC],fL,savePath,['\\SIOC'],nL,0.0002,case)
    else:
        print('case ',case,' for ErrorExtraAndInt not defined')
    
def ErrorExtraVInt(case):
    lL=[1,2,3,4]
    tL=[1,2,3,4]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    if case=='Gamma':
        tL=[0,1,2,3]
    

    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    #nL=['\\SIOC']
    #fL=[fSIOC]
    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/coupled/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    if case=='Ca':
        tabWriter.writeErrorExtraVIntTab(eP,fL,savePath,nL,0.0002,case)
    elif case=='Gamma':
        tabWriter.writeErrorExtraVIntTab(eP,fL,savePath,nL,-0.0005,case)
    else:
        tabWriter.writeErrorExtraVIntTab(eP,fL,savePath,nL,threshold,case,'Coupled')

def ErrorExtra(case):
    lL=[1,2,3,4]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.6
    if case=='Volumes':
        listEvaluationPoints=[1,2]
    else:
        listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    if case=='Ca' or case =='Gamma':
        fL=fL=[fSIOC]
        nL=nL=['\\SIOC']
    savePath='../../../thesis-lindner/thesis/tables/erstmal/coupled/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    if case=='Volumes':
        tabWriter.writeVolumesExtraError(eP,fL,savePath,nL,False)
        tabWriter.writeVolumesExtraError(eP,fL,savePath,nL,True)
    else:
        tabWriter.writeErrorExtraTab(eP,fL,savePath,nL,case)



def plotSchwinger():
    lL=[1,2,3,4]
    tL=[0]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    listEvaluationPoints=[8]
    
    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    
    savePath='../../../thesis-lindner/thesis/images/erstmal/coupled/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=3
    for p in listEvaluationPoints:
        plot.Unterschwinger(fL,nL,threshold,p,j,savePath)
        plot.Oberschwinger(fL,nL,threshold,p,j,savePath)

def PlottingForAdaptive(path,name):
    lL=[2]
    tL=[0,1,2]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    
    l=2
    savePath='../../../thesis-lindner/thesis/images/coupled/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    plot.plotAdaptive(l,path,name,savePath)

def MechTimeStepTestTable():     
    lL=[2]
    tL=[0,1,2]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    nL=['${\\vartriangle}t^{\\mathrm{m}}={\\vartriangle}t^{\\mathrm{e}}$ ','${\\vartriangle}t^{\\mathrm{m}}=0.8$','${\\vartriangle}t^{\\mathrm{m}}=1.6$', '$\\dtMechAd=1.6$',' $\\dtMechAd=3.2$',]
    fL=[fSISVI,fMech0008,fMech0016,fMechAdaptive16,fMechAdaptive32]
    numberofAdaptive=2

    #read.writeVolumeDataInFile(fMechAdaptive16,lL,[0,1,2],1)
    #read.writeVolumeDataInFile(fMechAdaptive32,lL,[0,1,2],1)

    #read.writeDataInFile(fMechAdaptive16,lL,[0,1,2],1)
    #read.writeDataInFile(fMechAdaptive32,lL,[0,1,2],1)

    #read.writeCaDataInFile(fMech0032,lL,[0,1,2],1,len(listEvaluationPoints),True)
    #read.writeCaDataInFile(fMechAdaptive16,lL,[0,1,2],1,len(listEvaluationPoints),True)
    #read.writeCaDataInFile(fMechAdaptive32,lL,[0,1,2],1,len(listEvaluationPoints),True)

    #read.writeGammaDataInFile(fMechAdaptive16,lL,[0,1,2],1,len(listEvaluationPoints),True)
    #read.writeGammaDataInFile(fMechAdaptive32,lL,[0,1,2],1,len(listEvaluationPoints),True)

    l=2
    savePath='../../../thesis-lindner/thesis/tables/erstmal/coupled/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    for p in listEvaluationPoints:
        tabWriter.writeMechTimeStepTestTable(l,p,fL,nL,threshold,savePath,numberofAdaptive)
        
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    #plot.volumesDifferentAlgs(nL,fL,l.j,)
def WorkPrecisonDiagram(case):
    lL=[1,2,3,4]
    tL=[1,2,3,4]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    
    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    j=3
    l=4

    point=8
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    sP='images/erstmal/'
    savePath='../../../thesis-lindner/thesis/images/erstmal/'
    
    ydescription='$\\errorVact$'
    
    (nameS,minprocS)=plot.workPrecisionInSpaceExtra(fSIOC,j,fL,nL,savePath,point,ydescription,case)
    (nameT,minprocT)=plot.workPrecisionInTimeExtra(fSIOC,l,fL,nL,savePath,point,ydescription,case,minprocS)
    print(minprocS,minprocT)
    filename='../../../thesis-lindner/thesis/images/erstmal/'+'WPCoupled'+case+'FigP'+str(point)
    if case=='tact'or case=='tactGrob':
        caption='Aufwand-Genauigkeit-Diagramm der Aktivierungszeit $\\tact$ des \\emm~berechnet mit \\SISVI, \\SIOC~und \GPDE~mit festem Zeitlevel $j='+str(j)+'$ (links) und festem Ortslevel $\ell='+str(l)+'$ (rechts). Der Aufwand wird anhand der auf $\\#$procs$='+str(minprocS)+'$ (links) und $\\#$procs$='+str(minprocT)+'$ (rechts) skalierten Rechenzeiten und die Genauigkeit wird durch den Fehler $\\errortact$ bestimmt '
    else:
        caption='Aufwand-Genauigkeit-Diagramm des Verlaufs der Transmembranspannung $\\V$ des \\emm~berechnet mit \\SISVI, \\SIOC~und \GPDE~mit festem Zeitlevel $j='+str(j)+'$ (links) und festem Ortslevel $\ell='+str(l)+'$ (rechts). Der Aufwand wird anhand der auf $\\#$procs$='+str(minprocS)+'$ (links) und $\\#$procs$='+str(minprocT)+'$ (rechts) skalierten Rechenzeiten und die Genauigkeit wird durch den Fehler $\\errorVact$ bestimmt '
    label='\\label{fig:WPCoupled'+case+'P'+str(point)+'}'
    with open(filename+'.tex', 'w') as output_file:
        output_file.write('\\begin{figure}[h]\n')
        output_file.write('\\centering\n')
        output_file.write('\\resizebox{7.5cm}{!}{\\input{'+sP+nameS+'}}\n')
        output_file.write('\\resizebox{7.5cm}{!}{\\input{'+sP+nameT+'}}\n')
        output_file.write('\\caption{'+caption+'.}\n')
        output_file.write(label+'\n')
        output_file.write('\\end{figure}\n')    
def AllValueTabs():
    lL=[1,2,3,4]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]

    savePath='../../../thesis-lindner/thesis/tables/erstmal/coupled/'
    
    nL=['\\SISVI','\\SIOC','\\GPDE']
    fL=[fSISVI,fSIOC,fGS]
    
    #nL=['\\SIOC']
    #fL=[fSIOC]
    
    #read.writeDataInFile(fSISVI,[4],[1],1)
    #read.writeDataInFile(fSIOC,lL,[0],1)
    #read.writeDataInFile(fGS,[4],[4],1)
    
    
    
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    
    tabWriter.writeTactTable(fL,nL,savePath,threshold,'Coupled')
    print('wrote tact Tab')
    tabWriter.writeAPDTable(fL,nL,savePath,threshold,'Coupled')
    print('wrote apd Tab')
    #tabWriter.writeIntervalLtwoNormTab(fL,nL,savePath,threshold)


########################################################################################################################
#plotting
###########################################################################################################################
def plotPotential():
    lL=[1,2,3,4]
    tL=[0,1,2,3,4]#,3]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    
    j=3
    resize=10
    
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    plot.plotSpaceForPointp(fSIOC,9,3,1,1)
    for p in listEvaluationPoints:
        #plot.plotSpaceForPointp(SIOC,p,j,1,resize,'SpaceConPurkinje') 
        plot.plotSpaceForPointp(fSIOC,p,j,1,resize,'CoupledSpaceV','') 
        plot.plotTimeForPointp(fSIOC,p,4,1,resize,'CoupledTimeV','')

def plotCa():
    lL=[1,2,3,4]
    tL=[0,1,2,3,4]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    listEvaluationPoints=[8]
    #read.writeCaDataInFile(fSIOC,lL,tL,1,len(listEvaluationPoints),True)
    #read.writeCaDataInFile(fSISVI,[4],[2,3,4],1,len(listEvaluationPoints),True)
    #read.writeCaDataInFile(fGS,lL,tL,1,len(listEvaluationPoints),True)
    j=3
    resize=10
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        #plot.plotSpaceForPointp(SIOC,p,j,1,resize,'SpaceConPurkinje')
        plot.plotSpaceForPointp(fSIOC,p,j,1,resize,'SpaceCa','Ca')
        plot.plotTimeForPointp(fSIOC,p,4,1,resize,'TimeCa','Ca')
        #plot.plotSpaceForPointp(fGS,p,j,1,resize,'GSSpaceCa','Ca') 
        #plot.plotTimeForPointp(fGS,p,4,1,resize,'GSTimeCa','Ca')
    
def plotGamma():
    lL=[1,2,3,4]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    listEvaluationPoints=[10]
    #read.writeGammaDataInFile(fSIOC,lL,tL,1,len(listEvaluationPoints),True)
    #read.writeGammaDataInFile(fSISVI,lL,tL,1,len(listEvaluationPoints),True)
    #read.writeGammaDataInFile(fSISVI,[4],[1],1,len(listEvaluationPoints),True)
    #read.writeGammaDataInFile(fGS,lL,tL,1,len(listEvaluationPoints),True)
    j=3
    resize=10
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=3
    l=4
    m=1
    ########################
    #Steigung ||g-f||_infty
    ########################
    for j in range(0,3):
        (time,V)= read.getDataFromTXT(fSISVI,read.createFilename(l,j,m)+'Gamma',10)
        steigung=[]
        for i in range(len(V)-1):
            #print(V[i]-V[i+1],abs(V[i]-V[i+1]))
            steigung.append(abs(V[i]-V[i+1])/abs(time[i]-time[i+1]))
        
        #plt.plot(time[1:],steigung, label = 'steigung in j='+str(j),linewidth=2.5)
        
    
    for p in listEvaluationPoints:
        (time,V)= read.getDataFromTXT(fSIOC,read.createFilename(l,j,m)+'Gamma',p)
        #plt.plot(time,V, label = 'SIOC',linewidth=2.5)
        (time,V)= read.getDataFromTXT(fSISVI,read.createFilename(l,j,m)+'Gamma',p)
        #plt.plot(time,V, label = 'SI',linewidth=2.5)
        (time,V)= read.getDataFromTXT(fGS,read.createFilename(l,j,m)+'Gamma',p)
        #plt.plot(time,V, label = 'GS',linewidth=2.5)
        plt.legend()
        plt.title('z_'+str(p))
        plt.show()
        #plt.close()
    
    #for p in listEvaluationPoints:
        #plot.plotSpaceForPointp(fSIOC,p,j,1,resize,'SpaceGamma','Gamma') 
        #plot.plotTimeForPointp(fSIOC,p,4,1,resize,'TimeGamma','Gamma')
        #plot.plotSpaceForPointp(fSISVI,p,j,1,resize,'SpaceGamma','Gamma') 
        #plot.plotTimeForPointp(fSISVI,p,4,1,resize,'TimeGamma','Gamma')
        #plot.plotSpaceForPointp(fGS,p,j,1,resize,'SpaceGamma','Gamma') 
        #plot.plotTimeForPointp(fGS,p,4,1,resize,'TimeGamma','Gamma')
    #plot.plotSpaceForPointp(fSIOC,10,j,1,resize,'SpaceGammaINfty','GammaInfty')
    
def plotSpaceConVolume(path,j,lL,plotname,resize):
    lvRvLabel=["LV volume", "RV volume"]
    for l in lL:
       vol, dataTime, dataStep = read.VolumeDataFromLog(path, read.createFilename(l,j,1))
       resizeLV=[]
       resizeRV=[]
       resizeVol=[]
       resizetime=[]
       number=resize*2**(j)
       
       for i in range(len(dataTime)):
            if i%number==0:
                resizetime.append(dataTime[i])
                resizeLV.append(vol[0][i])
                resizeRV.append(vol[1][i])
       resizeVol.append(resizeLV)
       resizeVol.append(resizeRV)
       
       for i in range(len(resizeVol)):
           plt.plot(resizetime, resizeVol[i], label = lvRvLabel[i]+', $\ell='+str(l)+'$',linewidth=2.5)
    plt.legend(bbox_to_anchor=(1.05,1.00), loc="upper left")
    plt.xlabel('Zeit (s)')
    plt.ylabel('Volumen (ml)')
    #plt.show()
    import tikzplotlib
    tikzplotlib.save(path+'/tex/'+plotname+ ".tex")
    plt.close()
    
def plotVolumen():
    lL=[1,2,3,4]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.6
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    
    #read.writeVolumeDataInFile(fSIOC,lL,tL,1,True)
    #read.writeVolumeDataInFile(fSISVI,[4],[1],1,True)
    #read.writeVolumeDataInFile(fGS,lL,tL,1,True)
    j=2
    l=4
    resize=10
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    #LV
    plot.plotSpaceVolumes(fSIOC,j,resize) 
    plot.plotTimeVolumes(fSIOC,l,resize)
    
if __name__=="__main__":
    language='d'
    
    
    #AllValueTabs()
    
    ##################
    #tact
    ###################
    diffcase ='tact'
    #DifferencePerPointTabs(diffcase)
    #tactValuesAndErrorPerPoint()
    #plotTactErrorPerPoint()
    #WorkPrecisonDiagram('tact')
    #################
    #V
    #################
    #plotPotential()
    diffcase='VDiffInt'
    #DifferencePerPointTabs(diffcase)
    #ErrorExtraVInt('')
    #plotExtraError()
    #plotSchwinger()
    #WorkPrecisonDiagram('')
    
    #####################
    #Ca
    #####################
    case='Ca'
    #plotCa()
    #ErrorExtra(case)
    #ErrorExtraVInt(case)
    diffcase='Ca'
    #DifferencePerPointTabs(diffcase)
    #ErrorExtraAndInt(case)
    ########################
    #gamma_f
    ################################
    #plotGamma()
    diffcase='Gamma'
    #DifferencePerPointTabs(diffcase)
    case='Gamma'
    #ErrorExtra(case)
    #ErrorExtraVInt(case)
    
   
    #########################
    #Volumes
    ########################
    plotVolumen()
    #DifferencePerPointTabs('Volumes')
    
    #ErrorExtra('Volumes')
    #plotExtraErrorVolumes()
    
    ################################
    #MechTimeStep listEvaluationPoints
    ##################################   
    #MechTimeStepTestTable()
    #PlottingForAdaptive(fMechAdaptive16,'${\\vartriangle}t^{\\mathrm{m,beg}}=1.6\,$ms')
    #PlottingForAdaptive(fMechAdaptive32,'A 32')
    
    
    ######################################
    
    #print(read.getActivaitionTimeFromLog(4,4,1, fSIOC, 8,threshold,False,''))
    #print(read.getActivaitionTimeFromLog(4,4,1, fSIOC, 8,threshold,True,''))
    #(t,V)=read.getDataFromTXT(fSIOC,read.createFilename(4,4,1),8)
    #(tact,id_tact)=comp.getTactPerCase(t,V,threshold,'',False)
    #print(4,tact,t[id_tact])
    #(t,V)=read.getDataFromTXT(fSIOC,read.createFilename(4,3,1),8)
    #(tact,id_tact)=comp.getTactPerCase(t,V,threshold,'',False)
    #print(4,tact,t[id_tact])
