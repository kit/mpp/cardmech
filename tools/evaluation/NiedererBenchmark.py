from utility import reading as read
from utility import computing as comp
from utility.plotting import Plot
from utility.latexTables import TableWriter

    
def plotActivationTimesDiagonal(path,tdisc):
    lL = [0, 1, 2]
    endTime = 0.4
    tL = []
    for j in range(len(tdisc)):
        tL.append(j)
    read.writeActivationDataFiles(path,lL,tL,False)

    plot=Plot(lL,tdisc,tdisc[0],endTime,1,[])
    time=tL[-1] 
    plot.ActivationTimePlotSpace(path,time)
    level=lL[-1] 
    plot.ActivationTimePlotTime(path,tdisc,level)


def Plot3DActivationTimesPointEight(path, tdisc, evaluationPoint):
    lL = [0, 1, 2]
    endTime = 0.8
    space_disc = [0.125, 0.25, 0.5]
    delta_x = []
    delta_t = []
    activation = []
    for i in range(len(lL)):
        for j in range(len(tdisc)):
            delta_x.append(space_disc[i])
            delta_t.append(tdisc[j])
            (points, activation_times) = comp.getActivationForPointsForBenchmark(path, i, j, evaluationPoint)
            activation.append(activation_times)
    plot = Plot(lL, tdisc, tdisc[0], endTime, 1, [])
    plot.ActivationTimeForBenchmarkPointEight3DPlot(delta_t, delta_x, activation, dataPath)

def evluationLaura():
    spacedisc = [0.5, 0.25, 0.125]
    lL = [0, 1, 2]
    endTime = 0.4
    timedisc = [0.05, 0.01, 0.005]
    dataPath = "../../../data/NiedererBenchmark/"

    P8 = [20.0, 7.0, 3.0]
    evaluationPoints = [[0.0, 0.0, 0.0], [20.0, 0.0, 0.0], [0.0, 0.0, 3.0],
                        [20.0, 0.0, 3.0], [0.0, 7.0, 0.0], [20.0, 7.0, 0.0],
                        [0.0, 7.0, 3.0], [20.0, 7.0, 3.0], [10.0, 3.5, 1.5]]

    plotActivationTimesDiagonal(dataPath, timedisc)
    Plot3DActivationTimesPointEight(dataPath, timedisc, P8)

    fL = dataPath
    tabWriter = TableWriter(lL, timedisc, timedisc[0], endTime, 1, fL, [])
    tabWriter.writeDatFilesForNiedererBenchmark(dataPath, spacedisc, timedisc, evaluationPoints)
    tabWriter.writeLatexTableForNiedererBenchmark(dataPath)
    
def evaluationForThesis(path,withDataReading=True):
    endTime = 0.06
    timedisc = [0.05, 0.01, 0.005]
    spacedisc = [0.5,0.2,0.1]
    tL = []
    for j in range(len(timedisc)):
        tL.append(j)
    lL=[]
    for l in range(len(spacedisc)):
        lL.append(l) 
    if withDataReading:
        read.writeActivationDataFiles(path,lL,tL,True)
    
    
    plot=Plot(lL,timedisc,timedisc[0],endTime,1,[])
    J=tL[-1] 
    plot.ActivationTimePlotSpace(path,J,spacedisc)
    L=lL[-1] 
    plot.ActivationTimePlotTime(path,timedisc,L)
def convergenceInSpacePlot(pathlist,writeDataFiles=False):
    for path in pathlist:
        evaluationForThesis(path,writeDataFiles)

def writeNiedererBMTables(path):
    print(path)
    endTime = 0.06
    timedisc = [0.05, 0.01, 0.005]
    spacedisc = [0.5,0.2,0.1]
    tL = []
    for j in range(len(timedisc)):
        tL.append(j)
    lL=[]
    for l in range(len(spacedisc)):
        lL.append(l) 
    evaluationPoints = [[20.0, 7.0, 3.0]]#, [10.0, 3.5, 1.5]]
    evaluationPointNumber= [8]
    
    tabWriter = TableWriter(lL, tL, tL[0], endTime, 1, [path],[])
    for i in range(len(evaluationPointNumber)):
        tabWriter.NiedererBMTabActivationTimeConvergence(evaluationPoints[i],evaluationPointNumber[i],timedisc,spacedisc)
        
def convergenceActivationTimes(pathlist,writeDataFiles=False):
    for path in pathlist:
        writeNiedererBMTables(path)
        
if __name__ == "__main__":
    pathlist=["../../../data/NiedererBenchmark/Tetra/", "../../../data/NiedererBenchmark/UnsmoothTetra/","../../../data/NiedererBenchmark/UnsmoothHexa/","../../../data/NiedererBenchmark/Hexa/"]
    #convergenceInSpacePlot(pathlist)
    #pathlist=[ "../../../data/NiedererBenchmark/Tetra/"]
    convergenceActivationTimes(pathlist)
