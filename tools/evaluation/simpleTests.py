from utility import reading as read
import matplotlib.pyplot as plt

# def plotPotential(self,dt,T,V)
def plotPotential(filename ='../../build/log/logfile.log'):
    print('plot from file', filename)
    (T,dt)=read.getTimeDataFromLog(filename)
    read.extractDataFromLog('../../build/','logfile.log',False)
    (t,V)=read.getDataFromTXT('../../build/log/','logfile',1)
    plt.plot(t,V)
    (t,V)=read.getDataFromTXT('../../build/log/','logfile',9)
    plt.plot(t,V)
    plt.show()
 
if __name__=="__main__":
    plotPotential()
