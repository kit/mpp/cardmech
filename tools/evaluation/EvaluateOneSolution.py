import math
from collections import OrderedDict
import re
import matplotlib.pyplot as plt
import numpy as np
from utility import reading as read


dataPath = '../../build/'
fn='logfile.log'
filePath = dataPath  +'log.txt'
shift=0
listEvaluationPoints =[1,2,3,4,5,6,7,8,9]
#listEvaluationPoints =[1]
numberOfEV = 3
#numberOfEV = 8
def getTimeData():
    f = open(dataPath+'/log/'+fn)
    T=0.0
    dt = 0.0
    for l in f:
        m = re.search("EndTime:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
          T = m.group(1) 
        m = re.search("DeltaTime:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
            dt = m.group(1)
    f.close() 
    return (T,dt)
def getElphyModelClass():
    eMC ="notSetJet"
    f = open(dataPath+fn)
    for l in f:
        m = re.search("     ElphyModelClass:\s*\.*\s*(\w+)", l)
        if m:
          eMC = m.group(1) 
          
    f.close()
    return eMC
def getNumberOfIonsteps():
    f = open(dataPath+'/log/'+fn)
    noIs = 1.0
    for l in f:
        m = re.search("ElphySubsteps:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
          noIs = m.group(1)
        
    f.close() 
    print(noIs)
    return noIs
def getSplittingIEx():
    f = open(dataPath+'/log/'+fn)
    name = 'notSetJet'
    for l in f:
        m = re.search("reading ... isIExternalinODE = \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
          name = m.group(1)
        
        
    f.close()
    print('name ' ,name)
    if name=='1':
        name="ODE"
    elif name =='0':
        name ="PDE"
    print(name)
    return name
def getIonModelName():
    f = open(dataPath+'/log/'+fn)
    name = ''
    for l in f:
        m = re.search("ElphyModelName:\s*\.*\s*(\w+)", l)
        if m:
          name = m.group(1)
        
        
    f.close()
    print(name)
    return name

def getScaleSigma():
    f = open(dataPath+'/log/'+fn)
    name = ''
    for l in f:
        m = re.search("ScaleSigma:\s*\.*\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
          name = m.group(1)
        
    f.close() 
    print("ScaleSigma ",name)
    return name
def getLevel():
    f = open(dataPath+'/log/'+fn)
    level = ''
    for l in f:
        m = re.search("ElphyLevel:\s*\.*\s*(\d)", l)
        if m:
          level = m.group(1)
        
    f.close() 
    print(level)
    return level
def getEVDataFromLog():
    (T,dt)=getTimeData()
    timesteps=(float(T)/float(dt)) -1
    f=open(dataPath+fn)
    dataX=[]
    dataY=[]
    for i in range(numberOfEV):
        dataX.insert(i,[])
        dataY.insert(i,[])
    time=[]
    for l in f:
        l = l.strip()
        m=re.search("# From\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m:
            time.append(m.group(1))
        m = re.search("EV(\d+)\s*:\s*\((\-{0,1}\d+\.{0,1}\d*[e+\-\d*]*),(\-{0,1}\d+\.{0,1}\d*[e+\-\d*]*)\)", l)
        if m: 
            dataX[int(m.group(1))].append(float(m.group(2)))
            dataY[int(m.group(1))].append(float(m.group(3)))
        if len(dataY[numberOfEV-1]) ==timesteps:
            break
    f.close() 
    return (time,dataX,dataY)
def getDataFromlog():
    eMC =getElphyModelClass()
    f = open(dataPath+fn)
    data = []
    time=[]
    time.append(0.0)
    for l in f:
        l = l.strip()
        m = re.search("startV = \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
        if m:
            data.append(m.group(1))
        if eMC=="IBTElphyModel" :  
            m=re.search("# Euler step (\d+)\s*from\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
            #m = re.search("LinearEuler  from ([+\-eE\d]+\.{0,1}[+\-eE\d]*) to ([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
            if m:
                time.append(m.group(2))
        elif eMC =="MElphyModel": 
            m=re.search("# From\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)\s*to\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
            if m:
                time.append(m.group(2))
        
        #m = re.search("Potential = ([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        #if m:
            #data.append( m.group(1))
        m = re.search("v(\d+)\(\s*([+\-eE\d]+\.{0,1}\d*)\,\s*([+\-eE\d]+\.{0,1}\d*)\,\s*([+\-eE\d]+\.{0,1}\d*)\)\s*=\s*(\-{0,1}\d+\.{0,1}\d*[e+\-\d*]*)", l)
        if m:        
            data.append( m.group(5))
            
    f.close() 
    return (time,data)
def WriteDataFile():
    read.extractDataFromLog(dataPath,fn,True)
def getBeelerReuterRHSDataFromLog():
    Iion=[]
    G_Ca=[]
    G_m=[]
    G_h=[]
    G_j=[]
    G_d=[]
    G_f=[]
    G_x1=[]
    time=[]
    document = open(dataPath+fn,'r')
    for l in document:
        l = l.strip()
        
        m = re.search("Elphyparameter ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
    
        if m:
            time.append(float(m.group(1)))
            Iion.append(float(m.group(2)))
            G_Ca.append(float(m.group(3)))
            G_d.append(float(m.group(4)))
            G_f.append(float(m.group(5)))
            G_h.append(float(m.group(6)))
            G_j.append(float(m.group(7)))
            G_m.append(float(m.group(8)))
            G_x1.append(float(m.group(9)))
    document.close()
    return(time,Iion,G_Ca,G_m,G_h,G_j,G_d,G_f,G_x1)
def getBeelerReuterGatingDataFromLog():
    Ca=[]
    _m=[]
    h=[]
    j=[]
    d=[]
    f=[]
    x1=[]
    time=[]
    document = open(dataPath+fn,'r')
    for l in document:
        l = l.strip()
        
        m = re.search("Elphyparameter ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*)",l)
    
        if m:
            time.append(float(m.group(1)))
            Ca.append(float(m.group(2)))
            d.append(float(m.group(3)))
            f.append(float(m.group(4)))
            h.append(float(m.group(5)))
            j.append(float(m.group(6)))
            _m.append(float(m.group(7)))
            x1.append(float(m.group(8)))
    document.close()
    return(time,Ca,_m,h,j,d,f,x1)
def getDataFromTXT(foldername, row):
    list = []
    time = []
    values = []
    f=open(str(foldername )+'.txt', 'r')
    for l in f:
        line = l.split(" ")
        list.append(line)
    f.close()

    for i in range(0,len(list)):
        time.append(float(list[i][0]))
        values.append(float(list[i][row]))
    
    return (time,values)

def getPlotSaveName(testname):
    name = ''
    IonModelName = getIonModelName()
    Level = getLevel()
    (T,dt) = getTimeData()
    namedt = '{:.10f}'.format(float(dt)).rstrip("0") [2:]
    numberOfIonSteps = getNumberOfIonsteps()
    #iExSplitting = getSplittingIEx()
    name +=IonModelName+'Level'+Level+'t'+namedt+'i'+str(numberOfIonSteps) 
    return name

def getPlotTitel():
    name = ''
    IonModelName = getIonModelName()
    ScaleSigma = getScaleSigma()
    Level = getLevel()
    (T,dt) = getTimeData()
    numberOfIonSteps = getNumberOfIonsteps()
    #iExSplitting = getSplittingIEx()
    name +=IonModelName+', l='+Level+ ', dt='+str(dt)+ ', m= '+str(numberOfIonSteps)
    return name
def PlotRow(row,testname):
    (T,dt) = getTimeData()
    T=float(T) 
    dt=float(dt)
    listlen = round(T/(dt))+1
    (time,V1) = getDataFromTXT(filePath,1)
    (time,V) = getDataFromTXT(filePath,row)
    (time,H1) = getDataFromTXT(filePath,12)
    (time,L2) = getDataFromTXT(filePath,11)
    DiffL2H1 = []
    if len(V)==listlen:
        for i in range(len(V)):
            V1[i] =float(V1[i])
            V[i] = float(V[i])
            H1[i] = float(H1[i])/1000.0
            L2[i] = float(L2[i])/1000.0
            DiffL2H1.append((-1)*(L2[i]-H1[i]))
        Vlabel = 'V in P'+str(row)+ ' in [mV]'
        plt.plot(time,V1, label ='V in P1 in [mV]')
        plt.plot(time,V, label = Vlabel)
        plt.plot(time,L2,label = "L2 in [V]")
        plt.plot(time,H1,label = "H1 in [V]")
        #plt.plot(time,DiffL2H1,label = "L2-H1")
    
    plotname=getPlotSaveName(testname)
    plt.title(getPlotTitel()  ) #' Comparing different norms '
    
    
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential (mV)')
    plt.legend()
    plt.savefig(savePNGPath + plotname + '.png')
    plt.show()
def plotFromTwologs():
    #atriafile=dataPath + 'logAtria' +'.txt'
    (timeAtria,V_Atria)= getDataFromTXT(filePath,1)
    hexafile =dataPath + 'logHexa' +'.txt'
    (timeHexa,V_Hexa)= getDataFromTXT(hexafile,1)
    if len(timeAtria)!=len(timeHexa):
           print('Achtung, da stimmt was nicht mit den Längen der Zeitvektoren')
    else:
        for i in range(len(V_Atria)):
            V_Atria[i] = float(V_Atria[i])
            V_Hexa[i] = float(V_Hexa[i])
        labelname='Atria in sinus node'
        plt.plot(timeAtria,V_Atria, label = labelname)
        labelname ='Cuboid in stimulation area'
        plt.plot(timeHexa,V_Hexa, label = labelname)
        plt.show()
    
def pointsInPlot():
    (T,dt) = getTimeData()
    T=float(T) 
    dt=float(dt)
    print(dt, T)
    listlen = round(T/(dt))+1
    file =''
    if fn=='logfile.log':
        file = dataPath+'/log/logfile'
    else:
        file =dataPath+'/log/'+fn
    for p in listEvaluationPoints:
        (time,V)= getDataFromTXT(file,p)
        #print(V,time)
        for i in range(len(V)):
            V[i] = float(V[i])
        
        labelname = 'P'+str(p+shift)
        #print(V)
        plt.plot(time,V, label = labelname)

    
    plt.title(getPlotTitel())
    plotname =getPlotSaveName('')+'AllPoints'
    xmin, xmax, ymin, ymax = 0.0, T+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential (mV)')
    plt.legend()
    plt.savefig(dataPath+'/log/' + plotname + '.png')
    plt.show()

def getSaveNameAndTitle():
    IonModelName = getIonModelName()
    (T,dt) = getTimeData()
    numberOfIonSteps = getNumberOfIonsteps()
    eMC =getElphyModelClass()
    if eMC=="IBTElphyModel":
        eMC="IBT"
    elif eMC=="MElphyModel":
        eMC="M++"
            
    print(numberOfIonSteps)
    if(int(numberOfIonSteps)!=1):
        print("Achtung, Rechnung ist mit substeps gemacht!")
    print(dt, T)
    title ="Potential for a single cell with "+IonModelName+ ', '+eMC+' Model and dt='+str(dt)
    
    namedt = '{:.10f}'.format(float(dt)).rstrip("0") [2:]
    save ='SingleCell'+eMC+IonModelName+'t'+namedt
    return(save,title)
def resizeData(t,V,number,length):
    time=[]
    pot=[]
    for i in range(length):
        if i%number==0:
            time.append(t[i])
            pot.append(V[i])
            
    return(time,pot)
def resizeDataVector(v,number,length):
    vector =[]
    for i in range(length):
        if i%number==0:
            vector.append(v[i])
    return vector
        
def plotOneCell():
    (T,dt) = getTimeData()
    (time,V)=getDataFromlog()
    xlength = int(float(T)/float(dt))
    for i in range(len(V)):
        V[i] = float(V[i])
        time[i] = float(time[i])
        #time[i] = 1000.0*float(time[i])
    
    (time,V)=resizeData(time,V,100,xlength)  #such that the tex file is not too big...
    plt.figure(figsize=(8,6), dpi=80)
    plt.plot(time,V, linewidth=3.5)#,label="V")
    (plotname,title)=getSaveNameAndTitle()
    #plt.title(title)
    xmin, xmax, ymin, ymax = 0.0, float(T)+0.05, -100.0,70.0
    plt.xlabel('time (s)')
    plt.ylabel('potential \\V~  (mV)')
    #plt.legend()
    #plt.savefig(savePNGPath + plotname + '.png')
    plt.show()
    import tikzplotlib
    tikzplotlib.save(saveTiKZPath + 'BRV' + ".tex")
    plt.close()
    plt.show()

def getJacobianFromLog(t):
    (T,dt)=getTimeData()
    timesteps=(float(T)/float(dt)) -1
    f=open(dataPath+fn)
    JAllTimes=[]
    J=[]
    for i in range(numberOfEV):
        J.insert(i,[])
    for l in f:
        l = l.strip()
        m = re.search("J\((\d+)\)\s*=\s*([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*) ([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m: 
            J[int(m.group(1))].append(float(m.group(2)))
            J[int(m.group(1))].append(float(m.group(3)))
            J[int(m.group(1))].append(float(m.group(4)))
            J[int(m.group(1))].append(float(m.group(5)))
            J[int(m.group(1))].append(float(m.group(6)))
            J[int(m.group(1))].append(float(m.group(7)))
            J[int(m.group(1))].append(float(m.group(8)))
            J[int(m.group(1))].append(float(m.group(9)))
            
            if int(m.group(1))== numberOfEV-1:
                JAllTimes.append(J)
                J=[]
                for i in range(numberOfEV):
                    J.insert(i,[])
        if len(JAllTimes) == timesteps:
            break               
    f.close() 
    n=int(float(t)/float(dt))
    Jacobian_t=JAllTimes[n]
    print(Jacobian_t)
    
def getValuesAtTimeT(t):
    (time,X,Y)=getEVDataFromLog()
    (T,dt)=getTimeData()
    real=[]
    im=[]
    n=int(float(t)/float(dt))
    for i in range(8):
        real.append(X[i][n])
        im.append(Y[i][n])
       
    return(real,im)   

    
    
def plotEigenvaluesBRcomponentwise():
    (time,X,Y)=getEVDataFromLog()
    (T,dt)=getTimeData()
    xlength = int(float(T)/float(dt))-1
    
    #print(xlength,len(X[1]))
    
    for i in range(numberOfEV):
        (X[i],Y[i])=resizeData(X[i],Y[i],10,xlength)  #such that the tex file is not too big...
    
    plotEV(X[0],Y[0],'V','EVComponent1ToV')
    
    plotEV(X[1],Y[1],'Ca','EVComponent2ToCa')
    plotEVaddMinusInYaxis(X[2],Y[2],'d','EVComponent3Tod')
    plotEVaddMinusInYaxis(X[4],Y[4],'h','EVComponent5Toh')
    plotEV(X[5],Y[5],'j','EVComponent6Toj')
    plotEV(X[6],Y[6],'m','EVComponent7Tom')
    #plotEV(X[7],Y[7],'x1','EVComponent8Tox1')
    #plotEV_f(X[3],Y[3])
    
    
def plotEVaddMinusInYaxis(x,y,name,filename):
    plot(x,y)
    plt.ylim(-2.0, 5.0)
    import tikzplotlib
    tikzplotlib.save(saveTiKZPath + filename + ".tex")
    plt.close()
def plotEV(x,y,name,filename):
    plot(x,y)
    import tikzplotlib
    tikzplotlib.save(saveTiKZPath + filename + ".tex")
    plt.close()
    
def getMinAndMax(X):
    xmin = 10000000000000000000000
    xmax = -1000000000000000000000
    for i in range(len(X)):
        if X[i]< xmin:
            xmin =X[i]
        elif X[i]>xmax:
            xmax =X[i]
    return (xmin,xmax)
            
        
    return(xmin,xmax,ymin,ymax)
def plot(X,Y):
    plt.figure(figsize=(8,6), dpi=80)
    plt.plot(X[1:],Y[1:],color='blue', linestyle='', marker='o',markersize=5.5)
    plt.plot(X[0],Y[0],color='red', linestyle='', marker='x',markersize=10)
    
def plotEV_f(X,Y):
    #(xmin,xmax)= getMinAndMax(X)
    plot(X,Y)
    #plt.xticks(np.linspace(xmin, xmax, 6,endpoint=True))
    plt.xticks( [-0.0022, -0.0018, -0.0014, -0.001] )
    import tikzplotlib
    tikzplotlib.save(saveTiKZPath + 'EVComponent4Tof' + ".tex")
    plt.close()
    plt.show()
def plotGating(time,y,yname,filename):
    plt.figure(figsize=(8,6), dpi=80)
    plt.plot(time,y, linewidth=3.5)
    plt.xlabel('time (s)')
    plt.ylabel('')
    import tikzplotlib
    tikzplotlib.save(saveTiKZPath + filename + ".tex")
    plt.close()
def plotBeelerReuterData():
    (T,dt)=getTimeData()
    (time,Ca,m,h,j,d,f,x1) = getBeelerReuterGatingDataFromLog()
    xlength = int(float(T)/float(dt))
    (time,Ca)=resizeData(time,Ca,100,xlength)
    (d,f)=resizeData(d,f,100,xlength)
    (h,j)=resizeData(h,j,100,xlength)
    (m,x1)=resizeData(m,x1,100,xlength)
    plotGating(time,Ca,'Ca','BRCa')
    plotGating(time,d,'d','BRd')
    plotGating(time,f,'f','BRf')
    plotGating(time,h,'h','BRh')
    plotGating(time,j,'j','BRj')
    plotGating(time,m,'m','BRm')
    plotGating(time,x1,'x1','BRx1')
def plotBeelerReuterRHS():
    (T,dt)=getTimeData()
    (time,Iion,G_Ca,G_m,G_h,G_j,G_d,G_f,G_x1) = getBeelerReuterRHSDataFromLog()
    xlength = int(float(T)/float(dt))
    time=resizeDataVector(time,100,xlength)
    Iion=resizeDataVector(Iion,100,xlength)
    G_Ca=resizeDataVector(G_Ca,100,xlength)
    G_d=resizeDataVector(G_d,100,xlength)
    G_f=resizeDataVector(G_f,100,xlength)
    G_h=resizeDataVector(G_h,100,xlength)
    G_j=resizeDataVector(G_j,100,xlength)
    G_m=resizeDataVector(G_m,100,xlength)
    G_x1=resizeDataVector(G_x1,100,xlength)
    
    plotGating(time,Iion,'Iion','BRIion')
    plotGating(time,G_Ca,'Ca','BRGCa')
    plotGating(time,G_d,'d','BRGd')
    plotGating(time,G_f,'f','BRGf')
    plotGating(time,G_h,'h','BRGh')
    plotGating(time,G_j,'j','BRGj')
    plotGating(time,G_m,'m','BRGm')
    plotGating(time,G_x1,'x1','BRGx1')
def plotEigenvaluesBRTimedependent(eT):
    for i in range(len(eT)):
        (x,y)=getValuesAtTimeT(eT[i])
        plt.figure(figsize=(8,6), dpi=80)
        plt.plot(x,y,color='blue', linestyle='', marker='o',markersize=5.5)
        filename = 'BREigenvlauest'+ str(int(eT[i]*1000))+'ms'
        import tikzplotlib
        tikzplotlib.save(saveTiKZPath + filename + ".tex")
        #plt.show()
        plt.close()
        
def texJacobiTimeDependent(eT):
    for i in range(len(eT)):
        J=getJacobianFromLog(eT[i])
if __name__=="__main__":
    WriteDataFile()
    row = 10
    #PlotRow(row,'S') #'Hexa2'
    #PlotNormQuader(row)
    pointsInPlot()
    #plotOneCell()
    #evaluationTimes =[0.0,0.001,0.002,0.05,0.2,0.28,0.3]
    #plotEigenvaluesBRTimedependent(evaluationTimes)
    #texJacobiTimeDependent(evaluationTimes)
    #plotEigenvaluesBRcomponentwise()
    #plotBeelerReuterData()
    #plotBeelerReuterRHS()
    #plotFromTwologs()
