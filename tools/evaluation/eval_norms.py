import math
import numpy as np
import matplotlib.pyplot as plt

def f(x,t):
    return -x*x

def anal(tstart, tspan, dt):
    t = np.arange(tstart, tspan, dt)
    x = np.zeros(len(t))

    for i in range(0,len(t)):
        x[i] = 1 / t[i]

    return [t,x]

def exp_euler(intx, tspan, dt):
    t = np.arange(0, tspan, dt)
    x = np.zeros(len(t))
    x[0] = intx

    for i in range(1,len(t)):
        x[i] = (x[i-1] + dt*f(x[i-1], t[i-1]))

    return [t,x]

if __name__=="__main__":
    t_start = 0.01
    t_end = 1

    N = [pow(2, n) for n in range(8,20)]
    E = []
    for n in N:
        dt = (t_end - t_start) / (n-1)

        ta, anl = anal(t_start, t_end, dt)
        te, exp = exp_euler(100, t_end, dt)

        #plt.plot(ta, anl, 'b')
        #plt.plot(te, exp, 'r')

        e = 0.0
        for i in range(len(anl)):
            e = max(e, anl[i] - exp[i])
        E.append(e)

    ref = [1e+3/n for n in N]

    plt.loglog(N, E, 'r')
    plt.loglog(N, ref, 'r--')
    plt.show()