#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 14 11:12:11 2021

@author: laura
"""

import sys
import csv
import matplotlib.pyplot as plt

sys.path.append('../..')

import mpp.python.mppy as mppy

if __name__ == "__main__":
    mpp = mppy.Mpp(project_name='CardMech',
                   executable='M++',
                   kernels=1,
                   mute=False)
    
    mpp.clean_log()
    mpp.build()
    
    #mech_level = {'0'}
    #mech_level = {'0','1','2'}
    mech_level = {'3','4','5','6','7'}
    
    
    #material = {"Bonet"}
    material = {"Linear","Bonet"}
    
    
    l2_error = []
    text = "test"
    inner_X_disp = []
    inner_Y_disp = []
    inner_z_value = []
    
    mid_X_disp = []
    mid_Y_disp = []
    mid_z_value = []
    
    outer_X_disp = []
    outer_Y_disp = []
    outer_z_value = []
    
    l2_error =[]
    for i in mech_level:
        for k in material:
            mpp = mppy.Mpp(project_name='CardMech',
                                   executable='M++',
                                   kernels=1,
                                   mute=False)
            mpp.clean_log()
            mpp.run(1, config='elasticity/benchmarks/ellipsoid', kwargs={
                       "MechLevel": i,
                       "ActiveMaterial": k,
                       "MechPolynomialDegree": 1
                       })
            mpp.parse_log()
         
            cells = mpp.data["Cells"]
            dof = mpp.data["Degrees of Freedom:"][0]
            l2_err = mpp.data["L2 Error"][0]
            h1_err = mpp.data["H1 Error"][0]
            energy_err = mpp.data["Energy Error"][0]
            init_volume = mpp.data["Initial Geometry Volume"][0]
            end_volume = mpp.data["Deformed Geometry Volume"][0]
            norm = mpp.data["Norm"][0] 
            outer_z_values = mpp.data["outer z values"][0]
            outer_Y_displacement = mpp.data["outer Y Displacement"][0]
            outer_X_displacement = mpp.data["outer X Displacement"][0]
            mid_z_values = mpp.data["mid z values"][0]
            mid_Y_displacement = mpp.data["mid Y Displacement"][0]
            mid_X_displacement = mpp.data["mid X Displacement"][0]
            inner_z_values = mpp.data["inner z values"][0]
            inner_Y_displacement = mpp.data["inner Y Displacement"][0]
            inner_X_displacement = mpp.data["inner X Displacement"]
            apex_displacement = mpp.data["Apex z Movement"][0]
         
            
            text += str(apex_displacement)
            content = mpp.read_log()  
            for line in content:
                start = line.find("inner X Displacement:")
                if start == -1:
                    continue
                end = line.find("]")
                inner_X_disp = (line[start + 44:end])
             
            
            for line1 in content:
                start1 = line1.find("inner Y Displacement:")
                if start1 == -1:
                    continue
                end1 = line1.find("]")
                inner_Y_disp = line1[start1+44:end]
            
            for line2 in content:
                start2 = line2.find("inner z values:")
                if start2 == -1:
                    continue
                end2 = line2.find("]")
                inner_z_value = (line2[start2 + 44:end2])
             
            for line4 in content:
                start4 = line4.find("mid X Displacement:")
                if start4 == -1:
                    continue
                end4 = line4.find("]")
                mid_X_disp = (line4[start4 + 44:end4])
             
            for line5 in content:
                start5 = line5.find("mid Y Displacement:")
                if start5 == -1:
                    continue
                end5 = line5.find("]")
                mid_Y_disp = (line5[start5 + 44:end5])
                
            for line6 in content:
                start6 = line6.find("mid z values:")
                if start6 == -1:
                    continue
                end6 = line6.find("]")
                mid_z_value = (line6[start6 + 44:end6])
             
            for line7 in content:
                start7 = line7.find("outer X Displacement:")
                if start7 == -1:
                    continue
                end7 = line7.find("]")
                outer_X_disp = (line7[start7 + 44:end7])
                 
            for line8 in content:
                start8 = line8.find("outer Y Displacement:")
                if start8 == -1:
                    continue
                end8 = line8.find("]")
                outer_Y_disp = (line8[start8 + 44:end8])
             
            for line9 in content:
                start9 = line9.find("outer z values:")
                if start9 == -1:
                    continue
                end9 = line9.find("]")
                outer_z_value = (line9[start9 + 44:end9])
         
        
            ellipsoid_tables = {"ellipsoid_tables"}
            #for l in ellipsoid_tables:
            with open(str(ellipsoid_tables) + '.csv', 'a') as end_table_data:
                writer = csv.writer(end_table_data)
                writer.writerow(["Material","Cells", "Level", "Degrees of Freedom",\
                                 "L2-Error", "Energy-Error",\
                                    "Apex Displacement" ])
            #for l in ellipsoid_tables:
            with open(str(ellipsoid_tables) + '.csv', 'a') as end_table_data:
                   writer = csv.writer(end_table_data)
                   writer.writerow([k,cells, i, dof, l2_err, energy_err, apex_displacement])


            ellipsoid_displacements = {"ellipsoid_displacements"}
            for q in ellipsoid_displacements:
                with open(str(ellipsoid_displacements) + '.csv', 'a') as displacements:
                    writer = csv.writer(displacements)
                    writer.writerow(["inner z values", "material", "level"])
                    writer.writerow([inner_z_value, k, i])
                    writer.writerow(["inner Y Displacement", "material", "level"])
                    writer.writerow([inner_Y_disp, k, i])
                    writer.writerow(["inner X Displacement", "material", "level"])
                    writer.writerow([inner_X_disp, k, i])
                
                    writer.writerow(["mid z values", "material", "level"])
                    writer.writerow([mid_z_value, k, i])
                    writer.writerow(["mid Y Displacement", "material", "level"])
                    writer.writerow([mid_Y_disp, k, i])
                    writer.writerow(["mid X Displacement" , "material", "level"])
                    writer.writerow([mid_X_disp, k, i])
                
                    writer.writerow(["outer z values", "material", "level"])
                    writer.writerow([outer_z_value, k, i])
                    writer.writerow(["outer Y Displacement", "material", "level"])
                    writer.writerow([outer_Y_disp, k, i])
                    writer.writerow(["outer X Displacement" , "material", "level"])
                    writer.writerow([outer_X_disp, k, i])
    
        
    print(text)