from math import sqrt

from activestrain import general
from activestrain.guccione import ventriclel4p1, ventriclel3p1, ventriclel2p1, ventriclel0p1, ventriclel1p1, ventriclel3p2, ventriclel2p2, ventriclel0p2, ventriclel1p2


def write_dat_file(name, datadict):
    with open(str(name + '.dat'), 'w') as output_file:
        datalist = []
        N=0
        datastring = ""
        for dataname, data in datadict.items():
            datalist.append(data)
            N = len(data)
            datastring += dataname + ","
        output_file.write(str(datastring[:-1] + "\n"))

        M=len(datalist)
        for n in range(N):
            datastring = ""
            for m in range(M):
                datastring+=str(round(datalist[m][n], 6)) + ","
            output_file.write(str(datastring[:-1] + "\n"))


def generate_table(name, lists):

    values = {"t": general.Time}
    N = len(general.Time)
    for i in range(len(lists)):
        values.update({str(i): lists[i]})

    errors = [0 for _ in range(len(lists))]
    refvalue = 0
    for n in range(N):
        refvalue +=  1/N * lists[-1][n] * lists[-1][n]

        for i in range(len(lists)-1):
            errors[i] += 1/N * (lists[i][n] - lists[-1][n])*(lists[i][n] - lists[-1][n])

    errorstring = name+"Errors "
    ratestring = name+"Rates "
    for i in range(len(lists) - 1):
        errors[i] = sqrt(errors[i]) / sqrt(refvalue)

        errorstring += "& $" + str(round(errors[i], 6))+ "$ "
        if i>0:
            ratestring += "& $" + str(round(errors[i-1] / errors[i],2)) + "$ "
    print(errorstring)
    print(ratestring)
    write_dat_file(name, values)

def generate_as_ventricle_tables():
    as_guccione_p1_table()
    as_guccione_p2_table()


def as_guccione_p1_table():
    data = [ventriclel0p1.L2NormL0P1, ventriclel1p1.L2NormL1P1, ventriclel2p1.L2NormL2P1, ventriclel3p1.L2NormL3P1,
            ventriclel4p1.L2NormL4P1]
    generate_table("GuccioneL2NormP1", data)
    data = [ventriclel0p1.LVVolumeL0P1, ventriclel1p1.LVVolumeL1P1, ventriclel2p1.LVVolumeL2P1,
            ventriclel3p1.LVVolumeL3P1, ventriclel4p1.LVVolumeL4P1]
    generate_table("GuccioneLVVolumeP1", data)
    data = [ventriclel0p1.MyocardStrainL0P1, ventriclel1p1.MyocardStrainL1P1, ventriclel2p1.MyocardStrainL2P1,
            ventriclel3p1.MyocardStrainL3P1, ventriclel4p1.MyocardStrainL4P1]
    generate_table("GuccioneMyocardStrainP1", data)
    data = [ventriclel0p1.EnergyNormL0P1, ventriclel1p1.EnergyNormL1P1, ventriclel2p1.EnergyNormL2P1,
            ventriclel3p1.EnergyNormL3P1, ventriclel4p1.EnergyNormL4P1]
    generate_table("GuccioneEnergyNormP1", data)
    
def as_guccione_p2_table():
    data = [ventriclel0p2.L2NormL0P2, ventriclel1p2.L2NormL1P2, ventriclel2p2.L2NormL2P2, ventriclel3p2.L2NormL3P2]
    generate_table("GuccioneL2NormP2", data)
    data = [ventriclel0p2.LVVolumeL0P2, ventriclel1p2.LVVolumeL1P2, ventriclel2p2.LVVolumeL2P2,
            ventriclel3p2.LVVolumeL3P2]
    generate_table("GuccioneLVVolumeP2", data)
    data = [ventriclel0p2.MyocardStrainL0P2, ventriclel1p2.MyocardStrainL1P2, ventriclel2p2.MyocardStrainL2P2,
            ventriclel3p2.MyocardStrainL3P2]
    generate_table("GuccioneMyocardStrainP2", data)
    data = [ventriclel0p2.EnergyNormL0P2, ventriclel1p2.EnergyNormL1P2, ventriclel2p2.EnergyNormL2P2,
            ventriclel3p2.EnergyNormL3P2]
    generate_table("GuccioneEnergyNormP2", data)




if __name__=="__main__":
    generate_as_ventricle_tables()

