from math import sqrt

from activestrain import general
from activestrain.linear import ventriclel4p2, ventriclel4p1, ventriclel3p1, ventriclel2p1, ventriclel0p1, \
    ventriclel1p1, ventriclel3p2, ventriclel2p2, ventriclel0p2, ventriclel1p2


def write_dat_file(name, datadict):
    with open(str(name + '.dat'), 'w') as output_file:
        datalist = []
        N = 0
        datastring = ""
        for dataname, data in datadict.items():
            datalist.append(data)
            N = len(data)
            datastring += dataname + ","
        output_file.write(str(datastring[:-1] + "\n"))

        M = len(datalist)
        for n in range(N):
            datastring = ""
            for m in range(M):
                datastring += str(round(datalist[m][n], 6)) + ","
            output_file.write(str(datastring[:-1] + "\n"))


def generate_table(name, lists):
    values = {"t": general.Time}
    N = len(general.Time)
    for i in range(len(lists)):
        values.update({str(i): lists[i]})

    errors = [0 for _ in range(len(lists))]
    refvalue=0
    for n in range(N):
        refvalue +=  1/N * lists[-1][n] * lists[-1][n]
        for i in range(len(lists) - 1):
            errors[i] += 1/N * (lists[i][n] - lists[-1][n])*(lists[i][n] - lists[-1][n])

    errorstring = name+"Errors "
    ratestring = name+"Rates "
    for i in range(len(lists) - 1):
        errors[i] = sqrt(errors[i]) / sqrt(refvalue)
        errorstring += "& $" + str(round(errors[i], 6))+ "$ "
        if i>0:
            ratestring += "& $" + str(round(errors[i-1] / errors[i], 2)) + "$ "
    print(errorstring)
    print(ratestring)
    write_dat_file(name, values)


def generate_as_ventricle_tables():
    as_linear_p1_table()
    as_linear_p2_table()



def as_linear_p1_table():
    data = [ventriclel0p1.L2NormL0P1L, ventriclel1p1.L2NormL1P1L, ventriclel2p1.L2NormL2P1L, ventriclel3p1.L2NormL3P1L,
            ventriclel4p1.L2NormL4P1L]
    generate_table("LinearL2NormP1L", data)
    data = [ventriclel0p1.LVVolumeL0P1L, ventriclel1p1.LVVolumeL1P1L, ventriclel2p1.LVVolumeL2P1L,
            ventriclel3p1.LVVolumeL3P1L, ventriclel4p1.LVVolumeL4P1L]
    generate_table("LinearLVVolumeP1L", data)
    data = [ventriclel0p1.MyocardStrainL0P1L, ventriclel1p1.MyocardStrainL1P1L, ventriclel2p1.MyocardStrainL2P1L,
            ventriclel3p1.MyocardStrainL3P1L, ventriclel4p1.MyocardStrainL4P1L]
    generate_table("LinearMyocardStrainP1L", data)
    data = [ventriclel0p1.EnergyNormL0P1L, ventriclel1p1.EnergyNormL1P1L, ventriclel2p1.EnergyNormL2P1L,
            ventriclel3p1.EnergyNormL3P1L, ventriclel4p1.EnergyNormL4P1L]
    generate_table("LinearEnergyNormP1L", data)


def as_linear_p2_table():
    data = [ventriclel0p2.L2NormL0P2L, ventriclel1p2.L2NormL1P2L, ventriclel2p2.L2NormL2P2L, ventriclel3p2.L2NormL3P2L,
            ventriclel4p2.L2NormL4P2L]
    generate_table("LinearL2NormP2L", data)
    data = [ventriclel0p2.LVVolumeL0P2L, ventriclel1p2.LVVolumeL1P2L, ventriclel2p2.LVVolumeL2P2L,
            ventriclel3p2.LVVolumeL3P2L, ventriclel4p2.LVVolumeL4P2L]
    generate_table("LinearLVVolumeP2L", data)
    data = [ventriclel0p2.MyocardStrainL0P2L, ventriclel1p2.MyocardStrainL1P2L, ventriclel2p2.MyocardStrainL2P2L,
            ventriclel3p2.MyocardStrainL3P2L,
            ventriclel4p2.MyocardStrainL4P2L]
    generate_table("LinearMyocardStrainP2L", data)
    data = [ventriclel0p2.EnergyNormL0P2L, ventriclel1p2.EnergyNormL1P2L, ventriclel2p2.EnergyNormL2P2L,
            ventriclel3p2.EnergyNormL3P2L,
            ventriclel4p2.EnergyNormL4P2L]
    generate_table("LinearEnergyNormP2L", data)


if __name__ == "__main__":
    generate_as_ventricle_tables()

