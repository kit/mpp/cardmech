from evaluation.print_tables.tables import write_dat_file

L2NormLinearOP1 = [19.20536007,6.546229901, 1.979646031, 0.4855841085]
L2AvgNormLinearOP1 = [76.30173843, 26.11428639, 7.906798242, 1.940384678]
H1NormLinearOP1 = [21.06908096,7.495583835 , 2.578458635, 0.8083805734 ]
EnergyLinearOP1 = [30.16462006,12.72917616 , 6.389951614 , 2.466498622]

LinearMyoCardOP1 = [3106.208548,3381.055216,3472.49153, 3498.169936,3503.859157]
LinearApexOP1 = [-1.84499766,-1.616780666,-1.530689241,-1.503014802, -1.494717791]

L2NormLinearOP2 = [1.741002717,0.5401430248,0.1708967429,0.04516478183]
L2AvgNormLinearOP2 = [18.47230684,5.737927712,1.83079516,0.484845602]
H1NormLinearOP2 = [ 2.871704563,1.573204977,0.8519697914, 0.4363084362]
EnergyLinearOP2 = [10.05504591,6.980759577,3.986163697,2.104425364]


LinearMyoCardOP2 = [3494.696806,3505.876049,3505.766809,3504.833276,3504.152215]
LinearApexOP2 = [-1.509042159,-1.495959039,-1.49272725,-1.491790768,-1.491467872]

L2NormGuccioneOP1 = [10.47430724,3.760976777, 1.139931179,0.275482152]
L2AvgNormGuccioneOP1 = [41.61656685,15.00944942,4.554955541,1.101288538]
H1NormGuccioneOP1 = [11.55910295,4.255234261,1.395741702, 0.3936507122]
EnergyGuccioneOP1 = [11.53784499,4.659333865,1.916432376,0.6651363658]


GuccioneMyoCardOP1 = [2278.929466,2401.266376, 2442.473097,2454.333834,2457.307156]
GuccioneApexOP1 = [- 5.098399214,- 4.918162279,-4.829258056,-4.7972885,-4.786853979]


L2NormGuccioneOP2 = [0.597854677,0.1739812794,0.06206531706,0.01864285389]
L2AvgNormGuccioneOP2 = [6.303710878,1.839465655,0.6671819959,0.2023739825]
H1NormGuccioneOP2 = [1.10808759,0.5561794777,0.2901666595, 0.1327260523]
EnergyGuccioneOP2 = [2.300186808,1.362040462,0.7320348173,0.3420455075]


GuccioneMyoCardOP2 = [2456.145946,2458.398586,2458.324621,2458.10392,2457.977782]
GuccioneApexOP2 = [-4.790396516,-4.785614466,-4.783410764,-4.782190273,-4.781643357]

L2NormHolzapfelOP1 = [17.04217333,7.283420056,2.607279343,0.8432934927]
L2AvgNormHolzapfelOP1 = [67.87580802,29.03949159,10.3933261,3.365154417]
H1NormHolzapfelOP1 = [18.12317292,8.620257485,4.281280496, 1.981352536]
EnergyHolzapfelOP1 = [84.45137519,34.67680813,117.625376, 4.087412246]


HolzapfelMyoCardOP1 = [ 377.4298787,450.6968924,485.991463,498.997275,502.5627703]
HolzapfelApexOP1 = [-3.649927327,-3.833866619,-3.950971758,-3.992070987,-3.988293945]


L2NormHolzapfelOP2 = [4.471020745,2.35566264,0.8779760802]
L2AvgNormHolzapfelOP2 = [48.35140883,25.60570832,9.584414966]
H1NormHolzapfelOP2 = [6.527019398,4.488104656,2.344571388]
EnergyHolzapfelOP2 = [20992404.81,11668675.71,1024209.89]

HolzapfelMyoCardOP2 = [0,0,0,0]
HolzapfelApexOP2 = [-4.119359495,-4.042324447,-3.992629888,-3.964570343]
#
#
#L2NormLinearUP1 = [,,,]
#L2AvgNormLinearUP1 = [,,,]
#H1NormLinearUP1 = [,,,]
#EnergyLinearUP1 = [,,,]
#
#LinearMyoCardUP1 = [,,,,]
#LinearApexUP1 = [,,,,]
#
#L2NormLinearUP2 = [,,,]
#L2AvgNormLinearUP2 = [,,,]
#H1NormLinearUP2 = [,,,]
#EnergyLinearUP2 = [,,,]
#
#LinearMyoCardUP2 = [,,,,]
#LinearApexUP2 = [,,,,]
#
#L2NormGuccioneUP1 = [,,,]
#L2AvgNormGuccioneUP1 = [,,,]
#H1NormGuccioneUP1 = [,,, ]
#EnergyGuccioneUP1 = [,,,]
#
#GuccioneMyoCardUP1 = [,,,,]
#GuccioneApexUP1 = [,,,,]
#
#
#L2NormGuccioneUP2 = [,,,]
#L2AvgNormGuccioneUP2 = [,,,]
#H1NormGuccioneUP2 = [,,, ]
#EnergyGuccioneUP2 = [,,,]
#
#GuccioneMyoCardUP2 = [,,,,]
#GuccioneApexUP2 = [,,,,]
#
#L2NormHolzapfelUP1 = [,,,]
#L2AvgNormHolzapfelUP1 = [,,,]
#H1NormHolzapfelUP1 = [,,, ]
#EnergyHolzapfelUP1 = [,,,]
#
#HolzapfelMyoCardUP1 = [,,,,]
#HolzapfelApexUP1 = [,,,,]
#
#
#L2NormHolzapfelUP2 = [,,,]
#L2AvgNormHolzapfelUP2 = [,,,]
#H1NormHolzapfelUP2 = [,,, ]
#EnergyHolzapfelUP2 = [,,,]
#
#HolzapfelMyoCardUP2 = [,,,,]
#HolzapfelApexUP2 = [,,,,]


def print_static_table(name, entries, N):
    values = {"DoFs": [7013, 56104, 448832, 3590656, 28725248]}

    for i in range(len(entries)):
        values.update({str(i): entries[i]})
        errorstring = name + "Rates "
        for n in range(N-1):
            errorstring += "& $" + str(round(entries[i][n] / entries[i][n+1], 2)) + "$ "
        print(errorstring)

    values.update({"d": [pow(0.5, 3*i) for i in range(N)]})

    write_dat_file(name, values)

if __name__=="__main__":
    print_static_table("LinearP1", [L2NormLinearOP1, L2AvgNormLinearOP1, H1NormLinearOP1, EnergyLinearOP1], 4)
    print_static_table("LinearP2", [L2NormLinearOP2, L2AvgNormLinearOP2, H1NormLinearOP2, EnergyLinearOP2], 4)
    print_static_table("GuccioneP1", [L2NormGuccioneOP1, L2AvgNormGuccioneOP1, H1NormGuccioneOP1, EnergyGuccioneOP1], 4)
    print_static_table("GuccioneP2", [L2NormGuccioneOP2, L2AvgNormGuccioneOP2, H1NormGuccioneOP2, EnergyGuccioneOP2], 4)
    print_static_table("HolzapfelP1", [L2NormHolzapfelOP1, L2AvgNormHolzapfelOP1, H1NormHolzapfelOP1, EnergyHolzapfelOP1], 4)
    print_static_table("HolzapfelP2", [L2NormHolzapfelOP2, L2AvgNormHolzapfelOP2, H1NormHolzapfelOP2, EnergyHolzapfelOP2], 3)