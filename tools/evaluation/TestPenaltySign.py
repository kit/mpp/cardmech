#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 13:07:51 2022

@author: laura
"""

import sys
import csv
import matplotlib.pyplot as plt

sys.path.append('../..')

import mpp.python.mppy as mppy

if __name__ == "__main__":
    mpp = mppy.Mpp(project_name='CardMech',
                   executable='M++',
                   kernels=52,
                   mute=False)
    
    #mpp.clean_log()
    mpp.build()
    
    sign = {"1","-1"}
    penalty = {'2.0','3.0','5.0','10.0','15.0','20.0','30.0'}

    
    for i in sign:
        for k in penalty:
            mpp = mppy.Mpp(project_name='CardMech',
                                   executable='M++',
                                   kernels=52,
                                   mute=False)
            mpp.run(52, config='elasticity/benchmarks/ellipsoid', kwargs={
                        "MechLevel":0,
                        "MechPolynomialDegree":1,
                        "DGSign": i,
                        "DGPenalty": k
                        })
    
    
    