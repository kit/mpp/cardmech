import numpy as np
import matplotlib.pyplot as plt

def plot_graph():
    euler_var =  [0.785398163397, 0.392699081699, 0.196349540849, 0.0981747704247, 0.0490873852123, 0.0245436926062, 0.0122718463031, 0.00613592315154, 0.00306796157577, 0.00153398078789, 0.000766990393943, 0.000383495196972, ]
    rk2_var =  [0.026172152977, 0.00645454279956, 0.00160818908397, 0.000401708154965, 0.000100405864184, 2.51001429512e-05, 6.27495304983e-06, 1.56873309543e-06, 3.92182950115e-07, 9.8045718655e-08, 2.45114288866e-08, 6.12785777676e-09, ]
    rk4_var =  [0.000134584974194, 8.29552396775e-06, 5.16684706353e-07, 3.22650008933e-08, 2.01612881945e-09, 1.26001209466e-10, 7.87503395827e-12, 4.92828000631e-13, 3.04201108747e-14, 3.44169137634e-15, 1.7763568394e-15, 1.66533453694e-15, ]

    n = [i+2 for i, _ in enumerate(euler_var)]
    dt = [pow(2, i) for i in n]
    ref1 = [1/n for n in dt]
    ref2 = [1/(n*n) for n in dt]
    ref4 = [1/(n*n*n*n) for n in dt]

    plt.xlabel("Delta t")
    plt.ylabel("L1 Error")

    plt.title("Error of Explicit Methods")
    plt.xticks(n, dt)
    plt.loglog(dt, euler_var, color='r')
    plt.loglog(dt, ref1, 'r--')
    plt.loglog(dt, rk2_var, color='b')
    plt.loglog(dt, ref2, 'b--')
    plt.loglog(dt, rk4_var, color='g')
    plt.loglog(dt, ref4, 'g--')
    plt.show()

if __name__=="__main__":
    plot_graph()
