from utility import reading as read
from utility import computing as comp
from utility.plotting import Plot 
from utility.latexTables import Table
from utility.latexTables import TableWriter


eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/'
fSISVI=eP+'SI0004/'
fSIOC=eP+'SIOC0004/'
fSIICI=eP+'SIICI0004/'
fGS=eP+'GS0004/theta1/' #'../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/GS0004/theta1/'
fGShalve=eP+'GS0004/'

nL=['\\SISVI','\\SIOC','\\SIICI','\\GPDE']#,'\\GShalve']
fL=[fSISVI,fSIOC,fSIICI,fGS]#,fGShalve]
nL=['\\SISVI','\\SIOC','\\GPDE']
fL=[fSISVI,fSIOC,fGS]

threshold=-50.0

def activationTimeExtrapolateTab():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    
    sP='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    nL=['\\SIOC']
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    tabWriter.writeActivationExtrapolationTab(eP,[fSIOC],sP,nL,threshold)

def DifferencePerPointTabs(case):
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    #listEvaluationPoints=[8]
    
    
    
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    l=5
    j=3
    
    tabWriter.writeDifferenceTablePerPoint(l,j,fL,nL,savePath,case,threshold)

def tactValuesAndErrorPerPoint():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    tabWriter.writetactAndErrorPerPoint(fL,nL,savePath,threshold)    
    
def ErrorExtraVInt():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]

    
    savePath='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    tabWriter.writeErrorExtraVIntTab(eP,fL,savePath,nL,threshold)
    
    
def AllValueTabs():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]

    savePath='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    

    
    #read.writeDataInFile(fSISVI,lL,tL,1)
    #read.writeDataInFile(fSIOC,lL,tL,1)
    #read.writeDataInFile(fGS,lL,tL,1)
      
    
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fSIOC],listEvaluationPoints)
    
    tabWriter.writeTactTable(fL,nL,savePath,threshold)
    print('wrote tact Tab')
    tabWriter.writeAPDTable(fL,nL,savePath,threshold)
    print('wrote apd Tab')
    tabWriter.writeIntervalLtwoNormTab(fL,nL,savePath,threshold)

def plotSchwinger():
    lL=[2,3,4,5]
    tL=[0]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    #listEvaluationPoints=[8]
    
    
    savePath='../../../thesis-lindner/thesis/images/erstmal/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=3
    for p in listEvaluationPoints:
        plot.Unterschwinger(fL,nL,threshold,p,j,savePath)
        plot.Oberschwinger(fL,nL,threshold,p,j,savePath)
def plotOnePotential():
    lL=[5]
    tL=[0]
    sdt=0.0004
    endTime=0.65
    #read.writeDataInFile(fGS,[5],[4],1)
    listEvaluationPoints=[1]
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    resize=10
    for p in listEvaluationPoints:
        plot.plotSpaceForPointp(fSIOC,p,0,1,resize,'AktionspotentialtTP','')
    
def plotPotential():
    lL=[2,3,4,5]
    tL=[0,1,2,3]#,3]
    sdt=0.0004
    endTime=0.65
    #read.writeDataInFile(fGS,[5],[4],1)
    listEvaluationPoints=[1,2,3,4,5,6,7,8,9]
    #listEvaluationPoints=[8]
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    resize=10
    for p in listEvaluationPoints:
        plot.plotSpaceForPointp(fSIOC,p,3,1,resize,'VortragSpaceV','') 
        plot.plotTimeForPointp(fSIOC,p,5,1,resize,'VortragTimeV','')
        #plot.plotSpaceForPointp(fSISVI,p,3,1,resize,'SVISpaceV','')
        #resize=1
        #plot.plotTimeForPointp(fSISVI,p,5,1,resize,'SVITimeV','')
        #plot.plotSpaceForPointp(fGS,p,3,1,resize,'GSSpaceV','') 
        #plot.plotTimeForPointp(fGS,p,5,1,resize,'GSTimeV','')
    #plot.plotSpaceForPointp(fSIOC,9,3,1,1)
def plotExtraError():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    #listEvaluationPoints=[8]
        
    savePath='../../../thesis-lindner/thesis/images/erstmal/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=tL[-1]
    l=lL[-1]
    for p in listEvaluationPoints:
        plot.VIntErrorInSpace(fL,nL,threshold,p,j,savePath,fSIOC)
        plot.VIntErrorInTime(fL,nL,threshold,p,l,savePath,fSIOC)
def plotTactErrorPerPoint():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    #listEvaluationPoints=[8]
        
    savePath='../../../thesis-lindner/thesis/images/erstmal/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=tL[-1]
    l=lL[-1]
    for p in listEvaluationPoints:
        plot.tactErrorInSpace(fL,nL,threshold,p,j,savePath,fSIOC)
        plot.tactErrorInTime(fL,nL,threshold,p,l,savePath,fSIOC)
    
def plotSchwinger():
    lL=[2,3,4,5]
    tL=[0]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]
    #listEvaluationPoints=[8]
    
    
    savePath='../../../thesis-lindner/thesis/images/erstmal/'
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    j=3
    for p in listEvaluationPoints:
        plot.Unterschwinger(fL,nL,threshold,p,j,savePath)
        plot.Oberschwinger(fL,nL,threshold,p,j,savePath)
        
        
def WriteDurationPerTimeStepTab():
    lL=[2,3,4]
    tL=[0]
    sdt=0.0004
    endTime=0.004
    eP='../../../data/biventricleTT/SimpleExcitation/GerachDiffusion/TimeTest/'

    sP='../../../thesis-lindner/thesis/tables/erstmal/TTGD/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[fL[0]],[])
    tabWriter.writeComparisionDurationTab(eP,fL,sP,nL,5)
def WorkPrecisonDiagram(case):
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    sdt=0.0004
    endTime=0.65
    listEvaluationPoints=[2,3,4,5,6,7,8,9]

    casename=''
    j=0
    l=0
    if case=='' or case=='tact':
        j=3
        l=5
    elif case=='Grob' or case=='tactGrob':
        j=0
        l=2

    point=6
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    sP='images/erstmal/'
    savePath='../../../thesis-lindner/thesis/images/erstmal/'
    ydescription=''
    if case=='tact'or case=='tactGrob':
        ydescription='$\\errortact$'
    else:
        ydescription='$\\errorVact$'
    
    (nameS,minprocS)=plot.workPrecisionInSpaceExtra(fSIOC,j,fL,nL,savePath,point,ydescription,case)
    (nameT,minprocT)=plot.workPrecisionInTimeExtra(fSIOC,l,fL,nL,savePath,point,ydescription,case,minprocS)
    print(minprocS,minprocT)
    filename='../../../thesis-lindner/thesis/images/erstmal/'+'WPMono'+case+'FigP'+str(point)
    caption=''
    if case=='tact'or case=='tactGrob':
        caption='Aufwand-Genauigkeit-Diagramm der Aktivierungszeit $\\tact$ für \\SISVI, \\SIOC~und \GPDE~mit festem Zeitlevel $j='+str(j)+'$ (links) und festem Ortslevel $\ell='+str(l)+'$ (rechts). Der Aufwand wird anhand der auf $\\#$procs$='+str(minprocS)+'$ (links) und $\\#$procs$='+str(minprocT)+'$ (rechts) skalierten Rechenzeiten und die Genauigkeit wird durch den Fehler $\\errortact$ bestimmt '
    else:
        caption='Aufwand-Genauigkeit-Diagramm des Verlaufs der Transmembranspannung $\\V$ für \\SISVI, \\SIOC~und \GPDE~mit festem Zeitlevel $j='+str(j)+'$ (links) und festem Ortslevel $\ell='+str(l)+'$ (rechts). Der Aufwand wird anhand der auf $\\#$procs$='+str(minprocS)+'$ (links) und $\\#$procs$='+str(minprocT)+'$ (rechts) skalierten Rechenzeiten und die Genauigkeit wird durch den Fehler $\\errorVact$ bestimmt '
    label='\\label{fig:WPMono'+case+'P'+str(point)+'}'
    with open(filename+'.tex', 'w') as output_file:
        output_file.write('\\begin{figure}[h]\n')
        output_file.write('\\centering\n')
        output_file.write('\\resizebox{7.5cm}{!}{\\input{'+sP+nameS+'}}\n')
        output_file.write('\\resizebox{7.5cm}{!}{\\input{'+sP+nameT+'}}\n')
        output_file.write('\\caption{'+caption+'.}\n')
        output_file.write(label+'\n')
        output_file.write('\\end{figure}\n') 
if __name__=="__main__":
    language='d'
    
    plotOnePotential()
    #plotPotential()
    
    diffcase ='tact'
    #DifferencePerPointTabs(diffcase)
    diffcase='VDiffInt'
    #DifferencePerPointTabs(diffcase)
    
    #tactValuesAndErrorPerPoint()
    #plotTactErrorPerPoint()
    
    #ErrorExtraVInt()
    #plotExtraError()
    
    #plotSchwinger()
    
    #WorkPrecisonDiagram('')
    #WorkPrecisonDiagram('Grob')
    #WorkPrecisonDiagram('tact')
    #WorkPrecisonDiagram('tactGrob')
    
    
    
    
    
    #AllValueTabs()
    #activationTimeExtrapolateTab()
    #WriteDurationPerTimeStepTab()
    
