import sys
sys.path.append('..')

import mpp.python.mppy as mppy
import mpp.python.mppy.utilities as utils


import pandas as pd

mpp = mppy.Mpp(project_name='CarMech',
               executable='Elphy-M++',
               kernels=4,
               mute=False)

mpp.build()
mpp.run()
