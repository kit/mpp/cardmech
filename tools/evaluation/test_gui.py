#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from math import sqrt
import argparse
import easygui


def number_of_tables():
    number_tables ="How many tables do you need? "
    title = "Number of tables"
    nb_tables = easygui.enterbox(number_tables, title)
    return int(nb_tables)   

def concatenate(tables_needed):
    if tables_needed >= 2:
        append = {"yes", "no"}
        msg ="Do you want to concatenate the tables? "
        title = "Concatenation needed?"
        concatenate_input = easygui.choicebox(msg, title, append)
    elif tables_needed == 1:
        concatenate_input="no"
    return concatenate_input


def choose_material():
    possible_material = {"Linear", "Bonet", "Holzapfel"}
    material ="Which material do you want to look at? "
    title = "Choose material!"
    material = easygui.choicebox(material, title, possible_material)
    return material   

def choose_degree():
    possible_degree = {"linear", "quadratic"}
    elements ="Which elements do you want to take? "
    title = "Choose polynomial degree!"
    degree = easygui.choicebox(elements,title, possible_degree)
    return degree

def choose_values():
    choices = ["Step", "ApexY", "ApexX","H1Norm(gamma_u)", "L2Norm(gamma_u)",
                         "FrobeniusNorm(gamma_u)", "ApexZ", "H1Norm(gamma_v)", "RA-Volume",
                         "LA-Volume", "LV-Volume", "MyocardThickness", "FrobeniusNorm(gamma_v)", 
                         "RV-Volume", "MyocardStrain", "L2AvgNorm(u)", "Time", "FrobeniusNorm(v)",
                         "L2Norm(gamma_v)", "Geometry-Volume", "H1Norm(u)", "L2Norm(u)", 
                         "EnergyNorm(u)", "FrobeniusNorm", "L2Norm(v)", "H1Norm(v)"]
    msg ="Which value do you want to look at?"
    title = "Choose values!"
    choice = easygui.choicebox(msg, title, choices)
    return choice 


def write_dat_file(name, datadict):
    with open(str(name + '.dat'), 'w') as output_file:
        datalist = []
        N=0
        datastring = ""
        for dataname, data in datadict.items():
            datalist.append(data)
            N = len(data)
            datastring += dataname + ","
        output_file.write(str(datastring[:-1] + "\n"))

        M=len(datalist)
        for n in range(N):
            datastring = ""
            for m in range(M):
                datastring+=str(datalist[m][n]) + ","
            output_file.write(str(datastring[:-1] + "\n"))

def generate_table(material, polynomial_degree, data_value, name, lists, concatenate, nb_t, needed_tables):
    values = {"t": time1}
    N = len(time1)
    for i in range(len(lists)):
        values.update({str(i): lists[i]})

    errors = [0 for _ in range(len(lists))]
    for n in range(N):
        for i in range(len(lists)-1):
            errors[i] += 1/N * (lists[i][n] - lists[-1][n])*(lists[i][n] - lists[-1][n])

    errorstring = data_value + "Errors "
    ratestring = data_value + "Rates "
    for i in range(len(lists) - 1):
        errors[i] = sqrt(errors[i])
        errorstring += "& $" + str(round(errors[i], 6))+ "$ "
        if i>0:
            ratestring += "& $" + str(round(errors[i-1] / errors[i],2)) + "$ "
    print(errorstring)
    print(ratestring)
    if concatenate == "yes":
        if nb_t == 1:
            with open(str(name) + "_Tables" + ".tex", "w") as o:
                o.write("\\begin{table}\n ")
                o.write("\centering \n ")
                o.write(" \\begin{tabular}[t]{c|")
                for j in range(len(lists)):
                    o.write("c")
                    if j == len(lists) - 1:
                        o.write("}  \n")
                o.write(errorstring + " \\\ \n")
                o.write(ratestring + "\\\ \n")
        elif nb_t < needed_tables:
            with open(str(material) + str(name) + "_Tables" + ".tex", "a") as o:
                o.write(errorstring + " \\\ \n")
                o.write(ratestring + "\\\ \n")
        elif nb_t == needed_tables:
             with open(str(name) + "_Tables" + ".tex", "a") as o:
                o.write(errorstring + " \\\ \n")
                o.write(ratestring + "\\\ \n")
                o.write("\\end{tabular}\n ")
                o.write("\\end{table}\n ")
    elif concatenate == "no":
        with open(str(material) + str(name) + "_Tables" + ".tex", "w") as o:
                o.write("\\begin{table}\n ")
                o.write("\centering \n ")
                o.write(" \\begin{tabular}[t]{c|")
                for j in range(len(lists)):
                    o.write("c")
                    if j == len(lists) - 1:
                        o.write("}  \n")
                o.write(errorstring + " \\\ \n")
                o.write(ratestring + " \n")
                o.write("\\end{tabular}\n ")
                o.write("\\end{table}\n ")
    write_dat_file(material + "_" + polynomial_degree + "_" + data_value, values)       



needed_tables = number_of_tables()
concatenate = concatenate(needed_tables)
nb_t = 1
append_tables = []

print("All following choices are made for the " + str(nb_t) + ". table!")
material = choose_material()



while nb_t <= needed_tables:
    polynomial_degree = choose_degree()
    choice = choose_values()

    path  = "/home/laura/Schreibtisch/cardmech/data/activestrain/" + str(material) + "/" + polynomial_degree +  "/*" 
    logfiles = easygui.fileopenbox(default=path,multiple=True)

    length_log = range(len(logfiles))
    
    
    Level = []
    data_test = []
    test_data = []
    test_time = []
    
    
    for l in length_log:
        data = []
        with open(logfiles[l], 'r') as g: 
            time = "Time" + " = ["
            t = len(time)
            for line1 in g:
                start1 = line1.find(time)
                if start1 == -1:
                    continue
                end1 = line1.find("]")   
                time = (line1[start1 + t:end1])
                time1 = time.split(",")
                time1 = [float(v) for v in time1]
                values_test = {"t": time1}
    
    
        #FullCardmechInfo
        parser = argparse.ArgumentParser()
        args = parser.parse_args()

        value = {}
        
        value = choice  + " = ["
        j = len(value)

        with open(logfiles[l], 'r') as f: 
            for line in f:
                start = line.find(value)
                if start == -1:
                    continue
                end = line.find("]")   
                Step = (line[start + j:end])
                Step1 = Step.split(",")
                Step1 = [float(u) for u in Step1]
                
            
                data_test.append(Step1)
                test_data.append(data)
                data.extend(data_test) 
                for w in range(len(Level)):
                    values_test.update({str(w): data[w]})
         
    if concatenate == "yes":        
        generate_table(material,polynomial_degree, choice, "Errors", data, concatenate, nb_t, needed_tables)    #"GuccioneL2NormP1"
    elif concatenate == "no":  
        generate_table(material, polynomial_degree, choice, str(choice), data, concatenate, nb_t, needed_tables)
    nb_t += 1 