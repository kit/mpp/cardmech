import numpy as np
import math
import matplotlib.pyplot as plt
from utility import reading as read
from utility import computing as comp
from utility import plotting as plotting
from utility.latexTables import Table

lList = [1,2,3]
tlist = [0,1,2,3,4]
start_dx = 0.25
start_dt=0.0001
T=0.0003
colorList = ['black','red','green','orange','purple','brown','blue']
substep=1


def writeDurationTable(fP1,fP2,fP3,name1,name2,name3):
    #Iext
    write2(fP1,fP2,name1,name2,1,1,'Duration of updating $\\Iext$ in s','DurationIext')
    #assembleA  
    write2(fP1,fP2,name1,name2,2,3,'Duration of assembling system matrix in s','DurationAssembleA')
    #invertA
    write2(fP1,fP2,name1,name2,2,4,'Duration of inverting system matrix in s','DurationInvertA')
    #assembleRHS 
    write2(fP1,fP2,name1,name2,2,5,'Duration of assembling rhs in s','DurationAssembleRHS')
    # gmres
    write3(fP1,fP2,fP3,name1,name2,name3,1,6,'Duration of GMRES in s','DurationGMRES')
    #PDE[7]
    write3(fP1,fP2,fP3,name1,name2,name3,1,7,'Duration of solving the PDE in s','DurationPDE')
    #gating[2]  ion[8]
    evaluationNames=['Update Gating Variables', 'Update ion concentration']
    write1(fP2,name2,1,[2,8],'Duration of updating $\\w$ and $\\c$ in s','DurationLIWPDEC',evaluationNames)
    #ODE[2] SplittingStep[8]
    evaluationNames=['Solve ODE', 'Splitting Step']
    write1(fP1,name1,1,[2,8],'Duration of solving the ODE system and solving the splitting step in total in s','DurationGODESS',evaluationNames)

def write1(fP1,name1,n,c,caption,saveName,n_names):
    tab1=Table(lList, tlist,start_dt,T,substep,fP1)
    with open('../../../datafiles/3DTetra/DurationTest/' +saveName+'.tex', 'w') as output_file:
            output_file.write(tab1.writeBeginDurationTable(caption))
            output_file.write(name1+' & & & & \\\\\n')
            for m in range(len(c)):
                output_file.write(n_names[m]+' & & & & \\\\\n')
                output_file.write(tab1.writeRowsDurationSteps(n,c[m]))
                output_file.write('\\hline\n')
            output_file.write(tab1.writeEndTable())
            
def write2(fP1,fP2,name1,name2,n,c,caption,saveName):
    tab1=Table(lList, tlist,start_dt,T,substep,fP1)
    tab2=Table(lList, tlist,start_dt,T,substep,fP2)
    with open('../../../datafiles/3DTetra/DurationTest/' +saveName+'.tex', 'w') as output_file:
            output_file.write(tab1.writeBeginDurationTable(caption))
            output_file.write(name1+' & & & & \\\\\n')
            output_file.write(tab1.writeRowsDurationSteps(n,c))
            output_file.write('\\hline\n')
            output_file.write(name2+' & & & & \\\\\n')
            output_file.write(tab2.writeRowsDurationSteps(n,c))
            output_file.write('\\hline\n')
            output_file.write(tab1.writeEndTable())
def write3(fP1,fP2,fP3,name1,name2,name3,n,c,caption,saveName):
    tab1=Table(lList, tlist,start_dt,T,substep,fP1)
    tab2=Table(lList, tlist,start_dt,T,substep,fP2)
    tab3=Table(lList, tlist,start_dt,T,substep,fP3)
    with open('../../../datafiles/3DTetra/DurationTest/' +saveName+'.tex', 'w') as output_file:
            output_file.write(tab1.writeBeginDurationTable(caption))
            output_file.write(name1+' & & & & \\\\\n')
            output_file.write(tab1.writeRowsDurationSteps(n,c))
            output_file.write('\\hline\n')
            output_file.write(name2+' & & & & \\\\\n')
            output_file.write(tab2.writeRowsDurationSteps(n,c))
            output_file.write('\\hline\n')
            output_file.write(name3+' & & & & \\\\\n')
            output_file.write(tab3.writeRowsDurationSteps(n,c))
            output_file.write('\\hline\n')
            output_file.write(tab1.writeEndTable())
            
def writeList(pathList,nameList,n,c,caption,saveName):
    if len(pathList)!=len(nameList):
        print("dimensions mismatch!!!!")
        return
    with open('../../../datafiles/3DTetra/DurationTest/' +saveName+'.tex', 'w') as output_file:
        output_file.write(writeBeginDurationTable(caption))
        for index in range(len(pathList)):
            tab=Table(lList, tlist,start_dt,T,substep,pathList[index])
            output_file.write(nameList[index]+' & & & & \\\\\n')
            output_file.write(tab.writeRowsDurationSteps(n,c))
            output_file.write('\\hline\n')
        output_file.write(tab.writeEndTable())

def writeBeginDurationTable(capt):
    block = ''
    block+= '\\begin{longtable}[H]{l'
    for l in lList:
        block +='|r'
    
    block+='|l} \n'
    block+= '\\caption{'+capt+'.}' + '\\\\ \n' 
    for l in lList:
        block += ' & $\\ell='+str(l)+'$'
    block+= '& \\\\\n'        
    block+='\\hline\n'
    return block
if __name__=="__main__":
    #extracts data from logfile into txt file
    fPGStandard='../../../datafiles/3DTetra/DurationTest/G0001SAT/'
    fPGReOnce = '../../../datafiles/3DTetra/DurationTest/G0001SATReassembleOnce/'

    fPLI='../../../datafiles/3DTetra/DurationTest/LI0001SAT/'
    fPLIREOnce='../../../datafiles/3DTetra/DurationTest/LI0001SATReassembleOnce/'
    fPLI_GS='../../../datafiles/3DTetra/DurationTest/LI0001SATGaussSeidel/'
    #read.ExtractDurations(fPGStandard,lList,tlist,1)
    #read.ExtractDurations(fPLI,lList,tlist,1)
    paths5=[fPGStandard,fPGReOnce,fPLI,fPLIREOnce,fPLI_GS]
    names5=['(G)','G Reassemble Once','(LI)','(LI) Reassemble Once','(LI) with Gauss Seidel']
    paths4=[fPGStandard,fPGReOnce,fPLI,fPLIREOnce]
    names4=['(G)','G Reassemble Once','(LI)','(LI) Reassemble Once']
    #for path in paths4:
        #read.ExtractDurations(path,lList,tlist,1)
    
    #writeDurationTable(fPGStandard,fPLI,fPLI_GS,'(G)','(LI)','(LI) with Gauss Seidel')
    
    #LI: [gating[i],assembleA[i],assembleRHS[i],gmres[i],PDE[i],ion[i]]
    #G: ODE[i],assembleA[i],assembleRHS[i],gmres[i],PDE[i],SplittingStep[i]
    #assembleRHS
    writeList(paths4,names4,2,2,'Duration of assembling rhs in s','DurationAssembleRHS')
    # gmres
    writeList(paths4,names4,1,3,'Duration of GMRES in s','DurationGMRES')
    #PDE[7]
    writeList(paths4,names4,1,4,'Duration of solving the PDE in s','DurationPDE')
    #gating[2]  ion[8]
    #evaluationNames=['Update Gating Variables', 'Update ion concentration']
    #write1(fP2,name2,1,[2,8],'Duration of updating $\\w$ and $\\c$ in s','DurationLIWPDEC',evaluationNames)
    #ODE[2] SplittingStep[8]
    #evaluationNames=['Solve ODE', 'Splitting Step']
    #write1(fP1,name1,1,[2,8],'Duration of solving the ODE system and solving the splitting step in total in s','DurationGODESS',evaluationNames)
   
    
    
