import matplotlib.pyplot as plt
import numpy


def plot_from_logfile(location, name):
    V = []
    with open(location + '/' + name, 'r') as logfile:
        for line in logfile:
            try:
                V.append(float(line.strip()))
            except ValueError:
                print('Line [', line.strip(), '] was excluded.')

    T = numpy.linspace(0,1,len(V))
    plt.plot(T, V)
    plt.ylabel("Activation potential")
    plt.show()


if __name__ == '__main__':
    plot_from_logfile('../../build/log', 'log')
