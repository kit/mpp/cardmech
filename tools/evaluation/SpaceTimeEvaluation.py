import numpy as np
import math
import matplotlib.pyplot as plt
from utility import reading as read
from utility import computing as comp
from utility.plotting import Plot 
from utility import plotting as plotting
from utility.latexTables import Table
from utility.latexTables import TableWriter
from utility.documentWriter import DW

#pathStandardLI ='../../../datafiles/Level0Tests/LinearImplicit/HexaBRLinearImplicit0001IextSmoothInSpaceAndTime/'
#folderPath ='../../../datafiles/convergenceTest3D/HexaBRSplittingGodunov0001IextSmoothTimeASpace/'
#folderPathGodunov ='../../../datafiles/convergenceTest3D/HexaBRSplittingGodunov0001IextSmoothTimeASpace/'
#folderPath ='../../../datafiles/convergenceTest3D/HexaBRLinearImplicit0001IextSmoothInSpaceAndTime/'
#folderPathLinImp ='../../../datafiles/convergenceTest3D/HexaBRLinearImplicit0001IextSmoothInSpaceAndTime/'
#folderPath= '../../../datafiles/Atria/AtriaBRLinearImplicit00001IextSmoothInSpaceAndTime/'

lList = [1,2,3,4,5]
tlist = [0,1,2,3]
start_dx = 0.25



#lList = [2]
#tlist=[0,1,2,3]

listEvaluationPointsPlot =[4,5,6,7]
listEvaluationPoints=[4,5,6,7]#
#listEvaluationPointsPlot=[1,2,3,4,5]
#listEvaluationPoints=[1,2,3,4,5]
#totalNumberOfEvaluationPointsWithNorms = 11
totalNumberOfEvaluationPointsWithNorms = 9
substep=1

    
def Plots(fL,nL,ePath):
    plot=Plot(lList,tlist,start_dt,T,substep,listEvaluationPoints)
    plot.plotAllPoints(fL)
    plot.plotSpaceTimeConvergence(fL)
    #plotExtrapolates(ePath,fL,nL)
    
    #plotName="FullExtraploateLIj"+str(tlist[-1])+"Gj"+str(tlist[-1])+'P'+str(p)
    #plotDiffExtrapolates(fPLI,fPGStandard,p,lList[-1],lList[-1],tlist[-1],tlist[-1],plotName) #L1,L2,J1,J2
    for point in listEvaluationPoints:
        plot.plotVTwoTimeIntMethodsTime(fL[0],fL[-1],point,lList[-1],fL[-1],ePath)
        plot.plotVTwoTimeIntMethodsTime(fL[0],fL[-1],point,lList[0],fL[-1],ePath)
    
    
    #plot.convergencePlotTime(lList[-1])
    #plot.convergencePlotSpace(tlist[-1])
    
def Tables(fL,nL):
    tabWriter =TableWriter(lList, tlist,start_dt,T,substep,fL,listEvaluationPoints)
    tabWriter.AllExtrapolationTables()#,file)
    #FullExtapolationTable(fPLI,fPLIStandard)
    #writeDurationTable(fPG,fPLI)
    #writeMeshInfoTable(fileList[0])

def writeValueTablesSchemeComparison(eP,fL,nL):
    sdt=0.0001
    endTime=0.03
    tab=Table(lList, tlist,sdt,endTime,substep,fL)
    for p in listEvaluationPoints:
        filename=eP+'tex/'+'SchemeComparisonValuesTabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            output_file.write(tab.writeBeginValueTable(p))
            output_file.write(tab.writeValueSchemeCompTable(p,nL))
            output_file.write(tab.writeEndTable())
    
    texFileList=convertPath(fL,-1)
    texnameList=nL
    texnameList.insert(0,'DataAsymptoticTest')
    dw=DW(lList, tlist,sdt,endTime,1,texFileList,texnameList,listEvaluationPoints,'../../../thesis-lindner/Paperdraft/experiments/','../../datafiles/TimeAsymptoticTest/TestEllipsoid3dExcitation/')
    plot=Plot(lList,tlist,start_dt,T,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        plot.plotInP(p,lList[0],tlist[0],fL,nL,eP)
        plot.plotInP(p,lList[0],4,fL,nL,eP,True,1)
    dw.writeAsymptoticData(lList,4,False)

def comparisonOfQuadrature():
    lL=[4]
    tL=[0,1,2,3]
    sdt=0.0001
    endTime=0.03
    eP='../../../datafiles/TimeAsymptoticTest/TestEllipsoid3dExcitation/'
    fSI=eP+'SI0001SAT/'
    fSINodes=eP+'SI0001SATNodes/'
    fIE=eP+'IE0001SAT/'
    
    fL=[fSI,fSINodes]
    nL=['(SI)','(SI) on Nodes']
    #for file in fL:
        #read.writeDataInFile(file,lL,tL,substep,False)
    
    tab=Table(lL,tL,sdt,endTime,1,fL)
    plot=Plot([2,3,4],tL,sdt,endTime,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        filename=eP+'QuadTest/'+'tex/'+'SchemeComparisonValuesTabl'+str(lL[0])+'P'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Comparison of different time integration schemes at $\\ell='+str(lL[0])+'$ in \\textbf{P'+str(p)+'}'
            output_file.write(tab.writeBeginValueTable(p,caption))
            output_file.write(tab.writeValueSchemeCompTable(p,nL))
            output_file.write(tab.writeEndTable())
        filename=eP+'QuadTest/'+'tex/'+'SchemeComparisonValuesTabP'+str(p)
        with open(filename+'.tex', 'w') as output_file:
            caption='Comparison of different time integration schemes on different levels in \\textbf{P'+str(p)+'}'
            output_file.write(tab.writeBeginValueTable(p,caption))
            output_file.write(tab.writeValueSchemeCompTable(p,nL,[2,3,4]))
            output_file.write(tab.writeEndTable())
    for p in listEvaluationPoints:
        plot.plotInP(p,lL[0],tL[0],fL,nL,eP+'QuadTest/')
        plot.plotInP(p,lL[0],tL[-1],fL,nL,eP+'QuadTest/',True,1)
        plot.plotVTwoTimeIntMethodsTime(fSI,fSINodes,p,2,'',eP+'QuadTest/')
        plot.plotVTwoTimeIntMethodsTime(fSI,fSINodes,p,3,'',eP+'QuadTest/')
        plot.plotVTwoTimeIntMethodsTime(fSI,fSINodes,p,4,'',eP+'QuadTest/')
        plot.plotVTwoTimeIntMethodsSpace(fSI,fSINodes,p,2,eP+'QuadTest/')
        plot.plotVTwoTimeIntMethodsSpace(fSI,fSINodes,p,3,eP+'QuadTest/')
        
        
    texFileList=convertPath(fL,-1)
    texnameList=nL
    texnameList.insert(0,'DataAsymptoticTestQuad')
    dw=DW(lL, tL,sdt,endTime,1,texFileList,texnameList,listEvaluationPoints,'../../../thesis-lindner/Paperdraft/experiments/','../../datafiles/TimeAsymptoticTest/TestEllipsoid3dExcitation/QuadTest/')
    dw.writeAsymptoticData([3,4],tL[-1],True)
    
    

def writeSplittingDocument(fL,nL,evP,savePath,sourcePath):
    dw=DW(lList, tlist,start_dt,T,substep,fL,nL,evP,savePath,sourcePath)
    dw.writeSplittingTest(1,3)

def splittingTest():
    lL=[0,1,2,3]
    tL=[0,1,2,3]
    sdt=0.0001
    endTime=0.03
    evalP=[4,5,6,7]
    experimentPath = '../../../datafiles/'
    (fileList,nameList)=fileNameList3DExcEllipsoid(experimentPath)
    ref_File =fPLI_SAT =experimentPath+ 'TestEllipsoid3d/' + 'LI0001SAT/'
    ref_Name= '\\LISVI'
    L=4
    J=3
    refFList=[ref_File,experimentPath+ 'TestEllipsoid3d/' + 'SI0001SAT/']
    refNList=[ref_Name,'\\SISVI']
    #read.writeDataInFile(ref_File,[3,4],[0,1,2,3],1,True)
    plot=Plot(lL,tL,sdt,endTime,1,evalP)
    
    for point in evalP:
        plot.plotSchemesInP(point ,lL[0],tL[-1],L,J,fileList,nameList,ref_File,ref_Name,experimentPath+'TestEllipsoid3d/SplittingTest/')
        plot.plotSchemesInP(point ,lL[-1],tL[-1],L,J,fileList,nameList,ref_File,ref_Name,experimentPath+'TestEllipsoid3d/SplittingTest/',True,1)
        pn='DiffExtraLISIl'+str(L)+'j'+str(J)+'P'+str(point)
        plot.plotDiffExtrapolates(refFList,refNList,point,[L,L],[J,J],'../../../datafiles/TestEllipsoid3d/',True,pn,True)
    texFileList=convertPath(fileList,-1)
    texnameList=nameList
    texnameList.insert(0,'DataTestEllipsoidSplittingTest')
    dw=DW(lL, tL,sdt,endTime,1,texFileList,texnameList,evalP,'../../../thesis-lindner/Paperdraft/experiments/','../../datafiles/TestEllipsoid3d/SplittingTest/')
    dw.writeSplittingTest(1,3,refNList,L,J,'../../datafiles/TestEllipsoid3d/')

def compareTWO():
    experimentPath = '../../../datafiles/'
    mesh='TestEllipsoid3d/'
    ePath=experimentPath+mesh
    #(fileList,nameList)=fileNameList3DExcEllipsoid(experimentPath)   
    (fileList,nameList)=FileNameListTestEllipsoid(experimentPath,mesh)
    
    lL=[0,1,2,3,4]
    tL=[0,1,2,3]
    sdt=0.0001
    endTime=0.03
    evalP=[4,5,6,7]
    tabWriter =TableWriter(lL, tL,sdt,endTime,1,fileList,evalP)
    tabWriter.AllExtrapolationTables()
    
    plot=Plot(lL,tL,sdt,endTime,1,evalP)
    plot.plotAllPoints(fileList)
    plot.plotSpaceTimeConvergence(fileList)
    for point in evalP:
        plot.plotVTwoTimeIntMethodsTime(fileList[0],fileList[-1],point,lL[-1],fileList[-1],ePath)
        plot.plotVTwoTimeIntMethodsTime(fileList[0],fileList[-1],point,lL[0],fileList[-1],ePath)
    texFileList=convertPath(fileList,-1)
    texnameList=nameList
    texnameList.insert(0,'DataTestEllipsoid3dGLI')
    dw=DW(lL, tL,sdt,endTime,1,texFileList,texnameList,evalP,'../../../thesis-lindner/Paperdraft/experiments/','../../datafiles/TestEllipsoid3d/')
    dw.write(4)
    
def  convertPath(fL,n):
    newList=[]
    for file in fL:
        f=''
        if n<0:
            f=file[(abs(n)*3):]
        elif n>0:
            for i in range(n):
                f+='../'
            f+=file
        else:
            f=file
        newList.append(f)
    return newList


def FileNameListCuboid():
    fPGStandard='../../../datafiles/3DTetra/TetraG0001SAT/'
    fPGIE='../../../datafiles/3DTetra/TetraG0001SATIE/'
    fPGinODE='../../../datafiles/3DTetra/TetraG0001SATinODE/'
    fPLIStandard='../../../datafiles/3DTetra/TetraLI0001SAT/'
    fPLI='../../../datafiles/3DTetra/LI0001SATNodesReassembleOnce/'
    fileList =[fPGStandard,fPGIE,fPGinODE,fPLIStandard,fPLI]
    nameList=['(G)','(G) IE','(G) in ODE', '(LI)','(LIoN)']
    return(fileList,nameList)
    
def FileNameListEllipsoid(): 
    fPGStandard = '../../../datafiles/Ellipsoid2Layer/G0001SATReassembleOnce/'
    fPGinODE='../../../datafiles/Ellipsoid2Layer/G0001SATinODEReassembleOnce/'
    fPLIStandard = '../../../datafiles/Ellipsoid2Layer/LI0001SATReassembleOnce/'
    fPLINodes = '../../../datafiles/Ellipsoid2Layer/LI0001SATNodesReassembleOnce/'
    fPSI = '../../../datafiles/Ellipsoid2Layer/SI0001SAT/'
    fPGtex = '../../datafiles/Ellipsoid2Layer/G0001SATReassembleOnce/'
    fPGinODEtex='../../datafiles/Ellipsoid2Layer/G0001SATinODEReassembleOnce/'
    fPLItex = '../../datafiles/Ellipsoid2Layer/LI0001SATReassembleOnce/'
    fPLINodestex = '../../datafiles/Ellipsoid2Layer/LI0001SATNodesReassembleOnce/'
    fPSItex = '../../datafiles/Ellipsoid2Layer/SI0001SAT/'
    fL=[fPGinODE,fPGStandard,fPLIStandard,fPLINodes,fPSI]
    #fileList=[fPSI]
    nL=['(GinODE)','(G)','(LI)','(LIoN)','(SI)']
    return (fL,nL)

def FileNameListEllipsoidOriented():
    fPGStandard = '../../../datafiles/Ellipsoid2LayerOriented/G0001SAT/'
    fPLIStandard = '../../../datafiles/Ellipsoid2LayerOriented/LI0001SAT/'
    fPGtex ='../../datafiles/Ellipsoid2LayerOriented/G0001SAT/'
    fPLItex='../../datafiles/Ellipsoid2LayerOriented/LI0001SAT/'
    fL=[fPGStandard,fPLIStandard]
    nL=['(G)','(LI)']
    return (fL,nL)

def FileNameListTestEllipsoid(mP,mesh):
    fPG_SAT_IE=mP+mesh+'G0001SATIE/'
    #fPG_Original =mP+'TestEllipsoid/SplittingTest/'+'G0001Original/'
    #fPG_IE_inODE=mP+'TestEllipsoid/SplittingTest/'+'G0001IEODE/'
    #fPG_T_IE_inODE=mP+'TestEllipsoid/SplittingTest/'+'G0001TIEODE/'
    #fPG_SAT_IE_inODE =mP+'TestEllipsoid/SplittingTest/' +'G0001SATIEODE/'
    #fPG_T_IE=mP+'TestEllipsoid/SplittingTest/' +'G0001TIE/'
    fPLI_SAT =mP+mesh+ 'LI0001SAT/'
    
    #fL=[fPG_SAT_IE,fPG_Original,fPG_IE_inODE,fPG_T_IE_inODE,fPG_SAT_IE_inODE,fPG_T_IE]
    #nL=['(G) IE', '(G) Original','(G) IE ODE no smoothing','(G) IE in ODE time smoothing','(G) smooth IE in ODE','(G) IE time smooth']
    fL=[fPG_SAT_IE,fPLI_SAT]
    nL=['\\GPDE','\\LISVI']
    return (fL,nL)

def fileNameList3DExcEllipsoid(mP):
    fPG_SAT_IE=mP+'TestEllipsoid3d/SplittingTest/G0001SATIE/'
    fPG_Original =mP+'TestEllipsoid3d/SplittingTest/'+'G0001Original/'
    #fPG_IE_inODE=mP+'TestEllipsoid3d/SplittingTest/'+'G0001IEODE/'
    fPG_T_IE_inODE=mP+'TestEllipsoid3d/SplittingTest/'+'G0001TIEODE/'
    fPG_SAT_IE_inODE =mP+'TestEllipsoid3d/SplittingTest/' +'G0001SATIEODE/'
    fPG_T_IE=mP+'TestEllipsoid3d/SplittingTest/' +'G0001TIE/'
    fPLI_SAT =mP+ 'TestEllipsoid3d/' + 'LI0001SAT/'
    fL=[fPG_SAT_IE,fPG_Original,fPG_T_IE_inODE,fPG_SAT_IE_inODE,fPG_T_IE]#,fPLI_SAT]
    nL=['\\GPDE-SAT', '\\GODE-CN','\\GODE-T','\\GODE-SAT','\\GPDE-T']#,'(LI) SAT']
    return (fL,nL)

def filesAsymtoticTest(mp):
    eP=mp
    fG=eP+'G0001SATIE/'
    fLI=eP+'LI0001SAT/'
    fLiNodes=eP+'LI0001SATNodes/'
    fSI=eP+'SI0001SAT/'
    fSINodes=eP+'SI0001SATNodes/'
    fIE=eP+'IE0001SAT/'
    
    fL=[fG,fLI,fLiNodes,fSI,fSINodes,fIE]
    nL=['(G) IE', '(LI)','(LI) on Nodes','(SI)','(SI) on Nodes','(IE) ']
    return (fL,nL)
    
if __name__=="__main__":
    
    #experimentPath = '../../../datafiles/TimeAsymptoticTest/TestEllipsoid3dExcitation/'
    experimentPath = '../../../datafiles/'
    mesh='TestEllipsoid/'
    mesh='TestEllipsoid3d/'
    #(fileList,nameList)=fileNameList3DExcEllipsoid(experimentPath)   
    (fileList,nameList)=FileNameListTestEllipsoid(experimentPath,mesh)
    
    #(fileList,nameList)=filesAsymtoticTest(experimentPath)
    
    
    #read.writeDataInFile(fileList[-1],[3],[0,1,2,3],substep,True)
    #for file in fileList:
        #read.writeDataInFile(file,[0,1,2,3,4],[0,1,2,3],substep)
    
    #(T,start_dt)=read.getTimeData(fileList[0],lList[0],tlist[0],substep)
    #comparisonOfQuadrature()
    #writeValueTablesSchemeComparison(experimentPath,fileList,nameList)
    splittingTest()
    #compareTWO()
    
    
    #Plots(fileList,nameList,experimentPath+'TestEllipsoid/')
    #Tables(fileList,nameList)

