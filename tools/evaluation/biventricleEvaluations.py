from utility import reading as read
from utility import computing as comp
from utility.plotting import Plot 
from utility.latexTables import Table
from utility.latexTables import TableWriter
from utility.documentWriter import DW
import Meshinfos

pathBR='../../../datafiles/KovachevaBiventricle/BR/'
pathTT='../../../datafiles/KovachevaBiventricle/TT/'


def Histplot(Path):
    path = Path+'SI0004/vtu/AT_l0j0.vtu'
    savePath=Path+'SI0004/tex/HistBiVentriclel0.tex'
    
    Meshinfos.comHistplot(path,savePath)

            
def table8():
    lL=[0,1,2,3,4]
    tL=[0,1,2,3,4]
    SI=pathBV+'SI0004/'
    sdt=0.0004
    endTime=0.16
    listEvaluationPoints=[]
    l0Path=pathBV+'SI0004/vtu/AT_l0j0.vtu'
    #read.changeNumberOFCellsiInVTU(SI+'data/',[4],[1,2,3,4],1)

    #plotTact:
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    backupArray=[[-1.0,59.295870358255655,57.96067339027709,57.5545764529468,57.468872878475665],[49.749577731126486,45.46817896679911,43.798303227582146,43.28796384626254,43.16367877136338],[44.95291943209599,40.81325902629804,39.09870508453703,38.55494553911038,38.41666873598738],[43.36173025251009,38.80735467113771,36.91194880075053,36.30816906164418,36.15129271842806],[42.583927617282406,37.81610107773048,35.8441826475743,35.229766544910895,35.07516596693433]]

    ValueArray=plot.MeanTactSpace(SI,l0Path,backupArray)
    #print('len value array: ', len(ValueArray))
    print('Plot finished')
    #tables tact
    L=4
    J=4
    savePath='images/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[SI],listEvaluationPoints)
    tabWriter.writeMeanTactTable(SI,l0Path,savePath,L,J,SI,backupArray)
    
def plotConvergenceActivationtime(path):
    lL=[0,1,2,3]
    tL=[0,1,2]#,3]
    SI=path+'SI0004/'
    sdt=0.0004
    endTime=0.1
    listEvaluationPoints=[]
    l0Path=path+'SI0004/vtu/AT_l0j0.vtu'
    #plotTact:
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    ValueArray=plot.MeanTactSpace(SI,l0Path,[])
    print('Plot finished')
    savePath='images/'
    tabWriter = TableWriter(lL,tL,sdt,endTime,1,[SI],listEvaluationPoints)
    L=0
    J=0
    tabWriter.writeMeanTactTable(SI,l0Path,savePath,L,J,SI,ValueArray)
    
def plotPotential(path):
    lL=[0,1,2,3]
    tL=[0,1,2]#,3]
    SI=path+'SI0004/'
    sdt=0.0004
    endTime=0.3
    listEvaluationPoints=[1,2,3,4]
    nL=['\\SISVI']
    
    #read.writeDataInFile(SI,lL,tL,1,True)
    
    plot=Plot(lL,tL,sdt,endTime,1,listEvaluationPoints)
    for p in listEvaluationPoints:
        #convergence in space
        plot.plotSpaceForPointp(SI,p,2,1,1)   
   
    #plotallPoints,
    plot.pointsInPlotForFixlAndjAndm(SI,3,2,1,1)

if __name__=="__main__":
    
    #Histplot(pathBR)
    #plotConvergenceActivationtime(pathBR)
   # plotConvergenceActivationtime(pathTT)
    #plotConvergenceActivationtime(pathBR+"OldDiff/")
    #plotConvergenceActivationtime(pathTT+"OldDiff/")
    plotPotential(pathBR+"OldDiff/")
    plotPotential(pathTT)
