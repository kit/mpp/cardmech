import datetime
import os
import easygui
import pandas as pd
from fillpdf import fillpdfs
from PyPDF2 import PdfFileMerger, PdfFileReader

ORIGINAL_PDF = 'Arbeitszeitdokumentation.pdf'

if __name__ == "__main__":
    try:
        os.mkdir("tmp")
    except FileExistsError:
        pass
    ods_file = easygui.fileopenbox()
    data_in = easygui.multenterbox("Bitte Name eingeben:", "Namensabfrage",
                                   ["Nachname", "Vorname", "OE", "Stundenlohn", "Soll-Stunden pro Monat"],
                                   ["", "", "IANM3", "10.77", 30])

    name = data_in[0] + ", " + data_in[1]
    name_flat = name.replace(", ", "")

    oe = data_in[2]
    hourly_rate = data_in[3]
    hours_per_month = data_in[4]
    hours_per_month_int = int(data_in[4])

    print("Datei:", ods_file)
    print("Name:", name)
    print("OE:", oe)
    print("Stundenlohn:", hourly_rate)
    print("Soll-Stunden:", hours_per_month)

    input("Weiter? CTRL+C für Abbruch, Enter für Weiter")

    print("Einlesen der ods-Datei ....")
    in_data = pd.read_excel(ods_file, engine="odf")

    months = {}
    months_sorted = []
    important_cols = ["Tätigkeit", "Datum", "Arbeitszeit", "Beginn", "Ende", "Pause", "Urlaub"]
    data_id = {k: list(in_data.columns).index(k) for k in important_cols}
    for k in in_data.iterrows():
        data = list(k[1])
        y, m, d = str(data[data_id["Datum"]]).split(" ")[0].split("-")
        if m not in months:
            months_sorted.append(m)
            months[m] = []
        if type(data[data_id["Arbeitszeit"]]) == float and data[data_id["Arbeitszeit"]] > 0:
            data_for_line = {k: data[data_id[k]] for k in important_cols}
            data_for_line["Datum"] = ".".join([d, m, y])
            months[m].append(data_for_line)
        if type(data[data_id["Urlaub"]]) is datetime.time:
            urlaubsstunden = data[data_id["Urlaub"]].hour + data[data_id["Urlaub"]].minute / 60
            data_for_line = {"Tätigkeit": "Urlaub", "Arbeitszeit": urlaubsstunden}
            data_for_line["Datum"] = ".".join([d, m, y])
            months[m].append(data_for_line)

    data_out_original = fillpdfs.get_form_fields(ORIGINAL_PDF)

    vormonat = 0
    files = []
    files_flat = []
    for month_id in months_sorted:
        data_out = dict(data_out_original)
        summe = 0.0
        urlaub = 0.0
        for r, line in enumerate(months[month_id]):
            data_out[f"Tätigkeit Stichwort ProjektRow{r}"] = line["Tätigkeit"]
            summe += line["Arbeitszeit"]
            workingtime = datetime.time(int(line["Arbeitszeit"]), int((line["Arbeitszeit"] % 1) * 60))
            data_out[f"hhmmRow{r}_4"] = workingtime.isoformat(timespec='minutes')
            data_out[f"ttmmjjRow{r}"] = line["Datum"]
            if line["Tätigkeit"] == "Urlaub":
                urlaub += line["Arbeitszeit"]
            else:
                data_out[f"hhmmRow{r}"] = line["Beginn"].isoformat(timespec='minutes')
                data_out[f"hhmmRow{r}_2"] = line["Ende"].isoformat(timespec='minutes') 
                if line["Pause"] != line["Pause"]:
                    data_out[f"hhmmRow{r}_3"] =  ""
                else:
                    data_out[f"hhmmRow{r}_3"] = line["Pause"].isoformat(timespec='minutes') if line["Pause"] > line["Pause"].min else ""
                    
        data_out["Ich bestätige die Richtigkeit der Angaben"] = line["Datum"] # Datum, Unterschrift <- Datum von letztem Eintrag
        data_out["abc"] = month_id
        data_out["abdd"] = str(line["Datum"].split(".")[-1])
        data_out["Std"] = hours_per_month
        data_out["monatliche SollArbeitszeit"] = hours_per_month
        data_out["GF"] = name
        data_out["OE"] = oe
        data_out["UB"] = "Off"
        data_out["GFB"] = "Off"
        data_out["Stundensatz"] = hourly_rate
        data_out["Summe"] = str(summe)
        data_out["Urlaub anteilig"] = str(urlaub)
        data_out["Übertrag vom Vormonat"] = str(vormonat)
        vormonat += summe - hours_per_month_int
        data_out["Übertrag in den Folgemonat"] = str(vormonat)
        file = f'tmp/filledDokuLukas{month_id}.pdf'
        fillpdfs.write_fillable_pdf(ORIGINAL_PDF, file, data_out)
        file_flat = f'tmp/filledDokuLukas{month_id}_flat.pdf'
        fillpdfs.flatten_pdf(file, file_flat)
        files.append(file)
        files_flat.append(file_flat)

    mergedObject = PdfFileMerger()
    for file in files_flat:
        mergedObject.append(PdfFileReader(file, 'rb'))
    final_file_name = f"ZeitDoku{name_flat}.pdf"
    mergedObject.write(final_file_name)
    print("Datei erstellt:", final_file_name)
