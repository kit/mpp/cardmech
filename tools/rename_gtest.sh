#!/usr/bin/env bash

cd ../build/lib

for file in *d.a; do
    mv $file ${file/d.a/.a};
done
