import argparse
import os


def checkIfFolderExists(path):
    if not os.path.exists(path):
        os.makedirs(path)


def getModelByName(name):
    model = ''
    if name == 'LI':
        model = 'LinearImplicit'
    elif name == 'SI':
        model = 'SemiImplicit'
    elif name == 'IE':
        model = 'ImplictEuler'
    else:
        model = 'Splitting'
    return model


def getSplittingMethodByName(name):
    if name[0] == 'S':
        return 'Strang'
    elif name[0] == 'B':
        return 'Stein'
    else:
        return 'Godunov'


def Standard():
    block = ''
    block += '#!/bin/bash\n'
    block += '#SBATCH --partition=cpuonly\n'
    block += '#SBATCH --account=hk-project-ipfem\n'
    block += '#SBATCH --export=ALL,EXECUTABLE=./M++\n'
    block += '#SBATCH --mail-type=ALL\n'
    block += '#SBATCH --mail-user=jonathan.froehlich@kit.edu\n'
    return block


def Jobname(name):  # name could be SIl3j4
    return '#SBATCH --job-name=' + name + '\n'


def Nodes(n):
    return '#SBATCH --nodes=' + str(n) + '\n'


def Tasks(n):
    return '#SBATCH --ntasks-per-node=' + str(n) + '\n'


def Time(h, m):
    if m == 0:
        return '#SBATCH --time=' + str(h) + ':00:00\n'
    elif m < 10:
        return '#SBATCH --time=' + str(h) + ':0' + str(m) + ':00\n'
    else:
        return '#SBATCH --time=' + str(h) + ':' + str(m) + ':00\n'


def Start(args):
    block = ''
    block += 'mpirun ${MPIRUN_OPTIONS} M++ elasticity/benchmarks/activestrain'
    for a in args:
        block += ' ' + a + '=' + str(args[a])
    block += '\n'
    return block


def decideTime(level, degree):
    h,m = 0,0
    minutes = 2 * (level+1)*(level+1)*degree*degree * 30
    h = int(minutes / 60)
    m = minutes % 60
    return (h, m)



def modifyJobskript(level, degree, logfolder, args):
    checkIfFolderExists(logfolder)

    mat = args['ActiveMaterial']
    mesh = args['Mesh']
    filename = 'run_as_' + mesh.lower() + '_' + mat.lower() + '_l' + str(level) + 'p' + str(degree)
    jobname = 'AS-' + mesh + mat +'L' + str(level) + 'P' + str(degree)

    args.update({'logfile': logfolder + jobname,
                 'MechLevel': str(level),
                 'ElphyLevel': str(max(3, level+max(degree-1, 0))),
                 'MechPolynomialDegree': str(degree)})
    (h, m) = decideTime(level, degree)
    with open(filename, 'w') as output_file:
        output_file.write(Standard())
        output_file.write(Nodes(64))
        output_file.write(Tasks(64))
        output_file.write(Time(h, m))
        output_file.write(Jobname(jobname))
        output_file.write('module load compiler/gnu/8 mpi/openmpi/4.1\n')
        output_file.write('export MPIRUN_OPTIONS="--bind-to core --map-by core -report-bindings"\n')
        output_file.write(
            'echo "${executable} running on ${SLURM_NTASKS_PER_NODE} tasks per node with ${SLURM_NNODES} nodes"\n')
        output_file.write(Start(args))
    return filename


materials = {
    0: 'Linear',
    1: 'Bonet',
    2: 'Holzapfel'
}

def activestrain_jobs(mesh, degree, mat, run_jobs=False):
    activemat = materials[mat]
    penalty = 'Ciarlet' if mat > 0 else 'None'

    arglist = {
        'CoupledProblem': mesh,
        'Mesh': mesh,
        'ActiveMaterial': activemat,
        'QuasiCompressiblePenalty': penalty
    }


    for l in range(5-(1 if mat>1 else 0)):
        jobname = modifyJobskript(l, degree, '../data/benchmarks/', arglist)
        if run_jobs:
            print('running job', jobname)
            os.system('sbatch ' + jobname)


if __name__ == "__main__":
    parser = argparse.ArgumentParser('Python interface for batch file creation')

    # Run options
    parser.add_argument('--runjobs', type=int, default=0, help='Should created batch files be run automatically')
    parser.add_argument('--degree', type=int, default=1, help='Polyonomial Degree of simulations')
    parser.add_argument('--material', type=int, default=0, help='Active Material Parameter')
    args = parser.parse_args()

    print(args.runjobs)
    #activestrain_jobs("FullEllipsoid", args.degree,args.material, args.runjobs > 0)
    activestrain_jobs("LeftVentricle", args.degree,args.material, args.runjobs > 0)
