import argparse
import os


def checkIfFolderExists(path):
    if not os.path.exists(path):
        os.makedirs(path)


def getModelByName(name):
    model = ''
    if name == 'LI':
        model = 'LinearImplicit'
    elif name == 'SI':
        model = 'SemiImplicit'
    elif name == 'IE':
        model = 'ImplictEuler'
    else:
        model = 'Splitting'
    return model


def getSplittingMethodByName(name):
    if name[0] == 'S':
        return 'Strang'
    elif name[0] == 'B':
        return 'Stein'
    else:
        return 'Godunov'


def Standard():
    block = ''
    block += '#!/bin/bash\n'
    block += '#SBATCH --partition=cpuonly\n'
    block += '#SBATCH --account=hk-project-ipfem\n'
    block += '#SBATCH --export=ALL,EXECUTABLE=./M++\n'
    block += '#SBATCH --mail-type=ALL\n'
    block += '#SBATCH --mail-user=jonathan.froehlich@kit.edu\n'
    return block


def Jobname(name):  # name could be SIl3j4
    return '#SBATCH --job-name=' + name + '\n'


def Nodes(n):
    return '#SBATCH --nodes=' + str(n) + '\n'


def Tasks(n):
    return '#SBATCH --ntasks-per-node=' + str(n) + '\n'


def Time(h, m):
    if m == 0:
        return '#SBATCH --time=' + str(h) + ':00:00\n'
    elif m < 10:
        return '#SBATCH --time=' + str(h) + ':0' + str(m) + ':00\n'
    else:
        return '#SBATCH --time=' + str(h) + ':' + str(m) + ':00\n'


def Start(args):
    block = ''
    block += 'mpirun ${MPIRUN_OPTIONS} M++ elasticity/benchmarks/ellipsoid'
    for a in args:
        block += ' ' + a + '=' + str(args[a])
    block += '\n'
    return block


def decideTime(degree):
    return (2*degree*degree, 0)


def modifyJobskript(name, degree, logfolder, args):
    checkIfFolderExists(logfolder)

    mat = args['ActiveMaterial']
    if mat=='Holzapfel' and degree > 1:
        args.update({'ReferenceLevel': '3'})
    else:
        args.update({'ReferenceLevel': '4'})
    filename = name + '_' + mat.lower() + '_p' + str(degree)
    jobname = args['MechProblem'] + mat + 'P' + str(degree)

    args.update({'logfile' : logfolder + jobname})
    args.update({'MechPolynomialDegree': str(degree)})
    (h, m) = decideTime(degree)
    with open(filename, 'w') as output_file:
        output_file.write(Standard())
        output_file.write(Nodes(32))
        output_file.write(Tasks(64))
        output_file.write(Time(h, m))
        output_file.write(Jobname(jobname))
        output_file.write('module load compiler/gnu/8 mpi/openmpi/4.1\n')
        output_file.write('export MPIRUN_OPTIONS="--bind-to core --map-by core -report-bindings"\n')
        output_file.write(
            'echo "${executable} running on ${SLURM_NTASKS_PER_NODE} tasks per node with ${SLURM_NNODES} nodes"\n')
        output_file.write(Start(args))
    return filename


def elliposid_jobs(mesh, run_jobs=False):
    arglists = [
        {
            'ActiveMaterial': 'Linear',
            'QuasiCompressiblePenalty': 'None'
        },
        {
            'ActiveMaterial' : 'Bonet',
            'QuasiCompressiblePenalty' : 'Ciarlet'
        },
        {
            'ActiveMaterial': 'Holzapfel',
            'QuasiCompressiblePenalty': 'Ciarlet'
        }
    ]
    for d in range(1,3):
        for arglist in arglists:
            arglist.update({"MechProblem": mesh})
            jobname = modifyJobskript('run_'+mesh.lower(), d, '../data/benchmarks/', arglist)
            if run_jobs:
                print('running job', jobname)
                os.system('sbatch ' + jobname)




meshes = {
    0: "FullEllipsoid",
    1: 'UnorientedEllipsoid',
    2: "LeftVentricle"
}

if __name__ == "__main__":
    parser = argparse.ArgumentParser('Python interface for batch file creation')

    # Run options
    parser.add_argument('--runjobs', type=int, default=0, help='Should created batch files be run automatically')
    parser.add_argument('--mesh', type=int, default=0, help='Should created batch files be run automatically')
    args = parser.parse_args()

    print(args.runjobs)
    elliposid_jobs(meshes[args.mesh], args.runjobs>0)
