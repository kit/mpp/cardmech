import argparse
import os


def checkIfFolderExists(path):
    if not os.path.exists(path):
        os.makedirs(path)


def getModelByName(name):
    model = ''
    if name == 'LI':
        model = 'LinearImplicit'
    elif name == 'SI':
        model = 'SemiImplicit'
    elif name == 'IE':
        model = 'ImplictEuler'
    else:
        model = 'Splitting'
    return model


def getSplittingMethodByName(name):
    if name[0] == 'S':
        return 'Strang'
    elif name[0] == 'B':
        return 'Stein'
    else:
        return 'Godunov'


def Standard():
    block = ''
    block += '#!/bin/bash\n'
    block += '#SBATCH --partition=cpuonly\n'
    block += '#SBATCH --account=hk-project-ipfem\n'
    block += '#SBATCH --export=ALL,EXECUTABLE=./M++\n'
    block += '#SBATCH --mail-type=ALL\n'
    block += '#SBATCH --mail-user=jonathan.froehlich@kit.edu\n'
    return block


def Jobname(name):  # name could be SIl3j4
    return '#SBATCH --job-name=' + name + '\n'


def Nodes(n):
    return '#SBATCH --nodes=' + str(n) + '\n'


def Tasks(n):
    return '#SBATCH --ntasks-per-node=' + str(n) + '\n'


def Time(h, m):
    if m == 0:
        return '#SBATCH --time=' + str(h) + ':00:00\n'
    elif m < 10:
        return '#SBATCH --time=' + str(h) + ':0' + str(m) + ':00\n'
    else:
        return '#SBATCH --time=' + str(h) + ':' + str(m) + ':00\n'


def Start(args):
    block = ''
    block += 'mpirun ${MPIRUN_OPTIONS} M++ elasticity/benchmarks/ellipsoid'
    for a in args:
        block += ' ' + a + '=' + str(args[a])
    block += '\n'
    return block


def decideTime(degree, mat):
    factor = 4
    if mat != 'Linear':
        factor = 8
    return (2*degree*degree * factor, 0)


def modifyJobskript(name, degree, logfolder,penalty, args):
    checkIfFolderExists(logfolder)

    mat = args['ActiveMaterial']
    if mat=='Holzapfel' and degree > 1:
        args.update({'ReferenceLevel': '3'})
    else:
        args.update({'ReferenceLevel': '3'})
        args.update({'DGPenalty': penalty})
    filename = name + '_' + mat.lower() + '_p' + str(degree) + 'DG' + str(penalty)
    jobname = args['MechProblem'] + mat + 'P' + str(degree) + 'DG' + str(penalty)

    args.update({'logfile' : logfolder + jobname})
    args.update({'MechPolynomialDegree': str(degree)})
    (h, m) = decideTime(degree, mat)
    with open(filename, 'w') as output_file:
        output_file.write(Standard())
        output_file.write(Nodes(64))
        output_file.write(Tasks(64))
        output_file.write(Time(h, m))
        output_file.write(Jobname(jobname))
        output_file.write('module load compiler/gnu/8 mpi/openmpi/4.1\n')
        output_file.write('export MPIRUN_OPTIONS="--bind-to core --map-by core -report-bindings"\n')
        output_file.write(
            'echo "${executable} running on ${SLURM_NTASKS_PER_NODE} tasks per node with ${SLURM_NNODES} nodes"\n')
        output_file.write(Start(args))
    return filename


def elliposid_jobs(mesh, run_jobs=False):
    arglists2 = [
        {
            'ActiveMaterial' : 'Holzapfel',
            'QuasiCompressiblePenalty' : 'Ciarlet',
            'DGSign': '-1',
            'DGPenalty': '700',
            'Overlap': 'dG1',
            'Overlap_Distribution': '1',
            'InterpolateStartVector': 'false',
            'NewtonDamping': '0.75'
        }
    ]

    penalty2 = {'700'}
    for d in range(1,3):
        for arglist2 in arglists2:
            arglist2.update({"MechProblem": mesh})
            for p2 in penalty2:
                jobname = modifyJobskript('run_'+mesh.lower() + 'DG', d, '../data/benchmarks/', p2, arglist2)
                if run_jobs:
                    print('running job', jobname)
                    os.system('sbatch ' + jobname)




meshes = {
    0: "FullEllipsoid"
}

if __name__ == "__main__":
    parser = argparse.ArgumentParser('Python interface for batch file creation')

    # Run options
    parser.add_argument('--runjobs', type=int, default=0, help='Should created batch files be run automatically')
    parser.add_argument('--mesh', type=int, default=0, help='Should created batch files be run automatically')
    args = parser.parse_args()

    print(args.runjobs)
    elliposid_jobs(meshes[args.mesh], args.runjobs>0)
