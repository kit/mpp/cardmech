import argparse
import os


def checkIfFolderExists(path):
    if not os.path.exists(path):
        os.makedirs(path)



def Standard():
    block = ''
    block += '#!/bin/bash\n'
    block += '#SBATCH --partition=cpuonly\n'
    block += '#SBATCH --account=hk-project-ipfem\n'
    block += '#SBATCH --export=ALL,EXECUTABLE=./CardMech-M++\n'
    block += '#SBATCH --mail-type=ALL\n'
    block += '#SBATCH --mail-user=jonathan.froehlich@kit.edu\n'
    return block


def Jobname(name):  # name could be SIl3j4
    return '#SBATCH --job-name=' + name + '\n'


def Nodes(n):
    return '#SBATCH --nodes=' + str(n) + '\n'


def Tasks(n):
    return '#SBATCH --ntasks-per-node=' + str(n) + '\n'


times = [("2:00:00", 64), ("4:00:00", 32), ("8:00:00", 16), ("16:00:00", 8), ("24:00:00", 4), ("32:00:00", 2), ("48:00:00", 1)]

def Time(time):
    return '#SBATCH --time=' +time +'\n'

def Start(loglocation):
    block = 'mpirun ${MPIRUN_OPTIONS} CardMech-M++ elasticity_benchmarks/activestrain'
    block += ' logfile='+loglocation
    block += '\n'
    return block


def modifyJobskript(time, nodes, logfolder):
    checkIfFolderExists(logfolder)

    filename = 'scalability_as_' + str(nodes)
    jobname = 'SCAL-' + str(nodes)

    with open(filename, 'w') as output_file:
        output_file.write(Standard())
        output_file.write(Nodes(nodes))
        output_file.write(Tasks(64))
        output_file.write(Time(time))
        output_file.write(Jobname(jobname))
        output_file.write('module load compiler/gnu/8 mpi/openmpi/4.1\n')
        output_file.write('export MPIRUN_OPTIONS="--bind-to core --map-by core -report-bindings"\n')
        output_file.write(
            'echo "${executable} running on ${SLURM_NTASKS_PER_NODE} tasks per node with ${SLURM_NNODES} nodes"\n')
        output_file.write(Start(logfolder+filename))
    return filename


def activestrain_jobs(run_jobs=False):

    for time, nodes in times:
        jobname = modifyJobskript(time, nodes, '../data/scalability/')
        if run_jobs:
            print('running job', jobname)
            os.system('sbatch ' + jobname)


if __name__ == "__main__":
    parser = argparse.ArgumentParser('Python interface for batch file creation')

    # Run options
    parser.add_argument('--runjobs', type=int, default=0, help='Should created batch files be run automatically')
    args = parser.parse_args()

    print(args.runjobs)
    activestrain_jobs(args.runjobs > 0)
