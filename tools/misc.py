from math import sin, cos

c = [-4333.618335582119, 2570.395355352195, 1329.53611689133, 104.943770305116]
d = [-2051.827278991976, 302.216784558222, 218.375174229422]

def rfl(gamma):
    l = 1.95*gamma
    flr = 0.0
    if l >= 1.7  and l <= 2.6:
        flr = c[0] / 2.0
        for i in range(1,4):
            flr += c[i] * sin(i * l)
            flr += d[i - 1] * cos(i * l)

    return flr

if __name__=="__main__":
    for i in range(60):
        g = 0.6 + i/100
        rfl_str = "RFL(" + str(g-1) + ") = " + str(rfl(g))
        print(rfl_str)