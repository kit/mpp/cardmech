#!/usr/bin/env bash

# reading variables
NODE="$($#>0? $1 : 00)"
CORES="$($#>1? $2 : 16)"
BRANCH="$(git rev-parse --abbrev-ref HEAD)"

# Committing everything in git
git add .
git commit -m "Preparing for cluster"
git push

# Login on specified cluster node
pdecluster $NODE

# Pull everything on ma-pde
cd M++/cardmech/ || (~. && exit)
git fetch
git checkout $BRANCH
git pull --recurse-submodules
cd build ||  (~. && exit)
make -j

# Run files and exit
mpirun -n $CORES M++
~.
