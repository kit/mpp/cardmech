#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
sys.path.append('../../')
import os
import shutil
import mpp.python.mppy as mppy
import re

level = [0,1]
T_a = [10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60]

def checkFileExistance(filePath):
    try:
        with open(filePath, 'r') as f:
            return True
    except FileNotFoundError as e:
        return False
    except IOError as e:
        return False

def moveDisp(l, t, expPath):
    if checkFileExistance('../../build/data/vtu/Boundary.vtu') == True:
            shutil.move('../../build/data/vtu/Boundary.vtu', pathToMakeFolders + expPath + 'l' + str(l) + '_Ta_' + str(t) + '.vtu')
    for i in range(0,5):
        if checkFileExistance('../../build/data/vtu/U-000'+str(i)+'.vtu') == True:
            shutil.move('../../build/data/vtu/U-000'+str(i)+'.vtu', pathToMakeFolders + expPath + '_l' + str(l) + '_Ta_' + str(t) + '.vtu')

path='../../../data/MechanicsBenchmark/TestActiveStress/'
pathToMakeFolders=path #'../'+path

def startExperiment(procs,startconf,pE,added=None):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='M++',kernels=procs,mute=False)
    
    for l in level:
        for t in T_a:
            file = path + pE + 'AS_T4_' + str(l) + 'Ta_' + str(t)
            kwargs = {"MechLevel": l, "activeStress": t, "logfile": file}
            if added!=None:
                kwargs.update(added)
            mpp.run(procs, config=startconf, kwargs=kwargs)
            moveDisp(l, t, pE)


def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=8
    
    
    startconf="elasticity/benchmarks/LandBenchmarkBiventricle"
    pathExperiment=""
    startExperiment(procs,startconf,pathExperiment)
    
    