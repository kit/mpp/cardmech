import sys
sys.path.append('../../')
import os
import shutil
import mpp.python.mppy as mppy

lList = [0,1,2,3]
lList = [5]
tList = [0]
#start_dt=0.0001
start_dt=0.0004

path='../../../data/KovachevaBiventricle/'
pathToMakeFolders=path #'../'+path

def moveActivationtime(l,j, expPath):
    shutil.move('../../build/data/vtu/ActivationTime.vtu', pathToMakeFolders+expPath+'AT_l'+str(l)+'j'+str(j)+'.vtu')
    
        

def startExperiment(procs,startconf,pE,added=None):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    
    for l in lList:
        for j in tList:
            file = path+pE+'log_l'+str(l)+'j'+str(j)+'m1'
            kwargs={"ElphyLevel": l,"DeltaTime":start_dt*2**(-j),"logfile":file,"EndTime":0.3,"ElphyVTK":0}
            if added!=None:
                kwargs.update(added)
            mpp.run(procs, config=startconf, kwargs=kwargs) #replace 64 by hosts if necessary
            moveActivationtime(l,j,pE)

def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=64
    

    #startconf="BiVentricleTest/start"
    #pathExperiment="SI0004/"
    
    #startconf="electrophysiology/BiVentricleTest/startGS"
    #pathExperiment="GS0004/"
    
    startconf="electrophysiology/BiVentricleTest/biventricelTest"
    pathExperiment="BR/OldDiff/SI0004/"
    #startExperiment(procs,startconf,pathExperiment,{"ElphyModelName": "BeelerReuter"})
    pathExperiment="TT/SE/SI0004/"
    startExperiment(procs,startconf,pathExperiment,{"ElphyModelName": "TenTusscher",'ElphyProblem':'KovachevaBiventricleSimpleExcitationProblem','EndTime':0.65})
    
    
    
