import numpy as np
import matplotlib.pyplot as plt
import math
import re

from scipy.integrate import solve_ivp

################################################################################################
#paths:
saveTiKZPath = '../../../datafiles/tikz/'
saveTexPath = '../../../datafiles/tex/'

################################################################################################

colorList = ['black','yellow','green','orange','purple','brown','blue','red']


#Model data:

Iext=30
duration=3.0
smoothing='tanh'  #'tanh'  #'arctan' #'stepfunction'
alphaC =[[0.095,  -0.01,  -5., 0.,  0.,  -0.072, 1.],[0.012,  -0.008, 28., 0.,  0.,  0.15,   1.],[0.126,  -0.25,  77, 0,  0,  0,      0],[0.055,  -0.25,  78, 0,  0,  -0.2,   1],[0,      0,      47, -1, 47, -0.1,   -1],[0.0005, 0.083,  50, 0,  0,  0.057,  1] ]

betaC=[[0.07, -0.017, 44, 0, 0, 0.05, 1],[0.0065, -0.02, 30, 0, 0, -0.2, 1],[1.7, 0, 22.5, 0, 0, -0.082, 1],[0.3, 0, 32, 0, 0, -0.1, 1],[40, -0.056, 72, 0, 0, 0, 0],[0.0013, -0.06, 20, 0, 0, -0.04, 1]]


y0= [-84.57,0.0000002, 0.002980,1.0,0.9877,0.975,0.011,0.00565]
y_names=['V','Ca','d','f','h','j','m','x1']
w_names=['d','f','h','j','m','x1']
##################################################################################################
#numerical imformation:
timeinterval =[0.0,10.0]
factor=2
startdt = 0.01
#################################################################################################

def Alpha(i,V):
    alpha =(alphaC[i][0]*np.exp(alphaC[i][1]*(V+alphaC[i][2])) +alphaC[i][3]*(V+alphaC[i][4]))/(np.exp(alphaC[i][5]*(V+alphaC[i][2]))+alphaC[i][6])
    return alpha
def Beta(i,V):
    beta =(betaC[i][0]*np.exp(betaC[i][1]*(V+betaC[i][2])) +betaC[i][3]*(V+betaC[i][4]))/(np.exp(betaC[i][5]*(V+betaC[i][2]))+betaC[i][6])
    return beta
def dAlpha( i, V):
    a =alphaC[i][0] * np.exp(alphaC[i][1] * (V + alphaC[i][2])) + alphaC[i][3] * (V + alphaC[i][4])
    da = alphaC[i][1] * alphaC[i][0] * np.exp(alphaC[i][1] * (V + alphaC[i][2])) + alphaC[i][3]
    b = np.exp(alphaC[i][5] * (V + alphaC[i][2])) + alphaC[i][6]
    db = alphaC[i][5] * np.exp(alphaC[i][5] * (V + alphaC[i][2]))
    return (b * da - a * db) / (b * b)

def dBeta(i, V):
    a = betaC[i][0] * np.exp(betaC[i][1] * (V + betaC[i][2])) + betaC[i][3] * (V + betaC[i][4])
    da = betaC[i][1] * betaC[i][0] * np.exp(betaC[i][1] * (V + betaC[i][2])) + betaC[i][3]
    b = np.exp(betaC[i][5] * (V + betaC[i][2])) + betaC[i][6]
    db = betaC[i][5] * np.exp(betaC[i][5] * (V + betaC[i][2]))
    return (b * da - a * db) / (b * b)

def gatingInfty(i,V):
    return (Alpha(i,V))/(Alpha(i,V)+Beta(i,V))

def tauGating(i,V):
    return 1.0/(Alpha(i,V)+Beta(i,V))

def readL2DiffsFromFile(path):
    f=open(path)
    List=[[],[],[]]
    for l in f:
        l = l.strip()
        #print(l)
        m = re.search("L2Diff EE \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m: 
            List[0].append(float(m.group(1)))
        m = re.search("L2Diff RL \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m: 
            List[1].append(float(m.group(1)))
        m = re.search("L2Diff RK4 \s*([+\-eE\d]+\.{0,1}[+\-eE\d]*)", l)
        if m: 
            List[2].append(float(m.group(1)))
    f.close() 
    return(List)
def resizeData(f,number,length):
    #print(number)
    g=[]
    for i in range(int(length)):
        if i%number==0:
            g.append(f[i])
    return(g)   
def plotDerivativesVDepending():
    V = np.linspace(-90, 40, 1000)
    for i in range(2,8):
        Derivative=[]
        for j in range(len(V)):
            vcw=[V[j],0,0,0,0,0,0,0]
            Derivative.append(dwGw(vcw,i,i))
        plt.figure(figsize=(8,6), dpi=80)
        plt.plot(V,Derivative,linewidth=3.5)
        plt.xlabel('potential (mV)')
        plt.ylabel('')
        filename = 'Dw'+str(i-1)+'Gw'+str(i-1)+'VDepending'
        import tikzplotlib
        tikzplotlib.save(saveTiKZPath + filename + ".tex")
        #plt.show()
        plt.close()
def plotGatingInfty(i):
    V = np.linspace(-90, 40, 1000)
    gate_infty =[]
    tau_gate =[]
    for j in range(len(V)):
        gate_infty.append(gatingInfty(i,V[j]))
        tau_gate.append(tauGating(i,V[j]))
        
    plt.plot(V,gate_infty)
    plt.show()
    plt.plot(V,tau_gate)
    plt.show()
    
def plotAlphaBetaOfI(i):
    V = np.linspace(-90, 40, 1000)
    alpha=[]
    beta=[]
    for j in range(len(V)):
        alpha.append(Alpha(i,V[j]))
        beta.append(Beta(i,V[j]))
    alphalabel= '$\\alpha_'+w_names[i]+'$' 
    betalabel= '$\\beta_'+w_names[i]+'$'
    plt.plot(V,alpha,label=alphalabel,linewidth=3.5)
    plt.plot(V,beta,label=betalabel,linewidth=3.5)
    plt.xlabel('potential (mV)')
    plt.ylabel('')
    plt.legend()
    import tikzplotlib
    tikzplotlib.save(saveTiKZPath + 'AlphaBeta'+w_names[i] + ".tex")
    plt.close()
    #plt.show()
    
def plotAlphaBeta():
    for i in range(len(w_names)):
        plotAlphaBetaOfI(i)
        
def plotRHSOfi(index,time,f):
    plt.plot(time,f,linewidth=3.5)
    plt.xlabel('time (s)')
    plt.ylabel('')
    plt.show()
    #import tikzplotlib
    #tikzplotlib.save(saveTiKZPath + 'BRG'+y_names[index] + '.tex')
    plt.close()
def plotRHS(dt,time,rhs):
    N = (timeinterval[-1]/dt)-1
    
    time=resizeData(time,100,N)
    for j in range(len(time)):
        time[j]=0.001*time[j]
    for i in range(len(rhs)):
        rhs[i] = resizeData(rhs[i],100,N)
        plotRHSOfi(i,time,rhs[i])
        
def plotPotential(t,V):
    plt.plot(t,V)
    plt.show()

def plotVW(t,vw):
    for i in range(len(vw)):
        plotPotential(t,vw[i])


def Tanh(t,s=4.0):
    return ((1+np.tanh(s*(t-0.0)))/2) - ((1+np.tanh(s*(t-duration)))/2)*Iext +Iext -1
def Arctan(t,s=4):
    start_t = s * (t - (0.0)) #0.4
    end_t = s * (t - (duration)) #0.6
    return (np.arctan(start_t) - np.arctan(end_t)) / math.pi * Iext;
def plotContinuousIEXT():
    #print(ContinousIext(100,4),sigmoid(100))
    t = np.linspace(0.0, 30,3000)
    #t = np.linspace(0.0, 4,1000)
    plt.plot(t,Arctan(t,4),label='$\\arctan$ $s=4$',linewidth=3.5)
    plt.plot(t,Arctan(t,10),label='$\\arctan$ $s=10$',linewidth=3.5)
    plt.plot(t,Arctan(t,50),label='$\\arctan$ $s=50$',linewidth=3.5)
    
    plt.plot(t,Tanh(t,2),label='$\\tanh$ $s=2$',linewidth=3.5)
    plt.plot(t,Tanh(t,4),label='$\\tanh$ $s=4$',linewidth=3.5)
    plt.plot(t,Tanh(t,10),label='$\\tanh$ $s=10$',linewidth=3.5)
    
    plt.legend()
    #plt.show()
    filename = 'SmoothedExternalCurrentDifferentSA30d003'
    #plt.show()
    import tikzplotlib
    tikzplotlib.save(saveTiKZPath + filename + ".tex")
    plt.close()
    
def L2(V,dt):
    sum = -0.5 * dt* (V[0]*V[0]);
    sum += -0.5 * dt * (V[-1]*V[-1]);
    for i in range(0,len(V)):
        sum += dt*(V[i]*V[i])
   
    return(math.sqrt(sum))#"%8.4f"%

def L2Diff(v1, v2,dt):
    if len(v1)== 0 or len(v2)== 0:
        return infvalue
    else:
        n1=len(v1)
        n2=len(v2)
        if n1%10!=0:
            n1=n1-(n1%10)
        if n2%10!=0:
            n2=n2-(n2%10)
        print(n1,n2,dt)
        Diff =[]
        if n1==n2:
            for i in range(n1):
                Diff.append(v1[i]-v2[i])
        elif n1<n2:
            k_ref=int(n2/n1)
            for i in range(n1):
                Diff.append(v1[i]-v2[k_ref*i])
        else:
            k_ref=int(n1/n2)
            for i in range(n2):
                Diff.append(v1[k_ref*i]-v2[i])
        
        norm =L2(Diff,dt)
        return(norm)#"%8.4f"%
def RHS(t,y):
    mVolt = y[0]
    Ca =y[1]
    d = y[2]
    f =y[3]
    h = y[4]
    j =y[5]
    m = y[6]
    x1 =y[7]

    I_Na = (4.0*m*m*m*h*j+0.003)*(mVolt-50.0)
    I_s = 0.09*d*f*(mVolt-(-82.3 -13.0287*np.log(Ca)))
    I_x1 = x1* ((0.8*(np.exp(0.04*(mVolt +77))-1))/(np.exp(0.04*(mVolt +35))))
    I_K = 0.35*(((4*(np.exp(0.04*(mVolt +85))-1))/(np.exp(0.08*(mVolt+53))+np.exp(0.04*(mVolt+53))))+((0.2*(mVolt+23))/(1-np.exp(-0.04*(mVolt+23)))))
    g=[]
    g.append(-1.0*(I_Na + I_s +I_x1 + I_K - getExternalCurrent(t)))
    g.append(-0.0000001*I_s +0.07*(0.0000001 -Ca))
    for i in range(2,len(y)): 
        alphaV=Alpha(i-2,mVolt)
        betaV = Beta(i-2,mVolt)
        g.append((alphaV + betaV)*((alphaV/(alphaV+betaV)) - y[i]))
  
    return g
def Iion(y):
    mVolt = y[0]
    Ca =y[1]
    d = y[2]
    f =y[3]
    h = y[4]
    j =y[5]
    m = y[6]
    x1 =y[7]

    I_Na = (4.0*m*m*m*h*j+0.003)*(mVolt-50.0)
    I_s = 0.09*d*f*(mVolt-(-82.3 -13.0287*np.log(Ca)))
    I_x1 = x1* ((0.8*(np.exp(0.04*(mVolt +77))-1))/(np.exp(0.04*(mVolt +35))))
    I_K = 0.35*(((4*(np.exp(0.04*(mVolt +85))-1))/(np.exp(0.08*(mVolt+53))+np.exp(0.04*(mVolt+53))))+((0.2*(mVolt+23))/(1-np.exp(-0.04*(mVolt+23)))))
    return -1.0*(I_Na + I_s +I_x1 + I_K)
def dVIion(VW):
    V = VW[0]
    dI_NaToV = 4.0 * VW[6] * VW[6] * VW[6] * VW[4] * VW[5] + 0.003
    dI_sToV = 0.09 * VW[2] * VW[3]
    a = 1.4 * np.exp(0.04 * (V + 85)) - 1.4
    b = np.exp(0.08 * (V + 53)) + np.exp(0.04 * (V + 53))
    dA = ((0.04 * 1.4 * np.exp(0.04 * (V + 85))) * b -
                 a * (0.08 * np.exp(0.08 * (V + 53)) + 0.04 * np.exp(0.04 * (V + 53)))) / (b * b)
    a=0.35*(0.2*(V+23))
    b=(1-np.exp(-0.04*(V+23)))
    dB =(0.07 * b - a * 0.04 * np.exp(-0.04 * (V + 23))) /(b*b)
    dI_KToV = dA + dB
    dI_x1ToV = 0.0078911 * VW[7] * np.exp(-0.04 * V)#//oder *-1?
    dIonOfV = dI_NaToV + dI_sToV + dI_KToV + dI_x1ToV
    return dIonOfV

def dwGw(vcw,i,j):
    dwGwofj=0.0
    if j ==0:
        dwGwofj = dAlpha(i - 2, vcw[0]) - vcw[i] * (dAlpha(i - 2, vcw[0]) + dBeta(i - 2, vcw[0]))
    elif j==1:
        dwGwofj=0.0
    else:
        if i==j:
            dwGwofj = -Alpha(i-2,vcw[0]) -Beta(i-2,vcw[0])
        else:
            dwGwofj=0.0
            
    return dwGwofj
def dcGc(vcw,j):
    dGcOfj = 0;
    V = vcw[0];
    if j == 0: 
        dGcOfj = -0.0000001 * 0.09 * vcw[2] * vcw[3]
    elif j == 1:
        dGcOfj = -0.07 - 0.0000001 * 0.09 * vcw[2] * vcw[3] * 13.0287 * (1 / vcw[1])
    elif j == 2:
        dGcOfj = -0.0000001 * 0.09 * vcw[3] * (V + 82.3 + 13.0287 * np.log(vcw[1]))
    elif j == 3:
        dGcOfj = -0.0000001 * 0.09 * vcw[2] * (V + 82.3 + 13.0287 * np.log(vcw[1]))
    else:
        dGcOfj = 0.0

    return dGcOfj

def computeNumericalDVIion(y):
    h=0.00000001
    yh=[y[0]+h]
    for i in range (1,len(y)):
        yh.append(y[i])
    
    F=Iion(y)
    Fh=Iion(yh)
    approx =(Fh-F)/h
    return approx
    
def getExternalCurrent(t):
    if smoothing=="arctan":
         return Arctan(t) 
    elif smoothing=="tanh":
        return Tanh(t)
    else :
        if 0.0<t<2.0:
            return Iext
        else:
            return 0.0
    
def SolveBRStandard():
    sol=solve_ivp(RHS,timeinterval,y0)

    min_dt =10000.0
    for i in range(len(sol.t)-1):
        diff = sol.t[i+1]-sol.t[i]
        if diff<min_dt:
            min_dt=diff
    
    print('min_dt = ',min_dt)
    print('number of steps: ',len(sol.t) )
    for i in range(len(sol.y)):
        plt.plot(sol.t,sol.y[i])
        plt.show()
def SolveRLMod(dt, withRHS=False):
    t=timeinterval[0]
    T =timeinterval[-1]
    time=[t]
    vw=[y0[0],y0[1],y0[2],y0[3],y0[4],y0[5],y0[6],y0[7]]
    VW=[[y0[0]],[y0[1]],[y0[2]],[y0[3]],[y0[4]],[y0[5]],[y0[6]],[y0[7]]]
    rhs=[[],[],[],[],[],[],[],[]]
    g=[]
    t+=dt
    while t<T:
        time.append(t)
        if withRHS:
            rhs[0].append(Iion(vw))
        for i in range(2,len(y0)):
            w_infty = Alpha(i-2,vw[0])/(Alpha(i-2,vw[0])+Beta(i-2,vw[0]))
            tau_wInverse = Alpha(i-2,vw[0])+Beta(i-2,vw[0])
            vw[i] = w_infty + (vw[i] -w_infty)*np.exp(-1*dt*tau_wInverse)
            VW[i].append(vw[i])
        
        g=RHS(t,vw) 
        vw[0]+=dt*g[0]

        g=RHS(t,vw)
        vw[1]+=dt*g[1]
        
        VW[0].append(vw[0])
        VW[1].append(vw[1])
        
        t+=dt
        if withRHS:
            for i in range(1,len(y0)):
                rhs[i].append(g[i])
    if withRHS:
        return(time,VW,rhs)
    else:
        return (time,VW)
def updateGating(dt,vcw):
    for i in range(2,len(y0)):
        w_infty = Alpha(i-2,vcw[0])/(Alpha(i-2,vcw[0])+Beta(i-2,vcw[0]))
        tau_wInverse = Alpha(i-2,vcw[0])+Beta(i-2,vcw[0])
        vcw[i] = w_infty + (vcw[i] -w_infty)*np.exp(-1*dt*tau_wInverse)
    return vcw

def updateConcentration(t,dt,vcw):
    g=RHS(t,vcw)
    vcw[1]+=dt*g[1]
    return vcw

def updatePotential(t,dt,vcw):
    g=RHS(t,vcw)
    vcw[0]+=dt*g[0]
    return vcw

def SolveRLModOrderSpecified(dt,order):
    t=timeinterval[0]
    T =timeinterval[-1]
    time=[t]
    vw=[y0[0],y0[1],y0[2],y0[3],y0[4],y0[5],y0[6],y0[7]]
    VW=[[y0[0]],[y0[1]],[y0[2]],[y0[3]],[y0[4]],[y0[5]],[y0[6]],[y0[7]]]
    rhs=[[],[],[],[],[],[],[],[]]
    DVIion=[]
    Approx=[]
    Diff =[]
    t+=dt
    while t<T:
        time.append(t)
        if order =='wvc':
            vw=updateGating(dt,vw)
            vw=updatePotential(t,dt,vw)
            vw=updateConcentration(t,dt,vw)
        elif order =='wcv':
            vw=updateGating(dt,vw)
            vw=updateConcentration(t,dt,vw)
            vw=updatePotential(t,dt,vw)
        elif order =='vcw':
            vw=updatePotential(t,dt,vw)
            vw=updateConcentration(t,dt,vw)
            vw=updateGating(dt,vw)
            
        elif order =='vwc':
            vw=updatePotential(t,dt,vw)
            vw=updateGating(dt,vw) 
            vw=updateConcentration(t,dt,vw)
        elif order =='cwv':
            vw=updateConcentration(t,dt,vw)
            vw=updateGating(dt,vw) 
            vw=updatePotential(t,dt,vw)
        else :
            vw=updateConcentration(t,dt,vw)
            vw=updatePotential(t,dt,vw)
            vw=updateGating(dt,vw)
            
            
        for i in range(len(y0)):
                VW[i].append(vw[i])    
        t+=dt
        
    return (time,VW)
def SolveBRRushLarson(dt):
    t=timeinterval[0]
    T =timeinterval[-1]
    time=[t]
    vw=[y0[0],y0[1],y0[2],y0[3],y0[4],y0[5],y0[6],y0[7]]
    VW=[[y0[0]],[y0[1]],[y0[2]],[y0[3]],[y0[4]],[y0[5]],[y0[6]],[y0[7]]]
    rhs=[[],[],[],[],[],[],[],[]]
    DVIion=[]
    Approx=[]
    Diff =[]
    g=[]
    t+=dt
    while t<T:
        time.append(t)
        
        g=RHS(t,vw)
        for i in range(2,len(y0)):
            w_infty = Alpha(i-2,vw[0])/(Alpha(i-2,vw[0])+Beta(i-2,vw[0]))
            tau_wInverse = Alpha(i-2,vw[0])+Beta(i-2,vw[0])
            vw[i] = w_infty + (vw[i] -w_infty)*np.exp(-1*dt*tau_wInverse)
            rhs[i].append(g[i])
            VW[i].append(vw[i])

        vw[0]+=dt*g[0]
        vw[1]+=dt*g[1]
        rhs[0].append(g[0])
        rhs[1].append(g[1])
        VW[0].append(vw[0])
        VW[1].append(vw[1])
        
        t+=dt
        
        ApproxdVIion=computeNumericalDVIion(vw)
        Approx.append(ApproxdVIion)
        DVIion.append(dVIion(vw))
    for i in range(len(Approx)):
        Diff.append(Approx[i]-DVIion[i])
    All=[[],[],[]]
    All[0]=DVIion
    All[1]=Approx
    All[2]=Diff
    return (time,VW,rhs)#,All)


def SolveBREE(dt):
    t=timeinterval[0]
    T =timeinterval[-1]
    time=[t]
    vw=[y0[0],y0[1],y0[2],y0[3],y0[4],y0[5],y0[6],y0[7]]
    vwList=[[y0[0]],[y0[1]],[y0[2]],[y0[3]],[y0[4]],[y0[5]],[y0[6]],[y0[7]]]
    rhs=[[],[],[],[],[],[],[],[]]
    t+=dt
    counter=1
    while t<T:
        counter+=1
        time.append(t)
        g=RHS(t,vw)
        for i in range(len(y0)):
            vw[i] += dt*g[i]
            #rhs[i].append(g[i])
            vwList[i].append(vw[i])
        t+=dt
        rhs[0].append(Iion(vw))
    #print('EE ', counter,t)
    return (time,vwList,rhs)
        
def SolveBRRK4(dt):
    t=timeinterval[0]
    T =timeinterval[-1]
    time=[t]
    vw=[y0[0],y0[1],y0[2],y0[3],y0[4],y0[5],y0[6],y0[7]]
    VW=[[y0[0]],[y0[1]],[y0[2]],[y0[3]],[y0[4]],[y0[5]],[y0[6]],[y0[7]]]
    rhs=[[],[],[],[],[],[],[],[]]
    t+=dt
    while t<T:
        time.append(t)
        t_old=t-dt
        u=[vw[0],vw[1],vw[2],vw[3],vw[4],vw[5],vw[6],vw[7]]
        k1=RHS(t_old,u)
        for i in range(len(vw)):
            u[i] =vw[i]+ 0.5*dt*k1[i]
        k2=RHS(t_old+(dt/2),u)
        for i in range(len(vw)):
            u[i] =vw[i] +0.5*dt*k2[i]
        k3=RHS(t_old+(dt/2),u)
        for i in range(len(vw)):
            u[i] =vw[i] +  dt * k3[i] 
        k4=RHS(t,u)
        
        for i in range(len(vw)):
            vw[i]+= (dt / 6.0) * (k1[i] + 2.0* k2[i]+ 2.0* k3[i]+ k4[i])
            VW[i].append(vw[i])
        
        t+=dt
        
    return(time,VW)
def plotDerivatives(time,Vw):
    m=len(Vw)
    for i in range(2,m):
        dwGwt=[] 
        for j in range(len(time)):
            vcw=[]
            for k in range(m):
                vcw.append(Vw[k][j])
            dwGwt.append(dwGw(vcw,i,i))
        resizetime =resizeData(time,100,len(time))
        resizeDG = resizeData(dwGwt,100,len(time))
        plt.figure(figsize=(8,6), dpi=80)
        plt.plot(resizetime,resizeDG,linewidth=3.5)
        #plt.legend()
        plt.xlabel('time (ms)')
        plt.ylabel('')
        filename = 'Dw'+str(i-1)+'Gw'+str(i-1)
        import tikzplotlib
        tikzplotlib.save(saveTiKZPath + filename + ".tex")
        #plt.show()
        plt.close()
    dCaGCa=[]
    dVDIon=[]
    for j in range(len(time)):
            vcw=[]
            for k in range(m):
                vcw.append(Vw[k][j])
            dCaGCa.append(dcGc(vcw,1))
            dVDIon.append(dVIion(vcw))
    resizetime =resizeData(time,100,len(time))
    resizeDGCa = resizeData(dCaGCa,100,len(time))
    plt.figure(figsize=(8,6), dpi=80)
    plt.plot(resizetime,resizeDGCa,linewidth=3.5)
    #plt.legend()
    plt.xlabel('time (ms)')
    plt.ylabel('')
    filename = 'DCa'+'GCa'
    import tikzplotlib
    tikzplotlib.save(saveTiKZPath + filename + ".tex")
    #plt.show()
    plt.close()
    resizeDVIon = resizeData(dVDIon,100,len(time))
    #plt.figure(figsize=(8,6), dpi=80)
    plt.plot(resizetime,resizeDVIon,linewidth=3.5)
    #plt.legend()
    plt.xlabel('time (ms)')
    plt.ylabel('')
    filename = 'DV'+'Iion'
    import tikzplotlib
    tikzplotlib.save(saveTiKZPath + filename + ".tex")
    #plt.show()
    plt.close()
    
def getTimeIndex(tList,tEval,dt):
    return int(tEval/dt)

def TestStiffness(tList,VCW,dt):
    evaluationTimes=[0.0,1.0,2.0,50.0,200.0,280.0,300.0]
    JGw =[]
    m =len(VCW)
    for time in evaluationTimes:
        print(time)
        vcw=[]
        dwGwt=[]
        index=getTimeIndex(tList,time,dt)
        for i in range(m):
            vcw.append(VCW[i][index])
        for i in range(2,m):
            dwGwt.append(dwGw(vcw,i,i))
        plt.figure(figsize=(8,6), dpi=80)
        x=np.linspace(0.0,0.0,m-2)
        #print(x,dwGwt)
        plt.plot(dwGwt,x,color='blue', linestyle='', marker='o',markersize=5.5)
        filename = 'GwEigenvlauest'+ str(int(time))+'ms'
        import tikzplotlib
        tikzplotlib.save(saveTiKZPath + filename + ".tex")
        #plt.show()
        plt.close()
        
def writeBeginTable(n,cap,nameList,withR):
    block = ''
    block+= '\\begin{table}[H] \n'
    block+= '\\caption{'+cap+'.}' + ' \n' 
    block+='\\centering\n'
    block +='\\begin{tabular}{l'
    for i in range(n):
        if withR:
            block+='| r r '
        else:
            block+='| r '
    block+='} \n'
    for i in nameList:
        block+='&'+i
        if withR:
            block+='&'
    block+= '\\\\\n'
    block += '$j$'
    for i in range(n):
        block += '&$ g_{j}$'
        if withR:
            block += '&  '
    block+= '\\\\\n'
    block+='\\midrule\n'
    return block

def writeEndTable():
    block = ''
    block +='\\end{tabular}\n'
    block +='\\end{table}\n'
    return block
def writeData(data,withR):
    block = ''
    for i in range(len(data[0])-1):
        block+=str(i)
        for j in range(len(data)):
            block+='& ' + str("%8.8f"%(data[j][i]))
            if withR:
                block+='& '
        block+=' \\\\\n'
        if withR:
            for j in range(len(data)):
                block+=" & & \\textbf{"+ str("%8.2f"%(math.log(data[j][i]/data[j][i+1],2)))+ "}" 
            block+="\\\\\n"
    block+=str(len(data[0])-1)
    for j in range(len(data)):
        block+='& '+str("%8.8f"%(data[j][-1]))
        if withR:
            block+='& '
    block+='\\\\\n'
    return block

def writeInTable(data,filename,cap,nameList,withRates=True):
    with open(saveTexPath +filename+str(int(timeinterval[-1]))+'.tex', 'w') as output_file:
            output_file.write(writeBeginTable(len(data),cap,nameList,withRates))
            output_file.write(writeData(data,withRates))
            output_file.write(writeEndTable())
            
def referenceSolution(sdt,j=8):
    (t,V)=SolveBRRK4(sdt/(factor**j))
    return(t,V)
def convergenceTest():
    numberOfTimesteps=5
    l2diff_List=[]
    l2diffList_EE=[]
    
    print("ExplicitEuler: ")
    for i in range(numberOfTimesteps):
        (time1,VW1,RHS1)=SolveBREE(startdt/(factor**i))
        (time2,VW2,RHS2)=SolveBREE(startdt/(factor**(i+1)))
        timelabel ="{\\vartriangle}t="+str(startdt/(factor**i))
        #plt.plot(time1,VW1[0],label=timelabel)
        l2diff=L2Diff(VW1[0],VW2[0],startdt/(factor**(i)))
        l2diffList_EE.append(l2diff)
        print(l2diff)
        
    l2diff_List.append(l2diffList_EE)
    l2diffList_RL=[]
    print("RushLarson: ")    
    for i in range(numberOfTimesteps):
        (time1,VW1,RHS1)=SolveBRRushLarson(startdt/(factor**i))
        (time2,VW2,RHS2)=SolveBRRushLarson(startdt/(factor**(i+1)))
        timelabel ="{\\vartriangle}t="+str(startdt/(factor**i))
        #plt.plot(time1,VW1[0],label=timelabel)
        l2diff=L2Diff(VW1[0],VW2[0],startdt/(factor**(i)))
        l2diffList_RL.append(l2diff)
        print(l2diff)
    l2diff_List.append(l2diffList_RL)
    
    l2diffList_RL_Mod=[]
    print("RushLarson modified:")    
    for i in range(numberOfTimesteps):
        (time1,VW1)=SolveRLMod(startdt/(factor**i))
        (time2,VW2)=SolveRLMod(startdt/(factor**(i+1)))
        timelabel ="{\\vartriangle}t="+str(startdt/(factor**i))
        #plt.plot(time1,VW1[0],label=timelabel)
        l2diff=L2Diff(VW1[0],VW2[0],startdt/(factor**(i)))
        l2diffList_RL_Mod.append(l2diff)
        print(l2diff)
    l2diff_List.append(l2diffList_RL_Mod)
        
    print("RK4: ") 
    l2deffListRK4 = []
    for i in range(numberOfTimesteps):
        (time1,VW1)=SolveBRRK4(startdt/(factor**i))
        (time2,VW2)=SolveBRRK4(startdt/(factor**(i+1)))
        timelabel ="{\\vartriangle}t="+str(startdt/(factor**i))
        ##plt.plot(time1,VW1[0],label=timelabel)
        l2diff=L2Diff(VW1[0],VW2[0],startdt/(factor**(i)))
        l2deffListRK4.append(l2diff)
        print(l2diff)
    l2diff_List.append(l2deffListRK4)
    #plt.legend()
    #plt.show()
    caption ='Convergence in time for ${\\vartriangle}t_0=  '+ str(startdt) + '\\,$ms, $T='+str(timeinterval[-1])+'\\,$ms  where $g_{j} = \\| V_{j}  - V_{j+1} \\|_\\L2$  is the differences of calculated potentials. The rate on timestep $j$  is computed by $\\operatorname{log}_2\\frac{g_{j}}{g_{j+1}}$'
    fName='TimeConvergenceBRT'
    nL=['Explicit Euler','Rush Larson','Rush Larson Modified','RK4']
    writeInTable(l2diff_List,fName,caption,nL)
    
    
def conTestWithPlot(dt): 
    (time_ref,REF)=referenceSolution(dt)
    V_Ref=REF[0]
    numberOfTimesteps=5 
    Vlist_RL_New=[]
    for i in range(numberOfTimesteps):
        (time1,VW1)=SolveRLMod(dt/(factor**i))
        timelabel ="RL dt="+str(dt/(factor**i))
        #plt.plot(time1,VW1[0],label=timelabel)
        Vlist_RL_New.append(VW1[0])
    Vlist_RL_Old=[]
    for i in range(numberOfTimesteps):
        (time1,VW1,RHS1)=SolveBRRushLarson(dt/(factor**i))
        timelabel ="RL dt="+str(dt/(factor**i))
        #plt.plot(time1,VW1[0],label=timelabel)
        Vlist_RL_Old.append(VW1[0])
    
    for i in range(len(Vlist_RL_New)):
        diff_RK4_RLNew=[]
        diff_RK4_RLOld=[]
        t = np.linspace(timeinterval[0],timeinterval[-1] ,int(timeinterval[-1]/(dt/(factor**i))))
        
        print(len(t),len(V_Ref))
        f_ref=1
        if len(V_Ref)%10!=0:
            f_ref=int((len(V_Ref)-1)/len(t))
        else:
            f_ref=int(len(V_Ref)/len(t))
        list_l2diff_RLnew=[]
        list_l2diff_RLold=[]
        for k in range(len(t)):
            #print(time_ref[k*f_ref],time1[k])
            diff_RK4_RLNew.append(V_Ref[k*f_ref]-Vlist_RL_New[i][k])
            diff_RK4_RLOld.append(V_Ref[k*f_ref]-Vlist_RL_Old[i][k])
        
        #print(len(t),len(diff))
        (diff_RK4_RLNew)=resizeData(diff_RK4_RLNew,1*2**(i),len(diff_RK4_RLNew))
        (diff_RK4_RLOld)=resizeData(diff_RK4_RLOld,1*2**(i),len(diff_RK4_RLOld))
        (t)=resizeData(t,1*2**(i),len(t))
        #print(len(t),len(diff_RK4_RLNew),len(diff_RK4_RLOld))
        plt.plot(t,diff_RK4_RLOld,linestyle='dashed',label='Ref-RL  '+str(dt/(factor**i)),color=colorList[i])
        plt.plot(t,diff_RK4_RLNew,linestyle='dotted',label='Ref-RL (Mod) '+str(dt/(factor**i)),color=colorList[i])
        
    plt.legend(bbox_to_anchor=(1.1,1), loc="upper left") 

    pname='CompareRK4RLVariants2'
    import tikzplotlib
    tikzplotlib.save('../../../thesis-lindner/Texte/BRProperties/images/' + pname + ".tex")
    #plt.show()
def RLconv(dt):
    ref_j=8
    (time_ref,REF)=referenceSolution(dt,ref_j)
    V_Ref=REF[0]
    L2norm_ref=L2(V_Ref,dt/(factor**ref_j))
    print("ref finished")
    numberOfTimesteps=5 
    Vlist_RL_Mod=[]
    Vlist_RL=[]
    l2difflist=[[],[]]
    for i in range(numberOfTimesteps):
        (t,VCW)=SolveRLMod(dt/(factor**i))
        Vlist_RL_Mod.append(VCW[0])
        (t,VCW,RHS)=SolveBRRushLarson(dt/(factor**i))
        Vlist_RL.append(VCW[0])
        print('dt ', dt/(factor**i),'finished')
    for i in range(len(Vlist_RL)):
        value = L2Diff(Vlist_RL[i],V_Ref,dt/(factor**(i)))/L2norm_ref
        l2difflist[0].append(value)
        value= L2Diff(Vlist_RL_Mod[i],V_Ref,dt/(factor**(i)))/L2norm_ref
        l2difflist[1].append(value)
    caption ='Convergence in time for ${\\vartriangle}t_0=  '+ str(dt) + '\\,$ms, $T='+str(timeinterval[-1])+'\\,$ms  where $g_{j} =\\frac{ \\| V_\\text{REF}  - V_{j} \\|_\\L2}{\\| V_\\text{REF}\\|_\\L2}$  is the normed differences to the reference solution. The rate on timestep $j$  is computed by $\\operatorname{log}_2\\frac{g_{j}}{g_{j+1}}$'
    fN='TimeConvergenceBRREF'
    nL=['Rush Larson','Rush Larson Modified']
    writeInTable(l2difflist,fN,caption,nL)
def RLModOrderDiff(dt):
    ref_j=8
    (time_ref,REF)=referenceSolution(dt,ref_j)
    V_Ref=REF[0]
    L2norm_ref=L2(V_Ref,dt/(factor**ref_j))
    numberOfTimesteps=5
    orderlist=['vwc','wcv','cvw','wvc','vcw','cwv']
    l2diffList=[[],[],[],[],[],[]]
    for i in range(numberOfTimesteps):
        for index in range(len(orderlist)):
            (t,VCW)=SolveRLModOrderSpecified(dt/(factor**(i)),orderlist[index])
            value=L2Diff(VCW[0],V_Ref,dt/(factor**(i)))/L2norm_ref
            l2diffList[index].append(value)
            
    fN='OrderComparison'
    caption='Convergence in time for different orders of the modified RL with ${\\vartriangle}t_0=  '+ str(dt) + '\\,$ms, $T='+str(timeinterval[-1])+'\\,$ms  where $g_{j} =\\frac{ \\| V_\\text{REF}  - V_{j} \\|_\\L2}{\\| V_\\text{REF}\\|_\\L2}$  is the normed differences to the reference solution. The rate on timestep $j$  is computed by $\\operatorname{log}_2\\frac{g_{j}}{g_{j+1}}$'
    
    nL=['$\V$-$\w$-$c$','$\w$-$c$-$\V$','$c$-$\V$-$\w$','$\w$-$\V$-$c$','$\V$-$c$-$\w$','$c$-$\w$-$\V$']
    writeInTable(l2diffList,fN,caption,nL,False)
def convergenceWithREF(dt):
    ref_j=8
    (time_ref,REF)=referenceSolution(dt,ref_j)
    V_Ref=REF[0]
    L2norm_ref=L2(V_Ref,dt/(factor**ref_j))
    numberOfTimesteps=5
    nL=['EE','RL','RL Modified ($\V$-$\w$-$c$)','RK4']
    l2diffList=[]
    for name in nL:
        l2diffList.append([])
    for i in range(numberOfTimesteps):
        (t,VCW,RHS)=SolveBREE(dt/(factor**i))
        value=L2Diff(VCW[0],V_Ref,dt/(factor**(i)))/L2norm_ref
        l2diffList[0].append(value)
        
        (t,VCW,RHS)=SolveBRRushLarson(dt/(factor**i))
        value=L2Diff(VCW[0],V_Ref,dt/(factor**(i)))/L2norm_ref
        l2diffList[1].append(value)
        
        (t,VCW)=SolveRLModOrderSpecified(dt/(factor**i),'vwc')
        value=L2Diff(VCW[0],V_Ref,dt/(factor**(i)))/L2norm_ref
        l2diffList[2].append(value)
        
        (t,VCW)=SolveBRRK4(dt/(factor**i))
        value=L2Diff(VCW[0],V_Ref,dt/(factor**(i)))/L2norm_ref
        l2diffList[3].append(value)
    fN='TimeConvergenceDiffSchemesRef'
    caption='Convergence in time for different integration schemes using the $\\'+smoothing+'$ smoothing with ${\\vartriangle}t_0=  '+ str(dt) + '\\,$ms, $T='+str(timeinterval[-1])+'\\,$ms  where $g_{j} =\\frac{ \\| V_\\text{REF}  - V_{j} \\|_\\L2}{\\| V_\\text{REF}\\|_\\L2}$  is the normed differences to the reference solution. The rate on timestep $j$  is computed by $\\operatorname{log}_2\\frac{g_{j}}{g_{j+1}}$'
    writeInTable(l2diffList,fN,caption,nL,True)
    
def conIionIOnTime():
    dt=0.1
    numberOfTimesteps=6
    IionList=[]
    for i in range(numberOfTimesteps):
        print('dt=',dt/(factor**i))
        (t,VCW,RHS)=SolveRLMod(dt/(factor**i),True)
        #(t,VCW,RHS)=SolveBRRushLarson(dt/(factor**i))
        #(t,VCW,RHS)=SolveBREE(dt/(factor**i))
        Iion=RHS[0]
        print(len(t),len(Iion))
        #if len(t)>805:
            #t=resizeData(t,2*(i-3),len(t))
            #Iion=resizeData(Iion,2*(i-3),len(Iion))
        #t=resizeData(t,2**(i),len(t))
        #Iion=resizeData(Iion,2**(i),len(Iion))
        print(len(t),len(Iion))
        labelName='$j='+str(i)+'$'
        if len(t)-len(Iion)==1:
            plt.plot(t[:-1],Iion,label=labelName,color=colorList[i])
        else:
            plt.plot(t,Iion,label=labelName,color=colorList[i])
    plt.legend()
    pname='IionInTimeRLMod01T'+str(int(timeinterval[-1]))
    #import tikzplotlib
    #tikzplotlib.save('../../../thesis-lindner/Texte/BRProperties/images/' + pname + ".tex")
    plt.show()
        
if __name__=="__main__":
    #SolveBRStandard()
    dt=0.01
    conIionIOnTime()
    #(time,VW,rhs)=SolveBREE(dt)
    #plotPotential(time,VW[0])
    #plotRHS(dt,time,rhs)
    #(time1,VW1,rhs1,DViion)=SolveBRRushLarson(dt)
    #plotVW(time1[1:],DViion)
    #plotPotential(time,VW1[0])
    #plotVW(time,VW)
    #plotRHS(dt,time,rhs)
    #plotGatingInfty(2)
    #plotAlphaBeta()
    #(time,Vw)=SolveBRRK4(dt)
    #plotDerivatives(time,Vw)
    #plotDerivativesVDepending()
    #TestStiffness(time,Vw,dt)
    #plotPotential(time,Vw[0])
    #plotVW(time,VW)
    #convergenceWithREF(dt)
    #RLModOrderDiff(dt)
    #RLconv(dt)
    #convergenceTest()
    #conTestWithPlot(dt)
    #path='../../../ODESingelCellConvergence.txt'
    #(List)=readL2DiffsFromFile(path)
    #writeInTable(List)
    #plotContinuousIEXT()
