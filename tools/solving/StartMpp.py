import sys
sys.path.append('../../')

import mpp.python.mppy as mppy

lList = [4]#,1,2,3]
tList = [0,1,2,3,4]
start_dt=0.0001

#path ='../../datafiles/TestMpp'
#pathExperiment=''

path='../../data/3DTetra/'
pathExperiment ="TetraG0001SATinODE"

elphyProblem='TetraTestProblem'
isIextInPDE=0
#isIextInPDE=1
crankNicolsonTheta=0.5
#crankNicolsonTheta=1.0
externalCurrentSmoothingInSpace ='linear' #'discrete'
isExternalCurrentSmooth=1 #0
#ElphyLinearSolverVerbose =

def startExperiment(startconf):
    hosts={21:32,22:32}
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    
    for l in lList:
        for j in tList:
            file = path+pathExperiment+'/log_l'+str(l)+'j'+str(j)+'m1'
            
            kwargs={"ElphyProblem":elphyProblem,"ElphyLevel": l,"DeltaTime":start_dt*2**(-j),"ExternalCurrentSmoothingInSpace":externalCurrentSmoothingInSpace, "IsExternalCurrentSmooth":isExternalCurrentSmooth,"logfile":file, "CrankNicolsonTheta":crankNicolsonTheta,"IextInPDE":isIextInPDE}
            mpp.run(hosts, config=startconf, kwargs=kwargs) #replace 64 by hosts if necessary

def startTest(startconf,l,j):
    file = path+pathExperiment+'/log_l'+str(l)+'j'+str(j)+'m1'
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    kwargs={"ElphyProblem":elphyProblem,"ElphyLevel": l,"DeltaTime":start_dt*2**(-j),"ExternalCurrentSmoothingInSpace":externalCurrentSmoothingInSpace, "IsExternalCurrentSmooth":isExternalCurrentSmooth,"logfile":file, "EndTime":0.0001}
    mpp.run(4, config=startconf, kwargs=kwargs)
    
    
if __name__=="__main__":
    startconfig="m++"
    if len(sys.argv)>1:
        if sys.argv[1]=='G':
            startconfig="electrophysiology/HexaCubeSplitting/start"
        elif sys.argv[1]=='LI':
            startconfig="electrophysiology/HexaTestCube/start"
        startExperiment(startconfig)
    else:
        print("standard elphy.conf started")
        startTest(startconfig,0,0)
