import sys
sys.path.append('../../')
import os

import mpp.python.mppy as mppy

lList = [2]
tList = [3,4,5,6,7,8]
lList=[0]
tList=[0]
start_dt=0.0001

#path ='../../datafiles/TestMpp/'
path='../../data/Test/'
pathToMakeFolders='../'+path
#pathExperiment=''

def startExperiment(procs,startconf,fL,aL):
    for file in fL:
        checkIfFolderExists(file)
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    
    for l in lList:
        for j in tList:
            for index in range(len(fL)):
                file = path+fL[index]+'log_l'+str(l)+'j'+str(j)+'m1'
                kwargs={"ElphyLevel": l,"DeltaTime":start_dt*2**(-j),"logfile":file}
                kwargs.update(aL[index])
                mpp.run(procs, config=startconf, kwargs=kwargs) 
                
def startExperimentPerAlg(algKeys, folderlist,procs,startconf,moveVTU=False):
    
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    for l in lList:
        for j in tList:
            
            for alg in range(len(folderlist)):
                checkIfFolderExists(folderlist[alg])
                file = path+folderlist[alg]+'log_l'+str(l)+'j'+str(j)+'m1'
                kwargs={"ElphyLevel": l,"DeltaTime":start_dt*2**(-j),"logfile":file,"EndTime":0.1,"ElphyVTK":0,"ElphyTimeLevel":j,"ParallelPlotting":False,"Compression":"ascii"}
                if algKeys[alg]!=None:
                    kwargs.update(algKeys[alg])
                mpp.run(procs, config=startconf, kwargs=kwargs) #replace 64 by hosts if necessary
                moveActivationtime(l,j,folderlist[alg],moveVTU)

def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=64
    #filelist =["AsymptoticTest/SI0001SAT/","AsymptoticTest/LI0001SAT/","AsymptoticTest/IE0001SAT/"]
    #argumentlist =[{"ElphyModel":"SemiImplicit"},{"ElphyModel":"LinearImplicit"},{"ElphyModel":"ImplictEuler"}]
   # startconf="TetraTest/start"
   # startExperiment(procs,startconf,filelist,argumentlist)
    
    startconf="electrophysiology/TetraTest/start"
    pathExperiments=["SIOC0001/"]
    keylist=[{"ElphyProblem":"EllipsoidProblem","ElphyModel":"SemiImplicitOnCells"}]
    startExperimentPerAlg(keylist,pathExperiments,procs,startconf,False)
    
