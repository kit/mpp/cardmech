from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
import tikzplotlib

Iext=0.05
a=0.13
b=0.013
c1=0.26
c2=0.1
c3=1
tau=10
tbeg=0
y0=[0,0]

savepath='../../../../thesis-lindner/thesis/images/erstmal/'

timeinterval =[0.0,400.0]
def getExternalCurrent(t):
    if tbeg<t<tbeg+tau:
        return Iext
    else:
        return 0.0
def RHS(t,y):
    V = y[0]
    w =y[1]
    g=[]
    g.append(c1*V*(V-a)*(1-V)-c2*w +getExternalCurrent(t))
    g.append(b*(V-c3*w))
    return g
def SolveFHN():
    sol=solve_ivp(RHS,timeinterval,y0,'BDF')
    plt.axhline(y = 0, color = 'tab:gray', linestyle = ':') 
    plt.plot(sol.t,sol.y[0],label='$V$',linewidth=3.5)
    plt.plot(sol.t,sol.y[1],label='$w$',linewidth=3.5)
    plt.legend()
    
    #plt.xlabel('Zeit in ms')
    #plt.ylabel('Transmembranspannung $\\V$ in mV')
    tikzplotlib.save( savepath+"AktionspotentialFHN.tex")
    #plt.show()
    plt.close()
    #plt.plot(sol.t,sol.y[1],label='$w$',linewidth=3.5)
    #plt.xlabel('Zeit in ms')
    #plt.ylabel('Erholungsvariable $w$')
    #import tikzplotlib
    #tikzplotlib.save( savepath+"FHNGating.tex")
    
if __name__=="__main__":
    SolveFHN()
