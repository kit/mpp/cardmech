from scipy.integrate import solve_ivp
import numpy as np
import matplotlib.pyplot as plt
import tikzplotlib


#Model data:
tbeg=2
Iext=30
tau=3.0
smoothing='stepfunction' #'tanh'    #'arctan' #'stepfunction'

alphaC =[[0.095,  -0.01,  -5., 0.,  0.,  -0.072, 1.],[0.012,  -0.008, 28., 0.,  0.,  0.15,   1.],[0.126,  -0.25,  77, 0,  0,  0,      0],[0.055,  -0.25,  78, 0,  0,  -0.2,   1],[0,      0,      47, -1, 47, -0.1,   -1],[0.0005, 0.083,  50, 0,  0,  0.057,  1] ]

betaC=[[0.07, -0.017, 44, 0, 0, 0.05, 1],[0.0065, -0.02, 30, 0, 0, -0.2, 1],[1.7, 0, 22.5, 0, 0, -0.082, 1],[0.3, 0, 32, 0, 0, -0.1, 1],[40, -0.056, 72, 0, 0, 0, 0],[0.0013, -0.06, 20, 0, 0, -0.04, 1]]


y0= [-84.57,0.0000002, 0.002980,1.0,0.9877,0.975,0.011,0.00565]
y_names=['V','Ca','d','f','h','j','m','x1']
w_names=['d','f','h','j','m','x1']

savepath='../../../../thesis-lindner/thesis/images/erstmal/'

timeinterval =[0.0,400.0]
########################################################################################################################################
def Alpha(i,V):
    alpha =(alphaC[i][0]*np.exp(alphaC[i][1]*(V+alphaC[i][2])) +alphaC[i][3]*(V+alphaC[i][4]))/(np.exp(alphaC[i][5]*(V+alphaC[i][2]))+alphaC[i][6])
    return alpha
def Beta(i,V):
    beta =(betaC[i][0]*np.exp(betaC[i][1]*(V+betaC[i][2])) +betaC[i][3]*(V+betaC[i][4]))/(np.exp(betaC[i][5]*(V+betaC[i][2]))+betaC[i][6])
    return beta



def getExternalCurrent(t):
    if tbeg<t<tbeg+tau:
        return Iext
    else:
        return 0.0
def RHS(t,y):
    mVolt = y[0]
    Ca =y[1]
    d = y[2]
    f =y[3]
    h = y[4]
    j =y[5]
    m = y[6]
    x1 =y[7]

    I_Na = (4.0*m*m*m*h*j+0.003)*(mVolt-50.0)
    I_s = 0.09*d*f*(mVolt-(-82.3 -13.0287*np.log(Ca)))
    I_x1 = x1* ((0.8*(np.exp(0.04*(mVolt +77))-1))/(np.exp(0.04*(mVolt +35))))
    I_K = 0.35*(((4*(np.exp(0.04*(mVolt +85))-1))/(np.exp(0.08*(mVolt+53))+np.exp(0.04*(mVolt+53))))+((0.2*(mVolt+23))/(1-np.exp(-0.04*(mVolt+23)))))
    g=[]
    g.append(-1.0*(I_Na + I_s +I_x1 + I_K - getExternalCurrent(t)))
    g.append(-0.0000001*I_s +0.07*(0.0000001 -Ca))
    for i in range(2,len(y)): 
        alphaV=Alpha(i-2,mVolt)
        betaV = Beta(i-2,mVolt)
        g.append((alphaV + betaV)*((alphaV/(alphaV+betaV)) - y[i]))
  
    return g
def SolveBR():
    sol=solve_ivp(RHS,timeinterval,y0,'BDF')
    plt.axhline(y = 0, color = 'tab:gray', linestyle = ':') 
    plt.plot(sol.t,sol.y[0],label='$V$',linewidth=3.5)
    #plt.legend()
    
    plt.xlabel('Zeit in ms')
    plt.ylabel('Transmembranspannung $\\V$ in mV')
    tikzplotlib.save( savepath+"AktionspotentialBR.tex")
    #plt.show()
    plt.close()
    #plt.plot(sol.t,sol.y[1],label='$w$',linewidth=3.5)
    #plt.xlabel('Zeit in ms')
    #plt.ylabel('Erholungsvariable $w$')
    #import tikzplotlib
    #tikzplotlib.save( savepath+"FHNGating.tex")
    
if __name__=="__main__":
    SolveBR()
