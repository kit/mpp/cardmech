import sys    
from vtk.util.numpy_support import vtk_to_numpy
from vtk.vtkIOXML import vtkXMLUnstructuredGridReader

def readVTU(filename):
    reader = vtkXMLUnstructuredGridReader()
    reader.SetFileName(filename)
    reader.Update()
    data = reader.GetOutput()
    # Extract Points
    points = vtk_to_numpy(data.GetPoints().GetData())

    point_data = data.GetPointData()
    data = vtk_to_numpy(point_data.GetAbstractArray(0))
    
    return  points, data
def extractAndMoveDataFromVTU(alg,l,j):
    block =''
    filename ='data/vtu/ActivationTime.vtu'
    points, point_data = readVTU(filename)
    out_f = open('../../data/BiVentricle/'+str(alg)+'0001/' +'AT_l'+str(l)+'j'+str(j) , "w")
    for i in range(len(points)):
        listvalues=[points[i],point_data[i]]
        out_f.write("{} {} {} {}\n".format('{:.8f}'.format(points[i][0]),'{:.8f}'.format(points[i][1]),'{:.8f}'.format(points[i][2]), point_data[i]))
    out_f.close()
    return block

if __name__=="__main__":
    
    extractAndMoveDataFromVTU(sys.argv[0],sys.argv[1],sys.argv[2])
    
    
