import sys
sys.path.append('../../')
import os
import shutil
import mpp.python.mppy as mppy

path='../../../data/NiedererBenchmark/'
pathToMakeFolders=path #'../'+path

def moveActivationtime(l,j, expPath):
    shutil.move('../../build/data/vtu/ActivationTime.vtu', pathToMakeFolders+expPath+'AT_l'+str(l)+'j'+str(j)+'.vtu')
    
        

def startExperiment(procs,startconf,pE,tdisc,listMeshes, added=None):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    
    for l in range(len(listMeshes)):
        for j in range(len(tdisc)):
            file = path+pE+'log_l'+str(l)+'j'+str(j)
            kwargs={"ElphyLevel": 0,"DeltaTime":tdisc[j],"logfile":file,"EndTime":0.06,"ElphyVTK":0,"Mesh":listMeshes[l]}
            if added!=None:
                kwargs.update(added)
            mpp.run(procs, config=startconf, kwargs=kwargs) 
            moveActivationtime(l,j,pE)

def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=64
    
    
    startconf="electrophysiology/niedererbenchmark"
    timedisc=[0.00005,0.00001,0.000005]
    unsmooth={"ExternalCurrentSmoothingInTime":"Stepfunction","ExternalCurrentSmoothingInSpace":"discrete"}
    
    listMeshesTet=["NiedererBM05_3DTet","NiedererBM02_3DTet","NiedererBM01_3DTet"]
    pathExperiment="Tetra/"
    #startExperiment(procs,startconf,pathExperiment,timedisc,listMeshesTet)
    
    pathExperiment="UnsmoothTetra/"
    #startExperiment(procs,startconf,pathExperiment,timedisc,listMeshesTet,unsmooth)
    
    
    listMeshesHex=["NiedererBM05_3DQuad","NiedererBM02_3DQuad","NiedererBM01_3DQuad"]
    
    pathExperiment="Hexa/"
    startExperiment(procs,startconf,pathExperiment,timedisc,listMeshesHex)
    
    pathExperiment="UnsmoothHexa/"
    startExperiment(procs,startconf,pathExperiment,timedisc,listMeshesHex,unsmooth)
    
