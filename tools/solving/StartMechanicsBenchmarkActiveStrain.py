#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
sys.path.append('../../')
import os
import shutil
import mpp.python.mppy as mppy
import re


gamma_f = [0.5, 0.55, 0.6, 0.62, 0.64, 0.66, 0.68, 0.7, 0.72, 0.74, 0.76, 0.78, 0.8]
scaled_gamma = [-1, -1.1, -1.2, -1.3, -1.4, -1.5, -1.7, -1.8, -1.9, -2.0]


path='../../../data/MechanicsBenchmark/TestActiveStrain/'
pathToMakeFolders=path #'../'+path

def startExperiment(procs,startconf,pE,added=None):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='M++',kernels=procs,mute=False)
    
    for g in gamma_f:
        for s in scaled_gamma:
            file = path + pE + 'log_gama_' + str(g) + '_scale_' + str(s)
            kwargs = {"g": g, "scale_gamma": s, "logfile": file}
            if added!=None:
                kwargs.update(added)
            mpp.run(procs, config=startconf, kwargs=kwargs)


def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=8
    
    
    startconf="elasticity/benchmarks/LandBenchmark"
    pathExperiment=""
    startExperiment(procs,startconf,pathExperiment)
    
    