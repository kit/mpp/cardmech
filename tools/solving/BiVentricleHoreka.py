import os
#pathToSave='../../data/BiVentricleSE/Test/'
pathToSave='../../data/BiVentricleSE/NewDiffusion/'
#pathToSave='../../data/BiVentricle/'
dataPath='../'+pathToSave

#dataPath='/lsdf/kit/ianm/projects/mppci/cardmech/monodomain/data/BiVentricle/'
def checkIfFolderExists(path):
    if not os.path.exists(path):
        os.makedirs(path)
        
        
def getModelByName(name):
    model =''
    if name =='LI':
        model = 'LinearImplicit'
    elif name =='SI':
        model='SemiImplicit'
    elif name =='IE':
        model ='ImplictEuler'
    elif name =='SIOC':
        model='SemiImplicitOnCells'
    elif name =='SIOCSVI':
        model='SemiImplicitOnCellsSVI'
    elif name =='SIOCCI':
        model=='SemiImplicitOnCellsIextPerCell'
    elif name =='SIICI':
        model='SemiImplicitNodes'
    else:
        model = 'Splitting'
    return model
def getSplittingMethodByName(name):
    if name[0]=='S':
        return 'Strang'
    elif name[0]=='B':
        return 'Stein'
    else:
        return 'Godunov'
def Standard():
    block=''
    block +='#!/bin/bash\n'
    block += '#SBATCH --partition=cpuonly\n'
    block += '#SBATCH --account=hk-project-ipfem\n'
    block += '#SBATCH --export=ALL,EXECUTABLE=./Elphy-M++\n'
    block += '#SBATCH --mail-type=ALL\n'
    block += '#SBATCH --mail-user=laura.lindner2@kit.edu\n'
    return block

def Jobname(name):#name could be SIl3j4
    return '#SBATCH --job-name='+name +'\n'

def Nodes(n):
    return '#SBATCH --nodes='+str(n)+'\n'
def Tasks(n):
    return '#SBATCH --ntasks-per-node='+str(n)+'\n'
def Time(h,m):
    if m==0:
        return '#SBATCH --time='+str(h)+':00:00\n'
    elif m<10:
        return '#SBATCH --time='+str(h)+':0'+str(m)+':00\n'
    else:
        return '#SBATCH --time='+str(h)+':'+str(m)+':00\n'


def Start(args):
    block=''
    block += 'mpirun ${MPIRUN_OPTIONS} Elphy-M++ electrophysiology/BiVentricleTest/biventricelTest'
    for a in args:
        block += ' '+a+'='+str(args[a])
    block+='\n'
    return block
def decideTime(level,j):
    h=0
    m=0
    if level <=2:
            m =2**(j+1)+10
            if m>60:
                h=int(m/60)
                m=m%60
    elif level ==3:
        h=2**(j)
    elif level == 4:
        h=j+1+2**(j+1) 
    elif level == 5:
        h=2**(j+2) +8
        
    else:
        print('no level time defined')
    #print(h,m)
    #h=h*4
    m=4*m
    if m>60:
        h=h+int(m/60)
        m=m%60
    return(h,m)

def modifyJobskript(name,sdtN,j,sdt,args,add=0):
    level=args['ElphyLevel']
    filename =name+'l'+str(level)+'Job'
    (h,m)=decideTime(level,j)
    with open(filename, 'w') as output_file:
        output_file.write(Standard())
        output_file.write(Nodes(2**(level+add-1))) #
        output_file.write(Tasks(64))
        output_file.write(Time(h,m))
        output_file.write(Jobname(name+'l'+str(args['ElphyLevel'])+'j'+str(j)))
        #output_file.write('module load compiler/gnu/8 mpi/openmpi/4.1\n')
        output_file.write('export MPIRUN_OPTIONS="--bind-to core --map-by core -report-bindings"\n')
        output_file.write('echo "${executable} running on ${SLURM_NTASKS_PER_NODE} tasks per node with ${SLURM_NNODES} nodes"\n')
        args.update({'DeltaTime':sdt*2**(-j)})
        output_file.write(Start(args))
        if args['ElphyProblem']=='BiVentricleWAProblem':
            output_file.write(moveActivation(level,j,name,sdtN))
    return name+'l'+str(args['ElphyLevel'])+'Job'


def pythonExtractAndMoveDataFromVTU(alg,l,j):
    return 'python ../tools/solving/ExtractAndMoveATData.py '+ str(alg)+' '+str(l)+' ' +str(j)
def moveActivation(l,j,alg,sdtName):
    block =''
    block+='mv data/vtu/ActivationTime.vtu'+dataPath+str(alg)+sdtName+'/' +'AT_l'+str(l)+'j'+str(j)+'.vtu'
    #block+='mv data/vtu/ActivationTime.vtu ../../data/BiVentricle/'+str(alg)+'0001/' +'AT_l'+str(l)+'j'+str(j)+'.vtu'
    block+='\n'
    return block
def startJobs(lL,tL,nL,sdt,testname,added=None): #testname must end with/!!!!!!
    arguments={}
    sdtName=str(sdt)
    sdtName=sdtName[2:]
    for alg in nL:
        if added!=None:
            if 'ElphyModel' in added:
                model =added['ElphyModel']
            else:
                model =getModelByName(alg)
        else:
            model =getModelByName(alg)
        arguments.update({'ElphyModel':model})
        checkIfFolderExists(pathToSave+alg+sdtName+'/'+testname)
        if model =='Splitting':
            arguments.update({'ElphySplittingMethod':getSplittingMethodByName(alg)})
        
        for l in lL:
            arguments.update({'ElphyLevel':l})
            for j in tL:
                logname=dataPath+alg+sdtName+'/'+testname+'log_l'+str(l)+'j'+str(j)+'m1'
                arguments.update({'logfile':logname})
                if added!=None:
                    arguments.update(added)
                if l>=4:
                    jobname =modifyJobskript(alg,sdtName,j,sdt,arguments,1)
                else:
                    jobname =modifyJobskript(alg,sdtName,j,sdt,arguments,0)
               
                os.system('sbatch '+ jobname)
        

def startSI():
    
    lL=[0,1,2,3,4]
    tL=[0,1,2,3,4]
    lL=[4]
    tL=[4]#,3,4]
    #lL=[3]
    #tL=[3,4]
    nL=['SI']
    sdt=0.0004
    startJobs(lL,tL,nL,sdt,'',{'ElphyProblem':'BiVentricleWAProblem'})

    
def startLI():
    lL=[1]
    tL=[0,1,2,3,4]
    nL=['LI']
    sdt=0.0001
    startJobs(lL,tL,nL,sdt,'',{'ElphyProblem':'BiVentricleWAProblem'})

def startGS():
    lL=[3,4]
    tL=[2,3]
    nL=['GS']
    sdt=0.0004
    startJobs(lL,tL,nL,sdt,'',{'ElphyProblem':'KovachevaBiventricleSimpleExcitationProblem','EndTime':0.65,'CrankNicolsonTheta':1.0})
    
    #startJobs(lL,tL,nL,sdt,'',{'ElphyProblem':'BiVentricleWAProblem'})
def startForTalk():
    lL=[0,1,2,3]
    tL=[2]
    nL=['SI','SIOC']
    sdt=0.0004
    
    startJobs(lL,tL,nL,sdt,'talkChangedDiffusion/',{'ElphyProblem':'BiVentricleProblem'})
    #lL=[4]
    #startJobs(lL,tL,['SIOC'],sdt,'talk/',{'ElphyProblem':'BiVentricleProblem'})
    #startJobs(lL,tL,nL,sdt,'talkBR/',{'ElphyProblem':'BiVentricleProblem',"ElphyModelName": "BeelerReuter","IonScheme":"ExplicitEuler"})
    
def startForTimeTest():
    lL=[5]
    tL=[0]
    nL=['SI']
    sdt=0.0004
    startJobs(lL,tL,nL,sdt,'level5Test/',{'ElphyProblem':'BiVentricleProblem'})

def startSimpleExcitation():
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    
    lL=[5]
    tL=[4]
    nL=['GS','SIICI','SIOC','SI']
    nL= ['SI','SIICI']
    #nL=['SIOCCI']
    sdt=0.0004
    #startJobs(lL,tL,nL,sdt,'',{'ElphyProblem':'KovachevaBiventricleSimpleExcitationProblem','EndTime':0.0004})
    startJobs(lL,tL,nL,sdt,'',{'ElphyProblem':'KovachevaBiventricleSimpleExcitationProblem','EndTime':0.65})
    #startJobs(lL,tL,nL,sdt,'',{'ElphyProblem':'KovachevaBiventricleSimpleExcitationProblem','EndTime':0.65,"ElphyModelName": "BeelerReuter","IonScheme":"ExplicitEuler"})
    lL=[4]
    tL=[4]#,1,2,3]
    nL=['SI']
    #startJobs(lL,tL,nL,sdt,'',{'ElphyProblem':'KovachevaBiventricleSimpleExcitationProblem','EndTime':0.65})

def startSimpleExcitationBR():
    sdt=0.0004
    lL=[2,3,4,5]
    tL=[0,1,2,3]
    nL= ['SIOC','SI']
    startJobs(lL,tL,nL,sdt,'BR/',{'ElphyProblem':'KovachevaBiventricleSimpleExcitationProblem','EndTime':0.5,"ElphyModelName": "BeelerReuter","IonScheme":"ExplicitEuler"})
if __name__=="__main__":

    #startSI()
    #startLI()
    startGS()
    #startForTimeTest()
    #startSimpleExcitation()
    #startSimpleExcitationBR()
    
    
    
    
    

    
    
