import sys
sys.path.append('../../')
import os
import shutil
import mpp.python.mppy as mppy

lList = [0,1,2,3,4]
lList=[3,4]
tList = [0,1,2]
tList=[3,4,5]
lList=[0]
tList=[0]
#start_dt=0.0001
start_dt=0.0001

path='../../../data/cuttedEllipsoidBR/'
pathToMakeFolders=path #'../'+path

def moveActivationtime(l,j, expPath,moveVtu=False):
    shutil.move('../../build/data/AT_l'+str(l)+'j'+str(j)+'.txt', pathToMakeFolders+expPath+'AT_l'+str(l)+'j'+str(j)+'.txt')
    if moveVtu:
        shutil.move('../../build/data/vtu/ActivationTime.vtu', pathToMakeFolders+expPath+'AT_l'+str(l)+'j'+str(j)+'.vtu')
    
        

def startExperiment(procs,startconf,pE,added=None,moveVTU=False):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    
    for l in lList:
        for j in tList:
            file = path+pE+'log_l'+str(l)+'j'+str(j)+'m1'
            kwargs={"ElphyLevel": l,"DeltaTime":start_dt*2**(-j),"logfile":file,"EndTime":0.1,"ElphyVTK":0,"ElphyTimeLevel":j}
            if added!=None:
                kwargs.update(added)
            mpp.run(procs, config=startconf, kwargs=kwargs) #replace 64 by hosts if necessary
            moveActivationtime(l,j,pE,moveVTU)

def startExperimentPerAlg(algKeys, folderlist,procs,startconf,moveVTU=False):
    
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    for l in lList:
        for j in tList:
            
            for alg in range(len(folderlist)):
                checkIfFolderExists(folderlist[alg])
                file = path+folderlist[alg]+'log_l'+str(l)+'j'+str(j)+'m1'
                kwargs={"ElphyLevel": l,"DeltaTime":start_dt*2**(-j),"logfile":file,"EndTime":0.1,"ElphyVTK":0,"ElphyTimeLevel":j,"ParallelPlotting":False,"Compression":"ascii"}
                if algKeys[alg]!=None:
                    kwargs.update(algKeys[alg])
                mpp.run(procs, config=startconf, kwargs=kwargs) #replace 64 by hosts if necessary
                moveActivationtime(l,j,folderlist[alg],moveVTU)
                
def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=64
    startconf="electrophysiology/TetraTest/start"
    moveVTU=True
    
    #pathExperiments=["SISVI0001/","SIOC0001/"]
    #keylist=[{"ElphyProblem":"EllipsoidWAProblem","ElphyModel":"SemiImplicit"},{"ElphyProblem":"EllipsoidWAProblem","ElphyModel":"SemiImplicitOnCells"}]
    #startExperimentPerAlg(keylist,pathExperiments,procs,startconf,moveVTU)
    

    pathExperiments=["SIOC0001/"]
    keylist=[{"ElphyProblem":"EllipsoidProblem","ElphyModel":"SemiImplicitOnCells"}]
    startExperimentPerAlg(keylist,pathExperiments,procs,startconf,moveVTU)
