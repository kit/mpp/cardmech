#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
sys.path.append('../../')
import os
import shutil
import mpp.python.mppy as mppy
import re

#Plots ausschalten

activeStress = [6, 60, 300]
volumetricPenalty = [1000, 5000, 10000]
pressure = [1.5, 15, 150, 500]
degree = [1, 2]
level = [0,1]


path='../../../data/MechanicsBenchmark/'
pathToMakeFolders=path #'../'+path

def checkFileExistance(filePath):
    try:
        with open(filePath, 'r') as f:
            return True
    except FileNotFoundError as e:
        return False
    except IOError as e:
        return False

def moveDisp(d,l,stress,penalty,p, expPath):
    if checkFileExistance('../../build/data/vtu/U-000'+str(4)+'.vtu') == True:
        shutil.move('../../build/data/vtu/U-000'+str(4)+'.vtu', pathToMakeFolders + expPath + str(d) + str(l) + '_stress' + str(stress) + '_pen_' + str(penalty) + 'p' + str(p) + '.vtu')


#def moveDisp(d,l,stress,penalty,p, expPath):
#    for i in range(0,5):
 #       if '../../build/data/vtu/U-000'+str(i)+'.vtu'.exists():
 #           shutil.move('../../build/data/vtu/U-000'+str(i)+'.vtu', pathToMakeFolders + expPath + str(d) + str(l) + '_stress' + str(stress) + '_pen_' + str(penalty) + 'p' + str(p) + '.vtu')
 #       else:
 #           continue



def startExperiment(procs,startconf,pE,added=None):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='M++',kernels=procs,mute=False)
    
    for d in degree:
        for l in level:
            for stress in activeStress:
                for penalty in volumetricPenalty:
                    for p in pressure:
                        file1=pE + 'log_Ta_' + str(stress) + '_penalty_'+ str(penalty) + '_p_' + str(p) + '_deg_' + str(d) + '_l_' + str(l)
                        file = path + pE + 'log_Ta_' + str(stress) + '_penalty_'+ str(penalty) + '_p_' + str(p) + '_deg_' + str(d) + '_l_' + str(l)
                        kwargs={"MechPolynomialDegree": d, "MechLevel": l, "activeStress": stress, "VolumetricPenalty": penalty, "LVPressure": p,"logfile": file}
                        if added!=None:
                            kwargs.update(added)
                        mpp.run(procs, config=startconf, kwargs=kwargs) 
                        moveDisp(d,l,stress,penalty,p, pE)
                        # check if programm is converging


def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=8
    
    
    startconf="elasticity/benchmarks/LandBenchmark"
    pathExperiment=""
    startExperiment(procs,startconf,pathExperiment)
    
    