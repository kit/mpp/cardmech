#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../../')
import os
import mpp.python.mppy as mppy


CoupledProblem = ["BiventricleCoarse", "DeformedBiventricleCoarse"]
cell_values = [0, 1]
deformConcentrationStretch = [0, 1]
updateStretch = [0, 1]


path='../../../data/preparationGamm/'
pathToMakeFolders=path #'../'+path


def startExperiment(procs, startconf, pE ,added=None):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='M++',kernels=procs,mute=False)
    
    for cP in CoupledProblem:
        for cV in cell_values:
            for dCS in deformConcentrationStretch:
                for uS in updateStretch:
            
                    file = path + pE + 'log' + str(cP) + '_cellV'+ str(cV) +'_deformCS' + str(dCS) + '_updateStretch' + str(uS)
                    kwargs={"CoupledProblem": cP, "cell_values": cV, "logfile": file, "deformConcentrationStretch": dCS,
                            "updateStretch": uS}
                    if added!=None:
                        kwargs.update(added)
                    mpp.run(procs, config=startconf, kwargs=kwargs) 

def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=64
    
    
    startconf="coupled/gamm-paper/coarse-biventricle"
    pathExperiment=""
    startExperiment(procs,startconf,pathExperiment)
    
    
