import sys
sys.path.append('../../')
import os
import shutil
import mpp.python.mppy as mppy

lList = [0,1,2,3]
tList = [0,1,2,3]
#start_dt=0.0001
start_dt=0.0004

path='../../../data/biventricleTT/'
path='../../../data/biventricleBR/'
pathToMakeFolders=path #'../'+path

def moveActivationtime(l,j, expPath,moveVtu=False):
    shutil.move('../../build/data/AT_l'+str(l)+'j'+str(j)+'.txt', pathToMakeFolders+expPath+'AT_l'+str(l)+'j'+str(j)+'.txt')
    if moveVtu:
        shutil.move('../../build/data/vtu/ActivationTime.vtu', pathToMakeFolders+expPath+'AT_l'+str(l)+'j'+str(j)+'.vtu')
    
        

def startExperiment(procs,startconf,pE,added=None,moveVTU=False):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    
    for l in lList:
        for j in tList:
            file = path+pE+'log_l'+str(l)+'j'+str(j)+'m1'
            kwargs={"ElphyLevel": l,"DeltaTime":start_dt*2**(-j),"logfile":file,"EndTime":0.3,"ElphyVTK":0,"ElphyTimeLevel":j}
            if added!=None:
                kwargs.update(added)
            mpp.run(procs, config=startconf, kwargs=kwargs) #replace 64 by hosts if necessary
            moveActivationtime(l,j,pE,moveVTU)

def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=64
    startconf="electrophysiology/BiVentricleTest/biventricelTest"
    moveVTU=True
    
    pathExperiment="SI0004/"
    #startExperiment(procs,startconf,pathExperiment,{"ElphyModelName": "TenTusscher","ParallelPlotting":False,"Compression":"ascii"},moveVTU)
    
    pathExperiment="OnCells/SI0004/"
    
    #startExperiment(procs,startconf,pathExperiment,{"ElphyModelName": "TenTusscher","ElphyModel":"SemiImplicitOnCells","ParallelPlotting":False,"Compression":"ascii"},moveVTU)
    
    #test original
    pathExperiment="SI0004/"
    startconf='electrophysiology/BiVentricleTest/start'
    #startExperiment(procs,startconf,pathExperiment,{'EndTime':0.16,"ParallelPlotting":False,"Compression":"ascii"},moveVTU)
    
    pathExperiment="OnCells/SI0004/"
    startconf='electrophysiology/BiVentricleTest/start'
    startExperiment(procs,startconf,pathExperiment,{"ElphyModel":"SemiImplicitOnCells",'EndTime':0.16,"ParallelPlotting":False,"Compression":"ascii"},moveVTU)
    
