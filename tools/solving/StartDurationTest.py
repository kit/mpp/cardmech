import sys
sys.path.append('../../')
import os

import mpp.python.mppy as mppy

lList = [1,2,3]
tList = [0,1,2,3,4]
start_dt=0.0001

#path ='../../datafiles/TestMpp/'
path='../../data/3DTetra/'
pathToMakeFolders='../'+path
#pathExperiment=''




elphyProblem='TetraTestProblem'
endTime = 0.0003
#isIextInPDE=0
isIextInPDE=1
crankNicolsonTheta=0.5
#crankNicolsonTheta=1.0
externalCurrentSmoothingInSpace ='linear' #'discrete'
isExternalCurrentSmooth=1 #0


def startExperiment(procs,startconf,pE,added=None):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    
    for l in lList:
        for j in tList:
            file = path+pE+'log_l'+str(l)+'j'+str(j)+'m1'
            kwargs={"ElphyProblem":elphyProblem,"ElphyLevel": l,"DeltaTime":start_dt*2**(-j),"ExternalCurrentSmoothingInSpace":externalCurrentSmoothingInSpace, "IsExternalCurrentSmooth":isExternalCurrentSmooth,"logfile":file, "CrankNicolsonTheta":crankNicolsonTheta,"IextInPDE":isIextInPDE,"EndTime":endTime,"ElphyLinearSolverVerbose":1}
            if added!=None:
                kwargs.update(added)
            mpp.run(procs, config=startconf, kwargs=kwargs) #replace 64 by hosts if necessary


def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=32
    
    print("Start Godunov Test")
    startconf="electrophysiology/HexaCubeSplitting/start"
    pathExperiment="DurationTest/G0001SAT/"
    startExperiment(procs,startconf,pathExperiment,{"ElphySolverVerbose":10,"ElphyVerbose":10})
    

    startconf="electrophysiology/HexaCubeSplitting/start"
    pathExperiment="DurationTest/G0001SATReassembleOnce/"
    startExperiment(procs,startconf,pathExperiment,{"ElphySolverVerbose":10,"ElphyVerbose":10,"ReassembleRHSonce":1})
    
    print("Start LI Test")
    startconf="electrophysiology/HexaTestCube/start"
    pathExperiment= "DurationTest/LI0001SAT/"
    startExperiment(procs,startconf,pathExperiment,{"LinearImplicitVerbose":10})
    
    startconf="electrophysiology/HexaTestCube/start"
    pathExperiment= "DurationTest/LI0001SATReassembleOnce/"
    startExperiment(procs,startconf,pathExperiment,{"LinearImplicitVerbose":10,"ReassembleRHSonce":1})
    
    print("Start LI GaussSeidel Test")
    startconf="electrophysiology/HexaTestCube/start"
    pathExperiment="DurationTest/LI0001SATGaussSeidel/"
    startExperiment(procs,startconf,pathExperiment,{"LinearImplicitVerbose":10,"ElphyPreconditioner":'GaussSeidel'})
