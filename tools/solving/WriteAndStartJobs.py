import os


def checkIfFolderExists(path):
    if not os.path.exists(path):
        os.makedirs(path)
        
def moveActivation(l,j,alg):
    block =''
    block+='mv data/vtu/ActivationTime.vtu ../../data/'+str(alg)+'0001SAT/' +'AT_l'+str(l)+'j'+str(j)+'.vtu'
    block+='\n'
    return block        
def getModelByName(name):
    model =''
    if name =='LI':
        model = 'LinearImplicit'
    elif name =='SI':
        model='SemiImplicit'
    elif name =='IE':
        model ='ImplictEuler'
    else:
        model = 'Splitting'
    return model
def getSplittingMethodByName(name):
    if name[0]=='S':
        return 'Strang'
    elif name[0]=='B':
        return 'Stein'
    else:
        return 'Godunov'
def Standard():
    block=''
    block +='#!/bin/bash\n'
    block += '#SBATCH --partition=cpuonly\n'
    block += '#SBATCH --account=hk-project-ipfem\n'
    block += '#SBATCH --export=ALL,EXECUTABLE=./Elphy-M++\n'
    block += '#SBATCH --mail-type=ALL\n'
    block += '#SBATCH --mail-user=laura.lindner2@kit.edu\n'
    return block

def Jobname(name):#name could be SIl3j4
    return '#SBATCH --job-name='+name +'\n'

def Nodes(n):
    return '#SBATCH --nodes='+str(n)+'\n'
def Tasks(n):
    return '#SBATCH --ntasks-per-node='+str(n)+'\n'
def Time(h,m):
    if m==0:
        return '#SBATCH --time='+str(h)+':00:00\n'
    elif m<10:
        return '#SBATCH --time='+str(h)+':0'+str(m)+':00\n'
    else:
        return '#SBATCH --time='+str(h)+':'+str(m)+':00\n'


def Start(args):
    block=''
    block += 'mpirun ${MPIRUN_OPTIONS} Elphy-M++ electrophysiology/TetraTest/start'
    for a in args:
        block += ' '+a+'='+str(args[a])
    block+='\n'
    return block
def decideTime(level,j):
    h=0
    m=0
    if level <=3:
            m =2**(j+1)
            if m>60:
                h=int(m/60)
                m=m%60
    elif level == 4:
        if j<=2:
            h=1
        elif j ==3:
            h=2
        elif j ==4:
            h=4
        elif j==5:
            h=8
        elif j==6:
            h=16
        else:
            print('no time for j='+str(j)+' defined')
    elif level==5:
        h=2**j +2
        
    else:
        print('no level time defined')
    #print(h,m)
    return(4*h,4*m)

def modifyJobskript(name,j,sdt,args,add=0):
    level=args['ElphyLevel']
    filename =name+'l'+str(level)+'Job'
    (h,m)=decideTime(level,j)
    with open(filename, 'w') as output_file:
        output_file.write(Standard())
        output_file.write(Nodes(2**(level+add)))
        output_file.write(Tasks(64))
        output_file.write(Time(h,m))
        output_file.write(Jobname(name+'l'+str(args['ElphyLevel'])+'j'+str(j)))
        output_file.write('module load compiler/gnu/8 mpi/openmpi/4.1\n')
        output_file.write('export MPIRUN_OPTIONS="--bind-to core --map-by core -report-bindings"\n')
        output_file.write('echo "${executable} running on ${SLURM_NTASKS_PER_NODE} tasks per node with ${SLURM_NNODES} nodes"\n')
        args.update({'DeltaTime':sdt*2**(-j)})
        output_file.write(Start(args))
        if args['ElphyProblem']=='EllipsoidWAProblem':
            output_file.write(moveActivation(level,j,name))
    return name+'l'+str(args['ElphyLevel'])+'Job'

def startJobs(lL,tL,nL,testname,added=None):
    sdt=0.0001
    arguments={}
    for alg in nL:
        if added!=None:
            if 'ElphyModel' in added:
                model =added['ElphyModel']
            else:
                model =getModelByName(alg)
        else:
            model =getModelByName(alg)
        arguments.update({'ElphyModel':model})
        checkIfFolderExists('../../data/'+alg+'0001SAT/'+testname+'/')
        if model =='Splitting':
            arguments.update({'ElphySplittingMethod':getSplittingMethodByName(alg)})
        
        for l in lL:
            arguments.update({'ElphyLevel':l})
            for j in tL:
                logname='../../data/'+alg+'0001SAT/'+testname+'/log_l'+str(l)+'j'+str(j)+'m1'
                arguments.update({'logfile':logname})
                if added!=None:
                    arguments.update(added)
                if model == 'ImplictEuler':
                    jobname =modifyJobskript(alg,j,sdt,arguments,1)
                else:
                    jobname =modifyJobskript(alg,j,sdt,arguments)
                #print(jobname)
                os.system('sbatch '+ jobname)
        
def startSmoothingTest():
    lL=[3,4]
    tL=[3,4]
    nL=['LI']
    startJobs(lL,tL,nL,'timeSmoothing',{'ExternalCurrentSmoothingInSpace':'discrete'})
    startJobs(lL,tL,nL,'spaceSmoothing',{'ExternalCurrentSmoothingInTime':'Stepfunction'})
    startJobs([4],[3],nL,'timeSmoothing',{'ExternalCurrentSmoothingInSpace':'discrete'})
    startJobs([4],[3],nL,'spaceSmoothing',{'ExternalCurrentSmoothingInTime':'Stepfunction'})

def startOderTest():
    lL=[3,4]
    tL=[3,4]
    nL=['LI']
    startJobs(lL,tL,nL,'Vcw',{'OrderStaggeredScheme':'Vcw'})
    startJobs(lL,tL,nL,'Vwc',{'OrderStaggeredScheme':'Vwc'})
    startJobs(lL,tL,nL,'cwV',{'OrderStaggeredScheme':'cwV'})
    startJobs(lL,tL,nL,'cVw',{'OrderStaggeredScheme':'cVw'})
    startJobs(lL,tL,nL,'wVc',{'OrderStaggeredScheme':'wVc'})
    
def startICI():
    lL=[3,4]
    tL=[3,4]
    nL=['LI']
    tL=[5]
    startJobs(lL,tL,nL,'ICI',{'ElphyModel':'LinearImplicitNodes'})

def startStein():
    lL=[3,4]
    tL=[1,2,3,4,5,6]
    startJobs(lL,tL,['B'],'Heun')
def startSplittingVerisons():
    lL=[3,4]
    tL=[3,4]
    nL=['GODE']
    startJobs(lL,tL,['GODE'],'IE',{'IextInPDE':0,'CrankNicolsonTheta':1.0})
    #startJobs(lL,tL,['B'],'')
    
def splittingTimeIntegration():
    lL=[3,4]
    tL=[2,5]
    nL=['GPDE']
    #startJobs(lL,tL,nL,'EE',{'CrankNicolsonTheta':0.0,'IextInPDE':1})
    ##startJobs(lL,tL,nL,'CN',{'CrankNicolsonTheta':0.5,'IextInPDE':1})
    #startJobs(lL,tL,nL,'IE',{'CrankNicolsonTheta':1.0,'IextInPDE':1})
    
    lL=[5]
    tL=[2,4,5]
    nL=['GPDE']
    #startJobs(lL,tL,nL,'EE',{'CrankNicolsonTheta':0.0,'IextInPDE':1})
    #startJobs(lL,tL,nL,'CN',{'CrankNicolsonTheta':0.5,'IextInPDE':1})
    startJobs(lL,tL,nL,'IE',{'CrankNicolsonTheta':1.0,'IextInPDE':1})
def startMoreGPDE():
    lL=[3]
    tL=[1,2,3,4,5,6,7]
    nL=['GPDE']
    startJobs(lL,tL,nL,'IENew',{'CrankNicolsonTheta':1.0,'IextInPDE':1})
def strangSplitting():
    lL=[5]
    tL=[3,4,5]
    nL=['S']
    startJobs(lL,tL,nL,'Exponential',{'CellModelScheme':'ExponentialIntegrator','IextInPDE':1})
    startJobs(lL,tL,nL,'RK2',{'CellModelScheme':'RungeKutta2','IextInPDE':1})
    startJobs(lL,tL,nL,'RK4',{'CellModelScheme':'RungeKutta4','IextInPDE':1})
def implicitStaggered():
    lL=[3]
    tL=[4,5]
    nL=['IE']#,'LI']
    startJobs(lL,tL,nL,'eps8',{'NewtonEpsilon' : '1e-8'})
    tL=[3,4,5]
    startJobs(lL,tL,nL,'eps10',{'NewtonEpsilon' : '1e-10'})
    startJobs(lL,tL,nL,'eps12',{'NewtonEpsilon' : '1e-12'})
def startMoreSI():
    lL=[5]
    tL=[6]
    nL=['SI']
    startJobs(lL,tL,nL,'')
def restartLevel2():
    lL=[2]
    tL=[4]
    nL=['IE','LI','SI','GPDE']
    startJobs(lL,tL,nL,'',{'CrankNicolsonTheta':1.0,'IextInPDE':1})
    
def startIE():
    lL=[3]
    tL =[3,4,5,6,7]
    nL=['IE']
    startJobs(lL,tL,nL,'')
    lL=[4]
    tL=[3,4,5]
    startJobs(lL,tL,nL,'')
    lL=[2,5]
    tL=[4]
    startJobs(lL,tL,nL,'')
def startSIWA():
    lL=[0,1,2,3,4]
    tL =[4]
    nL=['SI']
    startJobs(lL,tL,nL,'ActivationTest',{'ElphyProblem':'EllipsoidWAProblem','EndTime':0.05})
    
if __name__=="__main__":
    
    #startSplittingVerisons()
    #splittingTimeIntegration()
    #strangSplitting()
    #startStein()
    #startICI()
    #implicitStaggered()
    #startMoreSI()
    #startMoreGPDE()
    #restartLevel2()
    #startIE()
    startSIWA()
    
    
    
    
    
    

    
    
