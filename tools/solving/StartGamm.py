#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 25 15:24:04 2023

@author: laura
"""

import sys
sys.path.append('../../')
import os
import mpp.python.mppy as mppy

#Plots ausschalten

rossiViscosity = [5000, 5500, 6000, 6500, 7000, 7500, 8000]
rossiForce = [5, 6, 7, 8, 9]


path='../../../data/preparationGamm/Debugging/'
pathToMakeFolders=path #'../'+path


def startExperiment(procs,startconf,pE,added=None):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='M++',kernels=procs,mute=False)
    
    for rV in rossiViscosity:
        for rF in rossiForce:
            
                    file = path + pE + 'log_RossiVisc_' + str(rV) + '_RossiForce_'+ str(rF)
                    kwargs={"RossiViscosity": rV, "RossiForce": rF, "logfile": file}
                    if added!=None:
                        kwargs.update(added)
                    mpp.run(procs, config=startconf, kwargs=kwargs) 

def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=64
    
    
    startconf="coupled/gamm-paper/coarse-biventricle"
    pathExperiment=""
    startExperiment(procs,startconf,pathExperiment)
    
    
