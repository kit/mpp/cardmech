import os

#pathToSave='../../data/Kopplung/FullTest/'
pathToSave='../../data/Kopplung/TimeStepTest/'
dataPath='../'+pathToSave

#dataPath='/lsdf/kit/ianm/projects/mppci/cardmech/monodomain/data/BiVentricle/'
def checkIfFolderExists(path):
    if not os.path.exists(path):
        os.makedirs(path)
        
        
def getModelByName(name):
    model =''
    if name =='LI':
        model = 'LinearImplicit'
    elif name =='SI':
        model='SemiImplicit'
    elif name =='IE':
        model ='ImplictEuler'
    elif name =='SIOC':
        model='SemiImplicitOnCells'
    elif name =='SIOCSVI':
        model='SemiImplicitOnCellsSVI'
    elif name =='SIOCCI':
        model=='SemiImplicitOnCellsIextPerCell'
    elif name =='SIICI':
        model='SemiImplicitNodes'
    else:
        model = 'Splitting'
    return model
def getSplittingMethodByName(name):
    if name[0]=='S':
        return 'Strang'
    elif name[0]=='B':
        return 'Stein'
    else:
        return 'Godunov'
def Standard():
    block=''
    block +='#!/bin/bash\n'
    block += '#SBATCH --partition=cpuonly\n'
    block += '#SBATCH --account=hk-project-ipfem2\n'
    block += '#SBATCH --export=ALL,EXECUTABLE=./M++\n'
    block += '#SBATCH --mail-type=ALL\n'
    block += '#SBATCH --mail-user=laura.lindner2@kit.edu\n'
    return block

def Jobname(name):#name could be SIl3j4
    return '#SBATCH --job-name='+name +'\n'

def Nodes(n):
    return '#SBATCH --nodes='+str(n)+'\n'
def Tasks(n):
    return '#SBATCH --ntasks-per-node='+str(n)+'\n'
def Time(h,m):
    if m==0:
        return '#SBATCH --time='+str(h)+':00:00\n'
    elif m<10:
        return '#SBATCH --time='+str(h)+':0'+str(m)+':00\n'
    else:
        return '#SBATCH --time='+str(h)+':'+str(m)+':00\n'


def Start(args):
    block=''
    block +='module load compiler/gnu/11 mpi/openmpi/4.1 devel/cmake/3.26\n'
    block += 'mpirun ${MPIRUN_OPTIONS} M++ coupled/electroMechConHoreka'
    for a in args:
        block += ' '+a+'='+str(args[a])
    block+='\n'
    return block

def decideTime(level,j):
    h=0
    m=0
    if level <=3:
        h=2**(j+1)+4
        
    elif level==4:
        h=2**(j+2)+4
       
    else:
        print('no level time defined')
    #print(h,m)
    #h=h*4
    #m=4*m
    if m>60:
        h=h+int(m/60)
        m=m%60
    return(h,m)

def modifyJobskript(name,sdtN,j,sdt,args,add=0):
    level=args['ElphyLevel']
    filename =name+'l'+str(level)+'Job'
    (h,m)=decideTime(level,j)
    with open(filename, 'w') as output_file:
        output_file.write(Standard())
        output_file.write(Nodes(2**(level+add)))#2**(level+add-1))) #
        output_file.write(Tasks(64))
        output_file.write(Time(h,m))
        output_file.write(Jobname(name+'l'+str(args['ElphyLevel'])+'j'+str(j)))
        #output_file.write('module load compiler/gnu/8 mpi/openmpi/4.1\n')
        output_file.write('export MPIRUN_OPTIONS="--bind-to core --map-by core -report-bindings"\n')
        output_file.write('echo "${executable} running on ${SLURM_NTASKS_PER_NODE} tasks per node with ${SLURM_NNODES} nodes"\n')
        if 'MechDeltaTime'in args:
            args.update({'MechDeltaTime':args['MechDeltaTime']})
        else:
            args.update({'MechDeltaTime':sdt*2**(-j)})
        args.update({'ElphyDeltaTime':sdt*2**(-j)})
        output_file.write(Start(args))
        #if args['ElphyProblem']=='BiVentricleWAProblem':
            #output_file.write(moveActivation(level,j,name,sdtN))
    return name+'l'+str(args['ElphyLevel'])+'Job'


def pythonExtractAndMoveDataFromVTU(alg,l,j):
    return 'python ../tools/solving/ExtractAndMoveATData.py '+ str(alg)+' '+str(l)+' ' +str(j)
def moveActivation(l,j,alg,sdtName):
    block =''
    block+='mv data/vtu/ActivationTime.vtu'+dataPath+str(alg)+sdtName+'/' +'AT_l'+str(l)+'j'+str(j)+'.vtu'
    #block+='mv data/vtu/ActivationTime.vtu ../../data/BiVentricle/'+str(alg)+'0001/' +'AT_l'+str(l)+'j'+str(j)+'.vtu'
    block+='\n'
    return block
def startJobs(lL,tL,nL,sdt,testname,added=None): #testname must end with/!!!!!!
    arguments={}
    sdtName=str(sdt)
    sdtName=sdtName[2:]
    for alg in nL:
        if added!=None:
            if 'ElphyModel' in added:
                model =added['ElphyModel']
            else:
                model =getModelByName(alg)
        else:
            model =getModelByName(alg)
        arguments.update({'ElphyModel':model})
        checkIfFolderExists(pathToSave+alg+sdtName+'/'+testname)
        if model =='Splitting':
            arguments.update({'ElphySplittingMethod':getSplittingMethodByName(alg)})
        
        for l in lL:
            arguments.update({'ElphyLevel':l})
            for j in tL:
                logname=dataPath+alg+sdtName+'/'+testname+'log_l'+str(l)+'j'+str(j)+'m1'
                arguments.update({'logfile':logname})
                if added!=None:
                    arguments.update(added)
                if l>=4:
                    jobname =modifyJobskript(alg,sdtName,j,sdt,arguments,2)
                else:
                    jobname =modifyJobskript(alg,sdtName,j,sdt,arguments,1)
               
                os.system('sbatch '+ jobname)
        

def startSIOC():
    #lL=[3,4]
    tL=[2,3]
    lL=[2,3]
    lL=[1,2]
    tL=[2,3]
    nL=['SIOC']
    sdt=0.0004
    
    #startJobs(lL,tL,nL,sdt,'',{'Mesh':'TestBiventricle','ElphyProblem':'DeformedBiventricleCoarse','MechLevel':1})
    lL=[1,2,3]
    tL=[2]
    startJobs(lL,tL,nL,sdt,'TestAusgaben/',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.5})
    
    #startJobs(lL,tL,nL,sdt,'TestDifferentMeshes/',{'Mesh':'TestBiventricle','ElphyProblem':'DeformedBiventricleCoarse','MechLevel':1,'EndTime':0.1})
    #startJobs(lL,tL,nL,sdt,'LargerNewtonTest/',{'LinearEpsilon':'1e-3','LinearReduction':'1e-4','ElphyEpsilon':'1e-3','ElphyReduction':'1e-4','MechEpsilon':'1e-4','MechReduction':'1e-5','NewtonEpsilon':'1e-4','NewtonReduction':'1e-5','NewtonLinearizationReduction':'1e-3','Mesh':'TestBiventricle','ElphyProblem':'DeformedBiventricleCoarse'})
    
    lL=[1,2,3]
    tL=[2,3]
    #startJobs(lL,tL,nL,sdt,'SE/',{'Mesh':'TestBiventricleSimpleExcitation_smooth','MechLevel':1,'EndTime':0.5})

def startSI():
    lL=[1]
    tL=[2]
    nL=['SI']
    sdt=0.0004
    startJobs(lL,tL,nL,sdt,'',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.5})

def startGS():
    lL=[1]
    tL=[2]
    nL=['GS']
    sdt=0.0004
    #startJobs(lL,tL,nL,sdt,'',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.5})
    startJobs(lL,tL,nL,sdt,'theta1/',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.5, 'CrankNicolsonTheta':1.0})

def startAll():
    lL=[1,2,3,4]
    tL=[0,1,2,3,4]    
    sdt=0.0004
    #nL=['SI','GS']
    nL=['GS']
    lL=[3]
    tL=[2]
    #startJobs(lL,tL,nL,sdt,'',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.6,'CrankNicolsonTheta':1.0})
    lL=[4]
    tL=[2,3,4]
    #startJobs(lL,tL,nL,sdt,'',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.6,'CrankNicolsonTheta':1.0})
    lL=[4]
    tL=[1]
    nL=['SI']
    startJobs(lL,tL,nL,sdt,'',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.6})
    
    #startJobs(lL,tL,['GS'],sdt,'theta1/',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.5, 'CrankNicolsonTheta':1.0})

def startAdaptiveTimeStep():
    lL=[2]
    tL=[0,1,2]
    nL=['SI']
    sdt=0.0004
    startJobs(lL,tL,nL,sdt,'adaptive16/new/',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.6,'MechDeltaTime':0.0016,'AdaptiveTimeStepForMech':'true','MaximalGammaInf':0.3,'FactorMaximalGammaInf':0.8})
    startJobs(lL,tL,nL,sdt,'adaptive32/new/',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.6,'MechDeltaTime':0.0032,'AdaptiveTimeStepForMech':'true','MaximalGammaInf':0.3,'FactorMaximalGammaInf':0.8})
    
def startFixedMechTimeStep():
    lL=[2]
    tL=[0,1,2]
    nL=['SI']
    sdt=0.0004
    #startJobs(lL,tL,nL,sdt,'Mech00016/',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.6,'MechDeltaTime':0.0016})
    tL=[0,1]
    startJobs(lL,tL,nL,sdt,'Mech00032/',{'Mesh':'TestBiventricleSimpleExcitation_smooth','ElphyProblem':'DeformedBiventricleCoarse','EndTime':0.6,'MechDeltaTime':0.0032})
    
if __name__=="__main__":
    #startSIOC()
    #startSI()
    #startGS()
    #startAll()
    #startFixedMechTimeStep()
    startAdaptiveTimeStep()

    
    
    
    
    

    
    
