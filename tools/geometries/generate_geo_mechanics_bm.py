import math
import numpy as np
from pprint import pprint

geopath = "../../geo/"
datapath = "../cardiacmechanics/data/"

'''Parameters for Cuboid'''
x_length = 10
y_length = 1
z_length = 1

'''Parameters for Ellipsoid'''
circular_points = 16  # Refinement of circle. Gets doubled in construction.
n_layers = 1  # Amount of layers from endo- to epicard

refinement = 0


def cube_to_tetras(orientation, *p):
    tetras = [None] * 5
    if orientation % 2:
        tetras[0] = "4 0000 " + p[0] + p[3] + p[4] + p[1] + "\n"
        tetras[1] = "4 0000 " + p[5] + p[4] + p[6] + p[1] + "\n"
        tetras[2] = "4 0000 " + p[2] + p[1] + p[3] + p[6] + "\n"
        tetras[3] = "4 0000 " + p[7] + p[6] + p[4] + p[3] + "\n"
        tetras[4] = "4 0000 " + p[1] + p[3] + p[6] + p[4] + "\n"
    else:
        tetras[0] = "4 0000 " + p[0] + p[1] + p[2] + p[5] + "\n"
        tetras[1] = "4 0000 " + p[0] + p[2] + p[3] + p[7] + "\n"
        tetras[2] = "4 0000 " + p[0] + p[5] + p[4] + p[7] + "\n"
        tetras[3] = "4 0000 " + p[2] + p[6] + p[5] + p[7] + "\n"
        tetras[4] = "4 0000 " + p[0] + p[2] + p[7] + p[5] + "\n"
    return tetras


def prism_to_tetras(orientation, *p):
    tetras = [None] * 3
    if orientation % 2:
        tetras[0] = "4 0000 " + p[0] + p[1] + p[5] + p[3] + "\n"
        tetras[1] = "4 0000 " + p[5] + p[4] + p[3] + p[0] + "\n"
        tetras[2] = "4 0000 " + p[3] + p[4] + p[2] + p[0] + "\n"
    else:
        tetras[0] = "4 0000 " + p[0] + p[1] + p[5] + p[3] + "\n"
        tetras[1] = "4 0000 " + p[3] + p[5] + p[2] + p[0] + "\n"
        tetras[2] = "4 0000 " + p[2] + p[5] + p[4] + p[0] + "\n"
    return tetras


def square_to_triangles(orientation, bd, *p):
    tri = [None] * 2
    if orientation % 2:
        tri[0] = bd + p[0] + p[1] + p[3] + "\n"
        tri[1] = bd + p[1] + p[2] + p[3] + "\n"
    else:
        tri[0] = bd + p[0] + p[1] + p[2] + "\n"
        tri[1] = bd + p[0] + p[2] + p[3] + "\n"
    return tri


def orient_triangle(orientation, bd, *p):
    if orientation % 2:
        tri = bd + p[0] + p[1] + p[2] + "\n"
    else:
        tri = bd + p[0] + p[2] + p[1] + "\n"
    return tri


def pressure_beam_tet(delta, x, y, z):
    with open(geopath + "tetrahedral/PressureBeam.geo", 'w') as output_file:
        D = int(1 / delta)

        points = {}
        pointsfloat = {}
        cells = {}

        # Points
        active = 0
        n = 0
        output_file.write("POINTS:\n")

        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                for k in range(0, z * D + 1):
                    output_file.write(
                        '{0:.4f}'.format(i * delta) + " " + '{0:.4f}'.format(j * delta) + " " + '{0:.4f}'.format(
                            k * delta) + "\n")
                    points[(i, j, k)] = " " + str(n)
                    pointsfloat[n] = (i, j, k)
                    n += 1

        # Cells
        m = 0
        output_file.write("CELLS:\n")
        for i in range(0, x * D):
            for j in range(0, y * D):
                for k in range(0, z * D):
                    tetras = cube_to_tetras(i + j + k, points[(i, j, k)], points[(i, j + 1, k)],
                                            points[(i + 1, j + 1, k)],
                                            points[(i + 1, j, k)], points[(i, j, k + 1)], points[(i, j + 1, k + 1)],
                                            points[(i + 1, j + 1, k + 1)], points[(i + 1, j, k + 1)])

                    for t in range(5):
                        output_file.write(tetras[t])
                        cells[m + t] = (i, j, k, t)

                    m += 5

        # Faces
        output_file.write("FACES:\n")
        ''' Insert Dirichlet Boundary'''
        for j in range(y * D):
            for k in range(z * D):
                tri = square_to_triangles(j + k, "3 199", points[(0, j, k)], points[(0, j + 1, k)],
                                          points[(0, j + 1, k + 1)], points[(0, j, k + 1)])

                for t in range(2):
                    output_file.write(tri[t])

        '''Insert Pressure Boundary on Bottom'''
        for i in range(x * D):
            for j in range(y * D):
                tri = square_to_triangles(j + i, "3 230", points[(i + 1, j + 1, 0)], points[(i, j + 1, 0)],
                                          points[(i, j, 0)], points[(i + 1, j, 0)])

                for t in range(2):
                    output_file.write(tri[t])

        # ORIENTATION
        output_file.write("VDATA:\n")
        for i in sorted(pointsfloat):
            output_file.write("1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -1.0 0.0 0.0\n")

    with open(datapath + "BeamTrack.dat", 'w') as output_file:

        for i in range(0, x * D + 1):
            output_file.write(
                '{0:.4f}'.format(i * delta) + " " +
                '{0:.4f}'.format(0.5) + " " +
                '{0:.4f}'.format(0.5) + "\n")


def pressure_beam_hex(delta, x, y, z):
    with open(geopath + "hexahedral/PressureBeam.geo", 'w') as output_file:
        D = int(1 / delta)

        points = {}
        pointsfloat = {}
        cells = {}

        # Points
        active = 0
        n = 0
        output_file.write("POINTS:\n")

        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                for k in range(0, z * D + 1):
                    output_file.write(
                        '{0:.4f}'.format(i * delta) + " " + '{0:.4f}'.format(j * delta) + " " + '{0:.4f}'.format(
                            k * delta) + "\n")
                    points[(i, j, k)] = " " + str(n)
                    pointsfloat[n] = (i, j, k)
                    n += 1

        # Cells
        m = 0
        output_file.write("CELLS:\n")
        for i in range(0, x * D):
            for j in range(0, y * D):
                for k in range(0, z * D):
                    output_file.write("8 0000" + points[(i, j, k)] + points[(i, j + 1, k)] +
                                      points[(i + 1, j + 1, k)] + points[(i + 1, j, k)] +
                                      points[(i, j, k + 1)] + points[(i, j + 1, k + 1)] +
                                      points[(i + 1, j + 1, k + 1)] + points[(i + 1, j, k + 1)] + "\n")

                    m += 1

        # Faces
        output_file.write("FACES:\n")
        ''' Insert Dirichlet Boundary'''
        for j in range(y * D):
            for k in range(z * D):
                output_file.write("4 199" + points[(0, j, k)] + points[(0, j + 1, k)] +
                                  points[(0, j + 1, k + 1)] + points[(0, j, k + 1)] + "\n")

        '''Insert Pressure Boundary on Bottom'''
        for i in range(x * D):
            for j in range(y * D):
                output_file.write("4 230" + points[(i + 1, j + 1, 0)] + points[(i, j + 1, 0)] +
                                  points[(i, j, 0)] + points[(i + 1, j, 0)] + "\n")

        # ORIENTATION
        output_file.write("VDATA:\n")
        for i in sorted(pointsfloat):
            output_file.write("1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -1.0 0.0 0.0\n")


def scale_array(begin, end, npoints):
    scaled_a = np.sin(np.linspace(0, math.pi / 4, npoints))
    scaled_a = scaled_a / max(scaled_a) * (-begin + end) + begin
    # scaled_a = scaled_a[::-1]
    return scaled_a


def pressure_ellipsoid(delta, smoothness_circle, layers, apex_z, base_z, thickness):
    '''Ensures correct orientation of tetrahedrons'''
    D = int(1 / delta)
    layers = layers * D + D
    smoothness_circle *= 2 * D
    height = abs(base_z - apex_z) * D

    rl_epi = abs(apex_z)
    rl_endo = rl_epi - thickness

    rs_epi = rl_epi / 2
    rs_endo = rs_epi - thickness

    rl = np.linspace(rl_endo, rl_epi, layers + 1)
    rs = np.linspace(rs_endo, rs_epi, layers + 1)

    u = np.concatenate(
        [scale_array(-math.pi, -math.acos(base_z / rl[l]), height + 1) for l in range(layers + 1)]).reshape(layers + 1,
                                                                                                            height + 1)
    v = np.linspace(-math.pi, math.pi, smoothness_circle, endpoint=False)

    with open(geopath + "tetrahedral/PressureEllipsoid.geo", 'w') as output_file:

        def write_ellipsoid_point(j, k, l, lpoints, ln):
            lpoints[(l, j, k)] = " " + str(ln)
            output_file.write(
                '{0:.4f}'.format(rs[l] * math.sin(u[l][k]) * math.cos(v[j])) + " " +
                '{0:.4f}'.format(rs[l] * math.sin(u[l][k]) * math.sin(v[j])) + " " +
                '{0:.4f}'.format(rl[l] * math.cos(u[l][k])) + "\n")
            return 1

        '''Construct outer points'''
        output_file.write("POINTS:\n")
        n = 0
        points = {}
        for l in range(layers + 1):
            n = n + write_ellipsoid_point(0, 0, l, points, n)
            for k in range(height):
                for j in range(smoothness_circle):
                    n += write_ellipsoid_point(j, k + 1, l, points, n)

        # Cells
        m = 0
        cells = {}
        output_file.write("CELLS:\n")

        '''Prisms which arise from collapsing at center'''
        for l in range(layers):
            for j in range(smoothness_circle):
                tetras = prism_to_tetras(
                    l + j + k + 1, points[(l, 0, 0)], points[(l + 1, 0, 0)], points[(l, j, 1)],
                    points[(l + 1, j, 1)], points[(l, (j + 1) % smoothness_circle, 1)],
                    points[(l + 1, (j + 1) % smoothness_circle, 1)])

                for t in range(3):
                    output_file.write(tetras[t])
                    cells[m + t] = (l, j, k, t)

                m += 3

                '''Hexahedrons arising from the regular mesh'''
                for k in range(1, height):
                    tetras = cube_to_tetras(
                        l + j + k, points[(l, j, k)], points[(l, (j + 1) % smoothness_circle, k)],
                        points[(l + 1, (j + 1) % smoothness_circle, k)], points[(l + 1, j, k)],
                        points[(l, j, k + 1)], points[(l, (j + 1) % smoothness_circle, k + 1)],
                        points[(l + 1, (j + 1) % smoothness_circle, k + 1)], points[(l + 1, j, k + 1)])

                    for t in range(5):
                        output_file.write(tetras[t])
                        cells[m + t] = (l, j, k, t)

                    m += 5

        # Faces
        output_file.write("FACES:\n")
        ''' Insert Dirichlet Boundary'''
        ntris = 0
        for l in range(layers):
            for j in range(smoothness_circle):
                tri = square_to_triangles(l + j + k, "3 199", points[(l, j, height)], points[(l + 1, j, height)],
                                          points[(l + 1, (j + 1) % smoothness_circle, height)],
                                          points[(l + 1, (j + 1) % smoothness_circle, height)])

                for t in range(2):
                    output_file.write(tri[t])
                    ntris += 1

        '''Insert Pressure on inner layer'''
        for j in range(smoothness_circle):
            output_file.write(orient_triangle(j + k, "3 230", points[(0, 0, 0)],
                                              points[(0, (j + 1) % smoothness_circle, 1)], points[(0, j, 1)]))
            for k in range(1, height):

                tri = square_to_triangles(j + k, "3 230", points[(0, j, k)],
                                          points[(0, (j + 1) % smoothness_circle, k)],
                                          points[(0, (j + 1) % smoothness_circle, k + 1)], points[(0, j, k + 1)])

                for t in range(2):
                    output_file.write(tri[t])

        # ORIENTATION
        output_file.write("VDATA:\n")
        for i in range(n):
            output_file.write("1.0 0.0 0.0 0.0 0.0 1.0 -1.0 0.0 0.0\n")

    with open(datapath + "EllipsoidWall.dat", 'w') as output_file:
        for k in range(height + 1):
            output_file.write(
                '{0:.4f}'.format(rs[0] * math.sin(u[0][k]) * math.cos(v[0])) + " " +
                '{0:.4f}'.format(rs[0] * math.sin(u[0][k]) * math.sin(v[0])) + " " +
                '{0:.4f}'.format(rl[0] * math.cos(u[0][k])) + " " +
                '{0:.4f}'.format(rs[layers] * math.sin(u[layers][k]) * math.cos(v[0])) + " " +
                '{0:.4f}'.format(rs[layers] * math.sin(u[layers][k]) * math.sin(v[0])) + " " +
                '{0:.4f}'.format(rl[layers] * math.cos(u[layers][k])) + "\n")

    with open(datapath + "EllipsoidTrack.dat", 'w') as output_file:
        l = math.ceil(layers)
        for k in range(height + 1):
            output_file.write(
                '{0:.4f}'.format(rs[l] * math.sin(u[l][k]) * math.cos(v[0])) + " " +
                '{0:.4f}'.format(rs[l] * math.sin(u[l][k]) * math.sin(v[0])) + " " +
                '{0:.4f}'.format(rl[l] * math.cos(u[0][k])) + "\n")


def fibre_parametrization(rs, rl, u, v, a):
    dxdu = np.array([rs * math.cos(u) * math.cos(v), rs * math.cos(u) * math.sin(v), -rl * math.sin(u)])
    dxdv = np.array([-rs * math.sin(u) * math.sin(v), rs * math.sin(u) * math.cos(v), 0])

    return dxdu / np.linalg.norm(dxdu) * math.sin(a) + dxdv / np.linalg.norm(dxdv) * math.cos(a)


def normal_parametrization(rs, rl, u, v):
    dxdu = np.array([rs * math.cos(u) * math.cos(v), rs * math.cos(u) * math.sin(v), -rl * math.sin(u)])
    dxdv = np.array([-rs * math.sin(u) * math.sin(v), rs * math.sin(u) * math.cos(v), 0])

    dxdn = np.cross(dxdu, dxdv)
    return np.sign(dxdn * dxdn) * dxdn


def oriented_ellipsoid(delta, smoothness_circle, layers, apex_z, base_z, thickness):
    '''Ensures correct orientation of tetrahedrons'''
    D = int(1 / delta)
    layers = layers * D + D
    smoothness_circle *= 2 * D
    height = abs(base_z - apex_z) * D

    rl_epi = abs(apex_z)
    rl_endo = rl_epi - thickness

    rs_epi = rl_epi / 2
    rs_endo = rs_epi - thickness

    rl = np.linspace(rl_endo, rl_epi, layers + 1)
    rs = np.linspace(rs_endo, rs_epi, layers + 1)
    alpha = np.linspace(math.pi/2, -math.pi/2, layers + 1)

    u = np.concatenate(
        [scale_array(-math.pi, -math.acos(base_z / rl[l]), height + 1) for l in range(layers + 1)]).reshape(layers + 1,
                                                                                                            height + 1)
    print(layers + 1)
    print(height + 1)
    print(scale_array(-math.pi, -math.acos(base_z / rl[0]), height + 1))
    print(scale_array(-math.pi, -math.acos(base_z / rl[1]), height + 1))
    print(u)
    v = np.linspace(-math.pi, math.pi, smoothness_circle, endpoint=False)

    with open(geopath + "tetrahedral/OrientedEllipsoid.geo", 'w') as output_file:

        def write_ellipsoid_point(j, k, l, lpoints, ln):
            lpoints[(l, j, k)] = " " + str(ln)
            output_file.write(
                '{0:.4f}'.format(rs[l] * math.sin(u[l][k]) * math.cos(v[j])) + " " +
                '{0:.4f}'.format(rs[l] * math.sin(u[l][k]) * math.sin(v[j])) + " " +
                '{0:.4f}'.format(rl[l] * math.cos(u[l][k])) + "\n")
            return 1

        '''Construct outer points'''
        output_file.write("POINTS:\n")
        n = 0
        points = {}
        for l in range(layers + 1):
            n = n + write_ellipsoid_point(0, 0, l, points, n)
            for k in range(height):
                for j in range(smoothness_circle):
                    n += write_ellipsoid_point(j, k + 1, l, points, n)

        # Cells
        m = 0
        cells = {}
        output_file.write("CELLS:\n")
        tetcount = [0,0]
        '''Prisms which arise from collapsing at center'''
        for l in range(layers):
            for j in range(smoothness_circle):
                tetras = prism_to_tetras(
                    l + j + k + 1, points[(l, 0, 0)], points[(l + 1, 0, 0)], points[(l, j, 1)],
                    points[(l + 1, j, 1)], points[(l, (j + 1) % smoothness_circle, 1)],
                    points[(l + 1, (j + 1) % smoothness_circle, 1)])

                for t in range(3):
                    output_file.write(tetras[t])
                    cells[m + t] = (l, j, k, t)

                m += 3
                tetcount[0] += 3
                '''Hexahedrons arising from the regular mesh'''
                for k in range(1, height):
                    tetras = cube_to_tetras(
                        l + j + k, points[(l, j, k)], points[(l, (j + 1) % smoothness_circle, k)],
                        points[(l + 1, (j + 1) % smoothness_circle, k)], points[(l + 1, j, k)],
                        points[(l, j, k + 1)], points[(l, (j + 1) % smoothness_circle, k + 1)],
                        points[(l + 1, (j + 1) % smoothness_circle, k + 1)], points[(l + 1, j, k + 1)])

                    for t in range(5):
                        output_file.write(tetras[t])
                        cells[m + t] = (l, j, k, t)

                    m += 5
                    tetcount[1] += 5
        # Faces
        output_file.write("FACES:\n")


        '''Insert Pressure on inner layer'''
        for j in range(smoothness_circle):
            output_file.write(orient_triangle(j + k, "3 230", points[(0, 0, 0)],
                                              points[(0, (j + 1) % smoothness_circle, 1)], points[(0, j, 1)]))
            for k in range(1, height):

                tri = square_to_triangles(j + k, "3 230", points[(0, j, k)],
                                          points[(0, (j + 1) % smoothness_circle, k)],
                                          points[(0, (j + 1) % smoothness_circle, k + 1)], points[(0, j, k + 1)])

                for t in range(2):
                    output_file.write(tri[t])

        '''Insert Pressure on outer layer'''
        for j in range(smoothness_circle):
            output_file.write(orient_triangle(j + k, "3 330", points[(layers, 0, 0)],
                                              points[(layers, (j + 1) % smoothness_circle, 1)], points[(layers, j, 1)]))
            for k in range(1, height):

                tri2 = square_to_triangles(j + k, "3 330", points[(layers, j, k)],
                                          points[(layers, (j + 1) % smoothness_circle, k)],
                                          points[(layers, (j + 1) % smoothness_circle, k + 1)], points[(layers, j, k + 1)])

                for t in range(2):
                    output_file.write(tri2[t])


                ''' Insert Dirichlet Boundary'''
        ntris = 0
        for l in range(layers):
            for j in range(smoothness_circle):
                tri = square_to_triangles(l + j + k, "3 199", points[(l, j, height)], points[(l + 1, j, height)],
                                          points[(l, (j + 1) % smoothness_circle, height)],
                                          points[(l+1, (j + 1) % smoothness_circle, height)])

                for t in range(2):
                    output_file.write(tri[t])
                    ntris += 1

        # VDATA
        output_file.write("VDATA:\n")
        for t in range(n):
            output_file.write("3 -10.0 0.0 0.0\n")
        # ORIENTATION
        output_file.write("CDATA:\n")

        #for _ in range(m):
        #    output_file.write("0.0\n")

        if layers % 2 == 1:
            raise Exception("Take care of indices of layers! use odd number in n_layers")
        for l in range(layers +1):
            if 2*l == layers:
                continue
            for j in range(smoothness_circle):
                for t in range(3):
                    f = fibre_parametrization(rs[l], rl[l], u[l][k + 1], v[j], alpha[l])
                    n = normal_parametrization(rs[l], rl[l], u[l][k + 1], v[j])
                    ## Is this right?
                    output_file.write("9 " +
                                      str(f[0]) + " " + str(f[1]) + " " + str(f[2]) + " " +
                                      str(n[0]) + " " + str(n[1]) + " " + str(n[2]) + " -1.0 0.0 0.0\n")
                    #output_file.write("9 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0\n")
                '''Hexahedrons arising from the regular mesh'''
                for k in range(1, height):
                    f = fibre_parametrization(rs[l], rl[l], u[l][k + 1], v[j], alpha[l])
                    n = normal_parametrization(rs[l], rl[l], u[l][k + 1], v[j])
                    for t in range(5):
                        output_file.write("9 " +
                            str(f[0]) + " " + str(f[1]) + " " + str(f[2]) + " " +
                            str(n[0]) + " " + str(n[1]) + " " + str(n[2]) + " -1.0 0.0 0.0\n")

        #pprint(locals())
        return

        outcount = 0
        for l in range(layers + 1):
            for k in range(height):
                for j in range(smoothness_circle):
                    f = fibre_parametrization(rs[l], rl[l], u[l][k + 1], v[j], alpha[l])
                    n = normal_parametrization(rs[l], rl[l], u[l][k + 1], v[j])
                    outcount += 1
                    output_file.write(
                        str(f[0]) + " " + str(f[1]) + " " + str(f[2]) + " " +
                        str(n[0]) + " " + str(n[1]) + " " + str(n[2]) + " -1.0 0.0 0.0\n")




def unoriented_ellipsoid(delta, smoothness_circle, layers, apex_z, base_z, thickness):
    '''Ensures correct orientation of tetrahedrons'''
    D = int(1 / delta)
    layers = layers * D + D
    print(layers)
    smoothness_circle *= 2 * D
    height = abs(base_z - apex_z) * D

    rl_epi = abs(apex_z)
    rl_endo = rl_epi - thickness

    rs_epi = rl_epi / 2
    rs_endo = rs_epi - thickness

    rl = np.linspace(rl_endo, rl_epi, layers + 1)
    rs = np.linspace(rs_endo, rs_epi, layers + 1)
    alpha = np.linspace(math.pi/2, -math.pi/2, layers + 1)

    u = np.concatenate(
        [scale_array(-math.pi, -math.acos(base_z / rl[l]), height + 1) for l in range(layers + 1)]).reshape(layers + 1,
                                                                                                            height + 1)
    v = np.linspace(-math.pi, math.pi, smoothness_circle, endpoint=False)

    with open(geopath + "tetrahedral/UnorientedEllipsoid.geo", 'w') as output_file:

        def write_ellipsoid_point(j, k, l, lpoints, ln):
            lpoints[(l, j, k)] = " " + str(ln)
            output_file.write(
                '{0:.4f}'.format(rs[l] * math.sin(u[l][k]) * math.cos(v[j])) + " " +
                '{0:.4f}'.format(rs[l] * math.sin(u[l][k]) * math.sin(v[j])) + " " +
                '{0:.4f}'.format(rl[l] * math.cos(u[l][k])) + "\n")
            return 1

        '''Construct outer points'''
        output_file.write("POINTS:\n")
        n = 0
        points = {}
        for l in range(layers + 1):
            n = n + write_ellipsoid_point(0, 0, l, points, n)
            for k in range(height):
                for j in range(smoothness_circle):
                    n += write_ellipsoid_point(j, k + 1, l, points, n)

        # Cells
        m = 0
        cells = {}
        output_file.write("CELLS:\n")
        tetcount = [0,0]
        '''Prisms which arise from collapsing at center'''
        for l in range(layers):
            for j in range(smoothness_circle):
                tetras = prism_to_tetras(
                    l + j + k + 1, points[(l, 0, 0)], points[(l + 1, 0, 0)], points[(l, j, 1)],
                    points[(l + 1, j, 1)], points[(l, (j + 1) % smoothness_circle, 1)],
                    points[(l + 1, (j + 1) % smoothness_circle, 1)])

                for t in range(3):
                    output_file.write(tetras[t])
                    cells[m + t] = (l, j, k, t)

                m += 3
                tetcount[0] += 3
                '''Hexahedrons arising from the regular mesh'''
                for k in range(1, height):
                    tetras = cube_to_tetras(
                        l + j + k, points[(l, j, k)], points[(l, (j + 1) % smoothness_circle, k)],
                        points[(l + 1, (j + 1) % smoothness_circle, k)], points[(l + 1, j, k)],
                        points[(l, j, k + 1)], points[(l, (j + 1) % smoothness_circle, k + 1)],
                        points[(l + 1, (j + 1) % smoothness_circle, k + 1)], points[(l + 1, j, k + 1)])

                    for t in range(5):
                        output_file.write(tetras[t])
                        cells[m + t] = (l, j, k, t)

                    m += 5
                    tetcount[1] += 5
        # Faces
        output_file.write("FACES:\n")


        '''Insert Pressure on inner layer'''
        for j in range(smoothness_circle):
            output_file.write(orient_triangle(j + k, "3 230", points[(0, 0, 0)],
                                              points[(0, (j + 1) % smoothness_circle, 1)], points[(0, j, 1)]))
            for k in range(1, height):

                tri = square_to_triangles(j + k, "3 230", points[(0, j, k)],
                                          points[(0, (j + 1) % smoothness_circle, k)],
                                          points[(0, (j + 1) % smoothness_circle, k + 1)], points[(0, j, k + 1)])

                for t in range(2):
                    output_file.write(tri[t])

        '''Insert Pressure on outer layer'''
        for j in range(smoothness_circle):
            output_file.write(orient_triangle(j + k, "3 330", points[(layers, 0, 0)],
                                              points[(layers, (j + 1) % smoothness_circle, 1)], points[(layers, j, 1)]))
            for k in range(1, height):

                tri2 = square_to_triangles(j + k, "3 330", points[(layers, j, k)],
                                           points[(layers, (j + 1) % smoothness_circle, k)],
                                           points[(layers, (j + 1) % smoothness_circle, k + 1)], points[(layers, j, k + 1)])

                for t in range(2):
                    output_file.write(tri2[t])


        ''' Insert Dirichlet Boundary'''
        ntris = 0
        for l in range(layers):
            for j in range(smoothness_circle):
                tri = square_to_triangles(l + j + k, "3 199", points[(l, j, height)], points[(l + 1, j, height)],
                                          points[(l, (j + 1) % smoothness_circle, height)],
                                          points[(l+1, (j + 1) % smoothness_circle, height)])
                for t in range(2):
                    output_file.write(tri[t])
                    ntris += 1

        # VDATA
        output_file.write("VDATA:\n")
        for t in range(n):
            output_file.write("3 -10.0 0.0 0.0\n")
        # ORIENTATION
        output_file.write("CDATA:\n")

        #for _ in range(m):
        #    output_file.write("0.0\n")

        if layers % 2 == 1:
            raise Exception("Take care of indices of layers! use odd number in n_layers")
        for l in range(layers +1):
            if 2*l == layers:
                continue
            for j in range(smoothness_circle):
                for t in range(3):
                    f = fibre_parametrization(rs[l], rl[l], u[l][k + 1], v[j], alpha[l])
                    n = normal_parametrization(rs[l], rl[l], u[l][k + 1], v[j])
                    output_file.write("9 0 0 0 0 0 0 -1.0 0.0 0.0\n")
                '''Hexahedrons arising from the regular mesh'''
                for k in range(1, height):
                    f = fibre_parametrization(rs[l], rl[l], u[l][k + 1], v[j], alpha[l])
                    n = normal_parametrization(rs[l], rl[l], u[l][k + 1], v[j])
                    for t in range(5):
                        output_file.write("9 0 0 0 0 0 0 -1.0 0.0 0.0\n")

if __name__ == "__main__":
    delta_b = 1 / pow(2, refinement + n_layers)
    delta_e = 1 / pow(2, refinement)
    #pressure_beam_tet(delta_b, x_length, y_length, z_length)
    #pressure_beam_hex(delta_b, x_length, y_length, z_length)
    #pressure_ellipsoid(delta_e, circular_points, n_layers, -20, 5, 3)
    oriented_ellipsoid(delta_e, circular_points, n_layers, -20, 5, 3)
    #unoriented_ellipsoid(delta_e, circular_points, n_layers, -20, 5, 3)
