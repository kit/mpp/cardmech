def cube_to_edges(*p):
    if len(p) != 8:
        print("WARNING: INCORRECT VERTEX COUNT!")

    edges = [None] * 15
    for i in range(len(p)):
        edges[i] = p[i]

    edges[8] = (p[0] + p[2]) / 2.0


def cube_to_tetras24(*p):
    tetras = [
        [p[14], p[8], p[0], p[1]],
        [p[14], p[8], p[1], p[2]],
        [p[14], p[8], p[2], p[3]],
        [p[14], p[8], p[3], p[0]],
        [p[14], p[9], p[0], p[1]],
        [p[14], p[9], p[1], p[5]],
        [p[14], p[9], p[5], p[4]],
        [p[14], p[9], p[4], p[0]],
        [p[14], p[10], p[1], p[2]],
        [p[14], p[10], p[2], p[6]],
        [p[14], p[10], p[6], p[5]],
        [p[14], p[10], p[5], p[1]],
        [p[14], p[11], p[2], p[3]],
        [p[14], p[11], p[3], p[7]],
        [p[14], p[11], p[7], p[6]],
        [p[14], p[11], p[6], p[2]],
        [p[14], p[12], p[3], p[0]],
        [p[14], p[12], p[0], p[4]],
        [p[14], p[12], p[4], p[7]],
        [p[14], p[12], p[7], p[3]],
        [p[14], p[13], p[4], p[5]],
        [p[14], p[13], p[5], p[6]],
        [p[14], p[13], p[6], p[7]],
        [p[14], p[13], p[7], p[4]]
    ]

    return tetras


def cube_to_tetras(*p):
    tetras = []
    tetras.append([p[0], p[1], p[5], p[6]])
    tetras.append([p[0], p[1], p[2], p[6]])
    tetras.append([p[0], p[3], p[2], p[6]])
    tetras.append([p[0], p[3], p[7], p[6]])
    tetras.append([p[0], p[4], p[7], p[6]])
    tetras.append([p[0], p[4], p[5], p[6]])
    return tetras


def prism_to_tetras(orientation, *p):
    tetras = []
    if orientation % 2:
        tetras.append([p[0], p[1], p[5], p[3]])
        tetras.append([p[5], p[4], p[3], p[0]])
        tetras.append([p[3], p[4], p[2], p[0]])
    else:
        tetras.append([p[0], p[1], p[5], p[3]])
        tetras.append([p[3], p[5], p[2], p[0]])
        tetras.append([p[2], p[5], p[4], p[0]])
    return tetras


def square_to_triangles(*p):
    tri = []
    tri.append([p[0], p[1], p[2]])
    tri.append([p[0], p[2], p[3]])
    return tri


def orient_triangle(orientation, *p):
    if orientation % 2:
        tri = [p[0], p[1], p[2]]
    else:
        tri = [p[0], p[2], p[1]]
    return tri


def square_to_triangles4(*p):
    tri = []
    tri.append([p[0], p[1], p[4]])
    tri.append([p[1], p[2], p[4]])
    tri.append([p[2], p[3], p[4]])
    tri.append([p[3], p[0], p[4]])
    return tri


def square_corners(points, i, j):
    return [points[(i, j)], points[(i + 1, j)], points[(i + 1, j + 1)], points[(i, j + 1)]]


def square_corners4(points, i, j):
    return [points[(i, j)], points[(i + 1, j)], points[(i + 1, j + 1)], points[(i, j + 1)], points[(i + 0.5, j + 0.5)]]


def cube_face_x(points, i, j, k):
    return [points[(i, j, k)], points[(i, j + 1, k)], points[(i, j + 1, k + 1)], points[(i, j, k + 1)]]


def cube_face_y(points, i, j, k):
    return [points[(i, j, k)], points[(i + 1, j, k)], points[(i + 1, j, k + 1)], points[(i, j, k + 1)]]


def cube_face_z(points, i, j, k):
    return [points[(i + 1, j + 1, k)], points[(i + 1, j, k)], points[(i, j, k)], points[(i, j + 1, k)]]


def cube_face4_x(points, i, j, k):
    return [points[(i, j, k)], points[(i, j + 1, k)], points[(i, j + 1, k + 1)], points[(i, j, k + 1)],
            points[(i, j + 0.5, k + 0.5)]]


def cube_face4_y(points, i, j, k):
    return [points[(i, j, k)], points[(i + 1, j, k)], points[(i + 1, j, k + 1)], points[(i, j, k + 1)],
            points[(i + 0.5, j, k + 0.5)]]


def cube_face4_z(points, i, j, k):
    return [points[(i + 1, j + 1, k)], points[(i + 1, j, k)], points[(i, j, k)], points[(i, j + 1, k)],
            points[(i + 0.5, j + 0.5, k)]]


def cube_corners(points, i, j, k):
    return [points[(i, j, k)], points[(i, j + 1, k)], points[(i + 1, j + 1, k)],
            points[(i + 1, j, k)], points[(i, j, k + 1)], points[(i, j + 1, k + 1)],
            points[(i + 1, j + 1, k + 1)], points[(i + 1, j, k + 1)]]


def cube_corners24(points, i, j, k):
    return [points[(i, j, k)], points[(i + 1, j, k)], points[(i + 1, j + 1, k)], points[(i, j + 1, k)],
            points[(i, j, k + 1)], points[(i + 1, j, k + 1)], points[(i + 1, j + 1, k + 1)], points[(i, j + 1, k + 1)],
            points[(i + 0.5, j + 0.5, k)], points[(i + 0.5, j, k + 0.5)], points[(i + 1, j + 0.5, k + 0.5)],
            points[(i + 0.5, j + 1, k + 0.5)], points[(i, j + 0.5, k + 0.5)], points[(i + 0.5, j + 0.5, k + 1)],
            points[(i + 0.5, j + 0.5, k + 0.5)]]
