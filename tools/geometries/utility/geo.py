import itertools

precision = 6

def write_newline(to_write, offset=""):
    block = ""
    for item in to_write:
        line = offset
        for j in item:
            line += str(j) + " "
        block += line[:-1] + "\n"
    return block


def write_face(to_write, transform=None):
    if transform is None:
        transform = {x: x for x in range(1000000)}
    block = ""
    for item in to_write:
        line = "3 " + str(item[0])
        for j in item[1:]:
            line += " " + str(transform[j])
        block += line + "\n"
    return block


def write_cells(tets, transform=None):
    if transform is None:
        transform = {x: x for x in range(1000000)}
    block = ""
    for cell in tets:
        line = "4 " + str(cell[0]) + "000"
        for j in cell[1:]:
            line += " " + str(transform[j])

        block += line + "\n"
    return block


def write_properties_plain(orientations, transform=None):
    block = ""
    for _ in orientations:
        block += "1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -1.0 0.0 0.0\n"
    return block


def write_properties_oriented(orientations, activation, transform=None):
    if transform is None:
        transform = {x: x for x in range(1000000)}
    else:
        transform = {y: x for (x, y) in transform.items()}
    block = ""
    for i in range(len(orientations)):
        line = "12"
        for o in orientations[transform[i]][:9]:
            line += " " + '{0:.6f}'.format(o)
        if transform[i] in activation:
            for a in activation[transform[i]]:
                line += " " + '{0:.6f}'.format(a)
        else:
            line += " -1.0 0.0 0.0"
        block += line + "\n"
    return block


class Geometry:
    def __init__(self, name, path="", withdata=False):
        self.name = name
        self.path = path
        self.points = {}
        self.subdomains = {}
        self.cells = {}
        self.bnd = {}
        self.faces = {}
        self.vdata = {}
        self.cdata = {}
        self.is_data_geo = withdata

    def add_point(self, point):
        index = len(self.points)
        self.points[index] = [round(p, precision) for p in point]
        return index

    def add_points(self, points):
        for point in points:
            self.add_point(point)

    def add_cell(self, cell):
        index = len(self.cells)
        self.cells[index] = cell
        return index

    def add_cells(self, cells):
        for cell in cells:
            self.add_cell(cell)

    def remove_cell(self, cell_index):
        del self.cells[cell_index]

    def add_face(self, face):
        index = len(self.faces)
        self.faces[index] = face
        return index

    def add_faces(self, faces):
        for face in faces:
            self.add_face(face)

    def remove_face(self, face_index):
        del self.faces[face_index]

    def init_data(self):
        self.vdata = {i: [] for i in self.points.keys()}
        self.cdata = {i: [] for i in self.cells.keys()}

    def add_vdata(self, p_index, data):
        for d in data:
            self.vdata[p_index].append(d)

    def add_vdata_array(self, v_data):
        for i in range(len(v_data)):
            self.add_vdata(i, v_data[i])

    def fill_vdata(self, data):
        for i, vd in self.vdata.items():
            if len(vd) == 0:
                self.add_vdata(i, data)

    def add_cdata(self, c_index, data):
        for d in data:
            self.cdata[c_index].append(d)

    def add_cdata_array(self, c_data):
        for i in range(len(c_data)):
            self.add_cdata(i, c_data[i])

    def fill_cdata(self, data):
        for i, cd in self.cdata.items():
            if len(cd) == 0:
                self.add_cdata(i, data)

    def add_subdomain(self, c_index, sd):
        self.subdomains[c_index] = sd

    def add_bnd_condition(self, f_index, bnd):
        self.bnd[f_index] = bnd

    def fill_data(self, data="12 1 0 0 0 1 0 0 0 1 -10 0 0"):
        for i in self.points.keys():
            self.vdata[i] = data
        for i in self.cells.keys():
            self.cdata[i] = data

    def point_count(self):
        return len(self.points)

    def cell_count(self):
        return len(self.cells)

    def face_count(self):
        return len(self.faces)

    def get_points(self):
        return [p for p in self.points.values()]

    def get_cells(self):
        return [c for c in self.cells.values()]

    def cell_faces(self, cell_index):
        return itertools.combinations(self.cells[cell_index], 3)

    def clean(self):
        return
        cell_points = (dict.fromkeys([p for cell in self.cells.values() for p in cell]))
        for p in cell_points:
            cell_points[p] = self.points[p]

        point_map = {}
        for p in cell_points:
            point_map[p] = len(point_map)
        new_cells = {ci: [point_map[p] for p in cell] for ci, cell in self.cells.items()}
        new_faces = {fi: [point_map[p] for p in face] for fi, face in self.faces.items()}

        if self.is_data_geo:
            remove_cdata = []
            for ci in self.cdata:
                if ci not in self.cells:
                    remove_cdata.append(ci)

            for ci in remove_cdata:
                del self.cdata[ci]

            new_vdata = {point_map[p]: self.vdata[p] for p in cell_points}
            self.vdata = new_vdata

        self.points = {point_map[p]: cell_points[p] for p in cell_points}
        self.cells = new_cells
        self.faces = new_faces

    def save(self):
        with open(str(self.path + self.name + '.geo'), 'w') as output_file:
            output_file.write("POINTS:\n")
            output_file.write(self.print_points())

            output_file.write("CELLS:\n")
            output_file.write(self.print_cells())

            output_file.write("FACES:\n")
            output_file.write(self.print_faces())

            if self.is_data_geo:
                output_file.write("VDATA:\n")
                output_file.write(self.print_pointdata())

                output_file.write("CDATA:\n")
                output_file.write(self.print_celldata())

    def print_points(self):
        points_as_string = ""
        for point in self.points.values():
            for p in point:
                points_as_string += str(p) + " "
            points_as_string = points_as_string[:-1] + "\n"
        return points_as_string

    def print_cells(self):
        cells_as_string = ""
        for i, cell in self.cells.items():
            cells_as_string += str(len(cell)) + " " + str(self.subdomains[i])
            for p in cell:
                cells_as_string += " " + str(p)
            cells_as_string += "\n"
        return cells_as_string

    def print_faces(self):
        faces_as_string = ""
        for i, face in self.faces.items():
            faces_as_string += str(len(face)) + " " + str(self.bnd[i])
            for p in face:
                faces_as_string += " " + str(p)
            faces_as_string += "\n"
        return faces_as_string

    def print_pointdata(self):
        data_as_string = ""
        for data in self.vdata.values():
            data_as_string += str(len(data))
            for d in data:
                data_as_string += " " + str(d)
            data_as_string += "\n"
        return data_as_string

    def print_celldata(self):
        data_as_string = ""
        for data in self.cdata.values():
            data_as_string += str(len(data))
            for d in data:
                data_as_string += " " + str(d)
            data_as_string += "\n"
        return data_as_string

    def cell_midpoint(self, cell_id):
        cell = self.cells[cell_id]
        midpoint = [0 for _ in range(len(self.points[0]))]
        dim = len(midpoint)
        for p_id in cell:
            p = self.points[p_id]
            for i in range(dim):
                midpoint[i] += p[i] / len(cell)

        return midpoint
