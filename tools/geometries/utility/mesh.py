import itertools

precision = 6

default_vdata = [-10.0, 0.0, 0.0]
default_cdata = [1, 0, 0, 0, 1, 0, 0, 0, 1]


class cell:
    def __init__(self, i, point_indices, face_size):
        self.index = i
        self._indices = point_indices
        self._fsize = face_size
        self.subdomain = 0

    def __len__(self):
        return len(self._indices)

    def faces(self):
        return itertools.combinations(self._indices, self._fsize)

    def corners(self):
        return self._indices


class face:
    def __init__(self, i, lcell_index, point_indices):
        self.index = i
        self.left = lcell_index
        self.right = -1
        self.bnd = -1
        self._indices = point_indices

    def set_rside(self, rcell_index):
        self.right = rcell_index

    def on_bnd(self):
        return self.right < 0

    def __len__(self):
        return len(self._indices)

    def corners(self):
        return self._indices


def matches(meshobject, points):
    return all(p in points for p in meshobject.corners())


class Mesh:
    def __init__(self, name, path="", withdata=False, readfaces=True):
        self.name = name
        self.path = path
        self.points = {}
        self.cells = {}
        self._read_faces = readfaces
        self._bnd_faces = {}
        self._inner_faces = {}
        self.vdata = {}
        self.cdata = {}
        self.is_data_geo = withdata

    def add_point(self, point):
        index = len(self.points)
        self.points[index] = [round(p, precision) for p in point]
        return index

    def add_points(self, points):
        for point in points:
            self.add_point(point)

    def add_cell(self, p_indices, fsize):
        index = len(self.cells)
        c = cell(index, p_indices, fsize)
        self.cells[index] = c

        if self._read_faces:
            for f in c.faces():
                self.add_face(index, f)
        return index

    def add_cells(self, cells, fsize):
        for c in cells:
            self.add_cell(c, fsize)

    def remove_cell(self, cell_index):
        del self.cells[cell_index]

    def add_face(self, lcell_index, p_indices):
        existing_f = [f for f in self._bnd_faces.values() if matches(f, p_indices)]
        if len(existing_f) > 0:
            bnd_f = existing_f[0]
            inner_f = face(bnd_f.index, bnd_f.left, bnd_f.corners())
            inner_f.right = lcell_index
            del self._bnd_faces[inner_f.index]
            self._inner_faces[inner_f.index] = inner_f
        else:
            index = len(self._bnd_faces) + len(self._inner_faces)
            f = face(index, lcell_index, p_indices)
            self._bnd_faces[index] = f

    def add_vdata(self, p_index, data):
        if p_index not in self.vdata:
            self.vdata[p_index] = []
        for d in data:
            self.vdata[p_index].append(d)

    def set_vdata(self, p_index, data):
        self.vdata[p_index] = data

    def add_vdata_array(self, v_data):
        for i in range(len(v_data)):
            self.add_vdata(i, v_data[i])

    def fill_vdata(self, data):
        for i, vd in self.vdata.items():
            if len(vd) == 0:
                self.add_vdata(i, data)

    def add_cdata(self, c_index, data):
        if c_index not in self.cdata:
            self.cdata[c_index] = []
        for d in data:
            self.cdata[c_index].append(d)

    def add_cdata_array(self, c_data):
        for i in range(len(c_data)):
            self.add_cdata(i, c_data[i])

    def fill_cdata(self, data):
        for i, cd in self.cdata.items():
            if len(cd) == 0:
                self.add_cdata(i, data)

    def add_subdomain(self, c_index, sd):
        self.cells[c_index].sd = sd

    def add_bnd_condition(self, f_index, bnd):
        self._bnd_faces[f_index].bnd = bnd

    def point_count(self):
        return len(self.points)

    def cell_count(self):
        return len(self.cells)

    def face_count(self):
        return len(self._bnd_faces) + len(self._inner_faces)

    def get_points(self):
        return [p for p in self.points.values()]

    def get_cells(self):
        return [c for c in self.cells.values()]

    def bnd_faces(self):
        return [f for f in self._bnd_faces.values()]

    def corners(self, meshobject):
        return [self.points[i] for i in meshobject.corners()]

    def center(self, meshobject):
        corners = [self.points[i] for i in meshobject.corners()]
        midpoint = [0, 0, 0]
        for i in range(3):
            midpoint[i] += sum(p[i] for p in corners) / len(corners)
        return midpoint

    def nearest_cell(self, point):
        min_index = -1
        min_distance = 1e20
        for c in self.cells.values():
            midpoint = self.center(c)
            distance = sum((point[i] - midpoint[i]) * (point[i] - midpoint[i]) for i in range(3))
            if distance < min_distance:
                min_distance = distance
                min_index = c.index
        return self.cells[min_index]

    def consistency(self):
        missing_indices = [i for i in self.points if i not in self.vdata]
        for i in missing_indices:
            self.add_vdata(i, default_vdata)

        missing_indices = [i for i in self.cells if i not in self.cdata]
        for i in missing_indices:
            self.add_cdata(i, default_cdata)

    def save(self):
        self.consistency()

        with open(str(self.path + self.name + '.geo'), 'w') as output_file:
            output_file.write("POINTS:\n")
            output_file.write(self.print_points())

            output_file.write("CELLS:\n")
            output_file.write(self.print_cells())

            output_file.write("FACES:\n")
            output_file.write(self.print_faces())

            if self.is_data_geo:
                output_file.write("VDATA:\n")
                output_file.write(self.print_pointdata())

                output_file.write("CDATA:\n")
                output_file.write(self.print_celldata())

    def print_points(self):
        points_as_string = ""
        for point in self.points.values():
            for p in point:
                points_as_string += str(p) + " "
            points_as_string = points_as_string[:-1] + "\n"
        return points_as_string

    def print_cells(self):
        cells_as_string = ""
        for i, cell in self.cells.items():
            cells_as_string += str(len(cell)) + " " + str(cell.subdomain)
            for p in cell.corners():
                cells_as_string += " " + str(p)
            cells_as_string += "\n"
        return cells_as_string

    def print_faces(self):
        faces_as_string = ""
        for i, face in self._bnd_faces.items():
            if not face.on_bnd():
                continue
            faces_as_string += str(len(face)) + " " + str(max(face.bnd, 0))
            for p in face.corners():
                faces_as_string += " " + str(p)
            faces_as_string += "\n"
        return faces_as_string

    def print_pointdata(self):
        data_as_string = ""
        # vdata my not be ordered
        for i in range(len(self.vdata)):
            data = self.vdata[i]
            data_as_string += str(len(data))
            for d in data:
                data_as_string += " " + str(d)
            data_as_string += "\n"
        return data_as_string

    def print_celldata(self):
        data_as_string = ""
        # cdata my not be ordered
        for i in range(len(self.cdata)):
            data = self.cdata[i]
            data_as_string += str(len(data))
            for d in data:
                data_as_string += " " + str(d)
            data_as_string += "\n"
        return data_as_string

    def cell_midpoint(self, cell_id):
        cell = self.cells[cell_id]
        midpoint = [0 for _ in range(len(self.points[0]))]
        dim = len(midpoint)
        for p_id in cell:
            p = self.points[p_id]
            for i in range(dim):
                midpoint[i] += p[i] / len(cell)

        return midpoint

    def find_face(self, point_indices):
        for face in self.faces:
            if matches(face, point_indices):
                return face
        raise KeyError("Face not found")

