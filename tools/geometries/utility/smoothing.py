def interpolate_elphy(geometry):
    replace_vdata = {}
    
    for cell in geometry.cells.values():
        max_exc_time = -10
        for p in cell:
            old_exc_time = max_exc_time
            max_exc_time = max(max_exc_time, geometry.vdata[p][0])
            if max_exc_time > old_exc_time:
                vdata = geometry.vdata[p]

        if max_exc_time >= 0:
            for p in cell:
                old_vdata = geometry.vdata[p]
                replace_vdata[p] = [vdata[0], vdata[1], min(old_vdata[2], vdata[2])]

    for p, vdata in replace_vdata.items():
        geometry.vdata[p] = vdata

