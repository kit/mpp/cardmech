
#include "m++.hpp"

#include "VtuConverter.hpp"


void smoothMeshData(const Mesh &mesh) {
  for (cell c = mesh.cells(); c != mesh.cells_end(); ++c) {
    double max_exc_time = -10.0;
    double max_amplitude = 0.0;
    double max_duration = 0.0;

    for (int i = 0; i < c.Corners(); ++i) {
      max_exc_time = std::max(max_exc_time, mesh.find_vertex(c.Corner(i)).GetData()[0]);
      max_duration = std::max(max_duration, mesh.find_vertex(c.Corner(i)).GetData()[1]);
      max_amplitude = std::max(max_amplitude, mesh.find_vertex(c.Corner(i)).GetData()[2]);
    }
    if (max_exc_time >= 0.0 && max_amplitude > 0.0) {
      for (int i = 0; i < c.Corners(); ++i) {
        double amplitude =
            std::min(mesh.find_vertex(c.Corner(i)).GetData()[2], max_amplitude);
        DataContainer exc_data({max_exc_time, max_duration, amplitude});
        mesh.find_vertex(c.Corner(i)).SetData(exc_data);
      }
    }
  }
}

bool insertCoordinate(std::string &&line, std::vector<Point> &points) {
  try {
    auto z = splitToDouble(line, ",");
    if (z.empty()) return false;
    points.emplace_back(Point(z));
    return true;
  } catch (std::exception &e) {
    return false;
  }
}

std::vector<Point> pointsFromFile(std::string csvFileName) {
  std::ifstream csvFile(csvFileName);

  int N = 256;
  char line[N];

  std::vector<Point> points{};
  if (csvFile.is_open()) {
    csvFile.getline(line, N);
    while (insertCoordinate(line, points))
      csvFile.getline(line, N);
  }
  return points;
}


int main(int argc, char **argv) {

  if (argc < 2) {
    Warning("No file was provided. Call ConvertVtu with proper file path")
    return 0;
  }

  std::string filename(argv[1]);
  std::string geometryFile = loadFile("tools/geometries/vtk/" + filename + ".vtu");
  std::string confName = "default";
  strcpy(argv[1], confName.c_str());

  Logging::DisableFileLogging();
  Config::SetConfPath(std::string(ProjectSourceDir) + "/tools/geometries/vtu/");
  Config::SetConfigFileName("default.conf");
  Mpp::initialize(&argc, argv);

  auto lvPoints = pointsFromFile(
      std::string(ProjectSourceDir) + "/tools/geometries/vtk/" + filename + "LV.csv");
  auto rvPoints = pointsFromFile(
      std::string(ProjectSourceDir) + "/tools/geometries/vtk/" + filename + "RV.csv");
  auto outerPoints = pointsFromFile(
      std::string(ProjectSourceDir) + "/tools/geometries/vtk/" + filename + "Outer.csv");

  VtuConverter converter(geometryFile);
  const auto &M = converter.UseCellArrayAsSubdomain(0)
      .UseArrayForCellData(1)
      .UseArrayForCellData(2)
      .UseArrayForCellData(3)
      .WithExcitationFromFile(0, 0.003, 30.0)
      .WithBoundaryFromData({230, 230}, std::move(lvPoints))
      .WithBoundaryFromData({231, 231}, std::move(rvPoints))
      .WithBoundaryFromData({330, 199}, std::move(outerPoints))
      .WithBoundarySmoothing({199, 330, 331})
      .WithSubdomainConversion([](int sd) {
        switch (sd) {
          case 30:
            return 1000;
          case 31:
            return 0;
          default:
            return (sd - 30) * 1000;
        }
      })
      .Create();

  smoothMeshData(M);
  converter.WriteGeoFile(M, std::string(ProjectSourceDir) + "/tools/geometries/geo/");

  return 0;
}