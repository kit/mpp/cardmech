import geometries.utility.tetras as tt
from geometries import path
from geometries.utility.geo import Geometry


def inside_chamber2d(x, y):
    return (1 < x < 2 and 1 < y < 2) or (4 < x < 5 and 1 < y < 2) or (
            1 < x < 2 and 4 < y < 5) or (4 < x < 5 and 4 < y < 5)


def inside_chamber3d(x, y, z):
    return inside_chamber2d(x, y) and 1 < z < 2


def block_2d_tet():
    x, y = 6, 6
    geometry = Geometry("VolumeBlock2DTet", path, True)
    points = {}

    # Points
    active = 0
    for i in range(0, x + 1):
        for j in range(0, y + 1):
            if not inside_chamber2d(i, j):
                points[(i, j)] = geometry.add_point([i, j])

    for i in range(0, x):
        for j in range(0, y):
            if not inside_chamber2d(i + 0.5, j + 0.5):
                points[(i + 0.5, j + 0.5)] = geometry.add_point([i + 0.5, j + 0.5])

    # Cells
    tetras = []
    for i in range(0, x):
        for j in range(0, y):
            if not inside_chamber2d(i + 0.5, j + 0.5):
                for tetra in tt.square_to_triangles4(*tt.square_corners4(points, i, j)):
                    cell_id = geometry.add_cell(tetra)
                    geometry.add_subdomain(cell_id, 0)

    # Faces
    for j in range(y):
        geometry.add_bnd_condition(geometry.add_face([points[(0, j)], points[(0, j + 1)]]), 1)
        geometry.add_bnd_condition(geometry.add_face([points[(x, j)], points[(x, j + 1)]]), 1)

    for i in range(x):
        geometry.add_bnd_condition(geometry.add_face([points[(i, 0)], points[(i + 1, 0)]]), 1)
        geometry.add_bnd_condition(geometry.add_face([points[(i, y)], points[(i + 1, y)]]), 1)

    def insert_p_boundary(bnd_cond, xval, yval):
        for j in range(yval, (yval + 1)):
            geometry.add_bnd_condition(geometry.add_face([points[(xval, j)], points[(xval, j + 1)]]), bnd_cond)
            geometry.add_bnd_condition(geometry.add_face([points[(xval + 1, j)], points[(xval + 1, j + 1)]]), bnd_cond)

        for i in range(xval, (xval + 1)):
            geometry.add_bnd_condition(geometry.add_face([points[(i, yval)], points[(i + 1, yval)]]), bnd_cond)
            geometry.add_bnd_condition(geometry.add_face([points[(i, yval + 1)], points[(i + 1, yval + 1)]]), bnd_cond)

    insert_p_boundary(230, 1, 1)
    insert_p_boundary(231, 4, 1)
    insert_p_boundary(232, 1, 4)
    insert_p_boundary(233, 4, 4)

    geometry.init_data()
    geometry.fill_vdata([-10.0, 0.0, 0.0])
    geometry.fill_cdata([1, 0, 0, 0, 1, 0, 0, 0, 1])
    geometry.save()


def block_2d_quad():
    x, y = 6, 6
    delta = 0.5
    geometry = Geometry("VolumeBlock2DQuad", path, True)
    D = int(1 / delta)
    # delta *= 0.001

    points = {}
    for i in range(0, x * D + 1):
        for j in range(0, y * D + 1):
            if not inside_chamber2d(i * delta, j * delta):
                points[(i, j)] = geometry.add_point([i * delta, j * delta])

    for i in range(0, x * D):
        for j in range(0, y * D):
            if not inside_chamber2d((i + 0.5) * delta, (j + 0.5) * delta):
                cell_id = geometry.add_cell(tt.square_corners(points, i, j))
                geometry.add_subdomain(cell_id, 0)

    # Faces
    for j in range(y * D):
        geometry.add_bnd_condition(geometry.add_face([points[(0, j)], points[(0, j + 1)]]), 1)
        geometry.add_bnd_condition(geometry.add_face([points[(x * D, j)], points[(x * D, j + 1)]]), 1)

    for i in range(x * D):
        geometry.add_bnd_condition(geometry.add_face([points[(i, 0)], points[(i + 1, 0)]]), 1)
        geometry.add_bnd_condition(geometry.add_face([points[(i, y * D)], points[(i + 1, y * D)]]), 1)

    def insert_p_boundary(bnd_cond, xval, yval):
        for j in range(yval * D, (yval + 1) * D):
            geometry.add_bnd_condition(geometry.add_face([points[(xval * D, j)], points[(xval * D, j + 1)]]), bnd_cond)
            geometry.add_bnd_condition(
                geometry.add_face([points[((xval + 1) * D, j)], points[((xval + 1) * D, j + 1)]]), bnd_cond)

        for i in range(xval * D, (xval + 1) * D):
            geometry.add_bnd_condition(geometry.add_face([points[(i, yval * D)], points[(i + 1, yval * D)]]), bnd_cond)
            geometry.add_bnd_condition(
                geometry.add_face([points[(i, (yval + 1) * D)], points[(i + 1, (yval + 1) * D)]]), bnd_cond)

    insert_p_boundary(230, 1, 1)
    insert_p_boundary(231, 4, 1)
    insert_p_boundary(232, 1, 4)
    insert_p_boundary(233, 4, 4)

    geometry.init_data()
    geometry.fill_vdata([-10.0, 0.0, 0.0])
    geometry.fill_cdata([1, 0, 0, 0, 1, 0, 0, 0, 1])
    geometry.save()


def block_3d_tet():
    x, y, z = 6, 6, 3
    geometry = Geometry("VolumeBlock3DTet", path, True)

    points = {}
    for k in range(0, z + 1):
        for j in range(0, y + 1):
            for i in range(0, x + 1):
                if not inside_chamber3d(i, j, k):
                    points[(i, j, k)] = geometry.add_point([i, j, k])

    for k in range(0, z + 1):
        for j in range(0, y):
            for i in range(0, x):
                if not inside_chamber3d(i + 0.5, j + 0.5, k):
                    points[(i + 0.5, j + 0.5, k)] = geometry.add_point([i + 0.5, j + 0.5, k])

    for k in range(0, z):
        for j in range(0, y):
            for i in range(0, x + 1):
                if not inside_chamber3d(i, j + 0.5, k + 0.5):
                    points[(i, j + 0.5, k + 0.5)] = geometry.add_point([i, j + 0.5, k + 0.5])

    for k in range(0, z):
        for j in range(0, y + 1):
            for i in range(0, x):
                if not inside_chamber3d(i + 0.5, j, k + 0.5):
                    points[(i + 0.5, j, k + 0.5)] = geometry.add_point([i + 0.5, j, k + 0.5])

    for i in range(0, x):
        for j in range(0, y):
            for k in range(0, z):
                if not inside_chamber3d(i + 0.5, j + 0.5, k + 0.5):
                    points[(i + 0.5, j + 0.5, k + 0.5)] = geometry.add_point([i + 0.5, j + 0.5, k + 0.5])

    for i in range(0, x):
        for j in range(0, y):
            for k in range(0, z):
                if not inside_chamber3d((i + 0.5), (j + 0.5), (k + 0.5)):
                    for tetra in tt.cube_to_tetras24(*tt.cube_corners24(points, i, j, k)):
                        cell_id = geometry.add_cell(tetra)
                        geometry.add_subdomain(cell_id, 0)

    # Faces
    for j in range(y):
        for k in range(z):
            for face in tt.square_to_triangles4(*tt.cube_face4_x(points, 0, j, k)):
                geometry.add_bnd_condition(geometry.add_face(face), 1)
            for face in tt.square_to_triangles4(*tt.cube_face4_x(points, x, j, k)):
                geometry.add_bnd_condition(geometry.add_face(face), 1)

    for i in range(x):
        for j in range(y):
            for face in tt.square_to_triangles4(*tt.cube_face4_z(points, i, j, 0)):
                geometry.add_bnd_condition(geometry.add_face(face), 1)
            for face in tt.square_to_triangles4(*tt.cube_face4_z(points, i, j, z)):
                geometry.add_bnd_condition(geometry.add_face(face), 1)

    for i in range(x):
        for k in range(z):
            for face in tt.square_to_triangles4(*tt.cube_face4_y(points, i, 0, k)):
                geometry.add_bnd_condition(geometry.add_face(face), 1)
            for face in tt.square_to_triangles4(*tt.cube_face4_y(points, i, y, k)):
                geometry.add_bnd_condition(geometry.add_face(face), 1)

    def insert_p_boundary(bnd_cond, xval, yval, zval):
        for face in tt.square_to_triangles4(*tt.cube_face4_x(points, xval, yval, zval)):
            geometry.add_bnd_condition(geometry.add_face(face), bnd_cond)
        for face in tt.square_to_triangles4(*tt.cube_face4_x(points, xval + 1, yval, zval)):
            geometry.add_bnd_condition(geometry.add_face(face), bnd_cond)
        for face in tt.square_to_triangles4(*tt.cube_face4_y(points, xval, yval, zval)):
            geometry.add_bnd_condition(geometry.add_face(face), bnd_cond)
        for face in tt.square_to_triangles4(*tt.cube_face4_y(points, xval, yval + 1, zval)):
            geometry.add_bnd_condition(geometry.add_face(face), bnd_cond)
        for face in tt.square_to_triangles4(*tt.cube_face4_z(points, xval, yval, zval)):
            geometry.add_bnd_condition(geometry.add_face(face), bnd_cond)
        for face in tt.square_to_triangles4(*tt.cube_face4_z(points, xval, yval, zval + 1)):
            geometry.add_bnd_condition(geometry.add_face(face), bnd_cond)

    insert_p_boundary(230, 1, 1, 1)
    insert_p_boundary(231, 4, 1, 1)
    insert_p_boundary(232, 1, 4, 1)
    insert_p_boundary(233, 4, 4, 1)

    geometry.init_data()
    geometry.fill_vdata([-10.0, 0.0, 0.0])
    geometry.fill_cdata([1, 0, 0, 0, 1, 0, 0, 0, 1])
    geometry.save()


def block_3d_quad():
    x, y, z = 6, 6, 3
    delta = 0.5
    geometry = Geometry("VolumeBlock3DQuad", path, True)
    D = int(1 / delta)

    points = {}
    for i in range(0, x * D + 1):
        for j in range(0, y * D + 1):
            for k in range(0, z * D + 1):
                if not inside_chamber3d(i * delta, j * delta, k * delta):
                    points[(i, j, k)] = geometry.add_point([i * delta, j * delta, k * delta])

    for i in range(0, x * D):
        for j in range(0, y * D):
            for k in range(0, z * D):
                if not inside_chamber3d((i + 0.5) * delta, (j + 0.5) * delta, (k + 0.5) * delta):
                    cell_id = geometry.add_cell(tt.cube_corners(points, i, j, k))
                    geometry.add_subdomain(cell_id, 0)

    # Faces
    for j in range(y * D):
        for k in range(z * D):
            geometry.add_bnd_condition(geometry.add_face(tt.cube_face_x(points, 0, j, k)), 1)
            geometry.add_bnd_condition(geometry.add_face(tt.cube_face_x(points, x * D, j, k)), 1)

    for i in range(x * D):
        for k in range(z * D):
            geometry.add_bnd_condition(geometry.add_face(tt.cube_face_y(points, i, 0, k)), 1)
            geometry.add_bnd_condition(geometry.add_face(tt.cube_face_y(points, i, y * D, k)), 1)

    for i in range(x * D):
        for j in range(y * D):
            geometry.add_bnd_condition(geometry.add_face(tt.cube_face_z(points, i, j, 0)), 1)
            geometry.add_bnd_condition(geometry.add_face(tt.cube_face_z(points, i, j, z * D)), 1)

    def insert_p_boundary(bnd_cond, xval, yval, zval):
        for j in range(yval * D, (yval + 1) * D):
            for k in range(zval * D, (zval + 1) * D):
                geometry.add_bnd_condition(geometry.add_face(tt.cube_face_x(points, xval * D, j, k)), bnd_cond)
                geometry.add_bnd_condition(geometry.add_face(tt.cube_face_x(points, (xval + 1) * D, j, k)), bnd_cond)

        for i in range(xval * D, (xval + 1) * D):
            for k in range(zval * D, (zval + 1) * D):
                geometry.add_bnd_condition(geometry.add_face(tt.cube_face_y(points, i, yval * D, k)), bnd_cond)
                geometry.add_bnd_condition(geometry.add_face(tt.cube_face_y(points, i, (yval + 1) * D, k)), bnd_cond)

        for i in range(xval * D, (xval + 1) * D):
            for j in range(yval * D, (yval + 1) * D):
                geometry.add_bnd_condition(geometry.add_face(tt.cube_face_z(points, i, j, zval * D)), bnd_cond)
                geometry.add_bnd_condition(geometry.add_face(tt.cube_face_z(points, i, j, (zval + 1) * D)), bnd_cond)

    insert_p_boundary(230, 1, 1, 1)
    insert_p_boundary(231, 4, 1, 1)
    insert_p_boundary(232, 1, 4, 1)
    insert_p_boundary(233, 4, 4, 1)

    geometry.init_data()
    geometry.fill_vdata([-10.0, 0.0, 0.0])
    geometry.fill_cdata([1, 0, 0, 0, 1, 0, 0, 0, 1])
    geometry.save()


def generate_blocks():
    block_2d_tet()
    block_3d_tet()
    block_2d_quad()
    block_3d_quad()


if __name__ == "__main__":
    generate_blocks()
