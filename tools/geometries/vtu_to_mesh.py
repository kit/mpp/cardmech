import math
from vtkmodules.util.numpy_support import vtk_to_numpy
from vtkmodules.vtkIOXML import vtkXMLUnstructuredGridReader

from endocard_points import full_ellipsoid_endocard, quarter_ellipsoid_endocard, left_ventricle_endocard, \
    fullheart_endocard
from utility.mesh import Mesh, default_vdata
from utility.progress import print_progress


class VtuMesh:
    def __init__(self, vtu_file, geo_name, scale=1.0, readfaces=True):
        self.geo = Mesh(geo_name, "../../geo/", True)
        self.bnd_data = []
        self.scale = scale

        # Read File
        reader = vtkXMLUnstructuredGridReader()
        reader.SetFileName(vtu_file + ".vtu")
        reader.Update()
        data = reader.GetOutput()

        # Extract Points
        nodes = vtk_to_numpy(data.GetPoints().GetData())
        self.add_points(nodes)
        # Extract Cells
        self.add_cells(vtk_to_numpy(data.GetCells().GetData()))

        self.point_data = data.GetPointData()
        self.cell_data = data.GetCellData()

    def rename(self, new_name):
        self.geo.name = new_name

    def add_points(self, nodes):
        l = len(nodes)
        print_progress(0, l, prefix='Reading Nodes:\t\t', suffix='')
        for i, node in enumerate(nodes):
            self.geo.add_point([p * self.scale for p in node])
            print_progress(i + 1, l, prefix='Reading Nodes:\t\t', suffix='')

    def add_cells(self, cells):
        t = cells.size
        print_progress(0, t, prefix='Reading Cells:\t\t', suffix='')
        j, k = 0, 0
        while j < t:
            i = cells[j]
            if i == 10 or i == 4:
                self.geo.add_cell([p for p in cells[j + 1:j + 5]], 3)
            elif i == 3:
                self.bnd_data.append([p for p in cells[j + 1:j + i + 1]])
            j += i + 1
            print_progress(j, t, prefix='Reading Cells:\t\t', suffix='')

    def set_subdomain_data(self, sd_function):
        l = len(self.geo.cells)
        print_progress(0, l, prefix='Setting Subdomains:\t', suffix='')
        for i, c in enumerate(self.geo.cells.values()):
            c.subdomain = sd_function(self.geo.center(c))
            print_progress(i + 1, l, prefix='Setting Subdomains:\t', suffix='')

    def set_boundary_data(self, bnd_function):
        l = len(self.geo.bnd_faces())
        print_progress(0, l, prefix='Setting Boundary:\t', suffix='')
        for i, bnd_face in enumerate(self.geo.bnd_faces()):
            indices = bnd_face.corners()
            points = self.geo.corners(bnd_face)
            bnd_face.bnd = bnd_function({indices[i]: points[i] for i in range(len(indices))})
            print_progress(i + 1, l, prefix='Setting Boundary:\t', suffix='')

    def relax_fixed_bnd(self):
        # Read fixation boundary:
        fixed_bnd = []
        for i, bnd_face in enumerate(self.geo.bnd_faces()):
            if bnd_face.bnd == 199:
                for j in bnd_face.corners():
                    fixed_bnd.append(j)
        #Remove duplicates
        fixed_bnd = list(dict.fromkeys(fixed_bnd))
        for c in self.geo.cells.values():
            if any(i in fixed_bnd for i in c.corners()):
                c.subdomain += 100

    def electrocute_bnd(self, elphy_function):
        l = len(self.geo.bnd_faces())
        print_progress(0, l, prefix='Setting Activation:\t', suffix='')
        for i, bnd_face in enumerate(self.geo.bnd_faces()):
            if bnd_face.bnd != 230:
                continue

            indices = bnd_face.corners()
            points = self.geo.corners(bnd_face)
            for j in range(len(points)):
                self.geo.set_vdata(indices[j], elphy_function(points[j]))
            print_progress(i + 1, l, prefix='Setting Activation:\t', suffix='')

    def electrocute_cell(self, elphy_function):
        l = len(self.geo.cells)
        print_progress(0, l, prefix='Setting Activation:\t', suffix='')
        for i, cell in self.geo.cells.items():
            indices = cell.corners()
            points = self.geo.corners(cell)
            for j in range(len(points)):
                self.geo.set_vdata(indices[j], elphy_function(points[j], cell))
            print_progress(i + 1, l, prefix='Setting Activation:\t', suffix='')


    def read_celldata(self, cell_indices):
        for ci in cell_indices:
            data = vtk_to_numpy(self.cell_data.GetAbstractArray(ci))
            self.geo.add_cdata_array(data[:self.geo.cell_count()])

    def read_subdomain_data(self, sd_index, sd_map=lambda x: x):
        sd_values = vtk_to_numpy(self.cell_data.GetAbstractArray(sd_index))
        for c in self.geo.cells.values():
            c.subdomain = sd_map(sd_values[c.index])

    def read_boundary_data(self, bnd_index, bnd_map=lambda x: x):
        bnd_values = vtk_to_numpy(self.cell_data.GetAbstractArray(bnd_index))

        shift = self.geo.cell_count()
        for fi in range(len(self.bnd_data)):
            bnd_cond = bnd_map(bnd_values[shift + fi])
            bnd_face = self.geo.find_face(self.bnd_data[fi])
            bnd_face.bnd = bnd_cond

    def save(self):
        self.geo.save()

    def read_fixation(self, fixation_index):
        fixation_data = vtk_to_numpy(self.point_data.GetAbstractArray(fixation_index))
        for bnd_face in self.geo.bnd_faces():
            indices = bnd_face.corners()
            if all([fixation_data[i] > 0 for i in indices]):
                bnd_face.bnd = 199

    def overwrite_subdomains(self, othergeo):
        cell_size = len(self.geo.cells)
        print_progress(0, cell_size, prefix='Overwriting subdomains:', suffix='')
        for i, c in self.geo.cells.items():
            nearest_c = othergeo.nearest_cell(self.geo.center(c))
            c.subdomain = nearest_c.subdomain
            print_progress(i + 1, cell_size, prefix='Overwriting subdomains:', suffix='')

    def change_dirichlet_bnd(self):
        for bnd_face in self.geo.bnd_faces():
            if bnd_face.bnd==199:
                bnd_face.bnd = 331

    def change_full_bnd(self):
        for bnd_face in self.geo.bnd_faces():
            if bnd_face.bnd == 331:
                bnd_face.bnd = 199
            else:
                bnd_face.bnd = 0 if self.geo.center(bnd_face)[2] > 55 else 330



def quarter_ellipsoid_bnd(points):
    if all([p[0] == 0 for p in points.values()]):
        return 100
    if all([p[1] == 0 for p in points.values()]):
        return 101
    if all([p[2] == 5 for p in points.values()]):
        return 199
    if all([p in quarter_ellipsoid_endocard for p in points.keys()]):
        return 230
    return 330


def full_ellipsoid_bnd(points):
    if all([p[2] == 5 for p in points.values()]):
        return 199
    if all([p in full_ellipsoid_endocard for p in points.keys()]):
        return 230
    return 330


def ellipsoid_elphy(point):
    height = -15
    if point[2] < height:
        return [0.0, 0.003, 30.0]
    return default_vdata


def ellipsoid_aha(c_center):
    x, y, z = c_center[0], c_center[1], c_center[2]
    if z < -17.0:
        return 17

    angle = int(360 * (math.atan2(y, x) + math.pi) / 2.0 / math.pi)
    if z > -1:
        region = int(angle / 60) + 1
        return region
    if z > -9:
        region = int(angle / 60) + 1
        return region + 6
    region = int((angle + 45) / 90) % 4
    return region + 13


def left_ventricle_bnd(points):
    if all([p in left_ventricle_endocard for p in points.keys()]):
        return 230
    return 330


def left_ventricle_elphy(point):
    apex = [88.8, -38.6, -4.8]
    dist = 15.0

    norm = sum((point[i] - apex[i]) * (point[i] - apex[i]) for i in range(3))
    if norm < dist * dist:
        return [0.0, 0.003, 30.0]
    return default_vdata


def fullheart_bnd(points):
    if all([p in fullheart_endocard for p in points.keys()]):
        return 229 + fullheart_endocard[points[0]]
    return 330


def fullheart_elphy(point):
    A = (-31.0, -80.0,20.0)
    B = (35.5, 0.7, 16.0)
    C = (-2.5, -14, -19.5)

    if det(P-A, B-A, C-A) > 0:
        return [0.0, 0.003, 30.0]
    return [20.0, 0.003, 30.0]

def kovacheva_activation(point, cell):
    print(cell.subdomain)
    if cell.subdomain< 2000:
        return [0.25, 0.003, 30.0]
    #elif cell.subdomain < 3000:
    #    return [0.002, 0.003, 30.0]
    elif cell.subdomain < 4000:
        return [0.0, 0.003, 30.0]
    else:
        return [-10.0, 0.000, 0.0]


def convert_quarter_ellipsoid():
    print("Start conversion of QuarterEllipsoid")
    geo = VtuMesh("vtk/QuarterEllipsoid", "QuarterEllipsoid", 1)
    geo.set_subdomain_data(ellipsoid_aha)
    geo.set_boundary_data(quarter_ellipsoid_bnd)
    geo.electrocute_bnd(ellipsoid_elphy)
    geo.read_celldata([1, 2, 3])
    geo.save()
    print("Finished converting QuarterEllipsoid")


def convert_full_ellipsoid():
    print("Start converion of FullEllipsoid")
    geo = VtuMesh("vtk/ellipsoid2Fibers", "FullEllipsoid", 1)
    geo.set_subdomain_data(ellipsoid_aha)
    geo.set_boundary_data(full_ellipsoid_bnd)
    geo.relax_fixed_bnd()
    geo.electrocute_bnd(ellipsoid_elphy)
    geo.read_celldata([1, 2, 3])
    geo.save()
    print("Finished converting FullEllipsoid")


def convert_unoriented_ellipsoid():
    print("Start converion of UnorientedEllipsoid")
    geo = VtuMesh("vtk/ellipsoid2Fibers", "UnorientedEllipsoid", 1)
    geo.set_subdomain_data(ellipsoid_aha)
    geo.set_boundary_data(full_ellipsoid_bnd)
    geo.relax_fixed_bnd()
    geo.electrocute_bnd(ellipsoid_elphy)
    geo.save()
    print("Finished converting UnorientedEllipsoid")


def convert_left_ventricle():
    print("Start converion of LeftVentricle")
    geo = VtuMesh("vtk/leftventricle", "LeftVentricle", 1000.0)
    geo.set_boundary_data(left_ventricle_bnd)
    geo.read_fixation(1)
    geo.electrocute_bnd(left_ventricle_elphy)

    aha_geo = VtuMesh("vtk/leftventricle_AHA", "AHAVentricle", 1000.0, False)
    aha_geo.read_subdomain_data(1, lambda y: y - 100)
    geo.overwrite_subdomains(aha_geo.geo)
    geo.relax_fixed_bnd()
    geo.read_celldata([9, 10, 11])
    geo.save()
    print("Finished converting LeftVentricle")

    geo.electrocute_bnd(lambda x : True)
    geo.rename("ContractionVentricle")
    geo.save()
    print("Finished converting ContractionVentricle")

    geo.change_dirichlet_bnd()
    geo.rename("TractionVentricle")
    geo.save()
    print("Finished converting TractionVentricle")

    geo.change_full_bnd()
    geo.rename("HalfTractionVentricle")
    geo.save()
    print("Finished converting HalfTractionVentricle")


def convert_contracting_ventricle():
    print("Start converion of LeftVentricle")
    geo = VtuMesh("vtk/leftventricle", "ContractionVentricle", 1000.0)
    geo.set_boundary_data(left_ventricle_bnd)
    geo.read_fixation(1)
    electfunc=lambda x : True
    geo.electrocute_bnd(electfunc)

    aha_geo = VtuMesh("vtk/leftventricle_AHA", "AHAVentricle", 1000.0, False)
    aha_geo.read_subdomain_data(1, lambda y: y - 100)
    geo.overwrite_subdomains(aha_geo.geo)
    geo.relax_fixed_bnd()
    geo.read_celldata([9, 10, 11])
    geo.save()
    print("Finished converting LeftVentricle")


def convert_full_heart():
    print("Start converion of FullHeart")
    geo = VtuMesh("vtk/fullheart", "FullHeart", 1.0)
    geo.read_subdomain_data(1, lambda y: (y - 30) * 1000)
    #geo.set_boundary_data(fullheart_bnd)
    geo.read_fixation(1)
    geo.electrocute_bnd(kovacheva_activation)

    geo.read_celldata([2,3,4])
    geo.save()
    print("Finished converting LeftVentricle")


def convert_kovacheva_heart():
    print("Start converion of FullHeart")
    geo = VtuMesh("vtk/kovacheva_extruded", "KovachevaHeart", 1.0)
    geo.read_subdomain_data(1, lambda y: (y - 30) * 1000)
    #geo.set_boundary_data(fullheart_bnd)
    geo.read_fixation(1)
    geo.electrocute_cell(kovacheva_activation)

    geo.read_celldata([7,12,17])
    geo.save()
    print("Finished converting KovachevaHeart")

def convert_kovacheva_small():
    print("Start converion of FullHeart")
    geo = VtuMesh("vtk/kovacheva_small", "KovachevaHeart", 1.0)
    geo.read_subdomain_data(1, lambda y: (y - 30) * 1000)
    # geo.set_boundary_data(fullheart_bnd)
    geo.read_fixation(1)
    geo.electrocute_cell(kovacheva_activation)

    geo.read_celldata([7,12,17])
    geo.save()
    print("Finished converting KovachevaHeart")


if __name__ == '__main__':
    print("Choose a geometry to convert:\n",
          " - 0: Custom geometry\n",
          " - 1: Quarter Ellipsoid\n",
          " - 2: Full Ellipsoid\n",
          " - 3: Unoriented Ellipsoid\n",
          " - 4: Left Ventricle\n",
          " - 5: Full Heart (Old)\n",
          " - 6: Full Heart (Kovacheva)\n",
            " - 7: Misc\n")
    x = -1
    x_input = input()

    try:
        x = int(x_input)
    except ValueError as vex:
        print("Please enter an integer")
        exit(1)

    if x == 0:
        print("Custom geometry conversion is not implemented.")
    elif x == 1:
        convert_quarter_ellipsoid()
    elif x == 2:
        convert_full_ellipsoid()
    elif x == 3:
        convert_left_ventricle()
    elif x == 4:
        convert_unoriented_ellipsoid()
    elif x == 5:
        convert_full_heart()
    elif x == 6:
        convert_kovacheva_heart()
    elif x == 7:
        convert_kovacheva_small()
    else:
        print("No geometry conversion with given index", x, "implemented")
