from geometries import path
from geometries.utility.geo import Geometry


def unit_interval():
    geometry = Geometry("UnitInterval", path, True)
    points = {}
    for i in range(2):
        if (i, 0) not in points:
            points[(i, 0)] = geometry.add_point([i, 0])

    # Cells
    geometry.add_subdomain(geometry.add_cell([points[(0, 0)], points[(1, 0)]]), 0)

    # Faces
    geometry.add_bnd_condition(geometry.add_face([points[(0, 0)], points[(1, 0)]]), 1)

    # ORIENTATION
    geometry.init_data()
    geometry.fill_vdata([-10.0, 0.0, 0.0])
    geometry.fill_cdata([1, 0, 0, 0, 1, 0, 0, 0, 1])
    geometry.save()


def unit_triangle():
    geometry = Geometry("UnitTriangle", path, True)
    points = {}
    for i in range(2):
        for j in range(0, 2 - i):
            points[(i, j)] = geometry.add_point([i, j])

    # Cells
    geometry.add_subdomain(geometry.add_cell([points[(0, 0)], points[(1, 0)], points[(0, 1)]]), 0)

    # Faces
    geometry.add_bnd_condition(geometry.add_face([points[(0, 0)], points[(1, 0)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(1, 0)], points[(0, 1)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(0, 1)], points[(0, 0)]]), 1)

    # ORIENTATION
    geometry.init_data()
    geometry.fill_vdata([-10.0, 0.0, 0.0])
    geometry.fill_cdata([1, 0, 0, 0, 1, 0, 0, 0, 1])
    geometry.save()


def unit_tetrahedron():
    geometry = Geometry("UnitTetrahedron", path, True)
    points = {}
    for i in range(2):
        for j in range(2 - i):
            for k in range(2 - i - j):
                points[(i, j, k)] = geometry.add_point([i, j, k])

    # Cells
    geometry.add_subdomain(geometry.add_cell([points[(0, 0, 0)], points[(1, 0, 0)], points[(0, 1, 0)], points[(0, 0, 1)]]), 0)

    # Faces
    geometry.add_bnd_condition(geometry.add_face([points[(0, 0, 0)], points[(0, 1, 0)], points[(1, 0, 0)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(0, 0, 0)], points[(1, 0, 0)], points[(0, 0, 1)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(1, 0, 0)], points[(0, 1, 0)], points[(0, 0, 1)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(0, 0, 0)], points[(0, 0, 1)], points[(0, 1, 0)]]), 1)

    # ORIENTATION
    geometry.init_data()
    geometry.fill_vdata([-10.0, 0.0, 0.0])
    geometry.fill_cdata([1, 0, 0, 0, 1, 0, 0, 0, 1])
    geometry.save()


def unit_square():
    geometry = Geometry("UnitSquare", path, True)
    points = {}
    for i in range(2):
        for j in range(0, 2):
            points[(i, j)] = geometry.add_point([i, j])

    # Cells
    geometry.add_subdomain(geometry.add_cell([points[(0, 0)], points[(1, 0)], points[(1,1)], points[(0, 1)]]), 0)

    # Faces
    geometry.add_bnd_condition(geometry.add_face([points[(0, 0)], points[(1, 0)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(1, 0)], points[(1, 1)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(1, 1)], points[(0, 1)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(0, 1)], points[(0, 0)]]), 1)

    # ORIENTATION
    geometry.init_data()
    geometry.fill_vdata([-10.0, 0.0, 0.0])
    geometry.fill_cdata([1, 0, 0, 0, 1, 0, 0, 0, 1])
    geometry.save()


def unit_cube():
    geometry = Geometry("UnitHexahedron", path, True)
    points = {}
    for i in range(2):
        for j in range(2):
            for k in range(2):
                points[(i, j, k)] = geometry.add_point([i, j, k])

    # Cells
    geometry.add_subdomain(geometry.add_cell(
        [points[(0, 0, 0)], points[(1, 0, 0)], points[(1, 1, 0)], points[(0, 1, 0)],
         points[(0, 0, 1)], points[(1, 0, 1)], points[(1, 1, 1)], points[(0, 1, 1)]]), 0)

    # Faces
    geometry.add_bnd_condition(geometry.add_face([points[(0, 0, 0)], points[(0, 1, 0)], points[(1, 1, 0)], points[(1, 0, 0)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(0, 0, 0)], points[(1, 0, 0)], points[(1, 0, 1)], points[(0, 0, 1)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(1, 0, 0)], points[(1, 1, 0)], points[(1, 1, 1)], points[(1, 0, 1)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(1, 1, 0)], points[(0, 1, 0)], points[(0, 1, 1)], points[(1, 1, 1)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(0, 1, 0)], points[(0, 0, 0)], points[(0, 0, 1)], points[(0, 1, 1)]]), 1)
    geometry.add_bnd_condition(geometry.add_face([points[(0, 0, 1)], points[(1, 0, 1)], points[(1, 1, 1)], points[(0, 1, 1)]]), 1)

    # ORIENTATION
    geometry.init_data()
    geometry.fill_vdata([-10.0, 0.0, 0.0])
    geometry.fill_cdata([1, 0, 0, 0, 1, 0, 0, 0, 1])
    geometry.save()




if __name__ == "__main__":
    unit_interval()
    unit_triangle()
    unit_tetrahedron()
    unit_square()
    unit_cube()
