# ========================================================================================================
# importing a geometry via vtu_tu_mesh.py
# Then finding for each point on the endocardial surface its nearest neighbour on the epicardial surface
# ========================================================================================================
import math
from geometries.utility.progress import print_progress

from vtu_to_mesh import VtuMesh, left_ventricle_bnd, full_ellipsoid_bnd


def point_distance(point, other):
    d = sum((point[i] - other[i]) * (point[i] - other[i]) for i in range(3))
    return math.sqrt(d)


def find_nearest(point, point_list):
    index = 0
    dist = point_distance(point, point_list[0])
    for i in range(1, len(point_list)):
        d = point_distance(point, point_list[i])
        if d < dist:
            dist = d
            index = i
    return point_list[index]


def nearest_neighbour_pairs(geo, endo_bnd, epi_bnd):
    endo_points = {}
    epi_points = {}
    l = len(geo.bnd_faces())
    li = 0
    print_progress(0, l, prefix='Sorting faces:', suffix='')
    for face in geo.bnd_faces():
        indices = face.corners()
        points = geo.corners(face)
        if face.bnd == endo_bnd:
            for i, p in zip(indices, points):
                endo_points[i] = p
        if face.bnd == epi_bnd:
            for i, p in zip(indices, points):
                epi_points[i] = p
        li += 1
        print_progress(li, l, prefix='Sorting faces:', suffix='')

    print("Finding nearest neighbours")
    epi_list = [p for p in epi_points.values()]
    nn_pairs = [(p, find_nearest(p, epi_list)) for p in endo_points.values()]
    return nn_pairs


def mpp_string(nn_pairs):
    endo_string = "std::vector<Point> endocardPoints = {\n"
    epi_string = "std::vector<Point> epicardPoints = {\n"
    for pair in nn_pairs:
        endo_string += "Point(" + str(pair[0][0]) + "," + str(pair[0][1]) + "," + str(pair[0][2]) + "),"
        epi_string += "Point(" + str(pair[1][0]) + "," + str(pair[1][1]) + "," + str(pair[1][2]) + "),"
    print(endo_string + "\n};")
    print(epi_string + "\n};")


def generate_ventricle_points():
    print("Start converion of LeftVentricle")
    vmesh = VtuMesh("vtk/leftventricle/leftventricle", "LeftVentricle", 1000.0)
    vmesh.set_boundary_data(left_ventricle_bnd)
    vmesh.read_fixation(1)

    print("Searching nearest neighbour points")
    nn_pairs = nearest_neighbour_pairs(vmesh.geo, 230, 330)
    print("Printing points:")
    mpp_string(nn_pairs)


def generate_ellipsoid_points():
    print("Start converion of Ellipsoid")
    vmesh = VtuMesh("vtk/ellipsoid/ellipsoid2Fibers", "FullEllipsoid", 1)
    vmesh.set_boundary_data(full_ellipsoid_bnd)

    print("Searching nearest neighbour points")
    nn_pairs = nearest_neighbour_pairs(vmesh.geo, 230, 330)
    print("Printing points:")
    mpp_string(nn_pairs)


if __name__ == '__main__':
    # generate_ventricle_points()
    generate_ellipsoid_points()
