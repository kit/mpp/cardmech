import math
import numpy as np

from utility import tetras as tt
from utility.geo import Geometry


class Boundary:
    def __init__(self):
        self.left = 0
        self.right = 0
        self.lower = 0
        self.upper = 0
        self.front = 0
        self.back = 0

    def reset(self, left, right, lower, upper, front, back):
        self.left = left
        self.right = right
        self.lower = lower
        self.upper = upper
        self.front = front
        self.back = back


def default_sd(x, y, z=0):
    return 0


def default_elphy(x, y, z=0):
    return [-10, 0, 0]


def default_mech(x, y, z=0):
    return [1, 0, 0, 0, 1, 0, 0, 0, 1]


class CuboidGenerator:
    def __init__(self, name="DefaultCuboid"):
        self.path = "../../geo/"
        self.name = name
        self.x = 1
        self.y = 1
        self.z = 1
        self.delta = 1.0
        self.scale=1
        self.bnd = Boundary()

        self.sd_callback = default_sd
        self.elphy_callback = default_elphy
        self.mech_callback = default_mech

    def reset(self, name="DefaultCuboid"):
        self.path = "../../geo/"
        self.name = name
        self.x = 1
        self.y = 1
        self.z = 1
        self.delta = 1.0
        self.scale=1
        self.bnd = Boundary()

        self.sd_callback = default_sd
        self.elphy_callback = default_elphy
        self.mech_callback = default_mech

    def resize(self, x, y, z=1):
        self.x, self.y, self.z = x, y, z

    def rescale(self, s=1):
        self.scale = s
        
    def setDelta(self,delta):
        self.delta=delta
    
    def refine(self, level):
        self.delta = 1.0 / pow(2, level)

    def rename(self, name):
        self.name = name

    def set_boundary(self, left, right, lower, upper, front=0, back=0):
        self.bnd.reset(left, right, lower, upper, front, back)

    def set_sd(self, sd_cb):
        self.sd_callback = sd_cb

    def set_elphy(self, e_cb):
        self.elphy_callback = e_cb

    def set_mech(self, e_m):
        self.mech_callback = e_m

    def beam_2d_tet(self):
        delta = self.delta
        x, y = self.x, self.y
        geometry = Geometry(self.name + "2DTet", self.path, True)
        D = int(1 / delta)
        points = {}

        # Points
        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                if (i, j) not in points:
                    points[(i, j)] = geometry.add_point([self.scale * i * delta, self.scale * j * delta])

        # Cells
        for i in range(0, x * D):
            for j in range(0, y * D):
                for tetra in tt.square_to_triangles(*tt.square_corners(points, i, j)):
                    cell_id = geometry.add_cell(tetra)
                    geometry.add_subdomain(cell_id, self.sd_callback(*geometry.cell_midpoint(cell_id)))

        # Faces
        for j in range(y * D):
            if self.bnd.left > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(0, j)], points[(0, j + 1)]]), self.bnd.left)
            if self.bnd.right > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(x * D, j)], points[(x * D, j + 1)]]),
                                           self.bnd.right)

        for i in range(x * D):
            if self.bnd.upper > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(i, y * D)], points[(i + 1, y * D)]]),
                                           self.bnd.upper)
            if self.bnd.lower > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(i, 0)], points[(i + 1, 0)]]), self.bnd.lower)

        geometry.init_data()
        for p_index, point in geometry.points.items():
            geometry.add_vdata(p_index, self.elphy_callback(*point))
        for c_index, cell in geometry.cells.items():
            midpoint = sum(np.array(geometry.points[cell[i]]) / len(cell) for i in range(len(cell)))
            geometry.add_cdata(c_index, self.mech_callback(*midpoint))
        # interpolate_elphy(geometry)
        geometry.save()

    def beam_2d_quad(self):
        delta = self.delta
        x, y = self.x, self.y
        geometry = Geometry(self.name + "2DQuad", self.path, True)
        D = int(1 / delta)
        points = {}

        # Points
        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                if (i, j) not in points:
                    points[(i, j)] = geometry.add_point([self.scale * i * delta, self.scale * j * delta])

        # Cells
        for i in range(0, x * D):
            for j in range(0, y * D):
                cell_id = geometry.add_cell(tt.square_corners(points, i, j))
                geometry.add_subdomain(cell_id, self.sd_callback(*geometry.cell_midpoint(cell_id)))

        # Faces
        for j in range(y * D):
            if self.bnd.left > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(0, j)], points[(0, j + 1)]]), self.bnd.left)
            if self.bnd.right > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(x * D, j)], points[(x * D, j + 1)]]),
                                           self.bnd.right)

        for i in range(x * D):
            if self.bnd.upper > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(i, y * D)], points[(i + 1, y * D)]]),
                                           self.bnd.upper)
            if self.bnd.lower > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(i, 0)], points[(i + 1, 0)]]), self.bnd.lower)

        geometry.init_data()
        for p_index, point in geometry.points.items():
            geometry.add_vdata(p_index, self.elphy_callback(*point))
        for c_index, cell in geometry.cells.items():
            midpoint = sum(np.array(geometry.points[cell[i]]) / len(cell) for i in range(len(cell)))
            geometry.add_cdata(c_index, self.mech_callback(*midpoint))
        # interpolate_elphy(geometry)
        geometry.save()

    def beam_2d_quad_withchambers(self):
        delta = self.delta
        x, y = self.x, self.y
        geometry = Geometry(self.name + "2DQuad", self.path, True)
        D = int(1 / delta)
        points = {}

        # Points
        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                if (i, j) not in points:
                    points[(i, j)] = geometry.add_point([self.scale * i * delta, self.scale * j * delta])

        # Cells
        for i in range(0, x * D):
            for j in range(0, y * D):
                cell_id = geometry.add_cell(tt.square_corners(points, i, j))
                # geometry.add_subdomain(cell_id, 0000)
                if i < D:  # 3*D:
                    if j < y * D / 2:
                        geometry.add_subdomain(cell_id, 1000)
                    else:
                        geometry.add_subdomain(cell_id, 0000)
                else:
                    if j < y * D / 2:
                        geometry.add_subdomain(cell_id, 3000)
                    else:
                        geometry.add_subdomain(cell_id, 2000)

        # Faces
        for j in range(y * D):
            if self.bnd.left > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(0, j)], points[(0, j + 1)]]), self.bnd.left)
            if self.bnd.right > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(x * D, j)], points[(x * D, j + 1)]]),
                                           self.bnd.right)

        for i in range(x * D):
            if self.bnd.upper > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(i, y * D)], points[(i + 1, y * D)]]),
                                           self.bnd.upper)
            if self.bnd.lower > 0:
                geometry.add_bnd_condition(geometry.add_face([points[(i, 0)], points[(i + 1, 0)]]), self.bnd.lower)

        geometry.init_data()
        for p_index, point in geometry.points.items():
            geometry.add_vdata(p_index, self.elphy_callback(*point))
        for c_index, cell in geometry.cells.items():
            midpoint = sum(np.array(geometry.points[cell[i]]) / len(cell) for i in range(len(cell)))
            geometry.add_cdata(c_index, self.mech_callback(*midpoint))
        # interpolate_elphy(geometry)
        geometry.save()

    def beam_3d_tet(self):
        delta = self.delta
        x, y, z = self.x, self.y, self.z
        geometry = Geometry(self.name + "3DTet", self.path, True)
        D = int(1 / delta)
        points = {}

        # Points
        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                for k in range(0, z * D + 1):
                    points[(i, j, k)] = geometry.add_point([self.scale * i * delta, self.scale * j * delta, self.scale * k * delta])
        # Cells
        for i in range(0, x * D):
            for j in range(0, y * D):
                for k in range(0, z * D):
                    for tetra in tt.cube_to_tetras(*tt.cube_corners(points, i, j, k)):
                        cell_id = geometry.add_cell(tetra)
                        geometry.add_subdomain(cell_id, self.sd_callback(*geometry.cell_midpoint(cell_id)))

        # Faces
        for j in range(y * D):
            for k in range(z * D):
                if self.bnd.left >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_x(points, 0, j, k)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.left)

                if self.bnd.right >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_x(points, x * D, j, k)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.right)

        '''Insert Pressure Boundary on Top and Bottom'''
        for i in range(x * D):
            for j in range(y * D):
                if self.bnd.upper >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_z(points, i, j, z * D)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.upper)

                if self.bnd.lower >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_z(points, i, j, 0)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.lower)

        for i in range(x * D):
            for k in range(z * D):
                if self.bnd.front >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_y(points, i, 0, k)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.front)

                if self.bnd.back >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_y(points, i, y * D, k)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.back)

        geometry.init_data()
        for p_index, point in geometry.points.items():
            geometry.add_vdata(p_index, self.elphy_callback(*point))
        for c_index, cell in geometry.cells.items():
            midpoint = sum(np.array(geometry.points[cell[i]]) / len(cell) for i in range(len(cell)))
            geometry.add_cdata(c_index, self.mech_callback(*midpoint))
        # interpolate_elphy(geometry)
        geometry.save()

    def beam_3d_tet_withchambers(self):
        delta = self.delta
        x, y, z = self.x, self.y, self.z
        geometry = Geometry(self.name + "3DTet", self.path, True)
        D = int(1 / delta)
        points = {}

        # Points
        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                for k in range(0, z * D + 1):
                    points[(i, j, k)] = geometry.add_point([self.scale * i * delta, self.scale * j * delta, self.scale * k * delta])
        # Cells
        for i in range(0, x * D):
            for j in range(0, y * D):
                for k in range(0, z * D):
                    for tetra in tt.cube_to_tetras(*tt.cube_corners(points, i, j, k)):
                        cell_id = geometry.add_cell(tetra)
                        # geometry.add_subdomain(cell_id, 0)
                        if i < 3 * D:
                            if k < z * D / 2:
                                geometry.add_subdomain(cell_id, 1000)
                            else:
                                geometry.add_subdomain(cell_id, 0000)
                        else:
                            if k < z * D / 2:
                                geometry.add_subdomain(cell_id, 3000)
                            else:
                                geometry.add_subdomain(cell_id, 2000)

        # Faces
        for j in range(y * D):
            for k in range(z * D):
                if self.bnd.left >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_x(points, 0, j, k)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.left)

                if self.bnd.right >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_x(points, x * D, j, k)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.right)

        '''Insert Pressure Boundary on Top and Bottom'''
        for i in range(x * D):
            for j in range(y * D):
                if self.bnd.upper >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_z(points, i, j, z * D)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.upper)

                if self.bnd.lower >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_z(points, i, j, 0)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.lower)

        for i in range(x * D):
            for k in range(z * D):
                if self.bnd.front >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_y(points, i, 0, k)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.front)

                if self.bnd.back >= 0:
                    for face in tt.square_to_triangles(*tt.cube_face_y(points, i, y * D, k)):
                        face_id = geometry.add_face(face)
                        geometry.add_bnd_condition(face_id, self.bnd.back)

        geometry.init_data()
        for p_index, point in geometry.points.items():
            geometry.add_vdata(p_index, self.elphy_callback(*point))
        for c_index, cell in geometry.cells.items():
            midpoint = sum(np.array(geometry.points[cell[i]]) / len(cell) for i in range(len(cell)))
            geometry.add_cdata(c_index, self.mech_callback(*midpoint))
        # interpolate_elphy(geometry)
        geometry.save()

    def beam_3d_quad(self):
        delta = self.delta
        x, y, z = self.x, self.y, self.z
        geometry = Geometry(self.name + "3DQuad", self.path, True)
        D = int(1 / delta)
        points = {}

        # Points
        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                for k in range(0, z * D + 1):
                    points[(i, j, k)] = geometry.add_point([self.scale * i * delta, self.scale * j * delta, self.scale * k * delta])

        # Cells
        for i in range(0, x * D):
            for j in range(0, y * D):
                for k in range(0, z * D):
                    cell_id = geometry.add_cell(tt.cube_corners(points, i, j, k))
                    geometry.add_subdomain(cell_id, self.sd_callback(*geometry.cell_midpoint(cell_id)))

        # Faces
        ''' Insert Dirichlet Boundary'''
        for j in range(y * D):
            for k in range(z * D):
                if self.bnd.left >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_x(points, 0, j, k)), self.bnd.left)
                if self.bnd.right >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_x(points, x * D, j, k)), self.bnd.right)

        '''Insert Pressure Boundary on Top and Bottom'''
        for i in range(x * D):
            for j in range(y * D):
                if self.bnd.lower >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_z(points, i, j, 0)), self.bnd.lower)
                if self.bnd.upper >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_z(points, i, j, z * D)), self.bnd.upper)

        for i in range(x * D):
            for k in range(z * D):
                if self.bnd.front >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_y(points, i, 0, k)), self.bnd.front)
                if self.bnd.back >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_y(points, i, y * D, k)), self.bnd.back)

        geometry.init_data()
        for p_index, point in geometry.points.items():
            geometry.add_vdata(p_index, self.elphy_callback(*point))
        for c_index, cell in geometry.cells.items():
            midpoint = sum(np.array(geometry.points[cell[i]]) / len(cell) for i in range(len(cell)))
            geometry.add_cdata(c_index, self.mech_callback(*midpoint))
        # interpolate_elphy(geometry)

        geometry.save()

    def beam_3d_quad_withchambers(self):
        delta = self.delta
        x, y, z = self.x, self.y, self.z
        geometry = Geometry(self.name + "3DQuad", self.path, True)
        D = int(1 / delta)
        points = {}

        # Points
        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                for k in range(0, z * D + 1):
                    points[(i, j, k)] = geometry.add_point([self.scale * i * delta, self.scale * j * delta, self.scale * k * delta])

        # Cells
        for i in range(0, x * D):
            for j in range(0, y * D):
                for k in range(0, z * D):
                    cell_id = geometry.add_cell(tt.cube_corners(points, i, j, k))
                    if i < 3 * D:
                        if k < z * D / 2:
                            geometry.add_subdomain(cell_id, 1000)
                        else:
                            geometry.add_subdomain(cell_id, 0000)
                    else:
                        if k < z * D / 2:
                            geometry.add_subdomain(cell_id, 3000)
                        else:
                            geometry.add_subdomain(cell_id, 2000)

        # Faces
        ''' Insert Dirichlet Boundary'''
        for j in range(y * D):
            for k in range(z * D):
                if self.bnd.left >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_x(points, 0, j, k)), self.bnd.left)
                if self.bnd.right >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_x(points, x * D, j, k)), self.bnd.right)

        '''Insert Pressure Boundary on Top and Bottom'''
        for i in range(x * D):
            for j in range(y * D):
                if self.bnd.lower >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_z(points, i, j, 0)), self.bnd.lower)
                if self.bnd.upper >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_z(points, i, j, z * D)), self.bnd.upper)

        for i in range(x * D):
            for k in range(z * D):
                if self.bnd.front >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_y(points, i, 0, k)), self.bnd.front)
                if self.bnd.back >= 0:
                    geometry.add_bnd_condition(geometry.add_face(tt.cube_face_y(points, i, y * D, k)), self.bnd.back)

        geometry.init_data()
        for p_index, point in geometry.points.items():
            geometry.add_vdata(p_index, self.elphy_callback(*point))
        for c_index, cell in geometry.cells.items():
            midpoint = sum(np.array(geometry.points[cell[i]]) / len(cell) for i in range(len(cell)))
            geometry.add_cdata(c_index, self.mech_callback(*midpoint))
        # interpolate_elphy(geometry)

        geometry.save()

    def generate_all(self):
        self.generate_2d()
        self.generate_3d()

    def generate_2d(self):
        self.beam_2d_quad()
        self.beam_2d_tet()

    def generate_3d(self):
        self.beam_3d_quad()
        self.beam_3d_tet()


def beam_activation(x, y, z=0):
    if x <= 1.0:
        return [0.0, 0.002, 20]
    return [-10.0, 0.0, 0.0]


def niederer_activation(x, y, z=0):
    if x <= 1.5 and y <= 1.5 and z <= 1.5:
        return [0.0, 0.002, 20]
    return [-10.0, 0.0, 0.0]

def niederer_activation_TT(x, y, z=0):
    if x <= 1.5 and y <= 1.5 and z <= 1.5:
        return [0.0, 0.002, 50]
    return [-10.0, 0.0, 0.0]

def chamber_activation2d(x, y):
    if x <= 1.0 and y <= 1.0:
        return [0.005, 0.002, 20]
    elif x >= 4.5 and y >= 2.5:
        return [0.0, 0.002, 20]
    else:
        return [-10.0, 0.0, 0.0]


def chamber_activation2dTest(x, y):
    if x <= 0.5 and y <= 0.5:
        return [0.005, 0.002, 20]
    elif x >= 1.5 and y >= 1.5:
        return [0.0, 0.002, 20]
    else:
        return [-10.0, 0.0, 0.0]


def chamber_activation3d(x, y, z):
    if x <= 1.0 and y <= 1.0 and z <= 1.0:
        return [0.005, 0.002, 20]
    elif x >= 4.5 and y >= 2.5 and z >= 2.5:
        return [0.0, 0.002, 20]
    else:
        return [-10.0, 0.0, 0.0]


def generate_unit(cuboids):
    cuboids.rename("UnitBlock")
    cuboids.set_boundary(1, 1, 1, 1, 1, 1)
    cuboids.resize(1, 1, 1)
    cuboids.generate_all()


def generate_benchmark_beam(cuboids):
    cuboids.rename("BenchmarkBeam")
    cuboids.set_boundary(199, 230, 232, 231)
    cuboids.resize(10, 1, 1)
    cuboids.set_elphy(beam_activation)
    cuboids.generate_all()

def generate_oscillation_beam(cuboids):
    cuboids.rename("ThinBeam")
    cuboids.set_boundary(199, 0, 0, 0, 0, 0)
    cuboids.resize(100, 1, 1)
    cuboids.rescale(0.1)
    cuboids.set_elphy(beam_activation)
    cuboids.generate_all()


def generate_dirichlet_beam(cuboids):
    cuboids.rename("DirichletBeam")
    cuboids.set_boundary(1, 1, 1, 1, 1, 1)
    cuboids.resize(10, 1, 1)
    cuboids.generate_all()


def generate_benchmark_block(cuboids):
    cuboids.rename("BenchmarkBlock")
    cuboids.set_boundary(100, 230, 101, 231, 102, 232)
    cuboids.resize(5, 10, 5)
    cuboids.generate_2d()
    cuboids.set_boundary(100, 230, 102, 232, 101, 231)
    cuboids.resize(5, 5, 5)
    cuboids.generate_3d()


def generate_test_blocks(cuboids):
    cuboids.resize(1, 1, 1)
    cuboids.refine(1)

    cuboids.rename("DirichletBlock")
    cuboids.set_boundary(1, 1, 1, 1, 1, 1)
    cuboids.generate_all()

    cuboids.rename("TractionBlock")
    cuboids.set_boundary(1, 330, 1, 330, 1, 330)
    cuboids.generate_all()

    cuboids.rename("NeumannBlock")
    cuboids.set_boundary(1, 230, 1, 230, 1, 230)
    cuboids.generate_all()


def transfer_subdomains(x, y, z=-1):
    xi, yi, zi = math.floor(x), math.floor(y), math.floor(z)
    sd = 9000
    if 0 < xi < 3 and 0 < yi < 3:
        sd = 1000 * (xi + 2 * yi - 3)
    if zi == 0 or zi == 2:
        sd = 9000
    return sd


def generate_transfer_block(cuboids):
    cuboids.rename("TransferBlock")
    cuboids.refine(0)
    cuboids.set_boundary(1, 1, 1, 1, 1, 1)
    cuboids.resize(4, 4, 3)
    cuboids.set_sd(transfer_subdomains)
    cuboids.generate_all()
    cuboids.reset()


def generate_niederer_block(cuboids):
    cuboids.rename("NiedererBlock")
    cuboids.set_boundary(0, 0, 0, 0, 0, 0)
    cuboids.refine(1)
    cuboids.resize(20, 7, 3)
    cuboids.set_elphy(niederer_activation)
    cuboids.generate_all()
    
def generate_niederer_BM_TT(cuboids):
    cuboids.rename("NiedererBM05_")
    cuboids.set_boundary(0, 0, 0, 0, 0, 0)
    #cuboids.refine(1)
    cuboids.setDelta(0.5)
    cuboids.resize(20, 7, 3)
    cuboids.set_elphy(niederer_activation_TT)
    cuboids.generate_3d()
    
    cuboids.rename("NiedererBM02_")
    cuboids.setDelta(0.2)
    cuboids.generate_3d()
    
    cuboids.rename("NiedererBM01_")
    cuboids.setDelta(0.1)
    cuboids.generate_3d()


def generate_SmallHexaTestCube(cuboids):
    cuboids.rename("SmallTestCube1")
    cuboids.set_boundary(0, 0, 0, 0, 0, 0)
    cuboids.refine(0)
    cuboids.resize(3, 3, 3)
    cuboids.set_elphy(niederer_activation)
    cuboids.generate_all()


def generate_rossi_block(cuboids):
    cuboids.rename("RossiBlock")
    cuboids.refine(1)
    cuboids.resize(20, 16, 12)
    cuboids.set_elphy(niederer_activation)
    cuboids.set_boundary(100, 0, 102, 0, 100, 0)
    cuboids.generate_2d()
    cuboids.set_boundary(100, 0, 101, 0, 102, 0)
    cuboids.generate_3d()

    cuboids.rename("RossiBlockSmall")
    cuboids.refine(1)
    cuboids.resize(5, 4, 3)
    cuboids.set_elphy(niederer_activation)
    cuboids.set_boundary(100, 0, 102, 0, 100, 0)
    cuboids.generate_2d()
    cuboids.set_boundary(100, 0, 101, 0, 102, 0)
    cuboids.generate_3d()


def generate_HexaTestCube(cuboids):
    cuboids.rename("TestCube025")
    cuboids.set_boundary(0, 0, 0, 0, 0, 0)
    cuboids.refine(2)
    cuboids.resize(5, 3, 3)
    cuboids.set_elphy(niederer_activation)
    cuboids.generate_all()


def generateChamberCube2d(cuboids):
    cuboids.rename("ChamberCube05")
    cuboids.set_boundary(0, 0, 0, 0, 0, 0)
    cuboids.refine(1)
    cuboids.resize(5, 3, 3)
    cuboids.set_elphy(chamber_activation2d)
    cuboids.beam_2d_quad_withchambers()


def generateChamberCube2dTest(cuboids):
    cuboids.rename("TestChamberCube05WithActivation")
    cuboids.set_boundary(0, 0, 0, 0, 0, 0)
    cuboids.refine(1)
    cuboids.resize(2, 2, 2)
    cuboids.set_elphy(chamber_activation2dTest)
    cuboids.beam_2d_quad_withchambers()


def generateChamberCube3d(cuboids):
    cuboids.rename("ChamberCube025")
    cuboids.set_boundary(0, 0, 0, 0, 0, 0)
    cuboids.refine(2)
    cuboids.resize(5, 3, 3)
    cuboids.set_elphy(chamber_activation3d)
    cuboids.beam_3d_quad_withchambers()
    cuboids.beam_3d_tet_withchambers()


if __name__ == "__main__":
    cuboids = CuboidGenerator()
    #generate_unit(cuboids)
    #generate_benchmark_beam(cuboids)
    #generate_oscillation_beam(cuboids)
    #generate_dirichlet_beam(cuboids)
    #generate_benchmark_block(cuboids)
    #generate_test_blocks(cuboids)
    #generate_transfer_block(cuboids)
    #generate_niederer_block(cuboids)
    generate_niederer_BM_TT(cuboids)
    #generate_rossi_block(cuboids)
    #generate_HexaTestCube(cuboids)
    # generateChamberCube(cuboids)
    #generate_SmallHexaTestCube(cuboids)
    # generate_HexaTestCube(cuboids)
    # generateChamberCube2d(cuboids)
    # generateChamberCube3d(cuboids)
    # generateChamberCube2dTest(cuboids)
