import old.tetra_tools as tt

geopath = "../../geo/"
datapath = "../cardiacmechanics/data/"

'''Parameters for Cuboid'''
refinement = 0


def beam_2d_tet(name, x, y, z, t):
    delta = 0.5
    D = 2
    with open(geopath + name + "2DTet.geo", 'w') as output_file:
        # delta *= 0.001

        points = {}
        pointsfloat = {}
        cells = {}

        # Points
        active = 0
        n = 0
        output_file.write("POINTS:\n")
        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                output_file.write('{0:.4f}'.format(i * delta) + " " + '{0:.4f}'.format(j * delta) + "\n")
                points[(i, j)] = " " + str(n)
                pointsfloat[n] = (i, j)
                n += 1

        for i in range(0, 2):
            for j in range(1, t * D + 1):
                output_file.write(
                    '{0:.4f}'.format(x - (1 - i) * delta) + " " + '{0:.4f}'.format(y + (j * delta)) + "\n")
                points[(x * D - 1 + i, (y * D) + j)] = " " + str(n)
                pointsfloat[n] = (x * D - 1 + i, (y * D) + j)
                n += 1

                output_file.write(
                    '{0:.4f}'.format(x - (1 - i) * delta) + " " + '{0:.4f}'.format(- (j * delta)) + "\n")
                points[(x * D - 1 + i, -j)] = " " + str(n)
                pointsfloat[n] = (x * D - 1 + i, -j)
                n += 1

        for i in range(0, x * D):
            for j in range(0, y * D):
                output_file.write(
                    '{0:.4f}'.format(i + (0.5 * delta)) + " " + '{0:.4f}'.format(j + (0.5 * delta)) + "\n")
                points[(i + (0.5 * delta), j + (0.5 * delta))] = " " + str(n)
                pointsfloat[n] = (i + (0.5 * delta), j + (0.5 * delta))
                n += 1

        for j in range(1, t * D):
            output_file.write(
                '{0:.4f}'.format(x - (0.5 * delta)) + " " + '{0:.4f}'.format(y + j*(0.5 * delta)) + "\n")
            points[(x * D - 1 + i, (y * D) + j)] = " " + str(n)
            pointsfloat[n] = (x * D - 1 + i, (y * D) + j)
            n += 1

            output_file.write(
                '{0:.4f}'.format(x - (1 - i) * delta) + " " + '{0:.4f}'.format(- (j * delta)) + "\n")
            points[(x * D - 1 + i, -j)] = " " + str(n)
            pointsfloat[n] = (x * D - 1 + i, -j)
            n += 1

        # Cells
        output_file.write("CELLS:\n")
        tetras = []
        for i in range(0, x):
            for j in range(0, y):
                tetras += (tt.square_to_triangles4("3 0000", points[(i, j)], points[(i + 1, j)],
                                                   points[(i + 1, j + 1)], points[(i, j + 1)],
                                                   points[i + 0.5, j + 0.5]))

        for j in range(0, t * D):
            tetras += (
                tt.square_to_triangles4("3 0000", points[(x * D - 1, y * D + j)], points[(x * D, y * D + j)],
                                        points[(x * D, y * D + j + 1)], points[(x * D - 1, y * D + j + 1)]))
            tetras += (tt.square_to_triangles4("3 0000", points[(x * D - 1, -j - 1)], points[(x * D, -j - 1)],
                                               points[(x * D, -j)], points[(x * D - 1, -j)]))

        for t in tetras:
            output_file.write(t)

        # Faces
        output_file.write("FACES:\n")
        for j in range(y * D):
            output_file.write("2 199" + points[(0, j)] + points[(0, j + 1)] + "\n")
            output_file.write("2 230" + points[(x * D, j)] + points[(x * D, j + 1)] + "\n")

        for j in range(t * D):
            output_file.write("2 230" + points[(x * D, y * D + j)] + points[(x * D, y * D + j + 1)] + "\n")
            output_file.write("2 230" + points[(x * D, -j - 1)] + points[(x * D, -j)] + "\n")

        output_file.write("2 231" + points[(x * D - 1, (y + t) * D)] + points[(x * D, (y + t) * D)] + "\n")
        output_file.write("2 231" + points[(x * D - 1, -t * D)] + points[(x * D, -t * D)] + "\n")

        # ORIENTATION
        output_file.write("VDATA:\n")
        for _ in sorted(pointsfloat):
            output_file.write("12 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -10.0 0.0 0.0\n")


def beam_2d_quad(name, x, y, z, t):
    delta = 0.5
    with open(geopath + name + "2DQuad.geo", 'w') as output_file:
        D = int(1 / delta)
        # delta *= 0.001

        points = {}
        pointsfloat = {}
        cells = {}

        # Points
        active = 0
        n = 0
        output_file.write("POINTS:\n")
        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                output_file.write('{0:.4f}'.format(i * delta) + " " + '{0:.4f}'.format(j * delta) + "\n")
                points[(i, j)] = " " + str(n)
                pointsfloat[n] = (i, j)
                n += 1

        for i in range(0, 2):
            for j in range(1, t * D + 1):
                output_file.write(
                    '{0:.4f}'.format(x - (1 - i) * delta) + " " + '{0:.4f}'.format(y + (j * delta)) + "\n")
                points[(x * D - 1 + i, (y * D) + j)] = " " + str(n)
                pointsfloat[n] = (x * D - 1 + i, (y * D) + j)
                n += 1

                output_file.write(
                    '{0:.4f}'.format(x - (1 - i) * delta) + " " + '{0:.4f}'.format(- (j * delta)) + "\n")
                points[(x * D - 1 + i, -j)] = " " + str(n)
                pointsfloat[n] = (x * D - 1 + i, -j)
                n += 1

        # Cells
        output_file.write("CELLS:\n")
        for i in range(0, x * D):
            for j in range(0, y * D):
                output_file.write(
                    "4 0000" + points[(i, j)] + points[(i + 1, j)] + points[(i + 1, j + 1)] + points[(i, j + 1)] + "\n")

        for j in range(0, t * D):
            output_file.write(
                "4 0000" + points[(x * D - 1, y * D + j)] + points[(x * D, y * D + j)] + points[
                    (x * D, y * D + j + 1)] + points[(x * D - 1, y * D + j + 1)] + "\n")
            output_file.write(
                "4 0000" + points[(x * D - 1, -j - 1)] + points[(x * D, -j - 1)] + points[(x * D, -j)] + points[
                    (x * D - 1, -j)] + "\n")

        # Faces
        output_file.write("FACES:\n")
        for j in range(y * D):
            output_file.write("2 199" + points[(0, j)] + points[(0, j + 1)] + "\n")
            output_file.write("2 230" + points[(x * D, j)] + points[(x * D, j + 1)] + "\n")

        for j in range(t * D):
            output_file.write("2 230" + points[(x * D, y * D + j)] + points[(x * D, y * D + j + 1)] + "\n")
            output_file.write("2 230" + points[(x * D, -j - 1)] + points[(x * D, -j)] + "\n")

        output_file.write("2 231" + points[(x * D - 1, (y + t) * D)] + points[(x * D, (y + t) * D)] + "\n")
        output_file.write("2 231" + points[(x * D - 1, -t * D)] + points[(x * D, -t * D)] + "\n")

        # ORIENTATION
        output_file.write("VDATA:\n")
        for _ in sorted(pointsfloat):
            output_file.write("12 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -10.0 0.0 0.0\n")


def beam_3d_tet(name, x, y, z):
    with open(geopath + name + "3DTet.geo", 'w') as output_file:

        points = {}
        pointsfloat = {}
        cells = {}

        # Points
        active = 0
        n = 0
        output_file.write("POINTS:\n")

        for k in range(0, z + 1):
            for j in range(0, y + 1):
                for i in range(0, x + 1):
                    output_file.write(
                        '{0:.4f}'.format(i) + " " + '{0:.4f}'.format(j) + " " + '{0:.4f}'.format(k) + "\n")
                    points[(i, j, k)] = " " + str(n)
                    pointsfloat[n] = (i, j, k)
                    n += 1

        for i in range(x):
            for j in range(y):
                output_file.write(
                    '{0:.4f}'.format(i + 0.5) + " " + '{0:.4f}'.format(j + 0.5) + " " + '{0:.4f}'.format(0) + "\n")
                points[(i + 0.5, j + 0.5, 0)] = " " + str(n)
                pointsfloat[n] = (i + 0.5, j + 0.5, 0)
                n += 1

        for j in range(y):
            for k in range(z):
                output_file.write(
                    '{0:.4f}'.format(0) + " " + '{0:.4f}'.format(j + 0.5) + " " + '{0:.4f}'.format(k + 0.5) + "\n")
                points[(0, j + 0.5, k + 0.5)] = " " + str(n)
                pointsfloat[n] = (0, j + 0.5, k + 0.5)
                n += 1

        for i in range(x):
            for k in range(z):
                output_file.write(
                    '{0:.4f}'.format(i + 0.5) + " " + '{0:.4f}'.format(0) + " " + '{0:.4f}'.format(k + 0.5) + "\n")
                points[(i + 0.5, 0, k + 0.5)] = " " + str(n)
                pointsfloat[n] = (i + 0.5, 0, k + 0.5)
                n += 1

        for i in range(0, x):
            for j in range(0, y):
                for k in range(0, z):
                    output_file.write(
                        '{0:.4f}'.format(i + 0.5) + " " + '{0:.4f}'.format(j + 0.5) + " " + '{0:.4f}'.format(
                            k + 1) + "\n")
                    output_file.write(
                        '{0:.4f}'.format(i + 0.5) + " " + '{0:.4f}'.format(j + 1) + " " + '{0:.4f}'.format(
                            k + 0.5) + "\n")
                    output_file.write(
                        '{0:.4f}'.format(i + 1) + " " + '{0:.4f}'.format(j + 0.5) + " " + '{0:.4f}'.format(
                            k + 0.5) + "\n")
                    output_file.write(
                        '{0:.4f}'.format(i + 0.5) + " " + '{0:.4f}'.format(j + 0.5) + " " + '{0:.4f}'.format(
                            k + 0.5) + "\n")
                    points[(i + 0.5, j + 0.5, k + 1)] = " " + str(n)
                    points[(i + 0.5, j + 1, k + 0.5)] = " " + str(n + 1)
                    points[(i + 1, j + 0.5, k + 0.5)] = " " + str(n + 2)
                    points[(i + 0.5, j + 0.5, k + 0.5)] = " " + str(n + 3)
                    pointsfloat[n] = (i + 0.5, j + 0.5, k + 1)
                    pointsfloat[n + 1] = (i + 0.5, j + 1, k + 0.5)
                    pointsfloat[n + 2] = (i + 1, j + 0.5, k + 0.5)
                    pointsfloat[n + 3] = (i + 0.5, j + 0.5, k + 0.5)
                    n += 4

        # Cells
        m = 0
        output_file.write("CELLS:\n")
        for i in range(0, x):
            for j in range(0, y):
                for k in range(0, z):

                    tetras = tt.cube_to_tetras24(points[(i, j, k)], points[(i + 1, j, k)],
                                                 points[(i + 1, j + 1, k)], points[(i, j + 1, k)],
                                                 points[(i, j, k + 1)], points[(i + 1, j, k + 1)],
                                                 points[(i + 1, j + 1, k + 1)], points[(i, j + 1, k + 1)],
                                                 points[(i + 0.5, j + 0.5, k)], points[(i + 0.5, j, k + 0.5)],
                                                 points[(i + 1, j + 0.5, k + 0.5)], points[(i + 0.5, j + 1, k + 0.5)],
                                                 points[(i, j + 0.5, k + 0.5)], points[(i + 0.5, j + 0.5, k + 1)],
                                                 points[(i + 0.5, j + 0.5, k + 0.5)]
                                                 )

                    for t in range(len(tetras)):
                        output_file.write(tetras[t])
                        cells[m + t] = (i, j, k, t)

                    m += len(tetras)

        # Faces
        output_file.write("FACES:\n")
        ''' Insert Dirichlet Boundary'''
        for j in range(y):
            for k in range(z):
                tri = tt.square_to_triangles4("3 199", points[(0, j, k)], points[(0, j + 1, k)],
                                              points[(0, j + 1, k + 1)], points[(0, j, k + 1)],
                                              points[(0, j + 0.5, k + 0.5)])

                tri += tt.square_to_triangles4("3 230", points[(x, j, k)], points[(x, j + 1, k)],
                                               points[(x, j + 1, k + 1)], points[(x, j, k + 1)],
                                               points[(x, j + 0.5, k + 0.5)])

                for t in tri:
                    output_file.write(t)

        '''Insert Pressure Boundary on Top and Bottom'''
        for i in range(x):
            for j in range(y):
                tri = tt.square_to_triangles4("3 231", points[(i + 1, j + 1, z)], points[(i + 1, j, z)],
                                              points[(i, j, z)], points[(i, j + 1, z)], points[(i + 0.5, j + 0.5, z)])
                tri += tt.square_to_triangles4("3 232", points[(i + 1, j + 1, 0)], points[(i, j + 1, 0)],
                                               points[(i, j, 0)], points[(i + 1, j, 0)], points[(i + 0.5, j + 0.5, 0)])

                for t in tri:
                    output_file.write(t)

        for i in range(x):
            for k in range(z):
                tri = tt.square_to_triangles4("3 0", points[(i, 0, k)], points[(i + 1, 0, k)],
                                              points[(i + 1, 0, k + 1)], points[(i, 0, k + 1)],
                                              points[(i + 0.5, 0, k + 0.5)])
                tri += tt.square_to_triangles4("3 0", points[(i, y, k)], points[(i + 1, y, k)],
                                               points[(i + 1, y, k + 1)], points[(i, y, k + 1)],
                                               points[(i + 0.5, y, k + 0.5)])

                for t in tri:
                    output_file.write(t)

        # ORIENTATION
        output_file.write("VDATA:\n")
        for i in sorted(pointsfloat):
            output_file.write("12 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -10.0 0.0 0.0\n")


def beam_3d_quad(name, x, y, z):
    delta = 0.5
    with open(geopath + name + "3DQuad.geo", 'w') as output_file:
        D = int(1 / delta)

        points = {}
        pointsfloat = {}
        cells = {}

        # Points
        active = 0
        n = 0
        output_file.write("POINTS:\n")

        for i in range(0, x * D + 1):
            for j in range(0, y * D + 1):
                for k in range(0, z * D + 1):
                    output_file.write(
                        '{0:.4f}'.format(i * delta) + " " + '{0:.4f}'.format(j * delta) + " " + '{0:.4f}'.format(
                            k * delta) + "\n")
                    points[(i, j, k)] = " " + str(n)
                    pointsfloat[n] = (i, j, k)
                    n += 1

        # Cells
        m = 0
        output_file.write("CELLS:\n")
        for i in range(0, x * D):
            for j in range(0, y * D):
                for k in range(0, z * D):
                    output_file.write("8 0000" + points[(i, j, k)] + points[(i, j + 1, k)] +
                                      points[(i + 1, j + 1, k)] + points[(i + 1, j, k)] +
                                      points[(i, j, k + 1)] + points[(i, j + 1, k + 1)] +
                                      points[(i + 1, j + 1, k + 1)] + points[(i + 1, j, k + 1)] + "\n")

        # Faces
        output_file.write("FACES:\n")
        ''' Insert Dirichlet Boundary'''
        for j in range(y * D):
            for k in range(z * D):
                output_file.write(
                    "4 199" + points[(0, j, k)] + points[(0, j + 1, k)] + points[(0, j + 1, k + 1)] + points[
                        (0, j, k + 1)] + "\n")

                output_file.write(
                    "4 230" + points[(x * D, j, k)] + points[(x * D, j + 1, k)] + points[(x * D, j + 1, k + 1)] +
                    points[(x * D, j, k + 1)] + "\n")

        '''Insert Pressure Boundary on Top and Bottom'''
        for i in range(x * D):
            for j in range(y * D):
                output_file.write("4 231" + points[(i + 1, j + 1, z * D)] + points[(i, j + 1, z * D)] +
                                  points[(i, j, z * D)] + points[(i + 1, j, z * D)] + "\n")
                output_file.write("4 232" + points[(i + 1, j + 1, 0)] + points[(i, j + 1, 0)] +
                                  points[(i, j, 0)] + points[(i + 1, j, 0)] + "\n")
        # ORIENTATION
        output_file.write("VDATA:\n")
        for _ in sorted(pointsfloat):
            output_file.write("12 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -10.0 0.0 0.0\n")


def generate_beams(name, x, y, z, t):
    # beam_2d_tet(name, x, y, z, t)
    # beam_3d_tet(name, x, y, z, t)
    beam_2d_quad(name, x, y, z, t)


# beam_3d_quad(name, x, y, z, t)


if __name__ == "__main__":
    delta_b = 1 / pow(2, refinement)
    generate_beams("TBeam", 10, 1, 1, 4)
