from old import tetra_tools as tt

geopath = "../../geo/"
datapath = "../cardiacmechanics/data/"

refinement = 0


def lshape2d(delta):
    with open(geopath + "LShape2DTet.geo", 'w') as output_file:
        D = int(1 / delta)
        # delta *= 0.001

        points = {}
        pointsfloat = {}
        cells = {}

        # Points
        active = 0
        n = 0
        output_file.write("POINTS:\n")
        for i in range(0, D + 1):
            for j in range(0, D + 1):
                # Upper left
                if (-i, j) not in points:
                    output_file.write('{0:.4f}'.format(-i * delta) + " " + '{0:.4f}'.format(j * delta) + "\n")
                    points[(-i, j)] = " " + str(n)
                    pointsfloat[n] = (-i, j)
                    n += 1
                # Upper right
                if (i, j) not in points:
                    output_file.write('{0:.4f}'.format(i * delta) + " " + '{0:.4f}'.format(j * delta) + "\n")
                    points[(i, j)] = " " + str(n)
                    pointsfloat[n] = (i, j)
                    n += 1
                # Lower right
                if (i, -j) not in points:
                    output_file.write('{0:.4f}'.format(i * delta) + " " + '{0:.4f}'.format(-j * delta) + "\n")
                    points[(i, -j)] = " " + str(n)
                    pointsfloat[n] = (i, -j)
                    n += 1

        # Cells
        output_file.write("CELLS:\n")
        tetras = []
        for i in range(0, D):
            for j in range(0, D):
                # Upper left
                tetras += (tt.square_to_triangles(-i + j, "3 0000", points[(-i, j)], points[(-i, j + 1)],
                                               points[(-(i + 1), j + 1)], points[(-(i + 1), j)]))
                # Upper right
                tetras += (tt.square_to_triangles(i + j, "3 0000", points[(i, j)], points[(i, j + 1)],
                                               points[(i + 1, j + 1)], points[(i + 1, j)]))
                # Lower right
                tetras += (tt.square_to_triangles(i - j, "3 0000", points[(i, -j)], points[(i, -(j + 1))],
                                               points[(i + 1, -(j + 1))], points[(i + 1, -j)]))
        for t in tetras:
            output_file.write(t)

        # Faces
        output_file.write("FACES:\n")
        for j in range(D):
            output_file.write("2 1" + points[(-D, j)] + points[(-D, j + 1)] + "\n")
            output_file.write("2 1" + points[(D, j)] + points[(D, j + 1)] + "\n")
            output_file.write("2 1" + points[(D, -j)] + points[(D, -(j + 1))] + "\n")
            output_file.write("2 1" + points[(0, -j)] + points[(0, -(j + 1))] + "\n")

        for i in range(D):
            output_file.write("2 1" + points[(i, -D)] + points[(i + 1, -D)] + "\n")
            output_file.write("2 1" + points[(i, D)] + points[(i + 1, D)] + "\n")
            output_file.write("2 1" + points[(-i, D)] + points[(-(i + 1), D)] + "\n")
            output_file.write("2 1" + points[(-i, 0)] + points[(-(i + 1), 0)] + "\n")

        # ORIENTATION
        output_file.write("VDATA:\n")
        for _ in sorted(pointsfloat):
            output_file.write("12 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -10.0 0.0 0.0\n")

    with open(geopath + "LShape2DQuad.geo", 'w') as output_file:
        D = int(1 / delta)
        # delta *= 0.001

        points = {}
        pointsfloat = {}
        cells = {}

        # Points
        active = 0
        n = 0
        output_file.write("POINTS:\n")
        for i in range(0, D + 1):
            for j in range(0, D + 1):
                # Upper left
                if (-i, j) not in points:
                    output_file.write('{0:.4f}'.format(-i * delta) + " " + '{0:.4f}'.format(j * delta) + "\n")
                    points[(-i, j)] = " " + str(n)
                    pointsfloat[n] = (-i, j)
                    n += 1
                # Upper right
                if (i, j) not in points:
                    output_file.write('{0:.4f}'.format(i * delta) + " " + '{0:.4f}'.format(j * delta) + "\n")
                    points[(i, j)] = " " + str(n)
                    pointsfloat[n] = (i, j)
                    n += 1
                # Lower right
                if (i, -j) not in points:
                    output_file.write('{0:.4f}'.format(i * delta) + " " + '{0:.4f}'.format(-j * delta) + "\n")
                    points[(i, -j)] = " " + str(n)
                    pointsfloat[n] = (i, -j)
                    n += 1

        # Cells
        output_file.write("CELLS:\n")
        for i in range(0, D):
            for j in range(0, D):
                # Upper left
                output_file.write("4 0000" + points[(-i, j)] + points[(-i, j + 1)] + points[(-(i + 1), j + 1)] + points[
                    (-(i + 1), j)] + "\n")
                # Upper right
                output_file.write(
                    "4 0000" + points[(i, j)] + points[(i, j + 1)] + points[(i + 1, j + 1)] + points[(i + 1, j)] + "\n")
                # Lower right
                output_file.write(
                    "4 0000" + points[(i, -j)] + points[(i, -(j + 1))] + points[(i + 1, -(j + 1))] + points[
                        (i + 1, -j)] + "\n")

        # Faces
        output_file.write("FACES:\n")
        for j in range(D):
            output_file.write("2 1" + points[(-D, j)] + points[(-D, j + 1)] + "\n")
            output_file.write("2 1" + points[(D, j)] + points[(D, j + 1)] + "\n")
            output_file.write("2 1" + points[(D, -j)] + points[(D, -(j + 1))] + "\n")
            output_file.write("2 1" + points[(0, -j)] + points[(0, -(j + 1))] + "\n")

        for i in range(D):
            output_file.write("2 1" + points[(i, -D)] + points[(i + 1, -D)] + "\n")
            output_file.write("2 1" + points[(i, D)] + points[(i + 1, D)] + "\n")
            output_file.write("2 1" + points[(-i, D)] + points[(-(i + 1), D)] + "\n")
            output_file.write("2 1" + points[(-i, 0)] + points[(-(i + 1), 0)] + "\n")

        # ORIENTATION
        output_file.write("VDATA:\n")
        for _ in sorted(pointsfloat):
            output_file.write("12 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -10.0 0.0 0.0\n")


def lshape3d_tet(delta):
    with open(geopath + "LShape3DTet.geo", 'w') as output_file:
        D = int(1 / delta)
        # delta *= 0.001

        points = {}
        pointsfloat = {}
        cells = {}

        # Points
        active = 0
        n = 0
        output_file.write("POINTS:\n")
        for k in range(0, D + 1):
            for i in range(0, D + 1):
                for j in range(0, D + 1):
                    # Upper left
                    if (-i, j, k) not in points:
                        output_file.write('{0:.4f}'.format(-i * delta) + " " +
                                          '{0:.4f}'.format(j * delta) + " " +
                                          '{0:.4f}'.format(k * delta) + "\n")
                        points[(-i, j, k)] = " " + str(n)
                        pointsfloat[n] = (-i, j, k)
                        n += 1
                    # Upper right
                    if (i, j, k) not in points:
                        output_file.write('{0:.4f}'.format(i * delta) + " " +
                                          '{0:.4f}'.format(j * delta) + " " +
                                          '{0:.4f}'.format(k * delta) + "\n")
                        points[(i, j, k)] = " " + str(n)
                        pointsfloat[n] = (i, j, k)
                        n += 1
                    # Lower right
                    if (i, -j, k) not in points:
                        output_file.write('{0:.4f}'.format(i * delta) + " " +
                                          '{0:.4f}'.format(-j * delta) + " " +
                                          '{0:.4f}'.format(k * delta) + "\n")
                        points[(i, -j, k)] = " " + str(n)
                        pointsfloat[n] = (i, -j, k)
                        n += 1

        # Cells
        output_file.write("CELLS:\n")
        tetras = []
        for k in range(0, D):
            for i in range(0, D):
                for j in range(0, D):
                    # Upper left
                    tetras += (
                        tt.cube_to_tetras(points[(-i, j, k)], points[(-i, j + 1, k)], points[(-(i + 1), j + 1, k)],
                                          points[(-(i + 1), j, k)], points[(-i, j, k + 1)], points[(-i, j + 1, k + 1)],
                                          points[(-(i + 1), j + 1, k + 1)], points[(-(i + 1), j, k + 1)]))
                    # Upper right
                    tetras += (tt.cube_to_tetras(points[(i, j, k)], points[(i, j + 1, k)], points[(i + 1, j + 1, k)],
                                                 points[(i + 1, j, k)], points[(i, j, k + 1)],
                                                 points[(i, j + 1, k + 1)], points[(i + 1, j + 1, k + 1)],
                                                 points[(i + 1, j, k + 1)]))
                    # Lower right
                    tetras += (
                        tt.cube_to_tetras(points[(i, -j, k)], points[(i, -(j + 1), k)], points[(i + 1, -(j + 1), k)],
                                          points[(i + 1, -j, k)], points[(i, -j, k + 1)], points[(i, -(j + 1), k + 1)],
                                          points[(i + 1, -(j + 1), k + 1)], points[(i + 1, -j, k + 1)]))
        for t in tetras:
            output_file.write(t)

        # Faces
        output_file.write("FACES:\n")
        tri = []
        for k in range(D):
            for j in range(D):
                tri += tt.square_to_triangles(-D + j + k, "3 1", points[(-D, j, k)], points[(-D, j + 1, k)],
                                           points[(-D, j + 1, k + 1)], points[(-D, j, k + 1)])
                tri += tt.square_to_triangles(D + j + k, "3 1", points[(D, j, k)], points[(D, j + 1, k)],
                                           points[(D, j + 1, k + 1)], points[(D, j, k + 1)])
                tri += tt.square_to_triangles(D - j + k, "3 1", points[(D, -j, k)], points[(D, -(j + 1), k)],
                                           points[(D, -(j + 1), k + 1)], points[(D, -j, k + 1)])
                tri += tt.square_to_triangles(- j + k, "3 1", points[(0, -j, k)], points[(0, -(j + 1), k)],
                                           points[(0, -(j + 1), k + 1)], points[(0, -j, k + 1)])

            for i in range(D):
                tri += tt.square_to_triangles(-D + i + k, "3 1", points[(i, -D, k)], points[(i + 1, -D, k)],
                                           points[(i + 1, -D, k + 1)], points[(i, -D, k + 1)])
                tri += tt.square_to_triangles(D + i + k, "3 1", points[(i, D, k)], points[(i + 1, D, k)],
                                           points[(i + 1, D, k + 1)], points[(i, D, k + 1)])
                tri += tt.square_to_triangles(D - i + k, "3 1", points[(-i, D, k)], points[(-(i + 1), D, k)],
                                           points[(-(i + 1), D, k + 1)], points[(-i, D, k + 1)])
                tri += tt.square_to_triangles(- i + k, "3 1", points[(-i, 0, k)], points[(-(i + 1), 0, k)],
                                           points[(-(i + 1), 0, k + 1)], points[(-i, 0, k + 1)])

        for i in range(D):
            for j in range(D):
                tri += tt.square_to_triangles(-i + j, "3 1", points[(-i, j, 0)], points[(-i, j + 1, 0)],
                                           points[(-(i + 1), j + 1, 0)], points[(-(i + 1), j, 0)])
                tri += tt.square_to_triangles(i + j, "3 1", points[(i, j, 0)], points[(i + 1, j, 0)],
                                           points[((i + 1), j + 1, 0)], points[(i, j + 1, 0)])
                tri += tt.square_to_triangles(i - j, "3 1", points[(i, -j, 0)], points[(i, -(j + 1), 0)],
                                           points[(i + 1, -(j + 1), 0)], points[(i + 1, -j, 0)])
                tri += tt.square_to_triangles(-i + j, "3 1", points[(-i, j, D)], points[(-i, j + 1, D)],
                                           points[(-(i + 1), j + 1, D)], points[(-(i + 1), j, D)])
                tri += tt.square_to_triangles(i + j, "3 1", points[(i, j, D)], points[(i + 1, j, D)],
                                           points[((i + 1), j + 1, D)], points[(i, j + 1, D)])
                tri += tt.square_to_triangles(i - j, "3 1", points[(i, -j, D)], points[(i, -(j + 1), D)],
                                           points[(i + 1, -(j + 1), D)], points[(i + 1, -j, D)])

        for t in tri:
            output_file.write(t)

        # ORIENTATION
        output_file.write("VDATA:\n")
        for i in sorted(pointsfloat):
            output_file.write("12 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -10.0 0.0 0.0\n")


def lshape3d_hex(delta):
    with open(geopath + "LShape3DQuad.geo", 'w') as output_file:
        D = int(1 / delta)

        points = {}
        pointsfloat = {}
        cells = {}

        # Points
        n = 0
        output_file.write("POINTS:\n")
        for k in range(0, D + 1):
            for i in range(0, D + 1):
                for j in range(0, D + 1):
                    # Upper left
                    if (-i, j, k) not in points:
                        output_file.write('{0:.4f}'.format(-i * delta) + " " +
                                          '{0:.4f}'.format(j * delta) + " " +
                                          '{0:.4f}'.format(k * delta) + "\n")
                        points[(-i, j, k)] = " " + str(n)
                        pointsfloat[n] = (-i, j, k)
                        n += 1
                    # Upper right
                    if (i, j, k) not in points:
                        output_file.write('{0:.4f}'.format(i * delta) + " " +
                                          '{0:.4f}'.format(j * delta) + " " +
                                          '{0:.4f}'.format(k * delta) + "\n")
                        points[(i, j, k)] = " " + str(n)
                        pointsfloat[n] = (i, j, k)
                        n += 1
                    # Lower right
                    if (i, -j, k) not in points:
                        output_file.write('{0:.4f}'.format(i * delta) + " " +
                                          '{0:.4f}'.format(-j * delta) + " " +
                                          '{0:.4f}'.format(k * delta) + "\n")
                        points[(i, -j, k)] = " " + str(n)
                        pointsfloat[n] = (i, -j, k)
                        n += 1

        # Cells
        output_file.write("CELLS:\n")
        for k in range(0, D):
            for i in range(0, D):
                for j in range(0, D):
                    # Upper left
                    output_file.write("8 0000" + points[(-i, j, k)] + points[(-i, j + 1, k)] +
                                      points[(-(i + 1), j + 1, k)] + points[(-(i + 1), j, k)] +
                                      points[(-i, j, k + 1)] + points[(-i, j + 1, k + 1)] +
                                      points[(-(i + 1), j + 1, k + 1)] + points[(-(i + 1), j, k + 1)] + "\n")
                    # Upper right
                    output_file.write("8 0000" + points[(i, j, k)] + points[(i, j + 1, k)] +
                                      points[(i + 1, j + 1, k)] + points[(i + 1, j, k)] +
                                      points[(i, j, k + 1)] + points[(i, j + 1, k + 1)] +
                                      points[(i + 1, j + 1, k + 1)] + points[(i + 1, j, k + 1)] + "\n")
                    # Lower right
                    output_file.write("8 0000" + points[(i, -j, k)] + points[(i, -(j + 1), k)] +
                                      points[(i + 1, -(j + 1), k)] + points[(i + 1, -j, k)] +
                                      points[(i, -j, k + 1)] + points[(i, -(j + 1), k + 1)] +
                                      points[(i + 1, -(j + 1), k + 1)] + points[(i + 1, -j, k + 1)] + "\n")

        # Faces
        output_file.write("FACES:\n")
        for k in range(D):
            for j in range(D):
                output_file.write("4 1" + points[(-D, j, k)] + points[(-D, j + 1, k)] +
                                  points[(-D, j + 1, k + 1)] + points[(-D, j, k + 1)] + "\n")
                output_file.write("4 1" + points[(D, j, k)] + points[(D, j + 1, k)] +
                                  points[(D, j + 1, k + 1)] + points[(D, j, k + 1)] + "\n")
                output_file.write("4 1" + points[(D, -j, k)] + points[(D, -(j + 1), k)] +
                                  points[(D, -(j + 1), k + 1)] + points[(D, -j, k + 1)] + "\n")
                output_file.write("4 1" + points[(0, -j, k)] + points[(0, -(j + 1), k)] +
                                  points[(0, -(j + 1), k + 1)] + points[(0, -j, k + 1)] + "\n")

            for i in range(D):
                output_file.write("4 1" + points[(i, -D, k)] + points[(i + 1, -D, k)] +
                                  points[(i + 1, -D, k + 1)] + points[(i, -D, k + 1)] + "\n")
                output_file.write("4 1" + points[(i, D, k)] + points[(i + 1, D, k)] +
                                  points[(i + 1, D, k + 1)] + points[(i, D, k + 1)] + "\n")
                output_file.write("4 1" + points[(-i, D, k)] + points[(-(i + 1), D, k)] +
                                  points[(-(i + 1), D, k + 1)] + points[(-i, D, k + 1)] + "\n")
                output_file.write("4 1" + points[(-i, 0, k)] + points[(-(i + 1), 0, k)] +
                                  points[(-(i + 1), 0, k + 1)] + points[(-i, 0, k + 1)] + "\n")

        # ORIENTATION
        output_file.write("VDATA:\n")
        for i in sorted(pointsfloat):
            output_file.write("12 1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 1.0 -10.0 0.0 0.0\n")


if __name__ == "__main__":
    delta = 1
    lshape2d(delta)
    lshape3d_tet(delta)
    lshape3d_hex(delta)
