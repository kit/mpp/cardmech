from utility.geo import Geometry
import re


def readInfos(file):
    all=[]
    points=[]
    cells=[]
    faces=[]
    p_Data=[]
    c_Data=[]
    f = open(file)
    for l in f:
        l = l.split()
        all.append(l)
        
    stopindex=0 
    
    for index in range(1,len(all)):
        if all[index][0]!='CELLS:':
            points.append(all[index])
        else:
            stopindex=index
            break
    
    for index in range(stopindex+1,len(all)):
        if all[index][0]!='FACES:':
                cells.append(all[index])
        else:
            stopindex=index
            break
    
    for index in range(stopindex+1,len(all)):
        if all[index][0]!='VDATA:':
                faces.append(all[index])
        else:
            stopindex=index
            break
    for index in range(stopindex+1,len(all)):
        if all[index][0]!='CDATA:':
            print(all[index][1:])
            p_Data.append(all[index][1:])
        else:
            stopindex=index
            break
        
    for index in range(stopindex+1,len(all)):
        c_Data.append(all[index][1:])
       
        
    f.close()

    print(len(points),len(cells),len(p_Data),len(c_Data))
    return points,cells,p_Data,c_Data

    
def addExcitation(source,save):
    
    grid=Geometry(save, "../../geo/", True)
    points, cells, point_data, cell_data =readInfos(source)
    for p in points:
        grid.add_point(p)
    
    for index in range(len(cells)):
        c=[]
        for i in range(2,len(cells[index])):
            c.append(int(cells[index][i]))
        grid.add_cell(c)
        grid.add_subdomain(index, cells[index][1])
    grid.init_data()
    for index in range(len(cells)):
        grid.add_cdata(index,cell_data[index])
    
    #Punktliste:
    exPointsID=[]
    default=-10.0
    counter =0
    for index in range(len(point_data)):
        if float(point_data[index][0])==0.0:
            counter+=1
        if float(point_data[index][0])!=default:
            exPointsID.append(index)
    print(counter)
    #add points to list for cell activation
    exCells=[]
    for cell in cells:
        for id in cell:
            if int(id) in exPointsID:
                exCells.append(cell)
                break
    
    
    for cell in exCells:
        for i in range(2,len(cell)):
            if int(cell[i]) not in exPointsID:
                exPointsID.append(int(cell[i]))

    for pi,points in grid.points.items():
            if pi in exPointsID:
                grid.add_vdata(pi, [0.0, 0.003, 30.0])
            else:
                grid.add_vdata(pi, [-10.0,0.0,0.0])
    grid.save()        

if __name__ == "__main__":
    filename='geo/TestEllipsoid2dExcitation.geo'
    #filename='geo/CuttedEllipsoid2LayerOriented.geo'
    savename = 'TestEllipsoid3dExcitation'
    
    addExcitation(filename,savename)
