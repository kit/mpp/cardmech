import numpy as np
from vtk.util.numpy_support import vtk_to_numpy
from vtk.vtkIOXML import vtkXMLUnstructuredGridReader

from utility.geo import Geometry

def activationByPointData(geometry,point_indicies,path,filename, default_data=[-10.0, 0.0, 0.0]):
    reader = vtkXMLUnstructuredGridReader()
    reader.SetFileName(path + filename + ".vtu")
    reader.Update()
    data = reader.GetOutput()
    point_data = data.GetPointData()
    for pi in point_indicies:
        d = point_data.GetAbstractArray(pi).GetNumberOfComponents()
        data = vtk_to_numpy(point_data.GetAbstractArray(pi))
    
    for pi,point in geometry.points.items():
        if data[pi] >=0.0:
            geometry.add_vdata(pi, [data[pi], 0.003, 30.0])
        else:
            geometry.add_vdata(pi, default_data)
def activation_ventricle(geometry, default_data=[-10.0, 0.0, 0.0]):
    endocard_points = []
    for fi, face in geometry.faces.items():
        if geometry.bnd[fi] == 230:
            for pi in face:
                endocard_points.append(pi)
    endocard_points = list(dict.fromkeys(endocard_points))

    #endocard_points = list(dict.fromkeys([pi for face in geometry.faces.values() for pi in face]))

    min_z = min(geometry.points[pi][2] for pi in endocard_points)
    max_z = max(geometry.points[pi][2] for pi in endocard_points)

    def exc_time(point):
        rel_z = (point[2] - min_z) / (max_z - min_z)
        return rel_z * 0.02

    for pi, point in geometry.points.items():
        if pi in endocard_points:
            geometry.add_vdata(pi, [exc_time(point), 0.003, 20.0])
        else:
            geometry.add_vdata(pi, default_data)
def apex_endo_activaton(geometry,default_data=[-10.0, 0.0, 0.0]):
    endocard_points = []
    #for fi, face in geometry.faces.items():
        #if geometry.bnd[fi] == 230:
            #for pi in face:
                #endocard_points.append(pi)
    #endocard_points = list(dict.fromkeys(endocard_points))
    
    for pi, point in geometry.points.items():
        geometry.add_vdata(pi, default_data)
        
        
def no_activaton(geometry,default_data=[-10.0, 0.0, 0.0]):
    for pi, point in geometry.points.items():
        geometry.add_vdata(pi, default_data)


def instant_activation(geometry, default_data=[0.0, 0.003, 30]):
    for pi, point in geometry.points.items():
        geometry.add_vdata(pi, default_data)

def activationByPointList(geometry,isExtended,default_data=[-10.0, 0.0, 0.0]):
    print(isExtended)
    f = open('vtk/excitationEllipsoid2Layer.txt')
    exPoints=[]
    for l in f:
        l = l.split()
        exPoints.append([float(l[0]),float(l[1]),float(l[2])])
    f.close()
    exPointsIDs=[]

    for pi, point in geometry.points.items():
        if point in exPoints:
            exPointsIDs.append(pi)
            print(point)
    print(exPointsIDs)
    if isExtended:
        extendedPoints=[]
        for ci, cell in geometry.cells.items():
            for pi in cell:
                i=1#print(ci,cell,pi,point)
    else:
        for pi, point in geometry.points.items():
            if point in exPoints:
                geometry.add_vdata(pi, [0.0, 0.003, 30.0])
            else:
                geometry.add_vdata(pi, default_data)




def robin_bnd_ventricle(path, geometry, surface_data):
    surface_points = {}
    with open(path + surface_data) as datafile:
        lines = [line.split(',') for line in datafile.readlines()]
        for line in lines[1:]:
            surface_points[int(line[0])] = [float(line[-3]), float(line[-2]), float(line[-1])]

    for cell in geometry.cells.keys():
        for face in geometry.cell_faces(cell):
            if (all(p in surface_points for p in face)):
                geometry.add_bnd_condition(geometry.add_face([p for p in face]), 330)


def activation_acnd(path, geometry):
    # print("Extracting activation data")
    activations = {}
    with open(path + 'ActivationT4_Atria' + ".acnd") as excitefile:
        for fullline in excitefile.readlines():
            line = fullline.split()
            # print(line)
            # Excitation time, duration and amplitude
            activations[int(line[0])] = [float(line[4]), float(line[3]),30.0]# float(line[1])]
    with open(path + 'ActivationT4_Ventricle' + ".acnd") as excitefile:
        for fullline in excitefile.readlines():
            line = fullline.split()
            # Excitation time, duration and amplitude
            activations[int(line[0])] = [float(line[4]), float(line[3]), 30.0]# float(line[1])]
    # print("Data extraction complete")
    for a in activations:
        geometry.add_vdata(a, activations[a])


class VtuGeometry:

    def __init__(self, path, vtu_name, geo_name):
        self.path = path
        self.vtu = vtu_name
        self.geo = Geometry(geo_name, "../../geo/", True)
        self.selection = []
        self.remove_cells = []
        self.remove_faces = []
        self.scale = 1.0
        # LV, RV, LA, RA, Valves, Dirichlet boundaries, Tissue
        self.material_map = {k: k for k in range(200)}
        for k in range(11):
            self.material_map[k] = 1000 * k
        self.material_map[60] = 11000

    def rescale(self, scale):
        self.scale = scale

    def remap(self, material_map):
        self.material_map = material_map

    def read_vtu(self):
        # Read File
        reader = vtkXMLUnstructuredGridReader()
        reader.SetFileName(self.path + self.vtu + ".vtu")
        reader.Update()
        data = reader.GetOutput()

        # Extract Points
        points = vtk_to_numpy(data.GetPoints().GetData())
        # Extract Cells
        cells = vtk_to_numpy(data.GetCells().GetData())

        point_data = data.GetPointData()
        cell_data = data.GetCellData()
        return points, cells, point_data, cell_data

    def add_points(self, nodes, scale):
        for node in nodes:
            self.geo.add_point([p * scale for p in node])

    def add_cells(self, tetras):
        t = tetras.size
        j, k = 0, 0
        while j < t:
            i = tetras[j]
            if i == 10 or i == 4:
                self.geo.add_cell([p for p in tetras[j + 1:j + 5]])
            elif i == 3:
                self.geo.add_face([p for p in tetras[j + 1:j + i + 1]])
            j += i + 1

    def add_material_subdomain(self, materials):
        for i in self.geo.cells:
            mat = self.material_map[materials[i]]
            if self.is_included(mat):
                self.geo.add_subdomain(i, mat)  # 0 für LandEllipsoid

    def add_dirichlet(self, fixation):
        # Check if all nodes of a face are fixated.
        # If so, create the dirichlet face.
        all_faces = {}
        for c in range(self.geo.cell_count()):
            for face in self.geo.cell_faces(c):
                key = frozenset(face)
                all_faces[key] = np.append(199, face) if min(fixation[p] for p in face) > 0 else np.append(0, face)
        for face in all_faces.values():
            if face[0] == 199:
                face_index = self.geo.add_face(face[1:])
                self.geo.add_bnd_condition(face_index, face[0])

    def add_face_bnd(self, materials):
        shift = self.geo.cell_count()
        for i in range(self.geo.face_count()):
            try:
                mat = self.material_map[materials[shift + i]]  #self.material_map[materials[i]] für LandEllipsoid
                if self.is_included(mat):
                    self.geo.add_bnd_condition(i, mat)
            except KeyError:
                print("Material not mapped: ", str(materials[shift + i]), " - skipping it.")

    def convert(self, material_index=-1, fixation_index=-1, point_indicies=[], cell_indicies=[]):
        points, cells, point_data, cell_data = self.read_vtu()
        self.add_points(points, self.scale)
        self.add_cells(cells)
        self.geo.init_data()

        # Add Material subdomain to cells
        if material_index >= 0:
            materials = vtk_to_numpy(cell_data.GetAbstractArray(material_index))
            self.add_material_subdomain(materials)
            self.add_face_bnd(materials)
        else:
            materials = [0 for _ in range(len(self.geo.cells) + len(self.geo.faces))]
            self.add_material_subdomain(materials)
            self.add_face_bnd(materials)

        # Create Dirichlet Boundary Faces
        if fixation_index >= 0:
            fixation = vtk_to_numpy(point_data.GetAbstractArray(fixation_index))
            self.add_dirichlet(fixation)

        # Add Point and Cell data from vtu
        for pi in point_indicies:
            d = point_data.GetAbstractArray(pi).GetNumberOfComponents()
            data = vtk_to_numpy(point_data.GetAbstractArray(pi))
            if d == 1:
                data = [[item] for item in data]
            self.geo.add_vdata_array(data)

        for ci in cell_indicies:
            data = vtk_to_numpy(cell_data.GetAbstractArray(ci))
            self.geo.add_cdata_array(data[:self.geo.cell_count()])  #data[-self.geo.cell_count():] für LandEllipsoid

        ##################################################################

        ##################################################################
        # Add Excitation Data
        ##################################################################
        # activation_acnd("../../../IBT/EP_EK/EP_EK/source/", geometry)
        # activation_acnd(path, geometry)

        return self.geo

    def restrict(self, selected):
        self.selection = selected

    def is_included(self, cell_material):
        return len(self.selection) == 0 or cell_material in self.selection


def finish(geometry, default_vdata=[-10.0, 0.0, 0.0], default_cdata=[0, 0, 1, 0, 1, 0, 1, 0, 0]):
    if len(geometry.vdata) == 0:
        geometry.fill_vdata(default_vdata)
    for ci in geometry.cells:
        if len(geometry.cdata[ci]) == 0:
            geometry.add_cdata(ci, default_cdata)

    #interpolate_elphy(geometry)
    geometry.save()


def bnd_ventricle(geometry):
    for fi, face in geometry.faces.items():
        if geometry.bnd[fi] == 199:
            if 480 not in face:
                geometry.bnd[fi] = 102


def convert_ventricle():
    vtu = VtuGeometry("../../../heartgeometries/", "LV", "LeftVentricle")
    vtu.rescale(1000.0)
    mat_map = vtu.material_map
    for k in range(11):
        mat_map[30 + k] = 0
        mat_map[130 + k] = 230
    vtu.remap(mat_map)

    geometry = vtu.convert(1, 1, [], [9, 10, 11])
    robin_bnd_ventricle("../../../heartgeometries/", geometry, "LV_robinfacepoints.csv")
    # bnd_ventricle(geometry)
    activation_ventricle(geometry)
    finish(geometry)
    #
    # vtu = VtuGeometry("../../../heartgeometries/", "LV", "PlainVentricle")
    # #vtu.rescale(1000.0)
    # vtu.remap(mat_map)
    #
    # geometry = vtu.convert(1, 1, [], [])
    # activation_ventricle(geometry)
    # finish(geometry)


def bnd_fullheart(geometry, subdomain, bnd_cond):
    face_transform = [fi for fi in geometry.faces if geometry.bnd[fi] == bnd_cond]
    for ci, cell in geometry.cells.items():
        if geometry.subdomains[ci] == subdomain:
            for dface in geometry.cell_faces(ci):
                for fi in face_transform:
                    if all(p in dface for p in geometry.faces[fi]):
                        geometry.bnd[fi] = 199


def activation_fullheart_easy(geometry, default_data=[-10.0, 0.003, 1.0]):
    exc_time = {
        130: 0.24, 131: 0.27, 132: 0.0, 133: 0.004
    }

    for fi, face in geometry.faces.items():
        for pi in face:
            if geometry.bnd[fi] in exc_time:
                geometry.add_vdata(pi, [exc_time[geometry.bnd[fi]], 0.003, 1.0])
    geometry.fill_vdata(default_data)


def activation_fullheart(geometry, default_data=[-10.0, 0.003, 1.0]):
    # exc_time = {
    # 130: 0.24, 131: 0.27, 132: 0.0, 133: 0.004
    # }
    sinusnodeID = -1
    sinusnodeList = []
    counter = 0
    for p in geometry.points.values():
        if p[0] == -15.16539478302002:
            sinusnodeID = counter
        counter += 1
    sinusnodeList.append(sinusnodeID)
    for i, cell in geometry.cells.items():
        if sinusnodeID in cell:
            for id in cell:
                if id != sinusnodeID:
                    sinusnodeList.append(id)
    for id in sinusnodeList:
        geometry.add_vdata(id, [0.0, 0.003, 1.0])

    exc_time = {
        130: 0.24, 131: 0.27
    }

    for fi, face in geometry.faces.items():
        for pi in face:
            if geometry.bnd[fi] in exc_time:
                geometry.add_vdata(pi, [exc_time[geometry.bnd[fi]], 0.003, 1.0])
    geometry.fill_vdata(default_data)

def convert_T4():
    default_vdata = [-10.0, 0.0, 0.0]
    vtu = VtuGeometry("vtk/", "heartT4", "HeartT4")
    mat_map = vtu.material_map
    for k in range(11):
        mat_map[30 + k] = 1000 * k
        mat_map[130 + k] = 230 + k
    mat_map[160] = 330
    vtu.remap(mat_map)
    # vtu.restrict([m for mat in [[k * 1000 for k in range(9)], [130 + k for k in range(4)], [160]] for m in mat])
    geometry = vtu.convert( 1, -1, [], [2, 3, 4])
    activation_acnd("vtk/acnd/", geometry)
    geometry.fill_vdata(default_vdata)
    finish(geometry)
    
def convertBiVentricle():
    vtu = VtuGeometry("../../../IBT/A_W_BiVentricle/", "level_0", "BiVentricle")
    vtu.remap({0:0})
    geometry = vtu.convert(-1, -1, [], [1,2,3])
    activationByPointData(geometry,[0],"../../../IBT/A_W_BiVentricle/", "level_0")
    finish(geometry)
    
    vtu = VtuGeometry("../../../IBT/A_W_BiVentricle/", "level_1", "BiVentriclefine")
    vtu.remap({0:0})
    geometry = vtu.convert(-1, -1, [], [1,2,3])
    activationByPointData(geometry,[0],"../../../IBT/A_W_BiVentricle/", "level_1")
    finish(geometry)
def convert_heart_kovacheva():
    vtu = VtuGeometry("vtk/", "wholeHeartNotSmoothed", "KovachevaHeart")
    mat_map = vtu.material_map
    for k in range(11):
        mat_map[30 + k] = 1000 * k
        mat_map[130 + k] = 230 + k
    mat_map[160] = 330
    vtu.remap(mat_map)
    # vtu.restrict([m for mat in [[k * 1000 for k in range(9)], [130 + k for k in range(4)], [160]] for m in mat])
    geometry = vtu.convert(1, 1, [], [7, 12, 17])
    # bnd_fullheart(geometry, 8000, 250)
    # bnd_fullheart("../../../heartgeometries/", geometry, "LV_robinfacepoints.csv")
    activation_fullheart(geometry)
    finish(geometry)

def convert_plainEllipsoid():
    vtu = VtuGeometry("vtk/", "ellipsoidL2", "Ellipsoid2Layer")
    vtu.remap({0:0})
    geometry = vtu.convert(-1, -1, [], [])
    apex_endo_activaton(geometry)
    finish(geometry)

def convert_2LayerEllipsoid():
    #vtu = VtuGeometry("vtk/", "cutEllipsoid2Fibre", "CuttedEllipsoid2LayerOriented")
    #vtu.remap({0:0})
    #geometry = vtu.convert(-1, -1, [], [1,2,3])
    #activationByPointList(geometry,False)
    #finish(geometry)

    vtu = VtuGeometry("vtk/", "TestEllipsoidcellExcitation", "TestEllipsoidCellEx")
    vtu.remap({0:0})
    geometry = vtu.convert(-1, -1, [], [2,1,0])
    activationByPointList(geometry,True)
    finish(geometry)


def convert_ellipsoid():
    vtu = VtuGeometry("vtk/", "ellipsoidT4_4_fsn", "OrientedEllipsoid_4")
    vtu.rescale(1)
    #mat_map = vtu.material_map
    mat_map = {1:230, 2:330, 3:199, 30:0}
    #mat_map[1] = 230
    #mat_map[2] = 330

    # for k in range(4):
    #     mat_map[30 + k] = 0
    #     mat_map[10 + k] = 230
    vtu.remap(mat_map)
    geometry = vtu.convert(0, 0, [], [1,2, 3])
    faces_to_remove = []
    for fi in geometry.faces:
        if geometry.bnd[fi] < 100:
            faces_to_remove.append(fi)
    for fi in faces_to_remove:
        geometry.remove_face(fi)
    #activation_ventricle(geometry)
    print("Cells: ", len(geometry.cells))
    finish(geometry)

    # vtu = VtuGeometry("vtk/", "ellipsoid", "EllipsoidExcitationApex")
    # vtu.rescale(1000.0)
    # mat_map = vtu.material_map
    # for k in range(4):
    # mat_map[30 + k] = 0
    # mat_map[10 + k] = 230
    # vtu.remap(mat_map)

    # geometry = vtu.convert(1, 1, [], [2, 3, 4])
    # faces_to_remove = []
    # for fi in geometry.faces:
    # if geometry.bnd[fi] < 100:
    # faces_to_remove.append(fi)
    # for fi in faces_to_remove:
    # geometry.remove_face(fi)
    # no_activaton(geometry)
    # finish(geometry)

    # vtu = VtuGeometry("vtk/", "ellipsoid", "PlainEllipsoid")
    # vtu.rescale(1000.0)
    # mat_map = vtu.material_map
    # for k in range(4):
    #     mat_map[30 + k] = 0
    #     mat_map[10 + k] = 230
    # vtu.remap(mat_map)

    # geometry = vtu.convert(1, 1, [], [])
    # faces_to_remove = []
    # for fi in geometry.faces:
    #     if geometry.bnd[fi] < 100:
    #         faces_to_remove.append(fi)
    # for fi in faces_to_remove:
    #     geometry.remove_face(fi)
    # activation_ventricle(geometry)
    # finish(geometry)


def quarter_ellipsoid_bnd(geometry):
    # geometry.bnd.clear()
    # geometry.faces.clear()

    for ci in geometry.cells:
        for face in geometry.cell_faces(ci):
            if all(geometry.points[pi][0] == 0.0 for pi in face):
                fi = geometry.add_face(face)
                geometry.add_bnd_condition(fi, 100)
            elif all(geometry.points[pi][1] == 0.0 for pi in face):
                fi = geometry.add_face(face)
                geometry.add_bnd_condition(fi, 101)

            # elif all(geometry.points[pi][2] == 5.0 for pi in face):
            #    fi = geometry.add_face(face)
            #    geometry.add_bnd_condition(fi, 199)


def convert_quarter_ellipsoid():
    vtu = VtuGeometry("vtk/", "QuarterEllipsoid", "QuarterEllipsoid")
    vtu.rescale(1.0)
    mat_map = vtu.material_map
    for k in range(4):
        mat_map[30 + k] = 0
        mat_map[10 + k] = 230
    vtu.remap(mat_map)

    geometry = vtu.convert(0, -1, [], [1, 2, 3])
    quarter_ellipsoid_bnd(geometry)

    activation_ventricle(geometry)
    print("Cells: ", len(geometry.cells))
    finish(geometry)


def robin_bnd_ellispoid(vtuconv, geometry, surface_data):
    surface_points = []
    surface_indices = []
    with open(surface_data) as datafile:
        lines = [line.split(',') for line in datafile.readlines()]
        for line in lines[1:]:
            surface_points.append(
                [vtuconv.scale * float(line[-3]), vtuconv.scale * float(line[-2]), vtuconv.scale * float(line[-1])])

    spoints = len(surface_points)
    for _ in range(spoints):
        spoint = surface_points.pop()
        for pi, point in geometry.points.items():
            pmatch = [round(point[i], 2) == round(spoint[i], 2) for i in range(3)]
            if all(pmatch):
                surface_indices.append(pi)
                break

    for cell in geometry.cells.keys():
        for face in geometry.cell_faces(cell):
            if all(p in surface_indices for p in face):
                geometry.add_bnd_condition(geometry.add_face([p for p in face]), 330)

    # Change Dirichlet to Robin fixation
    for fi, bnd in geometry.bnd.items():
        if bnd == 199:
            geometry.add_bnd_condition(fi, 331)

def transform_robin_ellipsoid():
    vtu = VtuGeometry("vtk/", "ellipsoid", "RobinEllipsoid")
    vtu.rescale(1000.0)
    mat_map = vtu.material_map
    for k in range(4):
        mat_map[30 + k] = 0
        mat_map[10 + k] = 230
    vtu.remap(mat_map)

    geometry = vtu.convert(1, 1, [], [])
    faces_to_remove = []
    for fi in geometry.faces:
        if geometry.bnd[fi] < 100:
            faces_to_remove.append(fi)
    for fi in faces_to_remove:
        geometry.remove_face(fi)
    robin_bnd_ellispoid(vtu, geometry, "vtk/LV_robinfacepoints.csv")
    activation_ventricle(geometry)
    finish(geometry)


def convert_kovacheva():
    vtu = VtuGeometry("vtk/", "Unloaded_Geo", "TestBiventricle")
    mat_map = vtu.material_map
    mat_map[130] = 231
    mat_map[131] = 230
    mat_map[30] = 1000
    mat_map[31] = 0
    mat_map[36] = 7000
    mat_map[37] = 5000
    mat_map[38] = 8000
    vtu.remap(mat_map)
    geometry = vtu.convert(1, 1, [], [7, 9, 8])
    # activation_fullheart(geometry)
    finish(geometry)


def convert_kovachevaInstantActivation():
    vtu = VtuGeometry("vtk/", "Unloaded_Geo", "TestBiventricleInstantActivation")
    mat_map = vtu.material_map
    mat_map[130] = 231
    mat_map[131] = 230
    mat_map[30] = 1000
    mat_map[31] = 0
    mat_map[36] = 7000
    mat_map[37] = 5000
    mat_map[38] = 8000
    vtu.remap(mat_map)
    geometry = vtu.convert(1, 1, [], [7, 9, 8])
    instant_activation(geometry)
    finish(geometry)

def convertKovachevaSimpleStimulus():
    vtu = VtuGeometry("vtk/", "KovachevaLevel1", "KovachevaTestl0")
    
    points, cells, point_data, cell_data=vtu.read_vtu()
    idlist=[556,377,1011,1466,592,1043,380,860,741,1138,1291,379,523,46]
    data = vtk_to_numpy(point_data.GetAbstractArray(0))
    Excpoints=[[80.2251,-39.2528,-8.70475],[82.8159,-36.1903,-8.67192],[88.6114,-34294,-5.3367],[88.9793,-41.8553,0.386041],[84.4148,-42.8023,-6.57432],[83.9483,-40.003,-8.14911],[88.5356,-41.4902,-2.98307],[86.9446,-40.8347,-6.09057],[90.6793,-35.5745,-2.5418],[85.2274,-33.2056,-7.18132],[81.2827,-42.59,-7.42562],[89.235,-38.1623,-4.63742],[85.9538,-43.6362,-4.01681],[87.0799,-43.6532,-1.2918],[84.1461,-45.6256,-1.19714],[86.5792,-37.0951,-7.35359],[82.5376,-44848,-4.84664],[90.383,-38.9781,-1.27456]]
    datei = open('PointsLevel1.txt','w')
    for p in points:
        datei.write(str(p[0])+" "+str(p[1])+" "+str(p[2])+'\n')
 
    
    
if __name__ == "__main__":
    # convert_T4()
    # convert_2LayerEllipsoid()
    # convert_plainEllipsoid()
    #convertBiVentricle()
    # convert_kovacheva()
    #convert_kovachevaInstantActivation()
    #convert_ellipsoid()
    #convert_quarter_ellipsoid()
    # convert_ventricle()
    # convert_heart_kovacheva()

    # transform_robin_ellipsoid()
    # convert_vtu("../../../IBT/EP_EK/EP_EK/source/", "Heart", "HeartReg", 0, -1, [], [9, 10, 11])
    # convert_vtu("../../../IBT/Geomtries/", "heartT4", "HeartT4", 1, -1, [], [2, 3, 4])
    convertKovachevaSimpleStimulus()
    
