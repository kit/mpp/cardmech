


def readGeo(path,fn,new,Excpoints):
     f= open(path+fn+'.geo')
     liste=[]
     points=[]
     cells=[]
     faces=[]
     vdata=[]
     cdata=[]
     Names=['POINTS:\n','CELLS:\n','FACES:\n','VDATA:\n','CDATA:\n']
     for l in f:
         l = l.split(" ")
         liste.append(l)
     nextStart=0        
     for i  in range(len(liste)):
         if liste[i][0]==Names[1]:
             nextStart=i
             break
         else:
             points.append(liste[i])
     points=points[1:]
     for i in range(nextStart,len(liste)):
         if liste[i][0]==Names[2]:
             nextStart=i
             break
         else:
             cells.append(liste[i])
     cells=cells[1:] 
     for i in range(nextStart,len(liste)):
         if liste[i][0]==Names[3]:
             nextStart=i
             break
         else:
             faces.append(liste[i])
     faces=faces[1:] 
     for i in range(nextStart,len(liste)):
         if liste[i][0]==Names[4]:
             nextStart=i
             break
         else:
             vdata.append(liste[i])
     vdata=vdata[1:]
     for i in range(len(vdata)):
         vdata[i]=['3', '-10', '0', '0\n']
     for i in range(nextStart,len(liste)):
         cdata.append(liste[i])
     cdata=cdata[1:]
     
     idExcPoints=list(range(len(Excpoints)))
     difference=list(range(len(Excpoints)))
     counter=0
     for i in range(len(points)):
         for j in range(len(Excpoints)):
             diff=0.0
             for k in range(3):
                diff +=abs(float(points[i][k])-float(Excpoints[j][k]))
             if diff<0.005:
                 counter+=1
                 #print(points[i],Excpoints[j])
                 difference[j] =diff
                 idExcPoints[j]=i
                 #vdata[i]=['3', '0.0', '0.003', '30\n']
     if counter!=len(idExcPoints):
         print('somethig went wrong',len(idExcPoints),counter)
     #Smoothing
     idEcCells=[]
     for i  in range(len(cells)):
         for e in idExcPoints:
             if int(cells[i][2])==e:
                 idEcCells.append(int(cells[i][3]))
                 idEcCells.append(int(cells[i][4]))
                 idEcCells.append(int(cells[i][5]))
             elif int(cells[i][3])==e:
                 idEcCells.append(int(cells[i][2]))
                 idEcCells.append(int(cells[i][4]))
                 idEcCells.append(int(cells[i][5]))
             elif int(cells[i][4])==e:
                 idEcCells.append(int(cells[i][2]))
                 idEcCells.append(int(cells[i][3]))
                 idEcCells.append(int(cells[i][5]))
             elif int(cells[i][5])==e:
                 idEcCells.append(int(cells[i][2]))
                 idEcCells.append(int(cells[i][3]))
                 idEcCells.append(int(cells[i][4]))
     #for i in idEcCells:
         #vdata[i]=['3', '0.0', '0.003', '0.0\n']
     for i in idExcPoints:
         vdata[i]=['3', '0.0', '0.003', '30\n']
         
     out_f = open(path+new+'.geo' , "w")
     
     all =[points,cells,faces,vdata,cdata]
     for j in range(len(all)):
         out_f.write(Names[j])
         for p in all[j]:
             for i in range(len(p)-1):
                 out_f.write(p[i]+' ')
             out_f.write(p[-1])
    
            
     out_f.close()

if __name__=="__main__":
    path='geo/'
    
    #Kovacheva:
    fn='KovachevaBiventricle'
    newFN='KovacheveBiventricleSimpleExcitation_smooth'
    Excpoints=[[80.2251,-39.2528,-8.70475],[82.8159,-36.1903,-8.67192],[88.6114,-34294,-5.3367],[88.9793,-41.8553,0.386041],[84.4148,-42.8023,-6.57432],[83.9483,-40.003,-8.14911],[88.5356,-41.4902,-2.98307],[86.9446,-40.8347,-6.09057],[90.6793,-35.5745,-2.5418],[85.2274,-33.2056,-7.18132],[81.2827,-42.59,-7.42562],[89.235,-38.1623,-4.63742],[85.9538,-43.6362,-4.01681],[87.0799,-43.6532,-1.2918],[84.1461,-45.6256,-1.19714],[86.5792,-37.0951,-7.35359],[82.5376,-44848,-4.84664],[90.383,-38.9781,-1.27456]]
    
    
    #unloadedKovacheva
    fn='TestBiventricle'
    newFN='TestBiventricleSimpleExcitation'
    f= open('data/ExcitationPointsTestBiventricle.txt')
    Excpoints=[]
    for l in f:
        line=l.split(',')
        point=[float(line[0]),float(line[1]),float(line[2])]
        Excpoints.append(point)
        
    
    readGeo(path,fn,newFN,Excpoints)
