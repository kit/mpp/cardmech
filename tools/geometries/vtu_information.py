import argparse

from tabulate import tabulate
from vtk import *
from vtk.vtkIOXML import vtkXMLUnstructuredGridReader

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Python interface to M++')

    # Run options
    parser.add_argument('--filename', type=str, default="heartT4",
                        help='file names to be converted')
    parser.add_argument('--path', type=str, default="",
                        help='file names to be converted')

    args = parser.parse_args()

    # Read File
    reader = vtkXMLUnstructuredGridReader()
    reader.SetFileName(args.path + args.filename + ".vtu")
    reader.Update()

    data = reader.GetOutput()

    # Material Properties
    point_data = data.GetPointData()
    cell_data = data.GetCellData()

    print_data = []
    for i in range(point_data.GetNumberOfArrays()):
        array = point_data.GetAbstractArray(i)
        print_data.append([i, array.GetName(), array.GetNumberOfComponents()])
    print(tabulate(print_data, headers=["Index", "Point Data Array", "Dimension"]))

    print_data = []
    for i in range(cell_data.GetNumberOfArrays()):
        array = cell_data.GetAbstractArray(i)
        print_data.append([i, array.GetName(), array.GetNumberOfComponents()])
    print(tabulate(print_data, headers=["Index", "Cell Data Array", "Dimension"]))
