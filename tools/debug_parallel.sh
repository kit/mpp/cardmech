#!/usr/bin/env bash
cd ../build
echo 'Building Project...'
make -j
echo 'Debugging on 4 Cores...'
mpirun -np 4 xterm -hold -e valgrind --track-origins=yes ./M++ &
