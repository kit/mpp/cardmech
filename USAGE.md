# Using CardMech-M++

To use the CardMech suite, simply run
You can run the predefined code by using the command

```./M++``` or ```mpirun -n N M++```

where N is the number of parallel processes you want to start.

When executed, M++ searches for the ```m++.conf``` Config file in the ```conf/``` folder, which specifies all program
arguments

## .conf Files

The default ```m++.conf``` specifies, which part of the CardMech suite is executed:

```
#loadconf = electrophysiology/m++.conf;
loadconf = elasticity/m++.conf;
#loadconf = coupled/m++.conf;
```

Only one of these configurations should be loaded at one time, as the different configurations may interfere with each
other

### Problems

Problems are categorized in elasticity, electrophysiological and coupled problems. They are loaded with the following
commands:

```
MechProblem = Default
ElphyProblem = TetraTestProblem
CoupledProblem = BiventricleCoarse
```
