import sys
sys.path.append('cardmech/')
import os

import mpp.python.mppy as mppy

lList = [5]
tList = [0]
start_dt=0.0001

path ='../../datafiles/TestMpp/'
pathToMakeFolders='../datafiles/TestMpp/'
#path='../../data/Ellipsoid2Layer/'
#pathToMakeFolders='../data/Ellipsoid2Layer/'
#pathExperiment=''




elphyProblem='EllipsoidProblem'
crankNicolsonTheta=0.5
#crankNicolsonTheta=1.0
externalCurrentSmoothingInSpace ='linear' #'discrete'
isExternalCurrentSmooth=1 #0


def startExperiment(procs,startconf,pE,added=None):
    checkIfFolderExists(pE)
    mpp = mppy.Mpp(project_name='CardMech',executable='Elphy-M++',kernels=4,mute=False)
    
    for l in lList:
        for j in tList:
            file = path+pE+'log_l'+str(l)+'j'+str(j)+'m1'
            kwargs={"ElphyProblem":elphyProblem,"ElphyLevel": l,"DeltaTime":start_dt*2**(-j),"ExternalCurrentSmoothingInSpace":externalCurrentSmoothingInSpace, "IsExternalCurrentSmooth":isExternalCurrentSmooth,"logfile":file, "CrankNicolsonTheta":crankNicolsonTheta}
            if added!=None:
                kwargs.update(added)
            mpp.run(procs, config=startconf, kwargs=kwargs) #replace 64 by hosts if necessary



            
def checkIfFolderExists(path):
    folderPath =pathToMakeFolders+path
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)   
    
if __name__=="__main__":
    procs=32
    
    startconf="HexaCubeSplitting/start"
    pathExperiment="G0001SAT/"
    startExperiment(procs,startconf,pathExperiment,{"IextInPDE":1,"ReassembleRHSonce":1})
    
    #startconf="HexaCubeSplitting/start"
    #pathExperiment="G0001SATinODE/"
    #startExperiment(procs,startconf,pathExperiment,{"IextInPDE":0,"ReassembleRHSonce":1})
    
    #startconf="HexaTestCube/start"
    #pathExperiment="SI0001SAT/"
    #startExperiment(procs,startconf,pathExperiment,{"ElphyModel":'SemiImplicit',"IextInPDE":1,"ReassembleRHSonce":1})
    
    #startconf="HexaTestCube/start"
    #pathExperiment="LI0001SAT/"
    #startExperiment(procs,startconf,pathExperiment,{"IextInPDE":1,"ReassembleRHSonce":1})
    
    #startconf="HexaTestCube/start"
    #pathExperiment="LI0001SATNodes/"
    #startExperiment(procs,startconf,pathExperiment,{"ElphyModel":'LinearImplicitNodes',"IextInPDE":1,"ReassembleRHSonce":1})
    
    #startconf="HexaTestCube/start"
    #pathExperiment="IE0001SAT/"
    #startExperiment(procs,startconf,pathExperiment,{"ElphyModel":'ImplictEuler',"IextInPDE":1,"ReassembleRHSonce":1})
    
    
    
